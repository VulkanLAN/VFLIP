/**
 * HalloServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2beta3 Aug 17, 2004 (09:50:21 EDT) WSDL2Java emitter.
 */

package net.sf.flip.soap.test.client;

public class HalloServiceLocator extends org.apache.axis.client.Service implements net.sf.flip.soap.test.client.HalloService {

    // Use to get a proxy class for Hallo
    private java.lang.String Hallo_address = "http://stockholm:5555/axis/services/Hallo";

    public java.lang.String getHalloAddress() {
        return Hallo_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String HalloWSDDServiceName = "Hallo";

    public java.lang.String getHalloWSDDServiceName() {
        return HalloWSDDServiceName;
    }

    public void setHalloWSDDServiceName(java.lang.String name) {
        HalloWSDDServiceName = name;
    }

    public net.sf.flip.soap.test.client.Hallo getHallo() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(Hallo_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getHallo(endpoint);
    }

    public net.sf.flip.soap.test.client.Hallo getHallo(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            net.sf.flip.soap.test.client.HalloSoapBindingStub _stub = new net.sf.flip.soap.test.client.HalloSoapBindingStub(portAddress, this);
            _stub.setPortName(getHalloWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setHalloEndpointAddress(java.lang.String address) {
        Hallo_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (net.sf.flip.soap.test.client.Hallo.class.isAssignableFrom(serviceEndpointInterface)) {
                net.sf.flip.soap.test.client.HalloSoapBindingStub _stub = new net.sf.flip.soap.test.client.HalloSoapBindingStub(new java.net.URL(Hallo_address), this);
                _stub.setPortName(getHalloWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("Hallo".equals(inputPortName)) {
            return getHallo();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://stockholm:5555/axis/services/Hallo", "HalloService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://stockholm:5555/axis/services/Hallo", "Hallo"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        if ("Hallo".equals(portName)) {
            setHalloEndpointAddress(address);
        }
        else { // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
