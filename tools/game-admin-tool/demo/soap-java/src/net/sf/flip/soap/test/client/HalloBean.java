/**
 * HalloBean.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2beta3 Aug 17, 2004 (09:50:21 EDT) WSDL2Java emitter.
 */

package net.sf.flip.soap.test.client;

public class HalloBean  implements java.io.Serializable {
    private int testInt;
    private java.lang.String testString;

    public HalloBean() {
    }

    public HalloBean(
           int testInt,
           java.lang.String testString) {
           this.testInt = testInt;
           this.testString = testString;
    }


    /**
     * Gets the testInt value for this HalloBean.
     * 
     * @return testInt
     */
    public int getTestInt() {
        return testInt;
    }


    /**
     * Sets the testInt value for this HalloBean.
     * 
     * @param testInt
     */
    public void setTestInt(int testInt) {
        this.testInt = testInt;
    }


    /**
     * Gets the testString value for this HalloBean.
     * 
     * @return testString
     */
    public java.lang.String getTestString() {
        return testString;
    }


    /**
     * Sets the testString value for this HalloBean.
     * 
     * @param testString
     */
    public void setTestString(java.lang.String testString) {
        this.testString = testString;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HalloBean)) return false;
        HalloBean other = (HalloBean) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.testInt == other.getTestInt() &&
            ((this.testString==null && other.getTestString()==null) || 
             (this.testString!=null &&
              this.testString.equals(other.getTestString())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getTestInt();
        if (getTestString() != null) {
            _hashCode += getTestString().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HalloBean.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://test.soap.flip.sf.net", "HalloBean"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("testInt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "testInt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("testString");
        elemField.setXmlName(new javax.xml.namespace.QName("", "testString"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
