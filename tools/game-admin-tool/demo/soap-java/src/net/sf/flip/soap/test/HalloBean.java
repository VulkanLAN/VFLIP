package net.sf.flip.soap.test;

import java.io.Serializable;

/**
 * einfache Beanklasse
 */
public class HalloBean implements Serializable{
    private String testString;
    private int testInt = 0;

    public String getTestString() {
        return this.testString;
    }

    public void setTestString(String testString) {
        this.testString = testString;
    }

    public int getTestInt() {
        return this.testInt;
    }

    public void setTestInt(int testInt) {
        this.testInt = testInt;
    }

    public String toString(){
        StringBuffer ausgabe = new StringBuffer("testString = ");
        if (this.testString == null) {
            ausgabe.append("null");
        } else {
            ausgabe.append(this.testString);
        }
        ausgabe.append(", testInt = ");
        ausgabe.append(Integer.toString(this.testInt));
        return ausgabe.toString();
    }
}
