/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.flip.bean;

import java.util.HashMap;
import java.util.Map;

public class IconList {
	private final String type;

	private Map<Integer, String> list;

	public IconList(final String type) {
		this.type = type;
		this.list = new HashMap<Integer, String>();
	}

	@Override
	public String toString() {
		return this.getType();
	}

	public String getType() {
		return this.type;
	}

	public Map<Integer, String> getList() {
		return this.list;
	}

	public void setList(final Map<Integer, String> list) {
		this.list = list;
	}
}
