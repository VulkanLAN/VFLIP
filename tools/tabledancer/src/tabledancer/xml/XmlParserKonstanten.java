/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.xml;

/**
 * Diese Klasse enthaelt nur die Namen alle Tags, damit diese nicht mehrfach
 * instaziert werden muessen
 */
public abstract class XmlParserKonstanten {
	// Alle Tags
	public static final String BLOCK = "block", BLOCKS = "blocks", CAPTION = "caption", SEAT = "seat",
			CENTERX = "center_x", CENTERY = "center_y", ID = "id", ENABLED = "enabled", ANGLE = "angle", NAME = "name",
			IP = "ip", DESCRIPTION = "description", VIEW_RIGHT = "view_right", LINK = "link", HREF = "href",
			IMAGE_DIRECTORY = "imagedir_block", TABLEGREEN = "tablegreen", TABLEGREY = "tablegrey", CHAIR = "chair",
			ANGLE_ATTRIB = "angle", SEATS = "seats";

	// Feste Werte
	public static final String YES = "Y", NO = "N";
}
