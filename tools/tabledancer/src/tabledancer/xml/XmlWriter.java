/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.xml;

import java.io.IOException;
import java.util.List;

import tabledancer.bean.Block;
import tabledancer.bean.Seat;

/**
 * Klasse zum schreiben des XML-Streams - speichern.
 */
public class XmlWriter extends XmlWriterKonstanten {
	public static String write(List<Block> blocks) throws IOException {
		// todo: DOCTYPE vernueftig definieren
		// StringBuffer buffer=new StringBuffer("<DOCTYPE...>\n\n<seats>");
		StringBuffer aBuffer = new StringBuffer(XML_BLOCKS_AUF);
		for (int i = 0; i < blocks.size(); i++) {
			writeBlock(aBuffer, blocks.get(i));
		}
		aBuffer.append(XML_BLOCKS_ZU);
		aBuffer.append("<!-- Generiert mit FLIP TableDancer -->\n");
		return aBuffer.toString();
	}

	private static void writeBlock(StringBuffer myBuffer, Block myBlock) {
		myBuffer.append(XML_BLOCK_AUF);
		myBuffer.append(XML_BLOCKID_AUF).append(myBlock.getId()).append(XML_BLOCKID_ZU);
		myBuffer.append(XML_BLOCKCAPTION_AUF).append(myBlock.getId()).append(XML_BLOCKCAPTION_ZU);
		myBuffer.append(XML_BLOCKDESCRIPTION_AUF).append(encode(myBlock.getDescription()))
				.append(XML_BLOCKDESCRIPTION_ZU);
		myBuffer.append(XML_BLOCKVIEWRIGHT_AUF).append(myBlock.getViewRight()).append(XML_BLOCKVIEWRIGHT_ZU);
		myBuffer.append(XML_BLOCKIMAGEDIR_AUF).append(myBlock.getImageDirectory()).append(XML_BLOCKIMAGEDIR_ZU);
		myBuffer.append(XML_BLOCKLINK_AUF).append(encode(myBlock.getLink())).append(XML_BLOCKLINK_ZU);
		myBuffer.append(XML_BLOCKHREF_AUF).append(encode(myBlock.getHref())).append(XML_BLOCKHREF_ZU);
		myBuffer.append(XML_SEATS_AUF);
		List<Seat> seats = myBlock.getSeats();
		for (Seat aSeat : seats) {
			writeSeat(myBuffer, aSeat);
		}
		myBuffer.append(XML_SEATS_ZU);
		myBuffer.append(XML_BLOCK_ZU);
	}

	private static void writeSeat(StringBuffer myBuffer, Seat mySeat) {
		myBuffer.append(XML_SEAT_AUF);
		myBuffer.append(XML_ID_AUF).append(mySeat.getId()).append(XML_ID_ZU);
		myBuffer.append(XML_ENABLED_AUF).append(mySeat.isEnabled() ? YES : NO).append(XML_ENABLED_ZU);
		myBuffer.append(XML_CENTERX_AUF).append(mySeat.getCenterPosX()).append(XML_CENTERX_ZU);
		myBuffer.append(XML_CENTERY_AUF).append(mySeat.getCenterPosY()).append(XML_CENTERY_ZU);
		myBuffer.append(XML_ANGLE_AUF).append(mySeat.getAngle()).append(XML_ANGLE_ZU);
		myBuffer.append(XML_NAME_AUF).append(encode(mySeat.getName())).append(XML_NAME_ZU);
		myBuffer.append(XML_IP_AUF).append(mySeat.getIp()).append(XML_IP_ZU);
		myBuffer.append(XML_SEAT_ZU);
	}

	public static String encode(String text) {
		text = replace(text, CHAR_AMP, XML_AMP);
		text = replace(text, CHAR_LT, XML_LT);
		text = replace(text, CHAR_GT, XML_GT);
		return text;
	}

	private static String replace(String text, char find, String replace) {
		int i = 0, j = 0;
		while ((i = i + text.substring(j).indexOf(find)) > j) {
			j = i + 1;
			text = text.substring(0, i) + replace + text.substring(j);
		}
		return text;
	}
}
