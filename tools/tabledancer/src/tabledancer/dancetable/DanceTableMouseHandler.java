/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.dancetable;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;

import javax.swing.JPopupMenu;

import tabledancer.dancefloor.DanceFloor;

/**
 * Created by IntelliJ IDEA. User: jens Date: Jun 13, 2005 Time: 4:12:36 PM To
 * change this template use File | Settings | File Templates.
 */
public class DanceTableMouseHandler implements MouseMotionListener {
	private DanceTable table;

	private JPopupMenu tablePopUp;

	private int startx = 0, starty = 0;

	private DanceFloor floor;

	public DanceTableMouseHandler(DanceTable table, DanceFloor floor) {
		this.table = table;
		this.floor = floor;
		DanceTableMenu menu = new DanceTableMenu(table);
		tablePopUp = menu.setUp();
	}

	/**
	 * Verarbeitung jeglicher Mausevents
	 * 
	 * @param event
	 */
	public void processMouseEvent(MouseEvent event) {
		if (event.isPopupTrigger()) {
			/* PopUp */
			System.out.println("DanceTableMouseHandler.processMouseEvent()->trigger");
			event.getComponent().requestFocus();
			tablePopUp.show(event.getComponent(), event.getX(), event.getY());
		} else if (event.getID() == MouseEvent.MOUSE_PRESSED && (event.getModifiers() & InputEvent.BUTTON1_MASK) != 0
				&& table.isInnerBorder(event.getPoint())) {
			/* ziehen */
			System.out.println("DanceTable.processMouseEvent()->ziehen");
			if (!table.isSelected()) {
				table.isTemporarySelected = true;
			}
			floor.setDragCanceled(false);

			floor.setGettingDragged(true);

			Point dragpoint = new Point(event.getX(), event.getY());
			startx = dragpoint.x;
			starty = dragpoint.y;

			floor.setDragPosition(0, 0);
			table.getDragInstance().requestFocus();
			floor.setCursor(new Cursor(Cursor.MOVE_CURSOR));
		} else if (event.getID() == MouseEvent.MOUSE_RELEASED && (event.getModifiers() & InputEvent.BUTTON1_MASK) != 0
				&& floor.isGettingDragged()) {
			/* loslassen */
			System.out.println("DanceTableMouseHandler.processMouseEvent()->loslassen");
			floor.fixSelectedPosition();
			floor.setGettingDragged(false);
			floor.setShiftGedrueckt(false);
			table.isTemporarySelected = false;

			floor.setDragCanceled(false);

			if ((event.getModifiers() & InputEvent.SHIFT_MASK) != 0) {
				table.setSelected(!table.isSelected());
			} else {
				floor.setSelectedAll(false);
			}
			table.setVisible(true);
			if (!table.hasFocus()) {
				table.requestFocus();
			}
			floor.setCursor(new Cursor(Cursor.HAND_CURSOR));
		}
	}

	public void processMouseMotionEvent(MouseEvent event) {
		if (event.getID() == MouseEvent.MOUSE_DRAGGED && (event.getModifiers() & InputEvent.BUTTON1_MASK) != 0) {
			if (floor.isGettingDragged()) {
				table.setSelected(true);
				Point dragpoint = new Point(event.getX(), event.getY());
				floor.setDragPosition(dragpoint.x - startx, dragpoint.y - starty);
				startx = dragpoint.x;
				starty = dragpoint.y;
			} else {
				event.consume();
			}
		} else {
			if (table.isInnerBorder(event.getPoint())) {
				floor.setCursor(new Cursor(Cursor.HAND_CURSOR));
			}
		}
	}

	public void processMouseWheelEvent(MouseWheelEvent event) {
		// Habe kein Rad. Kann ich nicht testen :(
		if (event.getID() == MouseEvent.MOUSE_WHEEL) {
			int addition = event.getWheelRotation();
			if ((event.getModifiers() & InputEvent.CTRL_MASK) != 0) {
				table.setTableAngle(table.getSeat().getAngle() + (addition * 5));
			} else {
				table.setTableAngle(table.getSeat().getAngle() + addition);
			}

			table.repaint();
		}
	}

	public void mouseDragged(MouseEvent mouseEvent) {
	}

	public void mouseMoved(MouseEvent mouseEvent) {
	}
}
