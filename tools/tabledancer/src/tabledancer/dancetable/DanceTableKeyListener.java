/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.dancetable;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Verarbeitung der Tastatureingaben
 */
public class DanceTableKeyListener implements KeyListener {
	public void keyPressed(KeyEvent event) {
		System.out.println("DanceTableKeyListener.keyPressed()");
	}

	public void keyReleased(KeyEvent event) {
		System.out.println("DanceTableKeyListener.keyReleased()");
	}

	public void keyTyped(KeyEvent event) {
		System.out.println("DanceTableKeyListener.keyTyped()");
	}
	// todo: Verarbeitung implementieren
}
