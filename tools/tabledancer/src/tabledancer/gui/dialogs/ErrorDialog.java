/**
 * @copyright 2001-2010 The FLIP Project Team
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 */
package tabledancer.gui.dialogs;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.GroupLayout.SequentialGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * This class implements a simple error dialog, that displays a title, an error
 * message and (if applicable) a full stacktrace of the raised exception/error.
 * 
 * @author Matthias 'Scope' Gross
 * @version $Id$
 */
public class ErrorDialog extends JDialog {

	/**
	 * The serial version ID, should this class be ever serialized...
	 */
	private static final long serialVersionUID = 5569555496193546675L;

	/**
	 * Stores the current title message
	 */
	private String titleMessage = "Error";

	/**
	 * Stores the stack frames of the current exception
	 */
	private Vector<StackTraceElement> stackTrace = null;

	/**
	 * Reference to the text box which will contain the stack trace
	 */
	private JTextArea traceTextBox = null;

	/**
	 * Reference to the label which contains the error message
	 */
	private JLabel errorMessageLabel = null;

	/**
	 * Reference to the JPanel surrounding the trace text box
	 */
	private JPanel tracePanel = null;

	/**
	 * Internal class that handles the event when the OK button is clicked.
	 * 
	 * @author Scope
	 */
	private class ErrorDialogOKHandler extends AbstractAction {

		/**
		 * 
		 */
		private static final long serialVersionUID = -3863890231772238480L;

		@Override
		public void actionPerformed(ActionEvent arg0) {
			ErrorDialog.this.setVisible(false);
		}

	}

	/**
	 * The Default constructor. It initializes the dialog but does nothing else.
	 */
	public ErrorDialog() {
		this.initGUI();
		this.stackTrace = new Vector<StackTraceElement>();
	}

	/**
	 * Sets the error title and message but leaves the stack trace clear.
	 * 
	 * @param errorTitle
	 * @param errorMessage
	 */
	public ErrorDialog(String errorTitle, String errorMessage) {
		this.initGUI();
		this.stackTrace = new Vector<StackTraceElement>();

		this.setErrorTitle(errorTitle);
		this.setErrorMessage(errorMessage);
	}

	/**
	 * Sets the error title and -message but also the stack trace.
	 * 
	 * @param errorTitle
	 * @param errorMessage
	 * @param stackTrace
	 */
	public ErrorDialog(String errorTitle, String errorMessage, StackTraceElement stackTrace[]) {
		this.initGUI();
		this.stackTrace = new Vector<StackTraceElement>();

		this.setErrorTitle(errorTitle);
		this.setErrorMessage(errorMessage);
		this.setStackTrace(stackTrace);
	}

	/**
	 * Sets the error title, e.g. this dialog's title.
	 * 
	 * @param theTitle
	 *            The title to set
	 */
	public void setErrorTitle(String theTitle) {
		this.setTitle(theTitle);
	}

	/**
	 * Sets the error message that the user will see
	 * 
	 * @param theMessage
	 *            The error message describing what went wrong
	 */
	public void setErrorMessage(String theMessage) {
		this.errorMessageLabel.setText(theMessage);
	}

	/**
	 * Sets the stack trace to display. If trace is null, the box' trace will
	 * just be cleared.
	 * 
	 * @param trace
	 *            The stack trace to copy
	 */
	public void setStackTrace(StackTraceElement trace[]) {
		this.stackTrace.clear();

		// If the trace array in null, exit after the internal
		// store has been cleared and clear the text field.
		if (trace == null) {
			this.traceTextBox.setText("");
			return;
		}

		// Copy the array into the collection and build the
		// stack trace text.
		StringBuilder stackTraceText = new StringBuilder();

		for (int curPos = 0; curPos < trace.length; curPos++) {
			this.stackTrace.add(trace[curPos]);
			stackTraceText.append(trace[curPos].toString());
			stackTraceText.append("\n");
		}

		// Display the complete trace in the dialog's text area
		this.traceTextBox.setText(stackTraceText.toString());
	}

	/**
	 * Hides or shows the stack trace section of the error dialog.
	 * 
	 * @param isVisible
	 */
	public void setStackTraceVisible(boolean isVisible) {
		if (this.tracePanel == null) {
			return;
		}

		this.tracePanel.setVisible(isVisible);
	}

	/* ******************* */
	/* GUI builder methods */
	/* ******************* */

	/**
	 * Prepares and constructs the GUI of this dialog.
	 */
	private void initGUI() {
		this.setTitle(this.titleMessage);

		// Generate the parts of the GUI
		JPanel errorPanel = this.createErrorPanel();
		JPanel stackTracePanel = this.createTracePanel();
		JPanel buttonPanel = this.createButtonPanel();

		this.tracePanel = stackTracePanel;

		// Create layout objects
		GroupLayout mainLayout = new GroupLayout(this.getContentPane());

		this.getContentPane().setLayout(mainLayout);

		mainLayout.setAutoCreateGaps(true);
		mainLayout.setAutoCreateContainerGaps(true);

		// Horizontal grouping
		ParallelGroup horizontalGroup = mainLayout.createParallelGroup();

		horizontalGroup.addGroup(mainLayout.createParallelGroup().addComponent(errorPanel)
				.addComponent(stackTracePanel).addGap(1).addComponent(buttonPanel));
		mainLayout.setHorizontalGroup(horizontalGroup);

		// Vertical grouping
		SequentialGroup verticalGroup = mainLayout.createSequentialGroup();

		verticalGroup.addGroup(mainLayout.createParallelGroup(Alignment.BASELINE).addComponent(errorPanel,
				Alignment.BASELINE, 50, 75, 75));
		verticalGroup.addGroup(mainLayout.createParallelGroup(Alignment.BASELINE).addComponent(stackTracePanel));
		verticalGroup.addGroup(mainLayout.createParallelGroup(Alignment.BASELINE).addComponent(buttonPanel,
				Alignment.CENTER, 35, 35, 35));
		mainLayout.setVerticalGroup(verticalGroup);

		// Pack and adjust the default position of the dialog
		this.setLocationByPlatform(true);
		this.pack();
	}

	/**
	 * Creates the error message label which contains a brief description of
	 * what went wrong. The label is contained in a JPanel.
	 * 
	 * @return The created panel
	 */
	private JPanel createErrorPanel() {
		JPanel errPanel = new JPanel(new GridLayout());
		JLabel errMsg = new JLabel();

		errorMessageLabel = errMsg;

		errMsg.setAlignmentY(JLabel.TOP_ALIGNMENT);
		errMsg.setText("Default message");
		errPanel.add(errMsg);

		return errPanel;
	}

	/**
	 * Creates the panel that contains the complete stack trace text.
	 * 
	 * @return The created panel
	 */
	private JPanel createTracePanel() {
		JPanel tracePanel = new JPanel(new GridLayout());
		JTextArea traceTextBox = new JTextArea();

		this.traceTextBox = traceTextBox;

		traceTextBox.setPreferredSize(new Dimension(250, 50));
		traceTextBox.setBorder(BorderFactory.createEmptyBorder());

		tracePanel.add(new JScrollPane(traceTextBox));
		tracePanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), "Stacktrace"));

		return tracePanel;
	}

	/**
	 * Creates the panel that contains all buttons of the dialog.
	 * 
	 * @return The created panel
	 */
	private JPanel createButtonPanel() {
		JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		JButton okButton = new JButton("OK");

		okButton.addActionListener(new ErrorDialogOKHandler());
		okButton.setMaximumSize(new Dimension(100, 25));

		buttonPanel.setPreferredSize(new Dimension(50, 50));
		buttonPanel.add(okButton);

		return buttonPanel;
	}

	/**
	 * Displays a full error message (title + message itself) along with a stack
	 * trace for the developers to examine.
	 * 
	 * @param title
	 * @param message
	 * @param trace
	 */
	public static void showException(String title, String message, StackTraceElement trace[]) {
		ErrorDialog dlgException = new ErrorDialog(title, message, trace);

		dlgException.setModal(true);
		dlgException.setVisible(true);
	}
}
