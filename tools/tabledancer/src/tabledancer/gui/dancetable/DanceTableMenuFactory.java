/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.gui.dancetable;

import static tabledancer.Constants.COMMAND_CLONE;
import static tabledancer.Constants.COMMAND_REMOVE;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 * the dancefloor popup menu
 * 
 * @author Mario 'Xchange' Steinhoff
 * 
 * @version 1.0
 * @since 1.1
 */
public class DanceTableMenuFactory {
	/**
	 * the singleton instance
	 */
	private static DanceTableMenuFactory instance = new DanceTableMenuFactory();

	/**
	 * the item list
	 */
	private final MenuItem[] items = { new MenuItem("Klonen", COMMAND_CLONE), new MenuItem("Loeschen", COMMAND_REMOVE),
			new MenuItem("0", null), new MenuItem("45", null), new MenuItem("90", null), new MenuItem("135", null),
			new MenuItem("180", null), new MenuItem("225", null), new MenuItem("270", null), new MenuItem("315", null) };

	/**
	 * helper class
	 */
	class MenuItem {
		/**
		 * the name
		 */
		private String name;

		/**
		 * the action command
		 */
		private String actionCommand;

		/**
		 * instantiates a new menuitem object
		 * 
		 * @param String
		 *            name the name
		 * @param String
		 *            actionCommand the action command
		 */
		public MenuItem(final String name, final String actionCommand) {
			this.name = name;
			this.actionCommand = actionCommand;
		}

		/**
		 * 
		 * @return String the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * 
		 * @return String the action command
		 */
		public String getActionCommand() {
			return actionCommand;
		}

		/**
		 * creates a new JMenuItem from the current object values
		 * 
		 * @return JMenuItem the new JMenuItem object
		 */
		public JMenuItem getItem() {
			final JMenuItem item;

			item = new JMenuItem();

			item.setText(name);

			if (actionCommand != null) {
				item.setActionCommand(actionCommand);
			}

			return item;
		}
	}

	/**
	 * 
	 * @return DanceFloorMenu the singleton instance
	 */
	public static DanceTableMenuFactory getInstance() {
		return instance;
	}

	/**
	 * factory method for a popup menu
	 * 
	 * @param DanceTable
	 *            danceTable the dancetable the menu should control
	 * 
	 * @return JPopupMenu the new JPopupMenu instance
	 */
	public JPopupMenu newMenu(final DanceTable danceTable) {
		final JPopupMenu popupMenu;
		final DanceTableMenuActionListener actionListener;

		popupMenu = new JPopupMenu();
		actionListener = new DanceTableMenuActionListener(danceTable);

		for (final MenuItem element : items) {
			final JMenuItem item;

			item = element.getItem();
			item.addActionListener(actionListener);

			popupMenu.add(item);
		}

		return popupMenu;
	}
}
