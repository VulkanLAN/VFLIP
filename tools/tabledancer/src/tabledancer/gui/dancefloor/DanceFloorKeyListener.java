/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.gui.dancefloor;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Verarbeitung der Tastatureingaben
 * 
 * @author Mario 'Xchange' Steinhoff
 * 
 * @version 1.0
 * @since 1.1
 */
public class DanceFloorKeyListener implements KeyListener {
	/**
	 * the dancetablepanel context
	 */
	private final DanceFloor danceFloor;

	/**
	 * construktor
	 * 
	 * @param DanceTablePanel
	 *            table the table context
	 */
	public DanceFloorKeyListener(final DanceFloor danceFloor) {
		this.danceFloor = danceFloor;
	}

	/**
	 * Invoked when a key has been pressed. See the class description for
	 * KeyEvent for a definition of a key pressed event.
	 * 
	 * @param KeyEvent
	 *            event the event
	 */
	public void keyPressed(final KeyEvent event) {
	}

	/**
	 * Invoked when a key has been released. See the class description for
	 * KeyEvent for a definition of a key released event.
	 * 
	 * @param KeyEvent
	 *            event the event
	 */
	public void keyReleased(final KeyEvent event) {
	}

	/**
	 * Invoked when a key has been typed. See the class description for KeyEvent
	 * for a definition of a key typed event.
	 * 
	 * @param KeyEvent
	 *            event the event
	 */
	public void keyTyped(final KeyEvent event) {
		if ((new Integer(event.getKeyChar()).intValue() == KeyEvent.VK_DELETE)) {
			danceFloor.removeSelectedTables();
		}
	}
}
