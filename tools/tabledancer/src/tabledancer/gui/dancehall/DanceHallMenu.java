/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.gui.dancehall;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 * das Kontext-Men� f�r einen DanceFloor
 */
public class DanceHallMenu {
	/**
	 * der danceFloor kontext
	 */
	private DanceHall danceHall;

	/**
	 * die men�-instanz
	 */
	private JPopupMenu popupMenu;

	/**
	 * die men�-eintr�ge
	 */
	private final MenuItem[] menuItems = {
			// Name Checkbox Default
			new MenuItem("New Block", false, false), new MenuItem("Display grid", true, true),
			new MenuItem("Align to grid", true, false), new MenuItem("Background image", true, true),
			new MenuItem("Select all", false, false) };

	/**
	 * Konstruktor
	 * 
	 * @param DanceFloor
	 *            dancefloor
	 */
	public DanceHallMenu(final DanceHall danceHall) {
		this.danceHall = danceHall;
	}

	/**
	 * Liefert das Menu. Caching vorhanden.
	 * 
	 * @return
	 */
	public JPopupMenu requestMenu() {
		if (popupMenu != null) {
			return popupMenu;
		}

		final DanceHallMenuActionListener actionListener;

		popupMenu = new JPopupMenu();
		actionListener = new DanceHallMenuActionListener(danceHall);

		for (int i = 0; i < menuItems.length; i++) {
			final JMenuItem item;
			item = menuItems[i].getItem();

			item.addActionListener(actionListener);
			item.setAction(null);
			item.setName(Integer.toString(i + 1));
			item.setText(menuItems[i].getName());
			item.setSelected(menuItems[i].isDefault());

			popupMenu.add(item);
		}

		return popupMenu;
	}

	/**
	 * kleine Hilfsklasse fuers Menu
	 */
	class MenuItem {
		private String name;
		private boolean isCheckbox;
		private boolean isDefault;

		public MenuItem(final String name, final boolean isCheckbox, final boolean isDefault) {
			this.name = name;
			this.isCheckbox = isCheckbox;
			this.isDefault = isDefault;
		}

		public String getName() {
			return name;
		}

		public JMenuItem getItem() {
			if (isCheckbox) {
				return new JCheckBoxMenuItem();
			}

			return new JMenuItem();
		}

		public boolean isDefault() {
			return isDefault;
		}
	}
}
