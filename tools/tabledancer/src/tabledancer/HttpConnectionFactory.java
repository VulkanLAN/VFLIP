/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer;

import static tabledancer.Constants.CONFIG_DEFAULT_USER_AGENT;
import static tabledancer.Constants.EMPTY_OBJECT_ARRAY;
import static tabledancer.Constants.EMPTY_STRING;
import static tabledancer.Constants.ERROR_INCORRECT_CONNECTION_TYPE;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Formatter;

/**
 * low level interface to the backend system
 * 
 * @author Mario 'Xchange' Steinhoff
 * 
 * @version 1.0
 * @since 1.1
 */
public class HttpConnectionFactory {
	public static final String HTTP_PROTOCOL_PREFIX = "http://";

	public static final String HTTP_REQUEST_USER_AGENT = "User-Agent";
	public static final String HTTP_REQUEST_CONTENT_TYPE = "Content-Type";

	public static final String HTTP_METHOD_GET = "GET";
	public static final String HTTP_METHOD_POST = "POST";

	/**
	 * returns a url for the backend system.
	 * 
	 * if it seems that this is not an absolute http url, i.e. it does not start
	 * with http://, the base url of the applet is prepended.
	 * 
	 * @param String
	 *            url the url string
	 * 
	 * @return String the complete url
	 */
	public static String getBackendUrl(final String url) {
		return getBackendUrl(url, EMPTY_OBJECT_ARRAY);
	}

	/**
	 * returns a formatted url for the backend system.
	 * 
	 * if it seems that this is not an absolute http url, i.e. it does not start
	 * with http://, the base url of the applet is prepended.
	 * 
	 * this method uses String.format for formatting.
	 * 
	 * @param String
	 *            url the url string
	 * @param Object
	 *            ... arguments potential argument data the url string should be
	 *            formatted with
	 * 
	 * @return String the complete formatted url
	 * 
	 * @see String#format(String, Object...)
	 * @see Formatter
	 */
	public static String getBackendUrl(final String url, final Object... arguments) {
		if (url == null) {
			return EMPTY_STRING;
		}

		final StringBuilder sb;
		final String result;

		sb = new StringBuilder();

		if (!url.startsWith(HTTP_PROTOCOL_PREFIX)) {
			sb.append(TableDancer.getInstance().getBaseUrl());
		}

		if (arguments == null || arguments.length == 0) {
			sb.append(url);
		} else {
			sb.append(String.format(url, arguments));
		}

		result = sb.toString();

		return result;
	}

	/**
	 * returns the user agent of the browser the applet runs in, or a default
	 * string if no user agent parameter was found. the user agent string
	 * returned by this method is used for all http requests made.
	 * 
	 * the user agent parameter should be provided by the backend system.
	 * 
	 * @return String the user agent string
	 */
	public static String getUserAgent() {
		final String result;

		result = TableDancer.getInstance().getParameter(TableDancer.PARAM_USERAGENT);

		if (result == null || result.equals(EMPTY_STRING)) {
			return CONFIG_DEFAULT_USER_AGENT;
		}

		return result;
	}

	/**
	 * factory method for a new HttpURLConnection object
	 * 
	 * this method creates a new HttpURLConnection object with the given
	 * parameters. it sets the user agent string provided by getUserAgent() and
	 * disables all caching.
	 * 
	 * @param String
	 *            urlString the url string
	 * @param String
	 *            urlMethod HTTP_GET or HTTP_POST
	 * 
	 * @return HttpURLConnection the new HttpURLConnection object
	 * 
	 * @throws MalFormedURLException
	 *             when the url is not and http url or otherwise malformed
	 * @throws IOException
	 *             when there was an i/o error
	 * 
	 * @see HttpConnectionFactory#getUserAgent()
	 * @see HttpURLConnection
	 * @see MalformedURLException
	 */
	public static HttpURLConnection createURLConnection(final String urlString, final String urlMethod)
			throws MalformedURLException, IOException {
		if (urlString == null || urlString.isEmpty() || urlMethod == null || urlMethod.isEmpty()) {
			return null;
		}

		final URL url;
		final URLConnection urlConnection;
		final HttpURLConnection httpUrlConnection;

		url = new URL(urlString);
		urlConnection = url.openConnection();

		if (!(urlConnection instanceof HttpURLConnection)) {
			throw new MalformedURLException(ERROR_INCORRECT_CONNECTION_TYPE);
		}

		httpUrlConnection = (HttpURLConnection) urlConnection;
		httpUrlConnection.setRequestMethod(urlMethod);
		httpUrlConnection.setRequestProperty(HTTP_REQUEST_USER_AGENT, getUserAgent());
		httpUrlConnection.setDefaultUseCaches(false);
		httpUrlConnection.setUseCaches(false);

		return httpUrlConnection;
	}

	/**
	 * creates a connected HttpURLConnection object for the given url
	 * 
	 * this method creates a new HttpURLConnection object for HTTP GET requests
	 * with the given url and calls the connect() method.
	 * 
	 * @param String
	 *            url the url string
	 * 
	 * @return URLConnection a connected HttpURLConnection object
	 * 
	 * @throws MalFormedURLException
	 *             when the url is not and http url or otherwise malformed
	 * @throws IOException
	 *             when there was an i/o error
	 * 
	 * @see HttpConnectionFactory#createURLConnection(String, String)
	 */
	public static HttpURLConnection openURLConnection(final String url) throws MalformedURLException, IOException {
		if (url == null || url.isEmpty()) {
			return null;
		}

		final HttpURLConnection urlConnection;

		urlConnection = createURLConnection(url, HTTP_METHOD_GET);
		urlConnection.connect();

		return urlConnection;
	}

	/**
	 * returns an InputStream object from a HttpURLConnection for the given url
	 * 
	 * creates a connected HttpURLConnection object for the given url and
	 * returns its InputStream object.
	 * 
	 * @param String
	 *            url the url string
	 * 
	 * @return InputStream the InputStream object from the HttpURLConnection
	 *         object
	 * 
	 * @throws MalFormedURLException
	 *             when the url is not and http url or otherwise malformed
	 * @throws IOException
	 *             when there was an i/o error
	 * 
	 * @see HttpConnectionFactory#openURLConnection(String)
	 */
	public static InputStream getInputStreamFromUrl(final String url) throws MalformedURLException, IOException {
		if (url == null || url.isEmpty()) {
			return null;
		}

		final HttpURLConnection urlConnection;
		final InputStream inputStream;

		urlConnection = openURLConnection(url);
		inputStream = urlConnection.getInputStream();

		return inputStream;
	}
}
