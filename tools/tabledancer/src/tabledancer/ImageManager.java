/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer;

import static tabledancer.Constants.CONFIG_URL_IMG_GET_BGIMAGE;
import static tabledancer.Constants.CONFIG_URL_IMG_GET_BLOCKIMAGE;

import java.awt.Image;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import tabledancer.flip.bean.IconList;
import tabledancer.flip.xml.XmlParserConstants;

/**
 * this class is responsible for image handling and caching
 * 
 * @author Mario 'Xchange' Steinhoff
 * 
 * @version 1.0
 * @since 1.1
 */
public class ImageManager {
	/**
	 * flag to identify an icon list with icons for enabled tables
	 */
	public static final int ICONS_TABLE_ENABLED = 1;

	/**
	 * flag to identify an icon list with icons for disabled tables
	 */
	public static final int ICONS_TABLE_DISABLED = 2;

	/**
	 * flag to identify an icon list with icons for chairs
	 */
	public static final int ICONS_CHAIR = 3;

	/**
	 * the TableDancer instance
	 * 
	 * this is currently not used
	 */
	@SuppressWarnings("unused")
	private final TableDancer tableDancer;

	/**
	 * image type map
	 */
	private final Map<Integer, String> map;

	/**
	 * cache for all downloaded images
	 */
	private final Map<String, Image> cache;

	/**
	 * the constructor
	 * 
	 * @param TableDancer
	 *            tableDancer the TableDancer instance
	 */
	public ImageManager(final TableDancer tableDancer) {
		this.tableDancer = tableDancer;
		this.cache = new HashMap<String, Image>();

		this.map = new HashMap<Integer, String>();
		this.map.put(ICONS_TABLE_ENABLED, XmlParserConstants.ELEMENT_TABLEGREEN);
		this.map.put(ICONS_TABLE_DISABLED, XmlParserConstants.ELEMENT_TABLEGREY);
		this.map.put(ICONS_CHAIR, XmlParserConstants.ELEMENT_CHAIR);
	}

	/**
	 * returns the xml name for the image flag
	 * 
	 * @param int flag the flag
	 * 
	 * @return String the name
	 */
	public String getTypeName(final int flag) {
		final String result;

		result = map.get(flag);

		return result;
	}

	/**
	 * convinience method, clears the current image cache
	 */
	public void clearCache() {
		cache.clear();
	}

	/**
	 * creates a new Image object from an url and cache it
	 * 
	 * if the given url is relative (does not start with http://), the string
	 * from getBaseUrl() will be prepended.
	 * 
	 * if the absolute url is found in the cache, the image object will be
	 * returned. if the absolute url is not found in the cache, an attempt will
	 * be made to download the image. on success, the image object will be
	 * stored in an internal cache and returned.
	 * 
	 * @param String
	 *            url the url string
	 * 
	 * @return Image the cached image object
	 * 
	 * @see ImageManager#cache
	 * @see TableDancer#getBaseUrl()
	 */
	public Image requestImageFromUrl(final String url) {
		if (url == null || url.isEmpty()) {
			return null;
		}

		final String cacheUrl;

		cacheUrl = HttpConnectionFactory.getBackendUrl(url);

		// cache hit
		if (cache.containsKey(cacheUrl)) {
			final Image image;

			image = cache.get(cacheUrl);

			return image;
		}

		// cache miss, try to download image
		try {
			final Image image;
			final InputStream inputStream;
			final BufferedInputStream bufferedInputStream;

			inputStream = HttpConnectionFactory.getInputStreamFromUrl(cacheUrl);

			bufferedInputStream = new BufferedInputStream(inputStream);

			/*
			 * read() always returns a BufferedImage object, but we set the
			 * return type to Image to be more abstract
			 */
			image = ImageIO.read(bufferedInputStream);

			/*
			 * if(image == null) { new Exception().printStackTrace(); }
			 */

			cache.put(cacheUrl, image);

			bufferedInputStream.close();
			inputStream.close();

			return image;
		} catch (MalformedURLException e) {
			TableDancer.printException(e);
			/*
			 * TODO fehlerpopup beim laden eines bilds ist ein fehler
			 * aufgetreten: eine url ist nicht korrekt + url string bitte
			 * korrgiere das in den settings blablabla
			 */
		} catch (IOException e) {
			TableDancer.printException(e);
			/*
			 * TODO fehlerpopup beim laden der daten ist ein I/O-fehler
			 * aufgetreten textarea "daten fuer entwickler" mit stacktrace
			 */
		}

		return null;
	}

	/**
	 * retrieves the overview background image from the backend system
	 * 
	 * @return Image the image object or null if no image was found
	 * 
	 * @see getImageFromUrl(String)
	 */
	public Image getBackgroundImageOverview() {
		final String url;
		final Image image;

		url = HttpConnectionFactory.getBackendUrl(CONFIG_URL_IMG_GET_BGIMAGE);

		image = requestImageFromUrl(url);

		return image;
	}

	/**
	 * retrieves the block-specific background image from the backend system
	 * 
	 * @param int blockId the block id
	 * 
	 * @return Image the image object or null if neither no blockid is given nor
	 *         an image was found
	 * 
	 * @see getImageFromUrl(String)
	 */
	public Image getBackgroundImageByBlockId(final int blockId) {
		if (blockId == 0) {
			return null;
		}

		final String url;
		final Image image;

		url = HttpConnectionFactory.getBackendUrl(CONFIG_URL_IMG_GET_BLOCKIMAGE, blockId);

		image = requestImageFromUrl(url);

		return image;
	}

	/**
	 * loads the images for all snap angles and returns the result as a map
	 * 
	 * @param List
	 *            <IconList> iconList the list with urls for all snap angles
	 * @param int iconType the requested type of icons
	 * 
	 * @see ImageManager#ICONS_TABLE_ENABLED
	 * @see ImageManager#ICONS_TABLE_DISABLED
	 * @see ImageManager#ICONS_CHAIR
	 * 
	 * @return Map<Integer, Image> the image objects with their associated angle
	 */
	public Map<Integer, Image> getImagesFromIconList(final List<IconList> iconList, final int iconType) {
		final Map<Integer, Image> result;

		result = new HashMap<Integer, Image>();

		if (iconList == null || !map.containsKey(iconType)) {
			return result;
		}

		final String mappingName;

		mappingName = getTypeName(iconType);

		for (final IconList icon : iconList) {
			if (!mappingName.equals(icon.getType())) {
				continue;
			}

			final Map<Integer, String> iconUrls;

			// null check isn't nessecary at this point as the
			// parser always sets the list to a (empty) map
			iconUrls = icon.getList();

			for (final Integer angle : iconUrls.keySet()) {
				final Image image;

				image = requestImageFromUrl(iconUrls.get(angle));

				result.put(angle, image);
			}
		}

		return result;
	}
}
