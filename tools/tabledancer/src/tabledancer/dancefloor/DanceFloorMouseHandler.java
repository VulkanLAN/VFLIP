/**
 * @version $Id: $
 * @copyright 2001-2005 The FLIP Project Team? 2001-2004 The FLIP Project Team?
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 **/

package tabledancer.dancefloor;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JPopupMenu;

/**
 * MouseHandler-Klasse fuer alle Mausevents
 */
public class DanceFloorMouseHandler implements MouseMotionListener {
	private Point dragPoint = null;

	private int mouseX = 10, mouseY = 10;

	private boolean makieren = false;

	private DanceFloor panel;

	private JPopupMenu dancePopUp;

	private Point dragStart = null;

	/**
	 * Konstruktor
	 * 
	 * @param panel
	 */
	public DanceFloorMouseHandler(DanceFloor panel) {
		this.panel = panel;
		DanceFloorMenu menu = new DanceFloorMenu(panel);
		dancePopUp = menu.setUp();
	}

	/**
	 * wird bei jeder Mausbewegung aufgerufen
	 * 
	 * @param event
	 */
	public void mouseMoved(MouseEvent event) {
		mouseX = event.getX();
		mouseY = event.getY();
		event.consume();
	}

	/**
	 * Makieren von Tischen
	 * 
	 * @param event
	 */
	public void mouseDragged(MouseEvent event) {
		mouseX = event.getX();
		mouseY = event.getY();
		dragPoint = new Point(mouseX, mouseY);

		if (makieren) {
			panel.selectRectChairs();
		}
	}

	/**
	 * alle Mausevents, delegation vom Panel
	 * 
	 * Beinhaltet bewegen der Maus innerhalb des Applets,
	 * 
	 * @param event
	 */
	public void processMouseEvent(MouseEvent event) {
		if (event.isPopupTrigger()) {
			/* PopUp */
			System.out.println("DanceFloorPanel.processMouseEvent()->trigger");
			dancePopUp.show(event.getComponent(), event.getX(), event.getY());
			return;
		} else if (event.getID() == MouseEvent.MOUSE_PRESSED && (event.getModifiers() & InputEvent.BUTTON1_MASK) != 0) {
			/* ziehen */
			System.out.println("DanceFloorPanel.processMouseEvent()->ziehen");
			panel.setSelectedAll(false);
			panel.requestFocus();

			Point p = new Point(event.getX(), event.getY());
			setDragPoint(p);
			dragStart = p;
			setMakieren(true);
		} else if (event.getID() == MouseEvent.MOUSE_RELEASED && (event.getModifiers() & InputEvent.BUTTON1_MASK) != 0) {
			/* loslassen */
			System.out.println("DanceFloorPanel.processMouseEvent()-> loslassen");
			setMakieren(false);
			panel.selectRectChairs();

			panel.repaint();
		}
	}

	/**
	 * Berechnung des Rects bei Makierung
	 * 
	 * @return
	 */
	public Rectangle getSlopeRectangle() {
		int x1, y1, x2, y2, xdiff, ydiff;
		x1 = Math.min(dragStart.x, dragPoint.x);
		x2 = Math.max(dragStart.x, dragPoint.x);
		y1 = Math.min(dragStart.y, dragPoint.y);
		y2 = Math.max(dragStart.y, dragPoint.y);
		xdiff = x2 - x1;
		ydiff = y2 - y1;
		return new Rectangle(x1, y1, xdiff, ydiff);
	}

	/**
	 * Setzen des DragPoints
	 * 
	 * @param dragPoint
	 */
	public void setDragPoint(Point dragPoint) {
		this.dragPoint = dragPoint;
	}

	/**
	 * liefert die X-Position der Maus
	 * 
	 * @return
	 */
	public int getMouseX() {
		return mouseX;
	}

	/**
	 * Setzt die X-Position der Maus
	 * 
	 * @param mouseX
	 */
	public void setMouseX(int mouseX) {
		this.mouseX = mouseX;
	}

	/**
	 * liefert die Y-Position der Maus
	 * 
	 * @return
	 */
	public int getMouseY() {
		return mouseY;
	}

	/**
	 * Setzt die Y-Position der Maus
	 * 
	 * @param mouseY
	 */
	public void setMouseY(int mouseY) {
		this.mouseY = mouseY;
	}

	/**
	 * Makieren an?
	 * 
	 * @return
	 */
	public boolean isMakieren() {
		return makieren;
	}

	/**
	 * Setzen des Makieren-FLAGS
	 * 
	 * @param makieren
	 */
	public void setMakieren(boolean makieren) {
		this.makieren = makieren;
	}

}
