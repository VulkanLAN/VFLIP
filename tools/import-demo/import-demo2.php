<?php

/*********************************************************************************************/
/*                                                                                           */
/*                           FLIP Datenbank-Import-Example                                   */
/*               (um Userprofil, Anmeldestatus und Sitzplätze zu importieren)                */
/*                                                                                           */
/*********************************************************************************************/



/***************************************** Config ********************************************/

// die Bilbiotheken des FLIPs werden gebraucht, um die Daten in die DB zu schreiben.
$cfg["flipdir"] = "./../../web/";

// das Tool sql-file kann vor jedem Import eine saubere Datenbank einspielen.
$cfg["sql-file"] = "./../sql-file/exec-sql-file.php";

// Zugangsdaten zu der Quell-Datenbank, aus welcher die Daten importiert werden sollen.
$cfg["db_server"] = "localhost";
$cfg["db_user"]   = "root";
$cfg["db_pass"]   = "";

// eine saubere Datenbank einspielen.
ExecSqlFile($cfg);

// hack, um Cookies und Sessions im FLIP zu deaktivieren (wer braucht die schon in der Konsole?)
$_COOKIE["disable-flip-cookie"] = "x";
$DisableSubjectCaching = true;




/***************************************** Programmablauf **************************************/

// wenn mit den Bibliotheken des FLIPs gearbeitet wird, muss das Arbeitsverzeichniss immer das Wurzelverzeichnis des FLIPs sein.
chdir($cfg["flipdir"]);
require_once("core/core.php");

function MainPageExists() {return 0;}
include_once("forum.php");


// Verbindung zur Quell-Datenbank aufnehmen.
$db = mysql_connect($cfg["db_server"],$cfg["db_user"],$cfg["db_pass"]);
if(!$db) die("kann nicht mit db-server verbinden.\n");
if(!mysql_select_db($cfg["db_db"])) 

// User importieren.

$ForumMapping = array(
  "pwd"       => array("col" => "password"),
  "icq"       => array("col" => "icq"),
  "lastlogin" => array("col" => "last_login_time"),
  "joined"    => array("col" => "registration_time"),
  "homepage"  => array("col" => "homepage"),
  "gender"    => array("col" => "sex",           "func" => "sex"),
  "birthday"  => array("col" => "birthday",       "func" => "strtotime"),
  "aimid"     => array("col" => "aim"),
  "yahooid"   => array("col" => "yahoo"),
  "msnid"     => array("col" => "msn"),
  "skype"     => array("col" => "skype_username"),
  "signatur"  => array("col" => "description",  "func" => "desc", "param" => "Signatur"),
  "info1"     => array("col" => "description",  "func" => "desc", "param" => "Info1"),
  "info2"     => array("col" => "description",  "func" => "desc", "param" => "Info2"),
  "info3"     => array("col" => "description",  "func" => "desc", "param" => "Info3"),
  "usertitel" => array("col" => "description",  "func" => "desc", "param" => "Titel"),
  "ircserver" => array("col" => "description",  "func" => "desc", "param" => "IRC-Server"),
  "ircchan"   => array("col" => "description",  "func" => "desc", "param" => "IRC-Channel")
);
     
//     name email
$LansurferMapping = array(
  "clan"            => array("col" => "clan"),
  "pwd"             => array("col" => "password", "func" => "noover"),
  "realname1"       => array("col" => "givenname"),
  "realname2"       => array("col" => "familyname"),
  "homepage"        => array("col" => "homepage"),
  "hometown"        => array("col" => "city"),
  "birthyear"       => array("col" => "birthday", "func" => "birthyear"),
  "infotext"        => array("col" => "description",  "func" => "desc", "param" => "Infotext"),
  "forum_signature" => array("col" => "description",  "func" => "desc", "param" => "Signatur"),
  "wwclid"          => array("col" => "tournament_wwcl_player_id"),
  "wwclclanid"      => array("col" => "tournament_wwcl_clan_id"),  
);


importUserTable("tsh_forum","SELECT * FROM `pfuser`;",$ForumMapping,"nickname","email","UserIDMapping");
importUserTable("lansurfer_tsh_io","SELECT * FROM `user`;",$LansurferMapping,"name","email");
importUserTable("lansurfer_tsh_sn","SELECT * FROM `user`;",$LansurferMapping,"name","email");
importUserTable("lansurfer_tsh_lan","SELECT * FROM `user`;",$LansurferMapping,"name","email");
importUserTable("lansurfer_tsh_sh4","SELECT * FROM `user`;",$LansurferMapping,"name","email");
importUserTable("lansurfer_tsh_sh5","SELECT * FROM `user`;",$LansurferMapping,"name","email");
importUserTable("lansurfer_tsh_sh6","SELECT * FROM `user`;",$LansurferMapping,"name","email");
importUserTable("lansurfer_tsh_sh7","SELECT * FROM `user`;",$LansurferMapping,"name","email");
importUserTable("lansurfer_tsh_sh8","SELECT * FROM `user`;",$LansurferMapping,"name","email");
importUserTable("lansurfer_tsh_sh9","SELECT * FROM `user`;",$LansurferMapping,"name","email");
importUserTable("lansurfer_tsh_sh10","SELECT * FROM `user`;",$LansurferMapping,"name","email");
importUserTable("lansurfer_tsh_sh11","SELECT * FROM `user`;",$LansurferMapping,"name","email");
importUserTable("lansurfer_tsh_sh12","SELECT * FROM `user`;",$LansurferMapping,"name","email");


importForen();

importNews();

// und Fertig.
echo "\ndone.\n";

// Da das FLIP einen Error-Hook einsetzt, werden Fehler nicht direkt ausgegebn, sondern müssen explizit "geflushed" werden.


/***************************************** User Importieren **************************************/

function imp_desc($new,$old,$param) {
  $old .= "$param: $new\n";
  return $old;
}

function imp_sex($new,$old,$param) {
  if($new == "weiblich") return "f";
  else return "m";
}

function imp_strtotime($new,$old,$param) {
  return strtotime($new);
}

function imp_birthyear($new,$old,$param) {
  if(empty($old)) $old = strtotime("$new-01-01 00:00");
  return $old;
}

function imp_noover($new,$old,$param) {
  if(empty($old)) $old = $new;
  return $old;
}

function getUserID($Name,$Email) {
  $Name = trim($Name);
  $Email = trim($Email);
  if(!CheckSubjectName($Name,false)) $Name = "@$Name";
  while(SubjectExists($Name)) $Name = "@$Name";
  if($id = GetSubjectID($Email)) return $id;
  return CreateSubject("user",$Name,$Email);
}

function convertData($Mapping,$from,$to) {
  foreach($Mapping as $col => $m) {
    $f = $from[$col];
    $t = $to[$m["col"]];
    if(!empty($f)) 
    {
      if(isset($m["func"])) 
      {
        $func = "imp_$m[func]";
        $t = $func($f,$t,$m["param"]);
      }
      else $t = $f;
      $to[$m["col"]] = $t;
    }
  }
  return $to;
}

function importUser($Mapping,$row,$NameCol,$EmailCol,$IDMappingVar) {
  $cols = array();
  foreach($Mapping as $m) $cols[$m["col"]] = 1;
  $cols = array_keys($cols);

  $id = getUserID($row[$NameCol],$row[$EmailCol]);
  if(empty($id)) 
  { 
    echo "X"; 
    return;
  }
  $u = createSubjectInstance($id);
  $to = $u->getProperties($cols);
  
  $data = convertData($Mapping,$row,$to);
  $data["enabled"] = "Y";
  $u->setProperties($data);
  if(!$u->isChildOf("registered")) $u->addParent("registered");  
  if(!is_null($IDMappingVar)){
     eval("global \$$IDMappingVar;");
     ${$IDMappingVar}[$row["id"]] = $u->id;
     ${$IDMappingVar}[$row[$NameCol]] = $u->id;
  }   
}

function importUserTable($DB,$Query,$Mapping,$NameCol,$EmailCol,$IDMappingVar = null) {
  echo "\nimportiere user aus $DB:";
  if(!mysql_select_db($DB)) die("kann db $cfg[db_db] nicht auswaehlen.");
  $r = mysql_query($Query);
  while($row = mysql_fetch_assoc($r)) {
    importUser($Mapping,$row,$NameCol,$EmailCol,$IDMappingVar);
    echo ".";  
  }
}

function ExecSqlFile($cfg)
{
  chdir(dirname($cfg["sql-file"]));
  require_once($cfg["sql-file"]);
}

//------------------------------ forum importieren ---------------------------

$ForumIDMapping = array();
$ThreadIDMapping = array();

function importForen() 
{
  global $db,$UserIDMapping;
  mysql_select_db("tsh_forum",$db);
  importBoards();
  importThreads();
  importPosts();
  echo "\nzaehle beitraege...\n";
  $f = new ForumPage();
  $f->actionCountPosts();
}

function getUserByM($id,$name) {
  global $UserIDMapping;
  if(isset($UserIDMapping[$id]) and !empty($id)) return $UserIDMapping[$id];
  if(isset($UserIDMapping[$name])) return $UserIDMapping[$name];
  return 1;
}

function importBoards() 
{
  global $ForumIDMapping,$db;
  $q = mysql_query("SELECT * FROM `pfboard`;",$db);
  if(!$q) echo mysql_error();
  while($f = mysql_fetch_assoc($q)) 
  {
    $subj = CreateSubject("forum",$f["name"]);
    $ForumIDMapping[$f["id"]] = $subj->id;
    $dat = array();
    $dat["group_id"] = 3;
    $dat["description"] = $f["info"];
    $dat["sort_index"] = time()+$f["id"];
    $subj->setProperties($dat);    
    $subj->addParent("forums_internal");
    echo "b";
  }  
}

function importThreads()
{
  global $ThreadIDMapping,$ForumIDMapping,$db;
  $q = mysql_query("SELECT * FROM `pfthread`;",$db);
  if(!$q) echo mysql_error();
  while($f = mysql_fetch_assoc($q))  
  {
    $dat = array();
    $dat["forum_id"] = $ForumIDMapping[$f["boardid"]];
    $dat["poster_id"] = getUserByM($f["userid"],$f["name"]);
    $dat["title"] = $f["titel"];
    if($f["status"] == "close") $dat["post_right"] = "-1";
    $ThreadIDMapping[$f["id"]] = MysqlWriteByID("flip_forum_threads",$dat);
    echo "t";
  }
}

function importPosts()
{
  global $ThreadIDMapping,$ForumIDMapping,$db;
  $q = mysql_query("SELECT * FROM `pfpost`;",$db);
  if(!$q) echo mysql_error();
  $i = 0;
  while($f = mysql_fetch_assoc($q))  
  {
    $dat = array();
    $dat["thread_id"] = $ThreadIDMapping[$f["threadid"]];
    $dat["poster_id"] = getUserByM($f["userid"],$f["name"]);
    $dat["post_time"] = $f["time"];
    $dat["text"] = $f["post"];
    $dat["allow_nl2br"] = "1";
    MysqlWriteByID("flip_forum_posts",$dat);
    if(($i++ % 100) == 0) echo "p";
  } 
}

function getNewsCat($Cat) {
	$cats = MysqlReadCol("SELECT e.key FROM flip_table_entry e, flip_table_tables t WHERE t.name='news_category' AND t.id=e.table_id","key");
	$cat = strtolower($Cat);
	
	if(in_array($cat,$cats)) return $cat;
	
	$id = MysqlReadField("SELECT `id` FROM flip_table_tables WHERE name='news_category'");
	MysqlWriteByID("flip_table_entry",array("table_id"     => $id,
                                              "key"          => $cat,
                                              "value"        => $Cat,
                                              "display"      => $Cat,
                                              "display_code" => $Cat
                                              ));
	return $cat;
}

function importNews() {
	global $db;
	mysql_select_db("tsh_news",$db);
	$q = mysql_query("SELECT * FROM new_news;");
	$i = 0;
	while($r = mysql_fetch_assoc($q)) {
		$dat = array();
		$dat["author"] = $r["autor"];
		$dat["title"] = $r["topic"];
		$dat["category"] = getNewsCat($r["cat"]);
		$dat["date"] = strtotime($r["datum"]);
		$dat["text"] = $r["news"];
		MysqlWriteByID("flip_news_news",$dat);
		echo "n";
		$i++;
	}
	echo "\n$i news importiert\n";
}

?>