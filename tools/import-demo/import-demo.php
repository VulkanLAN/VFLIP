<?php

/*********************************************************************************************/
/*                                                                                           */
/*                           FLIP Datenbank-Import-Example                                   */
/*               (um Userprofil, Anmeldestatus und Sitzplätze zu importieren)                */
/*                                                                                           */
/*********************************************************************************************/



/***************************************** Config ********************************************/

// die Bilbiotheken des FLIPs werden gebraucht, um die Daten in die DB zu schreiben.
$cfg["flipdir"] = "./../../web/";

// das Tool sql-file kann vor jedem Import eine saubere Datenbank einspielen.
$cfg["sql-file"] = "./../tools/sql-file/exec-sql-file.php";

// Zugangsdaten zu der Quell-Datenbank, aus welcher die Daten importiert werden sollen.
$cfg["db_server"] = "localhost";
$cfg["db_db"]     = "my_lanparty";
$cfg["db_user"]   = "root";
$cfg["db_pass"]   = "";

// eine saubere Datenbank einspielen.
// ExecSqlFile($cfg);

// hack, um Cookies und Sessions im FLIP zu deaktivieren (wer braucht die schon in der Konsole?)
$_COOKIE["disable-flip-cookie"] = "x";




/***************************************** Programmablauf **************************************/

// wenn mit den Bibliotheken des FLIPs gearbeitet wird, muss das Arbeitsverzeichniss immer das Wurzelverzeichnis des FLIPs sein.
chdir($cfg["flipdir"]);
require_once("core/core.php");

// Verbindung zur Quell-Datenbank aufnehmen.
$db = mysql_connect($cfg["db_server"],$cfg["db_user"],$cfg["db_pass"]);
if(!$db) die("kann nicht mit db-server verbinden.\n");
if(!mysql_select_db($cfg["db_db"])) die("kann db $cfg[db_db] nicht auswaehlen.");

// User importieren.
ImportUsers();

// Sitzplan importieren.
ImportSeats();

// und Fertig.
echo "\ndone.\n";

// Da das FLIP einen Error-Hook einsetzt, werden Fehler nicht direkt ausgegebn, sondern müssen explizit "geflushed" werden.
DisplayErrors();



/***************************************** User Importieren **************************************/

function ImportUsers()
{
  echo "adding users...";
  include_once("mod/mod.user.php");
  
  // Zuerst werden die zusätzlich beötigten Datenfelder im User-Profil "Columns" erstellt. 
  // Diese lassen sich auch über Admin->Columns manuell bearbeiten.
  // Das welche Werte das Feld "val_type" annehmen darf findet sich unter http://doc.flipdev.org/tpl/_Cmd_Input.html 
  // oder inc/inc.form.php im FLIP-Source
  // "sort_index" ist ein beliebiger Wert, er muss nur in jedem Datensatz anders sein, da nach ihm sortiert wird.
  $edit = GetRightID("edit_own_profile");
  $view = GetRightID("view_personal_informations");
  $a = array("type" => "user", "access" => "data", "required_view_right" => $view, "required_edit_right" => $edit);
  MysqlWriteByID("flip_user_column",array_merge($a,array("name" => "bank_name",      "val_type" => "string",  "caption" => "Kreditinstitut","hint" => "", "sort_index" => 1234555)));
  MysqlWriteByID("flip_user_column",array_merge($a,array("name" => "bank_blz",       "val_type" => "integer", "caption" => "Bankleitzahl",  "hint" => "", "sort_index" => 1234556)));
  MysqlWriteByID("flip_user_column",array_merge($a,array("name" => "bank_knr",       "val_type" => "integer", "caption" => "Kontonummer",   "hint" => "", "sort_index" => 1234557)));
  MysqlWriteByID("flip_user_column",array_merge($a,array("name" => "bank_owner",     "val_type" => "string",  "caption" => "Kontoinhaber",  "hint" => "", "sort_index" => 1234558)));
  MysqlWriteByID("flip_user_column",array_merge($a,array("name" => "myparty_barcode","val_type" => "string",  "caption" => "NfragBarcode",  "hint" => "", "sort_index" => 1234559, "required_edit_right" => GetRightID("edit_internal_profile"))));
  // (...)
  
  
  // User-Datensätze aus der Quell-DB auslesen
  $q = mysql_query("SELECT * FROM `my_user_table`;");
  if(!$q) echo mysql_error();
  
  while($i = mysql_fetch_assoc($q))
  {
    echo "."; // Statusanzeige.
    
    // User-Datensatz erstellen (in `flip_user_subject`) und User-Objekt instanziieren.
    $u = CreateSubject("user",$i["username"],$i["user_email"]);
    /*
    * An der obigen Stelle werden vermutlich Fehler auftreten, weil...
    * - Nicknames unique sein müssen. (Jeder Nickname darf nur einmal im FLIP verwendet werden)
    * - Nicknames nicht nur aus Ziffern bestehen dürfen.
    * - Emailadressen unique sein müssen.
    * - Emailadressen gültig sein müssen.
    * - Nicknames keine gültige Emailadresse sein dürfen.
    * 
    * Damit ist es meißt erforderlich, wenige Userdaten leicht zu modifizieren um sie ins 
    * FLIP übernehmen zu können.
    */
    
    // Wenn der User nicht erstellt werden konnte.
    if(!is_object($u))
    { 
      echo " $i[username] ";
      continue;
    }
    $u->setProperties(array(
      "enabled"    => "Y",                 // auf "Y", sonst kann der User sich nicht einloggen
      "password"   => $i["user_password"], // kann ein Hash vom typ MD5, CRC32, Crypt oder Mysql-PASSWORD() sein.
      "givenname"  => $i["vorname"],
      "familyname" => $i["nachname"],
      "street"     => $i["strasse"],
      "city"       => $i["ort"],      
      "plz"        => $i["plz"],
      "tele"       => $i["telefon"],
      "birthday"   => 0 /* timestamp */, 
      "bank_name"  => $i["bank"],
      //  (...)
    ));
    
    // Jeder User _muss_ in der Gruppe registered sein, um mehr rechte als Anonymous zu haben.
    AddParent($u,"registered");
    
    if(false /* user_ist_orga */) 
    {
      AddParent($u,"orga");
      AddParent($u,"usermanager");
    }
    
    if(false /* user_hat_bezahlt */) UserSetStatus($u,"paid");
    elseif(false /* user_ist_zur_lan_angemeldet */) UserSetStatus($u,"registered");
  }
}

function AddParent($u,$parent)
{
  if(!$u->isChildOf($parent)) $u->addParent($parent);
}



/***************************************** Sitzplätze Importieren **************************************/

function ImportSeats()
{
  /* Jetzt geht es darum, in der Tabelle `flip_seats_seats` für jeden Sitzplatz einen Datensatz 
  * zu erstellen. Der Sitzplan besteht aus einem Hintergrundbild (Admin->Content->system->baseplan)
  * auf welches serverseitig Sitzplatz-Icons gezeichnet werden. Ihre Position wird über Koordinaten 
  * in Pixeln und einem Winkel in Grad in `flip_seats_seats` gespeichert.
  */
  
  echo "importiere sitzplan...";
  MysqlWrite("DELETE FROM `flip_seats_seats`;");

  foreach($x as $y /* sitzplatz */)
  {
    // (...)
    // hier sollten die Koordinaten des Sitzplatzes berechnet werden, i know, das kann tricky sein ;)
  
    $dat = array(
      "enabled"  => "Y", // "Y", damit der Sitzplatz reserviert werden kann.
      "center_x" => $x,  // Abstand des Mitelpunktes des Sitzplatzes in Pixeln zum linken Rand des Hintergrundbildes.
      "center_y" => $y,  // Abstand des Mitelpunktes des Sitzplatzes in Pixeln zum oberen Rand des Hintergrundbildes.
      "user_id"  => $user_id,  // die User-ID der Person, welche den Sitzplatz vorgemerkt oder reserviert hat.
      "reserved" => (false /**/) ? "Y" : "N", // "Y" für reserviert, "N" für vorgemerkt.
      "name"     => $seatname, // ein beliebiger String, welcher dem Sitzplatz einen Namen gibt. (z.B.: "Block 3 Platz B4")
      "ip"       => $ip,  // Die IP-Adresse, welche auf diesem Sitzplatz verwendet werden sollte.
      "angle"    => 90, // der Winkel des Sitzplatzes in Grad. Nicht vergessen: Im typischen Sitzblock sind die 
                        // gegenüberliegenden PLätze um 180 Grad gedreht zu einander!
    );
    MysqlWriteByID("flip_seats_seats",$dat);
  }

  // weitere Felder in `flip_seats_seats` müssen nicht geschrieben werden, denn sie werden beim Aufruf von RedrawSeats() 
  // automatisch generiert.
  echo "\ndrawing seats...\n";
  include("mod/mod.seats.php");
  RedrawSeats();
}

function ExecSqlFile($cfg)
{
  chdir(dirname($cfg["sql-file"]));
  require_once($cfg["sql-file"]);
}
?>