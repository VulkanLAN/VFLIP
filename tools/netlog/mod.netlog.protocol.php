<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: mod.netlog.protocol.php 1541 2009-11-02 01:52:17Z Scope $
 * @copyright � 2001-2004 The FLIP Project Team
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package netlog
 **/
 
class NetLogPacket
{
  var $Command   = "";
  var $Keys      = 0;  
  
  function Error($msg)
  {
    trigger_error($msg,E_USER_WARNING);
    return false;
  }  

  function parse($dat)
  {
    $lines = explode("\n",$dat);
    list($ident,$cmd) = explode(" ",rtrim(array_shift($lines)),2);
    if($ident != "netlog") return $this->Error("Das Paket ist nicht vom netlog-Protokoll. Sein Header ist ungültig. |Header:$ident");
    if(!preg_match("/[0-9a-z_]+/",$cmd)) return $this->Error("Der Netlog-Befehl enthält ungültige Zeichen.|cmd:$cmd");
    $this->Command = $cmd;
    $this->Keys = $keys = array();
    $key = "unknown";
    foreach($lines as $l)
    {
      if(empty($l)) break;
      if($l{0} == "\t") $keys[$key] .= substr($l,1); // die Zeile beginnt mit tab
      else
      {  // die Zeile ehthält einen neuen Key
        list($key,$val) = explode(":",$l,2);
        $keys[$key] = $val;
      }    
    }
    foreach($keys as $k => $v)
    {
      $key = trim($k);
      if(!preg_match("/[0-9a-z_]+/",$key)) return $this->Error("Ein Netlog-Key enthält ungültige Zeichen.|key:$key");
      $this->Keys[$key] = trim($v);
    }
    return true;
  }
  
  function build()
  {
    if(!preg_match("/[0-9a-z_]+/",$this->Command)) return $this->Error("Der Netlog-Befehl enthält ungültige Zeichen.|cmd:{$this->Command}");
    $r = "netlog {$this->Command}\n";
    foreach($this->Keys as $k => $v)
    {
      if(!preg_match("/[0-9a-z_]+/",$k)) return $this->Error("Ein Netlog-Key enthält ungültige Zeichen.|key:$k");
      $r .= trim($k).": ".trim(chunk_split(addcslashes($v,"\r\n"),76,"\n\t"))."\n";
    }
    return "$r\n";
  }

  function buildHTTP($URL)
  {   
    $dat = $this->build();    
    $u = parse_url($URL);
    $dir = (empty($u["query"])) ? $u["path"] : "$u[path]?$u[query]";
    $r  = "POST $dir HTTP/1.0\n";
    $r .= "User-Agent: Netlog-Client 0.1\n";
    $r .= "Cache-Control: no-cache\n";
    $r .= "Content-Length: ".strlen($dat)."\n";
    $r .= "Content-Type: application/x-netlog\n";
    $r .= "Accept: */*\n";
    $r .= "Host: $u[host]\n";
    $r .= "Cookie: disable-flip-cookie=1;\n";
    $r .= "\n";
    $r .= $dat;
    return $r;
  }
  
  function _socketPutGetContents($Address,$Data,$DefaultPort = 80,$Timeout = 30)
  {
    list($host,$port) = array_pad(explode(":",$Address,2),2,$DefaultPort);
    $errno = 0; $errstr = "";
    $f = fsockopen($host,$port,$errno,$errstr,$Timeout);
    if(!$f) return $this->Error("Eine Socketverbindung konnte nicht hergestellt werden.|Host:$host:$port Err: $errno:$errstr");
    if(!fwrite($f,$Data))
    {
      fclose($f);
      return $this->Error("In eine Socketverbindung konnte nicht geschrieben werden.|Host:$host:$port Err: $errno:$errstr");
    }
    $r = "";
    while(!feof($f)) $r .= fread($f,4096);
    fclose($f);
    return $r;
  }

  
  function send($Addr,$TimeOut = 30)
  {
    $dat = $this->build();
    $resp = $this->_socketPutGetContents($Addr,$dat,17371,$TimeOut);
    if(!$resp) return false;
    $r = new NetLogPacket();
    $r->parse($resp);
    return $r;
  }
  
  function sendHTTP($URL)
  {
    $u = parse_url($URL);
    $adr = (empty($u["port"])) ? $u["host"] : "$u[host]:$u[port]";
    $dat = $this->buildHTTP($URL);
    $resp = $this->_socketPutGetContents($adr,$dat,80);
    
    $lines = explode("\n",$resp);
    do { $l = trim(array_shift($lines)); } while(!empty($l));
    
    $r = new NetLogPacket();
    $r->parse(trim(implode("\n",$lines)));
    return $r;
  }
  
  function handleResponse()
  {
    switch($this->Command)
    {
      case("ok"): 
        return true;
      case("error");
        trigger_error("Auf den netlog-Befehl wurde eine Fehler berichtet.|code:{$this->Keys[error_code]} message:{$this->Keys[error_message]}",E_USER_WARNING);
        return false;
      default: 
        trigger_error("Auf den netlog-Befehl wurde mit einem unbekannten Befehl geantwortet.|cmd: {$this->Command} keys:".var_export($this->Keys,true),E_USER_WARNING);
        return false;
    }  
  }
  
  function buildResponse()
  {
    if($err = GetErrors())
    {
      $e = array();
      foreach($err as $r) $e[] = $r->getMessage(true);
      $this->Command = "error";
      $this->Keys = array(
        "error_code" => 0,
        "error_message" => implode("\n ",$e)
      );      
    }
    else
    {
      $this->Command = "ok";
      $this->Keys = array();
    }
    return $this->build();
  }
}


?>