<?php

// ----------- Allgemeines ---------------
$cfg["tempdir"] = "";
$cfg["queuename"] = "netlog";


// ----------- Arpwatch -------------
$cfg["arpdat"] = "/var/arpwatch/eth0.dat";
$cfg["loopdelay"] = 20; // seconds

// Wenn ein Host länger als x Sekunden nicht mehr von Arpwatch bemerkt wurde, gilt er als offline.
$cfg["hosttimeout"] = 18 * 60;


// ------------ Lookup ---------------
//$cfg["dbserver"] = "localhost";
$cfg["dbname"] = "flip";
$cfg["dbuser"] = "root";
$cfg["dbpass"] = "";  

$cfg["flipurl"] = "http://127.0.0.1/flip/trunk/web/";

$cfg["dblog"] = "dblog.sql";


// ------------- Server ------------------
$cfg["lsport"] = 17371;
$cfg["lsip"] = "127.0.0.1";
$cfg["lsclients"] = array("127.0.0.1",&$cfg["lsip"]);



?>
