#!/usr/local/bin/php -q
<?php

require_once("inc.lockarray.php");
require_once("cfg.netlog.php");

Main();

function Main()
{
  global $cfg,$queue;
  ob_implicit_flush();
  set_time_limit(0);
  $queue = new LockArray($cfg["queuename"],$cfg["tempdir"]);
  doLog("Starte Endlosschleife, um $cfg[arpdat] alle $cfg[loopdelay] Sekunden auf Aenderungen zu ueberpruefen und diese in der Queue $cfg[tempdir]$cfg[queuename] abzulegen...");
  while(true) 
  {
    CheckArpWatch();
    sleep($cfg["loopdelay"]);
  }
//  SetStatus(array("ip" => "11.11.11.11"),"host_offline");
}

function doLog($Msg,$Die = false)
{
  echo date("m.d.y G:i:s")." $Msg\n";
  return !$Die;
}

function SetStatus($Items,$Action = "",$Description = "")
{
  global $Stats,$cfg,$queue;
  if(!empty($Action))      $Items["action"] = $Action;
  if(!empty($Description)) $Items["description"] = $Description;
  
  // statistics
  if(isset($Stats[$Action])) $Stats[$Action]++;
  else $Stats[$Action] = 1;
  
  if($Action == "host_offline") $Items["time"]++;

  // write data
  $queue->push($Items);
}

function ParseLines($Lines)
{
  $r = array();
  foreach($Lines as $l)
  {
    $i = array();
    list($i["mac"],$i["ipv4"],$i["time"],$i["hostname"]) = 
      array_pad(explode("\t",rtrim($l)),4,"");    
    $r[$i["mac"]] = $i;
  }
  return $r;
}

function CheckArpWatch()
{
  global $cfg,$Stats;
  $TempDat = $cfg["tempdir"]."arp.dat";
  
  // Die Datei $cfg["arpdat"] einlesen und parsen
  $Arp = @file($cfg["arpdat"]);
  if(!is_array($Arp)) return doLog("ERROR: die Datei $cfg[arpdat] konnte nicht gelesen werden.",true);
  $NewList = ParseLines($Arp);
  $NewTime = time();
  
  // Die Datei $TempDat einlesen und parsen
  $Old = @file($TempDat);
  if(!is_array($Old)) $OldList = array();
  else
  {
    $OldTime = rtrim(array_shift($Old));
    $OldList = ParseLines($Old);
  } 
  
  $Stats = array();
  // Die Datei $TempDat schreiben
  $f = @fopen($TempDat,"wb");
  if(!$f) return doLog("ERROR: Die Datei $TempDat konnte nicht zum schreiben geoeffnet werden.",true);
  if(!fwrite($f,"$NewTime\n".implode("",$Arp)))
    return doLog("ERROR: In die Datei $TempDat konnte nicht geschrieben werden.",true);
  fclose($f);
  
  // Den Diff zwischen $cfg["arpdat"] und $TempDat erstellen und entsprechende Logs in die DB schreiben.
  foreach($NewList as $mac => $ni)
  {
    $NewOnline = ($NewTime - $ni["time"] < $cfg["hosttimeout"]) ? true : false;
    if(!isset($OldList[$ni["mac"]]))
    {
      if($NewOnline) SetStatus($ni,"host_online","MAC erstmalig online");
      else SetStatus($ni,"host_offline","alte MAC gefunden");
    }
    else
    {
      $oi = $OldList[$ni["mac"]];
      unset($OldList[$ni["mac"]]);
      $OldOnline = ($OldTime - $oi["time"] < $cfg["hosttimeout"]) ? true : false;
      if($OldOnline and !$NewOnline) SetStatus($ni,"host_offline");
      if(!$OldOnline and $NewOnline) SetStatus($ni,"host_online");
      if($oi["ipv4"] != $ni["ipv4"]) SetStatus($ni,"host_ipchange","Alte IP: $oi[ipv4]");
      if($oi["hostname"] != $ni["hostname"]) SetStatus($ni,"host_namechange","Alter Hostname: $oi[hostname]");
    }
  }
  foreach($OldList as $oi) SetStatus($oi,"host_offline","Mac offline, da neuerdings unbekannt.");
  
  // Statusreport ausgeben:
  if(count($Stats) > 0)
  {
    $s = array();
    ksort($Stats);
    foreach($Stats as $k => $v) $s[] = "$k: $v";
    $s = implode(", ",$s);
  }
  else $s = "keine";
  doLog("CheckArp vollstaendig. Ausgegebene Meldungen: $s");
}


?>