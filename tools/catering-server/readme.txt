:: WICHTIG:


:: Dieses Verzeichnis enthält:

bin/  -  die Binarys für den Catering-Server

src/  -  die Delphisourcen für das Projekt




:: Allgemeines
Dieses ist ein erstes Testrelease welches nur das vorgegebene
Template parst und im internen Webbrowser zur Ansicht öffnet.
Es werden noch keinerlei Updatequeries innerhalb der Datenbank
vorgenommen. Nach der Ausführung findet man im tmp\ verzeichnis
die geparsten Rechnungen für den aktuellen Durchlauf zur Einsicht.

Zum Compilieren benötigt man ausßerdem die unit mysql.pas, die
unter http://www.fichtner.net/delphi/mysql.delphi.phtml zu finden ist.
Dort findet sich auch die zur Programmausführung benötigte libmysql.dll.

Im Cateringscript muss die Warteliste aktiviert sein, damit der
Server diese Bestellungen abarbeitet.