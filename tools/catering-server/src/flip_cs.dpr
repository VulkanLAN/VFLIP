program flip_cs;

uses
  Forms,
  main in 'main.pas' {main_form},
  config in 'config.pas' {config_form},
  debug in 'debug.pas' {debug_form},
  template in 'template.pas',
  ownmysql in 'ownmysql.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(Tmain_form, main_form);
  Application.CreateForm(Tconfig_form, config_form);
  Application.CreateForm(Tdebug_form, debug_form);
  Application.Run;
end.
