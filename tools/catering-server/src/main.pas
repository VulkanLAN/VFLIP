unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, template, inifiles, ExtCtrls, StdCtrls, StrUtils, ownmysql,
  OleCtrls, SHDocVw;

type
  Tmysql_config = record
    host: String;
    database: String;
    username: String;
    password: String;
    port: Integer;
  end;

  Tmain_form = class(TForm)
    MainMenu1: TMainMenu;
    Datei1: TMenuItem;
    Einstellungen1: TMenuItem;
    N1: TMenuItem;
    Beenden1: TMenuItem;
    N2: TMenuItem;
    t_interval: TTimer;
    Panel1: TPanel;
    status: TListBox;
    Panel2: TPanel;
    wb_preview: TWebBrowser;
    Button1: TButton;
    b_startstop: TButton;
    procedure Einstellungen1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure b_startstopClick(Sender: TObject);
    procedure t_intervalTimer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure WriteConfig();
    procedure Beenden1Click(Sender: TObject);
    procedure wb_previewDocumentComplete(Sender: TObject;
      const pDisp: IDispatch; var URL: OleVariant);
  private
    { Private declarations }
    mysql_con: TownMySQL;
    template_path: String;
    procedure CreateBill();
    procedure CreateBrowsePrint(page: TStringList);
    procedure status_message(text: String);
    function getRealname(subject_id: Integer): String;
    procedure ReadConfig();
  public
    { Public declarations }
    procedure debug(text: String);
  end;

var
  main_form: Tmain_form;
  config_mysql: Tmysql_config;
  debug_enable: Boolean;
  copy_count: Integer;
  counter: Integer; //durchzählen für ausgabe
  interval: Integer;
  template: Ttemplate;

implementation

uses config, debug;

{$R *.dfm}

function GetUniqueStr(): string;
var 
  TimeMS : comp;
begin
  TimeMS := TimeStampToMSecs(DateTimeToTimeStamp(now));
  Result := FormatFloat('0000000000000000', TimeMS )
            + FormatFloat('0000', int(10000*Random))
            + IntToHex(round(65535*Random),4)
            + FormatFloat('0000', int(10000*Random));
end;


// auslesen der Einstellungen
procedure Tmain_form.ReadConfig();
var  ini: TIniFile;
begin
  ini := TIniFile.Create(ExtractFileDir(Application.ExeName) + '\flip_cs.ini');
  with ini do begin
    config_mysql.host     := ReadString ('mysql', 'host', 'localhost');
    config_mysql.port     := ReadInteger('mysql', 'port', 3306);
    config_mysql.database := ReadString ('mysql', 'database', 'flip');
    config_mysql.username := ReadString ('mysql', 'username', 'root');
    config_mysql.password := ReadString ('mysql', 'password', '');
    template_path         := ReadString ('global', 'template_path', '\template\std.tpl');
    interval              := ReadInteger('global', 'interval', 10);
    debug_enable          := ReadBool   ('global', 'debug', false);
    copy_count            := ReadInteger('global', 'copy_count', 1);
    counter               := ReadInteger('global', 'counter', 0);
    Free;
  end;
   t_interval.Interval := interval * 1000;

end;

procedure Tmain_form.WriteConfig();
var  ini: TiniFile;
begin
  ini := TIniFile.Create(ExtractFileDir(Application.ExeName) + '\flip_cs.ini');
  with ini do begin
    WriteString ('mysql',  'host',          config_form.e_mysql_host.Text);
    WriteString ('mysql',  'username',      config_form.e_mysql_username.Text);
    WriteString ('mysql',  'database',      config_form.e_mysql_database.Text);
    WriteString ('mysql',  'password',      config_form.e_mysql_password.Text);
    WriteInteger('global', 'interval',      config_form.tb_interval.Position);
    WriteInteger('mysql',  'port',          StrToInt(config_form.e_mysql_port.Text));
    WriteString ('global', 'template_path', config_form.e_template_path.Text);
    WriteInteger('global', 'copy_count',    config_form.tb_print_count.Position);
    WriteInteger('global', 'counter',       counter);
  end;
  ReadConfig();
end;

procedure Tmain_form.debug(text: String);
begin
  if debug_enable then begin
    debug_form.memo.Lines.Add('(' + TimeToStr(Time()) + ')  ' + text);
  end;
end;

procedure Tmain_form.status_message(text: String);
begin
  status.Items.Add('(' + TimeToStr(Time()) + ')  ' + text);
  status.ItemIndex := status.Items.Count-1;
end;

// schreiben der aktuellen Einstellungen
procedure Tmain_form.Einstellungen1Click(Sender: TObject);
begin
  with config_form do begin
    e_mysql_host.Text       := config_mysql.host;
    e_mysql_port.Text       := IntToStr(config_mysql.port);
    e_mysql_username.Text   := config_mysql.username;
    e_mysql_password.Text   := config_mysql.password;
    e_mysql_database.Text   := config_mysql.database;
    e_template_path.Text    := template_path;
    tb_interval.Position    := interval;
    tb_print_count.Position := copy_count;
    l_interval.Caption      := VarToStr(interval) + ' Minute(n)';
    ShowModal;
  end;
end;

procedure Tmain_form.FormCreate(Sender: TObject);
begin
  //einlesen der konfiguration
  ReadConfig();
end;

procedure Tmain_form.FormShow(Sender: TObject);
begin
  //ist debug eingeschaltet
  if debug_enable then begin
    debug_form.Show;
  end;

  //versuchen datenbankverbindung aufzubauen
  with config_mysql do begin
    mysql_con := TownMySQL.Create(pChar(host), pChar(username), pChar(password), pChar(database), port);
    if (mysql_con.ConnectionEstablished()) then begin
      status_message('Datenbankverbindung erfolgreich');
    end else begin
      status_message('Datenbankverbindung fehlgeschlagen');
    end;
  end;
end;

procedure Tmain_form.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  mysql_con.Free;
end;

procedure Tmain_form.b_startstopClick(Sender: TObject);
begin
  with t_interval do begin
    if Enabled then begin
      Enabled := false;
      b_startstop.Caption := '&Start Timer';
      status_message('Timer gestoppt');
    end else begin
      Enabled := true;
      b_startstop.Caption := '&Stop Timer';
      status_message('Timer gestartet');
    end;
  end;
end;

procedure Tmain_form.t_intervalTimer(Sender: TObject);
begin
  status_message('Doing Timer event');
  CreateBill();
end;

procedure Tmain_form.Button1Click(Sender: TObject);
begin
  CreateBill();
end;

procedure Tmain_Form.CreateBill();
var query, lastnick: String;
    i: Integer;
    res: TownMySQLResult;
    t: Ttemplate;
begin
  //query für die abfrage basteln
  //p.s. lange string am stück zuweise will der compiler nich =D
  //p.p.s. zumindest nicht bei mir *g*
  
  query :=         'SELECT    us.id, ';
  query := query + '          us.name as nick, ';
  query := query + '          ca.titel, ';
  query := query + '          ca.preis, ';
  query := query + '          cb.anzahl  as anzahl, ';
  query := query + '          s.name as seat ';
  query := query + 'FROM      flip_catering_bestellungen cb ';
  query := query + 'LEFT JOIN flip_catering_artikel ca ON (ca.id = cb.artikel_id) ';
  query := query + 'LEFT JOIN flip_user_subject us ON (us.id = cb.subject_id) ';
  query := query + 'LEFT JOIN flip_seats s ON (s.user_id = us.id) ';
  query := query + 'WHERE     cb.status = ''warteliste'' ';
  //query := query + 'GROUP BY  cb.subject_id, ';
  //query := query + '          cb.artikel_id ';
  query := query + 'ORDER BY  us.name ';
  res := TownMySQLResult.Create(mysql_con.do_mysql_query(query));

  if mysql_con.querySuccessful then begin
    debug('query successful -> ' + query);
  end else begin
    debug('query fault -> ' + query);
    exit;
  end;
  if (res.getRowCount = 0) then begin //nix zu tun =D
    status_message('Nichts zu tun');
    res.Free();
    exit;
  end;

  t := Ttemplate.Create(ExtractFileDir(Application.ExeName) + '\' + template_path);

  lastnick := res.getField(0, 'nick');
  for i := 0 to res.getRowCount-1 do begin
    if lastnick <> res.getField(i, 'nick') then begin
      //neuer benutzer gefunden also mal ne rechnung ausgeben =D
      //kleine anmerkung: i muss hier mindestens schon 1 sein
      t.setUserdata(lastnick, getRealname(StrToInt(res.getField(i-1, 'id'))), res.getField(i-1, 'seat'), TimeToStr(Time()), counter);
      CreateBrowsePrint(t.parse());
      t.Free();
      //updatequery für die bestellung
      query :=         'UPDATE `flip_catering_bestellungen` SET `status` = ''weitergeleitet'' ';
      query := query + ' WHERE `status` = ''warteliste'' AND subject_id = ' + res.getField(i-1, 'id');
      status_message(VarToStr(mysql_con.do_mysql_update_query(query)) + ' Bestellungen von ' + res.getField(i-1, 'nick') + ' abgeschickt');
      if mysql_con.querySuccessful then begin
        debug('query successful -> ' + query);
      end else begin
        debug('query fault -> ' + query);
      end;


      //weiterlaufen =D
      t := Ttemplate.Create(ExtractFileDir(Application.ExeName) + '\' + template_path);
      lastnick := res.getField(i, 'nick');
    end;
    //artikel hinzufügen
    t.addItem(res.getField(i, 'titel'), StrToFloat(AnsiReplaceText(res.getField(i, 'preis'), '.', ',')), StrToInt(res.getField(i, 'anzahl')));

  end;
  //letzten benutzer nochmals ausgeben
  res.last();
  t.setUserdata(lastnick, getRealname(StrToInt(res.getField('id'))), res.getField('seat'), TimeToStr(Time()), counter);
  CreateBrowsePrint(t.parse());
  t.Free();
  //updatequery für die bestellung
  query :=         'UPDATE `flip_catering_bestellungen` SET `status` = ''weitergeleitet'' ';
  query := query + ' WHERE `status` = ''warteliste'' AND subject_id = ' + res.getField('id');
  status_message(VarToStr(mysql_con.do_mysql_update_query(query)) + ' Bestellungen von ' + res.getField('nick') + ' abgeschickt');
  if mysql_con.querySuccessful then begin
    debug('query successful -> ' + query);
  end else begin
    debug('query fault -> ' + query);
  end;

  res.Free();
end;

procedure Tmain_Form.CreateBrowsePrint(page: TStringList);
var  tmpdir, filename: String;
begin
  //existiert ein temporäres verzeichnis?
  tmpdir := ExtractFileDir(Application.ExeName) + '\tmp\';
  //tmp verzeichnis erstellen, wenn noch nicht vorhanden
  if DirectoryExists(tmpdir) = false then begin
    if CreateDir(tmpdir) then debug('Tempverzeichnis erfolgreich erstellt');
  end;
  //speichern der TStringliste und öffnen im browser
  filename := tmpdir + 'tmp.htm';
  page.SaveToFile(filename);
  wb_preview.Navigate(filename);
  
  //warten bis datei gedruckt wurde
  while (FileExists(filename)) do begin
    Application.ProcessMessages();
  end;
end;

function Tmain_Form.getRealname(subject_id: Integer): String;
var  query: String;
     res: TownMySQLResult;
begin
  result := '';
  //echten benutzernamen herausfinden
  query :=         'SELECT      ud1.val as vorname, ';
  query := query + '            ud2.val as nachname ';
  query := query + 'FROM        flip_user_column uc1, ';
  query := query + '            flip_user_column uc2 ';
  query := query + 'INNER JOIN  flip_user_data ud1 ON (ud1.column_id = uc1.id) ';
  query := query + 'INNER JOIN  flip_user_data ud2 ON (ud2.column_id = uc2.id) ';
  query := query + 'WHERE       uc1.name = ''givenname'' ';
  query := query + '        AND uc2.name = ''familyname'' ';
  query := query + '        AND ud1.subject_id = ' + VarToStr(subject_id);
  query := query + '        AND ud2.subject_id = ' + VarToStr(subject_id);
  res := TownMySQLResult.Create(mysql_con.do_mysql_query(query));
  if mysql_con.querySuccessful then begin
    debug('query successful -> ' + query);
  end else begin
    debug('query fault -> ' + query);
    exit;
  end;  
  if (res.getRowCount > 0) then begin
    result := res.getField(0, 0) + ' ' + res.getField(0, 1);
  end;
  res.Free();
end;

procedure Tmain_form.Beenden1Click(Sender: TObject);
begin
  Close;
end;

procedure Tmain_form.wb_previewDocumentComplete(Sender: TObject;
  const pDisp: IDispatch; var URL: OleVariant);
var
   tmpdir, filename: String;
   i: integer;
   ini: TIniFile;
begin
  //ausdrucken und temporäre datei löschen
  tmpdir := ExtractFileDir(Application.ExeName) + '\tmp\';
  filename := tmpdir + 'tmp.htm';
  for i := 1 to copy_count do begin
    wb_preview.ExecWB(OLECMDID_PRINT, OLECMDEXECOPT_DONTPROMPTUSER);
  end;
  status_message('Document Printed');
  //counter erhöhen
  inc(counter);
  ini := TIniFile.Create(ExtractFileDir(Application.ExeName) + '\flip_cs.ini');
  ini.WriteInteger('global', 'counter', counter);
  ini.Free;
  //tmp datei löschen
  DeleteFile(filename);

end;

end.
