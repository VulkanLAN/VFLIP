unit ownmysql;

interface

uses mysql, Classes;

type
  TownMySQL = class(TObject)
  private
    connection: PMYSQL;
    connection_established: Boolean;
    query_successful: Boolean;

  public
    constructor Create(host, username, password, database: pChar; port: Word);
    destructor Kill();
    procedure Disconnect();
    function ConnectionEstablished(): Boolean;
    function do_mysql_query(Query: String): PMYSQL_RES;
    function do_mysql_update_query(Query: String): Integer;
    function querySuccessful(): Boolean;
    procedure do_mysql_free_result(result: PMYSQL_RES);
  end;


  
  TownMySQLResult = class(TObject)
  private
    fieldcount: Integer;
    rowcount: Integer;
    pointer: Integer;
    fieldname: Array of String;
    rows: Array of Array of String;
    function getColIndex(col: String): Integer;
    
  public
    constructor Create(result: PMYSQL_RES);

    function next(): Boolean;
    function prev(): Boolean;
    procedure first();
    procedure last();
    function getRowCount(): Integer;
    function getColCount(): Integer;

    function getRow(): TStringList; overload;
    function getRow(row: Integer): TStringList; overload;

    function getCol(col: String): TStringList; overload;
    function getCol(col: Integer): TStringList; overload;

    function getField(col: String): String; overload;
    function getField(col: Integer): String; overload;
    function getField(row: Integer; col: String): String; overload;
    function getField(row: Integer; col: Integer): String; overload;
  end;



implementation

//mysql verbindung aufbauen
constructor TownMySQL.Create(host, username, password, database: pChar; port: Word);
var myComp: Cardinal;
begin
  myComp := 0;
  connection := mysql_init(nil);

  if (port = 0) then port := 3306;

  if (mysql_real_connect(connection, pChar(host), pChar(username), pChar(password), pChar(database), port, nil, MyComp) = nil) then begin
    //verbindung fehlgeschlagen
    connection_established := false;
  end else begin
    connection_established := true;
  end;
  mysql_select_db(connection, pChar(database));
end;

//verbindung schließen beim zerstören des objekts
destructor TownMySQL.Kill();
begin
  Disconnect();
end;

//verbindung schließen
procedure TownMySQL.Disconnect();
begin
  if (connection_established) then mysql_close(connection);
  connection_established := false;
end;

//ist eine verbindung hergestellt?
function TownMySQL.ConnectionEstablished(): Boolean;
begin
  result := connection_established;
end;


//mysql query ausführen
function TownMySQL.do_mysql_query(Query: String): PMYSQL_RES;
begin
 if mysql_query(connection, pChar(Query)) <> 0 then begin
   query_successful := false;
 end else begin
   query_successful := true;
 end;
 result := mysql_use_result(connection);
end;

function TownMySQL.querySuccessful(): Boolean;
begin
  result := query_successful;
end;

//mysql update query
function TownMySQL.do_mysql_update_query(Query: String): Integer;
begin
 if mysql_query(connection, pChar(Query)) <> 0 then begin
   query_successful := false;
 end else begin
   query_successful := true;
 end;
 result := mysql_affected_rows(connection);
end;

//result löschen
procedure TownMySQL.do_mysql_free_result(result: PMYSQL_RES);

begin
  mysql_free_result(result);
end;



//#############################################
//############# TownMySQLResult ###############
//#############################################

//constructor der klasse mit gleichzeitigem einlesen des resultsets
constructor TownMySQLResult.Create(result: PMYSQL_RES);
var i, j, itemindex: integer;
    fields: PMYSQL_FIELDS;
    row: PMYSQL_ROW;
    tmp: TStringList;
begin
  pointer := 0;
  //result einlesen und in interne variablen speichern
  //auslesen der feldnamen
  fields := result.fields;
  fieldcount := result.field_count;

  
  SetLength(fieldname, fieldcount); //array länge setzen
  for i := 0 to fieldcount-1 do begin
    fieldname[i] := fields[i].name;
  end;

  //wieviele rows haben wir denn
  //muss das über diesen kack machen, da mysql_num_rows erst dann was
  //exaktes ausgibt... is zwar real q&d, aber egal, wers besser weiß
  //soll bescheidsagen
  tmp := TStringList.Create();
  row := mysql_fetch_row(result);
  while row <> nil do begin
    for i := 0 to fieldcount-1 do begin
      tmp.Add(row[i]);
    end;
    row := mysql_fetch_row(result);
  end;

  rowcount := tmp.Count div fieldcount;  //anzahl der zeilen errechnen

  //umlagern auf lokale variable
  SetLength(rows, rowcount, fieldcount);
  for i := 0 to rowcount-1 do begin
    for j := 0 to fieldcount-1 do begin
      itemindex := (i * fieldcount) + j; //berechnen des aktuellen index
      rows[i][j] := tmp.Strings[itemindex];
    end;
  end;

  tmp.Free();
  mysql_free_result(result);
end;

//pointer auf erste reihe setzen
procedure TownMySQLResult.first();
begin
  pointer := 0;
end;

//pointer auf letzte reihe setzen
procedure TownMySQLResult.last();
begin
  pointer := rowcount-1;
end;


//nächste reihe auswählen (pointer um 1 erhöhen)
function TownMySQLResult.next(): Boolean;
begin
  inc(pointer);
  if (pointer < rowcount) then begin
    result := true;
  end else begin
    result := false;
    dec(pointer);
  end;
end;

//nächste reihe auswählen (pointer um 1 verringern)
function TownMySQLResult.prev(): Boolean;
begin
  dec(pointer);
  if (pointer > -1) then begin
    result := true;
  end else begin
    result := false;
    inc(pointer);
  end;
end;

//aktuelle zeile zurückgeben
function TownMySQLResult.getRow(row: Integer): TStringList;
var  i: Integer;
begin
  result := nil;
  if row < rowcount then begin
    for i := 0 to fieldcount-1 do begin
      result.Add(rows[row][i]);
    end;
  end;
end;

function TownMySQLResult.getRow(): TStringList;
begin
  result := getRow(pointer);
end;

//spalte zurückgeben
function TownMySQLResult.getCol(col: String): TStringList;
begin
  result := getCol(getColIndex(col));
end;

function TownMySQLResult.getCol(col: Integer): TStringList;
var  i: Integer;
begin
  result := nil;
  if col < fieldcount then begin
    for i := 0 to rowcount-1 do begin
      result.Add(rows[i][col]);
    end;
  end;
end;


//spaltenindex anhand des namens herausfinden
function TownMySQLResult.getColIndex(col: String): Integer;
var  i: Integer;
begin
  result := -1;
  for i := 0 to fieldcount-1 do begin
    if fieldname[i] = col then begin
      result := i;
      break;
    end;
  end;
end;

//einzelnes feld in der tabelle herausfinden
function TownMySQLResult.getField(row: Integer; col: String): String;
var  colindex: Integer;
begin
  result := '';
  //colindex finden
  colindex := getColIndex(col);
  //row muss innerhalb der grenzen sein, col <> -1
  if (colindex <> -1) and (row < rowcount) then begin
    result := rows[row][colindex];
  end;
end;

function TownMySQLResult.getField(row: Integer; col: Integer): String;
begin
  result := '';
  if (row < rowcount) and (col < fieldcount) then begin
    result := rows[row][col];
  end;
end;

function TownMySQLResult.getField(col: String): String;
var  colindex: Integer;
begin
  result := '';
  colindex := getColIndex(col);
  if colindex <> -1 then begin
    result := rows[pointer][colindex];
  end;
end;

function TownMySQLResult.getField(col: Integer): String;
begin
  result := '';
  if col < fieldcount then begin
    result := rows[pointer][col];
  end;
end;




function TownMySQLResult.getRowCount(): Integer;
begin
  result := rowcount;
end;

function TownMySQLResult.getColCount(): Integer;
begin
  result := fieldcount;
end;

end.

