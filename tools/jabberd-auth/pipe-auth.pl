#!/usr/bin/perl -w
# This script is for jabberd2-flip-pipe-authentification. Your have to change the parameter
# for realm und database to use it. Jabberd2 had to compile whith tha parameter:
# ./configure --enable-authreg=pipe
#
#

use strict;
use DBI;
use MIME::Base64;
use Digest::MD5  qw(md5 md5_hex md5_base64);

use vars qw($jabberrealm $user $password $database $hostname $port);
use vars qw($dsn $dbh);

####################################################
# The only and one realm
# please change to your realm
$jabberrealm = "localhost";

# database stuff
# please change attributes to flip-database
$user        = "root";
$password = "";
$database = "flip";
$hostname = "127.0.0.1";
$port         = "3306";
####################################################

# DB-Connection
$dsn      = "DBI:mysql:database=$database;host=$hostname;port=$port";
$dbh = DBI->connect($dsn, $user, $password);

# Flush output immediately.
$| = 1;

# On startup, we have to inform c2s of the functions we can deal with. USER-EXISTS is not optional.
#print "OK USER-EXISTS GET-PASSWORD CHECK-PASSWORD SET-PASSWORD GET-ZEROK SET-ZEROK CREATE-USER DESTROY-USER FREE\n";
print "OK USER-EXISTS CHECK-PASSWORD FREE\n";

# Our main loop
my $buf;
while(sysread (STDIN, $buf, 1024) > 0)
{
    my ($cmd, @args) = split ' ', $buf;
    $cmd =~ tr/[A-Z]/[a-z]/;
    $cmd =~ tr/-/_/;

    eval "print _cmd_$cmd(\@args), '\n'";
}

# Determine if the requested user exists.
sub _cmd_user_exists
{
    my ($user, $realm) = @_;
    
    if ($realm ne $jabberrealm)
    {
        return "NO";
    }
    
    my ($sql) = "SELECT * FROM `flip_user_subject` WHERE NAME='$user' AND TYPE='user'";
    my ($sth) = $dbh->prepare($sql);
    $sth->execute;
    if (my (@row) = $sth->fetchrow_array)
    {
        return "OK";
    }
    else
    {
        return "NO";
    }
    # !!! return "OK" if user exists;
}

# Retrieve the user's password.
sub _cmd_get_password
{
    my ($user, $realm) = @_;

    # !!! $pass = [password in database];
    #     return "NO" if not $pass;
    #     $encoded_pass = encode_base64($pass);
    #     return "OK $encoded_pass" if $encoded_pass;

    return "NO";
}

# Compare the given password with the stored password.
sub _cmd_check_password
{
    my ($user, $encoded_pass, $realm) = @_;
    if ($realm ne $jabberrealm)
    {
        return "NO REALM";
    }
    
    my ($hash) = md5_hex $encoded_pass;
    my ($sql) = "SELECT d.val FROM flip_user_column c, flip_user_subject s, flip_user_data d WHERE s.name='$user' AND s.type='user' AND c.name ='password' AND	d.subject_id = s.id AND d.column_id = c.id;";
    my ($sth) = $dbh->prepare($sql);
    $sth->execute;
    if (my (@row) = $sth->fetchrow_array)
    {
        if ($row[0] eq $encoded_pass)
	{
	    return "OK";
	}
	elsif ($row[0] eq $hash)
	{
	    return "OK";
	}
	else
	{
	    return "NO";
	}
	
   }
    return "NO";
    # !!! $pass = decode_base64($encoded_pass);
    #     return "NO" if not $pass;
    #     $spass = [password in database];
    #     return "OK" if $pass eq $spass;
}

# Store the password in the database.
sub _cmd_set_password
{
    my ($user, $encoded_pass, $realm) = @_;

    # !!! $pass = decode_base64($encoded_pass);
    #     return "NO" if not $pass;
    #     $fail = [store $pass in database];
    #     return "OK" if not $fail;

    return "NO";
}

# Retrieve the user's stored zerok data.
sub _cmd_get_zerok
{
    my ($user, $realm) = @_;

    # !!! $hash = [hash in database];
    #     $token = [token in database];
    #     $sequence = [sequence in database];
    #     return "OK $hash $token $sequence" if $hash and $token and $sequence;

    return "NO";
}

# Store the user's zerok data.
sub _cmd_set_zerok
{
    my ($user, $hash, $token, $sequence, $realm) = @_;

    # !!! $fail = [store $hash in database];
    #     $fail and $fail = [store $token in database];
    #     $fail and $fail = [store $sequence in database];
    #     return "OK" if not $fail;
    
    return "NO";
}

# Create a user in the database (with no auth credentials).
sub _cmd_create_user
{
    my ($user, $realm) = @_;

    # !!! $fail = [create user in database]
    #     return "OK" if not $fail;

    return "NO";
}

# Delete a user and associated credentials.
sub _cmd_delete_user
{
    my ($user, $realm) = @_;

    # !!! $fail = [delete user in database]
    #     return "OK" if not $fail;

    return "NO";
}

# c2s shutting down, do the same.
sub _cmd_free
{
    # !!! free data
    #     close database handles

    $dbh->disconnect;
    exit(0);
}
