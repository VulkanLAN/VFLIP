<?php
//config must be included
if(!isset($cfg["prefix"])) {
	trigger_error("Es wird eine Config ben&ouml;tigt in der der Tabellenpr&auml;fix angegeben ist!", E_USER_ERROR);
}


// testing|system|none|array("query1","query2")

function Subj($tbl)
{
  return "(($tbl.name IN ('test','Anonymous','System','dbexport')) OR ($tbl.type IN ('group','forum','type')))";
}

$TableContent = array(
//  "flip_archiv_articles" => "testing",
//  "flip_archiv_events" => "testing",
//  "flip_calender_events" => "testing",
  "flip_calender_types" => "system",
//  "flip_catering_artikel" => "testing",
  "flip_catering_gruppen" =>                                        "deleted",
//  "flip_catering_bestellungen" => "none",
//  "flip_catering_kategorien" => "testing",
// "flip_checkin_list" => "testing",
//  "flip_clan_join" => "testing",
  "flip_config" => "system",
  "flip_config_categories" => "system",
  "flip_config_values" => "system",
  "flip_content_groups" => "system",
  "flip_content_image" => "system",
  "flip_content_text" => "system",
  "flip_forum_groups" => "system",
//  "flip_forum_posts" => "testing",
//  "flip_forum_threads" => "testing",
//  "flip_gb_entry" => "testing",
  "flip_gb_icons" => "system",
//  "flip_konten_data" => "testing",
//  "flip_konten_list" => "testing",
  "flip_konto_data" =>                                              "deleted",
  "flip_konto_list" =>                                              "deleted",
  "flip_log" =>                                                     "deleted",
  "flip_log_log" => "none",
  "flip_lastvisit" => "none",
  "flip_menu" =>                                                    "deleted",
  "flip_menu_items" =>                                              "deleted",
  "flip_menu_links" => "system",
  "flip_menu_blocks" => "system",
  "flip_message" =>                                                 "deleted",
  "flip_messaging" =>                                               "deleted",
  "flip_messaging_webmessaging" =>                                  "deleted",
  "flip_messaging_messages" =>                                      "deleted",
  "flip_netlog" =>                                                  "deleted",
  "flip_netlog_log" => "none",
  "flip_netlog_status" => "none",
  "flip_news" =>                                                    "deleted",
//  "flip_news_comments" => "testing",
//  "flip_news_news" => "testing",
//  "flip_poll_list" => "testing",
//  "flip_poll_users" => "testing",
//  "flip_poll_votes" => "testing",
  "flip_seats" => "deleted",
 // "flip_seats_blocks" => "testing",
 // "flip_seats_seats" => "testing",
  "flip_sendmessage_message" => array("system|SELECT id FROM `".$cfg["prefix"]."flip_sendmessage_message` WHERE `type`='sys'", "testing|*"),
  "flip_session" =>                                                 "deleted",
  "flip_session_data" => "none",
  "flip_session_cache" => "none",
  "flip_sponsors" =>                                                "deleted",
//  "flip_sponsor_sponsors" => "testing",
  "flip_table" =>                                                   "deleted",
  "flip_table_entry" => "system",
  "flip_table_tables" => "system",
//  "flip_textdata" => "testing",
  "flip_ticket" =>                                                  "deleted",
//  "flip_ticket_event" => "testing",
//  "flip_ticket_tickets" => "testing",
//  "flip_tournament_combatant" => "testing",
  "flip_tournament_coins" => array("system|SELECT id FROM `".$cfg["prefix"]."flip_tournament_coins` ORDER BY id LIMIT 1", "testing|*"),
  "flip_tournament_groups" => array("system|SELECT id FROM `".$cfg["prefix"]."flip_tournament_groups` ORDER BY id LIMIT 1", "testing|*"),
//  "flip_tournament_invite" => "testing",
//  "flip_tournament_matches" => "testing",
//  "flip_tournament_ranking" => "testing",
//  "flip_tournament_servers" => "testing",
//  "flip_tournament_serverusage" => "testing",
//  "flip_tournament_tournaments" => "testing",
//  "flip_user_binary" => "testing",
  "flip_user_column" => "system",
  "flip_user_data" => array("system|SELECT d.id FROM `".$cfg["prefix"]."flip_user_data` d, `".$cfg["prefix"]."flip_user_subject` s WHERE ((s.id = d.subject_id) AND ".Subj("s").")", "testing|*"),
  "flip_user_groups" => array("system|SELECT g.id FROM `".$cfg["prefix"]."flip_user_groups` g, `".$cfg["prefix"]."flip_user_subject` s1, `".$cfg["prefix"]."flip_user_subject` s2 WHERE ((s1.id = g.child_id) AND (s2.id = g.parent_id) AND ".Subj("s1")." AND ".Subj("s2").")", "testing|*"),
  "flip_user_right" => "system",
  "flip_user_rights" => array("system|SELECT r.id FROM `".$cfg["prefix"]."flip_user_rights` r, `".$cfg["prefix"]."flip_user_subject` s1, `".$cfg["prefix"]."flip_user_subject` s2 WHERE ((s1.id = r.owner_id) AND (((s2.id = r.controled_id) AND ".Subj("s2").") OR (r.controled_id = 0))AND ".Subj("s1").")", "testing|*"),
  "flip_user_subject" => "system",
  "flip_user_type" =>                                               "deleted",
//  "flip_webmessage_message" => "testing",
  "flip_i18n_countrycodes" => "system"
);


?>
