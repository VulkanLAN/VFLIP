/* $Id: lookup.c 404 2004-08-25 21:20:48Z docfx $ */

/* Copyright (C) 2004 by Soeren Bleikertz <soeren@openfoo.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <netdb.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>

#include "lookup.h"
#include "ethcodes.h"
#include "error.h"

void *lookup(void *job)
{
    char *vendor = NULL, *host = NULL;
    struct hostent *resolv;
    
    /* eth-vendor lookup */
    if ((vendor = ec_lookup(((job_t*)job)->mac)) == NULL) {
	Perror("can not lookup vendor");
	vendor = "unknown";
    }
    
    
    /* PTR-lookup - IPv4 */
    if ((resolv = gethostbyaddr((char*)&(((job_t*)job)->ipv4), 
		    sizeof(struct in_addr), AF_INET)) == NULL) {
	Perror("can not lookup hostname");
    } else
        host = resolv->h_name;

    printf("DEBUG: host=%s, vendor=%s\n", host, vendor);

    if (vendor)
        free(vendor);

    return NULL;
}
