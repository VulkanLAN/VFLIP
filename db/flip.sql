# --------------------------- make-sql-file SQL Dump --------------------------- #
# $Id: make-sql-file.php 1390 2007-04-09 11:46:22Z loom $
# Erstellungszeit: 2010-09-17 01:09
# Type: System
# Dieser Dump enthaelt alle Daten und Tabellenstrukturen, die das FLIP zum laufen benoetigt.
# ------------------------------------------------------------------------------ #

# --------------------------- flip_archiv_articles --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_archiv_articles`;
CREATE TABLE `$prefix$flip_archiv_articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `event_id` int(11) NOT NULL DEFAULT '0',
  `group` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `caption` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `text` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin AUTO_INCREMENT=16;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_archiv_articles -------------------------- #


# --------------------------- flip_archiv_events --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_archiv_events`;
CREATE TABLE `$prefix$flip_archiv_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `caption` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `location` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `participant_group_id` int(11) DEFAULT NULL,
  `edit_right` int(11) NOT NULL DEFAULT '0',
  `view_right` int(11) NOT NULL DEFAULT '0',
  `time_start` int(11) NOT NULL DEFAULT '0',
  `time_end` int(11) NOT NULL DEFAULT '0',
  `text` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin AUTO_INCREMENT=34;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_archiv_events -------------------------- #


# --------------------------- /flip_archiv_tournament_coins -------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_archiv_tournament_coins`;
CREATE TABLE IF NOT EXISTS `$prefix$flip_archiv_tournament_coins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `maxcoins` mediumint(9) NOT NULL DEFAULT '0',
  `currency` varchar(20) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Coins',
  PRIMARY KEY (`id`),
  UNIQUE KEY `currency` (`currency`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin AUTO_INCREMENT=2 ;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_archiv_tournament_coins -------------------------- #


# --------------------------- /flip_archiv_tournament_combatant -------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_archiv_tournament_combatant`;
CREATE TABLE IF NOT EXISTS `$prefix$flip_archiv_tournament_combatant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `team_id` int(11) NOT NULL DEFAULT '0',
  `tournament_id` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `team_tournament` (`team_id`,`tournament_id`),
  KEY `i_tournamentid` (`tournament_id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin AUTO_INCREMENT=1178 ;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_archiv_tournament_combatant -------------------------- #


# --------------------------- /flip_archiv_tournament_groups -------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_archiv_tournament_groups`;
CREATE TABLE IF NOT EXISTS `$prefix$flip_archiv_tournament_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order` mediumint(9) NOT NULL DEFAULT '0',
  `name` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `image` blob,
  PRIMARY KEY (`id`),
  UNIQUE KEY `order` (`order`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin AUTO_INCREMENT=2 ;
# --------------------------- /flip_archiv_tournament_groups -------------------------- #


# --------------------------- /flip_archiv_tournament_invite -------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_archiv_tournament_invite`;
CREATE TABLE IF NOT EXISTS `$prefix$flip_archiv_tournament_invite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `from` enum('team','user') COLLATE utf8mb4_bin NOT NULL DEFAULT 'team',
  `tournaments_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `team_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userperteam` (`user_id`,`team_id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin AUTO_INCREMENT=2091 ;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_archiv_tournament_invite -------------------------- #


# --------------------------- /flip_archiv_tournament_matches -------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_archiv_tournament_matches`;
CREATE TABLE IF NOT EXISTS `$prefix$flip_archiv_tournament_matches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tournament_id` mediumint(9) NOT NULL DEFAULT '0',
  `team1` int(11) DEFAULT NULL,
  `team2` int(11) DEFAULT NULL,
  `points1` smallint(6) DEFAULT NULL,
  `points2` smallint(6) DEFAULT NULL,
  `ready1` int(11) DEFAULT NULL,
  `ready2` int(11) DEFAULT NULL,
  `endtime` int(11) NOT NULL DEFAULT '0',
  `level` float NOT NULL DEFAULT '0',
  `levelid` smallint(6) NOT NULL DEFAULT '0',
  `editor` int(11) NOT NULL DEFAULT '0',
  `comment` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `i_tournamentid` (`tournament_id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin AUTO_INCREMENT=1462 ;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_archiv_tournament_matches -------------------------- #


# --------------------------- /flip_archiv_tournament_ranking -------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_archiv_tournament_ranking`;
CREATE TABLE IF NOT EXISTS `$prefix$flip_archiv_tournament_ranking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `combatant_id` int(11) NOT NULL DEFAULT '0',
  `tournament_id` int(11) NOT NULL DEFAULT '0',
  `rank` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `team_tournament` (`combatant_id`,`tournament_id`),
  KEY `i_tournamentid` (`tournament_id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin AUTO_INCREMENT=682 ;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_archiv_tournament_ranking -------------------------- #


# --------------------------- /flip_archiv_tournament_servers -------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_archiv_tournament_servers`;
CREATE TABLE IF NOT EXISTS `$prefix$flip_archiv_tournament_servers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `game` varchar(20) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `ip` varchar(21) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `name` varchar(63) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_game_name` (`game`,`name`),
  UNIQUE KEY `ip` (`ip`),
  KEY `mtime` (`mtime`),
  KEY `game` (`game`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin AUTO_INCREMENT=20 ;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_archiv_tournament_servers -------------------------- #


# --------------------------- /flip_archiv_tournament_serverusage -------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_archiv_tournament_serverusage`;
CREATE TABLE IF NOT EXISTS `$prefix$flip_archiv_tournament_serverusage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `server_id` int(11) NOT NULL DEFAULT '0',
  `match_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `match_id` (`match_id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin AUTO_INCREMENT=5 ;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_archiv_tournament_serverusage -------------------------- #


# --------------------------- /flip_archiv_tournament_tournaments -------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_archiv_tournament_tournaments`;
CREATE TABLE IF NOT EXISTS `$prefix$flip_archiv_tournament_tournaments` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` enum('ko','group','dm','points') COLLATE utf8mb4_bin NOT NULL DEFAULT 'ko',
  `looser` enum('N','Y') COLLATE utf8mb4_bin NOT NULL DEFAULT 'N',
  `liga` enum('Keine','WWCL','NGL') COLLATE utf8mb4_bin NOT NULL DEFAULT 'Keine',
  `u18` enum('Y','N') COLLATE utf8mb4_bin NOT NULL DEFAULT 'Y',
  `screenshots` enum('N','Y') COLLATE utf8mb4_bin NOT NULL DEFAULT 'N',
  `status` enum('closed','open','start','grpgames','games','end') COLLATE utf8mb4_bin NOT NULL DEFAULT 'closed',
  `teamsize` tinyint(4) NOT NULL DEFAULT '1',
  `maximum` smallint(6) NOT NULL DEFAULT '64',
  `roundtime` tinyint(4) NOT NULL DEFAULT '0',
  `coins` tinyint(4) NOT NULL DEFAULT '0',
  `start` int(11) NOT NULL DEFAULT '0',
  `groups` smallint(6) DEFAULT NULL,
  `game` varchar(20) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `treetype` enum('default','small','narrow') COLLATE utf8mb4_bin NOT NULL DEFAULT 'default',
  `group_id` int(11) NOT NULL DEFAULT '1',
  `coin_id` int(11) DEFAULT NULL,
  `description` mediumtext COLLATE utf8mb4_bin,
  `icon` longblob,
  `icon_small` blob,
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin AUTO_INCREMENT=88 ;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_archiv_tournament_tournaments -------------------------- #


# --------------------------- flip_archiv_trees --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_archiv_trees`;
CREATE TABLE `$prefix$flip_archiv_trees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `event_id` int(11) NOT NULL DEFAULT '0',
  `caption` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `data` text COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`),
  KEY `i_event_id` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin AUTO_INCREMENT=39;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_archiv_trees -------------------------- #


# --------------------------- flip_calender_events --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_calender_events`;
CREATE TABLE `$prefix$flip_calender_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type_id` int(11) NOT NULL DEFAULT '1',
  `start_time` int(11) NOT NULL DEFAULT '0',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `caption` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `view_right` int(11) NOT NULL DEFAULT '0',
  `edit_right` int(11) NOT NULL DEFAULT '0',
  `signup_right` int(11) NOT NULL DEFAULT '0',
  `last_change_user_id` int(11) NOT NULL DEFAULT '0',
  `last_change_time` int(11) NOT NULL DEFAULT '0',
  `location` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `text1` mediumtext COLLATE utf8mb4_bin,
  `text2` mediumtext COLLATE utf8mb4_bin,
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_calender_events -------------------------- #


# --------------------------- flip_calender_signups --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_calender_signups`;
CREATE TABLE `$prefix$flip_calender_signups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `event_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `event_id` (`event_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_calender_signups -------------------------- #


# --------------------------- flip_calender_types --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_calender_types`;
CREATE TABLE `$prefix$flip_calender_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `caption` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `is_wholeday` tinyint(1) NOT NULL DEFAULT '0',
  `is_point` tinyint(1) NOT NULL DEFAULT '0',
  `can_signup` tinyint(1) NOT NULL DEFAULT '0',
  `text1_caption` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `text2_caption` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `text1_editable` enum('always','never','before','after') COLLATE utf8mb4_bin NOT NULL DEFAULT 'always',
  `text2_editable` enum('always','never','before','after') COLLATE utf8mb4_bin NOT NULL DEFAULT 'always',
  `location_editable` tinyint(1) NOT NULL DEFAULT '1',
  `view_right` int(11) NOT NULL DEFAULT '0',
  `edit_right` int(11) NOT NULL DEFAULT '0',
  `style` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8mb4_bin,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze:
INSERT INTO `$prefix$flip_calender_types` VALUES (1,CURRENT_TIMESTAMP,NULL,'Unbestimmter Zeitraum',0,0,0,'Ankündigung','Bericht','always','always',0,0,2,'border-color:grey; background-color: DDDDDD;','Für Ereignisse, die eine fest definierte Anfangs- und Endzeit haben.');
INSERT INTO `$prefix$flip_calender_types` VALUES (2,CURRENT_TIMESTAMP,NULL,'Unbestimmter Zeitpunkt',0,1,0,'Ankündigung','Bericht','always','always',0,0,2,'border-color:grey;','Für Ereignisse, die zu einem bestimmten Zeitpunkt stattfinden aber keine Dauer haben.');
INSERT INTO `$prefix$flip_calender_types` VALUES (3,CURRENT_TIMESTAMP,NULL,'unbestimmter Tag',1,1,0,'Ankündigung','Bericht','always','always',0,0,2,'border-color:grey; background-color: DDDDDD;','Für Ereignisse, die nicht zu einer bestimmten Uhrzeit anfangen oder enden,sondern für einen ganzen Tag gelten.');
INSERT INTO `$prefix$flip_calender_types` VALUES (4,CURRENT_TIMESTAMP,NULL,'Geburtstag',1,1,0,'Geburtsjahr','','always','never',0,0,2,'border-color:blue; background-color:AAAAFF;','Für Geburtstage. (nicht für Geburtstagspartys!)');
INSERT INTO `$prefix$flip_calender_types` VALUES (5,CURRENT_TIMESTAMP,NULL,'unbestimmte Tage',1,0,0,'Ankündigung','Bericht','always','always',0,0,2,'border-color:grey; background-color: DDDDDD;','Für alle Ereignisse, die an einem bestimmten Tag anfangen und an einem anderen wieder enden.');
INSERT INTO `$prefix$flip_calender_types` VALUES (6,CURRENT_TIMESTAMP,NULL,'LAN-Party',0,0,0,'Facts','Review','before','after',1,0,2,'border-color:orange; background-color: #FFFFAA;','Für Local-Area-Network-Parties ;-)');
INSERT INTO `$prefix$flip_calender_types` VALUES (7,CURRENT_TIMESTAMP,NULL,'Ferien',1,0,0,'','','never','never',0,0,2,'border-color:green; background-color: #CCFFCC;','Für (Schul-)Ferien.');
INSERT INTO `$prefix$flip_calender_types` VALUES (8,CURRENT_TIMESTAMP,NULL,'Meeting',0,0,1,'Tagesordnung','Protokoll','before','after',1,0,2,'border-color:red; background-color: #FFAAAA;','Für Besprechungen.');
INSERT INTO `$prefix$flip_calender_types` VALUES (9,CURRENT_TIMESTAMP,'tournament','Turnier',0,0,0,'','','never','never',0,0,2,'border-color:red; background-color: #FFFFAA;','Vordefiniert für Turniere aus dem Turniersystem.');
INSERT INTO `$prefix$flip_calender_types` VALUES (10,CURRENT_TIMESTAMP,'ticket_deadline','Ticket-Deadline',0,1,0,'','','never','never',0,0,2,'border-color:red; background-color:#FFCCCC;','Vordefiniert für Deadlines aus dem Ticketsystem.');
INSERT INTO `$prefix$flip_calender_types` VALUES (11,CURRENT_TIMESTAMP,NULL,'Party',0,0,0,'Einladung','Bericht','before','after',1,0,2,'border-color:green; background-color: #AAFFAA;','Für Parties.');
# --------------------------- /flip_calender_types -------------------------- #


# --------------------------- flip_catering_artikel --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_catering_artikel`;
CREATE TABLE `$prefix$flip_catering_artikel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `aktiv` enum('1','0') COLLATE utf8mb4_bin NOT NULL DEFAULT '1',
  `sichtbar` enum('1','0') COLLATE utf8mb4_bin NOT NULL DEFAULT '1',
  `kategorien_id` int(11) NOT NULL DEFAULT '0',
  `preis` float NOT NULL DEFAULT '0',
  `titel` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `beschreibung` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `amount` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_catering_artikel -------------------------- #


# --------------------------- flip_catering_bestellungen --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_catering_bestellungen`;
CREATE TABLE `$prefix$flip_catering_bestellungen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `subject_id` int(11) NOT NULL DEFAULT '0',
  `artikel_id` int(11) NOT NULL DEFAULT '0',
  `status` enum('shoppingcart','cashdesk','ordered','paid','delivered','completed') COLLATE utf8mb4_bin NOT NULL DEFAULT 'shoppingcart',
  `anzahl` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin AUTO_INCREMENT=8;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_catering_bestellungen -------------------------- #


# --------------------------- flip_catering_gruppen --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_catering_gruppen`;

# Diese Tabelle wird nicht weiter verwendet.
# --------------------------- /flip_catering_gruppen -------------------------- #


# --------------------------- flip_catering_kategorien --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_catering_kategorien`;
CREATE TABLE `$prefix$flip_catering_kategorien` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `aktiv` enum('1','0') COLLATE utf8mb4_bin NOT NULL DEFAULT '1',
  `sichtbar` enum('1','0') COLLATE utf8mb4_bin NOT NULL DEFAULT '1',
  `warteliste` enum('1','0') COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `image_id` int(11) DEFAULT NULL,
  `bestellzeit` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `beschreibung` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_catering_kategorien -------------------------- #


# --------------------------- flip_checkin_list --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_checkin_list`;
CREATE TABLE `$prefix$flip_checkin_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order` mediumint(9) NOT NULL DEFAULT '0',
  `check_not` enum('0','1') COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `status` enum('aktiv','optional','deaktiviert') COLLATE utf8mb4_bin NOT NULL DEFAULT 'aktiv',
  `button_color` varchar(7) COLLATE utf8mb4_bin DEFAULT NULL,
  `button_type` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `input_type` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `action_type` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `action_name` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `action_value` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `check_type` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `check_name` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `check_value` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_checkin_list -------------------------- #


# --------------------------- flip_clan_join --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_clan_join`;
CREATE TABLE `$prefix$flip_clan_join` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `clan_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_clan_join -------------------------- #


# --------------------------- flip_config --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_config`;
CREATE TABLE `$prefix$flip_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `key` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `type` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT 'longstring',
  `value` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `default_value` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `editable` tinyint(4) NOT NULL DEFAULT '1',
  `description` mediumtext COLLATE utf8mb4_bin,
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_key` (`key`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=135 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze:
INSERT INTO `$prefix$flip_config` VALUES (1,CURRENT_TIMESTAMP,'session_cookiename','LongString','flipid','flipid',1,'Der Namen unter dem das Cookie beim HTTP-Client gespeichert wird.');
INSERT INTO `$prefix$flip_config` VALUES (2,CURRENT_TIMESTAMP,'session_timeout','Integer',180,180,1,'Der Timout einer Session in Minuten. D.h.: Wenn sich der Client nach max * min nicht beim Server gemeldet hat, wird die Session gelöscht und der User damit ausgeloggt.');
INSERT INTO `$prefix$flip_config` VALUES (3,CURRENT_TIMESTAMP,'session_checkip','YesNo','N','N',1,'Soll neben dem der ID im Cookie auch die IP des HTTP-Clients auf Gleichheit zwischen den einzelnen Seiteaufrufen überprüft werden?');
INSERT INTO `$prefix$flip_config` VALUES (4,CURRENT_TIMESTAMP,'session_checkagent','YesNo','Y','Y',1,'...und auch der UserAgent-Name des Browsers?');
INSERT INTO `$prefix$flip_config` VALUES (5,CURRENT_TIMESTAMP,'template_dir','LongString','tpl/','tpl/',1,'Das Haupt-Templateverzeichnis, in welchem sich die einzelnen \'Skins\' für die Page befinden sollte.');
INSERT INTO `$prefix$flip_config` VALUES (6,CURRENT_TIMESTAMP,'template_basename','Template','default','default',1,'Der Name des \'haupt-Skins\'.');
INSERT INTO `$prefix$flip_config` VALUES (7,CURRENT_TIMESTAMP,'template_customname','Template','VulkanLAN_bstrap4','VulkanLAN_bstrap4',1,'Der Name des eines weiteren Skins, in ihm wird zuerst nach Templatedateien gesucht.');
INSERT INTO `$prefix$flip_config` VALUES (8,CURRENT_TIMESTAMP,'template_temp','LongString','tmp/tpl/','tmp/tpl/',1,'Ein Verzeichnis in welchem die Templateengine temporäre Dateien zwischenspeichert.');
INSERT INTO `$prefix$flip_config` VALUES (9,CURRENT_TIMESTAMP,'page_title','LongString','FLIP','FLIP',1,'Der Titel der Webseite');
INSERT INTO `$prefix$flip_config` VALUES (10,CURRENT_TIMESTAMP,'page_errorsinline','YesNo','Y','Y',1,'Sollen die Fehler in oder unter der Webseite ausgegeben werden? (N|Y)');
INSERT INTO `$prefix$flip_config` VALUES (11,CURRENT_TIMESTAMP,'sendmessage_receive_type','MessageType','webmessage_emailnotice','webmessage_emailnotice',1,'Default, auf welchem Wege der User seine WebMessages empfangen soll.');
INSERT INTO `$prefix$flip_config` VALUES (12,CURRENT_TIMESTAMP,'forum_postsperpage','Integer',20,20,1,'Default, wie viele Posts per Page der User auf einer Seite zu sehen bekommt.');
INSERT INTO `$prefix$flip_config` VALUES (13,CURRENT_TIMESTAMP,'subject_min_namelen','Integer',2,2,1,'Die minimale Länge für einen Subjekt-Namen. (Usernamen, Gruppennamen...)');
INSERT INTO `$prefix$flip_config` VALUES (14,CURRENT_TIMESTAMP,'netlog_enabled','YesNo','Y','Y',1,'(Y|N) In den Netlog werden beim erstellen einer Session, Login und Logout alle Daten geschrieben, die über den User in Erfahrung zu bringen sind.');
INSERT INTO `$prefix$flip_config` VALUES (15,CURRENT_TIMESTAMP,'netlog_server','LongString','','',1,'(192.168.0.1:17371|192.168.0.1|) wenn angegeben, werden die netlog-daten nicht direkt in den Netlog geschrieben, sondern an einen Lookup-Server gesandt. Dieser ermittelt noch weitere Daten Über den User (NetBIOS-Name, MAC-Adresse...) und schreibt dann in den Netlog.');
INSERT INTO `$prefix$flip_config` VALUES (17,CURRENT_TIMESTAMP,'seats_baseplan','LongString','baseplan','baseplan',1,'Der Name des Hintergrundbildes des Sitzplanes. Es kann sowohl in der Datenbank als auch im Dateisystem liegen.');
INSERT INTO `$prefix$flip_config` VALUES (18,CURRENT_TIMESTAMP,'seats_tmpplan','LongString','tmpplan','tmpplan',1,'');
INSERT INTO `$prefix$flip_config` VALUES (19,CURRENT_TIMESTAMP,'seats_max_notes','Integer',2,2,1,'Die Anzahl der Plätze, die ein User für sich vormerken kann.');
INSERT INTO `$prefix$flip_config` VALUES (20,CURRENT_TIMESTAMP,'page_permanent_message','LongString','','',1,'Eine Nachricht, die auf jeder Seite angezeigt wird.');
INSERT INTO `$prefix$flip_config` VALUES (21,CURRENT_TIMESTAMP,'lanparty_fulltitle','LongString','Meine erste LAN','Meine erste LAN',1,'Der Volle Titel der Lanparty');
INSERT INTO `$prefix$flip_config` VALUES (22,CURRENT_TIMESTAMP,'lanparty_maxguests','Integer',100,100,1,'Die Anzahl der Teilnehmer');
INSERT INTO `$prefix$flip_config` VALUES (23,CURRENT_TIMESTAMP,'lanparty_date','Date',1583427600,1583427600,1,'Datum der LAN-Party');
INSERT INTO `$prefix$flip_config` VALUES (27,CURRENT_TIMESTAMP,'tournament_losersubmit','YesNo','Y','Y',1,'Nur Verlierer darf Ergebnis eintragen (Y/N)');
INSERT INTO `$prefix$flip_config` VALUES (28,CURRENT_TIMESTAMP,'calender_locale','LongString','de_DE.ISO8859-1; German_Germany; de_DE','de_DE.ISO8859-1; German_Germany; de_DE',1,'Passt die vom Kalender verwendeten Datumsfunktionen an verschiedene Sprachen an.');
INSERT INTO `$prefix$flip_config` VALUES (32,CURRENT_TIMESTAMP,'forum_max_post_length','Integer',10240,10240,1,'Legt fest, wie viele Zeichen ein Text im Forum maximal lang sein darf.');
INSERT INTO `$prefix$flip_config` VALUES (33,CURRENT_TIMESTAMP,'sendmessage_jabber_server','LongString','jabber.org','jabber.org',1,'');
INSERT INTO `$prefix$flip_config` VALUES (34,CURRENT_TIMESTAMP,'sendmessage_jabber_port','Integer',5222,5222,1,'');
INSERT INTO `$prefix$flip_config` VALUES (35,CURRENT_TIMESTAMP,'sendmessage_jabber_name','LongString','','',1,'sendmessage_jabber sind die Zugangsdaten für einen Jabber-Account, über den das FLIP Nachrichten versenden kann.');
INSERT INTO `$prefix$flip_config` VALUES (36,CURRENT_TIMESTAMP,'sendmessage_jabber_pass','LongString','','',1,'');
INSERT INTO `$prefix$flip_config` VALUES (37,CURRENT_TIMESTAMP,'sendmessage_jabber_resource','LongString','flip','flip',1,'');
INSERT INTO `$prefix$flip_config` VALUES (38,CURRENT_TIMESTAMP,'poll_duration','Integer',31,31,1,'Dauer einer Umfrage in Tagen');
INSERT INTO `$prefix$flip_config` VALUES (39,CURRENT_TIMESTAMP,'server_iptype','String','fix','fix',1,'Gibt an wie die IP festgelegt wird: "fix"=>manuelle Vergabe, "id"=>die IP wird aus dem Bereich "server_ip_start"-"server_ip_end" zugewiesen, "seat"=>Sitzplatz des Servers, "user=>IP wird wie bei Usern aus der ID gebildet');
INSERT INTO `$prefix$flip_config` VALUES (40,CURRENT_TIMESTAMP,'page_compress_output','YesNo','N','Y',1,'(Y|N) Komprimiert die HTML-Seiten, bevor sie an den Browser gesendet werden. (Wenn der Browser dies unterstützt)');
INSERT INTO `$prefix$flip_config` VALUES (41,CURRENT_TIMESTAMP,'user_iptype','LongString','seat','seat',1,'Nach welcher Methode die IPs dem User zugewiesen werden: nach Sitzplatz oder nach ID.');
INSERT INTO `$prefix$flip_config` VALUES (42,CURRENT_TIMESTAMP,'user_ip_wrong_msg','LongString','Deine IP-Adresse entspricht nicht der dir zugewiesenen. Wenn du dich von deinem Sitzplatz aus eingeloggt hast, dann korrigiere sie bitte. Wenn du deinen Sitzplatz getauscht haben solltest, dann informiere darüber bitte die Orgas.','Deine IP-Adresse entspricht nicht der dir zugewiesenen. Wenn du dich von deinem Sitzplatz aus eingeloggt hast, dann korrigiere sie bitte. Wenn du deinen Sitzplatz getauscht haben solltest, dann informiere darüber bitte die Orgas.',1,'Kann angezeigt werden, wenn der User sich mit einer IP-Adresse einloggt, die laut Sitzplan vergeben, aber nicht die seinige ist.');
INSERT INTO `$prefix$flip_config` VALUES (43,CURRENT_TIMESTAMP,'user_ip_empty_msg','LongString','Dir wurde keine IP-Adresse zugewiesen. Bitte wende dich an die Orgaschaft um dies zu korrigieren.','Dir wurde keine IP-Adresse zugewiesen. Bitte wende dich an die Orgaschaft um dies zu korrigieren.',1,'Kann angezeigt werden, wenn sich ein User einloggt, der keine IP-Adresse besitzt.');
INSERT INTO `$prefix$flip_config` VALUES (44,CURRENT_TIMESTAMP,'user_ip_bogon_msg','LongString','Die von dir verwendete IP-Adresse ist ungültig. Bitte verwende die, die dir zugewiesen wurde.','Die von dir verwendete IP-Adresse ist ungültig. Bitte verwende die, die dir zugewiesen wurde.',1,'Kann angezeigt werden, wenn sich der User mit einer IP einloggt, an niemanden vergeben wurde.');
INSERT INTO `$prefix$flip_config` VALUES (45,CURRENT_TIMESTAMP,'user_ip_bogon_msg_opt','LongString',0,0,1,'Gibt an, ob und wie die Nachricht der Config-Option "user_ip_bogon_msg" angezeigt werden soll. (0: nicht anzeigen; 256: Als "Fataler Fehler" anzeigen; 512: Als "Warnung" anzeigen; 1024: Als "Information" anzeigen.) Wenn die Nachricht als "Fataler Fehler" angezeigt wird, ist der User nicht in der Lage mit dem Intranetsystem zu arbeiten.');
INSERT INTO `$prefix$flip_config` VALUES (46,CURRENT_TIMESTAMP,'user_ip_wrong_msg_opt','LongString',0,0,1,'(siehe "user_ip_bogon_msg_opt")');
INSERT INTO `$prefix$flip_config` VALUES (47,CURRENT_TIMESTAMP,'user_ip_empty_msg_opt','LongString',0,0,1,'(siehe "user_ip_bogon_msg_opt")');
INSERT INTO `$prefix$flip_config` VALUES (48,CURRENT_TIMESTAMP,'seats_show_ip','YesNo','Y','Y',1,'Legt fest, ob im Sitzplan die IP-Adresse eines Platzes angezeigt werden soll.');
INSERT INTO `$prefix$flip_config` VALUES (49,CURRENT_TIMESTAMP,'tournament_playright','Rights',22,22,1,'Recht, um an Turnieren teilnehmen zu dürfen.');
INSERT INTO `$prefix$flip_config` VALUES (51,CURRENT_TIMESTAMP,'network_nslookup','YesNo','Y','Y',1,'Sollen DNS-Anfragen gemacht werden? (Y/N)');
INSERT INTO `$prefix$flip_config` VALUES (52,CURRENT_TIMESTAMP,'sendmessage_disable_email','YesNo','N','N',1,'Deaktiviert das Versenden von Emails. (was im LAN sinnvoll ist)');
INSERT INTO `$prefix$flip_config` VALUES (53,CURRENT_TIMESTAMP,'dbupdate_revision','LongString','$Id: flip.sql 61 2019-07-10 20:00:00Z dbupdate $','$Id: flip.sql 61 2019-07-10 20:00:00Z dbupdate $',0,'Die Revisionsnummer der Datenbank. Sie wird automatisch von Subversion geupdated.');
INSERT INTO `$prefix$flip_config` VALUES (54,CURRENT_TIMESTAMP,'dbupdate_autoupdate','YesNo','Y','Y',1,'(Y|N) wenn Ja, wird die DB automatisch an neu eingespielte Dateien angepasst.');
INSERT INTO `$prefix$flip_config` VALUES (56,CURRENT_TIMESTAMP,'tournament_defaultwinbytimeout','LongString',0,0,1,'Soll wenn die Zeit abgelaufen ist automatisch ein Team zufällig als Gewinner eingetragen werden? (0/1)');
INSERT INTO `$prefix$flip_config` VALUES (57,CURRENT_TIMESTAMP,'ticket_force_owner','YesNo','N','N',1,'Bestimmt, ob nur Tickets mit ausgefülltem "Verantwortlich für"-Feld erstellt werden können.');
INSERT INTO `$prefix$flip_config` VALUES (58,CURRENT_TIMESTAMP,'tournament_comment_orgaonly','LongString',1,1,1,'Darf jeder Kommentare zu Matches abgeben oder ist dies den Orgas vorbehalten? (0/1)');
INSERT INTO `$prefix$flip_config` VALUES (59,CURRENT_TIMESTAMP,'changelog_sourcexml','LongString','https://gitlab.com/VulkanLAN/VFLIP/releases','https://gitlab.com/VulkanLAN/VFLIP/releases',1,'Updated the changelog URL');
INSERT INTO `$prefix$flip_config` VALUES (60,CURRENT_TIMESTAMP,'translate_language','LongString','de_DE','de_DE',1,'Sprache des Systems');
INSERT INTO `$prefix$flip_config` VALUES (61,CURRENT_TIMESTAMP,'checkininfo_subnetmask','IP','255.255.0.0','255.255.0.0',1,'Die Subnetzmaske die im Clientnetz verwendet wird.');
INSERT INTO `$prefix$flip_config` VALUES (62,CURRENT_TIMESTAMP,'checkininfo_dnsserver','LongString','','',1,'Die Adresse des oder der DNS-Server die die User verwenden sollen.');
INSERT INTO `$prefix$flip_config` VALUES (63,CURRENT_TIMESTAMP,'checkininfo_winsserver','LongString','','',1,'Die Adresse des oder der WINS-Server die die User verwenden sollen.');
INSERT INTO `$prefix$flip_config` VALUES (64,CURRENT_TIMESTAMP,'checkininfo_webserver','LongString','','',1,'Die Adresse bzw. URL des oder der Web-Server die die User verwenden sollen.');
INSERT INTO `$prefix$flip_config` VALUES (65,CURRENT_TIMESTAMP,'checkininfo_ftpserver','LongString','','',1,'Die Adresse des oder der FTP-Server die die User verwenden sollen.');
INSERT INTO `$prefix$flip_config` VALUES (66,CURRENT_TIMESTAMP,'lastvisit_timespan','Integer',50,50,1,'Die Zeitspanne in Tagen für die der Status "gelesen" (z.B. im Forum) gespeichert wird. Alles was älter ist, ist immer "gelesen".');
INSERT INTO `$prefix$flip_config` VALUES (68,CURRENT_TIMESTAMP,'statistic_numberonlineusers','Integer',5,5,1,'So viele Usernamen werden als online angezeigt.');
INSERT INTO `$prefix$flip_config` VALUES (69,CURRENT_TIMESTAMP,'statistic_onlinemaxlastseen','Integer',5,5,1,'Zeit in Minuten die maximal seit letztem Besuch vergangen sind damit User als Online angezeigt wird.');
INSERT INTO `$prefix$flip_config` VALUES (70,CURRENT_TIMESTAMP,'forum_additionaluserdata','LongString','clan','clan',1,'Eigenschaften (Columns) von Usern die zu jedem Post angezeigt werden sollen. Trennzeichen ;');
INSERT INTO `$prefix$flip_config` VALUES (71,CURRENT_TIMESTAMP,'forum_additionaluserdata_spacer','LongString','<br />','<br />',1,'Trennzeichen von Zusatzinfos je Poster (z.B. Komma, Bindestrich oder Zeilenumbruch)');
INSERT INTO `$prefix$flip_config` VALUES (72,CURRENT_TIMESTAMP,'page_index','LongString','news.php','news.php',1,'Die Startseite welche aufgerufen wird, wenn keine Datei angegeben wurde (Weiterleitung von index.php)');
INSERT INTO `$prefix$flip_config` VALUES (73,CURRENT_TIMESTAMP,'checkininfo_show_ip','YesNo','Y','Y',1,'Ob die IP-Adresse aus dem Userprofil im Checkininfo angezeitg werden soll.');
INSERT INTO `$prefix$flip_config` VALUES (74,CURRENT_TIMESTAMP,'lanparty_partlist_addgroups','String','','',1,'Eine mit Komma getrennte Liste der Gruppen, deren Mitglieder zusätzlich in der Teilnehmerliste aufgelistet werden.');
INSERT INTO `$prefix$flip_config` VALUES (75,CURRENT_TIMESTAMP,'banktransfer_account_no','String',1234567,1234567,1,'Die Kontonummer des Kontos.');
INSERT INTO `$prefix$flip_config` VALUES (76,CURRENT_TIMESTAMP,'banktransfer_account_owner_name','String','Dagobert Duck','Dagobert Duck',1,'Der Name des Inhabers des Kontos.');
INSERT INTO `$prefix$flip_config` VALUES (77,CURRENT_TIMESTAMP,'banktransfer_account_bank_code','String',66600023,66600023,1,'Die Bankleitzahl des Kontos.');
INSERT INTO `$prefix$flip_config` VALUES (78,CURRENT_TIMESTAMP,'banktransfer_account_bank_name','String','Geldspeicherbank','Geldspeicherbank',1,'Der Name der Bank des Kontos.');
INSERT INTO `$prefix$flip_config` VALUES (79,CURRENT_TIMESTAMP,'banktransfer_currency','String','Taler','Taler',1,'Die Währung in welcher der Beitrag auf der Lanpartykonto überwiesen werden soll.');
INSERT INTO `$prefix$flip_config` VALUES (80,CURRENT_TIMESTAMP,'banktransfer_subject_prefix','String','Lanparty','Lanparty',1,'Ein Prefix, welcher in den Betreff der Überweisung übernommen wird.');
INSERT INTO `$prefix$flip_config` VALUES (81,CURRENT_TIMESTAMP,'banktransfer_money_amount','String','13,00','13,00',1,'Der Geldbetrag, der überwiesen werden soll.');
INSERT INTO `$prefix$flip_config` VALUES (82,CURRENT_TIMESTAMP,'news_perpage','Integer',10,10,1,'Die Anzahl der Newsartikel pro Seite.');
INSERT INTO `$prefix$flip_config` VALUES (83,CURRENT_TIMESTAMP,'external_accessabledirs','LongString','','',1,'Die Verzeichnisse und ihre Modulnamen welche der Script external.php einbinden kann. Beispiel: \'mod1=/var/www/ mod2=tpl/default/\'. Der Aufruf external.php?mod=mod1&file=index.html würde jetzt die Datei /var/www/index.html einbinden.');
INSERT INTO `$prefix$flip_config` VALUES (84,CURRENT_TIMESTAMP,'forum_signature_show','YesNo','Y','Y',1,'Die Signaturen in den Foren anzeigen.');
INSERT INTO `$prefix$flip_config` VALUES (85,CURRENT_TIMESTAMP,'news_use_categories','YesNo','Y','Y',1,'Soll in den News eine Einordung der Artikel in Kategorien möglich sein?');
INSERT INTO `$prefix$flip_config` VALUES (86,CURRENT_TIMESTAMP,'webmessage_menu_max_len','Integer',12,12,1,'Wenn größer 0, werden die Titel von Webmessages im Menü auf diese Länge beschnitten.');
INSERT INTO `$prefix$flip_config` VALUES (87,CURRENT_TIMESTAMP,'forum_signatures_html_for','Dropdown','niemand','',1,'Sollen die Signaturen im Forum HTML beinhalten? (Alle/HTML-Poster/Niemand)');
INSERT INTO `$prefix$flip_config` VALUES (88,CURRENT_TIMESTAMP,'statistic_usersonline_show_idle','YesNo','Y','Y',1,'Soll die Zeit angezeigt werden, seit dem der User zu letzt die Seite aufgerufen hat ("Ja") oder seit dem er eingeloggt ist ("Nein")?');
INSERT INTO `$prefix$flip_config` VALUES (89,CURRENT_TIMESTAMP,'seats_images_large','LongString','tpl/default/images/seats/classic/','tpl/default/images/seats/classic/',1,'Das Verzeichnis, in dem sich die "grossen" Bilder fuer den Sitzplan befinden. Diese werden z.B. in der Ansicht fuer einen einzelnen Sitzplatz angezeigt.');
INSERT INTO `$prefix$flip_config` VALUES (90,CURRENT_TIMESTAMP,'banktransfer_last_check','Date','','',1,'Das Datum an dem zuletzt die Überweisungen geprüft wurden.');
INSERT INTO `$prefix$flip_config` VALUES (91,CURRENT_TIMESTAMP,'server_gateway','IP','10.10.0.1','10.10.0.1',1,'Dies ist der Defaultwert für die Gateway-IP der Gästeserver.');
INSERT INTO `$prefix$flip_config` VALUES (92,CURRENT_TIMESTAMP,'server_subnetmask','IP','255.255.0.0','255.255.0.0',1,'Die Default-Subnetmask für die Gästeserver.');
INSERT INTO `$prefix$flip_config` VALUES (93,CURRENT_TIMESTAMP,'server_dns_ip','IP','10.10.0.1','10.10.0.1',1,'Die Default-DNS-Server-IP der Gästeserver.');
INSERT INTO `$prefix$flip_config` VALUES (94,CURRENT_TIMESTAMP,'server_wins_ip','IP','10.10.0.1','10.10.0.1',1,'Die Default-WINS-Server-IP der Gästeserver.');
INSERT INTO `$prefix$flip_config` VALUES (95,CURRENT_TIMESTAMP,'tournament_u18_check','String','user_property','',1,'Soll der ü18-Status des Users für ein Turnier nach dem Sitzplatz oder der Eigenschaft \'volljährig\' entschieden werden?');
INSERT INTO `$prefix$flip_config` VALUES (98,CURRENT_TIMESTAMP,'paypal_ipn_server','longstring','https://www.sandbox.paypal.com/cgi-bin/webscr','https://www.sandbox.paypal.com/cgi-bin/webscr',1,'Der Server, der die PayPal-IPN-Daten auf Gültigkeit prüft');
INSERT INTO `$prefix$flip_config` VALUES (99,CURRENT_TIMESTAMP,'forum_avatars_allow','YesNo','Y','Y',1,'Sollen Avatars im Forum angezeigt werden?');
INSERT INTO `$prefix$flip_config` VALUES (100,CURRENT_TIMESTAMP,'forum_avatars_maxheight','integer',100,100,1,'Die maximal dargestellte Höhe/Breite der Avatars');
INSERT INTO `$prefix$flip_config` VALUES (101,CURRENT_TIMESTAMP,'forum_avatars_maxwidth','integer',100,100,1,'');
INSERT INTO `$prefix$flip_config` VALUES (102,CURRENT_TIMESTAMP,'user_import_lansurfer','YesNo','Y','Y',1,'Soll bei der Registrierung der Import von Lansurfer möglich sein?');
INSERT INTO `$prefix$flip_config` VALUES (103,CURRENT_TIMESTAMP,'gb_perpage','Integer',20,20,1,'Anzahl Gästebucheinträge je Seite');
INSERT INTO `$prefix$flip_config` VALUES (104,CURRENT_TIMESTAMP,'dns_user_domain','String','xxx.lan','xxx.lan',1,'Die Domain-Zone für die User-Domains.');
INSERT INTO `$prefix$flip_config` VALUES (105,CURRENT_TIMESTAMP,'dns_server_domain','String','xxx.lan','xxx.lan',1,'Die Domain-Zone für die Server-Domains.');
INSERT INTO `$prefix$flip_config` VALUES (106,CURRENT_TIMESTAMP,'watches_enabled','YesNo','Y','Y',1,'Bestimmt, ob Watches global aktiviert werden oder nicht.');
INSERT INTO `$prefix$flip_config` VALUES (107,CURRENT_TIMESTAMP,'server_ip_start','IP','10.10.200.1','10.10.200.1',1,'Erste IP für Server, falls seine IP durch seine IP ermittelt wird.');
INSERT INTO `$prefix$flip_config` VALUES (108,CURRENT_TIMESTAMP,'server_ip_end','IP','10.10.210.250','10.10.210.250',1,'Letzte IP für Server, Erste IP für Server, falls seine IP durch seine IP ermittelt wird.');
INSERT INTO `$prefix$flip_config` VALUES (109,CURRENT_TIMESTAMP,'inet_allowed_hosts','String','127.0.0.1;127.0.1.1;','127.0.0.1;127.0.1.1;',1,'Die Adressen der Geräte (getrennt mit einem Semikolon), die sich die IPs der User zum Freischalten des Internets holen dürfen.');
INSERT INTO `$prefix$flip_config` VALUES (110,CURRENT_TIMESTAMP,'inet_address_seperator','String',';\\n',';\\n',1,'Ein Trennzeichen, das zum Trennen der IP-Adressen in der formatierten Ausgabe dient.');
INSERT INTO `$prefix$flip_config` VALUES (111,CURRENT_TIMESTAMP,'inet_address_header','string','## INET START ##\\n','## INET START ##\\n',1,'Eine Zeichenfolge, die als Header vor die IP-Adressen gehängt wird.');
INSERT INTO `$prefix$flip_config` VALUES (112,CURRENT_TIMESTAMP,'inet_address_footer','string','\\n## INET END ##\\n\\n','\\n## INET END ##\\n\\n',1,'Eine Zeichenfolge, die als Footer ans Ende der IP-Adressen gehängt wird');
INSERT INTO `$prefix$flip_config` VALUES (113,CURRENT_TIMESTAMP,'inet_max_requests','Integer',500,500,1,'Die Höchstanzahl der User, die INet beantragen (in der Warteschlange sein) können ');
INSERT INTO `$prefix$flip_config` VALUES (114,CURRENT_TIMESTAMP,'inet_max_online','Integer',3,3,1,'Die Anzahl an Usern, die max. gleichzeitig fürs Internet freigeschaltet werden.');
INSERT INTO `$prefix$flip_config` VALUES (115,CURRENT_TIMESTAMP,'inet_default_priority','integer',1,'',1,'Die Standardpriorität für User, falls sie in keiner der definierten Gruppen Mitglied sind.');
INSERT INTO `$prefix$flip_config` VALUES (116,CURRENT_TIMESTAMP,'inet_default_onlinetime','integer',25,25,1,'Die Standardonlinezeit der User der Defaultgruppe in Minuten.');
INSERT INTO `$prefix$flip_config` VALUES (117,CURRENT_TIMESTAMP,'inet_noexpiry_as_online','YesNo','N','N',1,'Sollen dauernd als online freigeschaltete User ebenfalls zu der Menge an max. Online geschalteten Usern zählen oder werden sie seperat behandelt?');
INSERT INTO `$prefix$flip_config` VALUES (118,CURRENT_TIMESTAMP,'inet_allow_multiple_requests','YesNo','Y','Y',1,'Bestimmt, ob das Internet nur einmal beantragt werden kann oder nicht. (Neu Anstellen)');
INSERT INTO `$prefix$flip_config` VALUES (119,CURRENT_TIMESTAMP,'lanparty_stats_regasnonfree','YesNo','Y','Y',1,'Bestimmt, ob zur LAN angemeldete User mit zur Menge der belegten Plätze im Statusbalken gezählt werden oder nicht.');
INSERT INTO `$prefix$flip_config` VALUES (120,CURRENT_TIMESTAMP,'lanparty_password','String','','',1,'Gibt ein Passwort an, dass der Nutzer vor dem Anmelden zur LAN eingeben muss.');
INSERT INTO `$prefix$flip_config` VALUES (121,CURRENT_TIMESTAMP,'lanparty_password_required','YesNo','N','N',1,'Gibt an, ob zur Anmeldung das oben genannte Passwort benötigt wird oder nicht');
INSERT INTO `$prefix$flip_config` VALUES (122,CURRENT_TIMESTAMP,'gallery_file_ends','longstring','jpg,gif,png','jpg,gif,png',1,'Nur Dateien mit diesen Endungen können den Kategorien hinzugefügt werden. Durch Komma trennen und ohne Punkt davor.');
INSERT INTO `$prefix$flip_config` VALUES (123,CURRENT_TIMESTAMP,'gallery_hits','longstring','Y','Y',1,'Soll gezählt und angezeigt werden, wie oft ein Bild angesehen wurde?');
INSERT INTO `$prefix$flip_config` VALUES (124,CURRENT_TIMESTAMP,'gallery_maxheight','longstring',600,600,1,'Bestimmt die maximale Anzeigehöhe eines Bildes. Größere Bilder werden automatisch skaliert.');
INSERT INTO `$prefix$flip_config` VALUES (125,CURRENT_TIMESTAMP,'gallery_maxpics','Dropdown',5,5,1,'Die Anzahl der Bilder in einer Reihe');
INSERT INTO `$prefix$flip_config` VALUES (126,CURRENT_TIMESTAMP,'gallery_maxrows','longstring',4,4,1,'Die maximale Anzahl Bilderreihen pro Seite');
INSERT INTO `$prefix$flip_config` VALUES (127,CURRENT_TIMESTAMP,'gallery_maxwidth','longstring',800,800,1,'Bestimmt die maximale Breite eines Bildes. Zu große Bilder weden beim Anzeigen automatisch skaliert.');
INSERT INTO `$prefix$flip_config` VALUES (128,CURRENT_TIMESTAMP,'gallery_picpath','longstring','media/pics','media/pics',1,'Gibt den Ort an, wo die Bild-Dateien liegen werden. Dieser Ordner muss beschreibbar sein!');
INSERT INTO `$prefix$flip_config` VALUES (129,CURRENT_TIMESTAMP,'gallery_thmaxheight','longstring',75,75,1,'Bestimmt die maximale Höhe der Thumbnails in der Kategorieansicht.');
INSERT INTO `$prefix$flip_config` VALUES (130,CURRENT_TIMESTAMP,'gallery_thmaxwidth','longstring',100,100,1,'Bestimmt die maximale Breite der Thumbnails in der Kategorieansicht.');
INSERT INTO `$prefix$flip_config` VALUES (131,CURRENT_TIMESTAMP,'gallery_thumbpath','longstring','media/thumbnails','media/thumbnails',1,'Gibt den Ort an, wo die Thumbnails der Galerie liegen werden. Dieser Ordner muss beschreibbar sein!');
INSERT INTO `$prefix$flip_config` VALUES (132,CURRENT_TIMESTAMP,'gallery_albumsperrow','Integer',4,4,1,'Wie viele Alben sollen in der Übersicht nebeneinander angezeigt werden?');
INSERT INTO `$prefix$flip_config` VALUES (133,CURRENT_TIMESTAMP,'gallery_directtopics','YesNo','Y','Y',1,'Soll direkt zu den Bildern statt zur Albenübersicht gesprungen werden, wenn eine Kategorie nur ein Album enthält?');
INSERT INTO `$prefix$flip_config` VALUES (134,CURRENT_TIMESTAMP,'template_buttons_shortcut','YesNo','N','N',1,'Soll das Templatesystem Button-Shortcuts anzeigen?');
INSERT INTO `$prefix$flip_config` VALUES (135,CURRENT_TIMESTAMP,'lanparty_date_end','date','1585483200','1585483200',1,'Ende der LAN Party');
INSERT INTO `$prefix$flip_config` VALUES (136,CURRENT_TIMESTAMP,'banktransfer_account_bic_swift','String','RZSTATxxxx','', 1, 'BIC/SWIFT Code');
INSERT INTO `$prefix$flip_config` VALUES (137,CURRENT_TIMESTAMP,'user_bcrypt_cost','Integer','12','12', 1, 'Passw&ouml;rter werden mit 2^12 bcrypt Runden gehasht. Je gr&ouml;sser, desto sicherer, aber langsamer. 12 ist im Jahr 2017 ein guter Wert.');
INSERT INTO `$prefix$flip_config` VALUES (138,CURRENT_TIMESTAMP,'banktransfer_money_amount1','String','10,00','10,00', 1, 'Geldbetrag den die beguenstigten Teilnehmer &uuml;berweisen sollen.');
INSERT INTO `$prefix$flip_config` VALUES (139,CURRENT_TIMESTAMP,'google_recaptcha_data-sitekey','String','','', 1, 'Sitekey fuer Google recaptcha, src="https://www.google.com/recaptcha/api.js"');
INSERT INTO `$prefix$flip_config` VALUES (140,CURRENT_TIMESTAMP,'google_recaptcha_secretkey','String','','', 1, 'Secretkey fuer Google recaptcha, user.php<br />{#SUB _input_recaptcha} inc.form.tpl');
INSERT INTO `$prefix$flip_config` VALUES (141,CURRENT_TIMESTAMP,'seats_new_template','YesNo','Y','Y', 1, 'Umschalten zwischen alten Sitzplan u. neuem Bootstrap Template.');
INSERT INTO `$prefix$flip_config` VALUES (142,CURRENT_TIMESTAMP,'clan_seat_limit','Integer','8','8', 1, 'Die Anzahl der Pl&auml;tze, die der Clanleader vormerken kann.');
INSERT INTO `$prefix$flip_config` VALUES (143,CURRENT_TIMESTAMP,'sendmessage_format_html','YesNo','Y','N', 1, 'Ob die Systemnachrichten als HTML (yes) oder als Text (no) versendet werden.');
INSERT INTO `$prefix$flip_config` VALUES (144,CURRENT_TIMESTAMP,'lanparty_location','LongString','8888 Musterstraße 1','8888 Musterstraße 1', 1, '');
INSERT INTO `$prefix$flip_config` VALUES (145,CURRENT_TIMESTAMP,'clan_no_seat_count','YesNo','N','N',1,'Clan Seatcount devault is 0');
INSERT INTO `$prefix$flip_config` VALUES (146,CURRENT_TIMESTAMP,'page_organisation_name','String','Organisation Max Muster','',1,'Name der Organisation oder Vereins');
# --------------------------- /flip_config -------------------------- #


# --------------------------- flip_config_categories --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_config_categories`;
CREATE TABLE `$prefix$flip_config_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `custom_name` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze:
INSERT INTO `$prefix$flip_config_categories` VALUES (1,CURRENT_TIMESTAMP,'banktransfer','Bankdaten','Alle Daten für die Überweisung');
INSERT INTO `$prefix$flip_config_categories` VALUES (2,CURRENT_TIMESTAMP,'calender','Kalender','Kalenderspezifische Einstellungen');
INSERT INTO `$prefix$flip_config_categories` VALUES (3,CURRENT_TIMESTAMP,'changelog','Changelog','Einstellungen für das Abrufen des FLIP-Changelogs vom Entwicklerserver');
INSERT INTO `$prefix$flip_config_categories` VALUES (4,CURRENT_TIMESTAMP,'checkininfo','Checkin-Infos','Einstellungen für die Adressen der Server auf dem Checkin-Zettel');
INSERT INTO `$prefix$flip_config_categories` VALUES (5,CURRENT_TIMESTAMP,'dbupdate','Datenbank-Updater','Einstellungen für den automatischen Update der Datenbank');
INSERT INTO `$prefix$flip_config_categories` VALUES (6,CURRENT_TIMESTAMP,'external','External-Modul','Config der external.php');
INSERT INTO `$prefix$flip_config_categories` VALUES (7,CURRENT_TIMESTAMP,'forum','Forum','Hier sind alle Einstellungen für das Forum zu finden');
INSERT INTO `$prefix$flip_config_categories` VALUES (8,CURRENT_TIMESTAMP,'gb','Gästebuch','Einstellungen des Gästebuchs');
INSERT INTO `$prefix$flip_config_categories` VALUES (9,CURRENT_TIMESTAMP,'lanparty','LAN-Party','Grundeinstellungen für die LAN wie der Name');
INSERT INTO `$prefix$flip_config_categories` VALUES (10,CURRENT_TIMESTAMP,'lastvisit','Lastvisit','Der Wert, wie lange etwas im Forum als ungelesen markiert wird');
INSERT INTO `$prefix$flip_config_categories` VALUES (11,CURRENT_TIMESTAMP,'netlog','Netlog','Netlog-Einstellungen');
INSERT INTO `$prefix$flip_config_categories` VALUES (12,CURRENT_TIMESTAMP,'network','Netzwerk','Das Verhalten des FLIPs bei der Namensauflösung');
INSERT INTO `$prefix$flip_config_categories` VALUES (13,CURRENT_TIMESTAMP,'news','Newsconfig','Einstellungen für das News-Modul');
INSERT INTO `$prefix$flip_config_categories` VALUES (14,CURRENT_TIMESTAMP,'page','Seiteneinstellungen','Globale Einstellungen, die das ganze System betreffen');
INSERT INTO `$prefix$flip_config_categories` VALUES (15,CURRENT_TIMESTAMP,'paypal','PayPal','Einstellungen des eingebauten PayPal-Handlers');
INSERT INTO `$prefix$flip_config_categories` VALUES (16,CURRENT_TIMESTAMP,'poll','Polls / Abstimmungen','Konfiguration des Poll-Features');
INSERT INTO `$prefix$flip_config_categories` VALUES (17,CURRENT_TIMESTAMP,'seats','Sitzplan','Einstellungen des Sitzplans');
INSERT INTO `$prefix$flip_config_categories` VALUES (18,CURRENT_TIMESTAMP,'sendmessage','Sendmessage','Einstellungen des FLIP-Internen Nachrichtensystems');
INSERT INTO `$prefix$flip_config_categories` VALUES (19,CURRENT_TIMESTAMP,'server','Server','Config des Gästeserver-Systems');
INSERT INTO `$prefix$flip_config_categories` VALUES (20,CURRENT_TIMESTAMP,'session','Session','Einstellungen des Session-Systems');
INSERT INTO `$prefix$flip_config_categories` VALUES (21,CURRENT_TIMESTAMP,'statistic','Statistik','Konfiguration des Statistik-Moduls');
INSERT INTO `$prefix$flip_config_categories` VALUES (22,CURRENT_TIMESTAMP,'subject','Subject','Einstellungen für FLIP-Subjekte wie User, Turnier...');
INSERT INTO `$prefix$flip_config_categories` VALUES (23,CURRENT_TIMESTAMP,'template','Template','Hier lässt sich das Aussehen des FLIPs verändern');
INSERT INTO `$prefix$flip_config_categories` VALUES (24,CURRENT_TIMESTAMP,'ticket','Ticket-Einstellungen','Einstellungen für das Ticket-System');
INSERT INTO `$prefix$flip_config_categories` VALUES (25,CURRENT_TIMESTAMP,'tournament','Tunier-Config','Config des Turniersystems');
INSERT INTO `$prefix$flip_config_categories` VALUES (26,CURRENT_TIMESTAMP,'translate','Übersetzungen','Übersetzungs-spezifische Einstellungen');
INSERT INTO `$prefix$flip_config_categories` VALUES (27,CURRENT_TIMESTAMP,'user','User','Spezifische Einstellungen für die User');
INSERT INTO `$prefix$flip_config_categories` VALUES (28,CURRENT_TIMESTAMP,'webmessage','Webmessaging','Spezielle Config-Werte für Webmessages');
INSERT INTO `$prefix$flip_config_categories` VALUES (29,CURRENT_TIMESTAMP,'watches','Watches','Konfiguriert das Watches-Modul im Forum');
INSERT INTO `$prefix$flip_config_categories` VALUES (30,CURRENT_TIMESTAMP,'dns','DNS-Namen','Legt die zu verwendenden Zonen für Sitzplätze und Server fest');
INSERT INTO `$prefix$flip_config_categories` VALUES (31,CURRENT_TIMESTAMP,'inet','Internet-Modul','Einstellungen, die das Internet-Modul betreffen.');
INSERT INTO `$prefix$flip_config_categories` VALUES (32,CURRENT_TIMESTAMP,'gallery','Bildergalerie','Konfiguriert die Bildergalerie');
INSERT INTO `$prefix$flip_config_categories` VALUES (33,CURRENT_TIMESTAMP,'clan','Clan','Globale Einstellungen f&uuml;r den CLAN.');
INSERT INTO `$prefix$flip_config_categories` VALUES (34,CURRENT_TIMESTAMP,'google','google recaptcha','Google Recaptcha Einstellungen.');
# --------------------------- /flip_config_categories -------------------------- #


# --------------------------- flip_config_values --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_config_values`;
CREATE TABLE `$prefix$flip_config_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `config_id` int(11) NOT NULL DEFAULT '0',
  `value` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`),
  KEY `config_id` (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze:
INSERT INTO `$prefix$flip_config_values` VALUES (24,CURRENT_TIMESTAMP,39,'fix');
INSERT INTO `$prefix$flip_config_values` VALUES (25,CURRENT_TIMESTAMP,39,'id');
INSERT INTO `$prefix$flip_config_values` VALUES (26,CURRENT_TIMESTAMP,39,'seat');
INSERT INTO `$prefix$flip_config_values` VALUES (33,CURRENT_TIMESTAMP,58,1);
INSERT INTO `$prefix$flip_config_values` VALUES (34,CURRENT_TIMESTAMP,58,0);
INSERT INTO `$prefix$flip_config_values` VALUES (37,CURRENT_TIMESTAMP,41,'seat');
INSERT INTO `$prefix$flip_config_values` VALUES (38,CURRENT_TIMESTAMP,41,'fix');
INSERT INTO `$prefix$flip_config_values` VALUES (39,CURRENT_TIMESTAMP,41,'id');
INSERT INTO `$prefix$flip_config_values` VALUES (40,CURRENT_TIMESTAMP,45,0);
INSERT INTO `$prefix$flip_config_values` VALUES (41,CURRENT_TIMESTAMP,45,256);
INSERT INTO `$prefix$flip_config_values` VALUES (42,CURRENT_TIMESTAMP,45,512);
INSERT INTO `$prefix$flip_config_values` VALUES (43,CURRENT_TIMESTAMP,45,1024);
INSERT INTO `$prefix$flip_config_values` VALUES (44,CURRENT_TIMESTAMP,47,0);
INSERT INTO `$prefix$flip_config_values` VALUES (45,CURRENT_TIMESTAMP,47,256);
INSERT INTO `$prefix$flip_config_values` VALUES (46,CURRENT_TIMESTAMP,47,512);
INSERT INTO `$prefix$flip_config_values` VALUES (47,CURRENT_TIMESTAMP,47,1024);
INSERT INTO `$prefix$flip_config_values` VALUES (48,CURRENT_TIMESTAMP,46,0);
INSERT INTO `$prefix$flip_config_values` VALUES (49,CURRENT_TIMESTAMP,46,256);
INSERT INTO `$prefix$flip_config_values` VALUES (50,CURRENT_TIMESTAMP,46,512);
INSERT INTO `$prefix$flip_config_values` VALUES (51,CURRENT_TIMESTAMP,46,1024);
INSERT INTO `$prefix$flip_config_values` VALUES (52,CURRENT_TIMESTAMP,87,'niemand');
INSERT INTO `$prefix$flip_config_values` VALUES (53,CURRENT_TIMESTAMP,87,'html-poster');
INSERT INTO `$prefix$flip_config_values` VALUES (54,CURRENT_TIMESTAMP,87,'alle');
INSERT INTO `$prefix$flip_config_values` VALUES (55,CURRENT_TIMESTAMP,95,'user_property');
INSERT INTO `$prefix$flip_config_values` VALUES (56,CURRENT_TIMESTAMP,95,'seat');
INSERT INTO `$prefix$flip_config_values` VALUES (57,CURRENT_TIMESTAMP,125,1);
INSERT INTO `$prefix$flip_config_values` VALUES (58,CURRENT_TIMESTAMP,125,2);
INSERT INTO `$prefix$flip_config_values` VALUES (59,CURRENT_TIMESTAMP,125,3);
INSERT INTO `$prefix$flip_config_values` VALUES (60,CURRENT_TIMESTAMP,125,4);
INSERT INTO `$prefix$flip_config_values` VALUES (61,CURRENT_TIMESTAMP,125,5);
INSERT INTO `$prefix$flip_config_values` VALUES (62,CURRENT_TIMESTAMP,125,6);
INSERT INTO `$prefix$flip_config_values` VALUES (65,CURRENT_TIMESTAMP,60,'de_DE');
INSERT INTO `$prefix$flip_config_values` VALUES (66,CURRENT_TIMESTAMP,60,'en_GB');
INSERT INTO `$prefix$flip_config_values` VALUES (67,CURRENT_TIMESTAMP,60,'de_DE');
INSERT INTO `$prefix$flip_config_values` VALUES (68,CURRENT_TIMESTAMP,60,'en_GB');
# --------------------------- /flip_config_values -------------------------- #


# --------------------------- flip_content_groups --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_content_groups`;
CREATE TABLE `$prefix$flip_content_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `view_right` int(11) NOT NULL DEFAULT '0',
  `edit_right` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_name` (`name`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze:
INSERT INTO `$prefix$flip_content_groups` VALUES (1,CURRENT_TIMESTAMP,'public','Öffentliche Dokumente',0,2);
INSERT INTO `$prefix$flip_content_groups` VALUES (2,CURRENT_TIMESTAMP,'intern','interne Dokumente',3,15);
INSERT INTO `$prefix$flip_content_groups` VALUES (3,CURRENT_TIMESTAMP,'system','Texte, die vom flip-system direkt verwendet werden',0,5);
INSERT INTO `$prefix$flip_content_groups` VALUES (4,CURRENT_TIMESTAMP,'archiv','Content der von archiv.php verwendet wird.',0,2);
INSERT INTO `$prefix$flip_content_groups` VALUES (5,CURRENT_TIMESTAMP,'Screenshots','Screenshots des Turniersystems',0,2);
# --------------------------- /flip_content_groups -------------------------- #


# --------------------------- flip_content_image --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_content_image`;
CREATE TABLE `$prefix$flip_content_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `group_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `caption` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `view_right` int(11) NOT NULL DEFAULT '0',
  `edit_right` int(11) NOT NULL DEFAULT '0',
  `data` longblob NOT NULL,
  `mime` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `width` int(11) NOT NULL DEFAULT '0',
  `height` int(11) NOT NULL DEFAULT '0',
  `tn_data` blob NOT NULL,
  `tn_width` int(11) NOT NULL DEFAULT '0',
  `tn_height` int(11) NOT NULL DEFAULT '0',
  `edit_user_id` int(11) NOT NULL DEFAULT '0',
  `edit_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_name` (`name`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze:
INSERT INTO `$prefix$flip_content_image` VALUES (1,CURRENT_TIMESTAMP,3,'baseplan','','',0,2,0x89504e470d0a1a0a0000000d4948445200000281000003170802000000ca791805000000097048597300000b1200000b1201d2dd7efc0000000774494d4507d506170a0509c595748d000020004944415478daeddd79909cf57de0ff4f6b2e8d46a39146a373840e240408eb84e840200ed9c298c10e259bb5bd0e4bb0035e27a15c4e7e397e549c54c52ec74ea5ca0657b226bbc592c4d9ac6b9db5572a63632e2124014242421874df1add238dae19cdb97f0c352bc4a89fee99e93934afd71fae467ebae79967ba9ff7f37dae4ed5d7d70700d0e306590400a0c100a0c100800603c05529bfeb2ff1f8e38f5b8e000c04f7de7b6ffbfff6890603c000d1dcdcdc8daf665f34006467e5ca957d6e1cbc6cd9327f1800ae3ecf3ffffc47335c5555651c0c00fd7234acc100d03b19d66000e89d0c6b3000f44e863518007a87060340efe8897b742c5fbebcef2f88a7373dddc7e7f0d1398f7abf5ec556ac58d1be3babaaaaeafefbefb74cb032ec3b2bc38f5e9b641c0c00fd980643dffb580ef2c1040d067a5c4949c982050bbafd65f7efdf6fd9425fe33b1b3a90fe6043df3f5842ff555959f9b5af7dada2a2a2db5f79f3e6cdafbdf6da830f3e989fef538f95a1067b677b07f36173e7ce7df8e187070f1e9ca3d75fb56ad5a143871e7becb161c38659da5819f605f645f7e6a6a5539d6977fdf5d73ffae8a3b90b709b9d3b777efbdbdfaeadadb5c0b132d460e003d3a74fcfe9a958a5a5a56d0f4e9f3ebd66cd1a0b1cfa02fba27bc1d39b9e6edffdf2d1ad3f3ba8c985db6fbf7defdebdebd6ad8beefe1272b032d4e05ed6feeec9f04dd336997dd1f498bcbcbc871f7ef8861b6ef8f18f7f6c696065a8c103f73d77d9b32e7de2a3731e7d74cea386c2e4c8c2850b274f9ebc6ddb368b022bc3bec0f1e0de79cfa5df2484dc193b76ec6db7dd66396065a8c1de731d6c0642aee5e5e559085819f605f64577cf7b25c32bd92f9becd2d310daf7ba180a035686036465681c0c00c6c1fdcda51b6e599d0198b8f5c700d7d2d2e26b1bb0321c088bcee7bcabefbcf6778ca541b7d8b56bd7ead5ab2d07ac0c3598dcbef3da4ebe0fc7808988880b172efcf8c73ffedef7be77e6cc194b032b430d2687efbcee3d8f9ffeaebabafa2ffff22f5f7df5558b022bc381c3f1e06e7ee7f5c0b3b82a6dd8b0c1f0172b430da6377df47c7dcb64a0292a2abaf1c61bbbfd650f1f3e3c76ecd8542a65096365a8c1c0e552a9d46db7ddf6e94f7f3a17dfeffbd65b6fbdf7de7b9ffbdce7aebdf65a8b1a349830e4a5dda851a3fee22ffea2b2b232773f62f7eedddffdee776fbef9e62f7de94b43860cb1ccb132ec75cec9823e61e1c285390d70bb0d1b36bcfcf2cb163818070fe8ad30035f7a4b4b4b8b858095a17130000c5c1a0c001a0c001a0c00e49a73b23ee03ee3005686c6c100a0c100800603800603b9d2dadaba7dfbf64d9b365914301038270bfa84bababa975e7a69ddba75c78f1fafaaaa9a33678e65021a0cf484175f7c71c58a1596030c28f645439fd0dada6a2180060357a18a8a8a542a6539409f625f340c088b162d9a356bd6bbefbebb79f3e68282020b043418e8392525250b162c58b06081ef2e843ec2be6818781ffb413ef8a0c100a0c100800603800603001a0c001a0c00683000683000a0c100d00fb85725f415a954aabcbcbca2a262e2c489ddfee2bb76edaaa9a9292f2f2f2f2f1f3e7cb8ef6f000d063ef0f18f7ffc539ffa545e5e5e8e5effbdf7de5bb97265dbe34f7ffad3f7dd779f650e1a0c4444141717f7d8cff29d0dd047381e0c001a0ce44c5959d9c891234b4a4a72b7bb1bc8967dd130202c59b264c992256d8fed8b06e360a0973ef6be3f1834180006b27ebf2ffab1c71ecb70ca1ffde8479d9ebeed71fa79c87cfa9e9fff4ca6ec3bf373a5e59febbf1d807130001807f71fd98e63fadab82793b15aee7edf8f8e0573f1faddb5fc8d59079abeb96fa67ba7b72f6ac07eae8d8301c03818208391622663e5beb06f29dbe9ed8bd260a077bcf9e69befbcf34e7171717171f1ac59b3a64d9b66998006f70fd91e71c9c5ebe77afebbf2fba69ffea34b267747b07ae6efd5937fbbee72f4e8d1f5ebd7b73d2e2828d06018081c0f0600e3e04ec964b4d4f59150b6c75a72fdfafde5bce82b9df1d893c79372fdb7a36feaf45f36cd1373b7efc7be280d067ad3bc79f3f2f3f34f9e3c595353535a5adaedafbf7ffffe0d1b361c3f7efcf8f1e3b7dc72cb3df7dc63998306031111959595959595b97bfdcd9b37fff297bf6c7b3c6bd6acab7e7966bb6f26fd13737ddeb57d510396e3c100601cdc299ddb86ea0be751f7e47668ae8f4565f2d3b33d76957ebbbeef2c5b00e36000300eee59b9be537426d3e7eeee363df3fab99bbeeb47a17a72d9d29775ef51c3ceed9bc9ddfcdb17651c0c00f4a8547d7d7d175fe2f1c71f6f7bb06cd9b20e2758be7cb9050dbd6bc58a152b57ae6c7b5c555575fffdf75b2690b9f6d1ff954a575555651c0c00fd860603800603800603001a0c03c28e1d3b0e1d3a6439c080e27ed1d0276cddba75e5ca95151515b367cf5eba74e9c891232d13300e067ace8913275e7cf1c5b56bd75a14a0c10040aed8170d03485959d9a2458beebefb6e8b023418e821e3c68dfbfddffffd9b6eba292f2fcfd2000d067ace2db7dc6221405fe3783000683000683000a0c100a0c100800603800603001a0c001a0cf459adadad16026830f0818a8a8a9ef941dbb76f5fbd7ab5050e7d817b55429fb068d1a2e2e2e29ffef4a7c78e1dcbd18f3871e2c44f7ffad38d1b3756555559e0a0c1c0ff3367ce9c993367ae5ab5aab1b1b1db5f7cebd6ad4f3df554535393e50c1a0c74202f2fefeebbefce458377ecd8d11ee0542a6551435fe07830f439050505b97bf1cacacac58b175bc8601c0cf4e0a73d3fffbefbeebbe79e7b7c85306830d0732a2a2abef9cd6f8e1933c6a2000d067ad4a2458b2c04e86b1c0f06000d06000d06003418003418c899f3e7cf5b08a0c1402f58bd7af55b6fbd65398006033dadb1b1f11ffff11fffe55ffe251737aa043418481e0d7fe73bdf3977ee5c4e7f4a4b4b8b450d1a0c7ca0b8b8b8edc1a1438772fafdbee7cf9f7fe38d372c70d060e003b7df7efbd4a953db1ee7ee1b06abababbff39def9c3871c202070d063e505454f48d6f7ce3f6db6fcfdd8fd8b265cbdffccddf1c3f7edcd2863ec2fda2a1cf7c1af3f3bff4a52f4d9c38f1ecd9b3ddfee2bb76edfae10f7f682183060357b464c9929a9a9a6e7fd9f7de7bcfb285bec6be68e873cacbcb2d04d06000408301408301000d06000d0600b2e1da24188856ac58d1fe78f6ecd913274e4c3ffdc18307df7efbedb6c7a3478f9e3f7f7e2a954a337d7d7dfdaa55ab1a1a1a329fa5f9f3e78f193326fd34bb76ed6abfc86ac2840973e7ce4d3ffd850b175e7ae9a5d6d6d6cc67e3b6db6e1b316244fa69de7ffffd9d3b77b63d9e3a75ea8c1933d24f7ffaf4e96cef3ffaf18f7fbcfdf6a557b269d3a603070eb43d9e316346fb7dd6aee4e8d1a36fbef9661643b44183962d5b565050907eb237de78e3d8b1636d8fe7ce9d3b61c284f4d3efdfbf7ff3e6cd99cf467171f11d77dc917e365a5a5ad6ae5d7bead4a98848a552f3e7cf1f3d7a74fa97ddb973e7fbefbfafc1404444636363e2caaebbac5cb9b2fdf1830f3e9818e0ad5bb73ef9e493cdcdcd11316edcb83ffdd33f4d1fe0e6e6e61ffce007bb77efce7c96e6cd9b9718e0f3e7cffff0873fbc70e142db7af6affffaaf135ff617bff8c5af7ffdebcc6763d8b061f7dd775f260bb0bdc18f3cf248e2f47bf7eebd749967b84592d8e0bd7bf73ef7dc736d8f6b6b6b131b5c525292ed6c0c1f3e7cf1e2c599bfec860d1bbef9cd6f0e1a946e0feb8409139e79e699eaeaeacc67e3e0c1830f3ffc70facd851b6fbcf15bdffa56dbdbe385175e78e28927468d1a95e62953a74e7dfdf5d7737a6ff67ed0e0c6c6c6356bd6b4dd1568d0a0410b162ca8a8a848ff947dfbf6bdf3ce3b99ff88bcbcbc3befbc73c89021996fd84e9e3c79e6cc99e9a76f686878e18517dad64a19bae9a69baebdf6daf4d39c397366d5aa556d8f0b0a0a3ef9c94f26beecfaf5eb8f1c3992f96c9495952d59b22471b2575e79a5fd6e4d990c50aaabab376cd890d55fffaebbee1a3a7468fa6976ecd8b175ebd6b6c7959595f3e6cd4b7c47bdf4d24b590dbfa64f9f7efdf5d7a79fe6ecd9b3ab57af6efb730f1e3cf8f6db6f1f3c7870e663940c5776f3e6cd2b2929e9c90fe0030f3cb074e9d2f4d31c3a74e81ffee11fda7ef7a14387fec11ffc41621b7ef6b39f6515e0e2e2e2cf7ffef389933df7dc736d6bd88858bc7871fa356c445cbc78f1b5d75ecb6a81cc9e3d3b7d3fdab630f6eddbd7fe9fd75c734de2cbd6d6d666fba7c964dd327cf8f0f6c799dc8274e8d0a163c68c397af468e6b3f1e69b6f263678c68c19ed2f7bf8f0e12d5bb6cc9e3d3b7d2f3ffbd9cf3ef9e49399cfc6ba75ebe6cd9b376bd6ac34d38c1c39f291471e69bb195c5d5ddddffffddfffd99ffd595151d195a64fa5525ff8c2174e9f3ebd65cb9601dae0c6c6c6279f7c72fbf6ed6dfff9d0430f250678fffefddffbdef7b2baa3fdf2e5cb13037cf2e4c9a79e7aaaed4d9f9f9fffed6f7f3bf1655f78e1859ffffce759fdbe0b172e4c9ce6edb7df6edfa29c3e7d7a260d7efef9e7f7efdf9ff96c54565666d2e04d9b36b5efa8292929496cf0e0c183b3ddc48e88fbefbf3ffd0463c78efdfef7bfdff6174fa552dffad6b7d2bf490a0a0af2f3f3fffddfff3df37948a5527ffee77f3e69d2a434d39496969697973ff3cc336dffb971e3c63ffaa33fcacf4ff7f1f9d8c73ef6f2cb2fb76f4064e2d5575ffdfad7bfde6319beefbefb12df60a74f9f7eeaa9a7eaebebdb3e1a5ffbdad7123fa49b376f7efef9e7b39a93e5cb97979595257e485f7ae9a5f60deb4c46ab6fbcf1465d5d5db60dce644cd6fe1dcff9f9f9891f8ddc35f8d27de619de067cead4a9593578dbb66d67ce9c19366c58faa62e5dbaf45ffff55fdbfef357bffa55e262bce9a69b66cc9891d5bddbfef99ffff9affeeaafd27f3a66ce9c79efbdf7b6ed1ba8aeaefea77ffaa7af7ce52b69f6d9e4e5e5fddeeffddedffddddf5dba5135501adcd2d2f2ecb3cfb60778d9b265895b5b172e5cf8d18f7e945580274e9c98b899dfb6d9defe8ebfebaebb2eddbabcd2c7e395575ec9eaf71d3f7e7ce2667b44ecdab52babedebb6b564b64b3ec36159561feff2f2f2e1c387673533ebd7afafaaaa4abf57b3b4b474fefcf96bd7ae8d88d6d6d65ffffad75ff8c217d2bfec1d77dcf1f2cb2f67fecd04adadadcf3cf3cc134f3c917e57f0c2850b77ecd8d136aedabd7bf74f7ef2932f7ef18be93e5af9f95ffdea57fff66ffff6d0a14319cec9fefdfbbffffdefe72ec3555555ed8fa74d9b76e38d37a69fbea9a9e99d77de69ff60ce9b37afb2b232fd53eaeaea8e1c3972e90f4a3472e4c85b6fbd35938573efbdf7b63d9e32654a26b7126b6d6dcd6a4ef2f3f36fbae9a6ac5e76c48811797979894f9934695256731211e9b3d7bec7aefd6553a9544b4b4be220feaebbeecaf62e6c353535893373db6db7d5d5d5b56f9ad4d4d424fe94dffddddf7df5d557b33a54bf6ddbb6c43d619ff9cc67c68c19d3fecd60bb77ef4ebf8bbea8a8e88ffff88f5f7bedb5f3e7cfa77fe54e8c31fa74837ff6b39fad5fbfbeedf1dcb9731f78e081c4f7fdb3cf3e9bd577aea552a9dff99ddf49fc84ecdbb7affd3c85a2a2a24c869e1b376ecc76c376ce9c39994c76e91ebcc4a3746d5b03d9dedf3fc3fde7d936b86d133babddd1478f1e3d78f060e2a6c6d2a54bdb1a1c116bd6aca9aaaa2a2d2d4dbf267de081079e7efae9cce7e4f0e1c32b57ae4c7c137efef39fdfbb77efc183072362d5aa5593264d4abfe1585c5cfc877ff887dffdee77db4e15c93cc37ff2277fd2edc78613773974b82433d96572d9af7ccf3df7e4628d3177eedcc433b03eba35968b39993c79f2e4c993b31d5e6732c2ce56595959b67fd689132766b262c9565e5e5e266bcecb3632b2dd2ec970b5bf68d1a2ac9e52585878f7dd77274e76553578efdebd0505056d7f80e1c387df7aebad899b6f7bf6ec99306142e21977979a366d5a26efb6eaeaeaf6b7c2f4e9d3138f504644434343b6ef9e4cd6650d0d0d0b162cb874774de2531a1b1b33d92977a90cc75873e6cc69df7c49dfbc769ff8c427c68d1b97d5cc9c3b772e719a0913263cfcf0c3ed9b5fd5d5d589876f6fbef9e6871e7a28ab6f3e48a552a74e9d4a7f426c4141c137bef18dd5ab57b76defd7d6d6d6d6d6a6df893a62c488279e7862cd9a35ed43844cac5bb72edbf801fd51aaed484f573cfef8e36d0f962d5bd6e104cb972fb7a001e8bf1e7becb1f4a5ebdcb0de3d3a00a077683000683000683000a0c100a0c100800603800603001a0c001a0c00683000683000a0c100a0c100a0c100800603c0d52dbf3fcef4133fe9e01fbffd60d6d30080713000683000a0c100a0c100800603800603009dd62faf4dcae41223972101601c0c00683000f419f91641863abcf1563bbbbefd05bdd900e36000300ecee550a013a3816e7f4100300e0600e360e0aae09834180703800603001a0c001a0c00749273b218289c4604180703001a0c00bdc7bee85cd9733cde3f14076be2c4d9b8d81811515c18234b635245cca88c09e5ddf9b36a2fc4cea371a8268e9d89da0b71be219a9aa3b5350af26368518c2889f12362f2a8983a3af2f372fe8b37b5c4f6c3b1fd701c3e1d35e7a3a1315a5a63e8e0a8288d2fdf99f0dc53e763d7d13876268e9f8953e7e3625334344543530c4a45417e14e6c5d0e2282b8ee143a262588c2b8b31c3a3a80fbc859b5a62dff1385013874fc7e9f371a62eea1ba3a93906a5a2b0208a0ba27c685494c6f8113165548c28e9fe1938792ede3d187b8fc5d13371e162b4b6464951940c8ef1c3e3bab171ddd8282af089040d1e18b656c7f35be268ede5ff7eb63eced6c7dee3b1eafd98501e9f9c1d534675e9073536c7a67df1e6aea83ed5f104171be362639c3c173b8fc6ab5ba3303f664f8cdbae8f8ad2ec7e5086b76b68698dd777c4aaad71aefef269ced4c5858be91af6e6ae787bef157f9196d6686a88ba88daba3874c9bfa7224696c6e4513179544c1915c38774c36f9195ddc762fdeed85a1d0d4d5798ed8b71e1629c3c173b8e7cf08f234a62d635317b528c29eb8637dbf1b3f1abcdb1b53a5a2fdb2cab8bdabaa83e156fed89e2c2b8f3c658785de40feafcf2c9766247df41837b7c3cd41c3fdf101bf7264f79b026feebcbb1e8bab87776e475ea80c0863df1c2bb71a62e8ba73434c5faddb1614f2c9c169f981985ddfac73f5b1f3f5e13074e5e7182d62bfcfbb6c3b162639c3adf991fda1a71e26c9c381b6fedeee9f5febe13f1cbcdb1ff64d64f3c753e566d8d555bbb616e5fdb16cf6f89e69684c9ea1ae2b9cdb1e540fca72531a4d0c71434f86a74b129fefbaaec56caeb76c4c9b3f1c5c55190cd2ee2ba86f8c91bb1fd7027e7b3a535d6ee88ed47e23f2e8ed1c3bae777afbd104fbf14a72f64fdc4b7f6c4cfdf8a96d6fef4876e6e895fbe13ebb6472fce754b6bfcaf3763f3be2c9e72b0269e7e29beba3406db2f0d7d8673b2ba6904dc12ffb4ba33a3a2ed47e27fbe1ead19afce4f9d8ffff262e703dceec4d978faa53858d33d1b1fcfbc9a1ce08ffe8e7b8ec5ff5edfcf025cdf18cfac8ab5bd1de07f5b975d80db1c3f132bdff661050dbeeaacd8187b8f77f2b9ef1f8a35db339af25c7d3cb32a4e9ced9e79ae6b88675fed8657fbf986387ea633a3c99f6de8677fe586a6f8efafc69ee3bd3c1bbfd814bf39d8c9e7bebd37761df579050dbebab41d8fecb417de8d93e792c791fff67af26459b9d0103f5e1b4dcd5d7a910c0764970d1c771ce9b68d891ef3d3f5e90e78f798753bbaf4f4b53b7c5ea1af703cb84f686c8e9f6f8847ee485875ee3996f03a53c7c4dcc9317164940e8e41a9385b1f7b8ec71b3bd3ed733e561b2ffe26ee9995fb5ff2c3117eef50c2e4132be2962971cdc8282b8e82fc686a8eba86b8d010c76ae3f0e938742af69f88a6969efb1b6ddc1bef1eb81ade6cdb0f476d5d9415fbd881065f758615c7add3e3fa715136245211b575b1fd70acdb917cdeefaea371f8748c1b7ec501eb4bbf49f7f4a183e3b3f3e3bab11ffac7112531a224e64d8e35dbe3b9cd573ceabc767b2c989670614f862a4a63dee4983636860f89e2c2686a8e337571ec4cec3b11dbaa3f34e5a153e95e67fed4f8f4cd91bae45f0af3a3303fca86c4b8e1317b5244444353ec3c1a5babe3dd831f5c819d3b179be2979b339ab2202f664f8a6963a272440c298a82bc387f31ced6c7fe13b1fb586c3b9c7c1a73e652111fbb26664f8af123626851d435c4fe93b1767bc2def296d6d87534e64df661050dbebadc303e1e5cf0a1fb218c2a8d51a5f15bd7c64fdf8c77938ee1bdb1337efb962bfe5ff557ce4c71617cf9ce7427392f9e1e2dad57ac48534bacdd1e9f9ad3b51ea462d9cc583cfd43975a15e64745695494c68ccab877f687a63f9bf6aaaabb667c28c01d2acc8f199531a332aae6c6e6fdb17e570effb2eb77c5f98bc993cd9e149f9a1d43075fbe5936ac382a47c4a2ebe26c7dbcb133d66ceff87ae2ac9414c57f5c1c932a3eb41d36a3326eac8c5f6d8ed5dbd23df7504d070dbeec5aa95c5c4e0d5cc6f1e06e33a13cbe706bc737242acc8f0717c6c48a8457d8bc3f1aaf706836fdf1e6fbe6265f6574dbf5571c6447c4c6bd5d3d3f79f9fc58724316d73ad7a71db9166773196b617efcd6b5f1b54fe4f08ffb6606815ffab17870c1e501be4ce9e0f8f8c7e2eb9f8c1b2bbb343f85f9f1e53b3f14e04b07c7f7cc8ef123d23dbdfa94cf2b68f0d5e5fe79e9ee439437283e3d2f616cd7d0d4f1caf1504dba2b7f2a4a63eea40cc6a9110ba75df1ffad6b887d5d38dd77dee48ce6e152e9ef31f9f6de3ef4973d7c3af954b899d7c4dd33327dc1b221f1a5c55d9aa54fcc4c779bad5424fc39ce5df479050dbe8a4c1c997c0be871c39387c21d9e76bb3b6d1de7641cbf29a3d3fdbf9dbee4266f50674ee92a493b5e5cb131fecfc63858d327ae1ede7d2c790974714f7e768bae28164c4d7ab3a51d07d737f8c8429fe07870f7b869424693cda88c7d27d24dd0e15d3e0ea5bd93c6e48a4c677258da53610f7576ffe40de31376c076684279baab8a5b5ae38d9df1c6ce28c88b51c3a2a2342a8646c5b018551aa387f5c4374f246e185d36081ed683a719cf9e98bccf3ffd69cff58d3eb2a0c1579134875a2f95fe285d44c7fb9cd3ef08fdafaf74cfaf5073be934f9c3aa633cf9a3e36a31dce8dcd517dea43bbe807a5624c594cac88696362da986ebeeb75c74b266947f4b4b13dfa669b94c1577d0c4e7b40bd7fdd9b0c3498041599dd787964d21716d57574a0aef6424ffc0a67eb3af9c4ca119d79d64d1362444967beaaa1a5350e9f8ec3a73f1825df3421164f4fdeb8e98ac4db705e53dea36fb64c36f8f21d6582fec027b57b64781ffcc4c9ea3ada4978b1a9277e85c6cefe94b24e5d589c3728ee9b9b7c0152e22879d3bef8fb5fc7ff5edf0d57fb5c49e22b9714f5e89b2d931f97e7930d1adc15892b91aceeb0d8d8dcd51f975e865f7c94385987f79a686eee8905dee97b4e75fa7b786e1c1ff7cfeb6a8623a235e2ad3df12f6bbaf3f617592d99a29efd26a2a20cf65ea552016870e7259e7793d5b82771e282ae9de6d3d8dc3d9375b836cfcbebd3efa1ae2cba05d3e2a125515ed20db3b1eb683cbfa57736072ff6ec294e99f47590068306774571d2d8e24236d757244e5cdcb5ef36cff044d3c4c98aaf708b8fabd8f4b1f1f57be3730be2dad15d1dbdbdbe33ced475ff1c268e3bcfbbdc16e8dc68b3cfce59d9908473618e9f898ad24c5f2df1cbf5caba76b7e4936733ba09fec9a46f0aea7053a0ac382e5c792dffff7fa6a78f47e662a0396752cc9914172ec6cea3b1ef44549f8aa367b21e5f3635c79603b1787af7bf15d357f6404d166f45807ed0e09143132ea53d7c3a8b1bfe1d399dfce3baa2fa545c3b3a79b2c349b3d1e1a640f9d0744f3c569b70f38d7e644851cc9a18b3267ef09fe7eae3e4b938792e4e9e8da367e2c0c938579ff00abb8f757f83470e4db8b9e3ce2359df260c20faf2bee8c40b3012bffc2eab8933bcc0f74a32fc4ef5c4c93abc9156fa8b7f765ebd5fc93e74704caa887993e31333e34b8be3cfee8f8797247cbfd3b1daee9f8dc43ba06d3990937de0bdbc6a487b5cc015c670953738f14604874f67fa85ea074e260f40275574696e0f9c4cbecfd4e1d3b1ff4452834776f08f53d22e8acdfb07ca0a31958aebc6c6e716a49be6420eeec298b887a3b9257eb1e96a5bdae9cf4248dc2101f4ef068f1f11a54977405cb131f90aa5a6e658b131619ad2c109f7d74dd41ab16263ba6b635a5a63c5c6684d5aeb7578af896b46a65b14a7cec7eb3b3b3fe7fb4fc67f7ba53fbd65278c4cf7ff3636e7e4ad589e74a862cb8178f9bd4c5ff0fcc5f81febfafa724e7f26dad66a2b4fb8aa1b9c8a0fbeaa3d8d43a7e2d9d5e936c9cfd6c7b3ab9347a8732777c355aa074ec6bfadebf87e1a8dcdf193d7130e6f47c4ec891d5fe7934ac5bc29e99ef8ab77b2fec685d6d6d87638fedb2bf1a31793bf932047fee185787b6fd6d725a7bf73e4d0dc9c9e96f81d0911f1c2bbf19337120688f58df1dab6f8c12fe3dd037d7dd53034ed3986cf6d8ed7b6c5b133d1d014764b43a7f5e9ab5ee64f8db5db1376b4ee3e167ff78bb8654a5c372ec694454961b4465c6888a3a763fb91d8b027f9cae041a9983fb57b66f8bd43f183e7e2d6e9317ddc07872dcfd4c5f623b1767b4637654c331b0ba7c5daed571ce43535c7b3afc62767c782a9c997f71c3e1dbf39186fef4dbe0563ae1dac89fff5663cb739665e13378c8f29a393efb0d8d89cb0d7b73437df9df05bd7c6aaade9ce4e6fb3795fbc7730664f8aebc6c4f8f228298cfcbcb8d010e7eae3604dec39165bab7be8ae675d37b62cdd97853434c5739be3b9cd1dffbfdf7ed0aa15fa7f83470e8d9ba7c4fadd09933534c5da1db176476757af53634449b7cd736d5dba75531ad78e4e77d3e361c5b1e48678f137e9e2b46263bcb62de64d8e491531ba2c0617442a15172e465d439cad8f433571a0260e9e8cb37dec48def98bf1face787d6714e6c7a48a185b16a3cb62f4b018521483f3a3a82052a9a86f8c936763cff1787357c2d6cc94513999c9a282f8e4acf8f7f5c9533636c75bbbe3adddfd7ed550392236ecb1868401dce088b867566c3b9cc3934ecb86c4b2997de0cf90179fb939619a3b6e8cad8713bec7f0d4f9749deee31a9a62c791d871a44b2ff2b16b72357b374f896d87333d01fe2a706365ac7cdbf9cf905b7dfdceeec585f11f16e6eafb620bf2e2f30b3b7fbbe34bdd32a54b4f5f7a53f24d1ef206c5176fcde84e2003d6f4b16ee77c4f00000af649444154c9d71175c567e7c7352307cac21c561cd3c7794fc1c06e70444c1e155f58d4d5fb397718e02fdedaf1f5b89d70ffcd9dbfbae986f171dbf5194d397c483c7267c205b203d6e082f8ed5b72fb230af3e3e1253179d44059a4f7cdb9caef930a1a9c69a5be7257771eb52d1f1a8fdedd9d9bf9f983e2a1db3b33489a36263ebf288b3bec5794c67ffe784c1de3adfb21a583e3cb7776f586a31996fe913be2d6eb62207c2742f9d0f8dc82eedffc05fa5983236242793c7e4fdc71435737cc0bf3e3ce1be3f17bbaff5bdf0717c497ef8cd913b378cac269f1d0ed59afe3860e8edfbd237efb96e4eba7139515c7edd7c71f2eebdf6fe21bc6c7634bbbff0f7a256ddf7cfc7b770f88fdd2332ae3b1a557cfcd50a1afe94f7b9a0af363d9ac587c7d6cd8139bf6c5d12cef4a386e78cc9914f3a6c490c25ccd61415e3cb8303e764d3cbf25e15b222acbe393b332bac574875211bf756dcc99149bf6c5863d71f06416d7680e4ac5f8113175745c372e268feacdf1dcff57157b8ec59ee3b1ef449c3c9bf565a685f9317d6c2cbaae77760e4faa88af2e8dddc762fdee78ff5016f70619511233af8939fde7fed2e386c757ee8c23a763ebe13870328e9f89fac6a86fccd5b735c38092aaafefeaa52a8f3ffe78db8365cb3a1e4f2d5fbe3c17b35e5b17bb8fc591d371ec4c9ca98bb375d1d0f4c17a216f50141544e9e018561ca387c5b8113165540cebc1b3995a23f61c8bf70fc5c19a3879ee83af2c1c5c1015a531a922665476f310ea6c7dec3c1ad53571f44cd45e88f317a3b1299a5ba3302f8a0aa2303f86154745698c2a8d51c3e29a91dd731a5af76a688a23b571e4749c3c17b517a2b62eced6c5c5a6686a8ec6e6c81b14857951981f254551312c460f8b71c363eae85c9dac97ada696d8773cf69f8c23a7e3d4f93853f741a252a928ca8fe2c2281f1a23874665794c19d59d8754801ef3d8638fa52f5d5555d5553e0ebe4c5971cc9d14d127c713a9886b47777e989badd2c1317752fffeea9ec2fc9838b2e3db65f77df98362ea1807e981ac0db20800408301408301000d06000d0600341800341800d06000d06000408301408301000d06000d06000d0600341800341800d06000d06000408301408301000d06000d0600341800341800341800d06000d06000408301408301000d06000d0600341800341800d06000d06000408301408301408301000d06000d0600341800341800d06000d06000408301408301000d06000d06000d0600341800341800d06000d06000408301408301000d06000d0600341800341800341800d06000d06000408301408301000d06000d0600341800341800d06000d06000408301408301408301000d06000d0600341800341800d06000d06000408301408301000d06000d06000d0600341800341800d06000d06000408301408301000d06000d0600341800341800341800d06000d06000408301408301000d06000d0600341800341800d06000d06000408301408301408301000d06000d0600341800341800d06000d06000408301408301000d06000d06000d0600341800341800d06000d06000408301408301000d06000d060034180034180034d82200000d06000d0600341800341800d06000d06000408301408301000d06000d0600341800341800341800d06000d06000408301408301000d06000d0600341800341800d06000d06000d06000408301408301000d06000d0600341800341800d06000d06000408301408301000d06000d06000d0600341800341800d06000d06000408301408301000d06000d0600341800341800341800d06000d06000408301408301000d06000d0600341800341800d06000d06000d06000408301408301000d06000d0600341800341800d06000d06000408301408301000d06000d06000d0600341800341800d06000d06000408301408301000d06000d0600341800341800341800d06000d06000408301408301000d06000d0600341800341800d06000d06000d06000408301408301000d06000d0600341800341800d06000d06000408301408301000d06000d06000d0600341800341800d06000d06000408301408301000d06000d0600341800341800341800d06000d06000408301408301000d06000d0600341800341800d06000d06000d0608b0000341800341800d06000d06000408301408301000d06000d0600341800341800d06000d06000d06000408301408301000d06000d0600341800341800d06000d06000408301408301408301000d06000d0600341800341800d06000d06000408301408301000d06000d0600341800341800341800d06000d06000408301408301000d06000d0600341800341800d06000d06000d06000408301408301000d06000d0600341800341800d06000d06000408301408301408301000d06000d0600341800341800d06000d06000408301408301000d06000d0600341800341800341800d06000d06000408301408301000d06000d0600341800341800d06000d06000d06000408301408301000d06000d0600341800341800d06000d06000408301408301408301000d06000d0600341800341800d06000d06000408301408301000d06000d0600341800341800341800d06000d06000408301408301000d06000d0600341800341800f890fc1ef8194f6f7a3a172ffbe89c47fdfd00300e0600341800341800e8c70d76dc17000d166000b8ea1aacb50068b00003404fc8efdd1fdf5edf1c5d430c001a9ccea5013626066080e8fd7dd1020c8071706ffa687ded9d06c03838e7f5ed70f86b4c0c8006f7a8a7373d6d040c8006e776ecfbe89c472fcb6dfb7fb63d301406e02ad6fbd7263dbde9e9f6d67eb4ca9765d81019000dee4eed65bdb4b8720bc0d5ad978f070b2d00c6c1d20b0003691c0c001adc27181f03a0c1008006038006e7da95ee5b09001a0c00683000683000a0c100a0c10080060380065fcec548006870dfcab03603a0c1bd90610106408373aefd7b1adabb2bc00068704f6738f11f014083739b61010640837b21c3020cc04090dfa7e6467d01300e0600341800ae463dbd2fba8bd71dd9590d80713000a0c100a0c100800603800603001a0c001a0c001a0c006830000c103d7d9f2c37ba0200e36000d06000d06000a067f4c4f1e02e7e571200180703001a0c001a0c00683000683000a0c100a0c100800603800603800603001a0c001a0c00683000683000a0c100a0c100800603800603001a0c001a0c001a0c00683000683000a0c100a0c100800603800603001a0c001a0c00683000683000a0c100a0c100a0c100800603800603001a0c001a0c00683000683000a0c100a0c100800603800603800603001a0c001a0c00683000683000a0c100a0c100800603800603001a0c001a0c001a0c00683000683000a0c100a0c100800603800603001a0c001a0c00683000683000a0c100a0c100a0c100800603800603001a0c001a0c00683000683000a0c100a0c100800603800603800603001a0c001a0c00683000683000a0c100a0c100800603800603001a0c001a0c001a0c00683000683000a0c100a0c100800603800603001a0c001a0c00683000683000a0c100a0c100a0c100800603800603001a0c001a0c00683000683000a0c100a0c100800603800603800603001a0c001a0c00683000683000a0c100a0c1004016f2bbf1b59e7ffef9acfe1d008c8301000d06808121555f5fdff55759b972a54509c080555555d56be3e0cefd6c0018c8ba6d5fb40c0340ef343822eebfff7e0b14007aa1c1adadad46c300d00b0d6e23c300d03b0d966100e8b506cb300024ea9eeb830180be320e0600341800341800d06000d06000d06000408301408301000d06000d0600bae2ff0294e65b2271f807a70000000049454e44ae426082,'image/png',641,791,0x89504e470d0a1a0a0000000d4948445200000051000000640802000000715a4b7f00000dca49444154789ce55cdb531bd719ffadb4baac242e96c4c580c00603bec40c356e0a9d29ce38765f3cee64a61db74fed4c5ffad7f4afe84c1e321d279d38ed8c3b8deb348de39062c717820958b68cc18004028416743b7d58bc3e3ab75d099cb4f87b60ce7efb9defb2e77c97f3ed226d7b7b1b402e979b9a9ac2c185603078faf4694dd300e88410002b2b2b8f1f3f8e46a3169606b3645a34af4043480fd52d7e7b7b3b180cd63d9d8752a554281718a4dfebd73dba7d79efde3dafd77bf2e449003a6de4f8f8785353937d6999fae9d34f6346ecab85af0034041a08213be59d2b43576c1ae631d918fa163d9e9e9e3e7efcb885492693478f1e95b152f3b7e161fa61a9527a9c7dbc925f01416bb8356366c63ac74ec64fda3457af5edddcdc7cb5ce42a6f6da36059a8e1d3a962fe6b33b59b3649e8c9d9c589ca01f8a1a6c1a5bd0c0c0808524841c3e7c9827661e998d143e4d0bfa0ff59b25b3aba12b994df636f70a0db1911e5e4b8b9df612caa45c2817e6b2730bb985cd9dcd85dc82e1336c4a7e656c0cc3c7c63f7af4c8c6a4d3697a224dc64c67308cd042b9b0b9b339b33a93ddc97eb7fa5dd81716d25bf6eb705a2edda34f67a6fb9afb6c4c7763b7825eb845693871e2844d363939d9d5d5b5176e0062c1d8547a2aec0b1f6d3a6a23a3465446af2b385af246da47149b4ae184327ffeecb3cfc6c7c7ad713c1eafc93c21595bb8ad3dd2eea8155eaebcc09f218a438c7731c4a0b6348da489ed71369bb5ef8e8d8da1dae741f93333d1764eb50e348dd072d69f09210c5f7e2004f2121434169c3d7bd61e4f4f4f0b0d10f27164ce4fe4b52284b07bdbbebc75eb566b6b6b3a9d6e6969f1783c003c1e4f229198989828954a3e9fafa1a12197cb793c1e42482814324d737b7bdbe7f34522918d8d8d7038ac695a4f4f4f2693e9e9e9a177532c16b3c7966fdbdad8f8bb77effafdfe62b1a8eb7ab95c8e44225b5b5bd62dc330ac0cbfb3b36369a2eb7aa954f27abd00060707575656e2f1b8cc4d344dd319942d3897cbcdcfcfb7b4b40048a55216be5028140a8562b14808d175fdf9f3e7baae572a154dd3a2d1e8dada1a2164696949d775c3302a95cadcdc5cb158ece9e9a145cccfcff7f6eea693fbf7ef0f0d0dd1d22db50cc300f0e2c50b5dd72391482693d9dadab214b36ceee8e8585d5d2d168b3e9fafadad6d7676f6f2e5cb3333338f1e3dcae7f3749800b741589b6d8a8b172fdaa476d900a0afaf4f3806e56f4c56649cc53618c093274f8686860429d4e3f1f97ca3a3a3f97c3e9bcd7abdde53a74e594fc1b2b95c2e8f8c8ce4f3f95c2e572814c6c6c666666642a150575797699abc9d553633314c36760974769505cf4f3ef9e4d2a54bd67860604028a8bfbfdf1a8442217ad1e8e7c5dcb5b6a4855468b85b93c81e892c3cd684e109cae5325e4617cb6619a54b896e66d17841dc862468dbb76840756c6426d219cea6397ffebc4d36333323642e94ce4be16f096d61fd5918b7e9bf34de1198ca51383d9fcf4722116b3c3838a8602ee326e3cc20655678d45bc571a33a4ee191b412376fdeac95db5ef6b6758b8ddba9542a91482493c9e5e5e5aeaeaececece542ad5dddd7defdebd7c3e3f38386818c6fafa7a3c1e9f989800303232b2bebe6e1846b95c9e9a9a0a0402c3c3c3cf9e3debecec5c5a5a7afaf469341aedefef7ff6ec592291989d9dcd6432dddddd78b9ed9797973737374dd3b402f5cd9b3783c1e0850b176667677b7b7b53a9d483070f3a3a3a464646ac13e8d75f7fbdb8b878faf4e9eeeeeed9d9d9818181ebd7afefecec9c3b77ae582c562a15c3306edcb81108042e5ebc984c261389c4e2e2e237df7cb3b0b0108bc576d38ad527492693b76fdf7eefbdf7bc5e6f381c5e5b5b2b954a8d8d8d7ebfdf34cd5028944ea70921870e1d02502e97fd7ebf75248ac7e3c562d1e3f1689ab6babaaa695a3c1e374d331008944aa5f5f575afd71b8d46b7b6b6c2e1b0699ab95cceeff7d3a774feb85737085959c80f3ffc30168b8d8e8e82cfcfe1701880659b0556e8a71386aeeba07283dfefb70636c6aa28fc7ebf8db1d81a8661dda261bf0c96b1e29182f33380b9254c2fe2da9dfd52e67f0804feac695a7e07413f5a8208f9b1b6850603e90dac9b38de51b380f9559805f4579df3b0b685a7690cf748e6bc66d0344db0cefffc167ffe0a4d218403e48f7fc57c06df2ee0e6b7f5085858c3772f58e4fc2aae4d5661deff37be98a9877f7d20a8b785b05dc49f3ec786492eff484bc430f118b767e1f5e0ddb73078181f7c89700073cbf8e58f71630acb1b8804f1abb789a6691e0d00665fe01f0f6116307e1c3e1d1582bffc074f5670fe147c5ecc2e63218b67abf8f5e86b34d506363fb397d8bddc3471ee0422015cbb83950df2f1242e9ec6d95e7cf0252995f1348d4c0e577e824c0e332fc82fcee0d230823e6436b19845b184f7bf202d8df8cd18da9a01a05022fd6d48c4c8bfa671b8194d068ec4f1769f401359e1c550d6544708d6d9a3a15c268056ae00041e0f01702882ee98d61dc7adefc85a1e8460f20934e0581b0a2502e04407da9bb5e63019eec1c777c88689dffe0c0001b055c07611a73a497bb3460859cd21e0c3894eb26e2295268d06823e442338121797407419cb97a5323b1525b7a007d8db4a3e9fc1dfee92e54d8483686fc2dc12963770fd3eb99fc29116741e42c0475a1a106bc0d23a0c3f00681a08215b3b6869405f2baedd4166739761a381f626fcfd0116b3a4390caf074cb918f2e3e13c023efcb45fbc326a33dc03b1fb9e0cf4b7e3773fc3dc1212515c1a865f477f3be20d2894307c0463c760f8f18777712f85f426fa5a01e0fc29744501c0f08100cfd770e12d0cf7e0f91a123168c0efdfc1e413e477108bc0f0e3e7430070a405411f005c3e83e94544027bb4c82d88cf18fded5509261143225635adb51117de7a7539f2b2a7100a60fcf82b7ccfcb422612acc2c71b00e070330e3703407318a3c7f666873bb0bcc3e18c712041d533e0a1a607c404db9ad4da23280218008ff055884d218c93eab15a9e422d45d06606ee433483b18c65fb6144f40a42a88aa2c3ce68ac6685eaf3103d85375bb13c423e847b4d27a8b77949bc8ac24bc55cc78d2d5c4c97942e55a241dad3978974df0c5513bb6455934437b304670cc6871dd74de65dfc1e73dc380a03d4978e129929e233862232299e02efe1b2382433c006e15b0154fbb9b014e515e3175cd5d3af031431c691b37b4d14fd50e13b6326488bfb24f6ed5aa31194abaa662534b226896a5636b03509bf750905a87e10ea5bfcc08d83c8888594b2d023f3296be0b6a72f0445cc67b69f9beca098ae2693617826ce3d7d3721a7bed8eb1eea9ea800877a5bb8c11c93073d504454992c851ab28942e6321710bf7317ca10babada1eb5617406127a38136f154ad248fe7314c64104df0dd5172add10cb66a9f1c2bb0aa42224bfa1e7674dd36aabb70f06a86a121e0ec6a6d8ffbded26d4bbc4ec65a2021c6a4fc7f98a30f35a412d519df6a43d0326e22b3042497caab0d93a66323ed9287410466621c606b6a7ef32b5f2f26a4de90aa8bb387154db1ad716c30e06bc59b9ea0dad4960f5b77f681d54b0ef4b22fece809657c761c8657ee6918eb2147a3a626890f6f41d390ab302cd841913f97b02543f29863393ab847c140cc5b94a462d849ace898cd28e0be258e1a80984df87f12254719b3f79bac1086f29c8d4acdc4ca441d8f764081cf233a180c6d8630603e5daf273dd00bf503c4f75bc70d8db8cafca64bb0c1bea1d2e74395a0147ff97355278a134487bfafbdbdc77740a598fb2d6deab4c1c7dab9ef3b3cb05af35b5bac9672e27d6d6d3a7b9d0ae420f6c1ab5ebca064275d50385030b830b1f86681a1d12afa8c97b9928c0eb615f0a5f8bd37c64874d378f4cb62998c0b1d7f7cf32b71412bb11e186a0564a464387f718c27de272dfd2b71c770a2dae8e89ee850ad659180ff83d265c707040cb26ca6ebbd05914aa0bb5123a0e8db4c4e98a25521803778becc84aeda5b56ae588b4c61e1cf43e010dbbebfc46f549e0a6de3e90b0ffef315cbaf7fe92d534ab2a575548a55429952aa572e5d5bf71f22c1c4b05fa96ba5a5028ea183bdde82084aafef6d599ab9ea0c7eff5fb3cbe2b275efdea0ab828edb242724c39428c7da8e22732d98e4975f45d855655b5a7079e33ed670821c96cb2be4d2533460db212c091b3cbadc10862df634ca5a7b64bdbd66f9a7c6fb097e75b87a0aa5c5541a54ccacd81e662a5782093d66e4f9fc11a5e43f7bafda7abff47d098efb78f341d79a7ff1d00934b93c2097c99ada02135f600f71114e2d83346b15cbcb374c71a2866ba31c66519ef78f6e4c9dcb05200bbce639d638d8d8dd6a67793601ccf587c0aa19b7bc24f9b7829b6083e69f1b2641c68bce0ff6251d7595ca88db0a3488b90b51c1d99a87550436defe86a2aa7148d5897ac5097496ace823306b37569a09132013c99d04d985b0ab60cde0d46d696b3a1b6dfa171c4c8dad47be75387564226027ffede4a220508dfa2ec2373b6f6e49347ad69d6657e5663ea93e806a4ebcc640259caa127f24f473691be2b4c393673619253a7717e83f0a154fc7b800abe32a08d744cb6ea5bc2d8a398a5c6f3bb589c9ff9b13d5f1d9ce062a7edb108556be5a803b1be1b523c363ed3d4ba327c5a92f15173964d544c97cd95f6f479377063365db7ca28e94b756b9e375b58a8aa45d0970e3dfdfab285a347bd0e7a97776d02f6bde4c18637b4a70f3b6e472291f5f5f58f3efac8fa95cc8307f97c3e16dbfd1d8adddf0f03b0b2b2b2b0b0f0c369f5daa1a1a1c1fad948cd344de6ec4e0ff81a0892ef63644952588af2973c5e2691574c36b0e9515d26fe1752e41b3fba73e64f0000000049454e44ae426082,81,100,1296,1119524736);
INSERT INTO `$prefix$flip_content_image` VALUES (2,CURRENT_TIMESTAMP,3,'tournament_icon_NGL','NGL Logo','NGL',0,2,0xffd8ffe000104a46494600010201012c012c0000ffe1102c4578696600004d4d002a000000080007011200030000000100010000011a00050000000100000062011b0005000000010000006a01280003000000010002000001310002000000140000007201320002000000140000008687690004000000010000009c000000c80000012c000000010000012c0000000141646f62652050686f746f73686f7020372e3000323030333a31313a32362031393a33333a35310000000003a00100030000000100010000a002000400000001000000faa0030004000000010000006f0000000000000006010300030000000100060000011a00050000000100000116011b0005000000010000011e012800030000000100020000020100040000000100000126020200040000000100000efe0000000000000048000000010000004800000001ffd8ffe000104a46494600010201004800480000ffed000c41646f62655f434d0001ffee000e41646f626500648000000001ffdb0084000c08080809080c09090c110b0a0b11150f0c0c0f1518131315131318110c0c0c0c0c0c110c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c010d0b0b0d0e0d100e0e10140e0e0e14140e0e0e0e14110c0c0c0c0c11110c0c0c0c0c0c110c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0cffc00011080039008003012200021101031101ffdd00040008ffc4013f0000010501010101010100000000000000030001020405060708090a0b0100010501010101010100000000000000010002030405060708090a0b1000010401030204020507060805030c33010002110304211231054151611322718132061491a1b14223241552c16233347282d14307259253f0e1f163733516a2b283264493546445c2a3743617d255e265f2b384c3d375e3f3462794a485b495c4d4e4f4a5b5c5d5e5f55666768696a6b6c6d6e6f637475767778797a7b7c7d7e7f711000202010204040304050607070605350100021103213112044151617122130532819114a1b14223c152d1f0332462e1728292435315637334f1250616a2b283072635c2d2449354a317644555367465e2f2b384c3d375e3f34694a485b495c4d4e4f4a5b5c5d5e5f55666768696a6b6c6d6e6f62737475767778797a7b7c7ffda000c03010002110311003f00f5558dd6beb2e1f4b6ed27d4bcc86b06ba8ff5ff00ccd3fd65eb4de97844b75bacd18d1a1d78feaeeffc9af3ea28c9ea7936d965a2b6563d4cbcb7fd0aabeda7fd0c7c767f38a1cb94c4f0c7e62e9fc3fe1f1c9139f39e1c31d875c87f97f8eecb3ae7d64eb990fa70ac18ec634bec7cc358d027f4b746c66efea2a79acfad7825cfca76531adfa56b0ef60feb594eedbfdb45a33c7ecaead5e10763f4ea31db4575b88df6d99560a9f9595fbf90faebfa1fe02afd1a0e067db92d6623b29f879ec019859ed796ee03e8606739bfce55ff0071ee7ff35ff9f6026eae52323adde8ea42338199861c50c58c884b19871e58c7823938e538fe97eb3f59fcef07fac5617d6febb8c416e48caafbb2f01c0ffd719b6c6aecba0fd6ac1eb045047d9b3609343cc8701cba8b3fc2ff00e7cfe42e1f23a95c6f7d3d67029c9beb7117123ecd901dff008631218ffdefe65fea216563630c3aba9f4e7dd5b05fe8bd96b9bea537068bea7d5753b7731cdfe6ecda8c724a3b1e20378cbe65731c8e0ce0096218273f93361e1961948eb1f97838f8bfad861fdf7d6125c2e37f8c3bebc76332b17d5b9a21f6b5c1a1c47e7ecfcc73bf7513ff001c86ff00dc13fe7b54fefe3eff0083907e0fcf59031dd7512857e6f6c92e23ff001c91ff00704ff9e12ffc727fee81ff003c25efe3fde57fa1f9eff35ff3a1ff007cf6e92e1fff001caffba07fcf09bff1cbff00ba07fcf097de31fef7e0aff43f3dfe6bfe743fef9ee525c2ff00e399ff00744ff9e12ffc733fee89ff003c25f78c5fbc9ff4373ffe6bfe743fef9ee925c2ff00e399ff00740ff9e11b17fc64e33ef6332f11d4d0e203ee0e0ed9276ef733f398dfcfda90cf8ff796cbe11cf4412716dae9281ffba7ffd0d4fac66eeadd72ea85829c4c16cdf92ffe6ea69fa4f3fbf639bfccd3fe12c58f999b5db5b313118ea7a7d2edd5d6efa763fbe5e59fcfbdff0098cfa18eb57a7e453ff3933307306fc4ea6f7e2ded3c6e3fd1ecff008c6d9fa3affe3564752c0bfa666dd857c97547daf2237b0ff3570febb7ff00045467641977244bf607ace5a318ce18a5fe4f1c27823fa3c321c39727fb5f73fc4c7fdfc89b4afeae710fcccee7c6bc7aff00f4bd8b3dd0410750790b43a9eeab03a4629fcdc57649f8e4d8e7ff00d454d59c9b2debb00d9e5f58197efce72ff078cc71ff00e37183a74dccead5d78596f15e7d60330b31e60583f370b31fff00b6d7ff00adadd36ab5ccea9d1ee61aefb69f5595b81dc2fc43eb8ac0fdeb69362cb7004104483c85b9d23a9332b3b0fed0f0deab8af68c4c82e8fb432763f0329fafe99d539f5e2e43bfe2adff00843120917bedfde0c59e12c78e6602f1ff0039c3fe672c0fb909c7fd57b91fd6c3fc36afd58aa8caeb9894dec6db4dbb8398e1208da5cbbf3f57fa0030706893db6ae3ba4609e9df5e1b8510da6eb3d3133fa3731d6d1c7fc13d8bb0ebc5cde93d45ed25af6e25a5ae06083b4f0429f0fa719b1b48fe0e2fc54fbbce62e09111cb8f1f09f0c92970c997fcdce85ff7029ff3151ea3f52fa165d459453f63b8fd0ba99107b6fa8fe8ec67ef2e0713ad754c0b99934655a5d590e2c7d8f731e0735db5b9db5cc72f5b63839ad7b78700e1f3129d8e71ca0dc7662e6f97cff0fc98cc73126766328dc7e4f984a3fe13e73f57fea864f52b9eecc268c6a1eeaec2de5cf61daf6572bafa3eaa7d5ba1a1830d96b87e7592f715a6f1b436aa8066f2498103dc773ddfda71dee5e6dd67eb3f50ea596eaf0afb31f083cb28aea7161789da2dbdedd8f7bedff47fcdd69a4c3100046cff00ce93243ef7f12cb3272fb78e038a5a98e1c51fd1f4fef7f59eeffe6cf403ff0079d4ff0098542efab5f57dac9fd9f47f9a7fbd7007a77d6bd7f57ea1ff0082ff00e497a17491923a1610ca0f1902960b45b21fb808fd26ef76e4e8ccc84ae06143ab5f9be5e38040c3998731c44dfb72be0e1fdee19cfe67cf7eb5d34e3fd61cca68636aaab358631821a07a5570d0b1eefe66cfeaadbfae3ff8a7cffeb57ff9eaa58977f3367f55539fcf2fef1fcdeab95ff7261ff638ff00f49c5fffd19fd64c67e2f5dccac1d84d82eadc3b07c58c70fea3d68f586bfaf605591536735d43b2318346afda7d2ea980d93bdefc5c8fd6f1ff00e3bd2a96b7d7be8cfc8c76755a1bbacc5696e43409269fa5bffeb0ef7ffc5fa8b9ff00ab995419c7bec0c7e2da33f07decafd4b5ad7556e17af6fb5adcafd0ff00e08a9ca3c33940ed37a5c79bdde530f338ff009de5c70cba9bae090ff0ff0057c7fea7dc41d6aab72bae646360d56648c515e335b534be052c656e0ed9f47f4bea20bba3db418ea5938fd3b89aec7fab741ffba98bead9fe7ad5ea97750cd67a5d49f7743bcfb4d6e2e1d3ee713cfaf4ff0033658e7ff8676452b9ecac2c9c0b0559349a5cf1b98742d783aefa6d67e8ee67f51c9b30012689b37ae835feafcccfcae494e10c632c71f0c442a1c39724f807cf8f3cbf5193fadc18723677f41c73edab23a93c4cbad70c5abc8b6aa7d5c977f6ecad68f4eeb391858b67536d58d898ed269c2c5a2a0df5ef8d5f75aff5326cc7c4fe76efd2fbedd95ac7c0c27e7e50a03fd2a98d36e4de78aa966b6da7fef9ff00089753cf665dcd35b7d0c2c768a70e9740d950e37ffc3dcefd2deefdff00ea202440e2dba4697e4e5f1e598c46f2554f34b248e4e18fe8e3847f9bc7932ffab843f55fdfc4dffaa963ecfad189658e2fb2c7d8fb1e79739cd7b9ef77f59cbbafac1ff23f53ff00c296ff00d495e63d3baa1e9b9b5e7506ab2daa7636c77b648dbbbf46f6396b65fd7bea5978b7e2db5e1b59935baa7b9bbf706bc6d76ddd739bb93a196231989decb579df87e7cdce63cd8c47db808036787f9b9f11f4bcfbcfb0fc17b3d1fd1eafea37fea42f172fa8882f691dc6e1af972ba56ff8c4eaed686b6ac20d68000fd270040ff0e960cb18717175a5ff0017e4737352c471089f6c4f8b8a5c3f3f057fd07d11cd97b4f1c891e6bc7463d9899adc6cafd0bf1ee0cb8905c1bb1def74325cf6fe77b16fff00e38bd63fd1617fe09ffa5d64f56eba7abddf68c9a71aac8801d750e734b9adfa2db1afb6c63ffe33f9c472e484a8c4eb137aacf86723ccf2feec32c63ede78f0ca509478e15c5ffab1ef8fd7afab6493f6a7c7fc4dbff905af8f9146762d59543bd4a6f68b2b76a241f22bc7a8c9c6a9e5d65756483c36cb1ed03cff0057b2a72e82aff183d4e9ad9555460b2bac06b18dde000340d6817a74399d0f1d7900d7e6be092062395048fd29659c35edc118c5deeb7f527f69f52bfa88cef47d7da4d66add1b5adafe9fa8cfdcfdd5c87d67e8ade8b737105c720d98c2e7585bb44b9f6b36b592ef6eda9bf9cb4cff008c4eae447a585ff827fe97591d57aa66fd63cea87a753b32d637168a71e608dce7873b7becdbb7d47ef7a66496397ca3d44b6b92c3cfe197f48c83eef0818f0f146a3c23d1fe2bffd2f555c3fd62fa8d66f7e5746635f53f57e09204127ddf6573bf47b3f3bd0b3feb4fff0002bb849459bdbe1f5fd2be6fa37fe17f7bf7bfa2f87b9c7fccf0f4f73ff41fd63e3d567f51e9569a6bbeec07b75763bc960d7f7f1723f44fff00b6d6851f5b5ecc4fb167e161e661924b981a283b8fe7b7d3dd431ffcaae9ad769f5cbfe45b3e21790d1fd39bfd655a3c5fa0655e23d3ff007aeee53cbd7f4b8e1196c7f37294b2f17e8fc908e7feebd0bb2a8cca4f4bfab9879360bde2dcc6922c7120feaf8e6faff46dc2c777e93d4bdffce7f5174df56bea18a6c6e775d0cbae001a7087baaac9e5d74fb722ff00fc06bff85fd1fa7d2741ff0092317fa81682931fb7c438be6af4edc1fe0f0b4b9a3ce1e5e5ec011c5c47dde19649f35c5fa5ef7bd1864fddff00a9ff0051c6ea9d3ba361e2facde9f8409b2bacbeca5818d1658ca5d63e1bfe0f7ee596db3a7801efe918676b4b9f53681ea1a98df59f9f53767f45b5adf471d8f6eff5f67f84fd12dfeaff00d1e9ff00c358dff9faa5794b2abd3f639d84cb83d409959f98e48e9fe0c64f2b67d8d8435dd1b0aa707fa6ef52bac377c7acdabd57b6b6fe9e9b71bd377fa4f5d9e95be92560c265ac69e8d8643f7398df498d73c35efa7d0abd46b37e4b595face632bb3f9caeaffbb0baa490fe5fa2becfee8f1f567ffbc794af1f06de9d9f94cc5c27ba92cf42caf1d86b6835d563da3754e7d9b1efb3d5ff00d17fcda3e251d3afc9a68fd9584e6ddbdccbd95376baaa9cfaadb980b3f3acfb2fa5eff7fda3d4fd232b5d22a0dff972cffc2acffcf9623a58448cb86562bf76a5232f923fb9170acc2afd6b69aba7e33dd65b79c5fd56b0d8a1a7f54b5de9ff0085b1ac7d577fa3b6dff408775380436f6e363d15e4556dd55471a996d4db31a9a6f3ea52e7eef41f9193efff00b6ff0042bb0490d35d4b2033b8fa63e3eaff00a5c50fee3c81a7a7d36bbf55c7c8633ed1f666fd96a69c92d18fe80f6d3eefd62fba865947a35dbe9fa8b67a06263d343b762d74e6d0e7517dada6ba5cf887b2cfd06e66dbaa7556ed63f62d6493a35c4c3cc19fb5a8036e2e195cbf4b7e18f0ff007ffea7feadffd9ffed151050686f746f73686f7020332e30003842494d0425000000000010000000000000000000000000000000003842494d03ed000000000010012c000000010002012c0000000100023842494d042600000000000e000000000000000000003f8000003842494d040d000000000004fffffffb3842494d04190000000000040000001e3842494d03f3000000000009000000000000000001003842494d040a00000000000100003842494d271000000000000a000100000000000000023842494d03f5000000000048002f66660001006c66660006000000000001002f6666000100a1999a0006000000000001003200000001005a00000006000000000001003500000001002d000000060000000000013842494d03f80000000000700000ffffffffffffffffffffffffffffffffffffffffffff03e800000000ffffffffffffffffffffffffffffffffffffffffffff03e800000000ffffffffffffffffffffffffffffffffffffffffffff03e800000000ffffffffffffffffffffffffffffffffffffffffffff03e800003842494d040000000000000200053842494d040200000000000c0000000000000000000000003842494d040800000000002900000001000002400000024000000005000000000100000de001000000000000001f40000000000001003842494d041e000000000004000000003842494d041a00000000035f0000000600000000000000000000006f000000fa00000015006e0067006c0020006600fc00720020006800700020006d006900740020006e007600690064006900610000000100000000000000000000000000000000000000010000000000000000000000fa0000006f00000000000000000000000000000000010000000000000000000000000000000000000010000000010000000000006e756c6c0000000200000006626f756e64734f626a6300000001000000000000526374310000000400000000546f70206c6f6e6700000000000000004c6566746c6f6e67000000000000000042746f6d6c6f6e670000006f00000000526768746c6f6e67000000fa00000006736c69636573566c4c73000000014f626a6300000001000000000005736c6963650000001200000007736c69636549446c6f6e67000000000000000767726f757049446c6f6e6700000000000000066f726967696e656e756d0000000c45536c6963654f726967696e0000000d6175746f47656e6572617465640000000054797065656e756d0000000a45536c6963655479706500000000496d672000000006626f756e64734f626a6300000001000000000000526374310000000400000000546f70206c6f6e6700000000000000004c6566746c6f6e67000000000000000042746f6d6c6f6e670000006f00000000526768746c6f6e67000000fa0000000375726c54455854000000010000000000006e756c6c54455854000000010000000000004d7367655445585400000001000000000006616c74546167544558540000000100000000000e63656c6c54657874497348544d4c626f6f6c010000000863656c6c546578745445585400000001000000000009686f727a416c69676e656e756d0000000f45536c696365486f727a416c69676e0000000764656661756c740000000976657274416c69676e656e756d0000000f45536c69636556657274416c69676e0000000764656661756c740000000b6267436f6c6f7254797065656e756d0000001145536c6963654247436f6c6f7254797065000000004e6f6e6500000009746f704f75747365746c6f6e67000000000000000a6c6566744f75747365746c6f6e67000000000000000c626f74746f6d4f75747365746c6f6e67000000000000000b72696768744f75747365746c6f6e6700000000003842494d0414000000000004000000323842494d040c000000000f1a000000010000008000000039000001800000558000000efe00180001ffd8ffe000104a46494600010201004800480000ffed000c41646f62655f434d0001ffee000e41646f626500648000000001ffdb0084000c08080809080c09090c110b0a0b11150f0c0c0f1518131315131318110c0c0c0c0c0c110c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c010d0b0b0d0e0d100e0e10140e0e0e14140e0e0e0e14110c0c0c0c0c11110c0c0c0c0c0c110c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0cffc00011080039008003012200021101031101ffdd00040008ffc4013f0000010501010101010100000000000000030001020405060708090a0b0100010501010101010100000000000000010002030405060708090a0b1000010401030204020507060805030c33010002110304211231054151611322718132061491a1b14223241552c16233347282d14307259253f0e1f163733516a2b283264493546445c2a3743617d255e265f2b384c3d375e3f3462794a485b495c4d4e4f4a5b5c5d5e5f55666768696a6b6c6d6e6f637475767778797a7b7c7d7e7f711000202010204040304050607070605350100021103213112044151617122130532819114a1b14223c152d1f0332462e1728292435315637334f1250616a2b283072635c2d2449354a317644555367465e2f2b384c3d375e3f34694a485b495c4d4e4f4a5b5c5d5e5f55666768696a6b6c6d6e6f62737475767778797a7b7c7ffda000c03010002110311003f00f5558dd6beb2e1f4b6ed27d4bcc86b06ba8ff5ff00ccd3fd65eb4de97844b75bacd18d1a1d78feaeeffc9af3ea28c9ea7936d965a2b6563d4cbcb7fd0aabeda7fd0c7c767f38a1cb94c4f0c7e62e9fc3fe1f1c9139f39e1c31d875c87f97f8eecb3ae7d64eb990fa70ac18ec634bec7cc358d027f4b746c66efea2a79acfad7825cfca76531adfa56b0ef60feb594eedbfdb45a33c7ecaead5e10763f4ea31db4575b88df6d99560a9f9595fbf90faebfa1fe02afd1a0e067db92d6623b29f879ec019859ed796ee03e8606739bfce55ff0071ee7ff35ff9f6026eae52323adde8ea42338199861c50c58c884b19871e58c7823938e538fe97eb3f59fcef07fac5617d6febb8c416e48caafbb2f01c0ffd719b6c6aecba0fd6ac1eb045047d9b3609343cc8701cba8b3fc2ff00e7cfe42e1f23a95c6f7d3d67029c9beb7117123ecd901dff008631218ffdefe65fea216563630c3aba9f4e7dd5b05fe8bd96b9bea537068bea7d5753b7731cdfe6ecda8c724a3b1e20378cbe65731c8e0ce0096218273f93361e1961948eb1f97838f8bfad861fdf7d6125c2e37f8c3bebc76332b17d5b9a21f6b5c1a1c47e7ecfcc73bf7513ff001c86ff00dc13fe7b54fefe3eff0083907e0fcf59031dd7512857e6f6c92e23ff001c91ff00704ff9e12ffc727fee81ff003c25efe3fde57fa1f9eff35ff3a1ff007cf6e92e1fff001caffba07fcf09bff1cbff00ba07fcf097de31fef7e0aff43f3dfe6bfe743fef9ee525c2ff00e399ff00744ff9e12ffc733fee89ff003c25f78c5fbc9ff4373ffe6bfe743fef9ee925c2ff00e399ff00740ff9e11b17fc64e33ef6332f11d4d0e203ee0e0ed9276ef733f398dfcfda90cf8ff796cbe11cf4412716dae9281ffba7ffd0d4fac66eeadd72ea85829c4c16cdf92ffe6ea69fa4f3fbf639bfccd3fe12c58f999b5db5b313118ea7a7d2edd5d6efa763fbe5e59fcfbdff0098cfa18eb57a7e453ff3933307306fc4ea6f7e2ded3c6e3fd1ecff008c6d9fa3affe3564752c0bfa666dd857c97547daf2237b0ff3570febb7ff00045467641977244bf607ace5a318ce18a5fe4f1c27823fa3c321c39727fb5f73fc4c7fdfc89b4afeae710fcccee7c6bc7aff00f4bd8b3dd0410750790b43a9eeab03a4629fcdc57649f8e4d8e7ff00d454d59c9b2debb00d9e5f58197efce72ff078cc71ff00e37183a74dccead5d78596f15e7d60330b31e60583f370b31fff00b6d7ff00adadd36ab5ccea9d1ee61aefb69f5595b81dc2fc43eb8ac0fdeb69362cb7004104483c85b9d23a9332b3b0fed0f0deab8af68c4c82e8fb432763f0329fafe99d539f5e2e43bfe2adff00843120917bedfde0c59e12c78e6602f1ff0039c3fe672c0fb909c7fd57b91fd6c3fc36afd58aa8caeb9894dec6db4dbb8398e1208da5cbbf3f57fa0030706893db6ae3ba4609e9df5e1b8510da6eb3d3133fa3731d6d1c7fc13d8bb0ebc5cde93d45ed25af6e25a5ae06083b4f0429f0fa719b1b48fe0e2fc54fbbce62e09111cb8f1f09f0c92970c997fcdce85ff7029ff3151ea3f52fa165d459453f63b8fd0ba99107b6fa8fe8ec67ef2e0713ad754c0b99934655a5d590e2c7d8f731e0735db5b9db5cc72f5b63839ad7b78700e1f3129d8e71ca0dc7662e6f97cff0fc98cc73126766328dc7e4f984a3fe13e73f57fea864f52b9eecc268c6a1eeaec2de5cf61daf6572bafa3eaa7d5ba1a1830d96b87e7592f715a6f1b436aa8066f2498103dc773ddfda71dee5e6dd67eb3f50ea596eaf0afb31f083cb28aea7161789da2dbdedd8f7bedff47fcdd69a4c3100046cff00ce93243ef7f12cb3272fb78e038a5a98e1c51fd1f4fef7f59eeffe6cf403ff0079d4ff0098542efab5f57dac9fd9f47f9a7fbd7007a77d6bd7f57ea1ff0082ff00e497a17491923a1610ca0f1902960b45b21fb808fd26ef76e4e8ccc84ae06143ab5f9be5e38040c3998731c44dfb72be0e1fdee19cfe67cf7eb5d34e3fd61cca68636aaab358631821a07a5570d0b1eefe66cfeaadbfae3ff8a7cffeb57ff9eaa58977f3367f55539fcf2fef1fcdeab95ff7261ff638ff00f49c5fffd19fd64c67e2f5dccac1d84d82eadc3b07c58c70fea3d68f586bfaf605591536735d43b2318346afda7d2ea980d93bdefc5c8fd6f1ff00e3bd2a96b7d7be8cfc8c76755a1bbacc5696e43409269fa5bffeb0ef7ffc5fa8b9ff00ab995419c7bec0c7e2da33f07decafd4b5ad7556e17af6fb5adcafd0ff00e08a9ca3c33940ed37a5c79bdde530f338ff009de5c70cba9bae090ff0ff0057c7fea7dc41d6aab72bae646360d56648c515e335b534be052c656e0ed9f47f4bea20bba3db418ea5938fd3b89aec7fab741ffba98bead9fe7ad5ea97750cd67a5d49f7743bcfb4d6e2e1d3ee713cfaf4ff0033658e7ff8676452b9ecac2c9c0b0559349a5cf1b98742d783aefa6d67e8ee67f51c9b30012689b37ae835feafcccfcae494e10c632c71f0c442a1c39724f807cf8f3cbf5193fadc18723677f41c73edab23a93c4cbad70c5abc8b6aa7d5c977f6ecad68f4eeb391858b67536d58d898ed269c2c5a2a0df5ef8d5f75aff5326cc7c4fe76efd2fbedd95ac7c0c27e7e50a03fd2a98d36e4de78aa966b6da7fef9ff00089753cf665dcd35b7d0c2c768a70e9740d950e37ffc3dcefd2deefdff00ea202440e2dba4697e4e5f1e598c46f2554f34b248e4e18fe8e3847f9bc7932ffab843f55fdfc4dffaa963ecfad189658e2fb2c7d8fb1e79739cd7b9ef77f59cbbafac1ff23f53ff00c296ff00d495e63d3baa1e9b9b5e7506ab2daa7636c77b648dbbbf46f6396b65fd7bea5978b7e2db5e1b59935baa7b9bbf706bc6d76ddd739bb93a196231989decb579df87e7cdce63cd8c47db808036787f9b9f11f4bcfbcfb0fc17b3d1fd1eafea37fea42f172fa8882f691dc6e1af972ba56ff8c4eaed686b6ac20d68000fd270040ff0e960cb18717175a5ff0017e4737352c471089f6c4f8b8a5c3f3f057fd07d11cd97b4f1c891e6bc7463d9899adc6cafd0bf1ee0cb8905c1bb1def74325cf6fe77b16fff00e38bd63fd1617fe09ffa5d64f56eba7abddf68c9a71aac8801d750e734b9adfa2db1afb6c63ffe33f9c472e484a8c4eb137aacf86723ccf2feec32c63ede78f0ca509478e15c5ffab1ef8fd7afab6493f6a7c7fc4dbff905af8f9146762d59543bd4a6f68b2b76a241f22bc7a8c9c6a9e5d65756483c36cb1ed03cff0057b2a72e82aff183d4e9ad9555460b2bac06b18dde000340d6817a74399d0f1d7900d7e6be092062395048fd29659c35edc118c5deeb7f527f69f52bfa88cef47d7da4d66add1b5adafe9fa8cfdcfdd5c87d67e8ade8b737105c720d98c2e7585bb44b9f6b36b592ef6eda9bf9cb4cff008c4eae447a585ff827fe97591d57aa66fd63cea87a753b32d637168a71e608dce7873b7becdbb7d47ef7a66496397ca3d44b6b92c3cfe197f48c83eef0818f0f146a3c23d1fe2bffd2f555c3fd62fa8d66f7e5746635f53f57e09204127ddf6573bf47b3f3bd0b3feb4fff0002bb849459bdbe1f5fd2be6fa37fe17f7bf7bfa2f87b9c7fccf0f4f73ff41fd63e3d567f51e9569a6bbeec07b75763bc960d7f7f1723f44fff00b6d6851f5b5ecc4fb167e161e661924b981a283b8fe7b7d3dd431ffcaae9ad769f5cbfe45b3e21790d1fd39bfd655a3c5fa0655e23d3ff007aeee53cbd7f4b8e1196c7f37294b2f17e8fc908e7feebd0bb2a8cca4f4bfab9879360bde2dcc6922c7120feaf8e6faff46dc2c777e93d4bdffce7f5174df56bea18a6c6e775d0cbae001a7087baaac9e5d74fb722ff00fc06bff85fd1fa7d2741ff0092317fa81682931fb7c438be6af4edc1fe0f0b4b9a3ce1e5e5ec011c5c47dde19649f35c5fa5ef7bd1864fddff00a9ff0051c6ea9d3ba361e2facde9f8409b2bacbeca5818d1658ca5d63e1bfe0f7ee596db3a7801efe918676b4b9f53681ea1a98df59f9f53767f45b5adf471d8f6eff5f67f84fd12dfeaff00d1e9ff00c358dff9faa5794b2abd3f639d84cb83d409959f98e48e9fe0c64f2b67d8d8435dd1b0aa707fa6ef52bac377c7acdabd57b6b6fe9e9b71bd377fa4f5d9e95be92560c265ac69e8d8643f7398df498d73c35efa7d0abd46b37e4b595face632bb3f9caeaffbb0baa490fe5fa2becfee8f1f567ffbc794af1f06de9d9f94cc5c27ba92cf42caf1d86b6835d563da3754e7d9b1efb3d5ff00d17fcda3e251d3afc9a68fd9584e6ddbdccbd95376baaa9cfaadb980b3f3acfb2fa5eff7fda3d4fd232b5d22a0dff972cffc2acffcf9623a58448cb86562bf76a5232f923fb9170acc2afd6b69aba7e33dd65b79c5fd56b0d8a1a7f54b5de9ff0085b1ac7d577fa3b6dff408775380436f6e363d15e4556dd55471a996d4db31a9a6f3ea52e7eef41f9193efff00b6ff0042bb0490d35d4b2033b8fa63e3eaff00a5c50fee3c81a7a7d36bbf55c7c8633ed1f666fd96a69c92d18fe80f6d3eefd62fba865947a35dbe9fa8b67a06263d343b762d74e6d0e7517dada6ba5cf887b2cfd06e66dbaa7556ed63f62d6493a35c4c3cc19fb5a8036e2e195cbf4b7e18f0ff007ffea7feadffd93842494d042100000000005500000001010000000f00410064006f00620065002000500068006f0074006f00730068006f00700000001300410064006f00620065002000500068006f0074006f00730068006f007000200037002e003000000001003842494d04060000000000070008000000010100ffe11248687474703a2f2f6e732e61646f62652e636f6d2f7861702f312e302f003c3f787061636b657420626567696e3d27efbbbf272069643d2757354d304d7043656869487a7265537a4e54637a6b633964273f3e0a3c3f61646f62652d7861702d66696c74657273206573633d224352223f3e0a3c783a7861706d65746120786d6c6e733a783d2761646f62653a6e733a6d6574612f2720783a786170746b3d27584d5020746f6f6c6b697420322e382e322d33332c206672616d65776f726b20312e35273e0a3c7264663a52444620786d6c6e733a7264663d27687474703a2f2f7777772e77332e6f72672f313939392f30322f32322d7264662d73796e7461782d6e73232720786d6c6e733a69583d27687474703a2f2f6e732e61646f62652e636f6d2f69582f312e302f273e0a0a203c7264663a4465736372697074696f6e2061626f75743d27757569643a31366562653630622d323033352d313164382d613530662d656335623830333564623935270a2020786d6c6e733a7861704d4d3d27687474703a2f2f6e732e61646f62652e636f6d2f7861702f312e302f6d6d2f273e0a20203c7861704d4d3a446f63756d656e7449443e61646f62653a646f6369643a70686f746f73686f703a31366562653630392d323033352d313164382d613530662d6563356238303335646239353c2f7861704d4d3a446f63756d656e7449443e0a203c2f7264663a4465736372697074696f6e3e0a0a3c2f7264663a5244463e0a3c2f783a7861706d6574613e0a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020200a3c3f787061636b657420656e643d2777273f3effe20c584943435f50524f46494c4500010100000c484c696e6f021000006d6e74725247422058595a2007ce00020009000600310000616373704d5346540000000049454320735247420000000000000000000000010000f6d6000100000000d32d4850202000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001163707274000001500000003364657363000001840000006c77747074000001f000000014626b707400000204000000147258595a00000218000000146758595a0000022c000000146258595a0000024000000014646d6e640000025400000070646d6464000002c400000088767565640000034c0000008676696577000003d4000000246c756d69000003f8000000146d6561730000040c0000002474656368000004300000000c725452430000043c0000080c675452430000043c0000080c625452430000043c0000080c7465787400000000436f70797269676874202863292031393938204865776c6574742d5061636b61726420436f6d70616e790000646573630000000000000012735247422049454336313936362d322e31000000000000000000000012735247422049454336313936362d322e31000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000058595a20000000000000f35100010000000116cc58595a200000000000000000000000000000000058595a200000000000006fa2000038f50000039058595a2000000000000062990000b785000018da58595a2000000000000024a000000f840000b6cf64657363000000000000001649454320687474703a2f2f7777772e6965632e636800000000000000000000001649454320687474703a2f2f7777772e6965632e63680000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000064657363000000000000002e4945432036313936362d322e312044656661756c742052474220636f6c6f7572207370616365202d207352474200000000000000000000002e4945432036313936362d322e312044656661756c742052474220636f6c6f7572207370616365202d20735247420000000000000000000000000000000000000000000064657363000000000000002c5265666572656e63652056696577696e6720436f6e646974696f6e20696e2049454336313936362d322e3100000000000000000000002c5265666572656e63652056696577696e6720436f6e646974696f6e20696e2049454336313936362d322e31000000000000000000000000000000000000000000000000000076696577000000000013a4fe00145f2e0010cf140003edcc0004130b00035c9e0000000158595a2000000000004c09560050000000571fe76d6561730000000000000001000000000000000000000000000000000000028f0000000273696720000000004352542063757276000000000000040000000005000a000f00140019001e00230028002d00320037003b00400045004a004f00540059005e00630068006d00720077007c00810086008b00900095009a009f00a400a900ae00b200b700bc00c100c600cb00d000d500db00e000e500eb00f000f600fb01010107010d01130119011f0125012b01320138013e0145014c0152015901600167016e0175017c0183018b0192019a01a101a901b101b901c101c901d101d901e101e901f201fa0203020c0214021d0226022f02380241024b0254025d02670271027a0284028e029802a202ac02b602c102cb02d502e002eb02f50300030b03160321032d03380343034f035a03660372037e038a039603a203ae03ba03c703d303e003ec03f9040604130420042d043b0448045504630471047e048c049a04a804b604c404d304e104f004fe050d051c052b053a05490558056705770586059605a605b505c505d505e505f6060606160627063706480659066a067b068c069d06af06c006d106e306f507070719072b073d074f076107740786079907ac07bf07d207e507f8080b081f08320846085a086e0882089608aa08be08d208e708fb09100925093a094f09640979098f09a409ba09cf09e509fb0a110a270a3d0a540a6a0a810a980aae0ac50adc0af30b0b0b220b390b510b690b800b980bb00bc80be10bf90c120c2a0c430c5c0c750c8e0ca70cc00cd90cf30d0d0d260d400d5a0d740d8e0da90dc30dde0df80e130e2e0e490e640e7f0e9b0eb60ed20eee0f090f250f410f5e0f7a0f960fb30fcf0fec1009102610431061107e109b10b910d710f511131131114f116d118c11aa11c911e81207122612451264128412a312c312e31303132313431363138313a413c513e5140614271449146a148b14ad14ce14f01512153415561578159b15bd15e0160316261649166c168f16b216d616fa171d17411765178917ae17d217f7181b18401865188a18af18d518fa19201945196b199119b719dd1a041a2a1a511a771a9e1ac51aec1b141b3b1b631b8a1bb21bda1c021c2a1c521c7b1ca31ccc1cf51d1e1d471d701d991dc31dec1e161e401e6a1e941ebe1ee91f131f3e1f691f941fbf1fea20152041206c209820c420f0211c2148217521a121ce21fb22272255228222af22dd230a23382366239423c223f0241f244d247c24ab24da250925382568259725c725f726272657268726b726e827182749277a27ab27dc280d283f287128a228d429062938296b299d29d02a022a352a682a9b2acf2b022b362b692b9d2bd12c052c392c6e2ca22cd72d0c2d412d762dab2de12e162e4c2e822eb72eee2f242f5a2f912fc72ffe3035306c30a430db3112314a318231ba31f2322a3263329b32d4330d3346337f33b833f1342b3465349e34d83513354d358735c235fd3637367236ae36e937243760379c37d738143850388c38c839053942397f39bc39f93a363a743ab23aef3b2d3b6b3baa3be83c273c653ca43ce33d223d613da13de03e203e603ea03ee03f213f613fa23fe24023406440a640e74129416a41ac41ee4230427242b542f7433a437d43c044034447448a44ce45124555459a45de4622466746ab46f04735477b47c04805484b489148d7491d496349a949f04a374a7d4ac44b0c4b534b9a4be24c2a4c724cba4d024d4a4d934ddc4e254e6e4eb74f004f494f934fdd5027507150bb51065150519b51e65231527c52c75313535f53aa53f65442548f54db5528557555c2560f565c56a956f75744579257e0582f587d58cb591a596959b85a075a565aa65af55b455b955be55c355c865cd65d275d785dc95e1a5e6c5ebd5f0f5f615fb36005605760aa60fc614f61a261f56249629c62f06343639763eb6440649464e9653d659265e7663d669266e8673d679367e9683f689668ec6943699a69f16a486a9f6af76b4f6ba76bff6c576caf6d086d606db96e126e6b6ec46f1e6f786fd1702b708670e0713a719571f0724b72a67301735d73b87414747074cc7528758575e1763e769b76f8775677b37811786e78cc792a798979e77a467aa57b047b637bc27c217c817ce17d417da17e017e627ec27f237f847fe5804780a8810a816b81cd8230829282f4835783ba841d848084e3854785ab860e867286d7873b879f8804886988ce8933899989fe8a648aca8b308b968bfc8c638cca8d318d988dff8e668ece8f368f9e9006906e90d6913f91a89211927a92e3934d93b69420948a94f4955f95c99634969f970a977597e0984c98b89924999099fc9a689ad59b429baf9c1c9c899cf79d649dd29e409eae9f1d9f8b9ffaa069a0d8a147a1b6a226a296a306a376a3e6a456a4c7a538a5a9a61aa68ba6fda76ea7e0a852a8c4a937a9a9aa1caa8fab02ab75abe9ac5cacd0ad44adb8ae2daea1af16af8bb000b075b0eab160b1d6b24bb2c2b338b3aeb425b49cb513b58ab601b679b6f0b768b7e0b859b8d1b94ab9c2ba3bbab5bb2ebba7bc21bc9bbd15bd8fbe0abe84beffbf7abff5c070c0ecc167c1e3c25fc2dbc358c3d4c451c4cec54bc5c8c646c6c3c741c7bfc83dc8bcc93ac9b9ca38cab7cb36cbb6cc35ccb5cd35cdb5ce36ceb6cf37cfb8d039d0bad13cd1bed23fd2c1d344d3c6d449d4cbd54ed5d1d655d6d8d75cd7e0d864d8e8d96cd9f1da76dafbdb80dc05dc8add10dd96de1cdea2df29dfafe036e0bde144e1cce253e2dbe363e3ebe473e4fce584e60de696e71fe7a9e832e8bce946e9d0ea5beae5eb70ebfbec86ed11ed9cee28eeb4ef40efccf058f0e5f172f1fff28cf319f3a7f434f4c2f550f5def66df6fbf78af819f8a8f938f9c7fa57fae7fb77fc07fc98fd29fdbafe4bfedcff6dffffffee000e41646f626500644000000001ffdb008400010101010101010101010101010101010101010101010101010101010101010101010101010101010101010202020202020202020202030303030303030303030101010101010101010101020201020203030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303ffc0001108006f00fa03011100021101031101ffdd00040020ffc401a20000000602030100000000000000000000070806050409030a0201000b0100000603010101000000000000000000060504030702080109000a0b1000020103040103030203030302060975010203041105120621071322000831144132231509514216612433175271811862912543a1b1f02634720a19c1d13527e1533682f192a24454734546374763285556571ab2c2d2e2f2648374938465a3b3c3d3e3293866f3752a393a48494a58595a6768696a767778797a85868788898a9495969798999aa4a5a6a7a8a9aab4b5b6b7b8b9bac4c5c6c7c8c9cad4d5d6d7d8d9dae4e5e6e7e8e9eaf4f5f6f7f8f9fa110002010302040403050404040606056d010203110421120531060022134151073261147108428123911552a162163309b124c1d14372f017e18234259253186344f1a2b226351954364564270a7383934674c2d2e2f255657556378485a3b3c3d3e3f3291a94a4b4c4d4e4f495a5b5c5d5e5f52847576638768696a6b6c6d6e6f667778797a7b7c7d7e7f7485868788898a8b8c8d8e8f839495969798999a9b9c9d9e9f92a3a4a5a6a7a8a9aaabacadaeafaffda000c03010002110311003f00dfe3dfbaf75ef7eebdd7bdfbaf741876377175ff005652a4dbbb39141593aeaa3c2d12fdf672b07faa871d0b79121278f2ca6386fc6bbfb417bb9d9edea0dccb463c14658fe5fe5341d0d7947dbee6be77999361db19add4f7ccfd90a7c8c8704ff45753533a69d54f7c81fe6e5b3badeaea7198ba9c2e1aa92eb1e2d207de5bd7942c92d4e331d51161f07ac1042d5bb8bff69bdc75bcfb916960cd1a322b7a7c6ff98074afe7d671fb5df712def9aa086f3704b9b988e4c9516969c721647532cd4f58c0fb075541d9ff00ce1bb8f73cf345b621dc2d4c259447559fdcb361609236b6871b7f66263e9e3ff82bd4c961f9bf3ee3adc3dcedce7245babe9f566d3ff194a0fe67acdde4efb86fb7bb2c71beec2d3c5a0aac36e26607cc78f77e231fb446b5e8b0e5bf98bfc8dcb4c93cb57b595a291a584d463f3593786462a7c91cb92dc15322c9e8176041361fd07b20939e37d90825a3c7c98ff858f531d8fdd57da8b08da38edef34b0a1d2f14608f422385453271f3e9eb0dfcd03e59e0a78a7a4dd78ed5132b2a45fde3a24b824f228b724208b9fa1047fb73edc8bdc0e6485815b85c7fa61fe06e8b770fb9d7b1fb9c4f1dc6c5251852a440e7fe3701e8e1f55ff3d5ef3db53414fd93b3b0dbbb1e26884b5143228aa8e9cf133882714555348072a1ab7fdbdfd89f6ef777768085beb55912bc471a7e7427fdeba80f9dbfbb67db3de23966e52df6e2c2eb49a06f84b790a8d6a07ad22eae4be367f364f8aff20aaa8b6ed4ee53d71bd2ada1822c26f11fc369abaa5e22ec28321388e092ee8c2d7654e2ee6f7f727ec5ee372f6f4cb035c781747f0be013f2271feae3d6007bbbf71cf7a7dae82e776b6da86efcbc80b196d7bdd141fc718a9182338273451d59d43343511473d3cb1cf04d1a4b0cd0bacb14b148a19248e442c9246ea41041208f63d0430054d41eb0de48e48a478a542b2a9208208208c1041c823cc1eb27bdf54ebdefdd7baf7bf75eebdefdd7baf7bf75eebdefdd7baf7bf75eebdefdd7baf7bf75eebdefdd7baf7bf75eebdefdd7baf7bf75eebdefdd7baf7bf75eebdefdd7baf7bf75eebdefdd7baf7bf75eebdefdd7baf7bf75eebfffd0dfe3dfbaf75ef7eebdd5787cb4f9c5b4fa5f099fa2c167b1b43361d24a6dc3bcaac0a8a0c1d535d171782a65121ce6e477051634495124f485770c1017cc7cd96db54332c53282b867390a7d147e26f9673ebe5965ec57ddb37bf70770daeef76db669219c8682d17b5e65e3e24cd8f06de9924952cb9aaa952daac7c87f9cbd8bdb992cbd26d7c8e676beddafa89cd7e6a7af964defba564babcb95cb2caf262a96704ff93533ea2a4092560348c79def9bafb727912ddda3809cb57f51fed3e40fa0fccf5dabf6bbeeebca7c89676336ef696f77b9c48ba225402d2dc8c811c7402465ff007e38a572a838f44d76a6d7dc7bf37262b69ed0c4d6e7f7267eba3a3c7e3e8a379ea6a6a677e64908b9589012f24ae42a202cc4004fb0cd9595d6e5750d9d942d25cc86800ff09f40389270064f52ff003b73c72cfb7bcb1baf37736eed15972fd8c45e491c8000185445e2f239a2471a02cee42202481d6c4fd51fcad7e396c4e9dd9f8df957b98603b3fb7b7863f67ecfcb8c855e2a3a4de793c1e63318edad4af24b4b4b4d255d1616a16952afc66a2afc6927ef4f0d2c73c6ddc91ca9b66d56f69bfaac9b85c4a104a5993f54ab111c641000014d0b8ef6a578aa8e2f7347df4fef1bee573f733f357b3067b1e4ad8ac1ae5ac04105c13631cd1c725ddf23a3b3c8cd2a1956ddc0b686ba18ac734ec0776eff00255dd18dcc4d8ae9bee3d979bcf54413e4711d7bbf3254fb7f76e4b1346c90d75763e658a24ae8a8e79116422984711755690922e41b97b616af3b43b3ef4a9707222b8a062071219454d0ff00c2f15c9ea60f6dbfbcaf7a3b79baf74fdab966dae1711cb7fb4eaf0d647cc68f6f72de1a3328269f5a19a84a474a81589dc9f0d7e4cf434d2af64f516eec550248c91e768f195196c0d485bde587258f8e78bc5753ea709fef23d81b76e4ae65d9b535d6d723423f1c7debf6f6d48ff6c0759bfedbfdee3d80f740c16fb0fb816b6bbb3ffc45beff00139ebfc2a27d31ca7fe68c920f4383d15a7620907eb7b107ea0ff88fc5bd853863cfac93521c06520a1e0479f5819ac41fed0219581b1561cab291cab29fa11c8f7a3d3a07ecead4be0f7f35eee9f8a35b8dda1bde7c976e749354d3c559b6b2d5ed36e9dab47c452d56cacd56bb13f6f159bec2a9fede5d1a55e1275890b94fdc6dd7975e3b6bb2d73b4d40284f7a0f58d8fa7f09c1f51c7ac2dfbc77dc97dbdf7badaef7de5f8e1d8bdc508c52e234a5bdcb710b771201f11c78d18f116b52b201a4ee0bd23de7d5df22baf30dda3d43bae8776ed0cc868d2ae975c3598dc842913d6617378da858eb70f9aa0f32f9a9a74491432b8ba3a3364c6d3bbedfbdd945b86d97224b66f31c41f3561c55879839f3e0475c18f717db7e73f6a39aaff9339ef6592c77db7c956a149232484961916a92c4f43a64424120a9a32b2816bd997406ebdefdd7baf7bf75eebdefdd7baf7bf75eebdefdd7baf7bf75eebdefdd7baf7bf75eebdefdd7baf7bf75eebdefdd7baf7bf75eebdefdd7baf7bf75eebdefdd7baf7bf75eebdefdd7baf7bf75eebdefdd7bafffd1dfe3dfbaf755f1f387e5661fa6769e6f014b9e8f09514d8a35dbcf702311360b11531814b89c69521df70e78caa912a7ee2248ba6cf22b20339af9862daeda5884da085abb7f083c00fe93797dbf3c656fddb7d8fbff007077bdbf77b9db0dc40d3e8b4808c4d2a9ee964ae3c08684b1382ca6bda855b4dcef9ef8dcfde7ba1b2b946971bb631b2cc9b5b6b2cc5e9b174aec41adadb109599dad400cf39be9bf8e3b4605f18778de2e377b8f124aadba9ec4f203d4fab1f33f90c75df7f6e3db8d9bdb8d9859d98597779541b8b8228d230fc09fc30a1c220e3f13558e020dafb5f73f606e6c46ccd9787adcfee3ced645438dc65044d34f3cf2b05d4d6f4c50440ea9247212340598803d97d9595dee7770d8d840d25d486800ff005600e249c0193d2fe7ae7ae59f6fb96b75e6de6edde2b1e5fb28cbcb2b9ff7944515692473448e340cf239088a49a7578213ae7f948f5a4234edeec9f9e9d8f870f4141510415d8ae93c264e1d5459cccd348b3b0af46d2f418f7f0cb5929f3d4011c71c693048fb7fb65b77d2da98ee39c2e17b9a951129e183e55caa9a173dec2815472e76adaf9ebeff003cf69cd9cd51de6c7f769d926260b6d546bd9509d6752d15ee1d2ab34ebae3b28c98202d2bcb2bb8ff00386defbc693a47e11f5aee9cb4b92dd359b3ab3b237ce4249da4acc86e8a6c0ed8c051656793d2ed5334d5b93769005569246d2001a433ee93cf6bb7f2c6d734bae7f0cc92379b38544d5f99d67f3e8f3fbb9b61d9779e6bfbc07b85b56d8b6db3cb7b1dad9c4076456b2cd7173e08ad70b1adb29193db9ad7a69f8cff0024baf7e7bf58e17e1a7cb3dc93ed9ee7c4f8d7e3bfc8d69e38f37367a9a248f1581cee4a4929653ba248a15a749649d57704216091d32294f2d531cb9bd6ddcdd651f2bf33c9a3710296f738d551f0a963f8f80049fd51dac75e92cbbdf2f6439bbeed7cdf7bf786f6076f59f929c96def62d2c6d8c0c7f55d615d41ad0d4b90a85f6e90f8d106b5f112101773fca4fe635fcbbfb1b23d33bff7e64377e231835e3701da948dd89b23776da32bc14394c0657365770261aa6284a7869aba0fb770d1c88aea402bbcddf9e7907703b6dc5fb4908ca789fa91c89c0152ddc07910082a707a91f96bd9bfba57df0792d79e796f94a3db77473a2e85895b3bbb4b92353c73c31836d231aea499e0759928ead9203fcdf39fe09fc80228be527c308b60666a94bd6f64fc7cca52d3553575fd33c9b5eb62c1494b48ececd269adac90d87a58f2167f5f396b79023e68e538cb1e32c386afafe07a7afea1fb3a091fb9f7de2bda566befbbff00de22ecdb2612c770d42111ff000e96fabb47714001fa483ce8ea38e73fcbbfe2df7eaff11f86bf34f6267abea98474dd61db5a7676f97ae917ca9418fa6ca9c3e46b82f9163f28a3788b83691be82cbc99c9fbf90dcb5cd6b1ca784538eeafa0ae86a7d8aff69e9e4fbdd7de6bda0ff14f7fbeef135dd8c796dc36d2c9108c6353b27d5da33920b50dc5b608ac6bc491af901f067e4dfc6b15957da3d715f41b7e8ac64dd78b9e0cb6da28f318217fe254cda635a89ac89ad5433b2a0f5103d85b7ee43e64e5e8a4babcb557b253996360c82a682a30cb53402aa3240e3d6507b3bf7c2f63fdebdc6cb60e59dfe6b5e6d9d494b0bc85a1b86d285dc46cbe2412e850ccc22998855662348af4f7f04fe6e6fef853dc18ede1869ab733d739da8a6c6f6875ff00dcca28372edf69023e42869da64a4a5ddb82563363aad85c30781c9826954a7e50e6cbce54dc96e2225ec1cd258eb861ea3c83af153f683827a37fbcb7ddc7967ef11c8d3ecb7cb1db7375a2b3edf7da417865a57c290d3535b4c68b3460e3122f7a2f5ba46cef9a7f1837f6dbc6eecda7dbb80cc60b2d119296b29a933674ba3689e96a6238bf252d6d24a0a4d0b85923716207bca5b5e69d82f608ee6db72478586080df9838c11e63cbaf9fbdf7eeedef3f2d6eb77b26f5c877506e30b51959e1e072194f8946461956150c320f4a43f297a047d7b2713ff9c39cff00eb57b51fbff67ff94e5fd8dfe6e8a7fd64bdd33c393ae3fdee1ffad9d713f29fe3f8faf65623ff0038739ffd6af7afeb06cfff0029cbfb1bfcdd6c7b21eea9e1c9b71fef70ff00d6deb8ff00b355f1f7fe7e6623ff003833bffd6af7efeb06cdff0029cbfb1bfcdd5bfd63bdd6ff00a636e3fdee1ffadbd713f2b7e3d8faf66e23ff003833bffd69f7efeb0ecdff0029ebfb1bfcdd5bfd633dd83ff3a65c7fbdc3ff005b7ae27e587c791f5ecec3ff00e7067bff00ad3ef5fd62d97fe53d7f637f9badff00ac57bb27fe74bb8ff7b87feb6f5c4fcb2f8ee3ebd9f87ffce0cf7ff5a7dfbfac5b2ffca7afec6ff375bff588f76ffe98ab8ff7b83feb6f5c4fcb5f8ea3ebda386ffce0cfff00f5a3dfbfac5b2ffca7afec6ff375bff588f76cff00ce9371fef70ffd6deb81f971f1cc7d7b4b0dff009efcff00ff005a3debfac7b27fd1c17f637f9badff00ac37bbbff4c4dcff00bdc1ff005b7ae8fcbaf8e23ebda786ff00ce0cff00ff005a3dfbfac7b27fd1c17f637f9badff00ac2fbbc7fe748b9ff7b83feb6f5c4fcbcf8e03fe6aa61bff003dfb83ff00ad1efdfd64d93fe8e0bfb1bfcdd6ff00d60fddff00fa61ee7fdee0ff00adbd75fecdefc6ff00f9fa985ffcf7ee0ffeb47bd7f59363ff00a3827ec6ff00375bff00582f783fe987b9ff007b83feb6f5d7fb37ff001b7fe7eae17ff3dfb83ffacfefdfd64d8ffe8e09fb1bfcdd6ffd607de0ff00a61ae7fdee0ffadbd7bfd9bff8dbff003f570bff009efdc1ff00d67f7efeb2ec7ff4704fd8dfe6ebdfeb03ef0ffd30d73fef707fd6debdfecdff00c6dff9fab85ffcf7ee0ffeb3fbf7f59763ff00a3827ec6ff00375eff00581f787fe986b9ff007b83feb6f5eff66ffe36ff00cfd5c2ff00e7bf707ff59fdfbfacbb1ffd1c13f637f9baf7fac0fbc3ff004c35cffbdc1ff5b7af7fb37ff1b7fe7eae17ff003dfb83ff00acfefdfd65d8ff00e8e09fb1bfcdd7bfd607de1ffa61ae7fdee0ff00adbd7bfd9bff008dbff3f570bff9efdc1ffd67f7efeb2ec7ff004704fd8dfe6ebdfeb03ef0ff00d30d73fef707fd6de844d8bdd1d55d9934b4bb177d6037156c1199a5c75255187269082434ff00c32b529abcc0a472e232a3f2791ed6da6e9b7df12b69768ee3c81cfec343fcba09733fb75cf1c9b1a4fccdcb175696cc682465ac64fa7888592bf2d55f97427fb5fd02faffd2dec7b67b0a8babf6167b78d5a2cf350538831544c587f10ccd637831b464a06711bd4306908175895dbf1ed0ee37ab616735cb0a90303d58f01fe7f9742fe44e53b8e75e69daf97e062b1caf595c7e0853ba47ce2a1451479b151e7d6941f3cbe4365bb57b1f27b3a1cc3e4b09b6f31515db8f2026693fbc5be65f20c8cf3c9fa25a5c0195a9a0552631299187d16d8adce3bdc9b8df496ab2ea8636258ff149e7f92f01f3af5f449f76df6bac392794ecf7d7b010df5d5baa4094a7816629e1aa8e21a6a07727257403c5aa47b6a6d3dd3d89b9b15b3364e16bb706e3cd5547498fc6e3e179a695dd82991f482b0d3c20ea92472123504b103d856cac6f374bb8ac6c20692e5cd001fcc9f40389270064f52ef3ef3f72b7b75cb5b9f36f38ef30d8ec16895796434a93858e35f8a49643db1c680bbb10aa093d5e14527577f290eb981218307d93f3cfb03011d47dad62a57607a4b1191892a28f2b9ca42dac648abacb8fc7b78e6a9602a2a3446b12198a49b6bf6c36e36b69a2e39be74ef6e2b1039151e43cd5705cf7bd0695eb981b072dfb85fde01cf51f3673725d6c9f76eda2e9be92d876cb78ea4ab518555ee18556e2e0168ad118db5b6a90cd2b545f5e53ee8efcf91bb168f7566727b9b74f67f69edb8b716772d552d764f25539ddc34632b90aca9999a49a5f0c9239fc002c00000f717ed11dc6f9cc9b6c771234935c5d26b663526ac35124f1c57ae857bad7db37b4dec573dde6c7610d96d1b372f5d0b68625091c656074823451800c8507a926a7249eac2bf9d5ef6a7cf7cc58f65d0b8143d4dd57b17658a5420c505757435dbc2a0a81fa58d16e5a543fe110f62ff75af05cf349b7538b7b78d3f33590ff00c7c758f3fddc1ca8db07ddd20de254a49bc6ef77720f998e3d16883ec0d6f211fe98f54fd2178dd66899924465746462aeaea752bab2d8ab2917047d0fb8cf81c71eb3ce48c3a90541422841ff000757cbf1c3e45f56ff00317eaac4fc2ef993998b0ddd18748e9be3c7c809fed932f95ca474f1d263f6c6e1afab60b55ba2a7424244acb1ee2802c6e53271c3355ccfcbbcc3b773a6da9ca3cd9252f462dee31ab55280127fd13cb3894769fd40a5b95fef4fb33ceff754e79b9fbc97ddcad35f2936a3bd6cf4630a42cc5e4758d33f455abf6d5f6e93f563ada192386a13e45fc76ecff8c3d9798eaded6c0c98acd63d9aa31b918d64930bba308f3491d16e1dbd5cc8895d8cad119e78921903472aa488ca232e62e5edc796b707dbf718a878a38f8645f2653e9ea38a9c100f5d00f653deae47f7e392ed39cb932f6a984b9b67205c59dc6905a09d47022b5471549528f1b153d00042ab065ba95370412082390411f420fb21e1f6f52ef84a7cb1d5f87f2b5df99df93dd37f2cfe107626eace6eaa8dd3d5b0eeeea183766732395a0dbd90dbd52b406928df21555325163e8777566ddaf4a4813c3a29ea18ad8b0332fb75772f306d3ccbca57b70f23496e1e1d6c4852a6941526803989b48c60f5cb7fbee72d6ddecafb8dec47de3b94b64b6b2b7b3de5adb7336d0c713ceb28f135384550f24968b7f09958ebab460370a509e6e827c3657258aaa8a682a31d5b53473c33a78a78e4a799e2649a3b9f1caa56ccb7e0f1ee1a951a295e36043292287ca9ebd75276dbc8771b0b2bfb7756866895d4a9aa90c01041f306b83e63ae541baf7461a9cd1e1f736e2c452195e73478acee531d4a66902892634f4557042659020d4da751b0b9e3ded2e6e225d115c3aa578062057ec07ad5cecdb45fcbe3df6d36b3cf403549146ed41c06a652682b815a0eb33f60efde7fdff5bd3fc7fdfd79ff00f78ff721ef66f6f3fe5325ff007b6ff3f541cb3cb63fe75db0ff00b2787fe80ea23f60efdff9eeb7a7fe8579fe3ff5a1cfbafd6de7fca64bfef6dfe7eae396b973fe99eb0ffb2787fe80ea33760efdff009eeb7a7fe8579fe7ff005a1ee86f6f3fe52e5ff7b6ff003f4f2f2d72effd33d61ff64f0ffd01d467ec1dfbff003dd6f4ff00d0af3fff00d70f7afadbcff94c97fdedbfcfd38396f97787f57ec29ff3cf0ffd01d456ec0df9ff003dd6f3ff00d0af3ffef1fee43dd4dedeff00ca5cbfef6dfe7e9c5e5ae5dffa67ec7fec9e1ffa03a8efd81bf3fe7b9de7feb7f7af3fff00d70f75fadbdff94c97fdedbfcfd3a396f9740ff957ec7fec9e1ffa03a8add81bf3fe7b9de77ffc3af3dffd70f7afaebcff0094c969fe9dbfcfd5c72e72e9ff009d7ec69ff3cf0ffd01d606dffbf3e9fdf9de7fe3fefebcf7ff005c3dd4df5eff00ca64b5ff004edfe7e9c1cb7cbbff004cfd8ffd93c3ff00407585b7fefcfa7f7e779ffe8559effeb87bafd6deff00ca64bfef6dfe7e9c1cb9cba32797ec7fec9e1ffa03a8edbff7e1e06f9de76ffc3af3dffd70f7efadbcff0094c97fdedbfcfd5872e72f71feafd8ff00d93c3ff407589b7fefcfa0df3bcffc7fdfd79fff00eb87bd7d6de7fca64bfef6dfe7eae397397b8fee0b1ffb2787fe80eb87f7fb7e7fcf73bcff00f42bcfff00f5c3dd7eb6f3fe5325ff007b6ff3f56feaef2f7fd182c7fec9e1ff00a03af7f7fb7e7fcf73bcff00f42bcfff00f5c3dfbeb6f3fe5325ff007b6ff3f5efeaef2f7fd182c7fec9e1ff00a03ae8effdf83ebbe779ff00e8579fff00eb87bf7d6de7fca64bfef6dfe7eb4797b9740a9d82c7fec9e1ff00a03af0dffbecfd37d6f3ff00d0af3fff00d70f7efadbcff94c97fdedbfcfd6872ff2eb70d82c7fec9e1ffa03aeff00bfdbf3fe7b9de7ff00a15e7fff00ae1efdf5b79ff2992ffbdb7f9fab7f57797bfe8c163ff64f0ffd01d7bfbfdbf3fe7b9de7ff00a15e7fff00ae1efdf5b79ff2992ffbdb7f9faf7f57797bfe8c163ff64f0ffd01d3c613bcbba7afab06edd8fdb1d85b67736095b2f87cce3b75e67eea87258b56ada0aa45a8ab9e090d3d542ada5d19180b302a482a6d376dd2cee22b8b6dc26499581043b71071e7d117327b7fc8fccbb26e7b36f9ca1b6dcedb3c1223c6f6f1156565208f84711fec67ab93ff00a0983baffe798d9dff006449e5ff008b18ff00b2bdff009ecffe04ff00c78fff00568fd1fed7ee76ff005dabff00f7d27fc93fd3fe24ff001ffa4fe8f5c7ff00f937af28ff00d1c2ebfe56ff00f7e1ff00921ffca3f0ff00723fe1fc7e5d7fffd3d98ff9b17c8597ac764d463a86a02cbb7f0b1e46920d574aadebba649f0fb692a22d4032e1a9049567fac72b7b8c7dc5de8edf68c887b912a07ac8fdabfef22a7eceba0df71cf6b23e6ddf96feea2ecbab831b3532b696c165b8d27fe1ada63ff4ca3ad4476aed6dd9d97bb713b3767e2ebb716e9dc9905a5a2a3a5479aa2aaaea642d3544ee0111c4a59a49a56b2a2dd98dbde36d8d95e6eb7b0d8d8c464bb95a800fe649f2038b13802a4f5da7e7de7de55f6d795375e6ee6cdd23b1e5cb08b53b9f976a45120ee9257348e289017772154127abbc23ad7f948f5af869ff00bbfd83f3dfb0b011c914722c395c3f4861f25189a1cc66a9a5ba265047207c7e3dc07a99556a6a4785638fdcc12cbb6fb63b69b5b4293f38ce9dcfc4440e781e00715522ae4077ed0aa398bcbbcbdee07dff007dc0879b79be1bad9feed5b3dd936b6b5d125ec8bdac032e249dc556e2e14b47691b35b5b132b4b2b5256f0de1b9f7eee7cf6f4de79cc8ee5dd7b9b275599cfe7b2f52f5791c9e4ab6432d45554ccff566636550022200aa02800439757571797135d5d4cd25c48c59998d4927893d758360d8366e57d976ce5ee5edb61b3d8eca158a086250a91c6828aaa07f326a58d5989249e8f67f29cebf8bb03e74753254d39aaa1da3fc7f7bd62ff623184c54f1d1cd29fc24592ae80ffaf6f639f6bec85df375ac8c3b6089e4fb31a07f371d6177f786733bf2f7ddb77db0864d336efb8d9d90f52be21ba900fb52d581f91a79f4017cd5ec43da7f2cbe42ef9130a8a7cb76aeeda5c64c08225c2603252edcc0b8b71a4e1b1305bfc3d8779aaf7f78731ef5775a86b9700ff454e95ff8c81d643fdde39507257b1bed572d787a25b7d92d5a41e934f189e61ff39657e8adb37fb7f61faf534815fb3a86cd2c12a5453c8f0cd0bacb1cb13b47247246c1d248e442192446008208208b8e7dd2a41041a53874dc9023a32ba068d8104115041c1041c104629e7d5f4743fc88eacfe65fd5b87f87ff003072f0e0be45e2166a4f8f3f2127a6812a7379334cbf69b7b73548929e39f71d7fdb2d3c914ba20cfc6106a4caac3254cd3b0f30eddcf3b6c7ca7cdb269dd07fb8f7141ab553009fe3f220d04c280d240ac7955eee7b37cf3f744e78bbfbc57ddd2d3c4f6f9803bd6cba98c6906bac8c88031fa40497475d52edd216750d66658e3a73f901d09d9bf1a7b373bd53daf819309b970afe582542d362b3d879a49571fb87015fa563c8e1b24b1318e40032b2b4722a4a8e8b177306c3b8f2dee32eddb945a641956195753c1d4f9834fb4104300411d743fd9af78b927df2e49b1e77e47bff0012c9cf873c2f459ed6e154192de74fc3226a0411549119648d9a375622eff2f9ee9a7e85f98fd15d8391aa6a4c00ddf1ed3dd13ebd30c3b737e5155ecdcad6d4dd94341888b362b483f46a6040240f6bf91f751b3f356cf7923521f1743fa05901424fd9ab57e5d02bef6feddcbee6fdde3dcce5ab38049baa58fd5db8f333593add2aaff4a5113423d7c422a2bd085fcd6ba7a7e9bf9b1db94a2048315bf72a7b3707e2896181e8b7ab4995ac4812355410e3f39256510b0e5a94fb5bee46d676ae6cdcd74d229dbc55fb24c9a7c836a5fcba0b7dc67dc08fdc2fbb8f224ad296bfdaa1fddd3549243da5234a939abc02297ec90755b8cffe3fec7fe29ec064f5982075b7cfc18f831f19bb9fe336c1ec2ec2d82f96dd5967cbd2d756d2e5aaf190cb1632ac52525a928c240acb0280cc06a66b92493ef396cececcd9da1369157c24fc0bfc23e5d7c95f33733732a732f3122f31ee0145fdc0005ccd80267a0f8fa373ff000d8bf0c3fe7d7d67fe84d94ff8afb53f4565ff0028717fbc2ff9ba25feb4733ffd34bb87fd94cdff0041f5d7fc3627c30ff9f6159ffa1364fdebe86c7fe50a1ff785ff00375bfeb4f347fd34db8ffd94cfff0041f5d1fe587f0bcfd7abeb0ffe4cd94ff88b7bf7d0d97fca1c5fef0bfe6eb5fd69e68ffa69771ffb299ffe83e9bf21fcabfe1164e2f155754cec42b2a4a73f572cb12bd848b1b54c3384594001801ea1c1f6dcbb5ed938d336db6eebfd28d0ff00857a5f61cfbcf7b54865db39db78b690906b15edcc66a2b43559470a9a7a54d3a285de7fc8abe2cef7c5d5c9d65b9376f53ee7303fd85515a0cfedf3529132d3475989860c478e8cbe9f298af33017565249208e60f6eb947758debb7adadc9e1241d943c05507e9b0f51a413e4c38f592fed47dfa3ef13ed75ed9f89ce736ff00cbe87becf7466b90ea58160974c7eae26a54230999149a989c0d3d6b15f2f7e1d7717c2eec687afbb628b1f3c399a39f2fb3378e02a24acdb1bcf070547db4b5f8a9e68a0aaa6aaa399952ae8ea238ea296465d4a51e391f1979ab95eff95371fa2bc21e271aa3917e174ad2b4f261c194f03e64104f7abeeedf784e4bfbc6f23af36f2a87b7bfb7758af6ce520cb6970575682462489c55a1994012283554915e3401762f5dee9ec6ca0c66dba132859228aa6be659feca9a4a86d14d03353c3513d45655c9e9869e18e59e537d2854315b72bf27ef3cdb74d06d9081027c72bd4469f224024b1f255049e3815235efe7de53db1fbba6c10eedcf3b8b49bbdc03f4bb7dbe97bcb9230592366554894fc73cac91afc20b48551afdbe32ff0022ede9bc3198adcbdc35d4fb1b175c90550a6dc90d6556eb969d879a39a8f6462ab28a9f109511ba8d196af925205cc097d3ef22f62f68f95b6a8d1afe26bebbc54c8484aff46352053fd397afcbae28fbb5fde37eff00fb8179710727ee10f2af2e92c122b35592e8a1141e2de4c8cdac64836e96fa49fc5407ab5dda5fc9a7e1a6dea5a64cbe2373ee7c8420b495be6db9b6a03332e9796968303b7a1fb2d4b604099d8db963ec7d06c7b25b53e9f67b58e9c34c518ff02f587dbafbabee86fa643bd7b93bfdd97f8bc6dc2ee4af9e43cc4115f2e1d088dfca9be0ed80ff0045d956200058ef0cc5cd87d4d88173f9f6abe86cbfe50a1ff785ff0037443fd6be69ff00a69f71ff00b299ff00e83eb09fe545f070fd7ab72bff00a1966bfe8ef7efa1b1ff0094287fde17fcdd6bfad5cd3ff4d36e3ff6533ffd07d623fca73e0cb7d7ab72e7ff00273cdffd1defdf4363ff002850ff00bc2ff9baf7f5ab9a7fe9a6dc7feca67ffa0fa4f6e0fe541f06e9e8cc917576615bd46ffdf5ce7f4ff83fb27dea1b6b6b52f15a421bfd22ff009bab2f34f34139e65dc7feca66ff00a0fa2adf30be0dfc64e87f815f2d378f5a75fb62f71d4f5f6d6a3fe2793cbd5e725a48e9fb2f6664d1f1a7201db1f52d534481e488abbc45a32743b0201e67293720730ccd04624f09321141fed13cc0eb29bee55be6f979f7a8f676def37abc9a037f3555e795d4ff008a5c710cc41eb4f3f78add7d3675ef7eebdd41ca7fc5ab29ff006acc87fee1cdeecbf12fdbd3571fd84ffe91bfc07a26bffdaffd883fe80eb1fbfedabaffd43e7fce03b32a77876b51ecfa7ac10c596de39ecd5589643f6c2836f4b1eccdb724f2682c90a24551290071f5b1201f78d1ee65f35d6e51da2bd03cac4d78517f4d6bf2e27aef17dc7f94a3e5cf6f6e37f7b6677b6db628d422d5cbcca6eee1516b9624a20ce4e2a2bd0b2db536ff00f28fe86dbfbf65da0fbbfe5cf77e37318ed93bbebe812bb6475d52d0c38da8c8e558d50782a3338c4cbc125150e826a5f44d521557c2a20b88adbdaed9226b683c6e64bc5651352b1ad284d2be4b505569de4066c00a31db951f98bfbc1fddddc5f99b75fdd7ec8f2c5c4730da84816ee5126b48cc8133e34e2391679cb15b446682daaeef2b5156e9dd3b8b7a6e2cdeedddb9ac8ee2dcfb93275799cee772f552d6e4b2b94af99aa2aeb6b2aa6669259e795c924f03e82c001ee12b8b89aea696e2e25679dd8b33135249c924faf5d7ed9364da797769dbb62d8b6e86d365b385628618942471c6834aa228c0000ff29cf49b91b4a93f9b7fbebfb60f46adf09f4eae9bf92dd3e3f6767fe567c83cc874c6f4ff0042e6ea64a8d178e386a63addc39061f8330a5da4a147e439f72f7b5291da45cd3becdf0dbdb53f2a348df9fe9afedeb975fde23757bcd1be7dde3da0db8069b79df5a523cf587b7b383f23f57357fd2fcbaa53aaaa9aaaa2a2aaa24696a2a6696a2795c96792699da49247637259dd893ee21662c4b13926a4f5d478218e08a286250b122850070000a003ec1d4266b7fc49f7427f674a40f33d4677ff006dfef7eebfe1ead4af513cb3534d15552cb2d3d4d3ca93c134123c5343344c1e2962950abc72c6ea0ab020822e3dd6a548209143c7cebd3535bc72c72452461a160432900820e0820e0823041c11d5f2f4577b7557f33aeaac3fc48f96998a4dbbf2876e52cd43f1d7e4255c11a556e7ab1047f69b5b7555ac90fdfe7eb8c0219a198a479c8c23ab2e51124a99a762df76de7cdb63e53e6b902ef0bfee35cd06a2d4c026a2ae6801068250056928563cacf76bda5e7cfb9df3e5e7de1feef566d37b6539077cd9033784916a25dd128da6d54b178a540d2edd233503593cb12d3677cf45f667c6eecddc1d53dab819b01bab6fcf74605e5c6e6b1923b8c7ee1c0643424792c264e38cbc3325882191d52547458b77ed8b72e5bdc65db7728b4ccb9561f0bad70e87cd4d3ed04156018103a23ecf7bbdc8fef9f23d8f3bf246e027dba6ec9a17a09ed670a0c96d731d4e895350f54911965899e27476b79fe6386afe4afc13f849f335228aab3b0edaa8ea8eccc90bc95f59b8716f5f42b3baa29f1d249b9367ee3ab7663fb66b22537d60fb9279f756ff00c9fca5cd6056611f8329f32c2a3f66b494fcb50f5eb03fee7661f673ef2ff78dfbbbc92326da6f06e3b7a704581b4353272ff4d7560800f8bc2723811d508bbf3fef5ff1bf70c13d75300af5bde7f2c5ff00b230eaff00fa8cdcdffbb43ef3c2cb365667fe149ff1d1d7c877347fcacfccbff4b0b9ff00abcfd1d0ccee68716e233cb7f4b5cf1fe1cfb23dd79822db9fc33f1744ca9abac2dba29e95a14c9647018c927a786aa383239fc2d0551a6a840f04c696aabe1a854917904af3eef0eef70cb134891aab28615740684541a160723e5d2fb7da373bc8fc6b3db6e2586a46a48dd96a388aaa9151d2831b95c7e5430a0c8e2f20cbfa971b94a0c891feb8a3a89c8e7d9c5bdcc738ed752df2607fc04f4cdd6ddb858806f2c27854f9bc6e83f6b01d4f66d3c7e7fa7f4ff5fdaae9174d55f4cd520589bff87b45796e6751439eb60d3aa95fe72ff1fe97b9fe2250e4561a4a7cef56f646dadcb4db8658d5eaf11b7338b51b5f72d2d3c68a6aabd72b3e42802d246c825aa860776448d9d413cedc9efcd5b5edd6319559e3ba462e78a46411291ea694217cd82d683232dbee83f78b8feee3cebce5ccd7cb2cfb55e6c17112daad4adc5ec6c9258abd3118126b5798826385e5d219982b4ffe5bff00cbd3687c7bd95b5bb277dedb8a4ecdaca28f2bb6b0b9231d5c7d7d4393a64922aeab88c6b0d5f60e4e95d5eaea5d6f41abede158cc648186cfb46dfb0edd6fb66d9008ed2318f527cd98f9b37127f214000ea02f727dc9e71f76f9cb78e7de7bdd9af398af5eacdc238d057c38204a91141129d31c6380ab31676666b53afcdd1d0dccd282fc920b724fe49279e7deaf376b4b2af8920af4075527a66a7dc7559290c78ac7d556daf734f049285b5b967505105ffa91ecaa3e617ba72b676cd211e809fdb4eac5283b881d71abcdcd8f611e466c4e3a56bda1c866f0b453716bfed54d74520b5ffa7b7db75bd4a092dd54fa33a03fb0b03d2c836bdc2e94bdb58cf22faac6ec3f6853d7a977363e56b4d9adb51aff005fef2e01bfe84c89f6fdbee6f2352531a8ff004e9fe463d3c762de470da2effe70c9ff0040f4f34998c357d45451e3b3586c955d1c504d594b8dcb63f215147154990534b550d1d4cf253475061711b3850fa1ad7b1f66d1cf0ca584532330e2030245785684d2be5d26badb372b18e196fb6eb88629090ad246e8ac569a82965018ad46a009a5456951d33ee87bd0103fdaf9febc7fbc7b25e62ff708fe7d268fe23d57a7f321ff00b7717cabff00c3276eff00ef79b5bdc7bcc5ff004ef398bfe69a7fd5c5eb2bbee43ff895dece7fcf7cdff68971d6881ef15fafa80ebdefdd7ba8394ff8b5653fed5990ff00dc39bdd97e25fb7a6ae3fb09ff00d237f80f44d7ff00b5ff00b107fd01d63f7fdb575fffd510ff00985e56a327f2002d43967a7d9d89918f3a4cb92cb677233b2dc93eb927e7f3c7bc45e7691a4debbbca25fe658f5f4bbf75db28acfdb11e12d15afa41f9471c28a3f203abb6f8fdb7f01fccc3f96550f556e8aea097b6ba9e39b64607726402a54edcdefb3284375b66eaa68e19ea970fb8b6654d2e332528476a845aa701a68d4acbfb1436fcfdedfaed974e3f785b8f0c31e29220fd27f5a3210ae7891abcfae60fbbbb86f1f738fbe5dc73d72f5acabc99bc38bd92dd0f6dcd95e3ff00bb1b650484f122ba5926b7524089fe9f82120eaf3bab6ce7f646e5dc1b3f75e2eaf07b9b6b66725b7f3f86af8fc5598bcce22b26a0c8d0d4a5c81352d5c0e8d62412382458fbc77bab69ecae67b3b98ca5cc4e5594f10ca6847edebb6bcbbbfecfcd9b0ecfccdcbf7c973b1dfdb477104a9f0c914aa1d18798aa9183420e080411d256a1fd06ff009f698e3ede8de4e1f2eaed3e385454750ff278f967d891431c792ee1df947d6d8e9caaacb3e23213ed9daf9605cfaa48928abb221547d1c31febee61da1ced7ed3ef574a2925d4e501f5562919fe4afd72e79fed23f70bfbc8bdb1d8a494b5972eecf0dc32d490b2c31dd5fa63802cf35b54f9803d075484cdff001bf70e13fb3aea881e67a8ceff00edbfdefdd7fc3d5c0af51d9bfe343dd49fd9d38abd4677faff00bcfbd74e814ea2c7535543554d90a0a9a8a3aea3a886ae8eae966929eaa96a69e45969ea69ea2164960a882540c8ea432b0041047ba8628c1918820d411c41f51d27b8b486e619a0b88564b7914abab00cacac28caca6a0a9048208a1183d5f5748773758ff34fea7c4fc4ff0093d95a0dadf2db67e2e4a6f8fbf20ebe0592af7b7d94425feeb6ebaad71d457e76ae0a722aa176232c8bf71195c8477a89ab63deb6ef7076b4e55e679026f88bfe2d734ab1207027cd8814652692815c4801eb955eeb7b61cf3f72ee7ebafbc27b0f6af73ed25dcc0ef7b2062b0c48cc6ae88a0848159c9b69954bd848da087b391e30207c5be9cecc93e29ff307fe5c1db7864c5f64f58c14ddcbd774b2f9eb466a9d8c1595557b42695625abdbf95c9ec5a18e96a201196933f32ca158c88abf63d97718f9739d390f738c0bd8009e13921d4e754751952d180080083232b006a004fdd9f753922f7df1fbae7defb90af58f2c6ef21da77443a124b49d078662bd552da678e1bd766562cad1d9c52c2cd198dceb7d551bd34f3d3c9c3c12c90c9f8f546e51b8fe971ee016054953c41ebb2f03acd1472a7c2ca08fb08af5bdff00f2c4ff00b22feaff00fa8bdcdffbb43ef3c2cbfdc1b3ff009a29ff001d1d7c86f34ffcad1ccbff004b1b9ffabefd1a6ccd3c753998d25b95d4dc7fb103d803768127dda3593857a295c29eb507fe7b94b0a7cea60ab603a57abd45adce883331adf81721500f709fbb31ac7cd4b1a7016708fd808ebe84bfbb521493eed36eefc7f7edff00fd61ea9e70d9bccedac853e5b6f65f2982cad23896932786c855e33214b22fe9929eb68a582a61917f055811ee358e6960759219591c702a483f91191d67d5e6d5b6ee76b2d8ee7610dcd8b8a3472a2c88c0f10c8e0a91f223adbe3f92e7cf7df3f247686e6e8aee6cb556e7ec5eaec753e5b6defbc8cf2d566b77ecca89cc0f41b96aa67924c867f6e541d2b58c4495346c824d72c4f249959ed3f32def306c57306e5334b7b69285d6d9668d855351e25810c2a72405ad4d4f5f3d3fde1fec5f2bfb39eed6c9baf246d9158f2bf3158bdc0b588698a0ba82411dc885076c7148b24322c6bdab234ba42a69517a9ee53eb007a6dcae1f139ea238dce63283318e6a8a1ab7a0c9d2435b46f558caea7c9e3a7929aa124864928b234714f1920e996356fa8f7eebdd42dc59618da392666bc8c18dc9bb1637ff0062493ec937bdc46df6aef5ef23aba2ea3d16eec6ec8d93d49d6bbfbbe7b8f2f2e23adfae311266b2de101ebf2d52f2c74b87dbb88859e315397cfe52786929a3d4a1e79914b2825d63453135b5f7316fd315daadd7511e6e49ed45f9b1a01f68e1c40f7dbaf6ff00997dd2e77e5cf6f793acfc7e61dcee045183844142d24d2350e98a18d5e591a86888c402680e9e1f317f9aefca2f9539ccb62f0dbbf33d29d2fe7920dbdd4fd7397a9c1403148c040778ee3c50c7e5f7764ea1115e712ba50093fccd3462e4c2fcc9ee06fbbebbc16f39b4da0612088e9017cb5b0a172789af6d7828ebe897d85fb8efb31ecb6d5613deec56fbf73d04066dc2f62597f5299fa5824d715b46a4908541988f8e56f2ac2a9f2564d25455cf3d5544aed24b3d4cd24f348ee6ecf24b2b33bbb1e492493ec0c58b1ab124f597f16dd69022c5042a918140140000f4000a0eb12411a1ba8f7ae9f4b78d0d40eae5bf91d6ecdcb84f9afb730189ce64b1f84ddd8bcc6377362696a5e2c7e768a936c6e8cb524193a71fb75494992c743345ab94910116f7905ec4336ae664af6d203fce5eb8e3fdedd045e0fb157217f5b5eecb5cfc20581029c3049a1e22a7d4f5b9bee7ff008007fd67ff007af733f317fb847f3eb8cd1fc47aaf7fe643ff006ee2f957ff00864edcff00def36b7b8f798ffe9de7317fcd34ff00ab89d6577dc87ff12bbd9cff009ef9bfed12e3ad103de2bf5f501d7bdfbaf750729ff16aca7fdab321ff00b8737bb2fc4bf6f4d5c7f613ff00a46ff01e89afff006bff00620ffa03ac7eff00b6aebfffd65bff00309a49b1ff0023f29473a912d2ed4c052b92a5419296ab314f2d8373e992323fc08f7885ceca537d910f111a8fd85875f4c9f75e9a3baf6a2c6e223fa6f79330fb196261fc88e8d07f262f90b0f54fc9aaaeacced78a5dafdf5864db34ff007154b4f474fd818379f2bb2e77592e92d4e5164adc4c2834bbd4646217e2c447ed56f8bb673036dd33d2def1348ce048b5287ed3dc83e6c3a807fbc5fda97e76f66edb9e76cb5d7bd72bdc19db4aea76b19f4c77401190b1110dcb1350a90c869e7d198fe74df10e899d3e60f5b520a81264a8366f7b63717099a3a0cba410d1edcdf75ab0a9fb26aa4fb7c5e44c9a54cd250c8016a891c897dd7e588e441cd1b7a558308ee02e69e4b21f43c11abea87d4f5067f773fbfd756939f6039c6e745acd14977b2c92b52b525ee2c92bf12b524b9800e056e93c9146b9f54fc01f9f70331f21d75e241e5e5d5e0fcb32fd3dfca5fe167513ff0091e5fb4f76d67686469802af362a9f1b97cd4cb28bf2c321bcf1ec6ff941ee63e70a6d5ede728ed1c259489187fb42e47fbd4a3f675cb8fbaa53dc4fbea7de63dcff00ed36eb03359c4e73426e12d6123e46dec24a7c9baa3677ff006dee1aff000f5d5502a7a8ecdff1af7527a7157a8cefef5d3a053edea2bbff00c6bdd49eae0751249916fa987f8fba13d5f520193d3fed2db7be77065292a761607746532d435b4d3d0d56d9c7e4ea2ae86be19926a4a886b31d197a3aa8674568dc3ab2b0041079f6bf6fdaf76bf914ed7633cb229ad63563a4f91a81823d6a29c7a8f39e7dc7f6c392ace74f70b9cf67dbaca58d95a3bdb8823f151810ca2291834aac09054236a15143d6e6bfcbd777f7876b6d5c16f4f95fd62db3bbb3a830136cac676be6bfdc7e4bb07adb3b0d365b2151baef047431cf8ba9c0534f58e6a24592655a9f1c2ef2f9329f9427e6092da06e68b344dc6042a252ca59a3342c64d24853dabaaa7ba80900835f9cff00bc6ffacf273aee9b7fdddb9aaf2f790f76b813cd602de78a0b7bc42e912daf8ca8d3c644d288088c344ac620f2232d3515f9673f504bf23fb8a4e83c8e432fd44dbd725fdcbc9e45228debe882c22beaa81628e1ff007033664549c6174499b1de032a890b8f78bdcc8db636fbba36ceecdb6f8a7413e63ce9fd1d55d1e7a695cf5f45bec6c5cf90fb45edfc3ee6db450f3d2edb18bb4424e87ce857a93fac22f0fea284a89fc4084a50f5ba37f2c137f85bd5dff517b9c7feb50fbcd9b2ff00706cff00e68a7fc7475f2b5cd3ff002b4732ff00d2c6e7feafbf46c323ff0017b8ff00e0cdff00437b036e1ff2578fede8a87c1fb3ad413f9f0b5be76c9ff885fac7feb566fdc21eef1ff916ff00d4243fe03d7d0c7f7678afdd9adfd3f7f5ff00fd61ea9759bfdbfb8b09eba1007577dfc846b66a7f983998227223aed8b94a5a85b9b3c430fb92a6dc11cf9a990f37fa7bc87f628fe8f330afe283fc12f5c5dfef6b50373f629a9dde06edff001edbfadc9bdcfdd71e7ae2580ff5ff00a7bf75ee82cde933cf550d39fd2d22a91f8b73ee3de6b91a59e2b71c091d3f162a7cfad7dffe1421d9d96dbfd5bf18ba3f155af4d86de79adf5d97bb28e2709f7d51b461c1edfda6b53a48924a689b726464d0de832c68d62d1a91197bb974f67b3f2decd0b52194c92b8f529a552bf2ee6fce9e9d75e7fba7f9276fbde61f767dc3bbb60db858dbda58dbb915d02e9a69ae4af90622de05a8c852c2a03107561f702f5db2eaeb7e0e7f29d87e6974ecfda58ceccc6ec7fe199c4db55d8ecb50e57292d5572e17199796ba9df1cb1c70533ae48208db530284dec47bc9ce5af6b394774e5fd9b71ba8273733db46ed49481a99413414c0af975c1cf7c7efff00f78de42f78fdcee4ae5fddb695d8f6adf2eed6dc3d846ee21866648c3397ab30502ac789cd07471cff00c277f25f8f903b5bfd8edddcc7fe247b3bff0059ee4aff00947b8ff9ca7fcdd457ff00272cfbd2ff00d1eb66ff00b9747ff41f46abe167f279ce7c47f905b3bbabfd326d9dd141b70660643034980cf53d7e41723b7b3385a614b59592ad2d37dbcd96f23eb56d4ab61627d8a396b93764e53378db3c72299c287d6e5be0d54a5787c46bd40bef8fde6bdd5fbc347cb717b957b65326d2d3b5bfd3db25bd0dc08849a8a93aaa214a57867d7aba9dcdff00000ffc87fef5ed5f30ff00b847f3ea028fe23d57bff321ff00b7717cabff00c3276e7fef79b5bdc7bcc7ff004ef398bfe69a7fd5c4eb2bbee43ff895dece7fcf7cdff68971d6881ef15fafa80ebdefdd7ba8394ff8b5653fed5990ff00dc39bdd97e25fb7a6ae3fb09ff00d237f80f44d7ff00b5ff00b107fd01d63f7fdb575fffd7365fce1f60546c8f957555460916873b8caf9696729a2392f99a9ce47127e0f828b72c2971c10bfd6fef16fdceb36b4e622d4ed7534ff7ad5fe061d7d0b7dc339a22e63f646d63f101b8b695030ad48a44b0927fd33dbb1fcfaab7db3ba337b2f73eddde3b6eba4c6ee2da99cc56e3c16421b796872f84ae83258dab8ee0a96a7aca64600dc1b73ee3d82796d2e20ba81f4cf1b8653e854820fe4475983bfec7b6f336c9bc72f6f16c26da2fad65b7990f078a6468e453fe99588f975b7ecbdf3b0f7ae13a33b6f79c34f53f1afe7cec5a6e90edcc3d6cae313b3fb96ab0f93c66daaa2ec64928a9b3a68735b5ebaa23f1a432e2f1d54cc3c65bde4f2ef769711ed1ba5c80761de21104ea7824c54aa13e81a9242e4500d28de5d7cfc45ed6f32ec9b8fb9fedd6c8ec9eef7b65b936efb5ca8078b73b5c734725c2ae006787559ee76e86acc26bb8829d74eb585f96bf14377fc69f9339ae83a98eb72905766f1d275b66e687c6dbbf686e7af34db5f251909142f5cb26aa1ad540238b234b3c6be9504c07cc5cad73b2f32fee250cc9248a213fc68ed44fcc1ed6fe903e54ebb13eccfde2397fdd6f61dbddd9a6860badbac673ba44a716b73670992e0532446e805c435a930ca953ab5007bff9d5672930dd9df1dba2314da711d31d03808969c7d29b27b9eade1a988f3cbb62f6bd0bff00acc3d8bfddeb91fbe36bdb23fecedad463c81763ff003eaaf58e9fdd91cbd2a7b55cfbcf778a7ebf7bdfdc163c592d62521bfe735c4e3ed07aa517703ebee2227ae97800713d4292a101b03a89360179249e0016fc9f75e240033d6a49e18119e470140a924d0003cc93c3a1abadbe34fc83ee3ac8a83ad7a8f7aee79e72be214986a9895d1b4fef27dcac26581435cba06502ff00d0fb11eddca1ccbbae9367b44a50f9b0080fd85e95fcabd639f3efdee7eef7edccb2daf30fb9960fb8a7182d0bdecc0ff0b2da2cde19c7090a5315a0e8ff00633f9476f9d8f8ca0dd3f2c7bc7a6be35ed9aad350b06f3ded89a3cfe428e3432d4c589c7caf2642a72c8384a54a492466e2c0dbd8d23f6c22dba34b8e6ae62b6b488fe10c3511e6017a1afc8237589db97f783f35f3f5d4fb2fddd3d86de77cdc00d3e3dcc6e638989a23bdbd9f8dfa6789696eade9e74c913e1aff00e4f1f1e214a8a7ff004c5f33b7953f94c70d2616a3aefaf1aa62fd1164ab377a6333020320b2d450d1548607569b587bb8dc7dace5feeb3b19f73bb1c0b0212a3d4c81453e6b11e931f6dffbc4bdec629cebcf961c91cbae28f15a48893146e2505935c4e481f826bf87d3049eb1637f9a5f7beebdd5b7baafe16fc7fe90f8e5539dac8b07b6ea76d6d1a5ec3ec6a7f3b10fe7de5bbe86a286a2082990b315c3ac96562ba98a81e87dc5e68df2f6d765e58dbedacbc46a28550eca38962ce3485550492231400d3341d577afb8ff00dde7d94e51e61f753df6e6dde7997e862f16769656b64b872424512470b9ba79a6959638c3de905997551751e8daff00324f985bff00e3b7c6dda1f09cf62e4f797c89dfdb423c87c96dfb559496b73fb7f6aee72f956d8b555f1cac22ce6f78eb9bef618cc70d1e04253471182aa17437f707989f97f698f94ad370926dd264adcccc6afa1b3a4fa196a495fc315172181e807f719f602d3dd7f70f74fbcbf33f26da6d5c93697a7f726dd0c74b7fa88688b32eacca962114098d5ae2ff005ce595e1746d68d9bfe37ee033d769157adf43f95eb0ff0064abab893ff299ba3fd736ca9f79eb65fee0d9ff00cd14ff008e8ebe4079a7fe568e65ff00a58dcffd5f7e8d7e4d8b6722fe9a9b8ff63ec0d7ff00f2588fed3d148f83f675a82ff3e336f9dd20ff00bf2dd61ff5ab37ee0ef778ff00c8b87fcf243fe06ebe86ff00bb347fe032db7fd2faff00feb0f54b6cdf8f716ff87ae84814c9eaecff0090b3dfe65d501f9d9997ff0078dbdbb7fe27de43fb13fd97337fa683fc12f5c5bfef6cafef2f62abfef8ddbfe3db775b9ab3db81f5ff007af73ff5c77eb17bf75ee901b8e89a4ad866b5c0914fb056fb68cf7714b4c06e9d43414eb58eff00851460eb63dedf1437499257c6e47aff00b0f6e430d87829f21b7b7260f2356e1820226aaa5dc905c163e988580e6f0d7bd10b89f966e0d7c36b7917e40ab827f6861fb3aedcff0074c6e96d2f287bcbb32a28bc8373b2998f9949a099147d8ad03f9716353c3ad6de49523fd5ee10ebadd24c91fc5d6e61fc8ab21f65f10b70ad452d547e7ecbf3c05e19104d036cadae1658894b491311c30b83ef33f94b7382db95796e3706bf4517fc7475f299f7a1224fbc6fbdeea7b4f33ee1ff00690fd5d753e4e2a96d291cbfec51bfe29ec5106e705c3694ad7a823491d399047d411f9e411fef7ecc86457aaf502be916ae1f1b5adcfd7fc47fc6bda3bcb55ba88c6dc3ad83435e8a0fcdde99de9dd5f107bbfa6faea0a5a9dd5bef6f6268b1d4f52f4d199d717b9b0b9eaca6a435b5f8bc7ff10aba2c649153f9ea6087ccea5e45507d87379e596dd797771d8e2b810b4eaa3595d4051d5b802b5a81419f9fcba97fd87f742dbd9bf773933dcebcd964dc6df699a590dba4a20690bc12c2a04ad1ca142b4818f63542951426a3507a8fe527f3d2012b0f8ffbcde34670afe3c32eb50c42be939a2407517f70cc9ec76e31ab39e60b7d23fe16ff00e7ebac83fbd9f91c815f67b77d5f2bcb623fead8ff0000e9f3ae3f940fcd9defbcf07b6337d5d9ad8788ca571a5c8ef0dc14f495186dbf02c52c8d5f91871b93a8af7a743185222477f5700fb496fecddcdcbb227315b9238d237ff38eb5ff002764e4bc04f67b7632120006f2dc0c9a64f86d4fd87aaaeca7fc5ab29ff6acc87fee1cdee185f897edebad571fd84ffe91bfc07a26bffdaffd883fe80eb1fbfedabaffd0bf6fe7b7d1353b87abf69f77626865a997666469b1b9b9208d8fdb5054b5443254d469075472d3d4ab3136d0b443f07dc35eeeed0d3edf6dbb4484989806f903e7fcff00e33d751ffbb53dca876ce6edfbdb8beb9545bf89a58431f89d749d2bf30cb403ccca7ad5419bfdbfbc78fb3aed505fd9d5e47f2e0cc41f28be297c9ff80bb8e76aacf47b6abbb6ba30c9288a5c7ee1a3aaa39da969b21505e2c752d16ff830f398e3547929f299125d433112df2248bcc3cbbcc1c9d706b2f866683e4c08e04f0a49a0fcc33e7ae617df1b6c9fd92f7cfd98fbd36cb168db5af136dde282a2488ab2ea645a17692c4dd47a9890af6f6d45240a9a1f8f953b3ff995759748c9da7999317f2a7e15f65edb9f79bd7c090e67786caa0ce502d54d918991249a2cea61615aa900325267286466544ae1ac49cbb7769ce16bb44dbac857983689d59abf13853827e4c55439e21d6bf8b38d7f783d9798feea7cc5ee643ede5ac72fb21ee6ecb2c500524c36f24a85996322ba64816695add4f64b6b7012a5a0250a77cccf869f2ffe68fcdeeebdd5b1baa2bf19b42933185dab80cfe7f278fa7c0ff04dabb730d85a1ad192865960a76cea533e4d2098c72c51d5057b30e43fcc1c9fcc9ce1ccdb96e305b2c362582ab48c32a8a101016addd42c3501c78f5347b15f7c1f623eef1f77ee42e4c7dc6f779e70481e7bab7b2b671e1dc5dcb25c346f35c98202600e90398e49092955522b409e6f801f133a2a697fd9baf9b7d7f88cbd1d4a4359b03a9cd4f636f4a4a88d75556333382d9f0672af0b33382a934f3c31aff697e9ed2b7277246c44ff0058f9a849383fd9c3920fa32a788c3ed2cbd0913ef39f7ccf79bc33ec8fb0036ad8e45aade5f8660ea4f6bc53dd9b1b438c9544b935e04d33cea3e5e7f2e7e804147f1abe20e7fb7f7251315a7ec6efddc4b82a1fb88ac63aca6db1b724ca643358e91c7f99ac9e825d24eae78f7a6e78e4fd8c14e58e56124a384b310a7ed14d6e47daea7a761fb977de4bde16177f788fbc2dc26df2773585897963a37146402d2ca261c2ab05cafccf1e809ec5fe6bff0033f7ae2aa76ded6df580e8ad9f51aadb57a0f69627ae2969ae4d8d2ee0a54aeded4d268b2931e517501cdcfb0d6e3ee4f36ee0af1a6e02da06fc30288ff63e64ff008df591dedffdc1feed7c88d6f72fc98dbcee11f0937395ae41ff004d6ca22b361e7dd6c69e47aaeadc3b8b3dba72d5b9fdcf9bcbee3cee4a67a9c866b3f93adcc65abea1cddea2bb25909ea2b2aa6726e5a47663fd7d81a69a59e479a7959e66352cc4b13f324d493f6f597db56cfb56c9636fb66cbb65bda6db0ae98e286348a2451c152340a8a07a0007498a99f42900f27db24f4be5708bd5ef7c34d89b4bf97dfc69cefcfeeeec2d3643b4774c526d9f8cdd75985f1cd9cdcf5d4d3494991aba41245591e170c13efb272fed98e380448cb522159676e5eb4b6f6eb95e6e68dd6256df6e974c31b711515542388f29253820054c360f1bbddcdfb7dfbf4fde1f6cf63390efe48fd9ae5bb869771bd8cd6395e26f0e7ba56a147a55acf6e1460f23cb73dd09d51d22f61eff00dd9da5be376763efbccd46e0de5bdf3f93dcdb973554234972197cbd5c959593086148e9e960124ba6286244860882c71aaa2aa8842f6f6e770bbb9bebc94bdd4ae5d89f3663527d07c80c01818ebb07cabcafb1f2672e6c7ca7cb5b7a5aec1b75ac76f6f12d4848a250aa2a49666a0ab3b12cec4b312c492867900fa9b7f87b475fdbd08bb5789eb7ccfe5775111f853d5d79545ab774f05871fee59bde79d9cb10b2b406415f053fe3a3af8fde69ff0095a3997fe96373ff0057dfa3715f223e6e3d2cadea6fa1bfe7d822f5d1f788f4b0393d148f83ad413f9f2b7fce77c83fefcb7587fd6acdfb83bddeff0095bbfea121ff0001ebe87bfbb300ff008192d89ffa3f5fff00d61ea9659bfdb7f5f71770fb7ae83fccf575dfc86a645f99b382ea2fb2f35f53fd36f6ebff008afbc86f62d9521e662cc07741fe097ae2d7f7b610772f62a87fd0376ff8f6ddd6e6fe787fe3aa7fc943dcf9e343fefc1fb7ae3bf5ef3c3ff1d53fe4a1efde343fefc1fb7af7506b169a75e648ee3fab0fc7b4774b04ea2aeb5eac2bd570ff0033bf87c9f317e306636deda8209bb67ad2a6a77ff553ff00bbf2994a4a17873db295c1e1379626311420fa7f88c14858aa063ec0bcf9cb09ccfcb925bdbb29dcedc9921f56207747fedc607f482f957acbbfb95fde087ddfbde3b0dcb799caf226f2ab65b97a448ce0c3774ff97590ea7f3f01e702ac40eb442cbe1abf1990aec465e86b31795c5d65563b278dc8534d475f8ec8514cf4d59435d475091d452d5d2d444d1c91c8aae8ea548041f7888e924523c72215914d08228411820839041e23afa63864b2ddad2d6fec6ea39ec678d648e48d83a488e032ba3a92acaca415604820820d3a36fb0ff9807cdbeb0da583d89b0fe4df69edbd9fb6b1f4b89dbdb7a8f3be5c6e1717430a53d1e371915653d49a2a0a68630a90c656351f41c9f626b7e76e6bb4b782d6db7eb84b78902aa86a055514007c80c75016f7f74afbbc731ef1ba7306f9ed4ed373bcdedc493cf33a3eb965958bc92351c0d4ec4b1a015249e9649fcd0bf98321bb7cb4ed86ff000396a13fef071c7dbffd7ee73f2e63baff007aff0063a2d5fb9afdd814f77b37b29ff68fff0041f569bfca4be69fcccee3f95986db7da3d9fbefb53616e9dbf9a873d0ee89a4cae3f018cc1e232b90a6dc7490c34a94f81be7968e93efbf692a5a730132b5824fbed55df33de586e1b9f325dcaf6b318fc032b1a90356b655f25355a353b8834a81d7203fbc2b68fbbe729736f26f237b29b36dd67bf6dab77fbd92c90684790c1f4d14d3558b4f1e89cbc5a8f841d750566a75b521a988fd654ff9287b95fc687fdf83f6f5cedebaf3c3ff001d53fe4a1efde343fefc1fb7af750abe787ed26b4a9fa7fd50f696f6687e9a5fd41c3d7ab2f11d26b67306ca4b637ff815f4ff00a8693d8576135bab820e33fe0e94c7fdb41fe9d7fc23af9a2e53fe2d594ffb56643ff70e6f7862bf12fdbd7d915c7f613ffa46ff0001e89aff00f6bff620ff00a03ac7effb6aebffd1deabb97aaf6e775f596f1eb0dd54f1d461b77616b31539923120a796789969ea954837304a4123fb4b75fcfb41b9edf06eb6175b7dc2d6295083fe7e867edef3b6ededd73972ff0039ec9295bfb0b859050d35007b97fdb0a8f91a1f2ebe7e3df3d35bb7e3e76def7ea0ded4725267766e5e6a2d7201a7218b9099b1195a7705964832340c9206048bdc7e3de196f1b5dcecbb95ded976b49626a7da3f09fb08cf5f517edaf3f6c5ee8f23f2ef3df2edc2c9b66e16e1f1f824189636f468deaa41cf0e961f10fe40e43e337c8ceafedea49a75c6607704143bba9209193f89ec9ce8388dd740ca3d1239c3d5c92c3a8154a98a27faa021572d6f326c3be586e48c74abd1c0f38db0e3f61a8f9807cba06fde27dadb5f797d9ee76e42963537d716ad25a3115f0ef20fd5b671e63f51423d3263775e0c7ad87f74f5e6c8f803dcdf307e7e64f29b7ea763f626cba05e9ed938eca25326fcec8ec034b9ccb6d7640bae6a2cc6f4c20cd34b4064fb4c397996e229104d5359d8728dc732f37dc346cb700783183f1bb0d457e7e249473a6ba5149eb8c9cb5cc9cf9f7aed97eefdf75d86d6ee21cbf2cefbadd3a6af0adeddda0826238a7d158bb5b0597478d7724719352a7a037747c98f8fdfcd3bae311d5b41dd5bb7e1f77c2af8475fee7de799a3ea7eccadab65893070e5693298cdafbad64ad923fb5872747479a699c4702cd1ab12591ef5cb1cfd66bb6cfb8cfb5eec40017c43e139f4e212415e0afa24ae14b006b305bfb2fef67dc8f9aae79eb62e40d9f9f7db957d724df468db85ac6a2a640da25bab070a09796137567a46a982315d3495f25fe19fc80f8a19c7c576d6c9aba2c649215c76edc4acb93da99442ec91bd3e5e385238a4900e239846f73600fb8d399391f7ee58264bcb71258d71347564cf00d80509f4602be44f5d19f627ef67ecefbf68963cb1bc358f368525f6dbdd10dde07734203b477283cda0772a332247c3a29eeff5e7fd73fd3d8349eb2840ea23bffc687fc49f7aaf5702bd439650aa493ff1bf7427ab921057ab15fe5b7f0fa9fe48f6657f62763d4d36def8ff00d2f4f53bd3b3773e5645a7c5418bdbb4ff00c5eba2a89a48a587ede968e1f2481810dc0b3209b4c9dedc72ac5b9dccbcc3bb695d92cc9625be16641a8d7fa118a335789d2b905a9ce7fbf4fde2374e4fdab6ef633db4334deebf34aa423c0af8b6d6b70e601e195208babc7d505be9ee44f166aa32c3a907fcc03e61d57cb9ee3190dbb4b51b73a3face864d8dd17b1c86a78b07b2e8a448bf8dd7d1077893736ee7a58eaab5aef2471ac14a64956991d83fcefcd52f356f0f72ba976d8aa9021f25ae5987f1b9ee6f4c2d4851d6427dd37eee7b5fddd3db1b3d81922979d6ff0045c6e970b90f71a68b046d4a9b7b452628460331967d28d332821aefff0022f60ceb2900a74d156b2bb5d49ff8a7ba9e07d3a453c52b9ec34eafd3a2bf9e0e13a0bab76c755ed7f87515562b6dd3c80d5d47c81cc2495b5d54c26c85698a6eb0ad6a75aaa9bb8884aeb183a4136bfb9957dde4548d0f2e7c2a17fdc96f214ff7d75c8edc3fba9e1dc770bfdc1fdef995a79e4908fdd0a686472e457f788ad2bc683ec1d0a90ffc286a34a81507e175079149219be41651bfde0f535bdb29eeada24c271cad5907adcb7fd6ae995fee99b76c37beb301ff004a75ff00bd9754f5f3bfe5f547cdcef89fbb6a36053f5aebd9fb6768c5b629b72cdbb163876e4756ab58f999b0bb7da592adab0fa0532045502ec6e48139bf995b9ab786dd4d9883f4913406d74d35cead2bc6be98eba23f76af6261fbbb7b670fb750f333eeeab7d3dc9b86805b54cfa7b044259e81420cf8849249a0e1d12b9b515214f363cff4f617e1d4fb22332e3a3cbf013e66e0be106ffdc5d915dd23fe97f72e4a869b1d81a893b2b23b061dbd4869333479547a7a4dafb9a0cc364e2caad8c8b1983c3e9bebf48e793f9d179522dc623b67d47d41435f14c7a746af446ad757ca94eb09fef57f7403f79ebce4aba9f9fa4d997668ee942ad90baf17ea4c049a9b9b7d1a7c0029df5d55c5336e1ff41154bff786147ffa50993ffed4bec63febbf17fd337ff672dff5abac48ff00934b5bff00e1749bfee4ebff007b2ebdff0041154bff00786147ff00a50993ff00ed4befdfebbf17fd337ff672dff5abaf7fc9a5adff00f0ba4dff007275ff00bd975d1ff8514c87ebf0be8fff004a1729ff00da97dfbfd77e2ffa66ff00ece5bfeb575eff00934bdbff00e1749bfee4ebff007b2ebb4ff8514c8ac08f85f4408febf213287ff992fbdafbc11a1aaf2d67fe7a5bfeb575bff934bdb9e3efa4dff7274ffbd97557ff0034fe63f417cc0cee4fb1a8fe21cbd35dcb9b968a4ceefedadde75d9bc66e6fb2a6146936e6d9390eb3a1c4d6e4da9d103d752cb4359394533c9358581fccfcd9b273296b96e56fa7dccf1952e0d5a98ef430e96fb70dfd2a7599df779fbb7fba9ec15ada72f5b7de09b79e418aba76ebcd9d690ea353f4b72bb919a01c691fea40092c21a924d73c818a9d3c1f602eb30a50c57b78f42df4eeede99d995f595fdb9d279aee8578f463b150f6b55f5d61a8e4f22bfdcd5c189d9b9ac9e4e550ba42355c74e413ae371c7b11ec3ba6c9b5ce2e773d84dec8a6aaad378718f425046c5bec662bfd1ea04f787dbbf75fdc2dae5d8f91fde1feaa6df3215964836d17378ea451952e5ef621003e4d0c2b32fe198756edf1ebf9c274cfc60dbf93c0f517c02c6e09b3f353d4ee3cd54fc8fcfe4f3b9e96910c7471d6e4f21d5951511e3e8958f828e231d2c258958c33331931fde347d20f2d80a0500172c001f2022eb9d927f74dc72c924d2fbef70f33b16666da14b331352589dcaa493924e49c9e8c1ffd044a7fef0ba8ff00f4a1729ffda9bdb7febbf1ff00d339ff00672dff005abaa7fc9a5edfff000b9cdff7274ffbd975effa0894ff00de1751ff00e942e53ffb537bf7faefc7ff004ce7fd9cb7fd6aebdff2697b7ffc2e737fdc9d3fef65d717ff0085117914a37c2ea320fd47fb30b95ff88ea61ef4deee42ea55b96ea0ff00cbcb7fd6aebdff002697b7ff00c2e737fdc9d7fef65d4ac67fc289bf8697928fe17e3a0999640247f901949f49911a32da64ea7b1e1be9eef67ef0436258dbf2b004fadcb1e3f6c5d2887fba5ad35a3bfbef351483ff002474f2cffd1cbad6473f550d1607355750e12183139079189b000d2ca8a3fc59dd8003f24fb851012ea071af5d85bc9161b3ba95cd15636ff01e8a5ff0cc87fca9d47fccb2fe23fe69bfe007fcae7d3fe03ffb57d3d88b4b7f09fecabf97af58fde343fefd5ff73b471fc7fc3f6fcbafffd2dfe3dfbaf754d9fcdaff0097b49f29bafd3b7baaf1b09ef6eb5c65438c7449a65ec6d9b4b1cd5357b5c325b5ee1a061e6c6335c48dae9cdbcaac9187b8fc97fd60b21b9edf18fdf16ea71fefd41929fe98714ff79f3c67f7dc73ef4e9eccf331e42e75bb61edaeef30fd42716174c42adc67fd01fe1b8029a46997f0306d31b214f5304d353544335354d3cb241514d3c6f0cf04f1398e58678a40b2452c5229565600a9041f78b8e19495208238d78d7d3aefb765c44934122bc2ea0ab29043022a082304106a08c533d5bd758ff356db157d13b37e357caff8d1b7fbc3af76661b07b7311ba30fb8a4c1ef1a1c6edaa55c7e0abe5a0ce6373f89c9e7b198b45a613c33e344d0ae9935067d526ed1ee3c50edd69b46fdcbf0de5a428143546aa28a0255d5d0b05c5468c7e7d739bdc9fb855fdef3d7337b97ecdfbc1b9f2bf336e7712dc4b120916332ccfe2c8a93dacd6f3450bcbfa86364b801b80a000643d19fca97e46f927ea0f931b8fe336ecac4bd1ecbefec3b61f11054bf3aeb379475395d9690097d2a91e46160bcdbf3ed7b59fb5bcc39b3be936dbb3c164aaad7d496f123fc8489d0347347f789fb22c17987962c39f796e361aa58104d705060ac7f4cb6d795233ae6b1b8a1e24f0e8f1f57ec7fe623f1cb65c5b72af19b03f98a7c4caba16a55da72ee5c7eeeaa8b0b35398506c2de51ff0078f2589a48a011ac70557de63a28469869e1673228cf66dbf9cb6181576fdc2df79d848a78723053a69c2392b2205a634b394a6020ad7ac5ef713dc4fbb3fbb9bbcd3f3efb79be7b5beedc7286fafb280dc5bf8cad5d779668b6971e206d4de2c16f1dcebee79df485209f63ff002cbea0f94f8fcd6f9f89916f5e88ec9a5a76c9ee0f8ebdcdb766c453d3cad2cf1cdfddddc9473d7e05a9a5ab42910a6aba848942acb0c0ee07b41ba7b7db1f3478b71b44136d3bd52ad0cb1d226f9ae9aa004fe2899940e31827a943dbafbec7b9fec38dbf65f7077ddb7dc4f6c0b08e0dd2c6ec35fc40053a25f1c4770ce89930ee304133313a6f2455ea8cfb83a4bb5ba1f74d5eceed8d939dd999ca5924458f2b472c5495d1c723c7f738caf0a693214ae50d9e276ff100f1ee12df797379e5cb936dbb593467f0b71471eaae307ecad4798071d7587da4f7c7daff007b7635df3dbae6982f1540f1603fa7756ec403a67b67a4b191c0350c6fc63775cf4cfd35d47bcbbffb476a754ec4c7d4e4339b9f250d1afdb53495428a95a445a9af9d23b288e046b2ea645691950b2eab86f97762bbe63ddadb6bb5c173576a54220f898d3d38015156217891d16fbf9ef3f2f7b17edbefdcff00bf1127d3a84b6b7d611eeeee4c436f1935f88d5e560ac62852494a91191d5ba7f311ee4d9bf183a736eff2d5f8f79280c3808b1798f945bcf132a2c9b8f78469156d1f599aaa6b24f4b85aa095b98b16f2d6f8626292256c4d25fb8bbfda6d5636fc89b09d36b02a89c8f323222247135fd494f9b9030430eb07bee2bec973173c733ef7f7bcf79236b8e6ade2795f6b4941a4692763dea2b9252311ff008a6de9811db23380caf038a27c8e468b17475190c8d54547434a864a8a99db4471a02072792cec480aa01662400093ee17c9341c7aeac3ba431b4b2b85451927a91d4bb37e437c84c76773bd05f177bcbb976eedecb0c2e573fd79b2b3db9f1743917812b21a1aeaac3e22b69e8f21250ca937dbbbf956391588b3024e2c397b79dd637936ddb679e25346291b300695a120115a669d44dcdfef97b65c83796d61ce9ce1b66d7773a178e3babb82079230c575aa48cac575022b4a54115a834158fc4af9fc7fee9fdf2cbff4526f4ffec77daefea4f35ffd182eff00e70c9fe6e827ff00058fddfc7fe0d6e5dffb995a7fd6ceb19f88ff003fcfd3f97f7cb2ff00d149bd3ffb1df7efea3f357fd186f3fe70c9fe6eb63ef69f77f1ff00835797bfee6569ff005b3ac47e22ff003003ff0074fef9643ff2926f4ffec77dfbfa8fcd5ff461bcff009c327f9baf7fc16dec057fe9ea72f7fdccad3feb675c0fc43fe601f8fe5fdf2cbfd7ff00449bd3ff00b1df7afea37357fd18aeff00e70c9fe6ebdff05bfb000ffd3d2e5eff00b995a7fd07d70ff6503f9807fdebfbe597fe8a3de9ff00d8efbf7f51b9abfe8c577ff3864ff375bff82e7d80ff00c2a5cbdff732b4ff00a0faf7fb281fcc03fef5fdf2cbff00451ef4ff00ec77dfbfa8dcd5ff00462bbff9c327f9baf7fc173ec0ff00e152e5effb995a7fd07d7bfd940fe601ff007afef965ff00a28f7a7ff63befdfd46e6aff00a315dffce193fcdd7bfe0b9f607ff0a972f7fdccad3fe83ebdfeca07f300ff00bd7f7cb2ff00d147bd3ffb1df7efea37357fd18aeffe70c9fe6ebdff0005cfb03ff854b97bfee6569ff41f5eff006503f9807fdebfbe597fe8a3de9ffd8efbf7f51b9abfe8c577ff003864ff00375eff0082e7d81ffc2a5cbdff00732b4ffa0faf7fb281fcc03fef5fdf2cbff451ef4ffec77dfbfa8dcd5ff462bbff009c327f9baf7fc173ec0ffe152e5eff00b995a7fd07d7bfd940fe601ff7afef965ffa28f7a7ff0063befdfd46e6affa315dff00ce193fcdd7bfe0b9f607ff000a972f7fdccad3fe83ebdfeca07f300ffbd7f7cb2ffd147bd3ff00b1df7efea37357fd18aeff00e70c9fe6ebdff05cfb03ff00854b97bfee6569ff0041f5eff6503f9807fdebfbe597fe8a3de9ff00d8efbf7f51b9abfe8c577ff3864ff375eff82e7d81ff00c2a5cbdff732b4ff00a0faf7fb281fcc03fef5fdf2cbff00451ef4ff00ec77dfbfa8dcd5ff00462bbff9c327f9baf7fc173ec0ff00e152e5effb995a7fd07d7bfd940fe601ff007afef965ff00a28f7a7ff63befdfd46e6aff00a315dffce193fcdd7bfe0b9f607ff0a972f7fdccad3fe83ebdfeca07f300ff00bd7f7cb2ff00d147bd3ffb1df7efea37357fd18aeffe70c9fe6ebdff0005cfb03ff854b97bfee6569ff41f5eff006503f9807fdebfbe597fe8a3de9ffd8efbf7f51b9abfe8c577ff003864ff00375eff0082e7d81ffc2a5cbdff00732b4ffa0faf7fb281fcc03fef5fdf2cbff451ef4ffec77dfbfa8dcd5ff462bbff009c327f9badff00c173ec07fe152e5eff00b995a7fd07d0d5f1f7f938ff00312f989d8bb7f686f4e90de5f187a763c8c151bd3b03b670d53b765a0c5c13c62a9f15b6730d8bdc5bbf38d0b30a2a5a7a68e8cce2f3d44280c8a21e5ef6cb99372bb8e3b8b092dadabdf24aba748f3d2a68ccde9414f52075097bcff7eef65f943976f2eb66e6bb3def780a7c0b2b0944fe2c943a4cf7081a28625342e492c0652391a83adbe3fe194be0d7fcf1799ffb241ff64ebfe2e91ffc7a3ff3f1bfe027fccd9ffabafe9ff9b5ef20ff00d6ff00977fe51dbfdc1fa5e3f87fdf9c3fb4fe97f2eb8e7ff05ffbcdff004778bfe56afdfbf01fedff00e517e2ff0070bfe13c7fa5d7ffd3dfe3dfbaf75ef7eebdd5157f328fe50987f913519fef1f8df1e2369f78d4473e4773ec7aa920c4ecded8ac445692ae0ac223a4da1bf6b5508358e3f8764a720d60865792b844dcf3edac1beb4dbaecba62ddce593824a7d7d15cff0017063f150d5bae8bfdd37efd7bcfb471edfedffb9e67dcbdb8521209c564badb96b4a283533daaff00be7fb48947e812a042751cdf3b377875aef0cbf5e7646d3dc5b0b7ee06c731b2f78626a705b9282279248e1ac7c756a23d562eada2634f5b4e66a2a941ae19644218e375fd8de6db73259dfdabc374bc55c107edf983e44541f23d77239479bf9539fb63b4e65e4ddfed773d8a71d934122c8848a554d32aeb51a91c2ba1c32838e91d2a46d7e2dfd48fc7b4273d0a3c142323a59f5e76c76b74ee61f3fd4bd95bebad73327884f90d91bab37b62a6b23824f247057b61eb69057d287fac5309226b9054827dabb2dc770db25f1b6ebe96097d637643f9d08afd87a08f367b77c95cf96436ee73e53dbb75b05ae94bbb78ae0292284a78a8da1a9f8928c3c8f5679d55fcefbe64ec26823ec2a3eb1ef5822658df2bbe368c781de71e3c05071f45ba360546d64d04aeaf25651d74858dd8b71ec7db77bb3cd166025e7837695e322697a7c9a329fb5831eb0b39effbb67eeffcd124977cb91ee9cbd7854d16d2e3c6b72dc7534578b3bd3fa314d0ad0600e8fe27f374f80bf2cb69375c7cb9e99dddd7506415616afabc75276c6d3c3b32d8d6e3f3f83a2c4efdc5cf0ccdad043889d45812588b7b1c43ee9f2d6f56ad61bed8c9044ff10651710fe740ae29c410848e20d7ac3be60feeecfbc1fb5fbcc1cd5ecc73ddbee3b95a92d0c904afb56e00ff000aeb76b72ac30ead76aae095642a4f4159df3f047f970f5776b7757c42eeadaddebdd9db1a36c748e1e3c95267335d50991a0a84c9ee6dd389aba6a7ca63a836cd34f23d126469926adab9129dd248da69204cfbd727f246c7b95df2a5ec53eeb78d48c06d663c61886019123a96557a967201240aa89397bd9dfbd1fdeb7dd8e43d8bef39cb97bb7721f2bc0cf732b5b9b58ef6aea5d637889827bcbc2b1c52cd6a52382d91de358dc8597596ec4ed9c6e3b2196cdeeace5567f75662bab72b9147ac6c8e7b2d96c8cf25656d7e56a6679244a8adaa99a4966a86d4eec4fa89f701bb4d712c934ae5a5724b313524935249f324e4f5da38ced5b058da6db650470da5bc4b1c50c4a15523450a888ab454455015460000003ab13fe5e5fc9c3e4b7f317dc184ecaeeea3dcbf1fbe26d398327065ab71b518ddddd8f47246b3d243d6986cd5222e4693231b8d7b8aae238e863626952a9c18849dc99edb6e5cc2f1dddf23db6cfc759147907a4608e07f8cf6fa6a38eb00fef3ff007e2e4ef696def397f95a7b6debdc5a955b68e40f6d66c0d0b5ec91b7f68be56ca44a4e1cc2b463be4f44f42f527c68eafdb1d37d23b270db07af76951a52e2f0986a758ccf39445accc662b181accd6e0cacb1f96b2baa9e5a9aa949691d8fbc9fdb76cb1da2ce1b0dbadd62b5414007f324f12c7892724f1eb83bcedcf1cd3ee2f32ee5cddce5bccd7dbf5d3ea792435a0a9d31c6bf0c71203a638d00445c281d0bfed7f414eab6b72fcf4df98a5ef6dd180f8b7bab747537c6bec9de7b07b5bb0297b236551d5ad2ec78b195d9fcf6d1d9952bfc7b3c71185ca2575553cc291162f44334ee241182a7e6cbb8d375ba8b627936fb2b878e5712a0348e9a9d108ab690751048c7024f5955b57ddcb96ef2ebdbce5fdcbddcb6b4e73e6ad9ad2f76fb56b0b968fc4bc0fe05bdcdd07d1119644f0a391165ee20c891ad2aa1dcdf38b3924fda1b97a7ba1b3fdcfd37d2091af66f666177bed9c054fdec7b4b13bf3358ed83b3b2f11ca6f2aadb7b433b4759545e5a18e46a810c0d24ab2057ee39a250bb85cedfb535cedd69fdac8aeaa70a1dbc3423bf42904d4ad780a9e8a764fbbfedf24dc97b0f3a7b8506c5cf1cc62b61692da4b34403cf25adb9bdb98dc0b61733c4e91958e7d200924d2a70cbbaff00991f5bed7ebdee9dfa9b3b3b9e6eae9ba72b36aedcc2e429e7cd76b6d6efadb541bb3acb726dca6968a29a813378996b75c12c729865c6d42b3fa18ab571ce969059ee9762d1dcdb98b4aa905a549903c6ea298aa96241ad34b0e8c767fbacf326efccbedf72e9e62b5b65dea2dc7c79e54658b6fb8daee64b3bcb7998390e56e16244752a1c5c44c06455f7b0bf98375f6c8a8de736376b576eedbdb7363f416e2c0ee2c7ee6db989c7ef1dd3f2572995a5eaed9b45579ca8a1c3e1a1abc46225c9d7662bab61c763e86d2ccca84317eef9b6d2d8dc1584bc2890156d4aa1dee2be1a55a817b46a662680744dcb9f771e64df62d9127dcc5b6e97375bbacd6ff4f34b2db5b6ca105e5c948b5492b78cff004f15bc71992494100e1b4a2d3f98ed0e276cf6cd66ebeada3abde3d5789eb0dca76cf5476ceccedbc5ef2db9d9dd814dd7548fb6f746df869a9e3dc582cf54ac7558caca7a791da488c6ed1cab2049fd7258e2dc0cd601ae20109d314a92ac8b348221a5c01dcac7b95803c28686bd093fe05f92f6ff0092c6d7ce1243b2eeedb946d2ee3b75cedf35a4db6d935fb89eddda42d04f0a9315c44ee3120640f1952f7bcfe776e8d9fb870bd7199e89c7ecdedecc61f2bbda5d9fda1df9d4db230581eb7a5c8c387c1ee0cdef88f219bc34bb9375e57ee29e97078e5afa887ed6496792380793dbf71cd335bcf1d8cbb7245b932190a4b711a2ac75d2ac5fb816720808a091424903a2ad97eef9b6ef5b5dd735d873b5dee1c8f1dd4766b73b7ecf7d7534b7a62134f125a9313ac16c8c85ae6568c49ad5628d9aa073db7f3eea3b6283af315d01d2995ed0ecfddf87df3b8374eccaaec0da3b6301d6b88eb7ded275d6e9abcf6ff273386cd2556f088d2e27f85c556725130a9023816568f56dcd8db925a26d3b699afa447674322aac623731b55fb8355c109a410c3bb03ab6f9f7738b91aeb992efdc6e7a8f6de53b3bab6b7b6ba8ace7b89afa4bbb55bd87c3b4668648745abac9702774685cf83467a74adfe5f1bfb7ef63f4def9dc1d8f5f9eaadc907c83eefc29c7ee2cdaee1afdb18ec46f3aaa5c7ed18f2b14b35354d0edb857ed6130b1874a5d383edfe4fbbbbbddaa79af59ccff00573ad19b5150b2b0095e0428c0a6283a29fbcdf2e72e72b7b89b5ed7cab6f6e9b39e5cd9e50d0c3e024ef2d842ef7062201579d8995c3776a63ab3d166d91f2ffba7aaa0f965bc7757536f6edae95ea8f959dbd41ba3b3a5ec5db51d6ec2d878eafdb74cb81d95b0ebe4aadc79dc3ec2a1a93575117f90c2b1cce298cc564284b6fcc5b9edf16f9773edd25c6db06e130793c45d49187028919a9658c1c8aae2b4af5296f5ec9f20f3a6e5ed472eecfced65b373d6efc9bb5cb6f642c66f0ae6f5ed5e42d737685638a6bd65a2b8496ae57c52b51d19eca7cddc1e27aebbd3755475fe667de9d35db38ae9fc6f5950e731b579becddc5bd5f6ab753556cfc8454ff6cd8eed0a3de1493503c91fedaaca1f8898fb3b3ccf18b4dd26366df576d7221116a15919f4f8454d38481c30a8c0ad7875142fb077b27317206dd1f3343fd5ddf3627dd5afcc2e23b286d85c0bf8ee232dabc5b196da486501a8ce534fc63a56fce2ec0debd61f107bb7b0f65e664da5be36c6c9872788cce3d60c83e1b28d95c4c12cb4cb5d4a60ac48967751e4846b53fa549e1fe69babab2e5ddcaead65315da4628c287492ca2a2a2878f98e8a7eef7cbbcbfcd3ef6722f2ff0030edab7fcbb71792096190b209a3482670ac51832d4a8274b60f99e8a96dbf913dcb5d43f1fba8f7eee79705ddfb2fe52d37c7df90af868b1f4d26f4c055f56eff00dc3b43b02829a7a165a2c3f63e1a8f1d99a69a08a3896be1a8863f4c2d1fb0fdbef3b948bb3edd79394dd22bff00a7b8a506b5f06464900a6165015d4814d408f223a99b7bf6bb912ca5f73b9e396f685b9f6fb70e505deb64f14bb0b597f79d8c17564ec1fbe5b091e7b395598b18991ce5d5bac31fca8ed2dadf0e7b2f6ee6b70ae5be5aec4ee5c8fc41c5e527831fe7dc7dbdbc776d3613aab78418fa6862a55a6caecedd18fcc02f0a44cf4d302a5548f7b8f7dbf8b972eede59b5730c5726ceb8ab4acd48dc0e198d849c29838eb57bed17276e1ef8f2f6f3b7ed620f656ff614e66310672b0edd0db992eed19c92d55bd89ec8d18b0f110d4541e973895ed8c07cbddf1b0721f20fb5b716d4ebcf8b3b53b4a0dbb915d890e1b39bd32736f1d8b92aacbad26cb8b2dfc325936f45958a9a0ac84c59276264682d00571fd7c5cc57f6cdbb4ef6f0d8472853a34976f1109344ad3b03015c35734c741bbbfea86e3eca7266fb07b73b3db6f1ba7385e58497082e4cc96b08b1ba8d50b5c98c38fa992067311d50851a4382e417da993f94f82f855b37e67eddf941bdb796eec775047dc1be3aabb536ef5ee5fad377e369716d9cceedcc3d46d8da1b57776d1aa14304a28eac57d6b6b55491486670530c9bf43cad6bccd06f92c972b6c269229446d14800d4ca34a2ba1a57490c734a8ea45dcecfd9fdcfef05cc5ec26f3ed2edf65b04fbf3ed967b86df2de457f66ed2986099c4d733dbdd20729e346f0a55753210c00e8c0ed6dd5be77a7cdbd8715076e76663faaf787c53a4f91517563c9b34edc8f3b5db9f6eed0a4c3544c7683ee0fe04b8bcc355cb0a57898e446b13087f63d9d5bcf7573cd36e17709c5849b7adc08aa9a7517094f83569a1a91abe2cd698ea2dde369d8363fbbeeef2cfc97b4bf37da7394bb335fe9b8f1cc31db35c971fe31e0f8be2ae80fe153c13a4a96efe827d85f287b6b25f2e63de39dde5472fc51ecdee5ec5f89fd67b5128b1f0a50ef9d81b7b1991c47681cc330c8e468b7def9db3b936ed346a3c492c31b7ac4aa632eb5df3706e628ee65bb0760b9b896d634a0c49180564af12246495079607af435e62f6979360f64eef62dbb97244f78b63d9ac3982fee8b49592cef64749ec4c67f4d1aca0b8dbee5880188696a7b4d6dc7dc87d61575ef7eebdd7ffd4dfe3dfbaf75ef7eebdd7bdfbaf7442fe7175dff2f8ed2c060766fce3cd7426de9b291d7c7d7d99ecfec6dabd59bf71f50d14915555f5ceeec867f6dee9a3a8a77935c894350d0bbaaf9a3702dec2dccf69ca57b0476fcd125a22b574196448dc7a9462cac3f234f51d4f9ec47307de2396377bade3d85b3e61b8b88990dc47b7da5c5ec0e2a0aadd411c5344c0d280c89a802743039eb58df90ffcabba17055394cdfc4dfe685f12775e24cb2d4e37acbe44f73758506629a0b8614149db1b2770489907e4ac46b3030d805124cc6ee609deb90796c3492ec1cf3b715f28e69e2047c848ac41f95507ccf5d70f6c3ef83efd78567b7fbb9f74fe74130003de6ddb46e2cac7f88da4f02b27913a2e5fcf4a0c0eaa73b1ba97bababf2359439ac47486f2a5a56222cef52fcadf8f9d838bc94615584f45498adf2fb82246d560951470ca2c6e9ee3cbcd8eeac9d95aeece451e71dd5bb83f6699357ed00f599dcb3eefecfccb6d0cd1729f3459ccc331de6c5bc5aba1f4632d8888fdab232fcfa2fd5bbeb72d248d0c9d63ba19d7f34f5b8aac84d895e2a289ea216e47e18f1fe07d94b44ca68596bfe981ff0001ea428f7a59535a6db774f9c3229fd8c80ff2ea7e1dfb5b774cb4fb6faef6fd24b210125de9dabd77b2e105ada7cb26e9dc3b7a08d47e4b4aa07e48f6fc56324e4059611fe9a5893fe3eebd126e7ce967b523493ecbb93d2bfd9585fdc1fd96f6d293f974753a77f96af6cf744d4355dd1f3afe06fc5fd9b55034b94825f937d45bcf79d253b863a1301b237ad6e32a2a345b5ad46e0a408a79f502bec5db5725457651aff9a369b588f1d5750c8e3ec547d35fb5c758ddee07de8f7dd861ba87947d84f7137dbe5345f0760dced2d9bd4b4f736a250bf35b77afd99eaf9fe0e7c06fe453f18f39b77319af97ff0014be54f790ca6346273bd97f21ba37258ba7dceb5e1b1dfdcbea8c76f5acc5479296b5a2108af6ccd689d14c52ab1b7b98396f967db6d9e48647df6c6f770a8d2649e1235571a220e456b4a5759af03d7357de9f7c7efb1ee3daee5696ded473472c727786fe2c769b5ee49218b4f7fd4debdbac8542ead5e1fd3a69275a902bd6cf54df6ff6f07da787ed3c317dafdb68fb7fb7d0be1f078bf6fc3e3b69d3e9d36b71ee6214a0d3f0f5cdc93c4f11fc5af8b535ad6b5ae6b5cd6bc6b9af59bdefaa75ef7eebdd51dec8eb7edeed0c47f303da54ddd1d61d45f1c772fcb8f90788ee9cdee3da75f2efdc1ed84dbdb27fbeb59b7f77576eec4ecdc3e3737b5a47a77acc95194c4ac6d52054a3e858c2cacafef62e6d81b73820d95f71b813164ef0b44d7472e11559704b2f68a9cf59fdcd9cd7c9fcafb97ddc3758b90b75de3dd2b5e47d8db6d58ae296b25c133fd287b45b57b9965866a322c1352e1b4a10a41a8d6fd05b87f84f7e617e297c95e86db1f1abb6ebb2153daf3d462577fe7ba6b2f2f58ed7db9baceccdc786df345b6218f23b0b1b8dacfb6ce78ff85a4cb540cb148aac67fba24f0f788f65df2d9363b86633546b685bc3559343870a2a80121c76575703d0087b9769f59ed85efba5ed36fb73eecec90c2bb691235ac5b9c4b7934d646e6d64b469a431dcbc91abdab52e74f84406527a9589f8f5f1831df227e3d7616ddefaeaa9369f597c7dd89b0b17d5d5bbab6666723d934d45b1b77e17a2b7fd7d6ff1b48f2f451758e5f71cd8f9528e48f20911aaa6711d3c845e3da3634deb6dbe877583c186ce38fc2d487c4a2388643dd9fd2326934a301a8607492f3dc9f76ae7dace77e54dc7dbdddc6e7b8f32dddeb6e2b6f7318b466b9b67dceca31e17e993b84767e32f881a2919629175c8a086db77e2efc79c27c76de9b167f959d2b575353de1d5bd95d73da396aeebddc580da989c151e15be32f5cee8c364f73c387dedb7e4d8785920a2a39aaa08b2f4d5d512d22e937f6860d876a8b6596cd7982dcb7d5472452931b050a01b78d94b69917c21a40246b5248e85bbc7bc1ee26e1ee9d97335c7b31bc241fd5fbdb1dc36e54bd8649da59245deafa1956032da4dfbc1c4af22a37d34f1a472e411d2921eaba5df7d51d9182ddfdeff0006b6ac35d9ce9b91abba0b6760769e07131ed7edfda7b8e8eb77967ea37d55642b6bf7a55e353178ea06fb7a5a7afa85d0f54eea8151b0faab1b98aef75db52b2406b0a2a28d13230d44b924c840455c00c45351c741d8f9c3f70736ec97fcb9edd73bdd32d9eeaa5374ba96ea67173b5dd5bbf811ada222a59a48f732ca159de28db5f8480b74217cadeb0d895bdf188ed3c0770fc5adb5dc94bd5b4db3b3dd59f27b1bb5f756d8dcfb08ee8adcae077353632a771e1b776d1c8e3331357431642963a8a5ae04c1225e3d414ef7616efbc25fdbee7671ee22008f15c057568f5128d4d4ae841d4030a86c8231d11fb55ce3bc5bfb6971c9bbdf21733de724b6f125d5b6e1b3493db5c5bde7d3c51dc5bf8a209adae639225b779207d324242c8ac03d0b0edfe9ddf716f7eb9dff00f1ff00e46fc57abef1a3eb1ddbb437bedbc3ec3a2a5eb1cc74fe77b3a4dc7439fd99d7db0f7ec99ec655ec5dcd1ff0d5c8cb5935365a769054bc129081a8f6dba17b6979b56f565fbd05bb2488231e1b44642cac91a49a94a355431243e4350f4617dcf5b0372a730f2cfb85ed5f340e417de22bab1b87bc905e43b8c76290cd05c5e5d5a18665ba83c399e158d24b7011a2050e4ca7c2beb38baabaaf74eddff4b1b33b972590ee5ed8dd1b8777ec5a7a4a2c343b93716eaa9afcdedfa8c75167771478acce0eb59a2aca56a967a79ae8ca34fb37e59b15dbf6e9605dc23b963713333a500d4d212cb40cc01524822b838ea34f7e79b27e73e76b1dde5e4cbdd8614d976d822b5ba2ed278105a4514330792285a48e68d5648e4d003a30604835e8a16e1f8ebbeb27b57e52ecda6f949f1f70df177b53bb7b5f7977fe7e3c3cf2efeeb5a5cd55e22a37cf5e4dbc27dfe363e0328983a28e9abaab274d4d2e1c4e6534f35c29219767b87b6deadff7f5aaec335d4af31d3df1866ac9197f13429f262c015f4ea63b0f73b64b6defdabded3da0e6197ddfdaf61dbad76b8ccdfe2b786280c7697ab682cfeaa5049f160486578e7217bc006bdee2d93d55bbbf996ed2cc526ebc46d9d91d57d53d3b98dd7869f3f8dc3ed9ec6eedcbd4761e1be2d61b154f532c516eacde0f6557ee0c8522d34924c1e9a8046aebaada96d2c27e768a7170a96f0410ea52c02c939f105bd057b99632e45388d34ad31bb1e64e6fda3eea77db3bec935d6f1baef1b908675824926b3da23fa26de4bc814886de6be4b54903502b990b152eba8eff00cc8d8587ed0f8c5dc5b033fbff006c75661b746d65c7643b0779cf4b4bb5f6b41fc571d50725999eb72588a58e975402305ea615d6ebeafc113731da457db26e16935da4113a80647a055ee06a6a40f2a711c7a81bd91e63dc3947dd4e4fe63daf96eeb78dc2d679192ced8334d39304a85630892312a18b9a2376a9c532001ee9e89ea2de5f31ba07b671bdddb0f6376b75de5b051efbeb2abdc38639eed3c33e1778ff00a3380edc39ea3c952e7f1d5155977c6d4b52ced53426a91011092a4fb96d3b6dcf336d1b926e9145b8c240922d4baa5055fc2edd40865ac854d0d5750e031277237b8dcf5b1fb09ee47235d7b7bb86e1c95ba47235a6e22097c2dba459ed0dfe99c42d1b43295b45b88fc4511cbe0b1a3480334ee0e80e9dc87cedc6779cddf5b031f062f2db4e1dcdd06db836fc794cd7c98a5d8dbbb6f757ee2c9d2b6723a98b70c7d5b9fc8cd458b7a335956f4b1d6c7a960bad5f68db4f35fef5fded10a140d6fa96a6e42308dcf7543f825a8b4a90350c0e9fb6f7279ea3fbbb0f6e8fb71b83ea8ee1edf79f067d29b135dc125e5b23785a1ad7f794717893893c38ddcc0d467a154d5f547634bf32bb13b1e4eede8f87099ee96a1d97b83ab1b0191ff49b85eaaa49f78566dddd1535877ba4343e5df196a812e466c6fd8cf4b4ad4e8ab32b48aa0edf74798afef4eeb6de1bdaaa345a4f88b1032147275e3bd9aac57490283209e89a2e73e5e5f64b93795d3dbddf4deda6ff002dd41b8f8ebf453dfbad9adcdbac7f49de56da1834c2970658de5123551d57a03b69fc62ed0c97c54da5d51d8bf32fa829be24e0763e33199fdcfd47b261c0d66fbea0c652c6f570d7775ee1ecdcf6dbdb985cce3a368eb325418eb9a66631cd1336b42bb6d8eecec16d617dcc96e797922019a2409e24407e299a4650a47c4ca054798e87dbefbb3cbd1fbc5bef37f2a7b19bc27bd577b8caf1417f74f706cf71958e61db22b28269278dcfe945348c55c0aa3103a1477274ff0068657e4f3efbea8ef9e80db94b53f1872bd71d7dd789b7f2992deb8deabc8d563eb70dbf6866c66fba75abc5e27b022a3f0e420a1fb16a42b4c0f99d640ba7db6f64df1eeec777b54ad898d23d24b8889aac8089320494a305d2476f1cf413da79eb95ac7da4b4e5de6df6cf7fbb58b9b52faeaf4dc2c76b2dfa2699ac9d5acc85792cf56a85a6f195c996853b3a2bf2fc64fe5e8fd29b47aafadfe40fc55a1f93d829f6053ed8ef5fefeec0c8ef4cc779edadcf87c8c9b8eaf018fdec991afcaeedde7453472e2e197cc1aacc111d688411becbc9afb6dad8596edb7a6f08d1e89c3c6643323a9ad03d4b3b020ad6b9238f52cdbfba7f7a0b5e7ae60e71e6cf6e79cae3db1b982f8dd6d4f697c9671ed7736f2c7e1eb7b631c515b40e8e931508be12b9a2923abc4f727f5807d7bdfbaf75ffd9,'image/jpeg',250,111,0x89504e470d0a1a0a0000000d49484452000000640000002c08020000001c8b8a020000194d494441546881d55a79785cd5753fe7bef766d38c345a6d799125cb96f7051b63bc8221100a214d1ad2f44b809210c0ac018c177028490d3518421308810275210442d3524259621b0cf102c6c62b961779b7655b92258d669fb7dd73fac77b6f66b4d0af4dbeaf69def77c357eefcd997b7f67fb9d731f32337cd1c1cc80000c000c8c0ca0eb1c8b73778cbbba3893856c8e53290c87c01fc09210d654634d3546a3100c020e200f71a0ab7f3e87cacc8838e008000e522025b49e912d87391ea7b676bbf5943c7d06d229324cb06d66062184a280cf2fa2a5caa8d1da94c9cab42950371c15a5f0535ff02b7f462332f37f87976df3d1e3b2791fb77558875aac032ddcd303cc2c10c0b33a60e70f023033129150d48a0adfac99dad55f51a64e024d4340863ffd6affd8b13f58790ca03b666fd9c667dbcccf3eb35b5a2893014440342aca955c4ed50dd7fc18d005ad608dc8c04c225ce2bfe452ff4ddf15c38616591830c3ff8bc5ff6f47221ac0a098687f0beddc2d0f1dd63ffe849209679d049c1c559fbbef1e7f30a86fdd16f8745be8e8894022850c8490c7ceb135440026201083aa83f7dead5d76292a8a83e4ff8b95ff616ed8278cb165cbed3ba9799ff9e9566bcf1e66c7cd9880130dc3734bee6a1c3f110e9fb01b871a7a4ec632a9bdfbe5962da57bf6f91349204666000400660266004006d414ffb5df09debe10340dfe6c233d1251af0b24e5a64fe9e0417dcd5ad9da0a80844c4c049c18dfa42dbdaf6a50856a72eafaef07ff7eb95d536ebcfe1fc1fa0665fab454a844dfbbcfdef4b17af050f84cbb90844c8e3f02021123b076f55525f72f11e1f09f68b17fec517043000022b96d07eddaa3af592bcf9c7690924c365366649d58f1d0a0e683b94828a406324b96636949e8c9c712b6454bff4e3574317aa46fde1c31778ee10fc48f1e939f6c29f9789b3fd6a398263032130020007eedeaf21f3d08a29025ffc0d8ef7cebff246fe4675af80f33d38183f4e967b977de95274fe691922c1313c6fa972fadd9d59c7eea19fdf9672a377faabfb09a81301a0d3db222a12af2c11ffbda3b189805c0b05adffc79feb973b2f5235227cf181b376bdbb6979d38297206032188d0a21ff8affa0b4031b0fabe88f7e5132e7c3131f41eebf3e98f3a10a13482810060b11bc613f6dbbf33376e3476ee4244894c4c36cbc484b1a1079655eddc9b79eae7a64ff3bdf99bd04f9e36d7be4fc0cc0465d1f0aa4752be80bd7899afb30b9cecc0c4aa2286d66a53a7f82ebd34d3d0402fbea4bef59f480ccca028be9933d5214301d885263f7ac901000008d84bb09c67278524024e302d96e03de9e47304e6e2eb7f2858180e6b577c499d3dcb034b4ab9ee436bdb36fdbd354c929989a50dd4337562c992c5d5db3fcffdfc1932adf488a135afbc64df7ca76c690106c9c4cc585911fe871549a1d00fffcedfd1c9cc000e2171121fabf7fc401d35d2b87b09eb3977914c401ecdf056db8bb23100930b5eff05172e7bc832e7f1f19e65843cae031614bd6e7de1630c8c60f97cea8f1f727d818e9fe0b36dc6c64d4c128025d81650cfc471e10796556dd96e3cfd0bb06c06a2c6916a32c59d9dc8c800021011a8bb3bbd785944017cf461bd228a8e780604440024b07ebf418c1b07c36ac125648c802810040222085118d1190190ddbb8828d039bc67105cf1e0dd031488422002b8620105821085af389f8b4e4677828cc8ee0ff57d068460818088ba71f4f155aa831eed3d60eede4df104234a9612b8e7fca9e1fb1655fdfe13fdc59740da8e1ea9b111da3ab827cecc88c0c08211002891cc2cbabff4b187938f3faa2f7dc07faeaba03b64d8fd39198698364d1e3ec600e829dfd2b47859a9e3b99e6381706cc423c71ecd252d9bf35b66c0b4905cdb430200f2acc88bbe0c36821108e8aa6a0703fd8951b1a93200313946182d292d397d1afb7003cfd6102177e2a40a007cfa2cb777d8fbf6b3609ba544ee993c21b26471e5e64f8de757a39404c0c02484367a241d39ea1001644010802c180098ba3b334b97973eb92ab1f2617dd1d280072830a2b4ad8d9bd5f973e51b6fa22d19180198e15c75e5c1ef7db7baba6a201781e250ce88605966260367ce94ecd859b77d67309703046027882132db42b40fae39316a5457e3487f4d4d285ae60b84344de37ce42bb83bbb350648cbceda765611fe19431ae1a65b309b73bcb930ba9f00985460962d87e5b1a376322e9908a8e7fca99125f7557cb8c9fce7979924b8ce837628e4af1b2137fd1b39d6912f8c98050083a0eeaecc7d4bcb1e7d24fe0f2bcc077ee88bc599019910d0dab2c5ffc3e5100973ac07193d43e2610df53367cc60e281834a1fec009929934977ecd9033f7b26b87397b7286aaf19b47bc674bee082a67163e78e1c591a09a3a2e58b300fa8fe2646a61533ad1e4d2d0d769a7145e5822d7963d13705e474e88e19cdcd36930ddc3d7d72d903f757aedf6cbef82f2ca553f739f194caa3818a4ad972c8751c2f8c0300020ac7333a3a5377dc1df56bf4c42a33124120e7abb0730f99a6983ebd90d81c4763262266ca1f4cc4e45df5fee3d2620666f2fbd5613326d7ac7e4e9b3801801961dff8f11bbffdadf1377eefafffe65bd3ce3bafac340a426192bd4412b1fb33ccecc8752f9324d779206f7abdc6fc2dc1dd31eaecb4bace11536cfa94b265cbcadf5e63ae7e198991191980919d795655fa7d3e3a7112dc6c05c0081e664e484504c864d28b979503db2b5718d172074c8cf5583b76a8972e00457584b1639be0c46744749428198881f2a10d391fcbdd2f114b228bfd6af0ae3b1995e689130e5c79c557aff9c6a44993bcc0e59459c44cc492411290c3238bd00004c0bc3d7b7ffb935228181b08ea38274f9d22a2ee1953234beeab58bfd1f8e5abec4474f68a38666632c636899e3819067ac118f389df9902a370e47776a7efbabbb22c428f3d6294461c74ccb5eb948913b034d2cbe13cf1449665a70c3366985d8619b3eca4241d4882539fbb26cc00288426840f4111e3c7b65d30bde5ca2bbefecd6f0e1a34d89914b165cbac69c575b3c7b0ba4dabc7347b2c2b665a3d969d2632d105c0515661fede7cb0df58ec86b198dd7a1afcfec8b5d755654df3e55f010330e4fb379e2e10ea86f3be03c8cc4c4e6eccdf65cf0a00bdfc984ca7efbaa75c5568e50a235a86ccbc6d3b0e1e84e3c7176528ced34649866176e97a8740f4e76c2d9553922991cd223bed8bc2af085415252884c6e5e5fbbf7cf965975d565e5ec1004c96652775e35c563f6b989d08d2a70502e8f319e4cb5abe9ce5cb195a4e5752498ec75d25f50f657d4872ef43e5449a9349cd92816d3bc46d0bad39b3d5f51f82537139ba02260612c2377912bdbfc133cbbe8210189d8141019048b2ab3b7df7a2ca9f3e197bf46163c9fdbe44c2d8ba55f9d225b4f96396121de3f4282191cd64825045caecfed60dd4d68e000a2a4a5999eff22ff97f70179695b2eb38088e9ba8daec4b2fadad1dc24c449665c50db35b4a3d141a144c60f6c5d7e2ebd6d9274fb36d39d315802aa00068993ca9fe99a7ababaab0c8fd3cc3c23e63f121389d02c30000f9ee5a9414b8f3361908a213d65dd3470436838160558ddcbfdfc1dc8d59452110f3e5073000084605807b12e9c54b2b4a42f68f1f3422116bdd07ca9489e0f739365560ae8e04543535ac2a014d82cf26d5926899dcd969bcfa7aebaa55e404d0dea1b7b6b6168000c8b65386d965dbd99250ad7fd7c99e6f7e3bf7f42fb0e588a6eb3e8b348b7c36a9b644db66db363b3b4f9d3ad94bd10553ef7bf6ce865292652100a452e6871ff96a6af0b65b48788a77cb0eb0070f0a0b854eb7b901dfcb86e8454a7709854a841110916547676ae11dd53535f4b39f182d87446d2d8e1ae528b1781e0e214350008470f58a5e7024fbbd75878f1ee38114ce0c44866925a4d403819a609b9eb8e9366e3d23009dd3e3fce0927d2716f1c05405fb9dbd63968b2803b37cfb5db6cce09557c8314d7d566e35d6abe934a792c5bcc1d3057ba73b7d64420600168c029893c9f4bd8bab02016be18de9d65665c17c14828b887b41bfe829a850f40132f80dbda3ad4d7830a177384fd8d2203ba7886089bf2af3c49390c9a097ab8ba2a36729e0150845800f085cff9baaab618760ee3b40478e89314ddae27bed857708c370312080c6466eebe044aa001378dfeac5b5d92980315ffa2330209d6d4bde7e47e5534fd84307f9aef8b2fcf56fb8bd3d8f16f79e562149793751ca6c260dfd523b00104929750652d588387dcedcb8a94812be79d5e55ab49c0baa6506c8a9ea2c9f3a2026fdcb232c9a998a810020bb5e6718e67beff9c78c0a368d495c7da5f26f6fba16a70affe851f2d06120d947442feb7090f218b393bd889911cd50a0abb606767cd634b64969efe064d29da14379f20598f397f316e6a90481a4cc679de29e1c8324b6185111013a7b9cd2199740398550fdc8bfbaf966a114f7ce98013555ed6bd3ee14b06fb9e3b47a5db04a2342d508dc324bbef33bdf2d375a878f94dc7c5366fb76f5d84906b04a42febae1b4eef7d40b1a47132ed00c4ee674e90001983e255959919d344ece9851366dda1026df901a9fcee965cb299b75e8405fb57a74b1d7d23c5fef657a7df5af08a1527b3b3895a7a74404f2f97d8a508a1baa6ee1e43a9440541015001bbc289ce7ed7d029b0a15e5e8f7bb1a66c05ccefae043a81bee3b715cb9f9267ae04120a668345851e5f4b08ad7e0d23140b7e1c740c87ac0df3d7a84ded458327b76d5a8b1c30d0bb76dcb3dfe644f3834f491878dc79e906d6d79a4f2d87b9ed22b117985562f03ce6743cfbe105145a12108ce66a198d720ba62b1a89a2e383800a022fca04504fa6d332d9949200292f7942214c5b61d3b0700550cadc5d232f074820cf65bef049f7d4affc9cfc2f7dc1d9b3f47f96803d50ed6888c93a711bcfd2e70eb15094c4096aaa4aaabd3f5c370d62cff79538705c3918e2eb9759bf9ec4bb9832d922931a82afbf08fd40f36a47ffbb6db79e967a3c5c6e451d13c66ae5138adcae2c885808af031d90c2486d4023ad800314b262975cb4e10ab8080a028c28fa8e6f32933081140541145baa262c3edb7769e3c09c0920c225b41e5fc0be78eba7749911b5656aa8d23ad96038c88ceaec2e163f2700b36d45bbf7dcb7fdb4dc6eeddc69826d17a1a2ccb59203113480234025aa2b6a667caf8c08ce93563270e0d04d52ddbcc175eb13edf9b89f5201133308154b0fbfa6f4f1bd69059742d4b1bf356ce9ec20bee8705cb2ac2138bf32ff41a1114a1041496000286d68a4088f41c015b401260f2a4c93e0641040088284022a22549683e4738a200d010211c29bdea6faf272260b6eca465251435581aac49dcbba4c80dc325cad449b0660d1239c51e58a6fdef6f6977dcaadf7e7770ee85fadf5c23aaaaf8f871c992996d01e9aab244dd509e3a29386b767955cdc8f64edebdc77ae371f3f366d330f2aeee14bd12a8f5a2f963aefcaaf5e3473811470f166fc5e4799f73dd8bf1eea542cbb7a83cec55ba01a0227ca0300073fd086dde6ce3fdf58241000270ed8a95f19212e7390550412118f60da9ad7f6c657555b5eba3ee0e390b2114c5e1340ab150554511bd7655544054a64c512a2b6467573ebfc98f36faeeba1dc78fb39e7b2172ff22cea6d2cfaeee1a56939c3046ccbab07464636324ea3b7642fee71afbb31db98e7368c9220f05cfcf8898baeb87476fbd39fcf6ef321b36f46250f97c590005bc868fe79d0571c5bda9be078222848f990884efbe45d6e64f2897d51801c0d7de05d089ec424b0012a05d53e9d8f1eaaaeafe85e080f28bc002100d23b41917c877df6100a76e5172a6f5de1ae59aaf59dfbf8d2f9c5e72d9dcee6bff327cf30d231239debacd7afb396bcfe764580080c082d9038a9191c1898f4c4ca6a6a56ef9de74f0a59ffe394b462f723bb5b8cd52ca9ca42c91b46596c906e132c62266c0c42041da529732d33f0f16857d0240ac1facfd70a9fef78fa2a103a00060a7c5e4f66e604b53c38199d32ea81f6cdbe92f408625e598ed7c26c8735b150040557d575f696cdc08c9a4a77aa235effbbe76656674fdb9d327472a97d7d435e67ef3dbd48bafa88924022a8549427e4fc5cbf26ef0b7014e5d75d9a40be7ea772d62c32cde9892cc164b11d02aa261261d981521841656d510eac42eb572681a582c2d55ad1d54c96cf4d77cdee505002320b1ff9aaf66c211f9f3677d478f0111034b600bd81836e4ec250bf43123af9b3bb3aaa214c0c2bedcc095ae0855f18535a544a08022d6ed6d851966f6472bf4f7de0310084c00245079f4c1543669353456afdb88c1007dfdeb6dcdfbc4ea5f961f6c11b62c2cbe574a6600964c92a86de2d8e8aa95837efd1ffa2bbf6270a4ba46201db38a84fd13c62b42e4e39382020dcbdabd872dd3114bc036b314429d315d53d5feef48f4b70e814265612413d47248314d66b6816d45c8c6915a59545315e1be9f329051e585302a8882c1fc640b0298521e48c60b9bac7cf254f2fa1b65a2c7710402e24be6f9562cc70347f4853f60c310436a83dfbddebae8e2d64d9bc32ffdb2b4f50c92f4e8681e297272762614ecfec75553404b2fbc83c97defc10b569c2f0c9db76fdcbad269887a175d7ee8eeb50e8c4b7f1373322a7a6fcee5d5c3e0745d3d3e3e4093c9f510afe204ef15344080989e6b65a93cf4d043ee93d1321108985bb630130313b1dd7aaae49a6f40387cd8a76418c4d9365aff91d8b96bd0ac5974e30deda51171e4a896d3bdb21980819189d952c4891bbe336dde82dca2a51cef01a739e1818a4e0f9a41000840c1e87c40cff59c689c5fbc707605ff67a7dba506b73be19ecedd623903ca046f04149e1c04ced9f6d15877f557aeeaf5ae039866e6e195c69b6f1122b12d99c4d021da9c59dacc9956e3c873d964aab999b66d0f1d3e5ed3d010fece75e72291d41b6f44df5d138cc7d14b7f16d3a979178e58f17078e593c6da35e02935679959d32026ec9dfb5c948b143d4003f3fffcc8bb7bd6b462b94c78c68c69afbfee36b80ba569369b5c78bbbd6b37b19389889c02b0bccc3765b236f302397552b2acacfbe041d8bb6f487d43c98205678e1c355e7ea566e366619a36cb6445547fe6a74dc74e671e78d08bff90d2f5e35d1dfe71e3a2b366178297936b8a8a72705a6303f79afe34070a116e6a1a72dd75fecacae2f2dd0d0c74aeb367c932d8be030080999cc1f9c7c09a8235d5814b2ed6e6ceb547d4898aa85014dbb24f6ddd2affe5a5f0be8367eebeedc20be7a66fb889babb9d0dc25826d51aeb8ece9f37f9b557fd836bdd745c38f276545cec43d1335c74177a7f46f3d3adea94492218a413ada62a02c386165159f783dc774034d4836d533a0da6a5d4d7412e67b6b563773757540a29653cae5654888611f2e429eaead6a64e01cb94ad67d4314de0ee3b212014625601cb9212ff2517675b4fd3d163ce6e8d4076de1a1000820892497bf75efded77e8fdf5fcd90e4ca67c656555d326ab0b66778e1ddd74c9e572f1723a760c0124d1a958577bbc67c8f7bf37f1b9e77c35837aa30074f8b075f8082a0a0683c6673b2097032290c4a91408411de74011e6ee3d90486149883bbb64770f6473c69ecf95ca4ac8e9c6f6edfa9a75eaf9d39560c8dcb1337be890924e8b50096733e8f35ba75b95b22800c6972d2743179adf387a547fe73dfffcb9c6a64ff478dcfce02318db64bef50ed70db73fdd2a8f1cc77038b9fea3d0ec5999dfbe9d79e30dffbcb9c2efcfcfb650551614cdac949656ad7ab47bdaeb99a77f118c27513824c6b98d0a2800c4c0dcd661b7b51b1b36b1a288fae1c1f9f327cc9b2b5f7839b7731711f764d367e3313168f0f8c7560ebbe5662cbcc056d860ebfec94f7dd77d879f7d5ed40db7eaebfc9b36c3d05ad6fc7673b3367b666ecf5e351ab5cacb7c7bf7072eb938befec3f0ac59a9cfb6954c39cf58fd929dcda9f3e6485d77a62584c8b677042cdbd8b71f5299c0972f4b7cfe79555d1d0072c30838dbc6c3eb5855d5e1c3aca3c733db7794ddf2fd6c4b8b367830f97ca14913c5b4f3628f3d1e9d3747ab1d8cc099a347a25fb9cadcb53b74d1fc3c3203bd5186008042d5aaaebdb6e65f5fcb7ee5cb3d66ceb2eda2da95f2494a01d440a892f0c87163f5cba9efde927cfe8573c9f8c1f6d3ad7ab6e6baeb666eda30fcd65b0b4815134104b5aaba6cce6c595569ec6dae58b02038778e456c3737cba143f8c021a8a8e0739d9acf2fa74c42550dcd9a15a81dac4896403871bcb0edb2d9b3d4ea6a47163385a64f0b5e7c9121896d3bf3c1fad2b973bcc588d035dfc8bcfa1a22faa79f67ac596b69aa1a2d0314e83935e772d27b6bdf3e768c3abbd27b9bf5f51f1503a3f62f205c03020680407dfd88279ec8dd7967db3f3ddfb566adda792e888aa6aa02057afb394c24892cdbca99663c93ce920c8e1e3df82ffeb6eeb65b83f50dd0c7727b751318dada7b5e5c2d2391e85f7f33fed4337a2e5b79d38d99b6b6d0a50b60e3c7c109e37d4db9ec8e9d5012a2e1c331e0571a4706226119eb2145d52aa2ddcffe13c762414500300782b46953e7e68f23d7fc95d6153bb7766d7448adfba3a5a5a27e846fda79ac6aeaf871d62baf96ddba901229f3e871deba1d8193bffab5914a86bff6557d6fb3b96f3f4b2abbf596d0e8513dfff854313e03bcaddc7b61940f31763291dabd27f1f127c92d9f18274f595d9d7626c352320a110aa935d58161c3ca2fba287ac105a5e79faf44227ded75207fef7ce0c1e87df768e5514061c5624a28240201320de1f39365a122502832956406b5a484995155414a3316d32a2a80c84e26959212e1f703225b96cce500518d447a5e7dcd77fef49231639c304f862efc01b66d004655235d17013f13cb741a350d019848098500917239b26cd45425100044d20d2518c84ff8bf00317d6feafaf413840000000049454e44ae426082,100,44,1296,1112804205);
INSERT INTO `$prefix$flip_content_image` VALUES (3,CURRENT_TIMESTAMP,3,'tournament_icon_WWCL','WWCL Logo','WWCL',0,2,0xffd8ffe000104a46494600010200006400640000ffec00114475636b79000100040000003c0000ffee000e41646f62650064c000000001ffdb0084000604040405040605050609060506090b080606080b0c0a0a0b0a0a0c100c0c0c0c0c0c100c0e0f100f0e0c1313141413131c1b1b1b1c1f1f1f1f1f1f1f1f1f1f010707070d0c0d181010181a1511151a1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1f1fffc0001108006400fe03011100021101031101ffc400c5000002020203010000000000000000000000070506040801020309010100020301010000000000000000000000010503040607021000010303020204050d0a0a0807000000010203040005061107211231411308516171141881223292a2d22393d31555165691a1b142526272b22617d182536373b324949557c133344454b43537e143a37425457511010002000303050c0a030101000000000001021103045105062161a11214f0318191b1c1d132521325164171e1f1224262232415a2333472b2ffda000c03010002110311003f00d54a0280a0280a02818bb2db3377dcdbf3d15978c1b3c14a5772b91473f2731d10d369d40538bd0e9a9e0013e221b0fe84db79f4dddfdb46f91a0e7d09b6f3e9bbbfb68df2340b1df1eee169c26258538b48b85dae97a9a613711eec9654793993c81b6db3aebe1a0b5619dca03b0112331bdb91e5b8904c0b6a507b2246ba2df702d2a23af951a7809a0b27a136de7d3777f6d1be4680f426dbcfa6eefeda37c8d01e84db79f4dddfdb46f91a03d09b6f3e9bbbfb68df23415b1dd3308fde31c5fe77b9f998b38b9f6dcd1fb5ed4ca2c72ffaae5e5e51af46bad04f4cee5bb7cc437df4deeec54d36b580551b4d52927f91a0d38a0283da1b297a5b0ca890975c4a14474e8a50141b8fe84db79f4dddfdb46f91a04dee37778976ddd28182e1264dd1d9701b9cebd30b6032953ae36b5b8b425094b69081c74d753a0d4e8281a361ee45614c249c832294f4e50056980db6d3493d601752ea97e5d13e4a093f426dbcfa6eefeda37c8d01e84db79f4dddfdb46f91a03d09b6f3e9bbbfb68df23407a136de7d3777f6d1be46814bbf3ddfed1813b8d45c6a4ceba4ebfc87a3223c92d2897105a0d25becd0df1529dd38d0462fbb9dd176de6857a665ddcf32111911dd10dd9085adb5466279f835bbdab4b6d3cc94a14b1ca951353823128568521450b052a4921493c0823a41a84bad01405014050140506659ed171bcdd62daadac2a4cf9aea598cc2012a52d6741d141bf7b6d6ec476c70c878fa5b9be7081db5ca622db3d7dbc9501da39aa1857ad1a72a3f340a9c10cc9dbfdb4b6f586ee17c3096ad7953261ce649d3a740e309d74d6a304b14f791d931d3943435e8fecf2fe46830a56fd6c04bb8c1b8c8c8e3b92edbda984e29895f06a7d1d9ad49f82e928d53af809a0cdf492d91fb52c7c4caf92a03d24b647ed4b1f132be4a80f492d91fb52c7c44af92a03d24b647ed4b1f112be4a80f492d91fb52c7c44af92a08ac637170fca378a4dd31d9c6e90a363a98efb91987d452e99c57cbc9c817ec4f4815285daf99d5818b7cb43c99e85165c1c6db7023d891d21822980f9ab509141ef01c4373a3b8b3ca843a8528f800502683e817a496c8fda963e2657c95060b3bf1b00cde655e519247f9c65b2d4679f2c492aec582b52103e0780e67546833bd24b647ed4b1f132be4a80f492d91fb52c7c44af92a03d24b647ed4b1f112be4a80f492d91fb52c7c44af92a03d24b647ed4b1f112be4a812bde237af0db9de705bde21726af1271c9eece7d80879b4fad530b42545c42382fb32385046277736eadf6582b89705cab7da9f45cad18f79b3ecdc9339b96fcd6da97301f3672325d93a2b42ad42414a42f535f58a1af32e4bb2a53d29e20bafb8a75c206839964a8f0f29af94bc680a0280a0280a02836efba1ed0798403b837867499350a6ac4d2c716e39f5ae48e3d05de294fe6ebd4aa0d929f3e1dbe0c89f35e4c78715b53d25f59d1286db054a528f800141f3c37ab74a6ee2e6b22ea4ad1688dac7b3c4570ece3a4fb229d480b70fae5fdce802837a76ba347736cf122b690a3f33c0e2a483feec8f0d052775aef835a37370d89965b60c9b2de22cf86a726b2d38d30ff691d6cba79d2427523909eae6d4f0a942d32f63767e61e77712b68e61d2d321a075fe8b92a128597dd7f6424857ece8654afc66654b469e41da91f7a82164f73ed9e7b5ecd1718daff252b5feb10e5042cdee4fb7ce1261deae91fc01c31dd1f79a6e82ad91f7229ad415bb8ee48995310094c59ac76295e9d41d6d4e687ca8d283af745c76f58e6e6e5566bd445c2b94480843f1dc1c47c3a08208e0a4a8710a1c08e8a91b5375ff00a5ccfe81cfd43503e5b507141936d015718a140105e6c107882398507d2491b5db69215ccf629685a8f4a8c18fafea502819c6b6493bd77ac12f58ddb5854a8f1255895d9f629538a6807a38e4291ccad02d03afd775e95285da4f766d907c927194364f5b7265a3ef0774a84a2647748d9774928812d8d7a0372dd3a7939cae823a47733da6701eca4dd9827a396432a03db32aa08895dc8f0a57379ae43726b5f63da258734fb896e829b9a772cc82df0172f16bca2f0eb692a54092d08ceab4ea6d616b4295e2572f96835c664397065bd0e632b8f2e3ad4d3ec3a9295a1683a292a49e2082283c680a0280a0280a0280a064ec36d4c8dc4cd9986f2549b0dbf964de5f1c07640fad641fca788e51e01a9eaa0fa0f1e3b11a3b51e3b696a3b284b6cb4801294a10344a520740006828356fbdf6efe806ddd99ee2791ec81d41eae0b6a2fe05aff8a3c341aa741f4af6affed9625ffe3c0ff964506bc77e4ff6cc3ffa39ff00acc502830cef01bab8843440b5de54f5b9a1cad439884c96d091d010560ad03c49501417587df377599d3b78969923f3d87927dc3c9a09b89df7b2a491e798d41787e3065e79ad7c9cddad04ec6efc76f2409588ba81d65a9a95fde5328fc340f2daadcbb66e2e2a320b7c47e1361f72338c480350e3612494a93c169d1638f8751413ff0057ed7f58864296826e9e68602df1d2b63b40ea52af0f2ac1e5f29a0cab834b7604969b1ccb71a5a503c24a48141f2da4477e348763c86d4d3eca94dbad2c14a92b49d149503d041a0f3a0cfb0439536f96f8911a53f25f92d36cb281cca52d4b002401e1a40fa8b41a31deca3cc56f44c75869c3cb12194b884ab810d8e822a447e35de377c6c31130db9ee5c23b6025b1708fe70b48034d3b5212e1fe328d05a61f7c1ddf6b4122cd6e92074eb1a4a09f6af69f7a826e177d0cc12b1e7d8732f23f1bb075f68fa9cc87682723f7d360ff00b561531b1e16e485feb328a601f7846590f2ec56dd91c361e8b1ae2d97111e4a425d468a282140123a52743d638d40d56efa58741b765166c9a236969cbd32eb33827873bd139395c3e3536e849fd1a0d70a0280a0280a0280a0c9b6dba75cee11add0185c99b2dc4311a3b6355adc7084a5291e124d07d11d9adb183b7784c5b33612e5c9ed245de527ff00364a80e600fe4207ad478b8f49341d77a373e16dde132aeea295dd1f063d9e2a8f1724a870511f90d8f5ebfb9d24507cefb8dc26dca7c9b84e795226cb756fc97d67552dc71454a51f1926831e83e95ed5ff00db2c4bff00c781ff002c8a0b0cbb75be67299715993c9af276ada57a6bd3a7303a745063fd5dc7fe8c89f10dfbda9c41f5771ffa3227c437ef69883eaee3ff004644f886fded3107d5dc7fe8c89f10dfbda6233188f1e3b49663b4865a4fb16db484a46bc7801a0a80b6cd37c71cc6372f1ec364c8640b8f682ed214ae1114e240861675d13da2fd9737b14e8ae835219b5023a4e358e4a7d72255aa1befaceab75d8ed2d6a3e3529249a623cbea8627f4240feeacfbda623da263b8fc27d3221db22467d3a84bacb0db6b1af03a292906833d4a4a5254a20240d493c0002821711cbad595db1dba5a95dac044a9115990082977cd9c2d29c469f8aa524f2f8471a09ba0280a02800001a0e8a0d2aef85b8102ff0099c1c7adcf25f8f8eb6e2653883aa7cedf29ed1008e9ecd2da41fced4755020280a0280a0280a0282ddb63b80ac0f254e42c5aa35d2732da91104b2be4656be05d48414fafe5d5235e8d4d0387d37336fb3f6df6cffbfa053eeb6ed647b937c62e7774b71da88d0661c18fcdd8b409d56a1cc492a5aba4f90755052280a07f639df072fb163f6cb23162b7bac5b22b30da756a7f994961b0d852b45e9a909e34123e9b99b7d9fb6fb67fdfd01e9b99b7d9fb6fb67fdfd01e9b99b7d9fb6fb67fdfd01e9b99b7d9fb6fb67fdfd01e9b99b7d9fb6fb67fdfd044e45df1f73ae705716db1a05996e02152d842dd786bf905e52d09f6b408d9d3a6cf98f4d9afb9266485971f90f28adc5ad4752a5295a924d036303ef45b9f89406ada5e62f56e61210c33714ad6e3681d0943c85217a0ea0a2ad3aa82e1e9b99b7d9fb6fb67fdfd05f1adf6de0530cbaeda31c8ea7da43c965f96f21c4a5c485279924f03a1ad6b6af2e270995de4f0feaf32b16ad7189fafd0edfbf5ddafa3f17fefcf7f0d476dcbdac9f2ceb7d89e9f4297b8599ef7e6701db5aef163b3da9f1cb222dbe51429d49e94b8eaf9dc293d612403d629db72f6a3e59d6fb13d3e8666dae6db95816251b1a80ce392e3465bae25f7e6b8164bce170ebcba0e1cd4edb97b53f2ceb7d89e9f42d1fbf6dd9ff80c5ffbf3dfc351db72f69f2ceb7d89e9f43a9df9dd61d30b16fefcf7f0d3b6e56d4fcafaef627a7d0e3f7f7babff00058b7f7e7bf869db72b69f2bebbd89e9f43a9dfddd41fee58b7f7e7bf869db72f69f2c6b7d8f2fa15bccf7637b72180e408775b05863ba92975c8128f9c2927a4079ce728f2a003e3a9edb97b51f2c6b7d8f2fa09356d3df4a8a9575b49513a926602493fc5a76dcbda7cb3acf67bbc4f27f6b6ecc34b79dbada52db60a967cec1e03c413ad2357499c21f17e1dd552b36b46110a73890871480a0b092405275d0e9d63500d6d4292d184e0eb440a0280a0281c3b276edbe9588e6571c971c45fe763ed31708ecf9c3f1d6a8ca516df00b4b4a7e0cf2ab88eba0ee3723bbeea35dad569d7ffc9caf7f4156decc52d98cee1ce85676047b2c96a3cdb53614a5811e4b297068a59528e8a2a1c4d04ded65970781836479ce696637c830e4c4b6dae076ee47e790ef338f1e76d48f60df29e3419dfbc8eefbfe56abfc5257bfa085deac771984fe357fc56ddf35d8724b53731a87da38f767210b521f473b8a5a894fadd78d064ed158f0c6b16cb733cc6ce6f56cb2a61c68707b6718ed24cb7483a2da520ea9427afc34123fbc8eefbfe56abfc5257bfa0af6f962d60b0e631d78ec7f35b05e6db0ee96d60296b086e435c405b8a5a8faf493c4d0486d1d8b0d6315caf36ccace6f56ab388b120c1ed5c60392e53bc745b4a41d5084fdfa091fde4777dff2b55fe292bdfd045ef363f88376bc472cc42d9f34d9b2384f1720875c7c37262bc50e82e38a59d7d701ea5071b318fe22edb72ecaf2eb6fcef66c720b45100bae31da4a94f0435a2db524ebcadac7ab4164c7b2ed87be5fadd668bb5aaf39b949662327e73967453cb0807d9f5735055339c731d91bdd2b1ac661a2259dbb8b56f6986d6e3891d994a1f57338a5a8fae0b3d35f379c22659b4f97d7ccad76cafb935c585dd274c71610c070a507a8211eb1207a82b98be36bce0f72d3457234f5eb7244579565c5765b7172661130b6ce3f6d747336f4f4a9725693d0a4c64e9ca3f4d42ac327774cf2d9c96f1e33ad266b9318f3f77daba35dd755cbabf974852facb70d9427ee152ff0d6cff5d96a29e32d5ccfdde8475e3bb5e471992e59afec5c5691af9b4c63cdc9f1075b52c6be54d62ccddb1f96563a3e36bc4feed7939bba0a7bc5baeb699ef5baeb1170a7b074763b9a6a35e20a48e0a49ea50e06aa3372ed49c25e87a0d6656a72e3332e71aca6f6d36e6667b72bbc66eec9b5a2d6d4770131fb7e7320b83f2dbd34eceb7749a4ae65719731c45c459da2cf8a52226b30b6645ddea4586cd2aef2324129a889e75474c40d15ea74039fb55f2f13e0acf9da0a52b32acdddc59a8d467d72e6223ad3ddf413ea5d55c43bcb59e6a557d4430dace8a554c4315ac88c924f636b7075b84247e1ff00456de8e98de1cf711ea7dde967f57279d41abe7938a0280a0280a067f7749d1d3b908b24bd3ccb26852ecb241e8d24b44b7ffa8da4502e6e305f8170950240e57e23ab61d4f816da8a543ee8a0666ec055e36f36e32bf66e2edaed9262fac396c78a5be6f1a9b5eb41df71d271eda1c0714d02645c50fe497103812659e48bcc3fa0140a8d0e80e9c0f45035ae2157eeee56b97ece461f7b7e12fc298970407927c9db0d2839bd1f98fbb963f052be57f2abccab9bc8eb2c42488a81e4e74f35029e81b1b864def65b6f72203576d7e778fcc575fc0a83b186bfd16b41c65e4e3fb0f87d8124a256492e564139bf0b69d2346d7c4a42398502a743a6bd540d88eaf9fbbb6ca674e69387df10f8f0889724147f5c28385e963eed8da4a4264e5f7e52c2bad512dcd848f503fcd41e3ddca133fbc437f9490a838b5be65e6493d03b064a5bf57b4712450626d2f6b71ce6e1914bf5ce418f2ae0e2bc2fbdab69f74f135adabbf572e579c3ba6f7dabac6c3f3613058d90dfa4e49736c3d6eb1b8966decac6a87277285add503d3d8a549e5fce3af5569eefc88c3ad2e8f8c77adbafee293c91dfeeeefa5b0b76bb42b540726cc586d86c713c389f00d6acef78ac632e1f4da6be75e2948c664a1baf78f80cc95221410fb293a7392ad0e9e03eb7f0555df7a444f243bad3f02ded5c6f6c257adbddc7b3e6d0643b092a6264252533222cf3147680942811a6a95729d387556ee9f5119b5c61cc6f9dd17d066fbbb4c5a263189557bc1626c5cf1645edb6c79fda96017074aa3ba795493e1e559491e0e3e1ad7de395d6a75be985d7066f09cad54e54cfe0cc8ff28e5f262a6775b3adf32cff00dbdbff0059faf9ddbea4b2f1b4e3a9affe4d5dd83a6dfddff411fd626b6357feb953f0effdb97e1f24b4e8aaa8307adcd9d0aabeb0629b3ccaaa70639b2b198c9e0cb00f57311e53ff008559eefa77e5c3717ea392b97e1eef12ad568e145014050140504863d799164bfdb6f31bfd7db65332daeaf5ccb81607b9a0bc7784b3336edd5bbbf1b4306f01abb43527a14dcd6c3ba8fe3955058b6e2c0fe79b3b3f148faaee168c8adf2e3048d54862e5a4374fe8a48e755056fbc0df22dd774eecd42d05bad1d9da60a53ec52d42406b41e2e70aa098dd3dbe360da4db8bb06b95e9b1e42a7384684aa5afce9807c8da88f5283c76553f3d631b8386105c76e767f9c603238954ab5b9daa0247e5282e83af781526df72c5f124680633618519f40ea94f23b67cf9495026815cec592d32cbce36a435201530b2340b09514a8a4f5e8a1a503736dadd372ed99cc3118682fdc6df3edf77b6b038925e5f9abfa7911a6b4117de1ee1197b8ce59217fd3b188716c91003a8e588d00af76a341239eedea6cfb0f81641d9724b9afcb7262b4e2a44dd171b5f17651c69e5a0f1d832aba2733c37a7eb1586418ad9e3cd2e17c3b1a0f0fb2a0edbee1568b76098694764bb1d8997e635f932e7a8bcf023c20d075c287cc3b139be41a8449bf4a898f4327a7907f699407e93640a0f4daa886261575b82868bb9cb6a236a3d3d9c749757a788a9605556f2bf2443bde09d3637b666ceef3b6cbbbe42447dabb5bc07c24e7254a74f854b92e01ee52056ee9630cb8731bf7326fabbcf3a07bc95d9e8f66b7416d652992a714e007a427947e051ad2de97c2b10e9f8134d16cdbde7f2e1e76ba297ad5260f519b24b1ecaef18ebb21eb4bc63bd292843ee24a829496c9294f023802a359f2f3af48c22553adddba7d45bad995eb4c2466ee9e6332339165cd5488cf0e5759714b52543a7882aac93a8bcc6132d5cbdcfa6cbb45a958ada3e98c17deeb075bce587f9881facfd596eef525c4719ce3a8afd46b6edff00dbdbc7e823fac4d6c6affd72a9e1effb69e1f24b4d8aaa8f07aa4d9d0aaa70639b3a1557d60c5365232391db5c97a1d423d68f5387fa2aeb494c28f31e22d47bcd4cf372799155b4a114050140501405035f73f96f7b59b7795a78bec457b1f9eae9217017f01cc7c25a56b4193dd9f396b13caaf6e3ea098f26cb317a13a6aec4479ca3dcb6a1ead02eec36d9d9666706dfa95ccbdcf6db5abf3a4ba39d5ea731340ffdcfbdb799d87742c514a4c6c2e65be4599a1d098f152614909f10e5e6f568149b0791fd5fdddc6a6a95cacbb28437bc1c92c1638f90b80d0466ec5f957edc9c8ee84ea97a73a86c9fe4d93d923dca05031779b00163d9bdb69c8003ccc65b73d3f8c1cb8ff6e4857e8eaa14111dd93306719dc771e92ef67125db66b4f1eaf8268c94ebeab1c2817894dc32bcb929079ae17d9e1209e3abb2ded07ba5d06c6ee1dd63e456ddcdc0a290e45c3615b1fb2363894a2d412ccc23dbe868119b3b90fd5edd0c6aeaa51434d4e69b7d4387c13e7b173dc38683277cafaabdeec64b30af9d0dcc5c568f48e58df0234f69ad04eee79163daadbbc4d2391f9119fc867a7acae72c88e4f91a1a504d448e6d78863b6b3c1c114cc7c7e7cb59706be308d0573dbc3331ccc1ec5c1fa4f77a5eb4fe6fbfced9aeef9391236aad6d0f6709c9319c1e02990b50f72b1571a4b63970f37dff0091397acbc4ed563bce4374da6cd3d2925a6de758715d414b4a568d7cbc8aad3de959988974bc099f5ae66652679662263c18e3e56bc29754f10f47b5d278ce1f97659324c5c7202672e1b6872515bcdb0101c24206ae10093ca6b734fa59cc8c61ce6f7dff004d1da2b68c71e7fb25372f6537521c75ca9b68623c464733ef998c2f911ae855ca825474f10acd6d04d6319956647165336f14ad396d3b7ec5e3bab7fd632cd38fc040e3fc67eb6777fab2a4e309c73abf51adbbbff6eef07f9b47f589acfabff5caab87bfeda787ff009969915552e0f4d9b3a1557d60c736793ae8436a59e8482afb835afaad719c1833737ab59b6c8c54094e17242d67a49abea4610f25d56675f32679de35f6d714050140501405035f0ce6beec466b60d029fb04b89904407a7917fd9a4e9fa284a4d02a90e38824a1452482925274d42868470ea2281a7ddce3311f359d94cb1fd8f12b5ccbb2c91eb4b896cb4d27ca54eea3c941db606e5f396e35c2cd715f3a332b7dc2db254a3c0bb21b2f2547c7da37c3cb40b150976eb81014a666437b8293c14871a5748f1850a093c36c8fe4d99da2cfc5c72eb39961d3d7a3ae00e289f12492681f1995f99cd95bc76168f3a2d2a8b72b3349e8422ce44491c83c0504506b6a1c5b6ae6428a55a11aa4e8742343d1e1140cdeeeb6d8eeee53379989e6b76311255ee613d004468f667e354834193b19922e6ef31f9c140359789d6fb8737104dc10b52471fe7b928167728526d3779505c2512a048719591c087195949fbe9a0c8b25b676479341b6a54a726dde634c76878a8b921d09e63eaab5a0606f6484e47bd926cd0b8c484f45b1414a78f2b718258207917cc6a2d38462c99397d7bc576ce0b164d29a76f52833c1865418653d410c80da7f56b95ce9eb5e65ef7bbb2e32b4f4af3794caeef19dc4b45ca5e3b70743512e6b4bd11c51d12892072949f076a9038f840f0d586efd47567a93de9721c5fb9a73a9da32e31bd3d68db5dbe0f39f992e3968c8ecb26cd7767b685293a2d20f2a92a0754ad0afc5524f106ae2f58b4612f38d3e7df2af17a4e16823e577549065ff0063cb14883af043f092e3c07839d2ea12a3e3e515a53bbe8e9abc5daa88c270e83576ef6eac98359dc816e5b921f92b0ece9cfe9dabcb0344f04f04a523d8a47456d65e545230850eb75d99a9bf5f3279509be394c7b36172630700953344211d7a748fba47dc06b06b3330a61b56dc37a49bea22f31f86be52efba91d6eb959fe6607eb3f58f41eacb738b671cdafd479e518ec4c8ec32ecb2dd7588d31212e3b1ca52ea425415eb4a92b03a3c15b97a45a309735a5d4df23322f4f5a3cfc8587a2c6dff00d2377f8f67e46b0f65a2c7fbed4edf2fa5c8eeafb7df48ddbe3d9f91a765a1fdf6a76f97d2d61cf99896b9d718713511d95869ae63aa88006a49f09d38d68e4571bbacde99deef4d38f7e620b72753ad5cbcd265c5014050140501405030764b29b05872b98c6472151b1ebe5b265aae6fa50a70a10fb7aa15c8904921c427aa82c7f513bb87f98d3bfc35cfe0a0f7bcdd76a310db2c8ec984e44fdf6f1933b11992e3d1571cb5163ad4e2802a481a28f03c68159875f978f65966be235d6d93589440eb4b4e254a4faa06940dfc9f1deedb7bc8ae578467b2e18b8c976579ab76e75486cbcb2b294929048d4d0656149eeff825fdbcaadb9a4abb5cedacc85c082ec075a42df5b0b6d1aab94e9a15f0f1d02ff677368162dc54dcb217ca2d175665c2be3dcaa592ccc6d414a2948528fc272aba282c7f513bb87f98d3bfc35cfe0a0cb9772da3c2f6fb2b858664efdf6fb91b31e000fc4723f65183bccff2929d3d7a781d4d027ac7757ed17a81768e747edf25a94d11f94cac2d3f7d340eecc2cfddc326c9ee5901cea5c055d1f54a721b76e75696d6efae580a291afaed4d077c2e17775c3f268593b19b4bba49b515c88d01d80eb6871e4b6a0deaa093a68b208f1d02fb6cd6f5cf701fbf4bf5ea888957590a3fca10794fc6b82b5b557ead2573b834fef75558d9f727dc754b51528eaa51249f19ae7307b4cd9e61e52141493a28710457d60c53734b0def0791d963b70ae405c623602505dd54b4a470002f50afbbad6f64eb2f5e49e5872dbc786f4d9f336afe0b4ecef7897c67bcce2ea402f4371b57580547f020d6d46be36282fc2578ef5fa3ed46de7bcf5b92ca85ae02d6e9f6255c07b65747b43516d76c864c9e15889fc76c7bbbbe92472fcd6f794dc0ccb9bbae84f64ca75e446be5e249eb26b4af79b4e32e9f4ba6a6457ab4859f67f72edd81aeed26434b92f5cc30d86929e084b1ce79b9b987b22e7469d559f233fddc77953bd774f6bbc4cdbab81887bd35b75e16b3a7abefab3f6d9d8acf9629edcf8a1c1ef536efa28fbaf7d4edb3b11f2ce5fb76f1438f4abb78ff00ea8fbaf7d4edb6d88f96b2bdbb78a1ac7b89756675cdf7d8e60d4870b890a1a1e3a0e2353e034d15396651c4ba8fc15ac297564e2c5014050140501405014050140501405014050140501405059313fad1e6377f98797fd535e7fcbcbdaf61cff8bcdf8bcfcbcda71e8ead6b5f51d4c3f1f796dba7b4f5a7b37afd3e0c5dcfd77ebed3dcd6a7f1799d14ff0079faff00c5c7edaf5f69ee6a7f8dccf9f8dfeae871fb67fce7b9a7f1b99f3f1afd5d0e0fd70ebe7f7353fc744ff73faba1c7ed77e7fb9a7f1df3f18fd5d0e3f6b3f3bef54ff1d1f17e7e871fb55d7cdf7a9fb08f8b73f438fda8f1fdea9fd847c579c7ed3f8fef53f611f1471fb4be3fbd4fd947c4d1d73f3fe64f9e7b3eaf256c64f53f2a9f79768c63df77d8359d56280a0283ffd9,'image/jpeg',254,100,0x89504e470d0a1a0a0000000d4948445200000064000000270802000000764c7ac10000151f494441546881ad5a7b7855c5b55f6bf679843c08218f0349209040c4c410cc138804350882c2557a55a89558df7ad17e57dbefb6bdd6d66bb96d2df57e4a2e4a8bda426b51d4abc045ac4210120209241012f234314f929ce49c9c9c9cd7de7b66dd3f26391c0209c1ebcafef2cd99993d6bd69a356bfd66cd46f8b684888aa270ce89e8aaadfef2553b8c3766e05b8c31221a6f7c44144204fe94657fe52439ca17890811253b59b84ae7c98f3b86144541c4071f7c70d3a64d8c3108109588344d7bfae9a73b3a3a6ebffdf61ffce007717171818a188f8868dfbe7d191919f1f1f172c63b76ecd8b367cf55859c3e7dfa471f7d247f0a215e7cf1c5f2f2f2b4b4b4c2c2c2b4b43445512629484949c92f7ef18ba8e898fcfce5e7cf9f6f6e6a909a9afc1a4f8a366cd8e0f57a4500c9951142b85caedcdcdcb7de7a4b55555dd7a501fa49f60cfc2f8973be7dfbf6868606ff80cf3ffffc78dc67cc98e17f51d3b4bbeebaeb273ff989dbed1e3399312cc69010e2e38f3f9e3163c61bfffde69b7f7afb6fef7f98bb64895cfb2be9eab593a159b367ffe6b7bf35988c000240100900f04f02001e79e491471f7dd4603030c6fc16ee6f1df37fb4010501010a2271ad951d23f6aa55ab5e7ae9a5a0a0a02b3b5cd939b00900162c585079fa4cde92bc2f3fffe2b6e5b78dc7f9db280b114d26d3af7fbd65d6acd940488400088081eccd66b37f7b4e9efc822122222382091c45e0be668c3dfcf0c353a64cb95e5924c7a6a6a6bca54b77efda75fbedb715171f9113b8b2b3e17a4797b35cb972654aca4dc5c5c71c8e41bb7dc0ed72e9ba2e0407002184e4349ea626b21864959555a9a9a94ea7cbe11872b95c8d8dcde3f575b95cafbdf6da9891bf4560a9adadede9e979e595ff58bd66cd1ffef0fbea7355e3b9d76fe3e06363638f1e3d9a9494a46abaa6aa76bbed85175ef8f8e38fefb9e7dea79e7ad2e7f31140cfc5decf3fffdc6c360d0cf45bad5687c3d1dfdfeff1785455bdc47b34e804149488888853a74ece4d9ccb75ae69da4f7ff6d3a26ddbc64e1a9188c2c2c29e7cf2499bcde6f57a071d436ddfb42143e7d090c331e8f57a354de39c8f27c29878c71813420020c044fabd6ecb32994c5bb66c494c4c444493d160321a8e1dfbeac0810300101b3b73c58a15722b3636369b8382befffd0d38ea2f38e79aa6f5f7f7dbed769bcde670380606067a7a7aac56abd56a1d1818387bf6acddee906230446634984c4693d178e51ca405858686befaeaabf2a773d8f5a73feddcbcf95f148509ce555575bbdd369bcde572399d4ea7d3e972b9060606fafbfb1d0e87dd6eb7dbed172e5c686d6d953863146d7ca7e10f00d6ad5be7f3f938e732e20c0c0c2c58b08031c618dbbc79b3ace75cd4d53764e7e45afbfbf92889f149eab1aeae6ed3a6c2a8a8988686467f387bfef9e703319424593363c60cff088e216746667665e559395a20c7d129f140769c73abd55a5454141e1e3e194c737d24e7171717d7d0d0e09f8aaaaa9b376f96cc1071f3e6cdfe0935343622b25b6fbdedc48913aaaa5ea92c59238337e75cc20b4dd39e7df6d9a6a6267f87175e7861bc19592c33e4eb4208a773383171deecd973f6ec797f68c839868bd4fc18ee92f6eedd1b141434497d5dc736648cbdf8e28b494949f227111d3d7af49d77dea1ab41380400a0a3478befb8e38e949494e4e4e4f0f0f0a8a8a8e8e8e8a8a8a88888889898188bc562369b0d0683c16090215fb230994c301aec68144f5f89aa114790301149613b3a3a366d2a9c3b77eec2850ba74d9b161515151919191d1d357dfaf4c8c8c8d8d8d8e0e060a3d168302866b359029ab56bd7de71c71dfbf7ef9fbc1eae4d88b86ad52ab901a521f4f7f7a7a4a404ae49a0653536366200f907092c9bcde6a953a7ce9a352b3b3bbbbebe5e5a813431ff382fbdf49294eaf2c90022ce9993e8079cd2b2101500261f590eac3199824243c32d96d8794937bcfefaeb9291aeeb3b76ecf86e2ccbbf9e168b65ebd6ad8aa2f81779cb962df5f5f5e385e7b0b0b0c2c24221212663440424a30d81dcb601c6181a16121a1a0a977016f8275f5858d8d6d63e383878f9f064341aefbbef7e095388c86432fdea57bf1c1a1a0ac42b57c02569eec0b9be303dcd2f606464e47887c1eb233f622a2a2a923e45d7755dd70f1f3e2cc50ba440cbbae4950491a02b0bfe72a00fd675ee74badada3a44c0c9e9aae18107d018a73e1e58f7d3187ffff2cb2f5f2f789e485f2b56ac901b506aca6ab5a6a5a55dd933505957c6a00948f692c2eedfffbf1b373e28cf92a3edba10fcf2e732169cf363c78e757575e9ba7e4d658de9d0d3d333c699fcbf283232f2ecd9b37eb1354dfbf18f7f7cd5a590caba2650b89ab246a8a9a979debcf9068371d7ae5daaaacac69187cb471742e33a1fa9d039e7a2b1b16949ee92eeaeee11ad4e86a3105c08b7d7fb50e1c3880a005e0d9e8fadbab6cf2a2c2c349bcd4d4d4db2e6c2850b45454557dde18383830d0d0d70f9a96d322484181e1efeecb3cfb66fdfded7d707008f3ffef8c183077ff8c31fc6c7c7cb640b0100094064026874a99c6e577171f1d6adbff7b9dd2dadad434e27c048328b00e992c023b365a3d3e65c9c3b77ee8d6d6f9c2e2f47240420504821906735220076a58c1349258357484848a01d699ae6f57a03cfcc7e32994c93c72c814444fe6147a4624cfe0f0e0e96039a9852306f419011888cc046ce313ae71eaf878818b2d09010441941080070e4783f12310000810048fe11c1b0db4582800b0044000e82816a22c339db60537f2fa7915703e7790dcb22a2e1e16108885c327b79d5ced2fd8bd108357188b96a6b60fa50b63a9dc3000200d32c516f2e5a1072fe1c2300c19138120282301b1180005155915010474410828486ba4e4290d415a100d208494617449902d28004111704a808148d029e730e1191d4e098f95d4359b367cf7ee69967a48949f9fffef7bf57555549c1d2d3d3376edc1898cffde31fffd8dada0a0046a3f1a9a79e92094fd9ba6fdfbed2d25239cea2458b366edce8e72284d8b56bd7850b17e4623cfdf4d3b366cd1a5528fbf493f7cb4e55e6ce4d0cadad87af5b482800e411d04c6805e165888c4de77caad06389a60211e7c8056a7a0f61afa21804f3a20a648c3790d9e7d301382007d20174000090050e400cea48e9222ee47a5da18d89944544369b6dcd9a35afbffeba74b7c9c9c92b57aeacaaaa928abbfffefb3d1e4f4b4b8bec9f9d9d9d9f9f2f9515121272efbdf7eedebd5b1efd1312121e78e081d2d252692febd7af77bbddfe1733333357af5e5d57574744111111ebd7af7ff7dd77898831f6e8238f7dfac95e86b074669c38779e29e01354a6aa6d3959f177dd9d909913326daa515134afe66d6ff9fae021e38103375aad06d085c0238c25ed7e6b9a39620a68661f37bef6ba565ea603702001a001704000d201340042d049a946e1fdd6788b31f6c1071ff88f380909097bf7ee9586a628cafefdfb2d168bbf737a7afadb6fbf2d01f792254b8a8a8afc66151e1e7ee8d02193c984880683e1c08103717171fe1753535377efde2d1d794646c6ce9d3b657d7070f0e79fff233c342c2428f8fc439b348bc51119f3defcf9673ff91f9fc7ab6abae0ba2634aeeb5cf5e93eafe6f3dace9f6dcf5ae2634c45f657343abadbb9735838dd5a5fd750466627c037005f033401d401d6009e07a802a8002c473c8e86250c2748884e04c6e496a9aeae5eb468915450777777484848686828112526260e0f0f5bad567fffc6c6c6b8b8b8a0a02022cac8c8a8acacf47b2587c3c1399f397326114d9f3e9d732ea39ea4cece4e8bc522f39c39393995959592fbbc79f3066c56f7f07074704882cfa3137e69362ddbf3de4debee319a8c0a320204418240630064601ca6ce4f8ddab9cd1711c989903891819800d21541e051d5d11da78dd8176972030271a25ee47d00fe787a7dca92a29694942c59b244d6e8ba5e5f5f9f92920200cb962dfbeaabaf44c0bd93d7ebb55aadd2647273732b2a2a02473b7ffefcfcf9f301a0a0a0a0a4a444d3347fd3f0f0705f5fdfac59b31071e1c285a74f9f96f537df7cf3c9b2720d292f21614a4f6fb32666befc52f4a29b91840a288607f981fdf8e22fe199a7d8efb7e9f61e5288c02b6e4c35ad7f801035241a4550d2afcb1da703e880a3859147003403d827bc45bb06cc27a2c6c6c61b6eb8c11fbcce9e3d9b9393c318bbf5d65b4f9e3c09008c31e3688aaeb6b6362b2bcb6c365b2c96b6b6b6c0a12a2a2a72737301203f3fffc89123d2540d06030070ce4f9d3a255b9392925a5b5b65eb2db7dc72ead42960b0383e9e7574d7c5842f5aff3d2310101a5c4edcf917f8d1bfc21bff85efbea3ffeadf6b7ef33b24043018882977deae064f09cc93128054161f551607d4479e11657d2dc08d6c023475ed33516f6f2f63cc62b1487d9d3c79322323232222222a2aaab1b1110066ce9cb966cd1ad9f9c48913999999f3e6cdebeded1d1a1a820034505353939696161a1a3a67ce9c9a9a1a228a8e8ebeebaebb24a4aaa8a8c8cbcb4b484870bbdd76bb9d880c06435c5c5c4b53a399996f890cf60d0f9bb3734ce1d3089860242a2bf51ddb0c1dedcceb235d084d570ffdc3b3e34dda5e846f6ce795e52c388c1131798f82c0085410faa5d82738d0e8031c4005a51648c0b89968984c3e8b735e5656969595b57fff7e44eceaea8a8e8ecececeeee8e8f0f97c88989a9a5a5050f0e9a79f02407373736a6a6a6a6a6a4d4d8ddf182534ebeaea8a8c8c4c4e4eeeececf4f97c0070d34d37ad5dbb76dfbe7d88585b5b1b1f1f9f9b9b2b8f5688386bd62c87c331303838377c5a9ccbebd0bc9129370886008201c3ea5abad82b04710001c048dcd4da22feed6776850cbae252c0a0f9f4cb6d8400744001208038a00012233524007a10ac041a4d9486bfb66521e2f1e3c757ac58216dc4ebf5da6cb6871e7ae8f0e1c312162c5bb62c2b2b4b666ffafafa7c3edfdab56b2b2a2ae456929e8888dc6e776f6fefe38f3f7ee2c40939545e5edea2458be44e1c1e1e763a9d0f3ef8e0a953a724df65cb969597971389f4b8d8693d035ee2e6981885482142226c6f439f4f25d28974041f3137e76ed5a77b3597ead53d1e9fc6f51177e557967fdf6180b7421d980ed84e300038ee95d424954544151515c9c9c966b3190038e7e7ce9dcbcfcf3f73e68c94392727c76030444545018010a2a6a6263f3fbfa1a14162b1279e7842be4844252525ebd6ad2b2b2b93bb2c232383739e9090205b2b2b2b172f5e5c5d5d2df92e5dbab4a4a44401256f762cebec465563008488c074019aa6ea44024807500955464042235405f8500090064c11c00888314210085c700d84064203d280b4919828388206d48ae801221013c0ac49e5713c1e8fdbed8e8d8d95f29f3973a6bebebebdbd1d00626363755d3f74e8904cda08214a4b4b5b5a5a6c361b11858686de79e79d528f44545e5eded2d2221dffb469d34c26d3a14387d2d3d3e56e2d2f2f6f6a6aeaefef0780e0e0e0c4c4c40b172e280c3243c384ad6f8a26d4012b009020406e981a46023400154005d0885ad1701259256035292780398d0681280f82329728e812580fb42f15849729edc435b82a6ebf4e6511515555557a7abafc59575777f0e0415dd789486e96d2d2d28282020040c4d3a74f9796964aaf347bf6ecf9f3e72f5ebc586ec9868686c3870f7b3c1e00c8cbcbaba9a93972e4486e6eaeb4d0fafafae2e26209291213133b3b3b5d2e57747070868e8a6b284c80afba8e090e2414ce3ce9a92ea6a87e6521d6cf9e63f9f3ce98f7fe1afdb73d893bb64d9b97ac4bff234fd18834a223891efc9a5234060e30b401e9d7b8369cf4f57d5959595e5e9e94ca66b3edddbb57ce60f9f2e5c78e1dabacac5cb06081f43e0e87e3b3cf3e9336b874e9d27dfbf6656767cb413c1ecf3befbc235f5cba74697171715d5d5d4a4a8ad96c4644bbddfec9279f482bcbccca3a7bae0a116f8c99197cb14b4742d0422b2b55a753104702c3f25b6bb232ac8ce9a0a8c8da1543cc3d77e7fef37db9ebd62efda775995939e87222010212032224d22faaa205b115591bb20e842ec48b8c790c0a2174010c30c6086042754df676a7b6b6f6d9679f95655dd73b3b3b01202c2c2c2929a9b2b2726868c86834c6c4c45cbc7851d3b4d2d252d9332f2f6fc78e1d3ffff9cf838282a44175747430c61445c9ccccdcba75ebe0e0a0cbe50a0f0fefededf57abd128e12516e4eeeceb7df4684823909bcbe9e741444f14d5fd3271fe9df7fd8041cc3a332f6be77f077af7e5379d6c8947905b7ae7af647484201e2a043d5f9616bbf204200405238f8cc53a7fce72f170a797686d1db3b0cdeb5bbf68bc35da47b90aef959d76495d5d7d72784888d8dedeeeef657c6c7c7cb5b5f00a8abab4b4e4ebe78f1228ddec84f9932252e2eaea6a6c66ab5c6c7c7373737d3e80d426262627f7fbf3c2ab5b6b6a6a5a5f5f6f6fa87658ccd9f9ff44d5bbb01203b6caa6eedd300098040e85b7e67ca5b4673e69a746e8c8cbbefd53784ae3224c11403111008c69975a0ebc8e7835eb7840e8c033130188d0beebdc7ffc9164326757671cf876e841612de495c584c761b4ab7959999e9cf3421627e7e7e595999aeeb00505e5e9e95951578f7959a9adad7d7e77038aaababb3b3b329e07a71f1e2c567ce9c91839c3871223b3bdb9f0242c43973e6d8ec36876dc012127a2357bdbaea03f220719d06bffedafdf013e24c253121505540538c0c18634002b9ce086d0ed7fb1fb47ff6852000448eaa00a3108004460e06028540110002901005f061e73052c70407c2009aac6511d1575f7df5d8638f49cf2de55cbd7af52bafbc227f565757171616d6d6d6ca4b01c6584141c1f1e3c7254c7beeb9e706060646d687b1bbefbe5b7ea600005555554f3ef9e4ca952b6114be2e5fbefc78c9094efa4dd1d141bd7d5e0222d201049020b29595b8376c88f8def7d8aad54ae21c3625882b0c0580cd0e0dcdbd5f7cd9f8e11e61b3ab8806835969ef8020bba26b3492921cbd8d0331926fe6dc4a34c0184d8419ae53598878eedc394dd3366cd8e0af6c69699197a944d4d6d6565b5bfbc0030f50c0c799d29d373636daedf6c017bbbbbb9b9b9ba501767575d5d6d66ed8b0c16fb088f8973fff0510165b2c7a670707d48104a20ea01310094f5bc760d136f3dfde572cd1c669e1bad9089a703a6c439d1d4303360250190288e4d0a00b8f3d65f0a93ec10511204870400402853474a7c7db41383cb954f8f5e5cb03f3eb387abdeecfc304de36fb2b699c0fc30273cafe9debd717111815f870d5eae4e223dced95a75f8e200804808aa3f711043a100790da240201c047d2edcc078ca31020048d20792e39036a23d719e463f025c13f08f8243ea1f9ae3f20f98e88019818de3223aea3a71b89040001b05171444037014001e972a4cbef73f0520d047868824bfd06117b26771dfd7fb4c67c7acec5564e0000000049454e44ae426082,100,39,1296,1112804227);
INSERT INTO `$prefix$flip_content_image` VALUES (4,CURRENT_TIMESTAMP,1,'tmpplan','','',0,2,0x89504e470d0a1a0a0000000d4948445200000281000003170802000000ca7918050000200049444154789ceddd79905dd57d2ffa5fabbb25359aa596849090040201c2a0018298475b9820485cc43cdbd7f1234e02794ea0524e5e86e78a93aa6b9763a752e558aee499dc5b5c27715e9eeb3ad7362a63638c2c0462100204048466090d484242039a5beaf7c7e19d3452f799fa9cb3f6e9f3f914459d6eadb3f7eaeeb3d777afb5f75abbe5e8d1a30100d4dda0d415008026258301200d190c0069c8600048a3adff9b78e8a187fabf1100c8be3beeb823fffffeab42060340933879f26415b7662c1a00cab378f1e2aa6ca79afde0050b1654716b0090118f3ffef869df59bc78f1c2850bfbb959fd6000a844ff7bc33218002ad4cf1896c10050b9fec4b00c06807ea93886653000a4218301208d7aacd171cf3df7d4612ffdf4f02b0fa7ae4211f7cfb93f7515a8a1471f7d343f9cb570e1c2bbeeba2b6d7d685a1ac35e9d3937a92af48301200d190c9933689003139a82431db265d8b061f3e7cfaffa66b76cd952f56d02fde4990dbd287cb121fb174b685c93274ffec217bed0d9d959f52daf5ab5eae9a79fbef7de7bdbda1cf5944a63586b8ec634729f6c9f607a9a3b77ee7df7dd3774e8d01a6d7fe9d2a5dbb66d7be08107468e1c59a35d40b99abc3134169d40fed4d2adcee45d74d145f7df7f7fed023867ddba755ffdea57f7efdf5fd3bd4089348632183261e6cc9935bd156bc48811b917fbf6ed7be699676ab723a074c6a21378f89587f3c32f679efd35ed980c3575c30d376cdab4e9d9679f8d6a3f841c2aa63194c1d591fff494f8a1c9156bdae117eaafb5b5f5befbeebbf8e28bbff7bdefa5ae0b0399c6b02c32b80acafdcc9df6ae9e6fbc7fcefdf7cfb9bf19cefe48e2eaabaf9e3e7dfa5b6fbd95ba220c4c1ac372b91edc5f957de6fa32e03f702477f6d9675f7ffdf5a96bc100a431ac800cee976a7de69a761c86245a5b5b535781814663581963d195ebf959297126fb69c57ade86901f756992b33f60c0d018564c3f1800d2d00fae5ccf13b7b2ee00ec6b23cd700302a53875ea94c736d040348615739cf74b5fe32a50b1f5ebd72f5bb62c752da03c1ac3cac8e0feeacf272f77f37d34c7650f8a3a7cf8f0f7bef7bd6f7ce31b070e1c485d17289bc6b00232b80a2afbe455f73e7e1addf6eddbfff22ffff2a9a79e4a5d11a89cc6b05cae075747651f9d26fcc0d197952b57eafe3200680ccb2283b3e5ccfbf553d58454860c1972c92597547db33b76ec38fbecb35b5a5aaabe65a88526690c653064454b4bcbf5d75f7ff7dd77d7e2f9be2fbef8e21b6fbcf1c94f7ef2fcf3cfaffac681cac8e0ac18a867799468fcf8f17ff1177f3179f2e4daed62c3860d5ffffad7afb8e28acf7ef6b3679d7556ed7604fdd1548da17bb22013aebefaea9a0670deca952b972c5952871d0145e907f7a23e67614d75ae47a69c3a752a7515680c1ac35ad30f068034643000a4218301200d190c0069b827eb03d61907088d617de90703401a321800d290c10090860c860ce9eeee5eb366cd2bafbc92ba22403db8270b32e1c891234f3ef9e4b3cf3ebb7bf7ee850b17ce993327758d809a93c19009bff8c52f1e7df4d1d4b500eaca58346442777777ea2a00f52683a129747676b6b4b4a4ae05f021c6a2a1295c73cd35975f7ef9ebafbfbe6ad5aaf6f6f6d4d501226430348f61c386cd9f3f7ffefcf99e5d0819612c1a9acea0410e7cc804872200a4218301200d190c0069c8600048430603401a321800d290c10090860c068034643000a461ad4ac88a969696b163c77676764e9d3ab5ea1b5fbf7efddebd7bc78e1d3b76ecd8d1a3477b7e0364810c864cf8e8473ffaabbffaabadadad35dafe1b6fbcb178f1e2dcebbbefbefbce3befacd18e80d2c960c8848e8e8ebaedcb331b20235c0f0680346430348551a3468d1b376ed8b061b51bee06ca652c1a9ac28d37de78e38d37e65e1b8b868cd00f86a6e3f9c190110e450048a3e1c7a21f78e081124b7ee73bdfa9b87cee75e13a945efeccf7965b9fea96ef59db2cd4e7ccf2f5f9db01d4997e3000a4d1f0fde09c72fb3159ebf794d2572bbd7c65dbef59a616db2fbd7c7ff6c5c093cdb199ea963716d5b4c7b57e3000a43140fac1c0c056ddb196fa8c2d955bde58541392c190092fbcf0c2abafbedad1d1d1d1d171f9e5975f70c105a96b04d4dc00c9e072afb8d462fbfd51eef6ab7b85e9ccdf4cedae6055b6fdfea8f5dfae5a76eedcb962c58adcebf6f676190ccdc0f5600048a3e1fbc1a5f496fadf132af75a4badb7df28f745f775c7633daf27d5fa6f473655fc972df0c6da8dfd188b6a5a0d9fc13030cc9b37afadad6dcf9e3d7bf7ee1d316244d5b7bf65cb96952b57eedebd7bf7eedd575e79e5edb7df5ef55d00e592c1900993274f9e3c7972edb6bf6ad5aa9ffef4a7b9d7975f7e79ed769411e58ecd147e63adefbb3616d5b45c0f0680341abe1f5cd9395416eea3aee77968adaf4595b2f772af5d153eafaf56dd0012d20f0680341abe1f5ceb95a24b295fbbd56deab3fdda95efff55a87afe6ec9b2ea5e35ac6c6ca6ff7bec8bb1a8a6a51f0c0069b41c3d7ab49f9b78e8a187722f162c58d06b817beeb9a79fbb00fae9d1471f5dbc7871eef5c2850befbaebaeb4f581c692effdf795740b172eac60b3fac10090860c068034643000a4218301200d190c99b076edda6ddbb6a5ae0550570d3f3f180686d5ab572f5ebcb8b3b373f6ecd9b7dd76dbb871e352d708a839fd60c89077df7df717bff8c5f2e5cb535704a807190c0069188b8626326ad4a86baeb9e6d65b6f4d5d112042064393983469d2effffeef5f7ae9a5adadada9eb027c40064353b8f2ca2b535701389debc10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c1d074babbbb5357018890c190119d9d9df5d9d19a356b962d5b569f7d018559ab1232e19a6baee9e8e8f8c10f7eb06bd7ae1aede2dd77dffdc10f7ef0d24b2f2d5cb8b046bb00ca2283212be6cc9973d965972d5dbaf4c4891355dff8ead5ab172d5ad4d5d555f52d031593c19021adadadb7de7a6b2d3278eddab5f9006e6969a9faf6810ab81e0c99d3dede5ebb8d4f9e3cf9baebaeabddf681d2e90743b3686b6bbbf3ce3b6fbffd768f10868c90c1d0143a3b3bbffce52f4f9c3831754580ff2483a1295c73cd35a9ab009ccef5600048430603401a321800d290c10090860c864c3874e850ea2a00f526832113962d5bf6e28b2fa6ae055057321832e1c48913fff88ffff82ffff22fb558a812c826190c19b26cd9b2af7ded6befbfff7e4df772ead4a99a6e1f28910c864ce8e8e8c8bdd8b66d5b4d9fef7be8d0a1e79f7fbe76db074a278321136eb8e186193366e45ed7ee0983dbb76fffdad7bef6eebbefd668fb4059643064c2902143bef8c52fde70c30db5dbc56bafbdf6d77ffdd7bb77efaedd2e80b2582f1ab2a2adadedb39ffdecd4a9530f1e3c58f58daf5fbffedbdffe76d5370bf4870c866cb9f1c61bf7eedd5bf5cdbef1c61b55df26d04fc6a22173c68e1d9bba0a403dc8600048430603401a321800d290c10090860c068034cc4d8266f4e8a38fe65fcf9e3d7bead4a985cb6fddbaf5e5975fcebd9e3061c255575dd5d2d252a0fcd1a347972e5d7afcf8f1d2ab74d555574d9c38b17099f5ebd7e727594d993265eedcb985cb1f3e7cf8c9279feceeee2ebd1ad75f7ffd9831630a9779f3cd37d7ad5b977b3d63c68c59b366152ebf6fdfbe72d71ffde8473f9a5fbeb42fafbcf2cadb6fbf9d7b3d6bd6acfc3a6b7dd9b973e70b2fbc507a1d060d1ab460c182f6f6f6c2c59e7ffef95dbb76e55ecf9d3b77ca942985cb6fd9b265d5aa55a557a3a3a3e3a69b6e2a5c8d53a74e2d5fbefcbdf7de8b88969696abaeba6ac284098537bb6eddba37df7cb3f46ad4880c864c3871e244d1c6ae5a162f5e9c7f7defbdf7160de0d5ab577feb5bdf3a79f264444c9a34e94ffff44f0b07f0c99327ffeeeffe6ec3860da55769debc794503f8d0a143dffef6b70f1f3e1c112d2d2dfff5bffed7a29bfdc94f7ef2f39fffbcf46a8c1c39f2ce3bef2c5a6cf1e2c5f90cfefce73f5fb4fca64d9b7afece4b71d5555715cde04d9b363df6d863b9d7fbf7ef2f9ac1c3860d2bb71aa3478fbeeebaeb4adfecca952bbffce52f0f1a54688475ca94298f3cf2c8f6eddb4bafc6d6ad5befbbefbe0205060d1a74c925977ce52b5fc97d3c9e78e2892f7de94be3c78f2ff0961933663cf7dc73355d9bbd148933f8c48913cf3cf34c6e55a0418306cd9f3fbfb3b3b3f05b366fdefceaabaf96be8bd6d6d69b6fbef9acb3ce2a5cace789edf4e9d32fbbecb2c2e58f1f3ffec4134fe45aa5125d7ae9a5e79f7f7ee132070e1c58ba7469ee757b7bfbc73ffef1a29b5db162c53befbc537a35468d1a75e38d37162df6cb5ffe32bf5a53291d94eddbb7af5cb9b2f46a44c42db7dc327cf8f0c265d6ae5dbb7af5eadcebc99327cf9b37af70f913274e3cf9e4936575bf66ce9c79d14517152e73f0e0c165cb96e5fedc43870ebde1861b860e1d5af82d3dfb28a5183d7af4bc79f3860d1b56fa5bfaef139ff8c46db7dd56b8ccb66ddbfee11ffe21f7b30f1f3efc0ffee00f8a66c30f7ff8c3b202b8a3a3e3539ffa54d1628f3df658ae858d88ebaebbae700b1b11c78e1d7bfae9a74baf4644cc9e3dbb707e44c4c99327376fde9cfff2dc73cf2dbad9fdfbf797558ddc5e8a96193d7a74fe75294b900e1f3e7ce2c4893b77ee2cbd1a2fbcf042d10c9e356b567eb33b76ec78edb5d766cf9e5da0fca041837ee3377ee35bdffa56e9d578f6d967e7cd9b77f9e5971728336edcb8cf7ffef3b9c5e08e1c39f2f77ffff77ff6677f3664c890becab7b4b47cfad39fdeb76fdf6bafbd567a4daa2e65069f3871e25bdffad69a356b725f7eee739f2b1ac05bb66cf9c637be51d68af6f7dc734fd100deb367cfa2458b721ffab6b6b6af7ef5ab4537fbc4134ffce8473f2abd1a1171f5d557172df3f2cb2fe7cf2867ce9c594a063ffef8e35bb66c29bd1a93274f2e25835f79e595fc40cdb061c38a66f0d0a143cb3dc58e88bbeebaab7081b3cf3efb9bdffc66ee2fded2d2f295af7ca5f087a4bdbdbdadadeddffffddf4baf434b4bcb9ffff99f4f9b36ad40991123468c1d3bf691471ec97df9d24b2ffdd11ffd515b5ba1c3e7231ff9c892254bf22710a578eaa9a7fef00fffb06e317ce79d7716fd80eddbb76fd1a245478f1e8d88b6b6b62f7ce10b450fd255ab563dfef8e365d5e49e7bee19356a54e1327bf6ec79f2c92773af5b5b5b4be9ad3efffcf3478e1c29ab2685c32367ebd6adf9673cb7b5b5153d34a26619dc73ccbcc465c067cc98515606bff5d65b070e1c18397264813283060dbaedb6dbfef55fff35f7e5cf7ef6b3a2bfc64b2fbd74d6ac5965adddf6cffffccf7ff5577f55f8e8b8ecb2cbeeb8e38edcd8c0f6eddbffe99ffee9777ee7770a8cd9b4b6b6feeeeffeeedffeeddff63ca9aab364197cead4a9ef7ef7bbf9005eb06041d1b3adc3870f7fe73bdf292b80a74e9d5af4343f227ef8c31fe63ff1b7dc724bcfb3cb5e9d3c79f297bffc65e9d5888873ce39a7e8697b44ac5fbf3effba94f3eb88d8b76f5f593529f1d9b1e59e628f1d3b76f4e8d1655566c58a150b172e2c3caa3962c488abaeba6af9f2e511d1ddddfdf39ffffcd39ffe74e1cdde74d34d4b962c29fdc904dddddd8f3cf2c897bef4a5c243c1575f7df5dab56b73fdaa0d1b367cfffbdfffcc673e53a07c5b5bdbeffddeeffdcddffccdb66ddb4aacc9962d5bbef9cd6fd62e86172e5c987f7dc105175c72c92585cb777575bdfaeaabf90373debc7993274f2efc9623478ebcf3ce3b3d7754d4b871e3aebdf6daa2c5b66cd972c71d77e45e9f77de79a52c25d6dddd5d564ddadada2ebdf4d2b2363b66cc98d6d6d6a26f99366d5a59358988c2b197337dfaf4fc665b5a5a4e9d3a55b4137fcb2db794bb0adbdebd7b8b56e6faebaf3f72e448fed464efdebd45f7f25bbff55b4f3df5545997eadf7aebada22361bff66bbf3671e2c4fc93c1366cd85078887ec890217ffcc77ffcf4d34f1f3a74a8f0962be86394225906fff0873f5cb16245eef5dcb9733ff1894f142edfddddfdddef7eb7ac67aeb5b4b4fce66ffe66d12364f3e6cdf9fb14860c19524ad7f3a5975e2af7c476ce9c39a514eb398257f42a5d449c3c79b2dcf5fd4b1c3f2f37832362c68c19650d47efdcb973ebd6ad454f356ebbedb65c0647c433cf3cb370e1c21123461428dfd6d6f6894f7ce2e1871f2ebd263b76ec58bc7871d10fe1a73ef5a94d9b366dddba3522962e5d3a6ddab4c2278e1d1d1d0f3ef8e0d7bffef5dcad22a5c8c5f09ffcc99f54fdda70d1218733b5b5b5953264d2534747c7edb7df5eee8e4a3177eedca277609de6a69b6eaa454da64f9f3e7dfaf4b2de327bf6ec527ad8e51a356a54b97fd6a953a796d2b094abb5b5b59496b3a7912347967b5e528a9696966baeb9a6acb70c1e3cf8d65b6f2d5a6c4065f0a64d9bdadbdb737f80d1a3475f7bedb5454fdf366edc3865ca94a277dcf574c1051794f269dbbe7d7bfea33073e6cca2572823e2f8f1e3e57e7a4a69cb8e1f3f3e7ffefcfc97a59c929f3871a29441b99e4aec63cd9933277ffa5238f3f23ef6b18f4d9a34a9accabcfffefb45cb4c9932e5befbeecb9f7e6ddfbebde8e5db2baeb8e2739ffb5c594f3e68696979efbdf70adf10dbdedefec52f7e71d9b265b9f3fdfdfbf7efdfbfbff020ea983163bef4a52f3df3cc33f92e42299e7df6d972c30f68442db92b3dfdf1d0430fe55e2c58b0a0d702f7dc734f3f770100093df0c003b9177d255d65dd7a6b7400401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048a32d75052af1a5eff7f2cdafde5b76190048483f1800d290c10090860c068034643000a4218301200d190c006934e4dca452a61899860440c6e90703401a321800d268c8b1e8247a5d782bcfd077f635d05fb081aa0af4877e3000a491dd7e70e1ae4094df1ba8fa0601a03ff48301208decf68381845c93863ad00f068034643000a4218301200d190c0069b8278b66e13622206bf48301200d190c0069188bae958dbbe3cd6db1756fbc7b308e9d8888e8181ce346c4b4ce983539a68cade6bef61f8e753b63dbded87520f61f8e43c7a3eb647477477b5b0c1f126386c5396362faf8983121da5aabb9df5e759d8a353b62cd8ed8b12ff61e8ae327e254770c1f1a9d23e2b76f2ef2def70ec5fa9db1eb40ec3e10ef1d8a635d71bc2b8e77c5a096686f8bc1ad31bc234675c4e8b3a273644c1a151347c7900c7c84bb4ec5e6ddf1f6ded8b12ff61d8a0347e2e889e83a19835a62707b74b4c7d8e1d13922ce1913e78d8f31c3aa5f813defc7eb5b63d3aed879200e1f8beeee183624860d8d7346c78567c78567c790f6eaef14e8bf0c346003ceeaedf1f86bb173ffe9df3f78340e1e8d4dbb63e99b31656c7c7c769c37be5f3b3a71325ed91c2fac8fedeff55ee0d889387622f6bc1feb76c653ab63705bcc9e1ad75f149d23cadb5189cb359cea8ee7d6c6d2d5f1fed1d3cb1c3812878ff5b985ae53f1c2fa7879539f3fc8a9eee83a1e4722f61f896d3dbedf12316e444c1f1fd3c7c779e363f45955f829cab26157acd810abb7c7f1ae3eaa7d2c0e1f8b3defc7da773ef8e6986171f9b9317b5a4c1c55c91e4fb3fb60fc6c55acde1edd1ffefefe23b1ff486c7f2f5edc181d83e3e64be2ea0ba3ade0b057d1c55ccb2aecea3b9442065753d7c9f8d1ca786953f1925bf7c67f5b12d75c1877cc8ed68a2e08acdc184fbc1e078e94f196e35db16243acdc18575f101fbb2c0657f58f7ff0687cef99787b4f9f05bafbf8fe5b3be2d197e2bd4395ecb43be2dd83f1eec1787143447ddbfdcdefc64f57c596be7fdebebc772896ae8ea5abab50dba7df8ac75f8b93a78a143b723c1e5b15afbd1dfffb8d71d6e0feee14a822195c35c7bae27f2c2daf517e766dec39189fb92edacb19223e723cbeff7cacd9516e053f70aa3b96af8d35efc47fb92e268cac7023a7d97f381e7e32f61d2efb8d2f6e8c1fbd18a7facae74c3a792a7efa6a3cbba6cfb38a3a38d51dfff38558b5b98cb76cdd1b0f3f19bf775b0c352e0d99e19eaceae83a15ffb4ac925ed19a77e2ff7d2eba4b6ecedf3b14fff72f2a0fe0bc770fc6c34fc6d6bdfddd4e441ceb8a479e2a1ec067fe8c1b77c5ff5ad160017cf4443cb23496a70ee07f7bb6bc00ced97d2016bf5c830a019592c1d5f1e84bb1697785ef7d735b3cb3a6a492ef1f8d4796c6bb072bdcd1698e1c8fef3e5585adfd6865ec3e50f6bb4e9e8a1faeecefaeebec7857fc8fa76263a57fe86af9c92bf11f5b2b7cefcb9b62fdceaad606e807195c1db9eb91157be2f5d8f37e9132ddddf16fcf152f5696c3c7e37bcba3eb64bf36526287ecb48ee3da77aa763251373f5851e88277dd3cbbb65f6f5fdebfb70355e47a70269c38193f5a199fbfa95099e56b63e3ae22db993131e64e8fa9e362c4d018d412078fc6c6ddf1fcba4263cebbf6c72ffe236ebfbc926a97e7c321fcc6b63e8afdffa676c695e7c5b9e3625447b4b745d7c938723c0e1f8f5dfb63c7bed8f65e6c7937ba8add8e54452f6d8ad7dfaedfee6a67cd8ed87f244675a4ae072083ab6e64475c3b332e9a14a3ce8a9688fd4762cd8e78766df1fb7ed7ef8c1dfb62d2e8defff5f0f178f23f0abd7df8d0f88dabe2c2b33ff4cd31c362ccb098373d9e59138fadeaf3aaf3f23531ff8222137b4ad43922e64d8f0bce8ed16745c7e0e83a19078ec4ae03b1f9dd786bfb874a6eeb631a52ce5533e2ee2ba2a5c77706b7c5e0b61875564c1a1db3a745441cef8a753b63f5f6787deb0733b06be75857fc74554925db5b63f6b4b860624c1e13670d89f6d638742c0e1e8d2defc6865df1d68ee2b73197ae25e223e7c6ec6971ce98183e248e1c8f2d7b62f99a22a3e5a7ba63fdce9837bd6ad5002a2683abe9e273e2def91f5a0f61fc88183f227ee5fcf8c10bf17ab16b78cfaf8b5fbfb2cf7f3ada77cc740c8edfbeb9d04dced7cd8c53dd7da648d7a958be267e754e91ea15d6d2120b2e8beb667e68aad5e0b6e81c119d2362d6e4b863f687ca1f2c38abea96591f0ae05e0d6e8b599363d6e4583837566d8915eb2bac792956ac8f437dcf6fce9b3d2d7e75760c1ffaa16f8eec88911d31794c5c73611c3c1acfaf8b67d6f43e9fb82cc386c47fb92ea675fee777860f8d5993e392c9f1b355b1ecad42efddb6b7970c3e6dae542da65303a7713db86aa68c8d4f5fdbfb824483dbe2deab636a672fffd4d3aa2d71a28f4bb385af37df39b7f82ca3eb2feab3931d112f6deaeffdc9f75c15375e5cc65ce702a71411d151ce34d6c16df12be7c7173e56c65bcaf54209017fdb47e2def9a707f069460c8d8f7e24fef0e371c9e47ed567705bfcf6cd1f0ae0bc9688db67c739630abdbdafb550803a93c15573d7bc42eb10b50e8abbe715e9db1defeabd71dcb6b7d0cc9fce1131775af1eab5445c7d419fff7ae4786ceec7edbef3a69754879e0aaf31f9f2a6ca2b53753bf615bf15eeb273e3d659a56e70d459f1d9ebfa55a58f5d566899ad9628f2e778bf843e3d500732b83aa68e2bbe04f4a4d1c5bbc2bdde76bba1603ace2939fcce9b50e85f2b9e72d33aa8925bba8615ec2f3efa52fcf8a5d8ba3713b3873714bb15ae75507f47f2cb326c48cc9f51a4cca482fde0a3c7ab581da072ae0757c7a5534a2a366b726c7eb750815e57f9d85670258de9c5723d6f64c15b610bdf2455c0c5e7141980edd594b18566159fea8ee7d7c5f3eba2bd35c68f8cce11d1393c3a47c6f8113161643d9e3cd153d1f948979d5be4775b5db3a7161ff32f7cdb73e10b0140ddc8e0ea2870a9b5a7c257e9227a1f732e3c10fadf7e59d2ae8bda5bd18acd1131636225ef9a79764903ce274ec6f6f73e34443fa825268e8aa99d71c1c4b860629557bdeed5de6203d1179c5da440754d2be1511f430b5e50cfc2e8021032b85a3a4b5b78795cb107161de9ed42ddfef2d761ae40e11b950b985cecc4a257974e8931c32a7954c3a9eed8b12f76ecfba0977ce994b86e66f1939bfe28ba0ce7b9557d126551a59cf0157e441290118ed4ea28711dfca2c58ef4364878acdff3584a71a2d2bd8caa686271eba0b8736ef1094885e59edef8f73f8fffb5a20ab37dfa5274cbc386d46ad715efaeb28771017596dd23b5682352d60a8b7dcdf9297d778595f8e0a3a2c57a5d6be264ffd6922c51c56b4e55fc1c9e4bce89bb8add2b5e8aee881737c6bf3c53cde52f7a2afa9be975425aed14bea53ca7a5ffbf56a0f6b29bc145efbb29abdf53b470594f0f3c53d18c2fb158afad796b7d6f412a577f7e75f32f88cfdd18638755a11aeb77c6e3af55613b672a7a7e56eb55ba4e534abe0e92c1d008b29bc11dc5fa1687cb995f51b470598b429ca9c41b4d8b16ebf5a7aec36d4709cd3c3bfef08ef8e4fc387f427f7b6fcfad8b03955ed52ea068bfb39425b400ce94ddd67dd45945ee85d97d203a8bdde2d4b370d1ddf5c79e83252d82bfa7d893827a3d1518d51187fb6ee5ffaf5fabf7f5c8aa6b1d1473a6c59c6971f858acdb199bdf8dedefc5ce0365f72fbb4ec66b6fc77533ab5cbd51671549d9b7f796f15104c8cb6e068f1b5e642aed8e7d652cf8f7cebee2bbeb8fedefc5f90557c0c8d951ac1abd9e0a8c1d5ee88dbbf617597ca3819c35242e9f1a974ffde0cbf78fc69ef763cffbb1e760ec3c106fef89f78f16d9c2865dd5cfe071c38b2ceeb8ee9db2970903882c8f45179d8051f4e17765152e71826f5f4a7ca67ad162bd2ea45578f2cfba81fb48f6e143635a67cc9b1e1fbb2c3e7b5dfcd95d71df8d459eefb46b7ff5ab517405b4d7deaec918785a85af289b610c5591dd0c2eba10c18e7da53e50fded3dc53ba0bd2e7f5fbab7f7145f676ac7bed852b0671f1153c7f5f2cdf30afe2a566d699606b1a5252e3c3b3e39bf5099b2ee122851d1118e93a7e227af547fbf6915be0ba1e88004508aec66f039636244b115101f7da9f80ca5ae93f1e84b45ca8c185a647ddda2ba231e7da9d0dc9853ddf1e84b51382b07b7f5bed6c4b9e30afd2ade3b14cfad2bb59e67dab227fefb2f2b7f7bfd4de9ed3425afc41bd4cb72ce98185bec52c56b6fc792374adde0a163f1ff3cdbcf4ad55ce13bd1566f2ff4af4089b29bc12df1c1a3da0bd8f65e7c7759a153f28347e3bbcb8af750e74eafc22cd5b7f7c4bf3ddbfb7a1a274ec6f79f2b72793b22664fed7d9e4f4b4bcc3bafd01b7ff66ad94f5ce8ee8eb776c47fff657ce717c59f495023fff044bcbca9ec79c985578e1c5e9bdbd38a3e2321229e783dbeff7c910ee2d113f1f45bf1773f8dd7dfae56d596ff3b8100001576494441546a6578c17b0c1f5b154fbf15bb0ec4f1ae2267964001d9bd272b22ae9a11cbd7141968ddb02bfef62771e57971e1a498382a860d8eee88c3c763e7be58f34eacdc587c66f0a096b8aa8416b6146f6c8bbf7b2cae9d1933277d70d9f2c09158f34e2c5f53d2a28c05aa71f505b17c4d9f9dbcae93f1dda7e2e3b363fe8ce2d37b76ec8bffd81a2f6f2abe0463ad6ddd1bfff385786c555c766e5c7c4e9c37a1f80a8b274e1619f51d519b6727fccaf9b17475a1bbd373566d8e37b6c6ec6971e1c438676c0c1b1c6dad71f878bc7f34b6ee8d8dbb62f5f63aad7ad67f678f2af4b090e35df1d8aa786c55effffad57b6b54291868329dc1e386c715e7c58a828faf8f88e35db17c6d2c5f5be15e7e65468ca9c61a1139fb8f146a9b0a387f42a1458f4776c48d17c72ffea3cf02274ec6a32fc5d36fc5bce931ad33268c8aa1edd1d212878fc591e371f0686cdb1b6fef8dad7be260c6aee41d3a16cfad8be7d6c5e0b698d619678f8a09a362c2c8386b480c6d8b21edd1d212474fc49e83b17177bcb0bec8d94ce16be7151bd21e1fbf3cfe7d45f192274ec68b1be2c5621fdaec9b3c26566e4c5d0918e8329dc11171fbe5f1d68e1ade743aeaac587059ad365ebab6d6f8b52b8a94b9e99258bda3c8730cdf3b5428a733ee7857ac7d27d6bed3af8d7ce4dc2ad5e60c579c176fed28f506f801e092c9b1f8e566b9dd0f52c9eef5e09c8ec1f1bf5d5dabe7c5b6b7c6a7aeae7cb9e39eae2c78bdb6a8db2e2dbec843eba0f8ccb525ad04d2b4669e5d7c1e517ffcc655716ec13bc20692911d317352ea4ac04097f50c8e88e9e3e3d3d7f4773de733b5b7c667aeed7d3e6e05eebaa2f2d94d179f13d75f5452c9d167c5e76f2e3241b6690d6d8f5fbfb2b6bb18dc16f7dd18d36b33dc9d4177ce19e0eba442720d90c11171f139f13bb754f3aaedd8e171ffadd53ccd6f1b149fbba1924ed20513e353d794b1c27ee788f83f3e1a332696bda3816dc4d0f8ed9bfbbbe0682986b6c7e76f8a6b2facc28df4d93776787c727ef54f7f81bcc6c8e0889832361eba3d6ebab8bf27e683dbe2e64be2a1dbabffd4f7a1edf1db37c7eca9c54be65d7d417cee86b2dbb8e143e3b76e8a5fbfb2f8fce9a24675c40d17c5830bfabb9db42e3e271eb8adfa7fd0bee49e7cfcbbb736c5b8f4acc9f1c06d03673154c89a461a691adc160b2e8feb2e8a951be395cdb1b3cc5509278d8e39d362de797156ff1e9154407b6bdc7b757ce4dc78fcb5224f89983c363e7e79494b4cf7aa25e257ce8f39d3e295cdb172636cdd53c61ccd412d71ce989831212e9c14d3c7a7eccffd9f0b63e3aed8b83b36bf1b7b0e963dcd74705bcc3c3baeb930cde0f0b4cef8bddb62c3ae58b121dedc56c6da206386c565e7c69cc6595f7ad2e8f89d9be39d7db17a47bcbd27761f88a327e2e8895a3dad199a4acbd1a3fd9daaf2d0430fe55e2c58d07b7fea9e7beee9e72e7ab5ff486cd815efec8b5d07e2c0913878248e777dd02eb40e8a21ed3162688cec88092363d298386f7c8cace3dd4cdd111b77c59bdb62ebded8f3fe078f2c1cda1e9d23625a67cc9a5ce52ed4c1a3b16e676cdf1b3b0fc4fec371e8589ce88a93dd31b83586b4c7e0b618d9119d2362fc88183f32ce1d579ddbd0aaeb7857bcb33fded9177bde8ffd8763ff913878248e7545d7c93871325a07c5e0d618dc16c38644e7c8983032268d8e19136a75b35eb9ba4ec5e6ddb1654fbcb32fde3b14078e7c10512d2d31a42d3a06c7d8e1316e784c1e1be78dafe62515a06e1e78e081dc8bbe926ee1c285156cb691fac1a719d51173a74564b23fd11271fe84cabbb9e51a3134e64e6bec47f70c6e8ba9e37a5f2e3bfbda06c58c892ed203656b98ebc10030c0c8600048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c10090860c1e20ee5e3221751580ba72d40f00327820c81d8a772f99e0988466903fd81df28d4e060f348e4918d81ce303890c6e780e4868665a80862683079a1fdfb22b7515801a728c0f2432b8b1390506b4038d4b060f284e90a11938d2070c19dcd81c8a8076a071c9e086f7e35b76e58e40c721348ffc51efc06f68327880701c42b371d40f00321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c10090860c068034643000a42183b3eeee25133ca01ba8cca2458b162d5a94ba16f4a92d750528493e863d2905284aee360afd600048430667da69a3d03ac140291e7cf0c19e5fea1667960c068034643000a42183b3cb40345031c3d10d410603401ae6266557bee36b7e3050b1d33ac4648a0c6e0046a1810a48dfec33160d0069c8600048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c10090860c068034647086dcbd6482470503b5a6a9c90ecf0fce8afc21917fe1b1c140159d96bb772f99a091494e3f38a31c1b4075695532480603401a321800d290c10090860c068034647026982700d49f962739739332217fbf62ee9070fb22500b3fbe65974626536470b63830809ad2c8648ab1680048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c10090860c068034643000a4e1d985899df60c6d8f15036a6ad1a2453dbf7cf0c10753d584d00f068054643000a4218301200d190c0069c8600048430603401a321800d2303f38311382817a32213853f48301200d190c0069c8600048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c10090860c0680346470b62c5ab468d1a245a96b010c581a994c694b5d0122224e3b24162d5ae439db40d5e59b9afc0b4d4d5afac199e03000ea4fcb939c0c068034643000a4218301200d190c0069c8e08c327900a82ead4a06999b94150f3ef860ee0871a722500bf9b64553931d3238431c12401d686ab2c3583400a4a11f0cc9dcbd6442eec58f6fd9d5cc7580a6258321bd5c10d63905f3e90ba4622c1a00d290c10090860c068034643000a4218301200d190c0069989b04c9e42623259f23646630a4228321b15411287a213963d10090860c068034643000a421831bc0dd4b2624bf6d0768389a8eec734f5676397880fef368ac2cd30f0680346470769d76d2aa5b0c94eeb4164327389b643000a4218301200d199c6986a3810a18886e14321800d23037a931388d054a976f310c9e659c0cce3ae90b544c039271c6a201200dfd60a89fecaf5894fd1ac240228321815cd4652ae75c3884fa33160d0069c8600048430603401a321800d290c10090860c068034cc4d82fac94d46caf82ca04ccd9882814d0643bd6533e4b2592b18d88c4503401a327880c8f8f02650758efa01400637bcbb974cc81d8a0e48681ef9a3de81dfd064706373f801da81c6258307148722340347fa8021831b9b7b5901ed40e392c1038d136418d81ce303890c6e784e81a19969011a9a0c1e681c9030b039c60712193c10e48ec91fdfb2cbc109cd207fb03be41b9d0c1e201c8ad06c1cf503800c068034643000a4218301200dcf2e84dacacfe66cb8ab778d5b7368143218ea2417690d9167568180fa30160d0069c8600048430603401a321800d290c10090860c068034cc4d6a3a0d34436660c8fdaa1b74b68fcf499d393c9b8d0c6e2e0d9a04034063b5aa8d55db81e7ee2513fc099a84b1e86671f792093d03581843d69c76843a489b810c6e16679e563bc2213bce3c1e75859b810c6e6a6218b2c091d8b4647013715a0d8dc2d1da2464707331220d596314ba99c9e0a6d3f3f0fef12dbb1ced90d66987a143b2a9c8e0e6e55087ec703c3627f3839b91a31d32c881d984f48301200d190c0069c8600048430603401a321800d290c1009086b9495013f9c58f06c0849381f4b340a6c860a8ad867e2abba54ca1a68c4503401a329832e81541291c2994480653aa5cb3a27181c21c29944e0653929e0d8ac605fae248a12c3298e2ce6c4a342e7026470ae592c114d7eb3dbd1a17e8a9d723a241ef87a76e64302511c3e53aedc1ec8d6e80fd38552780a98cf9c194eac7b7ec3aada1d1c414d5e8bfa246af7fdd383aa88c7e3065e8d9ac6862a02747071590c19427d7b86862e04c8e0eca2583299b2606fae2e8a02c321800d290c10090860c068034643000a4218301200d194cfd58578b46e1b34a7d58278bbaca356de66f9059d2977ad20fa64e3cd38decf329a5ce6430f5e0996e649f4f29f5672c9a648c4b9311b296546430f5900b5a2d1d0dc4d92175602c9afa39b3511bf0cddcdd4b26e4fe4b5d910a357afd4bd4849f4c3242065357cdf37cb701165d03ecc73953f37c32c91463d1d49b7169324bfa5267fac1a4a1b1236b7c26a93f190c0069c8600048430603401a321800d2705f348daa5196d9ca7e0d0b68949bd81be5c300a791c134a47c2a64b6f1cd60952a96e59fe5b4a72c64b9aa702619cc4090d924a676b2df3b87a25c0fa6f1f4d5f86a949b87cf0003830ca6f1f4d5dfd50f6e1e3e030c0cc6a269488d72af1075237d6944fac134b01fdfb22bdff26a829b4dcf3fbdbf3e0d4a3f9886a7fd6d5afef4343afd60004843064344133c1f376b162d5ab468d1a2d4b580c48c45c37fcac7b041ce1a91bbd0930c865e08e3ea12bdd02b63d1608e5332b299262783a14f3ac1d5f2e0830fa6ae026491b168f8cfacd521ae1ba90c2183a127615c6ba2177a92c1d08baa8f4237d64d5ef9cbb4558c4ce90b6792c1505bb5e852f7b5cdaa077c2e8cc527d4887bb200200d190c0069c8600048430603401aeec982c6d31037570345e90703401afac150270dd479cd4d46b29833d49a0c86da6aa0e83d8d69c1506bc6a201200d190c0069c8600048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c10090463d9e5df8f02b0fd762b3f7cfb9bf169b855abb7bc984dc8b4c3dd6309bb58281cdf383a17ef2399765b94a4a62a80363d10090860c0680341a20835df7056040ca7a060b600006aaac64b0ac05a0d96422830530004d28f1dca47cfad6680e3164530667fee4aad410b3a760c0c8c4fce09e01ac4fcc0096c1e83d4df66b080349fab168010c4073ca443f387a4b5fa3d3000c6ce9fbc1f7cfb9bfd7eeaf3e3100035bfa0c3ecdc3af3cac070c40334893c1b9beeffd73ee3f2d6ef35fe65ee80a033080a59f9bf4f02b0fe7b3f6cc543e2d86759101183032714f563e597b26aeb80560604b7c3d58d002d0b4d2f483452f0064eebe68006812d9ca60fd63009a47b63218009a870c068034b295c17dad5b0900034f26e60743335bb46851eec5830f3e58cffde61f15ec7985908a0c8664f2e99b562e8c2531d45fb6c6a201a079c8600048430603401a321800d2489fc1262301d09cd26770f411c3b21980812d2b7393ee9f737fcfc5a205304da5ce338373729391f2b38481fa4b9fc10fbff2702e71f3b92b80691249a2f734a605434299188beef571499ea104c0c096890c8e331257000330e0652583a347ee0a60009a41faebc13d495f009a4786fac100d054643000a451efb1e87ece3b32580dc080a11f0c0069c8600048430603401a321800d290c10090860c068034643000a491adb52a81458b16e55e54fdc986f947057b5e2164840c86acc8a76fade5c258124372f5ce600b5d01408eebc10090860c068034643000a4518febc1fd7c5612000c48fac1009086b9499039559f199c939b8c949f250c242783212b6a14bda7312d18b2c3583400a4218301200d190c0069c8600048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d2f0ec42c8aefcb37e2b7ee060ffb700d48e0c862cca67677537288921538c4503401a321800d290c10090860c068034643000a4218301200d739320d3fa399b28f7f6aacf7402aa42064316557722af69c1904dc6a201200d190c0069c8600048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c100908667174203c83f00b8c4a710965b1e48420643a6e5d3b43f6f97c4904dc6a201200d190c0069c8600048430603401a321800d290c1009086b949d018ca9a5f942bdccf794d40adc960c8b4fe4ced352d1832ce583400a4218301200d190c0069c8600048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d2f0ec42c8b4fc33802b7810617fde0bd4810c86c6900bd412d3349fbe4096198b068034643000a4218301200d190c0069c8600048430603401ae62641a6e52623553cd7c8cc60c832190c0da0dc2815bdd0108c4503401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c10090860c068034643000a4218301200d190c0069c8600048430603401a321800d290c10090860c068034643000a4218301208db62a6eebf1c71f2febfb00d0ccf48301200d190c0069b41c3d7ab4ff5b59bc7871ff3702000d6ae1c28515bcab3afde0caf60d00cdac6a63d1621800ca52cdebc177dd755715b70600035b3533b8bbbb5b6f18004a54fdfba2c5300094a2267393c430001455abf9c16218000aabcefc6000a05cd6c9028034643000a4218301200d190c0069c8600048430603401a321800d290c10090860c068034643000a4f1ff01c61b116383b079560000000049454e44ae426082,'image/png',641,791,'',0,0,0,1283639466);
INSERT INTO `$prefix$flip_content_image` VALUES (5,CURRENT_TIMESTAMP,1,'shop_category_food','Generische Kategorie','Speisen',0,2,0x89504e470d0a1a0a0000000d4948445200000080000000800806000001b439515d0000000467414d410000b18e7cfb5193000000206348524d00007a25000080830000f9ff000080e8000075300000ea6000003a970000176f97a999d400002e3749444154789c62f8ffff3f032518208028d20cc200018457f24236032310975dc862b0c3a50620805038974b182e5c2a62f87f319fe1ff855c04aeb01286b073e0f8174c0f4000c1353f5ec6f8ffd142c6ff0fe630febf3f83f1ffddc98cffef4c64fc7fbb97f1ffcd0e86ff375a18fe5f6f64f87fad96e1ff954a86ff40cbfe83f401041058f3e72bccff3f5e60feffe114f3fff72798ffbf3bcaf4ffcd01a6ffaf7733fd7fb583e9ff8bad4cff9f6f64faff740dd3ff272b98fe23592602104060037e3e65f9fff9ff1c30fefec6f0ff8f472cff7f3c44e0ef0f98ff7fbbcbfcffcb0de6ff68962d020820b00191cb18feff7ec582c02f59feff7a01c5cf81f81904832c02e32710ec3b9fe13f400041c2609dedbf295d12ff7ff8b9fd8f5bc9f03f760550d8cb0b8ca39633fccf9dcdf93f0268c9e6628dffc9f359ff872d61f81fba98e17f0810030410d880385dfeff20dc53aaff3fac96118c33d219fe6fe564f89f1fc70017036190d8c5ececff8fd61afd075afc1f2080202e78c7f01f6608085fee36004b82f0cbde60b006640c5233a79cf33f481f4000c10df8ff6d05580086d774b3ffdf53a1f5bfc089f7fff57dcc2872c818208010090959026418087f69fcffff53daffbfafa5feff7aca008c1186ffdfef33fcff7a8be1ffc72b907400104028291159f2c32586ff6fcf32fc7f7d82e1ff8bc30cff9fed67f8ff640fc3ff87db19fedfdfcc9004d30310401467268000a2d8008000a2d8008000a2d800800022541e3800710a3e35000184c2b954ccf0ff5221c3ff5e377e70fe3f9f832813ce6723ca03643d000104269e2c677af27809e3ff87f381597416e3ff7b5381e5c12460593081f1ffad2ec6ff37da80654133b02c6860f87fb51a581e94032d2a661005e9050820b0019f2e01b3e85960f63c09c4c799ffbf3d042c0ff602cb829d4cff5f6e0796059b99fe3f5b072c0b5602f172607900b50ca41720804065c1c22fff67c2cb831f8f5930ca836ff798ff7fbd0d2c0bae31ff47b60c6400400081cb82c5c718b19707cf11e501bc2c809607f3f6435c001040602f5c2d3301e7fd786039f0dbc7e37f0c903e9d61f4ffa7affbffb515aa60b9f0a50c600c2a0b90cb03800082970737fa8d30f23d08238ba56740c440d9bc682507d805000104ca85221b27b0a39407b0b2e0df1a7b8cb2a0d7570da2e61d243a0102089195df8bc2b3f30f60d6cdb4e2052b9c5bc189b32c00e9050820ccb2e07312a23cf85cf8ffdf3b97ff7f5e30fcfff91868f00386ffdfee30fcff7c1d98dd2f430c0008209494882cf9fe02c3ff37a719febf3ac6f0fff94186ff4ff731fc7fb493e1ff83ada82911208028ce4c000144b101000144b101946280001a7007000410518a80c5c965a4a6c5ff35e11cff5b1c05e16c10bd2090eb7f25a8190252938d822df0990d10401802c022c807d424011543e066491bb469d20c6d9e34409a285b52d8c1cd1450d1046aaa80cb4260db68591827a47d9487da4642c20ac8f60104109c012cbec2c1cd999d90260da81803376b36439a36a0e20cdcbc590969e2808b3560330754b4819a3ae0b2740ea43c05b5afc065ea6448b90a6e674d80b4b5601e83d90b1040600258e4317d7be78a6822bd5043348bee419a46a0e210dc3cba066922818bc50b90a211dc543a09699b81ca6350fb0c5c261f8094cb383cb60664374000811d002c3a13509a5340fcf56701dc41c8f8cbff59ff7fbc334569668130b81c87e14798653a7a7b0f16020001c4006c8e15839a63201c03c5d1cb21380a8a41657eee7a86ffd30e32fedf730118f40f813e7fc68c5a0720e1578f98ff9fbecaf47fed09a6ff651b19fe072d42e0c08528581420801009105402cfb5ffffbfdc05a508c787e38b10c57d38b0ae2c4a61fd3f3d44e8ff715f2d78fb1284736673fc8f05a617901da076260883ea1690bd0001047700727500c32dde92ffdf4c0ac0a81270e1933e5eff77cb8a82eb9cedfc6cff8f39d9c1e5ae97a4821df0638535a43a81da0b10401007bc63488554298cf05a6175273b8683d0eb2b74bca9401dabfa2bbb59705647000184880264890fea882ae9a30eeefa0c1f7e0bc1ffde30fcfffb8a015ca5c130a8a90bacde6680ec050820ccd2e92dc36f021ac1ed6450dd086a0e83aa40509318540d829bc597214d635075086a1e83aa44501319542d829ac9c0aa7131b27d000144b018066a14066afc01aa53416d6c50bd0a6a6783ea56505b1b54bf02dbdbffef0153fbdd0d0cff6faf65f87f6b35c3ff9b2b188e5e5fc6c044c87c80001af0ca08208006dc01000134e00e0008a001770040000db803000268c01d001040646b04b67454803806884b2e64314400b10439e6000410618b721942909b63205c016d7a817a7d673219a16222d89a63d709990f104058052f973230c19a58a02ee7c502c830d44c5f1e94e6d5e92c4630dde12cf07f63343b7c586a4314fb7f744703b10a36bb00020843e0761fb0d9d4036c3675024bb376607bb015da1e6c82b609eb21bebd5a056c0f5630fcafb111fe8fec58901cfa98193246b70f20805038e076de52601b6f11b08db70088e702db78b3816dbc99403c1dd8ce9b021d4b83b5f3808eedf214c0ebd8ab350c70c7024316ec58643b010208ce78bd07d876db85e857bfd806c45b806db74dc006e906205e0b6c94ae426b9492e858a4909d05b3172080c0c4e7abccd33f5d0636342f029b5ae7808dcc33688dcc634cffdf1d0136340f021b99fb810ea58263610e00082030016a28c21b9dffa6c11b8edfef435bc47780f816b0557c1dd822be0a7428151c0b730040008109500bf7c76b65cc16f09f2ed4562e8e962e398e8539002080c004bc490e6d96ff78a78fb5490ec2dfbea5fffff99c0ba5598eb5494ec0b130070004106898451bd4ec0635bf3fbfc0d1d47e89c0e88e451e96c5188e798a39540b722468b816e60080006240ea13ecc1d52700d18d3b18ffaf38c1f8ffe24d605c3e0606f74bec8efd0514bf0d8c867dc0fec3e4bd8cff2397e2ec13801d011040887200d8aa058d1dbf0d7504b7e567b74a83c790df00f9494b99c01d96effe6e60b94d65eaf03165982341fc9405ace036ffa6120df8b812fad8126c7c0984831731cc03082098e55fc04deb0a97ffdf8a1d89ea9464a5a08e4125e731fe6f89e2fabf3640f6ff6b2f5b948e09c8818757aba3764a40f601ed060820b00390dbf0f140fc7d912b4e8b574830c007c1d007c2d0f11259889a3311e1f0be43e20ac6ffffd6d980d84f4076030410d8010716b32a63eb50fc5a65fdff6a613a913da3acff874cf4ff6fe763055bba5745162c069387396062a8ecff4403bedfb0a80708204814403b13b87a42e92682587b42df16b8ffbf5b9d80d7611772d2507b5548bd22100608208403becd804bfe7ccef03fc1800fab63aef51a6275ccb359a6ffebdc2430d4b7c572e3ea3d81ca63068000420981ff5f3b810e998fb3bb756e33cbffae242e78c71544b7c670ffdf3993edffe7fb8ca475dda021001040886c883275b100d22ffcda4e5ebf10a96ff8f7356af7ee3730747f3d4138002080901d508dd5a00f9a40874c407456c118589b7ea9fcffff53d6ff7f1fa2ffff7da3ffffef4b56b8e13f1f21fa8fdfee02fb8eb719fe7fb9c1f0ffd35578df113e72061040a82da2770cbc845c4e84e160fcee3cb0737a06d8313d09ec981e67f8fff208b0637a08b34504104058db8440c3f3c831fcd90160e7753fa403fb7837b003bb03d879dd06eebcfec2660f080304105641640c34bc9380e1ffef6d82f48cefac07f68ed7007bc7ab80cdb3e588192a7c182080082aa0350608a001770040000db803000268c01d00104003ee8081c6000134e00e18680c104003ee8081c6000134e00e18680c1040343114341202c4ad407c11cb2809b6611c62f037209e0bc46ed4742b400091efc95c862c20fe8b6b14061d4ff5e2854c3342f9d3bd79c02339b0a947183e9dc9f87f7d243bca8a87cdd16c60b5f03130ecf82a108b90ea0f8000225ae1f52686b3a0111ff8b4653d64ea123cfa530d1d01aa848c0281a631418e3d9dcf0819ba820e5f6d4b64fb3fd98b0f65cc0d797af34406e3ff3a5b218847a101d5e3caff7f6f220b51818cb66cec3e31fe020820bc920f66311e078f36cd808e384d458c3ac1a743a1a34ff069d13ee8d4680f747a14341ad501193edb9cc10e1e4283cf014387d2401e5e16c3853360e78772c3031634c4069f23868d0b42c70671052c109be1f223400061157cbe99e92c68a40b3c57bc01325f0c1ef15a8334ea051bf9828e7ec1e7909742e7911741e7921740e693c1c37773204378e079652a062c78621d4bc0a2a7586c7e0508200c01f0281b74a40d3eda76143ae276083aea760032f2069e8fde039993868dc021cf4dc347e3b642870f37438610073260d1fd0b1040281cf0c0d90356c4e8e0cf86ff5fefb28327ecc1237e37a0a37ed720237fe0c9fbcb90097cf008e005c828207822ff0cd2643e6cf87270046c13b29f010208cef8f198251a3470f7fdb50ece914910fefaa3fcff8f2702381704601d8d840d9f425740c08751072660b72307004000c119bf9eb15c431ee504e3e72cffbf7d89c41b2028e3c9bf5bff7fffe8fdffe77311ec43b7b8867109acb2a072c0c62107004000a16481d987181fe11b1ac63a44fc92fdffcfb7caff7f7cb4fffffd6bf8ff6fdf12fe7ffb91f5ffebcfe2ff5f7fd7fefff2a71d4837fdfffaab02289e0f944ffdffed6bf4ff6f9f83fe7f7f6ff9ffe73376dc43ca6843cb3897a91019b0e780a9c9671e8338b29f01028821660503136c481a7d681a79f4377935b04edec3f87fd319a6ff3780a1fcfb35f3ff3faf595030ae652c84c6dcb1062cb671f8e7d8c7e4b18dcfdf00c6fe92438cff231633fc0f5800c1fe08bc0a16000001c490b08a81297e25c37f20b6020900698bffeb6cef8286c66118347e5f3a8bfb7f5f8f18cafa1ef4753ed802704da5cab7b485ac7f418108c39792cd2ec146aa9147ac9171f462c65fd886d8d187dad187dc435087df5170101a06f917208050abc175b62ef0e1d6743794217610fee1e3feff2710fff27607b2ddc0ecbfde9e286a1e47dbfc5f5ba582a11786ffbabbffffe789d0d30b0c5410c6a5fe5486d1ffb2e93cff4fa71be1547339c90c2300b16158e02d5929e303f22f30e0940002082500feadb5798f6b495057bb24d10baa70e17a6fd4a985c84afcd30bc4e2d2708879db7898fe1fb3d482bb79db6a258cacfc699d057c6e04840102082500e274f97fe29a2180e1bd955affdf4ff305f6ceb2fe4f2931fb5f5e21fb3fa18613ec90946adeff151572ff9715ba12bdd80b844f8506feaf4ce026b84aadcd89e1ff463ed44004e1a38eb670b32ee564e15cd7f565a9e5ffee40e92fc87e060820d42cf08ee1ffe7078c7803001bde55a609b7e4f31ccfff8f9aa3c10e212510a8812fe765feffbbdafeffb976fdff154e62286e5cdac4011beebf8bec67800042f67c3c64f59c20cadcc09bebc024e6c94372a0a41a0bfe3fd1a88b333670e15fcb9cffbfe809012ff5c3e7d935e1beffebec34b0dadd12c50d8e48423333200c1040c801a00557f09e193a3bd3066433e19d85b90f6c7d2daae780af5cc7850bac45b07af84ab7c1ffc9e172e0a9497cfa930cf9fe7702b3c9867ef6ff4fcfe37713b15353200c1040e859e0098662d012fd6fcb9176db1401c570ac851c2cf82d2a06af827c8d580989ec678000420d0048207813b4e093073030e6a14d9981f09cffff3f47fdffff4189260ec786c1b3664fa133678f9166cfa03368f0259e7721cb3cd1fd0b10409801800808abc1e470d04c1d688d2a68b60eb44e153463075fab8a3473075bb70a9ec13b0b99c503af5f3dc92087cd9f0001843b009030d0c16903e870c8e2dbe39005b8a0a948f022dc43903d2aa05943f09adafd909943d8dadac7bb19d612e3378000222a00d031d0e14a40079fa491c3c10b8341d39fe0c5c1db100b846153a1b085c2d029d1a7b7d732c491e30f10060820b23411c24087cb001d9e0374f846a0a3efe2703878a533783e7735644ef7e60af0bceecfebcb182e5e5fcab0fcda1286b4ab8bb0275d6a618000a299c143050304d0803b60a03140000db803061a0304d0803b60a03140000db803061a0304d0803b60a03140000db803061a0304d0803b60a03140000db803061a0304d0803b60a03140000db803061a0304d0803b60a0314000d1ddc20bd90cc2406c04c47e409c05c435409c03c47117b28062590c5a40cc462ff7000410f53d98c3c008c4a07d6f6b81f80b95164881f069202e026292d701e1c3000144b987731904817832d063fff02c60fadfe220085e0ab3339e152e06626f8a6683f3cf653180cf6600a903af0b221c58ed404c70c3303e0c1040e47a5a00888f11bb700974de4d95b5f0ffb591908d8667b318e0abbe0ea530a304d48e385684e7a118a417a4b6cb851fdf2ab12272fc0210402429be5cca1082b140097971127481d2d638b6ffdda0d55d49b857771d486646e12f0be5047bb41569291df2aaaffd49ccff0f2633e30b00189e4b8a9f0002882845d79b18ac30161dd521ade6425b22d7e62c08c6e881352780e7fff12c2694c0423e0164962f64ede072606080f8bb1258fe4ff7e1212a95a12d9103615f62fc0610400415dcee677c0d5f9dd58d65851696d55957ea31036b76300f7803307a605d2a65f8bf22921373e95b21625becce445642c7a5e0c2a709f90f2080704a3c98c52882b2e26a1a8ee56cc84bd9fa9196b2a105d6e91a26ac81b5331bb20a1457ca3a9ac78c58235886639d20966c881458eff00500400061157cb29c49156379da62b4256af39096a8a12f4f2321b0aeb433fd3f5dc74c54ca22251ba205d6435c0100104018022fb630f1a22c395b8fb4e46c35966567d8d6f2d128b048c98658020b6b2d0110401802f0357ae8ebf370add11b4281852d0000020885f3fe24f3649c6bee0ea3adbbdb87b4a071e80456217a000004100a07be960e7981e279a4058aa7b12c521c5a81f50f3d00000208cef8fe80391eb4e810b4d811be2af4950d62d1e14d1c0b0f875860a107004000c1193f1eb2bc453ed2006579ece738940587b0159ac8ab345156680ee2c0420f00800042040074a525a1e5b0dfde3990bea4751005167a000004109c81bc34f5dbb734a2d6067ffb12fbffc7535edc4b54c959ff4be3c0420f0080008233b02d3bfdf15af5ff97bfbd442f96069d73f1e38d01e135bd031858e801001040880020b446f725f7ffef5f028101d247748040569037fffffec9e7ff8f57f278173fd329b0aea007004000c119e14b1992de3e61fe4d684133c6a2e6d7a2ff7fbc37fcfffdb31f6415f8f754f0aa70d0ea70d02a71f06af15fb5e0d5e3dfbe674156937f0907068a2f3050e4c85a294e6e60f9ce6778831e00000104262297313c852d64062d5caedc02ecbc5c67a278f537ce15e0d8567fe358014e30808808ac0fc014003a3c05748a0b1087230700400031442f67388a6fa93c6c8d2d2850e61f61fc7ff42ab0347d42fda5f2d40cac8f8f98ffef3fcbf4bf6e3323b6a5f228e5004000c18fb3413beaf22cb1cbe1610104db5350b199e17ff71e46706081f6161c0306d86d6001f50ce8a8b74f99c167f6fcc471140eaec0faf48cf9ff63601ebe0c2cd48e5c62fabf0d68eea2c38cff5bb733fecf588d7d293cfad13968f8152c00000208b43fe03010ff017180b4d3ff75b6912fd69afc47df2f702fd61a3d90080650d12caebf7d5de23f90f70ae4cfe2fcded92dfa0adf5e818ac97c37712d752776bf00a1bd02c0400802f9192080c0a190be8e811b1c22eb6cbd614b5857d429fe6f9f24f47f47a926cadafce3d906ff8be772c2032777011b587c7d852a4a00b54d1042d137a55df2ff8b707bc43e812e71941494b890194c83d6fe83e4af269a636ca8f8e6eff63f7336073c50f26770511440203f0304106af710791d2f968d09df7cddc09b25401b25401b26fe787bfcffe785ba61624bb9faffdbf196383737fc727581b3ef00d565cf67fbff21d819abda7fde9efffb3ac5ff77f68ae0340f84e3163311bd59028461fe05068c2a4000217b9e0525002a800ead04e21297ff59d59c54d9d800c3134d716f94882d61fc9f95c9fcbf3a8efd7f6f38dfff8541e2ff77f9a9fcbfee63fcff8f9707f640f5758767b1892b45fe7f5c6bfe7ffd2a79ac5b724eaf066fa80880f91b208010bdc11556c940891fd856743f5d664cb1a7a32a18500ea39aa94d9ddd2211d50cff172841ccdc29c2f5fffb0267b8bbd10beaaa153c30b94498bf0102081e0071bafcc5b896aaf705cb80351eaf08fd1f5bc34692033bec317778c0f06a51f203a1d915d52cd011c3a07d04bf973ba244de8455c2e032e9f01a3564711698bf010208390024096d8228b517fdff7db9f5ff3bd509ffb7e587fc6f2a53ff9f5d250c76500c3060f22a45c1f7711cc88d217ea74746c6ffc5011aff232bf17bb83892e1ff2c2dcc40dc21ccfdff626606c6c961d8f0c11aedffc90602cac8e51e4000a11482a51e3c7f88dd1172b45ee7ffafe54eff1f3492e05922f1b9b8d8ff47edadffefd350fabf4388ebff362ec86506dbb899feef14e3fb7fd048f7ff99c808ac7ad13d0ddab5926c280076f39a2ef679e84d6180004270de31248096bec30e0c2316377848fcffb4c4126cd99f950eff5f4d08fa7fb33485eefb854018b4d5065896fd5f92aa84e2c6783dbeff3f9f31c096f7a34ca60204107200fc83adfffffe8481e016186c786ebcc2ff3f6b6c88da1bf47b852378f7d9e3d648120ecec38ef766f8fc4fd4c772169e05dfff87a730b6d74c400e008000420e00941b2c6018fd6a0c62f1940839706c90ba69eae34cefff0f1a62f0ee3a3b9a9af87faa97f5ff5403110c7b13f4f9feef99c7866f83c773e400000820d40000e1cfa9ffff7f72c2aa19746e7a63383749015162274a52008052d0d936bdff33a2e5ffa7180910343fdd9cefffba1e76f0418444ee70f9811c000001841900e04088879ef56e8dd7b05f2f18fe1f58c4fabf398a70a09cefd0c7f02ce8f47bd021f5c5c04022580379f0fc9f5bc9f9ffe45a5660ef9368cf62c3479003002080900360398ac20f1248f7b0c4926cd1a3d34ce0a408ba39aaca0fb2ef30db5ce8ffba5cd5ff490690984d35e303cb4dcce4029ff07f620d2be55be208e33ee400000820e40060c5aae14b2ed2aeb025c05461466b075286df62e27fb00d5c2fc1fb9e9490030020801890394003f6e234f8733ada16b9e5902d72f4dc4348d873d877a83d43dc9680de0e000820067401a0457ff03ae2832430559462d93308da54590d4c21c092ff3dced34c69ea396c3bd8c0bbd8eec14fe0c4d849061040980180d6262088df033b189fdc809eaf431c888a152ffbffff6b0f505d2d24003fe782ef7bfbf731fdffbff701ffffbdb5fafff7b52ad0c3fce47a0eb23def26747bde3586ff1f51b7e87dc3e6578000c21e009040783588628e90e7107b0f2f42b6f0bd3b87b48def14c3315cfe040820dc0100c26f197a8780e7e0e7bb82f7271e65f8ffe208fc86937f408c77db1d4000e10f80ffe05da34c40fc76107a0e7503266cf3e55ef0e6cb3f8f77313810f21b080304104105300cf4180f10df1d049efb0ff41c6277296c67e916f0ced29df736317011eb2710060820a2152263a0e7ca801efb4767cf21769f22ef405dc7b0f8f65a060172fc01c20001449626640cf49c1fd0734769e439f84542e0adb52b19bede58c130f9c672063d4add0dc300014415439031d0738c40cfb9003d3719e8b9e340cf7d26d273ff6f40f60e3fbdbe94612f10b75f5bcce0747511658ba109618000a299c143050304d0803b60a03140000db803061a0304d0803b60a03140000db803061a0304d0803b60a03140000db803061a0304d0803b60a03140000db80346f1c06280001a70078ce281c5000134e00e18c5038b010268c01d308a07160304d0803b60140f2c0608a00177c0281e580c104003ee80513cb018208006dc01a378603140000db80328c1e0a37ab219a480d80e889380b80188a703f146e8713b8f81f82799c7f560c3ff80f82310df05e22340bc18889b2f6431a400b10b102b0331f340870b2918208006dc01382256147a06531f10ef07e2f7441c1a83824f65308231a9fab09df1742899f9fff210ceff33bc79fe6f011d77445ec2b90a4d9c51402c33d0610cc300013470919c0bccb9908bfa7603f12f12cf84c17a60cec62876f8b14c300c3a90e75c36aa3ad0257e8df6427035a07bed76219d6b05c35b62d8fed7db0aa198d7e624f07f1fdaf14e300c3add68ae3ff7ff6e177eb0bd27d2b12440ec09e41910cf00622b7ac7034000d1dc824b850c8c406c77b1806121107f45be030f7ec64d1e99918fe7bc2e5ce29d2e02e088041d5535db8fe7ffc94c469c090b74d3237ae2c187d74670fcafb7434e30c29063b2c82b857e00f14c2056a465fc000410d50dbc56cbc07cb586210a88afc02e49849feb033bdb077abe0fca193fd0737e500e4642bb48b10129d7f6baf3ffbf908f7460121513d6e15426b48814019f6c47d26d97c41df1450afe790172772b49eb9e08618000a28a21373b18c48178fecd76867f28973e42cf344239d708fd7c23a4338e50ce3a82ddb8093d200a84e704f3fc5f10c6fdff5205fe84b5310151158012cdc562fc090b840f6730c34fe14239890b884f6631fe3f93c38891b040250aac2aa9b611fe7f3c9d89aa2519013c138839288d3b8000225be3bd298cec772733f603f13fe4739cd06f084539d70976b613f4c650f8194f48e73cc1cf7a821d8e053d208b9484b5329e139c6337a6701095b08e1731c313ccd61436924aac33f98cff9bc0e7630aff3f91cb8491b09013157ac2a25289f506888dc98d4780002259c3a3458cf28f16325e871fc904bbed147a8e15fc2c2ba49b4fe1e75ac1ceb6829e6f8572c615f49c2b94b3aee894b0f615b2421a83c062ff741513d54a2cf0b17648f70553ab2ac491b03e0231c9090120808856f8740d933810df851d4785722415faf5b848e779a19ceb053bdb0b7abe17ca195fd073be062a618112d1409558544e587b8098e8b1088000224ad1ab1d4c8b908fe24239920b762c17f4682e946b7fa1679ac1cf35839d6d06bd06187ec619d23967c32d610d5055f80198a0c488895b8000c22bf9ee2893d8bb234cef508e22831e47063f920c768531f46832f8596e48f744a39ceb8674be1bfa196fa3098baa09eb0710133c741c2080704a7c3ccfacfcf11cf34fd0316cf0a3d8908e634339920dfdce6ba4f3ec50ceb5839d6d073ddf6e3461d13c615d229400000208abe0d73bcc5c5f6f337f801d43f7f59ee0ff2f1f62ff7ff95a02a4e3ff7f79a40b3e960e7c241dec583ae8d1742897a443cff4839feb073bdb0f7a69fa68c2a24bc2c27b9c3840006115fcf190652dfcd8bd57faf88f06fcd3fbffdb7b9fffdf1ff1a29e658874f93bfc5c43d8d986b084857cc621f49c43f85987a3098b5a096b26be040010409891ff88c510f9d8c16fef1c493a2b12725e64dbff6f1ffcffff782a41d2198f28673dc2f07de6d1844559c2da832f0100041086c0cf272c7528472f3e17f8fff56705c9890013cf069a53094c18beff7fbc90c77dce258907838e262c82096b1dbe040010409809e029cb51bce773be10f9ffed7320f85054ca1305b4c400258e5ff5ffbf7d89fafffdadd9ff1fcf04b11f8e4aca81a9a3090b96b09af025008000c210f8f59c650bcef34a719d5ffa9cf5ffcf376affbf7ff607e6f2726084cea45ae24049287f2700cd2f052694c8ffdfdfdbfefff15a11982839701e1a4bd489bbc33f6129e34b0000018421f0fb250b23102fc775c02dc1c36e71e157c2ff7fbc37022612afffdfbec5838f53fef2a703985866d124b140124c2f38417efb9af0fffb47afff3fde9942ceb17ec649f649c44329617db8c79c4aa81b081040289c88650c3240bc1d76d826e8bcc9b69d8cff0f5d62fafff1190907fe92706232c5098bd8d20ac789cbb83049c75653e1886b6a26ac8bc02a227411c37f9f79608cf7a67980000213c088d600e2d7d88ece463e1d1a19830e492edec8f07fe23ec6ffeb4e3181cf1b7ff91874ac36e6d1da241faf3d9ab088c6df9e30ffbf086c1f4cdecdf83f7209e47870bff918f815108b624b000001043a3edc19fd346c42a76363c3b8120f7a22029997bb9ee17fe37686ff530e30fe5f7e82f1ffae0b4cffcfde60fa7f1f58d7bd821e31fe1b47421ace09eb3330329f00ebf6abc086e0d18b4cffd71f67fa3f6d2fe3ffeacd8cff9356103c121d8e6167c663c1a1e8090020804047a6fb231f950ec46f813803885d81f82ff4f8f475b52bb9398b577274a09f378feb4875521311b109081f8e208061e74783ec03b9397115c3ffd4d50cff33d630fccf5acbf03f0f98308b80a55ae9260806b1f38162b9eb20f2e9ab21ea5380fa9280e1120ff21bc87dcb709f5a4ee8147342c7be63c3e847c1937876fe0ae40400104060226115032f102f05e2ad18c5c43adb5ae4732eab1772ff8f07791e0d272e67fcbfa441e1ff377f57f081de7f7c3cfe6f2e57ff9fbd88157e063f3e5c399df7ffd974e3ff25b3b8884a403553f8ff7f0a449cc4fe25c0f5ff8c1669944413038c98e5354ae0c3c5616eeaec16253901452d65fcbfad48137eac7dec22c2a7b5472d66fc9fb88005ef29eed830a104448d44044c24cf81581014bf000184bb85b8ce1658b9db5ec438d935d91def51f684f04f3ff7ff0f636cfe9f0346f6c17cddfffb0ab5b11e9fff22c2fe7f7f8f1838712127a0196dd2f0082517838ee55f52a700cebdc889aabd4f049e98cea599fcbf9e60f1ffafb72756335e873afcaf99cc8f35112d009afd1fe95a0150c23b9366fc7f65a5caffce2eb1ff0dfd42ff97562bfdffe1e706c4eeff771468fd8f21225151988896027123106f0526925740fc0f88f50002085f02d88cf568df12179c01fbcedfe97f5f8ee4ff986a66b20e840f079db25fc3f43f1aa81f64467c15ebff04204eaa64fb9f52c9fe3fad82fd7f7a05c7ff8c72ceff59409c53c6f53faf8cfb7f7e29f7ffe212deff15c5fcffeb0a85fe37e78bfcefca15ffdf9624f0bf2698f97fad2fc3ff062f86ffad1e4cffbbdc59ff4f7063ff3fdd85fbff7c17feffcb9d85ffaf7311ffbfcd45e6ff5e17c5ffc75dd5ff5f76d5fdffd0d5f4ff0737fbff7fdcdd284a6ca4e075e56a78ab3050022b58cef13f92c88402c3e5cbb94171f70888f9d1e31920807045be31a9075e1f9dabf13fa28e7a774c500387d700ebf208608e54461cb6bf5009189095d4b9ba8118fbe34a18ff676431fd2f4e64f95f1fc9febf3788fbff6c1f81ffabdcc4feef7294fd7fda4ef5ff1d6bddff6facccfe7fb2b3f97f2441eb7f2cb0aece59c1f67fdd6ab9ff1fd6996384f5977516ff139631115595f5af1441d65b8e1ed70001843501c4e9f2c7c04f22d7e3ff9f6a2c083af2f94f91adc8d74a27b1f70d1e12af3bfca49f4f08957d34235afedeea2c9507afe69a7f85597471b2cdffda0a45708ea6678467a632fcefb665f8bf5e00f79515303c4f1558a594d22721e0746f1ad0bd360cffd70a61ba0f745102e802858b5959ff7f2c76c19ae9d6af96c7a8c6600deab69502ffbfadb344d763821ed70001842b018492735c3ef2850aa75af4feff5b63f3ffed64ffffe78b13fe2f2c72fa5f5faef23fae8683a2408b07ea2f2f96fc3f2959edff8e10fbff27fd7cfe9f0af0fd7f2ad0ffffe990a0ffa7c342803814cc3e15e807963fe9e3f9ff84a7dbff63cef6ff0f5b9afe3fa0aff57f9f9ac2ffdd32a2e0db29a61930fe4fcda64d6288a80216dbb10cff7bad18feaf12c79f28778af183dd79311bf5da807fabed0996c03fd659fdbfbe56efff9f7558ef6eb80ac466d8e21a208070b601801139879244808cd38025c8c264a5ff8fa79b0253b3ebff97bd21ff6f5726fdbf847627c2f9ecccffe7b23328ba48822ad79764a5ffdf98e8f27f4aacf2fff62891ff35519cff0be398c03936059850729219c0f7b9540530fc6f7267f8df69076c9cea30fc5f2d42b8e481dffb22c8f17fbf8edaff93febee05c8ecb2d570bd289ae86bf2db7fa7fa44e077ce10ff470febf40ec8f6f24102080b04b80ae1879c7b0e3e515a6ffa59e3c544904e818748bc0f20c95ffcf6799215231b0a80395188f9aa3fe5f2f4e1df0844016ceccf87f212d15e5521f4af09daa048c887e3bdf1c7c19d0a470b9ff298698f76b14bbf1febf73841976b039c6e00f320608205c09e034fae9e80797b0fecfb226fd4a195230e80a98fe10d9ff279b75ffff5a658d3595ff5b63ffffeb7cf7ffefa6fafd7fd51ff4ff795718f80a9afbf5b1ffefd6c4836f77ba5d99f8ff5645d2ff9be5c9ff6f94a6fcbf5694f6ff4a7e06de1b59060b06b9f1614bc4ff9b3de6ff37e4a9fd6ff591c27a350e328ed7e5fb3f299bebffe33338afddf0c095000002085be47b103a2affc75386ff8797b2fe6f8be5065b4ecb4401c299a642ff67c628fc3fd7ae8f3361500bff5ceafcffed144829044a38b48cecf35999ff3744fafd6f72d0f99fa8274854582419f2fdefcfe0025f2203babc86c8fb1d8ee34a000001842d0114a268fe1402c4c052e43d3b418b40c5ceba5ef6ffad31dc6087d23a6180ae060355233f57d2365160e0b5b6e0ebd3becc73fbffb8d7e3ffc55af7ff87f2ddfe6f4f75fdbf3adaf9ff2c3febffbd6e26ffdb9cf4fed7d969fe2fb752fe9f692c41b4bf4061d71cc9fd7f5507fbff4b3b58fe7f7f4a7444e3c23f7025008000c29600dc310d60445ca2f3b5fbffff8ffa243b02742b19e8fea96dd3d8fecf2886dc28946040bd44120fc4f31314c10d214a22f7cb52cbff577a0cfe6f2d52ff3f3552ee7f95b338fcb6234a719211dfffea009effd30b39ff6f9eccfeffe23696ff6f6e30d2f6be1d08be8f2b01000410b604c009c45f711af6d11c9810e6235d093305584278034b084eaa39f8cb43c6ff778f32ff3fb68af5ff867ef6ff334b38c13922c796f836c8f42879f8dd7ef0e21d585280227775b6eaff464f4982752b3a4e0426d80227deff75c13cff7b92b9c0f779816e983bba92f5ffcd83ccffdfdd64fcffef2dcd23931cbc125702000820ac82400d3e040d7dcffaffffe718c8955ae8b74a7dad07260a67f22e51a202065d98f8fc12d3ffebfb98ff1f5fcd0abe970c549482aa2890f8e7fb8c945ec236f8f05bbc38115702000820ec09009208224972c0076548c4e3bc3c6a2e30c164426ee57bcf37f001365830fe88835cd7f4067a6513f4da26425737a1e187f8ba810001843b014012813a107f22cb63ef392077517e6dc29328607811e4eab5cf91c00462014c4c62031f31031f7188bbb89e23aeac02e327d0ababa0d757a15c61857a95d55f20d6c017c70001843f0120124216d502ee3db0affa410d9838822091fe6d1611090419cf0326aa76a0de7c48820155351f8d216682eef603573b781a564323e250f0f7fb88fbc8c0f80ef4ea2ee8f55db02bbcbedc805ce305bdcaebe5a7ab84cf17020820e2120022214400f177bae63450f7135422bc5705d226c0c806962a1fbd8011ef0749449fc280381a981880d5dc27d00db840fa2390ff31ecffbf0f01ffffbdf7faffef9d0b10dbfdfffbd61c888dfeff7da30db9abf0b5f4ffbf2fb9065bc4fdff7415710f1bca5d6c6877b22163d01566ef2f80af317b0bc4eec4c62940009196001009411188af0ea3a27230441cea1d746721f7d081efa23b8db88f0e8c4f40efa583de4d07c4df5e1e65687c79848195d4b8040820f212001206469c0130e24e8c461c591107be58f0e511c4e582e03bf80e21eee183dfc507bd8f0f7c27df7ef09d7c0781d894d2f80308208a13003206469c1830d2da81f8dd68c4e18c38140cba58f1c91ee8e58ac8172c422f59045fb4b89de1cdc36d0c9d0fb631c85333be40182080a86a183a06461c3330e2c28011776c84461ce4964ce84d99c8b765c230f86249e8e59240fcf3ee4686ad7737302401b1102de30686010288e61660c3c0489307e22260a41d05e2bfc320e2506e0685df10ba1e714b2818af01df16fa1a8857df5ac59005c478bb68f4c0000134a09663c3c088e300469c1d30b22a807839109f07e22f8320e2e057bdc2ae7bbdb9027ee5eb7720be7e7d19c3d6eb4b197a81380688b5ae2d19fc27870304d0803b805a1818713cc0885302469c0530d2bc813804187131c0484b01e21c60a41501710530e21a8011d7008cb80a60a41500710630e21280111605c42140ec0d8c38ab6b8b19d4ae2e62100262c681f61b2d3140000db80346f1c06280001a70078ce281c5000134e00e18c5038b010268c01d308a07160304d0803b60140f2c0608a00177c0281e580c104003ee80513cb018208006dc01a378603140800100e56ae811d8878dd00000000049454e44ae426082,'image/png',128,128,0x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000200049444154789ced9d79905d7975df3fe777efdb7b7d6a49ad5d2369341acd824603c3300cab09b131609809619862092e5394c1aec445b99cc4a15c64a98490c421c676118243410a9b148c13e2502e2fe0b0d80c0c83469a55bb5a3d6a2dddad5edefeeefd9dfc71ef7defdefbee6bf5b0da557daa5edfedb79c73bebfb3fc7e7769d8a00ddaa00ddaa00ddaa00ddaa00ddaa00ddaa00ddaa00ddaa00dfabb41f2d366e085d2b10fe02a8c8850068a405195b208c5b088a7d011c5033ca0a15013a8019d23bf87fd69f1be1efa5b09c8b10f525665a708fb80edc014b00d98566502288be02ae4457111f228ae022258c053c50ac13ed052e81080b224c225850ba25c02ce0333aa2cddf5fb3f0d6993f41307e4d8af00900746803230061c06ee000ea31c2200210f18c068b88d7e02e810e635dc4aec84afd0f50511c83b6a43d02c811559054fe012f0a4c2f7119e44392dc212b082d2fa4959d64f049063bf8a012650762aec950080bb543928c241a01c295809fe48c859c717ae375c565b0611182ff94c963c8c09cb6b50a6ed090547c9bbda134c1556db8699eb0516ea2e7947b96d5b83d1a21d105e637d86b4a8ca31111e074ea09c44781665e9c8effdf874f56305e4d8af5244b91d781dc25dc03e55f612b8a00146347e4283a17b76bec8b3574bd43ba6a7d0fd532d5c036d4fb8783dcf95d53c6d4fd836d661ff549b5c084ab3231cbf54e6fc621155c83bcabd7b57d93edeed77a3b0dc7498afbbb806764db6714c920f848604aeed38f03d85af0b3c7ee477f17eb41afb110372fcd74015577da68187809f07f6025b08dc136828636278a6ce85c858e0d2529e63cf9751156e9d6eb07bb243ce51da5de1f1d90a732b793c5fd83cdae5ae1d75c64b3e468226ac85d596c3b5ba8baa3059f29828fbe49c3ef417afe7397ea94cab1b58e0cd9b9bdcbead19f09218213d37d900e681c7804f237c932036d9239ff8e175f82301e4f8af6344d9aeca8bd5f2569407809148208d0ba683f57d0bf58ec1b742ce514a391b8c524d168f83d8ea0a8fcd8cd0ea1ab68c76b975ba41cee9f7556b1b5a5d836fa1e02a23059b0022a2ae0f67e68b3cbf9ca7ed19aae52e77ed6c04ae4f63fdc6aa2a80800446fcac289f54f81311668f7c82ce0bd55f9c7e28409efa17006c57e5b5f8bc432daf04467a00c4b7718152005d5d7579e2628566c7b0bbdae6f0b6665f791900466df836d048ce4982e55b78e6728993574b747ca192b7dcb2a5c9bea9168e19b4442500a6d17128e72c39577b2ef4460a0a639e95c052be00fc2f552eddf5bb37a83884dc1fac1a3cf32fc9abf2b3c0fbc4722f2e53bd11ad2440d061c761d996efb0d47028e494dd536df285f47084a5bac395951c23059f6de35dc480eb90042cdc37027baa6dbabe7061b140bd63985dcab36dbcc368d1d2f5035758ef18764d76182958f22ee45dbf6f9502326430a44904a3f04a813b819f13e1b781afbe0075f6db7aa1154efd6771d5d3bdeaf16194b7a83282128441cd764f0900889509cf5f5b7299b99667ff748bf1924dd657b8b6e2f2ed5323343b86db773638b4bd950944bd6d500b9542d08667c1f3858e17b8c282a388805a3875b5c8937365468b3eaf3cb042214c047ad99ef6130d913ecf12c62749bbd37e5d0b2c2a7c4c84ff86b274e413eb4f99d70dc8d94f08aa4ca8f266b5fc06cae12c856bc479fc5cc44e78ae6745e1b6276c742a05d8b9cb79befb6c85e96a97a3fb1b548a76a08c5a787ab6c4ec7c9e7b0ed49828fb03fdc4b74b0d8747cf0431e8b5b72e331282981e3c3d1a34da80df10858cb4b9a1ca2322fc5be0d923bfb33e50d6edb224c784281f56cbc328d3116703ca45066247b28c0e2a73ad63a03ae973d38e3687f7b41829c51447bf5cb72b343dc3d5951c97567254abfe201f31654fb83e877636b9743d4fa5627bb3cd84cbcde80792ae2c6e39f1721aac263ca43085f241e06c965ed3b42e0b99f9ac8ca9e56328ef56a5188f11e9989116284bc169007a97e2a0c5ae5b2b74ba4221678782d6e90a8f3d53e6cc6c81d71c5d65c7e66e663ff13abe85765728e5747d836308ffbda633fa00ac2a5f1178df8b3ece656e406b02f2fc170d6ad987e53fa2bc515177e8e8cf62d6c6cead21505af01f240ea9c2cc5c0e51d8b1b58b111d481e328f53fda4c1bba135a7e41822af077c510cbf2686cbb7ff3b86d29a2e4bf24c8be537d5f2b328aea80cb828893308c91892167c9d80640aba0ed0f6eee9c6942903000c759f59a0a5781e2e6f4a96f4b9e0d845798308df47f804c1e43293d604c4c9f141b53c04a19bca6060f058b2194c2b2126647a9bb6ba3547f75a80a5fa1968231defd2f2918a777670a0a407e8a0bcbd3263c0fb11fe0c38c610ca74598bdf32c6b679ad2a5f52656c4d77c320536985aec765c58f358e5896e0c3f8c8ea67ad73f1e388f7f5584042de64bccbb2f0581b56e0f3262fefb9f9439a9975655b88c36e53e05f0163ebf6ff43cc356b140d58c88022646de566f1417a3b68894346ee0f09aa0cf6334c5ec5a03c20868f00a7c9a00140961f77f26af5018c1c56229fa9a09214324b21b03ec0d216911620dd7eba5e5a2919ca5524e9efd36560d012d3d7536d66f7932a93e66d50e622c243c0bf2683065cd6ca93e620ca275179353064521702140aa4bdfd609b50684ac80c06072de4468243b695a5fabd51c0be617291b6b2b89319d60643fa499efb0b8437edfd256d91a2848534ce3a462df7a0dc07a0e53bd1b1fb838bf567a0761cbc15b06136a382469a504230a4b79fb0aab514b19690c41495654543405b3760c3944928df8d064e465699d866f7b35de100f024294a002286b2187e1ec8e38ce16d7d2738e3c1b240e50e98b81f691c431acf42fb3c68b7cf742cbbd250986027baaeb163ed5f0f2d2b0e9aa0c96c0d45545242c6143104c87507f5ac3a641c0f032dce47066819de624c6037e80d00112902f703d8fc2e3023e179400d1476a0f96de8c88b31ad9301389db3e0379302c498ef0b203181e2a9718640712b8b84ce54a4f42d316d356977936575194abb91350f75adb17e927c645a485e8334788092415d3888b23dd877328a0b8a41725bb1ee6628df85780b48f338a6f11df09711f52095d1a503d5ba3231488118b3aa8405c562998dce690c68ed01dfeb3b043b5ad5ed33165b308cf3ba5e50d6035ab07535ba839aa23420b723c152bad86ba04d5446024625591071c019c59a11a4b0173bf6f791f67398e609a4330bfe75b0aba0fec0fd72b3c6828da6875cdaaac2fefb8a8a248e5c210cc4ae78b261fbab88aab115c5843223cb8b95a1dfee409b6bbab40c0b09cead631e22ec0fbb42fc799c952fa3c5dbb0f9fd20952096c434175732a68096eec42fdd01de22d2bd88746691eef3d0bd80f84b68643983faeeb51bdc6f90fe3d09136e253928fa296b3c7649ef8c6a0090846513d6105e832c65a5b2c7702b3177aaaae171b09f0645122e57c898e87a4af6adde3420fb08da40d5c334be01ade398dc0e6ce928b678044c25604824f5184d700e04dc4da8bb092dde01b686f88b88b788742f209db3686706b19d0130e2c711087d9088580bb6929a90496079bd7a1ac11a033bae142297961cf53d2b236d65f400eb83a80990fb40a62db56f71e1b98eaa2cdd10101126882921607409e92c613acf41edcbd8dca1009cdc4e9012482118c60950a2061dd48c811943737ba1f422501fb485787398ee2cd29901ef32d83aa21dd00e681759230e45ac4540c5f5dc0371c84276544601e9997cbfaca47b4a75a089f3113831b797c8226300c5dc20c88ab53a9bc55f1a90e30aaf8b8fd83e7b1ed8659cd6a338ad4751772b3677139adb8dba5b50a71a289f322a26e682e202ba202e6801cd8fe3150e8523d947ec32785711ef32e25d43fc25f05710bb0ab601da40d44f2836a9ca14c5077eff30259724da8ab7d7ab27c9e38147b634a6a5c8c5c5000332ac4866c566dfb04ac8d3b9ec1e257872e2c03039938123e058cd18ea6c42cd389849d4dd1a02341e9c73c6e805c1f006751ca8c0b22277138da47600925d416c3db0207f19fceb88bf84f8d7837ddb407bd123c55e9ad7356489bbc3847234793d5e2d2b7beccb11df2618f17ccb8746f6f9ff258ba55e9b6fff3cec196162efa87cf4de6979f8e0b88ce49c30c0c747d28d8444502980e4811c2a2e9852e0b6cc2898f100402904d6223920071296953c81e11ac023987c76c374ba8984d6820d7ee22f8756756540c359312a11f7d623cf0de51de876a8d5aa2a6757b1ffe609ff8f563abcff4f7e915aba8c00bce3f3b8aa1c053e8af06a01b695e1be6dc2cde3c2444118cd41250746fae0640a9a9141c53b7b214a58cb2d2532be0c7ed6a5c8583b89befaee3fc9f71abcaedd87d2f4e0e925e533272d971b2c011f11e1f7fecf7b93d9963cfc87b8aabc1af8b7c051626e5281a2035bcbb07344d83502d5823059806a51982840def4c44830190fb0eb15a0e7365ee0a84cd7ef1d6b865b899559573febe4231dabc2fc8ad50e9c5c568e2f288f5e5516dabd7ac711de2ff0ed2fbfb7df9c0bec16e1c32847a34961bcb3b60f33ab30b3aa3802e59c5271612407a379980c01da1402349e17aa85c09a32c7766cf4a59f778aa7a76b5ac710dda4cb66c595b8c2d6aaaf033b6b772a80afca4a07ae3695e7ebcab915986b2897eab0d80e9eb2947ebddb810f122c30f65c97bcf38ff800cac7c9b837a2c1827387febb1afd6ba154d10b1b4e380f9070bfe81202059b4b42b51858d7681e4a0ee48c60c23a86a09e912866f5f7a32d29c082edd01c6b7d1e4be94deca28cd446196c741c3eae6ac37d1fe8faca6a1796dab0d852ae34e1621dae36948617bc8fe2d9e017b537843ac02f8bf0993f7e4f3073770576213d301a048fdc6f0776034f3ac837157db3859d096963e9a0afc12fae88d52e5c6bf62ba497445c030507ca6eb02db9817b2cba5074846274cd859c0940734d1f788360447b96950030065e5ad1d1391b6e7d858eaf74fcc01bb47de8f8d0b5c17ed3874657a97761a50bb52eb4fca0bd61a6979eb066656321e545f84d8227e98f078008df0216812af05f513e89f062e025bf20bb16ee369b6efa8c7fba7856630941d843ce37dc7e698a95629b339b979388a4a837890baffb3678f9afd14d5671adb06da56273beb1cf8fd74ccbb5bdc75411c87bc61eba5a5dba5669e6e7c6ea239165a52d2d7c783e004221e799168a6db9b66c6380d868200de13bb14213db4fcd27137a19e62be3ee3946bb815f7beb67f8f53ffe47cc3b47dec6258115818ac03fffec439c3af2364e7ed4bdbbfc1233f5a131c9bfe24ab73972416a68cca5543a2e6f78fa26de7c623f479edfc24ab9cdfc48136b024729a95fa5ebf28a333be8e47c6ac56e6f1447ab2d22e000775fdcca7bbe7b9bde777ebb1cbc563573e335964b1d4460b299e7c11337cb5b9e3c2087af56f3cf6d5934abf96e4ff17ee8263ab15fd742a599e7ed8f1fe2beb33b9c67a6164dcdf5fb93e6d8807180bc3558a389b5b338af1203237e1cff459848ba4cbcbdfecf08ec443877f8014ef4b07aff23e481ce271f007de4fe7b14f99cc041806b273d9ebab6c29572036b946dcb23dc35b385b16621ec40b0285747eb3cb97d9e67b62d305f69d2712d9eb1385678d5a95dfcccb37bf08df295dbcef0d7fb2eb15869e19bc08717bb0ef79edfc6bb1fbd8d9c757a93a9e5529befedba82a386a317b730d62e04d704ce5597f9c3bb9fe5b92dd7f14da0e1bc6778d353fbf9fe8eab34731e9bea251e7efc107b9682db0f67ab4b7ceade13cc8ed7f08cf60672a9ebf2e08903bcf4c236be7a608663dbafb1546ad3767d1c2becbd3e46c7b13c3f5ea3160e826134d4dad207c20a500bef8d9c07de351015f591579415fd2d41fe094130478fe7e0f10278d12a6c72a6ed6303eb89cc5fa0e57aac94da34f31e8e35ec581a216783376aba8e7266f375bebb678e0bd55572bee1f0dc14af39b98b4a3b172e1c86b3ef68de135b17925894bf38b1cad70ecef0bd5d57e91a9f97cc4cf3d0e387f08d52cb77a8364ae47d937037b313abfce5cd177976cb224ba536938d022fbdb08dbf77720fe58e0b02f39526b3e335e6cb4d4a9ecb1d7353382a7c6bef25fee4d6735cab347b6e2cf21cf119fa7a80aa88f3472db55ff0d0bd28fb81ff3d0088fdd2fdb78bc8a755f51ec2155d7dd6451e2d42577a60589473e565be559d6321d74225bccd1a0a1d2dce8b4acc8c83172982add0717dae575ae47d43b55e22e73be16bb7411d470dae1a7236f879cd36dd7a13e3434e0dae3ae4d4e0e794b96a1de308fbaf57996e8d50d13c2352a04201c7a46eb60934731e97471bd40a1d46db79b6ad54287a6e02fcf4f20e40c7f5f9b35b2ef0a53b4ed3727dd2541687b6faf83180b200298bc33b73377de765cee65fa83cf83797dff639f2403e01887de41588f216843f54d5628fa155815339e886c73e5cb035fe53ee04d7dc26bee8c04430cd4c7072402f19fbd2ffab915f0e41f515a76d195f849d67e0f49dd0ac04574d584a883230c160300a85b621df8542d750ea18ca9e4bd17728db1ca336cfa82d30aa05366989092de3a84134781f5b54c2f7b2834164105a399f8fbdf6314e6e59c215a18ccbdd4e9517992abba5c2b7ed35bed0bdb0e67ce9256613efc9edb75553789b79e01b8f44d712730fdfb779237258c04d686b54d1a31d2496efce2faf509e33382d83af7e42d94317d95229a08db911e9997e6acd02402d9555d87c09f63e07dbcf83e343a901df7b85d2180d96177bcbeac43273052dc45cc99034b4770fc542a12d945a42a5ed30da7119e9b88c7a7926fd02d55681adf379eeffd30adeedcbeccd8ff2f2d1dd1cdc5fc531412efc1a679aaffb57b89c7aca27926c9794797d6e3b9b246f547567bc4c02902ffde565a3ca66c03a46705c21e788cde74c27e71a9bcf192fef8a2d150c37ef182f7f607729ffe8fc0a7fb3709d595948a68824b38a5e8e1eeb2f910ac7fd300138c62ae30b307d310061fa22e43afd65995da7c177e089fba03e164721b62f316b931800611fa2b17e4550079a2568959545f51009de7c2e34946d33b06509a667617c01eefb3ae436e559b97d0bdd1d6046bb08c288e678d0ddc397fc0b5c8983a2b0590afc43770f87648c3024cc0c05e4ff7e6bde088c80e609479c3160445c6304c7e01a11eb3ac2f4a682fbb23b2778cdc129ee2a6de51be73c1e97335c75167af123e1b63246660fbb58201420df52a66760ef49989c87f26a00444f8911f31db8e9191859866fbf4e59d9240957175704693008939068d068bf6cc4b351a57a19769e856d3330761d0acdc095214271e76e46ef3c4a7ef3168c7baad7b783f012b38949c9f3357f8eefd90510e1456692d73bdbb945c6837992b222a2c7870222c17af7e5884b55f07d8c8ff6d29428a82fd53c4eced4a98e5fe5e52faaf233b76ce2e52b2fe35cbdc353f6792ec815ead2a4235d7cf1b1587ca22f2884fe1d434e5df29aa3ac4536b7c7d9b334c9ceda268a368739e020070dc18d758bfa1eea5bb03eb6d3c66f34f09b0dc69a0da6ffaac67337b73873538b95429b4e2e76ff3ee6cab2173c15c70b40cfb761f43a6cbf00bbce048321f44441fd5c9edc6495d1db8e50dcb3172478bfdd71fd44f699c7e156c6b9c51da386471d8f2d1411156a750fab9c1e29bbbfe53a92b090047befbe63cc80bc4155ff3bc814a14feffbdefe2dcb283585c0d4a737e5397a689c9bb654992a4cd3ed94996dd7b9d26950a745d3b469491b8b25af39f29aa3a07926ec189bfc0926ec18ae3ac9019d4a79e34cc763454faf9d368bcdcb9c2f5d666e6c8525bb4453ebb469d33516df518c05b71bfc1c0f8a4da8acc2c8128c2f0616595909410807a518c12957c86fde4a61db0e4a7bf6614ae59eec2296db5ef514626c422f71aa377de6e6dbcc5c6ef2ecf9fafcb5a5ce87672fb7fee0d3c79692cbefe98afff48d23d38d3abfbbba641ef0bda442248027331d0c1883c9d11c376d2f7378cf24b7ec98a2e48c50bf3e4273b544a7950b9ee0a09f3e6be8d7922e26389798efc4ee3646fdc59f7c4994033cedb2ea2fd1b40d3a74e8d2a1dd5c45575691e506b2da406a4df2ab1d0aab16f143fc45103787532ae18c8ce28e4d90ab6ec21d1b27373985532c0e0c1a27e771ebfdcf608c26dcb46f952bf36d8e9f5ee5e44c9db96b6d1656dadeee5bbc3f3875c2fd8dcf9e581e78d021a98745b03ef75e9e359ffee2a70a878ffd750edfef83123de911cf9c7adcc59d2f502a18264773dc71608c571ed9cc44a5807a799a2b15ea4b0140d637048febc4a37daaf12c70628341a49f97493440a2f9534fc0d0ca7d0ff57dd4b7a8f5c1daf081040b7e8888e304fd1807711cc47111d70d1f2693541f811f2b8d34d977f769ac5a7cab2cad743971bac689d3abcc5e6d516bf8747d9ff1aaf2a677b578f9ebbbdf299478afc0d3b2690d40ec2223029f021e6ab7e08b9f2af0ddff9767693e545a4f3f6987915654a084a8443e67b8795799bb6f1d67ffce325baa794a79974eab406bb544ab5ea45d2fd0edb8f89e835a41ad606d10bae26f3a45e005fc24dd66641df199be061791d8b54439081f54184cd288b535d807b8c516525ec49db8c2426d9553330d4eced499bddac2f783d64494c929e5f0dd5d7eeeed1d76de64114307f80fc047a4ca7097a58bec56f8be042bbf74da70e23b2edff95a8ee38fba34564d3fcbe9b90bc90e9499a44c6f2a70704f85033bcbecdb5966c7e6626fe47b1d976e3b87f50dd6373d70225054052c586b7a8059dfe075dcc4cff79c1e58098586e0f4464dcc05f680eb8f2834ed4a01c5d2d10673ab5758f42fb1dc59e2d27c932b8b1dac8dbb6f65624ab9e50e8f7b5edbe5f0518f62b93f0550e12f04de2fd5e4d327690b791dca9f272673165696840b270d8f7e35cff1efb8ac2cca40ba923933271963e26ea75c346c1acfb36b6b913b0e8c7278df08e3232e11c06917956ea7e78cacc1f74c02c44e334f7da94263b94cbb59207afc2661cf31213514206131a9e0ac281797e638353fcb62eb3a4bad55569bedfe6de290676360cf419f97bea6cb81db7da6775a4627fab9766c4e7616789754f9eba180e802bf88f069a508328e680da8f714ee75a1b6223cfdb8cb37ff34c7a50b0ead86d06e820d9fd24b3dcf189b0593402cfef4a3eb0a859ce1e6dd65eeb96d82fdbbca54c772b88ec440891eb08e808af2d941456be8d2d40a7ed7a5b952a4590b5c63a7910fc0b382e707a1c35ac5b7e05b8b554555b16a596dd7995b5de05a6d91cbab0bd43a4d7c6bc37956a0fc42110a25656cd272f8a8c74b5eedb163af4fbe00c6c988b73df96988f0a054f9d3e1802cf22bc0ef00a8dc823877803d89da5308cd1e3000be0717cf18ce3de770e194c3e58b0e0b5784ebf306af1b9bf90e09c6092662e96d9442bffaee4dbcecce0926467303206659cf401f21b382e0ab526f7ad41a3eb5bacfe2122c2dc3ca0aacd694462bb85e6f77687b5d3abe47cb6bd3ecb6537d40b104135396a9ad96ad3b2dbbf6fbec3ee0b3fb80c5cd25bc604cbe8c736011de2a55be1c3f9f98182accf7c4b2cfa13289b86f017d1efcaf81edbb3b37077b6fb1ecbdc5626d97852bc2b539c3b54b86d9730e57660d572f19e62fc701ca0e9671ce55616ebecd235fbdccb9e71bbceea59bd8bfab127cd22f3eed0f2d24de44044cabed7379a1cd95c50ef3d73b5c5deab05af3586d78acd43d566a1ecd8ec5da648c09823f3df793cb4175ab65f3b465f336cbd65d96c929a5ba39389e98eadf421e9c790ca13eeb2dd5c107ae93316481fb45f846201c88b8e0bc189c0751c923fe63a8ff35d03984d8639d7d79b016da2d683785764b68d6e0e259878b670c17cf3a3c7fce505f35a15be9ffa2cc296ee24660f3649e57bfb8caab8e5629179d7e9aab813bb40addaee5f46c9d0b975acc5c693277ad4da3edd3ee58da5d4ba7ab7d95497fc61ecc3b82e5a1caa8656aabb2794700c0b63d966dbb7dca15c8179542512914c171fb32c7f38348fe9e4ea23f925272545eb804bcc354f9fa50407491edc013c0542205946d88fb0e3047402cf84f80ff9760678015820f7c46d957dc0a924c4483637529b4a639e1f26c6055d7af191af500c46e1bba1da1136ebd2eecdf59e16daf9b66f3649e66cb6761b9cba98b754ecd34383fd7c0b716c705c755dc1ce4f2d136d8cfe5a03ca28c4d2ae3d560746fda128cf4ea16dbcb8082a01e173e19fee241bc773db29278808fda898e63a085cd3f89f21ed9c4e36b0132027c1278782010c918386f02e73e54aa048f759e40edd3889e46ed79503fe12fb332cc543cee9db7166acbb072dd505f15eaab426d59a8ad0a8df0b82079b64e9658ae7558acb771f3967c01f205a55856f2452816956245295794f288521a512a234aa9d21fdd71c59056540600bdeb693932648bbbd0813efaa2037c5de17d6613278702621731280f134c0e8b03264809cc41c479156aee017140db885e033d87da27107b1cd5d56c338541ab19cc3e7a661e5db716ba9dc00d7a5ee062220b707384f1255b7911ef72833eb25c505cb159720c0013ef23a6e09e4ba6bf15e5b308ffd8d94262f9246d2128ec44f9b8086f217c382e2d1c14c0dc01ee1b417622940041e920d4c03f0ef618e805d03ad082e88baa917584fb03c2a6a44a2b7500d8749d78f5f8a81d563e76dc7b2660489369a006ae45d763ca1f52b7017c04f8f7b96dc9763245d1457e56e1f745d99b55a20f500935b722cedda8dc8cc85690422f4554ad217a1eec53a8bd88e87560197405e80e7565bd3e420e87ae9fade13e1242a6fb4836d1db4fee908887bd6bb173091052f535ab4e7fff34f0dee26ebe99d6ed30408aaa7c00f8a8844f35a6854d8e5c07cc5e9003a8d987c83ed46c037562696503d10554178079b05711bd0a3a077a1da58e30f8b5b8ecd97fb68b1ad8cfd0d780924816d22c40e2fda547bf26eb0d80a403d72cf03f8cc32f17f70e7ea66988b11305f88fabf28b91a0f18ed382077f0c50061905d90ce6163087c0ec032d2687b7768026681368034d54e7412f815e03bd0eba8cb00a5a83549abd96c752487e4a29c637b17389c3342819ca4fbba5441b7a837a7d7ee6117e66f410c7c9a0a18000e82255025ff76e943152a0442dc48323a9fd804c30c9941d20bb50d981c8242a158247bf5c0417255cfa4608d66a836749501fa501ba04da00ba40ff7dc460bf8b6a17b41e00a87554c3977bb4866a8be8fdf99ef242a5c5153d005c0aa884d2536da4ad44936d5a6016f8e5c93bf9ca309daf0908802e3205fc12f02155a686adcd441d67f9eba1a318035201ad80948102e0a0ea000ec1fbdd7066d00000032f494441548842601d1e68f823543e1e6a3da003b6866aff53ec034136cb02865d8b59d700589abab6be363d94c7103e26c257aa7731f0d199886e080804ee4b957f20c2c72250d213c02c37920e88f111d53b972a93a89f1eb5c394936a33babe66bde838eddae29f0dce8a1371771457ba1db490f0baa7f079117e1be5c9cdf7aefd01ff7501026017c80bdcaff0cf80fbd0c14f43648da6cc32c3ea6580361064334665a6cb4929682db774a336e360ade5ea52c70d827f77f1db18fe02589abe7f50f634ad1b9088ec35265479a72aef030eabe2c647735cd04c21236162658605cb758fe6417fbd76001e3e9ad7beb606b0b1b24b2a3c2df03985ffb9f3b52cf202e8050302e0cd81c2bd28ef52e58d283b091e634a32d7e3766d45c5051deaa75323f985b439340067d45f579c480da8b0dc3cf055843f17f8ab5dafcffe84df8de8070204a0330b1a7c7afc20ca5b517e4995a9f5a47e43b290c4688ce805a493d98a4d5b13d9e5d2d7d6d566703c23c29f217c0e38095cddfb861ffcdf23fdc08044d43c07806bbb6c51cbc3aa3ca8ca6e55a650f2034a871b8f6662fb595692527296a524f6537dfe004a8ff65b04cbdbf3c0d751be20c2e36268a8e2ed7bf30bd15c36fdd080a4e9fa09c650ee4379157004e57008d0fa46f3303795022aaa37cca5fd88dab40a4b02330867557902e518f098c2e5830ffee8ff51d88f1c908816be870b4ca31c54cb116b79915a6e0f01ea7d3fbe0750b885352c22ed5e8600dcb3acac36c37a6bb8ac15554e0327054e283c0dcc883273f0edccff906ab921fdd80089d3956fe1a28c288ca8654a2df7a8e56ee010704895a2dae07b1a1a7c96db844a32eb09c090543e90ce962cd107c334f81f871a1cb7506642009e12e118c2b3400da5a54a0368ddfaf08f4b3383f41301642dbaf0155c60a72afb8003aaec429902a6c2a4a148f0a9735709c1028362547b999d47a0642f54bc55a583d25268a02ca22c020baacca29c57e53cc1ff2e6cddf6ee9f8ae899f45307642d3afdc71451c634f82f3e45555cd570f12bb0245783fbc79d108c0e01182d94862a3594da6deff9dbfdef56376883366883366883366883366883366883366883366883366883fe2ed2ff07c6d17b965bc0ec7a0000000049454e44ae426082,100,100,1296,1282962745);
INSERT INTO `$prefix$flip_content_image` VALUES (6,CURRENT_TIMESTAMP,1,'shop_category_drink','Generische Kategorie Getränke','Alkoholfreie Getränke',0,2,0x89504e470d0a1a0a0000000d4948445200000080000000800806000001b439515d0000000467414d410000b18e7cfb5193000000206348524d00007a25000080830000f9ff000080e8000075300000ea6000003a970000176f97a999d40000323449444154789c62f8ffff3f0325182080c0c491234718efddbbf71f1706caffc765004000c118ffff3f7e05c1af5f43f0b3d7083108c06a00400041183085c806208b410cc16a00400041184f9ffeffffe4c9ffff8f1efdffffe0c1fffff7ef43f0cd9bffff5fbaf4ffffd1a3380d00082030f13c9ee1fff33886ff2fe298febf8b91fcff3954edffdb50feffcfa381e251108caed1a8022206104060ce0b6fa0662f087ee909c41e50ec0ec46e100c52b78b81c113dd00800002730e6b33fc7f6501c5e6406c06c1af4d81d884e1ff567586ff770c99feffaf49fcff7f76e97fc552848b0002084c8014bc0161352056056215285606622586ff409bff5f95032af572fcff3f3ff2fffffe9cffffd73582c5010208ee2790216f4519febf13016261201602624188ed679918fe9fe764f87f599ce1ff35a0c18f2cf9fe3ff6e0feff3880e13f4000a1040c50710948030cc3c42f7131fc3fc7c2f0ff0cd0c68b020cffafc832fcbfa9cbf0ffae25c37f8000c288165f5fdfff208c2c7603e89a2b3c0cff2fb0435c738e03e81a31a06b805e040820a20c00f9f526d06bd7f819fe835c739e15e21aa0f85f800042d15c565afafffcf9f3ffefdcb9f37fe78e1d2083f2d00d02b9e6241b58f30790184000210cf8f3ebff860d1bfeefddbbf7ffe9d3a7ffdfbe7dfbfff163474186acc6971b010208abf3b17903170608208aca02100608204828dfb881b32c00617c06000410c3ff676ffac1b91d57767ef51e246b86cb008000422d4c6006bc7a8d5e16e07405400001f3eaf3fff0f2e0e1434459008ccaff57affe0746c9ffff17afe034002080c07900561ebc8ee5fbff3152e9ff8750a9ffafc2d9e0e50172b246cacea06862000820b001b0f2005c1678229505d0f200660030f194209705200c104060035e59229505e6486501b43c00a54070369e90fbfffffac6ffc806000410a444422e0b5491ca0220de260bc9c60fb5f9ffff8f01baba3a015ca880bc0cd20b1040b06cfc05575900b2fd0c23301b0b420a155036fe1c6df1ff813b440e2080d0cb83ff487831486c0f03221b830b153148a172db88e1ff71201b208030a2e535281da0e540f46c0c2e5464202e000820820680f06d714836462e544eb342c20020809073e28323470e03d3ce5560ba7a8aad509182b9061c2b507180008267e34d9b36fddfb2650bb822bd04ac8d4065c3e70fef0866698000427601175a79709298ec0c1040782581ae51006227208e03e22a20ce02623f203622c670623040002173e0e0d3a74fffef0233e08963c7c14102c2c78e1d03d6d147e1fc2b67cefd7f79e30eacb88001921d001040a896a3372990cb275ccd0ed422876447000410c201d80c7df20a52b621e3275071dc8e20c901000128ad432300610080819bb102c360900814ba0e5385aaa94161ea6a10b000a33cbd63025c64542e1fdc17e749ad94c271b0efe44c4a6c1b31b606ac84d0dea0457c599867a6897164183c7df75be01540f022e07918148722f08b10241c8c8483a038108803a0d81f7bb98b8c81a5a002724908c200010477c04b27287644c20e48d81e815fd921615b083ea403760038b383f0efceb8ffbf67a6ff7f9d25f67f0f33588c19dd72100608209803c4418e786d00c5fa50ac87847591b00e1206b60d1f6b412cbd0c6ac2b9dbffff1fe4feff7f5af0ffff45d1ffffb7a603a3ade0ffffa555ff9580ed42bb22b0dac53007000410dc25400738839b784043deca42b10c129646c252080c2bf2cf02cbc60bbcc0e4042a3381e2d780edca7bba1cffff07bafdff9f1af4ff7f6114dc315f9adcfe3f8b04eb7b00104018f10434702ec8d0edc00aea1d3714734131270483e4a055c6d7ff9072f6ff05607beb1c33a4b4073b8607d531d73518fedf0286ee1d602d790f1865f781d17d0c589d000410c1540a2a56413504bea6d66e500b9a0f180540c75ee440730c0b16c7001d7f4b1f12720001441507000d6ab80df4cd0d606d7c5d00e1984b9c581cc30d71cc1e46b003580102882a0e4072c85b582eb80274c875414cc71c6144ad0e010208ddb2fcbfbf7e80ab4210debf7f3fb80e380b2ca42e5fbe0c6e773f0136e060f2515151c438ea26d4512bb1c9030410ba0398403eedefebfd0f6aaf83f0d3c70f8105e585ff9781a5e5ab17cfe1e2a434c1f1618000a24833353040000db803000208af24b0de3701e20020ce01e20a208e0162072096a19603000208d932bc9d1c7c189438c9750040004118af3eb4ff87827ffffefd7f71e7feff4b67cec25b3fb016118c7df2f8f1fff72e5dfdffe5eee3ff70f0f3f745721c00104030060490db224200921d001040a0d11e469c96a33be029c12619c90e00082060cbe3ed2ea21d804b0dc201daa43a00208018fefff885bb3d88de267cfe1a777b10847ffd3e4aaa03000288e1ffcf9f903e32ac9f0cc28f1f43c6cf407d66e43134100675fb6fddfaffffdab5ffc0f2f9fffff3e7216dc94387400e784aaa03000288e16cbccbffff172e403ae3a0f1c57dfbfeffdfb9f3ffffcd9bffff5fb7eeffff55abfeff5f0c6cc0cc9bf7fffff4e9ffff4f9cf8ff7f2fb0a86e07669ca6a6ffff6b6bffff2f2dfdffbfa080609b101b060820707bf0a21bb4a31f0fe9ec83f0ab38ceff6f6345fe7f8896fbff395cf5ffa750f9ffef4345febf09e7faff3292093200803428081d0c9882cf32609bf0327abb102080c00e38624a46ab3810a9550c6c119fb522ec7b90e5e80e00082090034ec0472a90462f51462d904732914730904635d1831f58fde600b11bba03d01d05104020075850a3590e6d9cd683eafe27c13ac04e4cc9ff6ffd41ff1f8781c59f012ddf8acd0100010426e0432d9648c3afc8c32e4843b1284330d061983bc0c6e62e26684b074b73bc779a2338e881f2c5e80e000820b8039ee893df2fd8a60cb1fc9a2a03646817349c031a17020de98086786797febfbf2c0f3c5e8cdc1c036180008239a01d3e760c1b33421e37421e4b461a4302e113aa1043915bbdffddecb1f607fe2fa9faff7f47dbff4721603de220bb010208ee1290034e2a93d6317983d431010d2181c6a240835aa0f128d050d21f77abffffa37d20a151050c8d3e6068cc2af9ff1498c30ef2424202208050e20364d81979c800387ce00b36f8051b00830e82dd15875acec870f31413e10ec96b0fa5ff1f220cfedf7766f8ffc8079450c10eef0108208cbc0a1b28bb2f8cbd6774570865308d691774200d34b07e16ea107068084006d54083eca0d0008dd08106d7ee5a417a45b0913a8000c25968000dff813672878c7990f23bf13d220d488f08d63d03e9050820826535a853826df00ed901a0b137d0201e683410141aa01141586880660950424319111aa0ce29400051c30132a474c9ae484242e3003b2411020410c50e808502686a03349c496c68c0ca038000a29603844f037d08efa062e913c2438319b530020820aa3800ea080958c7743f33eed080aa81f72b000208c320607fcf04d4e7dbba65f3ff53a74efdbf78f122b001740bd8387af87fdbb66dfffb808d11a0fc1f028e790eb2e83417343478e1166378042080d02d077746413ddfddbb77ff3f78f020b0b57502ec08d0eccc7d6093ecc0810360796807f5050187f8402dfe07c442d8d4000410b2e56273e7ccfe0f9bc502e19dc0a619a88b0e72c40560b3ed1ab01d78f8f0616407108c1a42182080d043e037c8d06f5f3e812d00d1d7af5ef97feeec99ffb76edef80f1a3b003b70dd5a58f7dc8452070004105641a0c18bd0a7e2d0b001a516c330400051c5104a3040000db803061a0304104e09602f9c1f883b80f83e689ee2cc9933e08c70e5ca95ffd7af5f074ff192338c00cacda0792550ce068d7d417bfd5781b80688d9e81d0000018429f8f3d7afaf5fbf823d0c2a0741f357a07290dcb11362020514a820fbce9f3b07eca8027baa740c0080004217c0045f7fe01f14401f3ec186f1f5e741f8d91b6c36d32500000208c1f9f61361f5bf7ff81d4c6c00a04fe413c23f7f23dcf0f7df637a0400400041188f5f85c02d7ef98e34475312f3d8f0bb4f744d05000104617cfc0ab10e6439a90ec617fbe4040008bfff0c4d057fbb691d000001849af761e372cf80f80508bf40c5a0e52630fcec192a860db1a10fb5c186db60436eb06137d8d01b1883f8403d8f5f2202e1cbf7ffff7fffb94feb00000840ab19a3000c024130efcd8b1404c14ec45e046bf1117e2593d38390226d8aeb972bf696b93d44c8f9087973c0af9953d9e0eadac929dc8c501c7cf7ee16271427670ce85d519f04575a835aa114fd432fecb7b264ce9012c4a83fe910c07b700eac551468ccf5f7026e014833631406a100867a027b8fe2e42d041d45541c4444f8e0a0088adb47044741050fd64b149c0a9d5a78a5fddaad53974c193264c9cb478eb17b7134aee2c44dda3c66a94ef275856581698271546cf27d980f03f43d741d48a93865db425d43554151409e8310906590a6902410c710451086e0fbe0793c5d8bbb6bb005fa174fee99fe6a80d968e79d0896bf3c2f01841200671d1810e39e58c63f5170241a8e40c3e168380c070ec5c44f82e001e0416100fc47c258c7ef0102081600ff4016eed4421b7b455e95001d8785633f34ec8b867dd0b0370eec8589b76b105e014184e78f2079de0b973a8000820540272c15a08cfd228dff62605734ec82869dd1b0130eec888a9fd8c1637f39cc91a0a95e588f1a07fe0cc422489e1741f27c0ebe8002082038031600b7cdb18f3f63601b346c8d86add0b0250e6c818a61ee007ae809cc83b74d807e2b8f03963529ffff776741065aa703b3f5828affff57d5fdffbfa5e5ffdf8d55ff337359ff179ba124fb5e42290520809003e034cc72f4f16facd8180736c2810dd1b0011ad687d8bd930b11b337b419fefff5b1071694c0149c18f0ff7f761864a0b72c1612185dd0c09856080f8c9c2e1db0e7b5ca19feab16c38743847105004000a1706001b05b1d75fc1d1b7ea385036be2c01a68581d15c346b941f83007648de3554586ff775539feff77b4818cfbfb3a430223c11f35301a925103637e393830fe6d6efe7f777dd1ff57699cffcf69c2038319d9cf0001841122b040d8ae8e58578e152be2c00ad8f15b79342c07c1cfe4513d7f9c8de1ff457ec828ea2551c8482a2c304023aa0f7479118111e609098cac50d4c0e8cc8404c6546860acacfdffb1c1fafff31860b3271075740e2080b0260ba083cec1cb04d03c8524162c81038be3c062a8f8b51822bf6f9782386a1f0374f014b4f21d348ec985233014a083ed6a906cf2d656097b60d443036362ceff477e90ea153437f238181108000184b780003aee29cc91db8096bd1664f8ff4e008af971603e1c9817e85020dea98a31d3a00c8b7dd0c02d7c9a83114f60884003431a33306003ffb781e5d11d60a17a0f5840dfb3874e89006bab879e0ce0c0d8cf01c912000144549d0a74242310dfc03655020a9823c0a47c12e898b340471d05a698bd4a38a75560d816d97c58008086d161d32ca404c665092202c31c35304ecb43ec040820b21b1a301ce0edf5ffdeee8dff6fad9ef7ffeaeceeff45f686200f7a0331d1cb7ac0c91fe8b92bbc88a17c6c81019e5f400e0c3648605ce02310185a48816104090ce88aceff0001447100c016d6c1303983e54087ac003906348d009b662225304034dec090470d8c1b5af04237122080064500c052c1257ec43417b6c080cd36911c18c290c96350605c91837b1eb4d08b012080064d00c002e114749aed962866608067bbc8090c4ec8d2de631c70cf6f83d9091040832a00a081e0002b14cff0400282d2c03881da6f6042b60f2080c8f1302b106700f17620fe0a9a3c034d9a81e609eedebdfbffd1a347ff7b215388ff40bb3980b80d88a52808902c6c1da0733cf803e33c3b861e6e6ce603041021cfaa8262b4b5a5f9ffd7cf1fc1fb5d4033831b376efc7fe8d0a1ffc78f1fff7ff2e4c9ffe7ce9d034f7480c6f76fdebc091eeb7ff0e00178530d288060b38d0780ec95cb97c1e6d79a280894b9047a8730ec4ac82c8000c29bb44f9f3c0e9e8ddcbf6f1fdc1320cf6fddbaf5ffae5dbbfeef038a83e66b413347a02954d0e4062820409329a0d923504080263f1e3f7e0cd70fc29fdebf455e809c466936a404030410be000025e1fff3e6ce81af98fefae9e3ff3d7b76a378061420a09973508080621b94324034686e1959dd99d3a7e0e65cbe780179a6956f200300208088c9f309d8a6884b8a8bffcf983eedff9e5d3bff9f3d7df2ff8d6b57fedfbe79fdff857367fe1f3e78e0ffa2850bfed7d7d5fef7f3f3c336bdbc9cd61e2316030410459a811ee105620f208e03e22220ae00e214200e00628581f61c3118208006dc01038d010268c01d30d018208006dc01038d0102882c4d478e1ce104621d20b601620f208e00e214202e01e23a202e80f243a0f21650f5ac03ed61740c1040f83ce903c45b81f82fa88e07357640f53ba8d5076aec805a7da42e86002db400e907ed88029d46005a800134ff1710af0605e6400400400061f3f849500b8f562b42f06150430a683fded5f7d4c60001842af0fcad029006b7e040ad3b502c815a74b4f43468cd1068ed1128953d044dd0fefef3899e01001040e80258c18f1f3ffebf7df1f2ffe3ab37fedf387ff1ffd993a7503678c330480c7df337b2dcb953a7ffdf04ea7f0234e7fded07ff7f3e78065a0982cd4aba050040002138afde6fc2eafb1778568c10b33ee80511cb64fefc45b5f3d7efd9f40a0080004270d02302b44a8392e531a42e9319a05400104010c6e357de44c73a327e45c0f3f8f6bca1e327af0724000002089afc3f204e67784744cc131300a4ae1003e14f5f910320931e0100104010c6b71f086b4971f04b3c01f09c8c0040ce0a7fff3da747000004108c0101a42e91c31700a4247f6c2bc4e8940d00020894ff45c98a7d107e8127009e911900a80522cd0300208040011001b6ead71f120a2c287ef11af3e81364fcf415fea35070e10f5fe816000001c4002c7dbbc156bdfe00b11ce4e8e720cfbdc4bd46107d9d20be3582e8eb0431d60802f14390dc736ca980e6010010400cffdf7e3cf3ffed5b4c8fe0f304ba4788595f78fb36648d21fafa42d01662d0b936c8eb0b8f1cfdffffcb37ba0400400031a0c4063e4f803650838e0d007902d87e077b02741819683335cc13a0bdd0a04dd5a0c30a411bab417ba24107f580f6456fdffeffff962d90037b40fba34187f680f6482f5ffeffff922590c37b407ba591d6261e301365a77500000410c3364da605efd72dc35ced09f304fa6a4f9027366d425ded09f304fa6a4fe405967d7dffff7777632eb0acabfbffbfba1a6581e5bfdc8cff7bf5b841b3cc24efcd271503041068eebf07366fff2c059823caf4fe7fefcefcff6fe624c49255d0914aa01deca06395409ee8ec84ec64071daf04dacd0ef304e89825d0aef6e262f0cef6ffd9d9c0e60cb03d9396f6ff7f4acaffff4949ffffc7c5fdff1f13f3ff3fe844940860f91b1afcff67a8edff0f11b2ff5fc4328157895ef6a07c9d20081b5530fc05617c6a000208140055b00080efa647c671a8f8651cdbff57713cffdfc40afe7f1b23f6ff7d8cccff0f510aff3f45a8fcff1ca6f6ff53a8d2ff8fa1b2ffdf878aff7f172af8ff4d18efffd7e11cff5f46b0fc7f81be0a15c74a54d0c67a2a05006cb91cce9d7e0001040a8074780010e9408ca5b2e8cb64295c2abb4d9d2a2b452b910200e7a26b8000020580073c008874e00bf4630e701d7980ed50302296e242ddf384c200f887140056b8d40104102800b8600140ac0331d60aa3af13a660adf0734f7800d45129f9e34d49000104266001005a4545cc62e697e8e74ce03a7302d7f91378d6225f3487078024ba6341db7e81380e882b803803791b309ae7b39002201a5f000004104a005cb12473b134fa42690a164ba3e77fa0274d80f8378169f076b400f84d4cec83304000a104c05e6dc20ec47ad0078e433fe0877fd8a32dc0c6b3181bea16f0925620fe0ef3e44929aeffdf6afcfeff9b9cfdffdfec82ff3f27c7fcffd860f9ff792cc3fff388454f2968c9bf8650000004102c00f6c102819003b1ae16475f294ee66af1cb06d095a3320cf3611edfc3c4f0ff4f6e10e4fc68d0911ca0736341cb606797820f2a019d21fb7f67fbff8f7daeff4381b556992571791f8601020816006ef00020b09c1deb492b384e5d819fbe827e020b8ed5e8e085970a88a47d949fe1ffcf4827a256898396cc830e4b31847a3ec19fe117d00c82335100010467c0cb01fd81592eff401768bf1ac2f3477819fedfd167819c18093a1d06744c0d28104047d5804e8901a506d0493148a9e1d7dabaff9a65900000054637e49419bc23cc0001841c00bfe0fb05b0ace5c7c07a38b02e0eac83034397df830a3fd8993c7b99200b1bdf9b2993b54c3eaf4b171c00207c0611a802d8020020809003c00fbe6d461bff7e813754de2f700394efa511b10f3a7a057c220e30103e9ba8438ee7010504e8881e506a001dd3030a04d0513dd85203e8d89e758dffafafc9fbff6b79ceff67310cff7743ccee440f00800042e1c0026007da6606f8f942c858150756c18195716025d4fd02a0c58cd8f6097cb3d2256b9fc0ffcd2dff3f369afe7f91c0f0ff201fd88ecbc87e060820f400c88405c253240752ba61e22d9e0d13bbd41027428130684527e85c24d01257f0f256a4d4003a15e88b9d2e223580ce4a424e0d55d0d4d0074d0db34a20a9616dc3ff675190cedc1e16b03dab607e0608208c3c81bcac1de648f8014fc858060796c681a530f15165e83e21c8a1fcff0f3110bf51e2adb5d2ffff3e4ef8374a4cce87a48679e5e08d124fc3211d3c686083075b0002085b00f020ef05c0ba5b840a3b468e21ed29802f8d85aeef051d4c055edfcb833f35808e67ba67c8f1ffa737f4e4ac14686a2880a6869634706a789da808bef5017488152820f6400adbeb20ff020410d6aa01e828639494208a8adf219fb6857ce2963096d3b7904ee182e15da8bb465ec10280d29d22778c18ff3f7394f8ff3e50e7ffe768f3ff6f4255113b45bc18e0db66ae1821b6cc000410cefa712bf4bc3b183e2f83b45d86cc2d33572530768edc003a441de498038c8805ceb0d39f60c794c1538300343588415203f87c2ca4d4000a04d0f165e04d11a690d3c360e72a838e32039d22063ace0c941a60eb870102086f2b09e84076f4ed2e2741f99f07e9b833e423cf9031f460e83740f66969acdb66f2a09d9d4090634027f9d364ab0c8e7d43d000b0070820826d65684004e1daff03da1f741c182867a420fb860e021db303736314323e886c36d01155e0d21fb6dc9d1d7afc16b6d4c042626ad0c7911a9ce001500c10404405005240c800f151021ba2b0e117408cb55f0e5b0e4ff3bd42681ba7a001900510402405003af6f5f565e928caf97f67d3f2ffd7174ff9bfbe2ced7f8183f15fa067cd8098a82571a06408720c786b0c6cbd3f176a6a00070496d4003a210e6f6a50c6911a2ce001600910401407404343037cb7c8aa55ab400ba17f936206d01142e00d5374d82c054b0de7e410b50040000d78004003e1ff455ec4ee0ff4d4003e968f98d4c08d961a24d152833a2410f6302102002080064b00fc3bc64ea3dd62e8a941159efcd781ec0608a0c11200e920478137460943038216a901d896d9c7020f001690dd000134280200960dae08d260bb1c526ab8280af7fc3d98bd00013498020034dcfd1f74dd1168df204a6a1024333530a3a60658931bd95e80001a3401000d84c72007de11a7fee6c9dd8821742f643b0102685005003410be81bbc640cf5c1644040425a901c9f35bd1ed030820aa06c0faf5eb290e0068204cc43601728a8bc4d4c08ca2ff2a36bb0002881c4f1b01f10c20be9e9b9b8bb15912b483d4dfdf1f1410cf41170a823650511010c2407c0a3d20405bed09a586434c287ab6e3b203208088f5743f68bb5b4040c0ff9933a6ff3f74f000780538e8484cd04649d00608d0b2775020800ebf055d0d035a02bf67f7eeffab81d922313111b65dee20100b9219184de801819e1a2e0203e0203346ca89c3672e400011f2f8513fa0c3cf9c3af1ffec99d3f04d90a0ab2389d92d0ada478cbc79f2ceed9bb0c0f801ca3e64068411ac9c2080cf12631e4000e1f3fcff49132780777a827687c23c01da250a0a00d0ee50502a000502e81e4d508c8302027d2335f24541207cf2c4f1fff7efdc82a508590ab20768ee702a9aa7ef02711229e60004102ecf9f076f83876e7545f600e8b2c01d3b76fcdfb3670f381040472d830201b4ff07b4c3041408a0536341fb8340a901742835884636036466694909558e66a614030410ced8470e80bbb76fa17800b48b7cfbf6ede003b061fb8571ed2407650be414f4e7e777b099212121832200000208570004821c072acd61e75c83f0b52b975102023d45800202742238288b6cdebc19ae06b4db1cf9daaaecac2c5816b831d0010010403825808eab81a584e0e0e0ffa78e1f837b0086dfbe7e054e1d572e5ffa7ffe1cb046387f0e1c488f1edcffff09748f2692da470feefdcfc9c946de407d6ba03d0fc200018457127a5ac435f4dddf616161ff3bdadbfeaf5ab1fcfff1a347fe5fbe701e5cb05d0506c4296021b765d3c6ff13fafbfea7a6a460db39fe879a8793538a010288688540470701f17d02a7aee3c2a06aaf71a03d8b0d030410459a819ed201e210e89922a02c9307c451406c4f6e3d4f6f0c104003ee8081c6000134e00e18680c104003ee80513cb018208006dc01a3786031400051c510609f9007744b39f4c88514e8651993a1c7221c06e29b40fc1edb7e72d87e731006f52640dd2a1006f52a4018d4c5826198184c0d483d08c3f6ab63d9b3fe0f885f03f16520de0dc48b81b8177a996902f46c0bed81b8d863b060800022368259a187848022f30ff2791ae4deb6329830ece61750f71534a8011adc0026a66f40bf6e00621720661ce888a215060820c28a5ebdf7be75e3e6ffa3d01c0b0a24720e4f196a18942040a50ec8cfb7af5cc339a03ad4314000e6ce1d05401806a093f7bf98b8555cab5b209350900c699f1297e2a093854c4d43dae6036f689f2737197c2f7c26db0d9e1311919380cf29f97865cccd483938382955223021d0184a46c5001241cba81c081988bc0d2cf491588f2dec629f0b044d12b0542a7c80d601b1c9705a49b48ea9f9ba34dd3567575537b37ed3e5f84cf6ff2c5500e2ad2d054010085e23e8fee70ab434a1a7456665d35af4d713828445d61d65dd99df39233f84b10753b858c66eae7f993ff6873b12d198a6b052a217025d92a08d6368ce51338692082948645914ad7b45b9a6f386ea1ee7f13ddd1de88d892b38a6def9d3f7f03d8fd3fd1f67047f13f6752c02085be4cbfefff21deba93e280074251509918d151373fc0c210cbec88882531ad01302f6038da009e01fe8440394e368873a060820f4c8e7f8ffee13fcd8060c003ae8e8e57bea043608133a7e8798c399c8bdc50a1ffef815671000c360da40471a35314000a10a3c7d33ff3fae0cf0ea03f5039a920440abc8472e0d7003e9818e386a61800042cefd2a4867b62000a82940ad22965a09e005158b7d7c186407faf16e20f0e7efae818e386a618000424e002d189efdfa9db6018ceff8297c98929399a89308400283ee50547230400021386f3ea2de760acaf9b40e5c7cc76fe16bf4917b3619751341c440471e35304000c172bf3d38b72303d0edb7f44800f88e1fc3859fd3a90a404f047f9112c1ef3f6b063af2a88101020896000a5052f8e76f44040814838e5b035dcbfa1c8a5f20e19720fc1215231fc9862e870dbf7a851b231fe3867e841b183f83e02730fc1c8a5f403189e3064fdffc07dfc20c02bf7e5fa666440c1406082008e3e5fb75f0c807791014a1cf8081f31ccbb979d8eed82574e72ebe63e9b01d51877e541dfa9175e8e7ef8130a123ec089d0c073ae20e763a1cec2a5d10be0514bb0bb2f319b4d47a0709a7df7f9e0e74e45103030420ee8e4d008481308cba42f69f265b445dc2ce268579c80f698580c541eafbdaf06e1b8b2ffd3cee4f31e6102b63cc21e20d2606b22ff660fc41741ff52e0e21fd0ee347c043f951f0c2f971096978b5be221eda8f8a87f723e321fe720b9990e71e32250ff7e73327f28f9687fdf36efbf577bc15f30840dc1d9b0008435114750b67710a7122858c9245ac1cc131ec6c8d1e44f855402c2cb2406efdcf6bae8f6ff765de5ec58821188ab518310420b2162386e02dd66208812e64301aab46181aacc618e67c53864cc68733e432d200d98c4440b42115106f4806441cd201ed4813026d4953027187a440e4212d30a5724c6359fbce01ecf077c0afef14406002e89169b05dbd7b4c85fe5f8c77ffff715ad5ffbf9b56608f0c6cb9025764204704e8804c7c91811c11a0f326f14506e8fc49d0d18da033281b1a204738822efb061de308baf01b749423e84c4ad8718ea08bbf41473ae6e5412e00071ded08ba041c74bc23e82270d0118fb0cbc041c73c822e044f88fbff3bcefbff9738c3ff6fe3c5ffdf0e6244defd0cda10cc3190916754c15008c48b80d89a5c3300020896009ad1b7775ff264f8ff2c090d2722f08b64f6ffafd325febfcb56ffff31dffcffe72297ffdf4afcfeff288bfcffab3cf1ff9fcaecffff2a4b502302744028bec8408e08d0799b28919100397b13744b3becfc4dd04deda03338c3c3ffff0f0dfdff3f24e4ffffa0a0ffff038119d3dfffff7f3fdfff7f7d5dfffff6b5f9ffd3d7f4ff773fbdff5ffdd5ff7f0e54fcff2158e2ffdb3081ffaf223820c75ac610c6a7ed50c268e700477e23da45ea205c48aa390001044b0005e809e0bc3303f854368298888043c1d88ee8c487711ddf49ccb19ed830b6a33ef11dfb8984f7e8629e76314091af8e25f261781e29660104102c01c4a227805336d40d3c82e7a89270be2a2e8cf75853528f3b45c337dd300ec0101fc004700647e46f24d52c80008225004df404b05f9fba0188f51c595c67cae23abe15d751aeb88e75c586b11df58aefd857283e6880123e2407341523bf0847e41f22c73c80008233a047baa02482673ed4093ca2ced125f27c5d7c18efb1b6a41e778b84ef3a60e47eef018a7c29e8b9e0e8917f1188c95ab7081040c80960257a02b86d4f9d0024ea1c615c18d7f1bdc41ceb8b0d633bea17dfb1bf407c10b5eec73900b48b81411474822d106f06e2b7446ce305e11bd0fdcf62442480bd5822ff3e1093bd0d092080901340267a0238694479e0117d8e3291e72b13c40e24627bfcf8b20946ee4f408a705520de862b72773332fc3f22c4f0ffac0ac3ff6b668cff6f017b117781e1741f58b2dd079656b780eebd62c6f0ff840ce2d27b20fe0c4d103c68919f8c25f25f01311725a50a40002127005620fe8a510dd8511680449f234de4f9d204b10d018ceda8671cc73e3fb5401ce80dc5978011230ec4fbb045f83161d6ffef136cfeff6f0176633b32ffffefcd869c5d093ac41384671683cfb0fcbfb8f2ffff1535ffffafa9ffff7f53f3ffbf6b2bfe7f9b16faff7d8dceffa7114cffcf6902130fe298870b0bc418fc8111fd132df23f03b110a5d50a4000a170801e6cc2e80e1a9217783831ae73b489385f9b288ceb586b128ebb86e1fd9a8870d8260f8e8c8fe8917e901b98abcdb9ffffcd0a449cd9591af3ff7f65fcffff754990b33b618961521e6a62985b869a183636ffffbfbdedffcfedcdffb52a58e10720eb9463e4fc5f408cf5f87c52314000a127005e20fe8d9c004039e09929790148d239e2449c2f4e14c675ac3789c77d1f86463ef8585f562cb91d74b2951ec3ff374e8affff47f9fcff1feff7ff7f3230116486feff9f1701490c208c9c189a53f127864588c4f06743e3ffe40e759433d161d83087e1ff5419b03b402512d9a380200c10401802404f97a097020735c9382f9dd873d4f19ca98e15633bca9c9863cd4938eefc3834f2b74b6046fc410ec8a173a0b3185f984942ce71f676faff3fc0f5ffff100fd4c49011829a184ad012437b06fec4b01c9418eafecf9feef55fb39ce9bf6a1903bc540061951286ff7314e16e7b0dc421a426008000c22a08f4fc06f44470418bf800c48b719d234fe07c796230de63dd893ceefd28d4bf3b793023ffac28e25452107ea0c1f3ff9f9dc5ffff2e76ffff7b3a602686486fd4c4901b8e9a182ae25013430f5a629881961856d7ffbfba2ceb7f76b7ce7f832a4e7022502e63fc7f238dedff1d608fe900c2cda053d75c8949000001842b01b0621b17b8a441c279f9c49ea38fe34c7d9c18db51f6c41c6b4fe0b8fb1740bc530d1af9ec68ad7920be003b7e520a720425e82c4ed83194b0c4f0cd4207353180307262882390186ad112c344b4c430073531fcdfd0f4ffffb656f01d223fe644838fc807e13bc0eef03e841ff680ce1cc3950000020867ca0006842010bf464f04e7d00319d77d01a4de2380e77e019230ae63fdf11cf77f07e920f79dbca891bf9711e9b46ad8a9b462d0c420893d31800e687da8c3f7ffabb50e6a62f077414d0c4901f813431381c4b0b0022531fcdb58ffff65320f788e0694109e8401ab2c847fae01313f7a3c030410dee20118202ca0ae0fc630313011bcc2735f022e8cf51e05642c4704c676950131d71a60c1af8178bf0ac25f3b843173fe5976e841b4dc90e34761e7b0c20fa5252231808e2d7ea0cbf3ff8b3d30417839201243b03b6a6248474b0cc5d1a889a18d40625856fdff5b4fe0ffa7c0887f16c900b933029818ae59a2f82b16398e010288a88602307056a027021006ddee81f3be0812ee91c08b715ddf40eab50e48f88d04eaf50df0d63e5a9d7f92017a9d030be4d451d0f1ab24250619ec89017c7cb316e3ffa766a2ffbf3ae903138333243144a025861cb4c4508e9618bab35013c3d4fcff2fe244e0d744803134315cb340f15b2e2c6e010288e8d6e256c8b1f14fd11301a89b78590ecfbd1904eed120884589c718d75aa0e1e740f30e28633fde7e073fc317e4c83fca8874ec2c03e2f85994c4c049203188e34f0cd75421890174a8f90d1dc899def78cd8ffbfb493feffc5dbe8ffdf280ffc890174b71830317c2ff1f9ff2c50107c2902f89a0c5fc80d118f0321d765c012c37149944400ee3e020410c9fd466060f903f10f6c81780458223c13c57277088e7b4488c2022462b4eb3c5e03c54ec9e1bdd7e0f53639061b6080fc432efa41a7cdc230f24d1628898199b2c400bb1f095762008d33dc3284de7d00c22690fb0fee028bf4bbd6d07b10ec80d811712904f89a103706ac89e181274a02b8008a4f8000227b0001187052407c1c57c0822e89b9062c0e5fa3dfa9c24b22e6210dbf06e2abc0c0de8323a723e15d400c1e4a0506860d4aee67421c3b8c7c1035c989819740629022901834092406332c89c1017f62382480920854010288ec04809618ecf0250618de0eacff0e033d7b119830ee0173f77350a40103ec3d3ecc8e8adf01f10bd0f02be86a2560401e9583984bc86e28de0ac42ae8ee0706441a720200dd55837c7907d989811d2d31f0a32606d0fd58781383121989c10a7f623829839200220102882a0900cb18423c10efdd0a5938496ce4501b7f02e299406c4ec8cdc080a8474e0097b910676fc30f22272231209fd94f4c6280dd0f863331c89191188cf1278693522809a0022080a89e00f0240cd06d54a06bb940eb0f9701f1c9ad58069b88c4a084f50c9ac8fa81380988f58098ac4511a0215458a0ec63841cc00e3b841df9207682898185b4c400ee51503b3118e04f0c8750c739a20002886e09001b069da70a3a4c16f9ea01640cbd87812a57111048009ac8033ff093f879494b0c28975710911840f7c3e14d0c426424066ddc89e1863e4ae4831abdfc0001349a001089e0212c70ae09206e22202531c0eeac18d0c4a0823b311c456d002e00f91b2080461300220164c302e70cf2052582242406ae41901814d012831a24319c41adfb41576d8b80fc0d1040a3090035111c0705d01e6044dc14465cd7436c6240b9bc859cc4c088961898a89318ce8a618c72fac0fc0c1040a3090035017042e7d5ff1f6183dcd904bfc08a88c400bbc1876e8981074b6210414d0c27f830221fe55a1580001a4d0098898007889f82020b748117e8f22ed8055e841203f2a57f03961860034f40371d62c3887c8ccdac0001349a00b027024620de040bb8f37cd07bfdc4f12706e4abacc84a0cec24260606ec89e1240746c4bf0362456c7e0508a0d104803f219802f1275840824a04d02d97b81203ecaec3814a0ca780fc3d0c18915f8fcf8f0001349a00884b08f640fc1c7dba18367174001829a04b70cf82ae3f0646f47521fa2686638c287744c2f06d202678511f40000d7402100f0a0a025fae053b281a46830ea2065dc0b77cf9f2ff1111117f816a2d80581d883907383170007118106f04e29fd812053a3e088cacd35c9044803731f0129f184e03cddc87ddbe63404cf47e018000a245a43202b11910b703f16120fe867e8d143042ff2f59b2047e9238ec3471d0e9dea053be612788831203e83a4ad82584d7af5ffb3f73e68cffe9e9e9e0ab2cb15c4ff51888d702711a1013dc6a45e5846101c44b8949148759494f0ce781917f94056b4e87e145404cf2c5170001448d087701e2ddb048005d8f390b1849278e1dfd7fe3dad5ff870e1d84df1d08ba731074b51eecc60fd8b1f2b04b1761d770c28e95875dc008bb9213741123a85478faf42998bf6bd72e946bfc407cd0bd069b366ef8dfd8508f9c483e017137104bd13141b081c6da81f828ae84b09709921840d5c70960041f07d247592177a082c62288285dd602b13c25ee04082072233d09883f8302b7203f1f18d157a0d703de07dfa5887ebf222c7240174dc22e9b045de902bb220674763fe80c7fe4fb0540090174d63fa854009dff8f5c2a80aa0750a9004a08a0920194a8b0d909c2674e9ffafffdcba7ff4b972cfe0faa6ea009e22410634c0bd32151806e042e03e2fbc4541d6818347a07da74ea444d37010410a911ef006a908102b111d870fbfbeb07fc6ec80307f6638d0050ee07dd390bba6013940860d7ae823028e240d7af822e6e402e1540973a20970ab0cb2640d50372a900aa1e40a50228118030c86c5c09e1f3c7f76077deba71edbf9f9f1f2c219c02e2013de7079a307881580988cda0580588f9e8613740009112f9c2c8f5ed2d607d8c7a39e87d9c810f4a04a09201748b2ae8da595042005d3dbb6fdf3e70423870e000b85400ddde815c2ac06e1d41ae1e40a502ecd611e4ea01542a80120a36fb41d502b25b9b9b1a91db0d2b063a010c2406082092140303eb262ce040b968df9e5d1837ca7e01e6b413c04844be3318578200950aa08400bb911a562a80aa07e45201543d804a05e44623286180d4a0b7036078f7ee5dff9f3e7e88e2b66f9f3ffe2f2a2c446f38160c74240c2406082092350003ac19bdf51d1f1f0fbe31f7cbc70f1809021cf0c03af8e1fd7bff2f9c3ff7ff10308271b51348c1a044b41758e49f3e75f2ffad9b37febf7ff31aabdda0f6495767e77f3fcc1ec34720d618e80818680c1040646b84f6cb6fe3ba3118d402afa8280737be40572cbf78faf8ffbfdf3fb146122518d4c00345f28e6d5bc15736270013239e5b8cff01f13c20e6a156000e750c104054310418a04c40ec0fc4ab81f80b9e08a037be03c45d40ac3cd0013d58314000d1dc02d0800c107b01710310af07e2f340fc9ec28805f5441efa42069a40393a0b884d8078c4de004a2e0608a00177c0281e580c104003ee80513cb018208006dc01a378603140800100eefc5dd20df01a7c0000000049454e44ae426082,'image/png',128,128,0x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000200049444154789ced9d7b8c1dd77ddf3fbf3373efddbbbb5c92cb15455214f5165559d1ab722c2bb2ad28369dc42490d4a9d3d8568a16298a00058ca240ecc008fa4710c446601841e0b40d1024b0520179362ea9d4565dd9b5695b561d49712459a22989121fa296cbe5ee72ef7be6fcfac73967eeb9b3b30f592425073ac06066ce73e6f73dbfd779ccc0dbe1edf076783bfcd804f9510a1d3e7c182005525535229202c6c719601c9851d571111903eafe18f3473d8a4f813ed055d53ed0f5f7e13adc2f03b3fedaaaaa15910cb0fec854357bcf7bdef3a3bcd25b266c0890c3870f1b6006d8016cf1d7572749b2534466446406d8eecfd3c09488180011d7447c2ec7a9eac8395c57dc67c03c3007ccaaea1c306fad9db3d6be6cad3d252221fd34b074efbdf7be7eaabc89614d400e1f3e3c0edc0bdc9d24c93bd234bdde18b3cb18339324496a8cc118531079b50386c4169191ebf582aa1665acb50550f161ad0d47db5a3b6bad7d25cbb22359963d037c0378eade7befb56f8450972aa4eba47f4144ee6f369b3b9acd663d49921584862191ab881fdf87eb321031d1e332656e0ae057d5e3db18b7d65eadaa575b6bdfdbed76975badd60955fd5de08baf97386f46305591faead95417ceffc2cecb777cb451afef1191fa7a15ad46c890560564551dab88aa9136caedc579475e4e64b2d96cde74e58e9d3fbddef3bf55c20aeae82bb330357e2fe38d3fb689b9b1dd6eb3b8b848bbdd26cbb2423ca82a658e594d5c413531ab4299a8b17e59ebb0d662443049429224d46a35c69b4d366fd942a3567f4afad90764bc31f786297691c34a91d5a8eda25efb8426c9f5c6089393934c4e4e92e739fd7e9fc160c0603020cb32068301f92023eff5c8fb0332b5e4d692a962d5a2a5aaab408975ca6a600018111231a42224c6501343620c892424464827c6a86d9a24add7a8d7ebd46a358c31a1a23d18d90ffce91ba6d8450e2380e8c9338666fdfdd4929f172323e22c49129acd266363638882f6fa68bb879a3e9a36d1a6a244bd16dc9182c562434ff647b8569cdc34115719635c6fcf04512ff2289f65e4de2406ad3590892688b8032f42618bd6d3f76996ff85a449fb5210f6470da31c524bb723f269d2643c8e2e64786ea1d7475b5da49f2184173690808320100204451b82989265157a7e85e82ab8227755c9b0ca90632462985f61a9eddadde440d1613b46acde8fea3daafad58d58776f56282bf5fbb551bf31dcc44494de003d771ece2d23fdace006972194884592bb97e23e2242a907abbb18e6539060a40aaead61e191072e7495f894e536b4ba2379d4f58edd9a989b59dfb27c534301881e9f4d49937f4feaa20ab352156d75616119bafd882b4a7ec60a8d8123a6aeada87db6a247ab2a9283da21e9c55714da584dd1036015963b686f30625c281851de476eb75c00ba5db41073c89d8c356e1ae9c9aad0ea228bcb90e5bebf97fd08a72d446578afa3fdb9ca5bf717a38a1e719c11814124dd421bae8c8e107c58a5a0b945965a90e5fe353480729f8a4cbf7e325dba30e49034d9a7f5746a24b5d387a536580d6c8fae600401042dd29c38726288157c33c22db10f11ce79c9b788c4a18eb45fd623a31d497b0327ba6ce4741a9946b8657572bcf9c100e8f1d971a9d76e1337f0e75ed02a2cb520b72591b39a991aec2a1fef010cf785382a397f453ca0d623a83ecfd056f3f9e3eb51a731b6f008a2b6dd854156a0e89febc005a6e1050d814376504bb66b1029802cb71dcb4b79407054640cf57ab5172e11666b79efaaea4c5c1db621882f1f9e2b6e4387cadc1b0245bd21776ea1d519695b90bbad7deb0e6b3940843dd4d21d05d7f707d0ee15992ad435cea8adb45c3da16428b20ac50f548f4121085a6a48a3065638952aa5724351e938d1d7d1eea1836c68a418d921222366fd5b2938406ab5dd886c2fbce6ee0072ebf5b3560f097b0952b6a05c924f94a061863db7d01515e5cac8c72e4b75a718e693b8ade00be19f7db9336c423555d5ebd7a8ee4d0d468fcfa6d4d21d2a320520590ebd41916155274a74447cc1686f1f52be542c28fcf8bef0415616085185a02a646485c5500e216b6f00bd41e85c29aa37af53f24d0b6e76cfc85562c4a0eaf4c620234881aa771eb182bce73c3a1c1e0da387fc3a3c1394f89091822e8fc45be47446c68103d4b7295ec933aadc9d35383420c82d747a81dd0cd6be65392455d5313132a3c6337ab04abc075d9ea720c4c9503c281aecb502c0c00752e8922a96d1613daa4313c35acf005a70c850b7c7f5c8702045225d42e177781da348b70fcd06346a06e5b20b43be0b1f5211a923b2a510d85e5c050b067063513813c88f888ce8954845470edd109ae0e885619215c329257f44860a6a651b5a6a3bf24ba4780283123c7467ad915bf76ef59a11987c3d44ba9421a5968e05858eb5900dc0d8e24d0b0346a3fb48ccb0aad25f29eca41c577212353eaf5247788620c9465c4255d7861d3e9360d0a406690d3a3d64620c15d69d707bb342aa6453f4bbbba5df41bb5df742aa855f17139ee8be4cb4d81a534ac48f3923bad60a0e1801deda427c62ddfc8a58bb22be10b1d6baf279eeae7d5a38d31883fe6e64ebd6b7ec00634aa7753b1d6648921544028604f7b781a865d12491c75dd5f32522a078e20d7bf4f05af3bcc81b133f1058435cb80e60c5f9ad45f2bc18319028afbcf4a2d1eddb27bffe939799fb1e3ff396f310d3bcd3ca802c6d4e0cd9d87bb21a0886ef99a5b8f20b4b44a040a491fb90a744c818a440c898b85a6e23103d02aba82fa4e7f910e4a8cd7cd0e7c4d7fe66a6b578661770e292527b0321fdee6ffccae3b5c9a96777def3b3b75f76dbbb49c72648d21410c40f2a4a78c120120241e2de1d13218e8b883542cca89ea28ed046281311b55c7ea4def2336559c459396a07a8ed71f6ecabbcf8e2532c2ebe96a9d2bf9884bdf353cea550c804ba4f7c6683802c1ef9c7ae0acb734f7e8bdac4143bdef1135c79fb3f636ceb1598da142619c79826227544cd48cf1f21b897db6500b402108d00d3124183c8525fa7443d3f88b491ba626035477580b55d2c5d941e39e7e9739ae5fe195e7aa1c3f92500e645b8680b1eeefc14007701bf297054e1bf01473704088215b75c93416b89b967bec544ff5b8c4fd5308d6992fa34496d33269dc098098c090035315247a863a8215a73f3152550e27bcdf3154a59ec2a5c5212358ef8396afba8f6b1f4510658e9fba3e70efa58dae4a683952e2a390af4fb8e71503284573ef43c174d7fa85217e117815f0010b8f68e4ff29b4f7e9623eb03e2d6c5f67d45a8eb68687f40de7f8d9cd78acc2206911a420d3135204124414880044382680218448dbb5641483c7709a28258e3c05350cd50722077042743d5a2e2e3b03eddc5a971f78a05f1d7fe8ce888f1118f04647dc83350b0022f5c28e25705813acacf87f92311f6030b777e924f3ff1594eaf55360532a0efbc72f7029a81e4c34c435bdf62e921da2be28155fc90351e372e54317eb5563db1df511873e5e21565063de762899029ebf7d43714845b54b935a24b1db80b610fac03882a16e806475dd571c808436bc9010bedae41c72aa2b867d591c8aa7a479dbd529bab5c57611ad2ad85be9b3c04c5ca06e5f91b08bf18cd520364221c56787abd82294226e0d62a7915900f1c28f1486b991b628fbd32ac41c8384f55fc0aa2ebb0ca11e7338eabe092106573e88411786141e1d86a8ffd46c39d9f6212b83f7e3e604ee02f9ffc0cebae094b45e9e22d0e51f7f0f9605464c1ca1ebb2a103ecf08700c47cb63226a455db14f19b719d33b2e5fee2ca1a3c475e403e80c57063dbdfff98b67f22afca4c0eed27b1e46f8f646caa72a74515e15c12298dc42dec76996a89572cf0c37ab899c3261b522ff5a22a79c5625aaaad450554719f4dcb8a207f03b15592e48f0bec7078169f52faa4a5b84cf3df1998d758274fff3d887f7328fdba134054ef9a953802b5f30121fab69f52add52106d841d56d631924f4b9c542e2b15224b2986f4f165dbdda883084fad7c3a171e71a719603b6e17d818ae6b2e01a7f7b9cd426b851d5e99d78b816ce15194efaf53ae0861906d015856f58064ced232159b1562bdb266a8c8b3829b2a744355de20eeaa445e65b912272e79c92dc2322585fe889b85b916d807bc1bb81a373c1fb6db5985aec0f223f03de0af81c7f751d9e36f16e146c2b309cb020f22742bf2568600c81cb020c22e70735436833459a5d42ad65595388b43594187b858a495c551207095c82c1b1854a40d7258ee15dcf594c2fc57c088db9a773bf009e03e1c37a480312948e23ba419366273eeb603fead5a1efd0a7c5ee02960691f70e727a903772a5c8d16ed3d0a7c1b36ee84a6beb13960413d657a0327b6ea54f7de92e53a428038612d2e0ae084fa57cb5b167fab5a65ab749285ce08351e336dda02f729fc1b9c273d294052334c6c9f64fcf22d34b735189bae913615493360c0a0d3a1f3dab9b475ba3fd59ae5173a67b9dfe63c24f0e78fc0e39f822955de2782f13eefbc280781534f7c760d429442eadfe804de6111815ee6f588595d24c4c4285b39ebf97a7158d3970975ae036efc4cf1b35885a55e11df968ce793e37c1cc715d78bdb05c1e62b36b3e3b61bd872dd4e9abb6630cd2692d6211534b158c9b0798fbc7b8ec1f933b45e3ec1fc3fbc32357f24fbb5ee39ee07feee5d4ff2cdefdec94f86b6519e55e191273ff3fa866882c86a03cf8ab21fa1aeb855a453b50a02ade1808d5c974cd2b86c1557ad46f4582754b559e8f20a2bab934137f3cf92733a79950fe306fd6604100357deba832beffae78ccd4c639a0dd014cd0d61ba5a4891fa18c9d8666a5b7731b65b99b8a9cfd6bbcfb0f48fdf490f1e7beec6e649bd5a958f28cc7871d557f86bf91186f753800f3d0f0fefe5bbea94d034c0721f2e6facef6fac97b6a6e357c169e5fceb1911557585dbc53ef42c904132c71e69b307e70c333601bbf66ee5eabbde45d268a2bd1cb53db7a83cb3683d41eb096215c92d3a48dcbc7c2dc5d49b34b65fcbe02ecb37c78eb1785da73e99b02bf5ba63bc4dfbfd873974cf13d87d6b90a82a146afb63db6823fc6b51a610d7f6f69a97efc1c4519c408eee554be91579367aaca8c7d7552c318d0e2ae2e2a39bc3e93ef47348ce8259c48853e634a7e0f23d63ecbae61dd41b9b868bec54877340ead852bc8273c0b8f8704c240dce651d8eb45fa38e759503d3b3989f7e8cad133d161f80d30f8e7a751b04648665517e113700860536253016baee2a04af24a282d8956952ca2f7665da8a3c6b75020b6a573e8f5a58c8602e03590413790f8d266cbb02b66ddbc1d4e42e04b38253b11a4d2b339cd616d753c52f0a6f9a1a374c6ce76cd6e685ce6b2e5961be41d2ae73cbee33dc3bde63db03f0e4836cccf42d0079e82cfab11936033f5ba45ad89a54f440bb928894cf76947b2422669ca70cc00af0226297b9516c75fb9985e319f43b90cc017e5cae5e876d97c358b3ced6c92b6824e3aee7fb03f5ab3111c70d1e1897ce70e39755f0e953668c7bb75c8726359e6f9fe17c9ed337cac9cb308371b65d37cbddb51e1ff8381cfb5538fd7118fcd9460001f8d80ca7818f03e3aa8e169b056a2562a91d2570598468056895e0d8d1b4384fcc05656eac0231e6b67339cc0ec09c05e938309204364f43bd0189a46c6a4c534b9b7e824c513b1457c1ab7313663e4dedf0b9c2ea490f4c0dc35d9baee28ecd57a1222ce55dce6b9fd333cabbc625b96c892bf20e3f8b322970ec4138b7514032e026e0d6b0c2bfa130c190101af56c8d095241c815a2671571b46a5a0994b5f448a8a7a7705c213b0f666168d94d4cc0f8b8a3b3554b9e0d483571cbd2201253a15e5fd0dbdd920fc19260127add82558c0abbea53bc7bcb35bc63d32e6e9adac9dea92bb867cf55ec484f23e41383f3bcd366bce3e3f0d2afc2f1073708485d61bf40aaee59d804d4022876253156236495a85b551157889fe2ba421f55892fab700a5850486641bc2a4d13d834e93d6f7545067997566f916cd0a591343149eaeac9ed50a1ab5bf71870094a3d80e2c0f639ac050b750c7bc6b672fba62bb96bebd5ecbcec5ad25a4eaac718db4ada39cbf5798f7702730fc091074b5efc08200f9d453f3a431be52744b806901e50b73069dd82c6919eec15aa446729c58543a3b285888af396883c22c2eca8eea9126d6a615ee0b4017b1ecc228549bca909f578699c2f9b6b4ea7bfcc526b967cd0259194c4244e3fe41ee1a04714108346691aeb1ee3f54eeed60118abd4541049a86ddb896d9f2391336cbe1a3a735c3e68b34fa1f3003cf5679115b662b4ea63332c023511eec58d783200365ba74bd652e8ab89ab153d3eca5be6ae15bac65697a7d44e1738994207a7c8cdc065a91b981cf35d3c942b01a3583afd65daed79f27c8031864452f730dea2123bba2e99119de39f3d8c5807b014508b981a92d6e99d7e09237d9adba0b7c0d8a0c5dd023f7c109e59159087cea21fddc67111ee0237729989a3cb967ca5a25d213a74659eaa63b53cab29eb15fa231271aa70ac068b06a4eb7407ea88379ee0767a076ac6c006026ba0714ea7779e4e6f91c1a04dcdd4a849cd13d8135bfd428a30a6935b442d3aba239562e19f0d5995fedc2be4ad259206a44d68cf3166fbec7d00befea01be05d0908144ee2cbaafcb2f885c95d20b130918f5a3455faa36c9256e986325157d34bb1fea8126956e1640d66fd308f5900693b22a4383fca4444c713b378d6828011307946b7d7a2d53e4bb7b784a890a8c1a8dff3e817738b8817976e39d3d07c663897e4ad34db69d13dfd43f2f67900ea13d05f86ce39b602f2003cfa20e495803c74161e3acbf18fcd30adca9d22a42ad011a74f9a19980a11522664d99c2df77e5621f688085bc55a537582f7641d5e6b386613eb7487f11bc01a78933d064487cce2a8bf32bd48ca737a8336e75bb3b43a67d17ce045955fcee4a705050f4ad03baabed37a64063d5ac79fa1f3ea11f7423eba360e4baf90d89cade29cc797579bf100e063333c237025c25e20c9818e81d4c278c429b1e858cd9cd50a62c70a7bbd3c650e1c089c1c833375b0c6f7c81ed62ca192230637c1517c8225e690f83e1a011de11a8f5a10695936a0d33d47bb7b8e5e7789acb78ce6997322318831ae88f74df0ba25efb6583efe7d965ff907eca0471cd2062c9f84acc326e08507e0f1f596e59f067e07d8a5ca7b45301d03af8cb98e70f968fda3a3bbba32ad2a3e4e938ab4aaf84e022f8fc342dd29524fbb8c3ef30c9854183740120f092bc3e54d29a3c02714fe5510e2450771ebfe10f52b0a6d877ebf43ab7d8674a14eda68501b9ba436be895a739ca4398e2409b9f6190c16e9b5cfd26f9fc58d703ac942e2db3530b1035af3d4057e0af8e29a83b9000fef05e016e00b0af7a0a422ce04beac079777a099af249ae86827ac0cab003022527c8402fd04e6c7e0d404f4cd48a63ecadfa6c798953eff01a021b0290c5379d1377c38470c127796647814696698467c1faed3e17d914f64588f01313a2c2bfe1cb795406f115efa1a00b30a1f5c536441a14f663fba8dc302bb45d8a34a1d03ad14966aeea5ebb913656b59565516d5aa622af46aeb88bfd080e39b6076dc597d116d1710fe5c16f964b2c43b15ee11a0699c75555ab05600bea618a3d4c1aa445ca48b62a91038ad98df09e7529ed8ba5b3a0179ce84c09736bc934884a30aff117851845f03a61168a7f0ca249cabc3a63e6ce9c164df71509943b4745dc8ee525ba15cab0e8b0d581c83560db2205a82f92a1c05be80f267e96b64c05430656ba137eb68dbc5598786070a1ac44924a68a826608907a51a37831a6513b3e5f9869b55e1c129dd544e51227464d0d70e27fc78601f193582780df56e5ff00ff59843b45185360a90ee7ebae07d77398ecc1a61e8c0fa091f99e1f113bbc6bac43078903a15587f30de8a590b935da4300c52d7d45f832f03b0acf025d754b77b6048093a44254969dc30086ff1cb34d4ac44d22e0a2f802ac08800064612d32b41283ce28ea34c34e410e49ad78d65daf6bafdd879e07dcfaad470eede5b02a1f011e10b805617b206adfc0720d4efbbdae461d48353ffc623cbbe7c6f5faccb883c8b70a6b8d0b91e3b6111cc3adf4f863e0ab1f7a7e38e4f01548052603072425677085d8b1517b81709eb826eac9850e89b9a7cc49511d4137a846ab56e2b400aa2fab79c471b0f347defcb8ff79da0fefe54f81af02f7003fa3ca5d22dc2832baed58057ac675e3f28721b414177f4ec3936d16b748f97bc057149ed8ff3c4b55cfa45ebd889f1a0c74ab342a2271160ae37b6c2c6a460848249282f88cd246800fe9210fa5fa421eeb38d43fa37d43bb513dc79c38b497bf10f832c21e941dc0ad08efc40dbdec51656644a9ea0ac287782bc269118e00cf09fc3d4e249d064e7de8f93567ddfac052f1fe61322952be81338af67d944ac9c008bac5135ac30068f8b23dace496a01318de5b33047644b4f93ac48bcb7c30d4ef17647bf07e07cc12f0f4c37b795ae1eba2a42aa4e7ea9bd247afbfefdc81f7dec3ad375c8bed76c8bb6d665f3dc5a3df3cccf2c2c2d1fb5f7bead755991547f82ece09cf10b28dee741207c87cb891320704d1158b9f38de5f6b64296ae6c1305ee9e3b9237045cc211e48bf77a948530f54613dc6222e01db8741b7788c172ef87e6dcf35993fd8bfff7de9646302aedacbe677b93f17a82a8b2fbec8c9e74ff39abeb6f4b96f3cf5d50bd074576121284be3b6430e39a3c429f1371d214a0b32bfcc2d1a595491c8a9d42d7695fb487784467bcb041fa92f70f4926fa02f7f37e502864ce08c42dfba45971813296d46cdde98f0ab3d4d2c7e346794f00cef834f11eb16f11c108fbda989063a3db89dc5a2aa63027315cba92f7ea8fc56d61b0cfb5c3f7c053f8c9d7bca4b7c98d27d1c1fa511e7f5d69f80e316bfee59cb871d2a68720f60e6ceeabfb24a0e76e0f3fbebced03c790298bfe41c72913f62fc9cc03155760dac5f2c1e9cc8208662dd52f64b2273988abc857ec9471dc2c244f62249a3eba29e20b6a259d476cb6d4655b7bafe3bc0fc25e5908bc119a5700c785621eb67516f5f8553a8ba2f739119bd2ee49bf53dbf825b56a4e5c3fb309d9df7a1bd5088cca3b82d0ed9251759171314bf67e37f03cbfddc77f630965241dc4244510152298d1280ee5d22c27ba2db701fd6110420f25190ba2de8750b5be109f51b422f292097e89beb87802383cc6dab18216e0908138fe096b8221edd2d734c5c0f5098c841b7d80cd4eb0a1bb825d21d590f5a4b0e3cdc86d03f113702726901b914619f5bc9ff07b9d26ff55cf73325c2523aaf7994c1aa48030a4b2c7040c1297e4773c14d03e8b4a1d72b2cbe2f03df088bb2ffc9585923f5c3df015fef0ca0d56384a0051855e2cbac01588993a88887a1a55580128144ee8068b50a304e009fdf17adcdfaa7666585b000fcbe2a372fb4d85d4f61acee09195951b171555854650f9f8ab850cf2a4195e28702b1e79e89db5e679d22ef035fc00d0d8d347549c3c89f0c2e52f8a023d7230abf9b2bf3b38b70beed3df72a1d52e21453a53fca711569a66428a04e9f58bf4570b9e3f63ce28687fe08f8a3f2e6d14b0ec87a3f06bb50619feb900f09fca155faf32d983befbecf3fc8bdc8285b545566ef5afa661d1117426ea195f90fca3806f906f00755dbac2fa9c8aafadbda453683171e81cfe13664fe5abbc778b7ef1cc6d4b8a39642cddf17432d32d4d52353b661945870935630740e7dd66244d9a7e5d65919c34f53330bfc16ab7c00e7a20312ff572afeab419aa68c8d8d618ce1c08103e13351f6e0c18317b47d0fca6f01df52f875abdcdacf98ecfbf12e61d8ab5303130d68a47ec54a0c0c8cea9232103e0420546160a185fb7b937fbf63c02704beb7da56b70b263b0e1c3880aa8e89c88caace0093223279d965974d5f73cd35ff7ddfbe7d4c4f4f17002549c2f1e3c7f9dad7be76ead4a953bf77f2e4c9391c0b2fe16cf27960f6e0c1831bde74bf5e78c475c03b810f2bdc2d70a3baa9dfb01bcd8d178a0365b2e116691783b33a3c88af198d1be46e4b5d77683864c0e30a9f06bef1c1354c82370cc8810307a6815bfd711d7035b0c7183373c515574cdf76db6d6377dc718769369b24fe1f83e197adc6187abd1e4f3ffd34cf3efb0c478fbed05e585898b3d6ce8bc829e0a8aabe2022cf014f1c3c78f0827c96cf7fbd61377097c2cf09bc5f618f4483b1824369bcee80a92515400482fbfb2c0091c3201a4556f83af06981c7f6ad699fbd01400e1c38703bf06155bd5744f6349bcdedd75e73cdf8eedd57982d5bb690a4299b376f616666a6f8a760e08c344d8bfb344d1111daed362fbffc32274f9ec488d0e9743871e2843dfac20bed4ea733afaa2744e4abaafa2511f9fec1830737bc9172adf088db757c23f073c0afe23ad448986cc0e6a69f160e1ce1c96a2df433b705bb9ffb458bbe9cba398ebf52f86d81e736b22377c3801c387000dcaa8ebb50fd77939b26f7cfcc5c3676c3f5d7999b6eba89a9a929e6cf2dd0eff7a9d56a4c4d4d15840f9c11744915a784fbf3e7cf73e2c409daed36aa4ab3d9241b0c387af4283f78ee07767171a9db6eb7bfaeaabf0f3caeaa0b870e1ddae86b5406ffd11983db7ef111e057146e112fcec08151f79fdb101c10993fe2e9116f03b405be0f7c5ee190407ba3dba35f0f2037029f68341affeac6ebaf9f7eef7bdfc35557ed61b9d5e6b5d959b22c2f94f5f8f838699aae207c150855e9ad568b93274f72fefcf9a2fdb146834d9b263975f214fff8ccd33cfdf433cbfd7eff8b22f25f80e72e14c7003ce2be8a748fc287717fcbbe16463f4f1e8caf12019714be2ff015e08bfbdcfccceb0a1b02e4c0810337abeaef6dda34b9ef831ff840fa81f7ff0c4992f2f40f7ec0f2726b5899088d46a3105165ee880108dc12808b455a9224743a1d8e1d3b46bb3dfc089b3186cbb75fc6b6e9ad3cf6d877f99bbffd527f30183c067ce2e0c183ab7e76e9470d8f388eb91d07ca7b70ab6b8abfbc794096811781c30adf1237d17474dfebd89b1e8775013970e0c02ee0734992fcd2fe0ffd7cfa2bbffc119acd26df79fcff71e6ccdc886f11f44399f0559cb096080bf7ed769b175f7c915eaf57b4638ce1b69fb8856dd35bf9abbffe1bfec797fea755d56fabea070f1d3a74c17fabeac5591dc735d3ea00991167b1cde3662897fcb1bc9ed25e2face98778bd71bfaaeeafa5697adffbde4bb3398e0824915f111c3d6b2d793efc776020721ce23fb62549321257be6f341a4c4f4ff3eaabaf166d04b3796262823beeb8ada6c690000003764944415483fffb8d6f9ab9b367ef15915fe222fc33ddcbfe3e8ef017fd6fd3eb398629b0434426ad2a4b4bc309e06bafb98676bbc35224e75595dc7fbb3d266e38c7f171280fa51863b0d6d26ab546da34c6b06be70e66b66d03a0dbedd01ff443f9bd1b7fedb76e580f108beb15edc160307ee8e1ffc5ce9d3bb97cfb76b64d6fe5b65b6fe1e5578e73f2d4ab645946f81b4f9ee7c57fd7d3d4359124490156888bc550b8eff57af47a3ddaed369d4e872c73a2b8d1a873c375d7b16be70ec6c6c6983f778e87ffeecb9c3fbf1c56b2bc74310874a9c39adb118e1c39a27bf7ee9dc77d4ce0bad3a74f9b1ffef0285bb66e61cbe6cd6cdab489cbb75fc6ee5dbb48d204ab616c1b5075ff56cf32f23c2fcef191655941fce5e565ce9d3bc7e2e222dd4e079be7d4ea75b66eddc20dd75dc72d37dfccb6e9adf4fb7d9efdc1737ce10fff2b3f78ee39002b224f02bf71e4c891de6aeff2e312366a65dd057c16b84f554db3d9e4a7ee793777de7107575fb587eddbb753afd7b1d6b2bcdc62716989e5568b4ea7cb603020cb0664798eb58aaa8d748fff6f7a92907a83a0d1a833313ecec4c4045b366f667cbc49afd7636e6e8e23478ff2e4934ff1bdeffd3dcbad16384be6099c95f5d845a2d1250d1b050455bd5d44fe13f02f54753c28d63d57ee66d7ae5ddc70c3f5ecd97d253b765ccef4f434c608b955b241c6201b90671979152046488c03234d136ab51ad65a1617173979ea14c78ebdccb1975fe6d4a953bcf8d2313a9d4e10515de0af803fc00dab5c303fe4cd0cafcb5357d51911b94f557f5d44eec1998306607cbc49b339ce58a3c1e6cd9bb962d72eb64e6f61cb962d6c9bde46b3d9a45eab51abd7a8a529832c6730e8d3eff769b73b2c2e2e323f3fcf99b9394e9f7e8dc5a525ba9d0eed769bae377b55d58a48a6aa4f88c8e781475575ee8d7aea6fa5f0238d651d3870c0e0c4d7bfc48d9eee066644a40ea3f31e23ff992a595355f9ca7325221216519f061e03fe12f8f6851c057e2b853734daeb81d9a5aa7789c82dc035aa7a351e20604a440a4baebcae37fef9a38fb7b8f9f079e094aabe08bc2022cfa9eaf70e1d3a74ec8d3cef8f43b860f321fbf7ef37b83990193c18fed80e5cc1f04bd1e3405d55fb5e0fb4fd7106ff311ffc7c88aace8ac8d2c18307df90f7fbe3142eeae4b6f7f4c3369770c070b52b80f5bac102d9859e317c3bbc1dde0e6f87b7c3450bff1fa1d6e8ef732bff9b0000000049454e44ae426082,100,100,1296,1282962729);
INSERT INTO `$prefix$flip_content_image` VALUES (7,CURRENT_TIMESTAMP,1,'shop_category_drink_alcohol','Bier, Schnaps, Wein...','Alkoholische Getränke',0,2,0x89504e470d0a1a0a0000000d4948445200000080000000800806000000c33e61cb000000017352474200aece1ce900000006624b4744000000000000f943bb7f000000097048597300000f6100000f6101a83fa7690000000774494d4507da081c02250cb70f7ead0000001d69545874436f6d6d656e7400000000004372656174656420776974682047494d50642e6507000020004944415478daedbd79b467d755dff9d9e7dc7b7fe39b5f55a9542acdb3644b48c60cb6b18db1598426183036c6904e4343032b895768e818d2c40ec9c2901808590d38e9069a7463063b8d57d2d8386d1bc7f18465599654525549a5a10655a9eabd7af3fb0df7de7376ff71ee6f7aeff7a652bd5249aadf5ab5deabf79bee3dfb7bf6f4dd7b1f99f739571eafdc87b9b204570070e5710500571e5b3d442c22f665775fd115d16e2ef4cec3e70f2b8089ee91cedf54dd1500bc9c059f671f56d7fc15a00a5a2a90d056b481adfc2a51e567051455ffd2bdd72b51c05ae11bbc3faa79fbfd787f08e34ba8283840000f2282fa0cdc29928987107bbdbc54b5c11500ac758ac4d2589d54e1208845bc22807a40fb5ee80988c84f934c7e19b1377441b0d657b89cc17105006b547fdafa01f5fed962bb2be2190e002db441018278f2734874bb00b8e5df544c1555401b4423fff365ab21ae00a04ff8de1fd6b4fd7d888ea16800800a78874a039c74b6342255c4595414e33c4477118dfc06f9d22fa0e9a321c052503cc6de4abce7e3b2d9f7bf580079450140b09d5fba8bdeafaed3f4ddeaf53038032288f778695373df438d1f46a55dbc3d61d5fd29abee6308158cd64115b40154c3f7f82e8410b740bcef4bf8d53f038233295a2814f598ca1b91d2ab5e142df18a0180c192f2db8ad440732c6fc4ca5dd272ef50977f864a728276fb6e44a651e70107ea88b89171ff0b78590912d33626be0193dc845045d3d3aca4ff96dc3d49ee8f0f9a0701ef0960ca1741aae0831d31be0300c03790e41ea2031fbfe42078450040b03479a72a47414c10822c12f9b7e1f86a50e52c15aa7f99d8df42c9be15c190b71f0cbb1f454c155bbe0fb4dd0dfd442c680ed90ae756df8c654f376ae80780711e15e9024014a400812068be4874e03f5d724d10bd1284ef784c3d8710eac1a11305a671f200e223d428e247f132c7a4fbd794e23717af51a4fa3fe0dd0caef5554c7c27680b551db0dd2206e21ab5e4efd14cff13a843c950afc197f012a248ef8b6bb0c16474fd490553c5373e8b2dbdea4a22e862085d717d11db8340a5bbdcc1bf0f9a4fc5812a4a4e5d7f82586e41c5759d329555442a44d5ef02b201e1f740a088c4d44a3f433bfb1cb5e8ef9344f721d13448099f9f05891029b3daf8239a8dff1b311388d36e6e019a9891770e8491974213bc6c4c40c7c113a0c507b5cc2f8916ff5fe2805a0e06832b1ecf7946d31fc5d34654113341ca3788fcb58cc8cf2076ac27e8ae1edf2a8a10d4b7d0f434aa2d6cf55ed014087ff7f90cea5b882438778a85c57f84d5f1c2093001b4d9f90050b78a99fe9798c99fdbf54ce3cb020082c1f184b6f93572be88610a9847f144bc15c7573024a828b1dfcb74f3577032d7f5c224ba061bdd82cf9f43a2f1c19dd70180c880dade10046e39fc6e47ba201211c0e2f373052423f2f643a4ad2f80429e3f419e1fed99070f6806d949ec0d5f464a77ed9a5ff0b20080c5b2c0d52a1c2cd43f8838444d50f522088a6781bdd9ef62fdbe9e0706881941cc68579daf916a11e2e936f30932fc73fa9e0b1758c2b58e61cbb7835bc267e7682cff21ade65f216604e3825932e33f8399fa87bb0680e8e5a0fa97f811355c0b185414c70255772f2dfb08564788fc7e4afe5692fc0ee2f85bc0a46badf8508175923edbd9fdfdfec0a6cf4901006d614bd7a27e053116533a48d5fc346068b7fe3f201ec859bc629cc0ad38f7b53b4100c7d7314c8378627f35075abf839765ac4ef24cf52dec6ffe5b9c9cc5266f4675f572416ed7b607d06498e41aca95bf439e3d8a6716f22632fac3dd75d90d2d602e27c18b58f2f4c3da5edaa3edc5ebb4bd50fc9b3fa8d9f2bbd5e7877538406c772727fe7a9ccce0592593e35cd7fa343e4e107b0068bdd08bbc68c26798a2d03626d9cbe8f8bf44fd1262c7704fdf8f3bf94ed5f646f7fe32f001421efe88e6edf7a3ee104229ec8ac2f9159510aee52b48f46a92b1bfecda448365813b5498c4cb59ae4b3f8b46a374df4cc1e22817ee4defd00fd81600fcb0af31b8f671349f6765f977f0e949440cea73c8cf62afff1ba47cf745f507cce522fcb4f99d787f0435a5c2eef6a9ca4ea82463a83b46b6f0f6f5bb411ce3ee3d104d0139aa59f1cfa1f817164a75fc805d7ea87a6cf916549bc4f1ed802f1c5a0b760a5df9ab97a709c8d30f80d9b7fe7264f0a71a058951f714d97c0102011541b216a5c60dacacfe2e7976149168f8cebbbc3cd80d9fd0c03517a49143aadf8699fe27a29abf7c002062c9f30fabd743889a6297296a24e4db59457515652510f25a506812a3ee495ce3dfa97727b47636a57c1e961b1fa2b1fa1f387ffe8771f971444cf02d881113879f12fe5d8e7e403001c70278555182f61222b4f105dc89bfab43817d39fb0082dd907e0568b6f6a8e1da507051a469bd5f6442ff1991dc087884122bfe4fc8fd1364ee284805e36320039f21660475da17ad092e3bce55d7cc91b5fe1b69fbcbc1847419b826b5c95f41ddccf64dc32ef801e205c4f61c0289c91b0f816f90674f93bb6771ed2338f75c71ed6dccd84f62f6fce245f303761500064b2abfad68156890c83f16a7dfd0ccfd3a684a127d9876fb0d0875d4bbb010aa54781b55ffdda8643d0a36b9191bdf0a3ea3d5fa38ce3f4d23fb338472f011fad8354582eacc67813242129eefc84e059f9f66e2eabf224a6ede1908bcbf48da4f50d7c4b59fc444d3a0e0dd3cea5600090413829149e6cfbd0363a650cd3093ff0033f973170d00d16e0abfc93bd4f364e1c9791cdf50e3cb783902ded0cade8061042f2b24722f25fb3a9aee93d4b3efc7c972109a2963cbaf076de3fd022242a9fa7d687a96b6fb32ce3fdfcbaef5396d2a823193a880385053fc2c54af89ae6175ee438cefffe31727372065f2c6e741cab8f46471fd41f0aa45fe5fc0fb392af51fa3d5f87f8b375edc5c40b45b6a3fe57f57c753184d8293a68ae7d1205489836d93119ccc32e93f4429f93e94156af627d024435a5f0b49ded2bda86f0e66d3b485c4534cca1f7276f99b4312c878c2a648e9384a5e1521462975b384a2010482216bff2d4802acee74fb5eb029109120fcd507402a140bd205f070b6310b9bc83790ea9b5e1a99c0361f4038d843ac14295729506c3c5e3326f5df10dbbb501603f26515bcc796ee43dd2c906ef00d0e6f955afce3a4f93748cc5d60cb24a53712c94da86f8058b2fc108dc61fe3f213c1de160bac4529069260ec24ea973a39dacdd3c31b083fa86c1342b72126a54308a95f256ffcb7609a4406bf635882484ae4d9e1e00b555f87942f6ec148f442777a7f34a303d14dadf85b581427818255daa1a8923116cdef10ebcd889d2a50de5b64d57490961d92571789a8d57e8172e36fc154b1d5fb41db218cca537c3e4b640e3236fa9b2c2efe439c3b5d5c9d41d420d13433c70ea2da6264eafd4495fbb07602d566e0ebcd280361d7105e200836c667cfa26e0eb1e398e46658c328aa5bc4b79f41fd32984aa819d42da2022cce9d20cf8f21452da16e9248bca44e60a8b4395a50b05f2ac09013f1ed94f84d1abc1e6102118ff513ec69fe6a1f05eb882a6fa1edbf42948f63e283c329d86da954834f9fc1946e437d6be0efea5ba85f0d36df4eb33cff4b1846c9b247c3a24a093409807019ea5b44c90dd4f7be0ff50dc4d4b0c9fd844210dff3de3b8e0431ea1783f0fd6adf2ee8074d7fc46d109190def11d6de2e906fcbeffbe629c7b9ee5c5f7636402f1e0a394fab37f8fd61bdf27bc98518060f09cd015de84b017b048b7d2c601ab88d48bffcd704de33fe3a3bc48c95a7c7698a8f2d66e9a769dcadca1b7bd11513240bf4200443e8b2dbf0adf3e417bf513b45a9fc0e5cf21d8b0c86e91896bff18f52ba87ac4d41119e9ed6ab1f8f69320093e3d06127705ab3078dd9d946fdf9615c077123c5d206981818ee73f41a3f517b49a1fc7e8189dfe04578fd9f3db0f32ffeb2af262460182b0ccebb05c5b44558a6315830f95b2c4c4fe1aa0c954fe01285d8321eb6e09131d4435bd68ced6463651fb53ca454421c941bc9b43e211ca933f41bc7c07cbcbbf89cf4f875ddac7e7072db28ab28a6f9c1a5c3655c4142168bf4fb19d848f76a211ed23c2ea64f9e3b8fc191aad3fc130813163e00abfc908d507e7d1fac5f5d5a20b51fd398fa930dd75ea229d625ff37700cf62f251c032927d2faa0d6ce975a836d77ccac513fed6e8e80921f813da13b05fc5546ea392bf9de5a5dfc030425cb9079132228d1eb004d0688d50a52bfc8d9346eb01d0ff274130668a46fae7349a1fc3480df111d64cf70a4a8b17fbb2a5f6d042c892bed8a9e09c2f00098ae059625feb57c8e50cb99c6324fd6e62b70fcf2212ed07da9731f9a288ad63cc5594ab6f0751f2f6139c7ff607682fff178c1dbbb054f136802b589cce32bbfc83b4d24f63cd1448a917a9146602117c2da2faf022be6c914c5f5c0d101e3dfebdc4ab505b278aded40df9aabcb6706efc0b2f62b8589a60a3304b336cf9062ad11edaab1fc730868da6692dff3559f361eafb7e19d5d6c6ef0744cb3df249a4e02fdcc6df2d16e7cfb1d07a3fd6ee43bc5fa74d04502b9865c7e447cfa2b147483975fffbe827bb5fd430d0c922d764bf8746aeb748c3e2c2cb55f81de64d1dea57b1c99df8f424884134264f8f85a495b6d6bf5f40d46298625efe29b97b3a249bb44dcdbc839abc03cf6ae02ba457612c94c8fd09165b1fc09a896ea348e00642c6122ff88a5079ac49e5d00aae6eb1ed8ce6d82d3c77ef3f65ecc5d70021bf9af8ebc056505ddad0eebe00fdbc2b7ec0c68f8c38be93767a3c08124f5cb90fd4231af5bd5f118d116aac988fd1d0bfc0308531a3a8578cd469fa4fd3d08f137307b1dc462cb78342a647c8dd1132770c6b26f05ec148e80f30820a6822948fb4a93ed6c4c58a26609bcb3446efe1f1eff93cf1f25340fd45068008b1bf9abded7f8c969abcb41f1ac24862b2f49162f70362c89a8fd0ccff0b3eceba4c2518520e937118a186d169a42f6b181c3783618cdc9f22e724adfc93c557d9e2b96a37f4c3808f84d2539e68c1119fcbc1387c1924cf698cdecbc2b7bc9bdad4090e34ff3dcfcb77bf783e806069f17b9af31013f9f74272cb60d26347aaf7c53105818e0eb9874e5edeb59e44cc28517c27adf4718c8c2024185b273df571d2db4a48aebd648d1a0ca3459859fccd3030450429ae1b09ce1d80094257a398652159023b0fd17c51b6e6154d3ca6dd6065f43b60b44a76e73dec69ff35e486066fb9e8f0df2100a0cd6f87685ffe5754ca9b53a9979df00d2e3d1ea05cba19cdcfe3d243202554db942b6fa552fd215aab1fa3ddfc0ca820260aa1572488d7e0ffbaa2bfb068015f17fe4b78deb462ec6ac819e015bb6430cb067ce83c16ef02031ee5983443d236ab236fc2bfea46aaa3cfa04e495a0fe329217299f40578314ce43f8263a950652fc4ee5e3a3f4030a836f1e9099018df3e1e3a85b15d5226d40fce53aebd9356e33f23524763c7d817af86c8e1a362a78ba2b623e84e24505ca32f7ef7458fb877213be87c91086d8378c481718b90c2f983ff9cf2c851fce83e2af99790ec183e2b15d945bd8899ff8be203147a4e97513f8298b1e0e9221b6b03d9d57bd8d6ce57df245f7d103149b814136f4cc11684522769a5510a91a2d62246d1750090b0a33b216f27e1d4a1ae7d3b00236fb0b8f75f806b90f8875839f8e38ceb1f339a7e06d5049baf502eff145af2648d4fe0ddc2aeafcd8e00a040ac6f622efa3346da6fc5b9439067187b3390163fb3c1fcfe36841fa8d42dcab6876882107b77f8848d5ab13a14ec8388947ad9c0cdae4b4a85d3d7c447ff8cf65e4fd47800dbfc1a5803a6e03d4c07269e3c790d79e9fe820bf0e02ba03959f97eb291118c7906b44245fe00f131a86534fb23bc5a8c5650dfa05cfe31bcce232a187b15decdb3db95acd1cedff04de43c48db3c49acfb03b9e34e008277a710190fe4497453d01692776fa25fc041e81adaa7b363c1478a6f2e862fe8a6c20f824d70adaf21760f624711d3e9b4f5dd9cfe20055b42fdd676498869363e8a4815b4851b9b204bcab4e47a54ded39587d7fe9265093b5dd3c2998b91da5f92e4d38ca515e2fc004be6bfa2ba88bad160fb55511323ae33682221cbbe4a14dd515cbfd9387b7931b5e34ed9402396396ed3b2bf8bbdd92fe2fb7200428c6884a789d2023c26ea70e3aed0108ed0297b8cc0a31feb79c9dac296be193195416d50541475e226f5cb0505dbe81bdfe543cc1e4d87366b2962acb5146c77d8df10a6ae4bc1fe73ac8e85caa16c9ec66d3f87642b015446baa5e8da89e38a0f3184ba46a31e55a8a63752496f0671385d62d5fe0de24d182aa51a5ad39d2ffeef11d7228ebf09cd4ee2f399a2722af814860516cd7fc762f4bdecbb6a52c4d8015ee3d200402cf199277559de43769525f60728fb5b8bd26543db1ca5651e614ffaf324ee86e02c754d8140f7bb3ca1f9518b0ede5e1bb56a8a89aec3c437f6155508eae651b788fac5a27aa7e7bca1fdef0f93bdfa53ab1d0a7680832fb48a140234668246f32f68353e8e91b1c27e0bd15c1bb37090a537bf01d4075018c18b1474ee1a0048070039b5f6ad94b350d9ec5862d57e067116a30e5530de0f00c0f8e0881a0fe23bfca292ca01662abf4ac9ac90440e232684b1d622c652ae552534bfe8ee9b007be6ab4c9c9ce2f9ef5f2433a7c9cca95e420541a8507677778b3a076c9844bdaacca2166e70dc8a2292e0f3e7f0d98935bacf74d5ad488476ec7e9f79e8d0bf1b2d447fd9b860105327cf1e2777cfd068fc0946263032d655b35a36818429d9e000e6c37c14dfc78d0cf26caba5435d87b0290f11b912880b555243ea1db4abb5fcc0e7b7cc9d589d0ffacc582263702a38e771694e73655547a6c6c54676c720d87914600ca5e76392a5887cd4215a245610a4dbcd52425845fb7af07b6161d8752a9b71f86188c29a1ab3ee67687f75c5367310dd62500563a669b4ff9c46eb63186a881614ac6a9817e40211537d6811bb90132f3f86122364ddef1436a90128d0667c89667c18f18a71e5408e15e3e786095f36109e11a5d16ab194a7b4d30c45a9944a4c8cd4898cc158cbdccc9c4eec9990288a7604821dd3c1266fe14acafe8f4c132dd96e1cdcfbc01ae7cabf4666ce60750443bdf74feb589d20b54f6375b400cf90355cbbaeba0d67b89f3ede80831fa060f34f63650a4c290c9528081b2984e12b96683645c838f9adff9ac82f74245568880d2e4a3b3bb9d8166abacb1c3a9ed6de980fca4d87b36722b0d252965a316d673026a8b134cb383d7b9e34cfb10508ce3e774eed0e45ba231f404498cb8ddef0efbf15377a1576d570e6ddb3e4a379b74e2e2ca0474981bc58a8decd85db8f8125ead90f3292be03cff92080b545927ec8ff3ba5539220850253cd8b56323ffcbd80912a993fc142eb03447e3cc8a118d2d419078b071f1ba2859cb14f9cc3278668e5798ebced738cddf729bc2f853bb48a88e0b48800b05d1f40518c144e9d775d870f2d268a7ac5f890208a749448f7e1fd697c7e1ea352f8023e1044801a839484e4cc33cc666fe1f4e48ff2aa9baee5f1678e93a6198d768bb15a9df1913a599651a957191d1b91ed6a811d6b80b8b1ccd3dff5616cf33c79dd73d55f4c3172a8462eae4f550b464b88d6315aebfb592b7e96b03a4923fe2ca76bef2237b318ad0edfe5b2967b8fc9e5344bfa5bccba9f64d6fd244bfeb7c8398d916ad10b680672054626586c7f90a5d607b13211e277d570f7da53c1ae6e19f9fc02239f9bc7d723a2e6f31cfebeff4a63f26ef0ae0fcc1d46b4f7833eba68c38befdbf99671aae6bb88e5162ad1db3066a2bf60008d63b456233afa04d53ff808d1a7be0233cb80d0066eb9e13a4a4982358613676748e2b019cecfceb1132d7001994025adefe7d937ff1ed77ff67fc2d5a61879a4cad857ea2c7cdb324b772d63bab1f1a0b20b3e6d87350baf8a742f73e5df20f607996efe2a5e9651b2a2bba7a0623b14acd458918fd2f41f0fb90646c3c06eff1c8b7c88d8dd1ce857132858216635ff08b97f1ae7cf22522dc2aa3e0145e063a172b84df5500357028d9478e5791eff3b0f908e5c85e4f91ab10a9e8cc84d23406a17301a0dab34186edf4589b80a25682da58535fb717e1ee212ccce101f3a843d73168c456b35c4650378cad471eb0dd7f2a5af3f4c12479c38738ee9f151acb5345a2d2d95936d69810b2b0af51e5f154ebcfd834c7df91354971e22af9719395465eceb75dad7b469efcd58bc6bb958aea2024ed71afa8efb58c5c92c672a3f46e2ef2071b790e86d24fe1e56ecff85f80aa91c21d3c710a96308c39cbb66430443955c4e91fb9334dd27c3f44e0d45aaa8f4ecbc82c66125eda2527a322399cd513cbe0292678182fdb6f770f5d89ff274febee0dc0ef0212dc61b6f22f2a13423b7cb9caf7c01bb06048a204686560ae7e6794afe4ed4a4080979b248f4d4b324870e05a12b68b98c6479d7795dfb48f15c73e02a9e3e719a674f9fe5aaa909448446a349a99cec5e3d808b47b8e96b3fc5ca2d7790dd9533dbfe01469ffa2cd1f20abe1411cd4544f311f547abb4af4e49f7a7480ecb772c170198768bc8830d0ebd7cd68f919b53e47a9c55f96bc4a74180c58088aee08df60ff9eaf1f09da60f3140a91b751083ba40c19a2588e695682e246dc4834f14db586579ec8d8182bdeb1ef6b43f89d1b4886e06d04fad7517d6d5f1260755acaf3392de412339da4b2a759c413fe08814ec5f82a3c18af912d52333a47686d2a39f47a2125aa984ec60eec27d1b83ba8d23a6ebf6ede3c8b1e3186330c68495b2bb6a02c272fba8828a05e789ec611a77de884611e59367289d7a0e9f587c59881722a2050b06aa47aba881f4aa946c4f1a3483179a372d855472a7f0c69ad0b0e10bc2c60473a279c1c4993ef3ead7b8fa5e914644b41c15a0f044272344145c8e381f0017e5e00da6b5c4ead877e2efbe81ead8b30314ac916c4d12a17bf7ac3d3d22f4f29bf03d086a4c481c598b4aa011e3271f0589b073e7b0e76710e77108c67bb45c433a82d6fec86673359ee2b9e5c6eb78fce8d31c3b7986fd7bc639777696c9b131f26d34925e784da002ea48aaf790d4bf89f6f257f0d92cad3d23b4ae790dd1ca32d26c503e711c2213162251d478a2c508bb544cf7b0503a5e09bd8fb6970617139eeb8ba2685fbd845d2d611a2590904aede694540bcab5f0ba9d0bbfe7a0718a7a174eff708b982c65f6ba7fc554fb7f63fea6f732d2fe1bc84fe0b318d6442dc113b7544e3fc5cac1d7a03852b348d5d541d2c21134683242e5f927c14421c5ab8a7a4f3c3f4334370b5ef1a61854ed3c24099a2be4ae68565dd32328dba55085340b40b5c5463122db265f2fd00750f2897a370453b74252bd3b1cb12225daab7f8b4c5568b51fa575603f58530041a1759078e12b609ba82d814468e4bae5511ac545b626085fad2012ec7d72be1eeae6a240498bf7ddee1af1594ff8bea0615d06ce9156bf9576f5f524ee6bac5efb6e4afe6146d34f92eadd8ce50bc4e59f256b7f19973e5e1481ea9a24a4a174ee14f1ccf942032979e55398da1dc1f8647394971e07532f6a087da03c0a40fa24091545eafa84bbc1be32206efb748f57cf1dd75ec3e1279eb954f50080f764e3f53546b8b8593292eaab11496836bf8ee406c9a17de04080b67534a3d7e24bef205afc22d1ca02d85b831a8daa24a73e005115e9eeec0ea7a380432d186fbb451861ec7a93c6f82f824b412daa6db2d277a093a7501793f028897e19d19811f7278803af35c43789caf7e3fd0251f26ad4cde1dd0c1bc5a33e4a103cde0a92b7c9971e2c32da82c42341be1d16cf684140141140873fde402d0bc54c830bf1c95e00697ce155c1fd6d57eb22dfc2c12a149b86b92fc5406583e482751fc59723dad51c350fa256c0b469efff433cad5e8b56e7434d064b6f08f8ab7f11f5415374949db8654cf424b1394b2dbd974c3e455b9fc09818718a6ab5d89dd2ad21e8d2d2628aa9de271053db3a95dac92188ed23183649571ad930d3d7092bd548311e76834fd9a5b280dd990fa08369806e924ffa5f60516d50e13e56f4eb185f2b3cf7ff18d4bf84104a450a7adfe0263f517c48b55893827e25a85d4fcad8f2bbf0ac6099c24895963c5e54e2baceb8c0de65488956fb9358b90acd9e03a976b90851c5e852b7b24bf279c44660a2a22154b7bf59367bad6c2ddddd6c92baf80058d3094ba7d9611d879632617f042f2becd1db39a77f8a956ae1f975ba27855ea2dc205a5dc3c17732723d162db56788dcd87a22c00fee242956d6fb05d49f0f146c91a812f5647235a7ab7fc07e7b9e24b22ced3f0c2b5fc7340e515ef810446360b7a8893402f996e5509b00249885ddac0932bbb2fb3bebbe0589a3a4846168cbd4f8a65e2e7fd82651dd7227885a9af1115ad193b4ed311af611c40c9eced14da8144d968214d537bd2ca590321ffd20919e0f6f1583752b68e916daa33fc8dc758fb038fdfb5837b3a906dc5c4bc8e6af298a4ed6b16d973d00b6a926440db9ce632861a8b3ca43ac77818a3de9d706666b154e4f1b3899a3193d41d31e46a43cf44d2ab28556ce49ed1dac361bac36db1c3b79865333e7995f5ec61ac5b8795c7490d989ff13eb67764e5dca36b4e805ecfb8e99f53b281bbbf4d3c2bbf59811abee8bc55c208f916ab7aaa01b2a15ad539b6ffd214d7b14f3f786f1ee862ed33640e1162d5a1dcd7d6ebe49640dd6af1259a191c5b4d3559aad94b15a95388e70f61ace8efc1fec5d7e2f48b9d736ae59a87ef2b206d485a7bfadfbd93ea76f01e795f1911ade2b7bf74de3b6d93e6a2e588ac6ec74d377ebdbb4cf113492608a397e5dcda89df4709fd7bde9cedfc26b2a76948a2baa7e2602e1d40545afaa48805ceadc3ef1fbdcb9f8cb449529a2d21477aefc1c999926cd339e3a7d86462b4570b4ec9d585d0ad722806f61a75e47b4ffedd8e9d7a3bebd4e2f6c3457200c4bddd9ce37623872e239ac358c8f14739976305de58200a0d65079fa0cbe1c83b5dbf898ce7169db508faadb78990e54170c2ea2ac57082aa8f1247a0715793315fb26caf15b074080081a45f85a0d7bf449a6ffe0d718fbd47fc0c7a130b3a467b8bdf95e329926b296536767b1c6607589f3e59f460af2c9d4ef426c1df56d241ac18cbd2ad4ef6da9166543b06f663b0cc2d2ca0a63f51abe10bcf7babb00c0189293e799f8cc37281f3b04cd47515b472546d79633ebda9bdad8e911bf3eefde972641c95149c3099e3274defa7af2a6f046959c32f71653ca328c990ec9276b208a91e565e22f7d91ca473e427ce408be5665d9a286000013504944415487a8aff2164bc99fe18ef67b715263a5d16269b581e0988dfe474c77d6e01a9e40b6568dba4524d06b4059db1721ac341bcc2d2c71edfebd78afe479cedea9c96d17845cb80f20a16801771e1a4f62561f4193fd904ca3d5d7168519044088df72118cd79061ebec8281122f2576e354b25b89f369440c2ba5a334e2a786acdbfa813c8a41bcc17116c348c82b10e32b5592c79ec0cece22333381a72c9791cc316c04838aa5aacf30c21334cd5e561a4d26c74610da186de11845d319a8df81f81c6dcfe1171f2d8653eb0efdc1224a3112ce2c1ef2f618c313cf9d61ac5e65a45aa1d96e33353d49be83f11117c1090c4e9a4a05c9e7c1cf21cd4360213ebb04a2346fb82e6402d51580284ab2bb8e1e7d7e81ac77ef54b16e8cc88de12505a3d4dbb7b1121feefa0f3aac87503a730a43b5f172fc392af63ee2c3dfc0d9f3d41efd3c4449a8f68d6334ef53d5b251dab5c2f5f2e79c955fe0d8c9337cfbd438365fe1f9e41f309dfe299a2f939ffe7f7a24952d054e60a8cd17d6d5c68a0c988561d6508092588e3e7382e595550eeedd8bf31e973b26c6c624dfc154968b1705682767135aae253224678e4012919c3b03385ad75f5fd8434ff3ba9b027f6024f0bc85e7df55757d4ea3978c91d6bde4d17237abe4a5cd68fb1e564b8f1596b0a8f59730121663d028269e9bc1cecd2262488e3e0afe53788910e7d04ab528f5f66b5d860d6da3228c9bc77194b0c674632fd3372145a242e8aa30a494bc93fac5e9701f686d82485d20bb7c388f76767e9e9327cf3052ab72fdfefd2451443bcbd87760afb81d0e8f79e100e84f54ac856b87968c22d446944e3f878f02b2a3995345422fa77ded2da133d712c071fdaddd260c35a10074b97e986aebe66ef5ae3175164a9f26a242dc9c21599c29267f29784f343f4b34771edf3902de397cb9120a337357c87cfd01542ab269d4196a494b4cdac738c5d4bad76e56de3d54d0c34cab91bee3723ccdbdafa53df5cdd8fddfc2d46895d66a93eb0fec238962b2dce1bd278a23e228c6eba506c076e355d562771624918dc0869fc9f3c77b451e56289f3b119e2b5840358a4a1bddf306249a083380cf7f96bded46cf97d54038a1845da71e4d12d4b950612306b6589c4d830fed4f63c74c468ff19cbc9985e555e2b8bccd048861ab099feb00a48ee69e6f61f19e9f27d68c5204b5b111bc2ace79bcf78c4c8e8ab5d18e857f691341ebb443e7efbec829682ffd1fc561748a11c406006012dcc2d7bad53886188d4aa10c5bb55be2dd0ba9fa3a868c043f74b3ec5fd7fc6c1d8085913011466061a5c1dec96d00c0f44e0eef453a1ef559d10626450e600367d1a585336d8b59068a8d2ce5919a186b2f48f81737152c17f086ac811dbf0bcd570bfbbd06287d644298bd63435fbf29f5accf76b8962d252a3b9a5fa018a693c338e26ddeb60cd588529ac01e780bf6e05b90f2d4f09b114bedf467c9ea1310573102b69450aa57a53c52bba076b0dd01c056c99bb5cfb936d1adefc6d40e10dff81e346fac7fd126e1f456b67a5b69c9fea2feed3c3a9c841aa6e3c3788db6b7f387ae578e9dbc1b298d43691caa7b86fb056248160f73fbbf1192cfff32c767962895ca626c4f135c1e64d056acd5b0ebf469b801d7c4540ff43579f624aeb0ae0278c7fec7d054f626a9e42d3f59bb7d0d6ea3b4ebb6b48a0c721743dea05284d995a0016ce158eb459a9bb47b3e800ef07943559bb6e691ca249a9e475be7202a12263b49a40dfb5ec3c6c3cb8a9c81d92a3bb7d94cdec27ee7ce73f33507586a643bc7a68970f38730f114e2053df7509130f2bd75bb04c7dced0e00a4876e5dd3d1607cc7bb8f70a7bf801a079180dda2ca663b0da26b1dad619a5f585f19b45d9fa0e393a8272e4da2789cf73d53b4c189a0ebb023a1915e5b73f8d599908b884a452f40f122b9344395cc6e2a8061f7a0fd2915b1604bdd73f23615b86e1e622a39eaf38d2778771d42d9e48265f30a1dede0ccf1f0dc77114b6b50ed5c80232cddcce81a9fe4120dd57a617d01dec3587dcb994bdb56db3b36efc5b746a3d8faeda1f67ee1109a2e0d771a2f5214a32a246699f1913a5e1525a1eabff1c2f693b09ec8baec35802a3a3ed2c7f16fec395fdc10b20f253ec54c7f0712d5916804bbef3bc1a76c99d41fc8c96fff221428cb1c8fb7fe3e63f504f51e4f89aa7f78f87276c0ad9bfb24b2053abd91cb0f00d2a17af54285291bef7859e3fdcb5659ba2224f2aea78e8c6c7a41b2d35d278221e3987f0fd6cd70c381abf02a54fc23c5d174ba6360f7fb249be6295e323e806e47d61a8e4dede700758bcfd9e8734d8c5f7e0c4c193125349d0bb7b5c58ee978d99b9aae353e88684e53aee704ef62b49a903b87972a53e91fe13b93cefa05bb85e0c46ce19374857f594d0add3900360c052f46820983369e265f391a22000f62e2cd0f9d2a66f26fee9cadd576192d7b1da7a37751d306375e7b2d8d7646c93d4ec91f468b63f2baf39244370f6ab6e393c8ee8f58bdf445a15b65daec7a1f6feb2a5a1b0a2754434b56ee2fdcdfe88469ea8a0653c1d0e66ce9ef72a2f433546c839ba62748f30c2f534cb6de877667266f328872274ee60efb035f1a00d8880cdaf67b642b4faa7316dbd651c3469f514463463dedc9bb496fff76ea5198ead9dcf3b35c9b401c4d90bb1c6fa7983effdf63fd2954a2ad29e02e3636e90f142eb83ff0a501800d84bb3516b64a8c6c43551ad9fcdc65e9398cb63dc7d33ffa3754b3456a9213976b44c6907b8ff3e0ed24e3cf07e17bb3cd25dc4e7fa02846b7a7f42ee344d0ce2e7dc054ca05680ad91a79c1de764f8f0ce1abf64d9690f587df9a3c477cd655288a414d1d9b3ecdd8999fc6e6a7c2e0cb6df6f0af7bb8762819f7f9f6fa035f121a40b730da3a347adb3c21b42db5baf5d3e1bb1c76e22ec853442d7af621d418cc1a87506d4275e6ab6407be9d389dc51883b49fa676f67731ed431055f03662cb310c43fb03158947890ffe10e439fefc43f8b9235bfa2497bf0918267bdd08c6b2c193ba2110c4f7fe26aadb2abd5ab76c3ec74cdc85664d0c116ef939589d59e737b87884eb3ef136646995077e6a0996325e7df6c7c8927da8a987fa43dd2e38755df24c2a5741de445d8e99ba0f3ff308906c901dec9c557cb99b806d462b32cc0ef6e50486add980387d1a5aaf7483b108dbd1221b56cf4817603e19814a844183f9b635063b92f5c26257117cebf9c08198186d3e5f50c1c32e65f73d815d720275c0b9194800e985c6b60a768478f20d80c7ad1c81c6d3eba5bd614bb68044b8f987b113f7e1cf3e00cd736bdedf5742e62e3c8cdcaa5449b325f2b39f81ccc3ead950e5e4d7fa24bb7b54cce597079021398168100092ec01df42d563475e45be7ab47bba47df89cc1b7cbc82b5f8c527d0730f87c19326ee965af7407281bb4efdf6ad02826f2f60721784df37064e8dacf3492edf28405f808b2a3b4df808da9e0153414c825bee74dcf40b6e1bfdf862105b5e5377af3bbe8f5eb393806f63eab762aa3722f104dbea021aa6e2b7ea0fdc0567f0e268806176b7bf7bd85f0c6f43205f263bfb1f31a5fdf86c16831d1803bf658e6003c6458dac1fd3bf839d6f6b7762cad70572d23d89729e819ab68d733f033900d94c7b19d9b2a4fc457302d72dbe1878f664d15cc9507fa0fbff62ba1885b73bc0fef9b5f13c88a9e0b3f96e0cbe55eddee6a490f676e20b32b7ae8b7429ef1f14d486b581dbdfcd2a6056ceb37cd59b2e3a087627eba8c5fc005d83f27ee7d00bea33ccf8add8d19b211e1d547dba95d0b6a1318c6c49c86cdb4ce9067e0a82cfe6d17c0975cbf895c3019c9df231d9d0a0159d4fb2b1aa5745d24564fc26667e495919bdeda20360778644ad593c1daa9f15294f61c66e0eedda1377919df8d8fa78f842f2a2b2cd8601ddc291df966b2268b640bef895d09686ed0ea0dadca66c52e0a00ed24574fab5b8d77e90d5c97b38f1e453443622a9562e9c59bd241a4087ece09d5caf1f6ebe5f5044b14e3bc9a68e9acaf6fb05ba4320252a5ae265fb99bb7583361db4cee2c7ef22fdde4fa3dfff098efb7d9c7aee38711493e739d3d393171500bb1206f6efa08d77b3a0adf3f8852790da3edcec57c0bcc0dd2f9b5405778a3eb740d5665f296b46dea9ec341f5698c6ee50e82cbca13d0393df818edf8fdef3bf603ccccf2f30fbf4b3e1381811f22ce7ba1bae91388e2f5a4fc0ae0140bb0edd9ae55cbbba26c62f1e43978f865133221bdb78d9e18e5afb54a73f7053fcec4e2d7e8852f230acc283e47368ba82b9f983d03e0777fe229243ab9d71fecc599a698a88104711de79922461ffd57bb9d8c2bf3800b8e07ac0be18c94877084caf9eaf3fc4d864d76eb3167fc31973033ec1c6f56c92cf23f85ee578dff1b1fde645faa697ab2b0647a7ab2c4fff134a26a54246ebda7f443502cd94569ed39e5f646e65a5bb22d618bc57b23c67726a9cc9f1717117782ee0a5d5005be5e1657808a9db04d85a3976caaab6bd2cae1db6a3f68eb40915b99b7f8271f3ccdf7b18e73c9d4242579ce7508a84d8f6b5b3ad498e2930568552a1f5e7579b98e60ac70b8177de12598bf31eef3ca57242524a981a1f971c4faebb7374fc4506c09054aceec0c00ef1d53af3a63afd81deec50eff7032d1e21bee687d095e7f0b38f4263a648d06ccdc33b85abaae0f2befc7c5fd2aebf303acd73d2be338632e7786e25c577c9ae70ec5c646d6faa977338ef199d18232925524a121425db45c1bf3000ecd03b5f970350191c1cedc3516c9bedfc0bd244dd936a73a2a9fb205f414a1348651adf38b7657f602789644538bbb484f33d06d2bb8114d5a6a568fd8321bd0fe5ebde79eae375bcf38c8d8e4a98495d00e2027bfd2f1b277050c50bdef8f56a7fa7f581dbd520c3fa038dc5b54e13d56f297c0e3bfce3060e04ef98085bd47508fdd3fe06cfa9ec3b1dd1eb6098a64ab95e0d1a40214e6289e3b8071cd85515bfcb00900b2ad9d2edbe762d23a89ba8a34d3901835b3c140eaa5e39092b33857d719b868a6a4b8c3eff7996c7ee242a97445c6faad3fa3a0f254a6289a268c8fd0ebef852eef0dd0580be20cb70111e9d2363e8d5fde9f00b1553c22d3c164eed3471af13771300fa2e00ee268e134cbc354f7b3909f792660237f5e62ffa9a08b806a6761b128d6c03769df263b3a10d19de1fa8ddc9a79da10c5bfd7ba93d2e1c001da47712382fe4de7d68ed5e9785d9a83fd0b789af7a27a67c103bfe7a241a63c3668c0bee0f2c4e0dc91b976450c34b0a00e2339ae3b782f198878e42a335c8ffefa43f301921daf73aa2abbe035c8b4d1b460792002e94766b8a94f66d00800bef0f149fb15abf9193aff9358c4baf0060e0913648dff5ef58befd1dd8b933c8234791d3e7a09c0c3458ac5decf53bbf8dbde63b91b88e4435ecf8ddc30b36d74927219ffb1c12d550b7825f3e34782b1b92116bef5c866a36692fe0276e23fdc9af611b67767400c34bedb16327b05c2ec9cae2b29e3ef91c07eeff71e60f7e33d103bf4fedc461e4f829fccd0761b406db39be542cbafa3c521aebb9ef1b558baf9d40ea1ae4cfff65b0eba6bc2671b09d839ad6fa301ed245fcf46b71dff3eb81823df634620c7bf64ce0f02f4b00c8bccf77a632c4307376565d9e534a62c6ea35ca950acde70e611ffc3deaf38fe0933a18834f2c62428243acf47e16d33f1185ca3854a7102bb885c7d0b814f2ecc5513e6a0a22c77abc84c1915e7a365a50d485f3bcc330481f2a6c730dbfbb9093c73b703e8c8a2dfe992c0397635666d03dafc7ddfb2fb0fbefe5d953cfd34adb08109762ae3d70b5bc98b1fa6505809019b3cc9c9d51e71c5e95f15a95f1911a6a23e632881ff85daacb4728cf3d842665bc8dc068e8a631f40060c296f79116423778a360190440717aa8b71ade3f1400c59ce0cd00a08a4983d069ce20136f84f1fbe1de40c1cece2f30b3b840644c38733a89999c1aa75c2ec94bd1c3df35007440307b6e56f32c478ce015c66b15c66b15b011cd5c682be4a71ea034ff75a45467e4c86f054044091ac5c5546f0de70a474188ded21d9b27b24d00a8847381fb00a0b9223e0797a34e215f803cc38cbc0e19bd0feefcf91e05bb3447a3ddeea66c5d41c11ebc66ffaeb1702f790074cc4196652c2f2e0720143173298928c51163d5328a2112cb62b3097185cc01e71f245e78108cc5588b597c00bbfa401826bdf60069138e5bc7c4a82d063c4a7f0630a400c5b531be8dfae274f02ca35d7e0d32f26d94a545fbdaf752296631b6da19ed7683f3cbcbe1704aedc5f8599673f0baaba5142738f5bcdc1f2f0800c1df0a33efb22c63756955b32c0bcd94857c4a7144390ebee668b91cfc39635184cc793257344714c1c34a9bbe72fde0fdd9c68344cd07f0d8aeb3af1a72eea3e532ea1d79e93564e557775bcb4a3194a3a2ca2acf69a74d5a5946334d7b2792a886a9db3eecf8a414333d3121f9cb7cd75f5400ac058220acacaca8cb73f234c3166090be23cd4b714cc90692a51445c4c5efa80e3067dd364231eb92359de1e0be90659a65a4792f5e6fe739cd2ced1e71dcf9fcce4ef7dee39c676c7290827da508fea203602d18829236345757d518a1b5d2c0581380d279be38436f20bfd337724ed71ccadca56065f068b4610d4aaa8a47bbeadd7b8f7add90827da5097e5701301c0ce1679ee7f83c746ffabc98c3d37fa29ec88030fa7fef9ffbb456607ee04950f594476ba8d7a114ec2b55e0971c001b0162a30c4dde6a05cbbf060885065f07807024bda75cabad2324fa69d82b02bf4c00f0c200b2f1e38a802f512a78b71f570479691fe6ca125c01c095c72bf8f1ff0326895f8de290314a0000000049454e44ae426082,'image/png',128,128,0x89504e470d0a1a0a0000000d494844520000006400000064080600000070e295540000200049444154789ced9d799465495de73fbf887bdf967bd6dad5d54bf54a03dd4ddb080c0834307214470e2e38a2b8efe33aea71448f7f319e518f23e78c833a7ad419650667064147044711100411b46569badb6ebaaaa9aacedab2322b335fbee5de88f8cd1f71ef7df7bdccacce5ae846a77fe754e57bf7c68de5f78bdff68db8f1e0197a869ea167e819fa62a0e5ecd7a7cf6c3cffe072ff975e767ef897c79607fffd3bcf9cbfeee072efd7169feebeed44f27477e00b412bfea156f00f7d45f01ffc16751ff95ac21a6802ea410392bcfa7e91d93727edefffe84272e4ecd3dddf3afdb314c8d9c10f1f545d3fa5fe214487a0820645144020648839826d7defff32c99ddfb690de3478bafb5c52f27477e00b411a1e7b6b088f030d40108a9927020a4803f5c7f1bdb77c834cffa203bef9fcda2fffb89ad657a38aeaf0edfbe67ef2379f8ebeffb3d390f3ee2f7f3fcf7eee8d681f5401c58469449b51180a4a170d3d2408927c6990f49e2cf4de9e10ba892a201d671b2ffd09496efa101ae273004c615af79c5d68ddbdf485eaff3f59819c0b6ff9566875448f60e4f03b33ff635f2b7a17a2e7bf2b84cf3c5f434034000d66c37791721be0109962289f22f78f11fc71bc3f89ea3a904088ecd0e030adaf8010852a8520a18198e9f787c103dfb7efba3ffedc17625cff2405722abcfe47c0ff22242dd126a2adf704fdcc6b24cc813ad0b3583d4222d7a16e9d69fd464090643fc62e82cc802a3e3c469e7d82eef0ad082d0851155405098ee86f00a5108a820a66e68d1f32d3aff9ba85d65dcb577b6cffe47cc872f8e477e7facb3faff2500b400988345e833a94b340ce0cdf432b793548136c00b78986554c728038640702d6de886974c8dc47c8fd43231f03a8d8280431313a43e2f4558f86959785c1df4f03ff7f0ae474f8f9670b72f280f99975c7ffdeab727e3a7227207430da02151047aab7d1941723b258084b20994638489ceea023a780d8fdb41b6fa0913f4cb3f12a4c72005547081ba019fdfe3b180e3f00be47d926d24237fef065cbc75efc2c33fbcd8f2ceef9c1a3576bac5fd426eb5cf8f04f0ff9f70dd1e657ab9cfe3b259c126dbf54a4fb2f21a511aea5ed5e888419040b761ef034e4054861965424fa01a220a41ab2524c79083dd4af23e901c0c4926180fa0ba87ab2e187c8067f0da18f0f6751bf81a822cc81997d9f99ff8e8fd2bee7cd8bad3bdd958ef98b5a2027c3bf5a5556e721033ca209480a0c111216f39f200d37a078c062ec019018ea4625880211d582fd3a9203515048211402608afb055b34f257d5e1878f20768ee04e92f53fc2b0ff3e5041c210997b23921e692f2ebcf18af3992f5a937522bcf60f9533f36010edd008d7e3cc71445bb4fc8b688617d0b02f07e36bb3aa36bf64f4bd144ae50728f92eb5c2b6285bab4592e26e42d2ba13106c7a2349e34e843683debb892c0c407e55c6fd940be4dce017be3e0cde328d36e3056d639bdf8d697cd55f2fa6cfae4249af0f778c2c60759af9fceb68f82378b342669668877b31c9b3898c1054b5c65c50d582f75b0d54493271a5ac4346ba34567ef45d91648166e7cbf1ee386ef8095087f63ffdc6d5c1671f58683de76357c29fa7cc64adb8075fe9dd3b5eaffe1fdea8eeb3d3a88916830431b700331f54dd78c381f93f3a0df0b87fd67b8dcc7e45539fc582ff3718660041a455d4d8898cdf61409587280425b5b2a5c02adf5e6a854a8c6f6bd7b5acacd6948821e44be4bd8fd2dbf85d089b600f2153af78d0ccbeeebb16da775eb6509e120d39ef3e7d97f77ff2fbdebde7100c10e98c669c82fa63a0e63e91037f72eefc6b5fb26fcfffc922935a74f4e524e666b44c0818fd11196947259cba00448ad0355ed71022f34b6d9222e2d2d1f7311f525daab551f81c93ec056962ec3e82e6e0cfa0837fb84d933db700972d1073b90f5e0a79f7db5fe67d144624053c5afc8b0c53349c7abea21f5e19bcef3e9bcf2f263d8f6e1c6573f37708e17cad46a9fe454695e6a9741023b3a3351b1005316e144447fcd7da3fca44b0782ed6580898327436314f41c1b480f01eedbef77d57c2aba7c4649d19bc4ed51f23463101a3d334c29d08298a230f8f12c239508f3087495f8ce6f7a3f93990400839ade62b99997f333e3f86868d82d301935c87310b287ecb60267d0b55b475711ab36895392bbea020163f7c8c30fc1c83c107f0d9e7d0b00a6a91d69def90c69ddfb778e0a7562e8757574d2067f5579e8b266f06c5e84dff0e5dff451fde49627f922cfb96d789ce14eb1196b6be8ab6be82a8a09e90582090e77f4fee3e8df39f05d2c8748dd90321a7917e29c1af42c8aa192c324567ee7b489b5fc298a1a710482db22a67fc16873d2638adb4618c8223b83388e9a06148c84f830ed0b0896acee6855f0012a4f372a479fb1d8b7b7ff4e1cbe1e35511c859ff0b377af9e0df2b1716631276c38a511643780c3445c210cb619af2427c38cdb47b1dca104c1bdb78162313e4c9871fa73bfc557c58ae40bdd23f888f51558cff63db1a02497284f96bde866a7f3cb5a8520d29cc90d6667bf1a11044dd1749edb9924dbeff001a8a6c5dc72a012c83cd77920f3e8a4cbd1a693de78ec5c5efbd2c815c15a71ee4c4bb026717a57049ca238ba132de039abc9069f343587308ac41ed06c19dc224d740ddee8ba1d1fc17b4c3e3f4867f809869443b18598839b671787f96e0a32923282216977d06a405ea10f1444617cdcb36a64b263f48219751f65e95d7213e5b42c3a0185ba8553392bcea2026adc9818f4beb45eb97cbcbab2210c77b10f6010195014d7f47616e12323ec534df85317b50f222886960d2c3e39548e9889546e33e24584466b18d5b31e6208421c15fc0fb93f4fb7f88cb1f4424464862da6c9eff794c7290b4f5426cb240144a07c18cbc7631f547023285edcf11330fd28c75224086baf304b78cfa2e65fc23451f857a84d7c7e58f829987c1677e7d61ffcf5cf67ac92599acf3e1a11739fdebb764fc1794750c8b34f96932fef35dc87ac790329dbd86d4df0c048cbd9edc9ea0e9ee4292bd8ca9f9167bae35332168be8424fb0aa824421bea37d0e0507af4367e03f51bb8fc1160800683216a53d2bc85f6fceb810493dc04d240358cc260003ceace12dc59c0032975b437625c1910085aeb23d4ea105487f47b7f80cffe11b0a46b5f72b4b17eebabdaf7bde9f14be1eda8c65dd2b9f0ee79cfc3ab19bf87d08122aa51320c6d04cb94fb6aa6c26b41021afa88b411bb3f0e4e430567c441956b0f23f3306e5aca0c609b4e2aa0c3a2ce0e79ffe3f4bb6fc3e58f20210096b943bf8c863e4803310b18bb1f80e0ce4018a07e2542ec63a172acbc0ca32b3f54b6595df0a8f6f1fe0906837713dcb91861294c7de804cd4757ef30ff812fb40f49be21d3df46640ea141dbdd8d4a0f67ced1f47790e801daf25ac4ce021ac3738891156c936c959747d18d94de58250ab5e6582b3341e198a559c5a3e9d48b11b16caeff3a213b1693b632c5d28ce04fa3f952612e0b00d124456d8cb99cd8bfc21c8df538fecdfd83787782e096c8dd03084d100b02c9a921e985217476cfd52d5cde6dc1c0b97f8b4ca1e4ccb8573395bf88204372f33869b81e6b6e42cc2c1046e6a89600c70ba3a8a69aff25ff472151e54f4446b3b674a05bc25100cdb18d5b69345ec0d0afa0a1477ff5ed34a75f8ed805a24932e3359499fc768315901aca28084137e9656fc7fb5384b0810413619c1a24939e1e607a9e9adfbf64dab540940b80603940932fc534eec600893cbfe07ccc25c650d47a165c8493752a730499285cce4dd1917076ee5711349b168dce2b70f96771c3cf92f5fe06973d4a67e1db0a986332bb30a83a4a8032b69a509342454157e9666fc387a598d9938c658e66d333f3910bc82063f586afa4377b0bf0cbbb65ed185d5294a504a6c2ab699a1745c803003bea5cdd511798d1380028350e161a31ba109fade5032a13704869d2eaceb79e7d2bd8e4082e7b04f004b70c9a8feaaf702a8f67893eef230b9f060d24dc405b5e856111611aa1816a97a02bf4f3bfc0eb12112ad16a28663d902ce7b41eed81f3f4e79ecba9e7fe089b0b77f21408c4d00c37d2d267a315a0a3e31ab19309a02c5ef8898ad93039fbebb891d64c5cad045b73edea61a2ef28fb93e2740991416186c0b38a9393e4fa00ca46851e7b3d4b37fc3e46f761d987a1850fab847026e61898288f01d8b580dd08344e0fb16b39b95d6078e3ddf40fdd8d9b3d88cd3676cbd42db42b819c0a6ffde9c0f1eb67c35792ca6d44d3a4132c99605269a36b11544593822b62c90ac08311c28a16a6ab2e88c9842eb6a76e1d9116aa7d440d10c856de4b7e5d0b0985bfd201aafdd8876047892380a4045d25e84ad5945890cc6037c0ae82dd08987ec0f632d437e8edb90377fd6d24b3390dbb4ee2d77072f95e7d570219f29f6eb77ab0d3e1fb516996aca871b3ca572b6655dfb51651951516c2aa56f2a4f4ec15d63a01a9d71f2bdb70f8ec04c62e00e0b3e380236d3c8f74f11e069b7f4436f818c17761e8ab0924019006f5fd565243f66d2f41060241b15d41361582475c0edea24ca376407fff5d70ed7ed2e61a562f00e098412b0dbe3cda9540543c53fe0504ba08ad1187642418194baa4a4b3372da5a9ab78948ab56d184bf18d5395acd1b4d82909f42dd595c7eb6a8b358822dca268d3bc9061f012b4c7d76114d14b50246511b9b8c7320c4e783462d0a1e7c0e3ea05ec8db77207e1d4d6748cc0586b37763dbd0b409e20604d727467113bef232695702b17a3b7df328b3f91a41d71099076920b240b993acbea3630b764411ffef30732ad0440b13542b3b161b890081903d11d1564c5ded62c9b1763d6a5e423e7b009bfd23a22b95a6946dfae426823d1c77398604d514d73808c926a21934bb98d027a48b184e92ba2768983b48eceda85947c32750bf362e836d5632774bbb1248ca7dbd5cfe2ce472dcd8308fb20118449e0099c1986b10b194cb9fe34cd1c8187f0e318b5098bc3a95fa12f9dd27f873889d43ccdc984f57bf42c84f8fa1aea35ca5564e73f2ec930806dfbe9dfedee782be9012220e351fa462106c5497f493b4fc2c53fe08993989e318e2379010b07a0a75826051ed01c5124091148ec67a25066b97bab5b1f4c8735717bef9bdadf4aec333ee95d5f5400fa36dacce0069643801917d7136878de844dd99d871f598e486885191d63898813a344414b8c490a2e94a22b334afa2a81194314a2ee3400caa3df2ec7e06bd7721da44b21ec303afc3cfb4c144d318c4147a621014c1638809ddf4f0d934dd213c5d7ae6afc0e7488866cd388ff880849c440e815b2e50602130c5d03e9bcde4e5248d83df93583929490262feeee0f4d4ae7738ee4a209bfff881dfcdfabffaedeb779d25d5450c6d148f932798762f65cabd8c5aa634915f003aeeeec52c22d2a13475f87e0c2db5b6ada9aef693d5d504124791a2fe2cce9f20cf1f8870bc4694b7796c13d523acbffca5a001cc48208201558c7884f8324f333b402b3f4c2e67c97914090e098a8480711ee3036888df7d28b2f240377925bde6d7d04c1da935846261cde5ee6dc1f9efb9f19afdbbdab3b52b93653697987aac41f776c535cfd5b8916375112125908fdbfa8a61a1ca274a3f13c20aa2e76bb9dec8846c8dd7ca5bf5b0b8dc0097e2c312fdeccf50bf8a86b568ce0abccaaee5348ff608f6282af72105a6514e8cedd29c3c398d6705420f0985c7a9e15a239f366e963d33f407eb74371d8ad26e3669371aa0fac65c8303be6337bcde95407cd222c960ef5fccb3fab275dc8c2ba2a2846ef201144733dc5a0027b1c3811e4816052653547ba88afbe30965e1d0a1606659a28462185d2b16b260482f7f2f59fe2108310888eea470f401d2b31976a3cfd97b7f00ab7d822645bf955006105ace8762c3049e203d44421521169d8ce653f2a869e58c2ba2c361b6c1203b479016d6a65cc8baf41b29739d0e22f2ad0f1e3fd9989a9dfec11be6e72f5cb140ceddfad52c2c3dc8ccd2df30ffd119369eb7c9f0400662f0b2c65afa474438be964710218a46b8994e7e1f0d7fa468ae7ebff001d5bcefe3f4f3a80e105a245c4f84fa4bb42e666a5e9718bafb19b8f76368148a5570cf805d76348ff5693ebe466ff60ebafb5fc4ac7e706c4c6328ff044955408abe59520e63cd1c419f2084252a08c718e8f6b9f6ecefb06f38cf896b7e986b8e7c1967cfaf707e6d8df56e8f6bf7ef35d6da6f1a0e861f067ee38a0592647d969ff506081933a73f8ef9c41483030d7ab7f571f3798c5248260402a8e2cc09d61bff8396bb878ebb0f1b1651718c94df20a43896e8f3e7387d04d51ea21d12b98586de8e956bb11c0632b2f04986ee63387f1c91e6083f3360064ae3644e7abc4fb2dea3bbef5e96ee7c13aebd8fd17a0c2882d13641b26da5329ec82a566649b919a181310d725901db87d555ece93398f32ba44f2ce1e6ef80bd9e20c2fe7d7bc8f29c9367cfd1edf549134b23b16f38d3efbffb40bb7df28a0402e0d30e17eef872c2ec34d3273e4ee7a8a57526c5cd79860733366fe98d9241940aed40403c83e4efc9cdc324fe08cd703b69b89d5c3e47d055721ec5cb99b8bd47143445c9c8f5419c3e868406a22968881b19428648dcf988154c4f691ecd49d61c66d3a1ceb071ed2bc96e7a0e616a9e5a180628b383e761c214def4596f7d72c25f151dafef7424abc4a426206be7491ffa24d21b20bd3e923934492ab5534012c3fcc22ccb6beb9c387d8e5baf3f842a2febf7077b812b17086238f0f8afd13f7488f5c57b683ff108b6b7496339a3b19c30fd6087ec40466807fa87fa902ada543021fa8886c74b97907c8a61b81f348fbbd95522eea480d102da186786860ce38a40c12bc60bd205bba624ab1e1906c4296aa619ee3d845f380887f731e31e6225bc8ac04c555b2bbb91c42d8228c6b768e737d04f8f52e6355ae26fc620833ee23c9a6f30087f41f3c405583f46ba7206f120b9afccd608fd1ed1fccc0cd3ed361b1b3d9c0f18e3c15d3cb0bd04b457092645cc348de622f9cd77900d564956ce61b221321cd038eb509bd03cd920341437efd0341052c5cfe6a54906d1311b1eac479380c9923899b500c00231c27160fa025e917e40068a388764035c7a23b6d9c54f5d8b76a6b1b38644727067886b34022160b221a1d5c6db0c71092ac53a8840d25b2bba15109f637b5d2477d8956524cf31eb6b98e110f50171016c02de8df1663b0a28f30b732c9d39cf5a779376b381f35b0577e90229d4373453c44c91b66e07cdd1568e9f3e8bf7ebb8fe31f09e6473037139324c48564fa256d034215d31a8014d6c0c920c6852604b494053056f0ab0aff043c1c5bd582180cb21cf0832cf70fa6ec46ce0676fc29a65e03c66ea5ed2a0b8ec1fa9968d4b997b47e7f30fe19b33883c4c484f208d3d68e893f61f22cd570be84421f798412f9a210d7112a0a849105ffa3e1d4dae8bb10d589c99aed06401cc96e584cb11489157b8d9a962738247ec746c2099071186e104d8367e763a366d9590bc04930db1bd0b60f6a33621597d0f90838d8e382e5f081a2c9862c00624640c66be2122ad0a2eb99930b38a844d12f91ca26d123d87b821ea2d4d7b2b18870d3d7cfe1863b35615dbdf8c4cb610ec72f56695d140dc5e1ce244c87d64b93111332c4cd116364aedea0e3c56a0d9688cf5e5c960954bdb97255b3f2a32da385092891811fa797c4bd1a90ece1c050bc36bbe9120360aa19c366106cdf7a38d1344ee840869840d5259a5e3aea1a9e719e867210c0a54569090532592ba0aaa04bfc26859d65341309a13e19a12b8ac43d3a3af55de53f66f9205f59dd925edc0e5ede4f464d0c8156d94aba0341dbb50fba2343942c27e06fa79723d07e13385692f62785b946e28237ccbc7198b47f27d246e3f20a4729e4c3e5f018a4ab1b74412b2ec639860d0b0494cfd12faf645a48d83b425216b7c3be44bd8c1874718d9e4780a60544399988e21365bd3fadaf5ed6e55efa15c025db64074e2ffda242b485002a91cc23045536fc4b3496050142cd46334258b6cbd48b345400dde6c30488e6143834c96e2cbfda32499725552c380a0a1ca859cec6123793569ba40bb91e2a6fe352eefa2ee6b49fb7f49b3f72eb47cf9a7ae3032be122af50f157c335e6072ed14d8f665a2ddd0956d25ddb2cf677c4609865c4f93720d3967f16c164964bc2b1a2193490ec4f9592ee5f619248f40f09830fe42cb785b631dc333c77ab607196e726a30a0dd6c32dd6e912687c9a7be09a74d3a833fa17c67bd442c27162999ecde18d5fdc844a92deb41c620d660ccc555e6125fd8194b9f907af85a0ca8ccd0cb9377b270946ef80819c7aae62286b4750655cbe6dbb52b76dc3a0a886912e1f862c35b3d96d60dd2fedfe286eb348c43fa9fe3fc7a8ff36b6bf860e8365f476eaea3528f641a691f46d2b8e85675236e9f19cfde65d4966edfe14a437afd3e2286e94e8b4ebbc5c2c2dcb6e54bba2481241b3d34b165b75095115fc7664bd9abc2e9632937aa95d76b7fc6ca97b8d468868e4fd112b64a3844d33c9fd4de85d01cd5670cd884a9c73ec64d7ffb53b487a769981e87b2df23f5a7b8d0edb3bade05693248ee4631605b98a99b30337760666e8364a6d6bb890000b675f6e33da45a723ebb728176b3416a2d4195e02fbe8bee924c56e7c1e3f8738aded244e79f173b2aa60a8bcb195c0108e5ee92ba1669ddd08c4628c5f61da52680d1741c77b2e269f25ce20ba37bf0e651f07d6430c03efe38f6c4494c7f88d95c073704947638c675f96fd0e5075859f7b49b0d06c9eb991dbe0b91a4d036059322a6b165b2146cde62c24663dd2aa83ccf59595be7c0de057c0878e77e3649d3072fc6e34b12883845b22eda3f8ae8e390ee419bd7839dc10e337ccba0695965dd7c142c9d88c662094b2b3f422bbf012161b3f139fae9d18a01d5e92fe55711542db93c41eaf722831ed25b2579ec339895d5882fb9003e6c917b4b4fb0878f732c7f39ddfe8076bb8993bd247e95902d233643fb67d06c99c98777f425b5ad4e75b3ad3eb0bcbc4ac31a66da6dbc86e3d3ede6df5dbfb0906d574d4997e1d4a5b09f16dc2ae257c02acd134b844e13df4cf1b333a81174aa83365b0523c398edad6a0b098dfc60316847d3ed27b3a708a64f14634d6b4430fd2e26cb082b7f4a1874d0ee7992952730590ecea3a6dc28c1361634e580fc150fe5afa0371832d5c9e9da1730efdf8d6e1e4543b12f572c525bdd29c7bc9d402a4d5225ae93407f30a0bbdea53f1cb267761645cfe659fe03b71d3af8e74fc6ddcb8bb22ae75a9813114c6f1393f5b146d18d55828d3049480cdab4b885bd04a39008d9fe6ba2333406c412acc7f8b8af564d82378a71198d8df33153f701bbb981bdb00259863887d9dc449c0717182582e361ea760c4c6583963987704d11801427fd98f2dd91b0b3a366a2ce2ac854067bee22dbff123af307e8753749ada13337030a01569e73e375efd90d6b2f5d201361e138452e481ed7a1f119d681e68aedad45a8c442e78947e3de28036a03343f864edf029260d6ee67c10da9c0c550981f1fc005d47bc4fb2afad9863f170f5531ccdbcfb191ed25777ef4602d3889ae6b9cfd0ae3870f486d215703dd1b5ecbe66ddf422381b68d1b319c0ba783863788f0d127636b495798a9c74e4f44be35123438a4358bba752ad8a2d8b5880135027e9db0f677f1098d5a23a2c5cb37b53cb2fc5b33d95b66ec3629f3648ed2303db2dce3fcd657a9470f09e532400cb113a4b308d900c22a48186bdf0e57d02441c423c6641ac2efa4a9fdd383738b1fdca989ede8320432e2f828f6a9ddab334e3d66e136ccd45efce6e3e8b0dc415ed6538f67eda88ee24f59624ce01312d0c2cf6c1b766fd36d4159488eb2e45eb9a5f436f15f712360a66ec04cdd0cbd6534fb54818d8dfa3efdf93f069fb37ac3d7d23e78eb9bae9b9fff959d3bb3335dfe490edbd885ea35b051214c7b1fa43398d67e308dc8e81a67e3deb58944a3f67112269b64988856e7996ce9e2b61794d4f42a81d7cb54db61b73385e914481ac72066749d68be92de12ed931f20df5ce1f4b90bbbf217dbd1154227d4b836212005c410369710f610faa720e471b3da93d539a68545d5ba0d83296df9d63bba8d8f296b5435cc4e7768359ad09f6c7b6b3b2a86b0fe189865e85f40b2f59a25a8cfa012897892315e842e2fec2d3b5af883f2ead86c53418d41d71f27f48e8195f86f1b1a7b0fa4ac4895b10367b674a31efb6feda36c77af90ee46b811caf7d9b9583d653302791f751b31b2937802c5ce26f2f28045b86493a5905ab0a39920b55bdb774c0aff5013d9048e3516e197da669a9899db90f661b642e5322aba1d4d5ace5addaa42cfefa1911a144b43979864ec16bf55752bded1b1123b74e032e9d2344481761392d1feaab1dbdbe1ff63ea3bf2f8b5f76927ca13738fd9e721c91cd2328410d0fc78d105a9d5526bbbcadf64cbbd3a052ccbdcc7b5ad1455688707286de2764e7decbb5e3c631ff9b2a74c43c6dbaa50d6b24f93e66ca27c2daddd0eb31b2fe307d134d432608cd4f56cd4ae4cb6be95045049785c5fcf5433303d354dc77f628b86ab6ce7911865ff3b557e055a51a7cb72ea1553b6ed79eda3cad8fb88e39dbec800c4a2fd63847c35c221f96a0c06c24483854628a385d9ed3269508226ac997be9997b39303707a1c7b47f7f152d94c1d5b6aea912c64524527f2de2e270f045e9b204320a412fde70ddfe6ebbd6b1934015d40fc03d015ee3bf6d7563f4c8e495ea683f84202d4e35dfc07af3c51c9cdd8bb10d6cffa334fcd1f15a6a666b32e4ded286506cb0dfceec5ebec9ba74814cfa5f263aab5b8bcad8cd8b4526325e6930942f00d619549fc55b6b8bb6d0b50fb276c70f92eeb909db982799f91a0e2696a090f43f4c7bf3b710b6be2130c6cbc24eede416af8e911aa72b32593bced91d42d19d9dddceaa5245c1b592525bb42a7d557c6b974a15b3d99b59b9f3c76832a09326882478d7a5d1ff075aebbf85487f3ca8a80daa6aa3ac5d1551cfe481683b67f64fa5c9d271e6eca27895576c49a6aa7e4f0ce062b2a3603e063375105c40369640fcd85c36ae8fc9bbd0682078ece63fd058ff736cfe30487fb412393998895844c462668f60920574e56174f3dc58a457f6778b702f93ae4ea65e7ebe58b97a41adeb1835a91599f784f31e3357557516b3f75ef03e0661c39a3f10a175fe93dcf83fefe5b197ff57bab30b1c597d33de26c54ec9228f998cd26b0ea46ac7b430adfd48320f0b06dd78ff0ebef3e98ab2644cbb779483e8d856815ac1d14b3ba3c2064966c0a490e96221450000086149444154af830c27d2fec9b68a67435ec0f3c32d9d143c267489ef2f2ae0a03acb44b7ccf0222ede6286340c08d932c607b47b1c26df2616b63c730569c89545595afd3f8a68b4b6eba166852746391e0e8869623ab7423a870e4e12361e641b29542400eaf0e7ef47bcc260794bb4b313e0580f8515196decaec630ce60514fe81e43dd51186c56bea59e84eedcc6a5d3a567eafa24267e57a9c6c40d1dad2d68f5b310e36168f954351904e89f85dcc58dd2131a75f13194ef0e7aa475187c8666178887fe6f7d5c8343822fdeb5af9dffb55d98b9f35ea65dd1652fe18e3964e7471ddc555f4a3f527cf343c2858f21c90cea37c7cc998c3f457d2653455552e505b52068472a75d7b40e63a66e07d723f807c1675b87b7cdd3113e99f4738a1a8b4a724526eb8a4eb6aef8b11233e92d6bd1c56c94c61c92140752564f8edb6191a43892cf548f4e56b5337c51aaed93a4aa650c5198290dc3e20c603756687b409f091c6bf4457c4e98bd8ecd5bbe9eacbd7fa2be4ba3abb0d91aeae79c94b06145b6895d7c2ee0f0eb0fc75340c7a2abf2c3382beb9a3f766ed6c413828cf98b8bcfee511f03805bc7773f0bc181db1c259cb5a8bc32a5321188a0f1b9648a70d337d2bdf6ab584e8f60bd2779bac25e29fa753115159340d28e854d1a831d5b060063355db49dc926ea16732b5221951b1a9379eda300aa0e756ba3305be206f16d83da5a688ea4a84d7037bd8e70f337735e17591f801b66bdb9b999f5ce54fbb255e4eafc3ac2162f58fb987709ab0fa126a0c315b65fc5bbd8bc1e01f5bae589adcf5c54b4176f662260d12805510843681e8246033ab710a6ef22bbf6b5643e617563833ccf31c6d26a367fefe6fdfb7ee0625d7832ba628128c403c6aaef13d3550ca17b12ac46c4d650454fa30460878acb2aca76cab861d284d5c3b0edbcba2a12fa94af15c6c394c7ef47fe6b8cbc82234f6fc3b4e630c91cb4aec34cdd4d2ed3f4dbb73018e6f4d6fae4cea31a68341a586b7f6e7e61f6e3bb60d945e9b2f765551f27063fe643ea7e65ccfc6ebfae51ab640cc298145b3495f9445f765ebd0ce91eba37be155fbcab1d8ab9d1a88fbe8aa795d4065ae91e94267d67183245ee1c21287e6303170221049acd06c06f1a6bde9ea6c987f6b7da57701e69a42bf421630a3e71efe2d15fddb6efb481a1aca72c5f9d3f99ce911c7835ba7e92b0f220e2bbeca4690218d36076f11e4218e757f5b26f50321f19ae283de7c8fb71cd5d155487d11fa9f69cf78f34db2d8c315fe3bc3b9da6a9bb6161e18a7f9dada44b784f7de22fe37951f93e485d3b2a6d897686ea270db654bcdd72ae8e7f2a7fdf511569ee8d4fb616c03681eed62aabd52621a8b2dadbac0452ca25fae96242d56c981821492daa74f32c7f5b0881f6541be0d875d7ccffd24e2cba1a74c551d6c573a0497f52fb505f41dc36fb2f0435e1125484d03f81980ef4ce81ebede8ab550c8dde12bdf9671f37d67cd058fbad30bef0a8aaa75d96bfa97601632cb69982d2bd61cfe23b2e3ac4ab4c970e9d5c124db279d213d43cc4184652bb577bc1b3325f7e88bff00026cf265ee01f6f474dcaf4b9bf65e5c02b564c92fc28f01f544707c9161a921d9a9bbd6abfd479a574f9fbb226a7e5365374dce88c83783b3d649a07103b4be89d8cc9da96924584151c93ebf4d1678fab94f8b82ab8afddb9005cf468a42f06da3d74a28a6f4cc3661fe947b8bb9e2bedf0503447e914a673186c6bec6e95d089564c3433f720adeb31adc363394b94a5b05d1eb3d3dede605b6ceeb967d743fc62a04bc0b284fe4b7e04efa690478e226b1b88adffdcc30e8f058fd9772f66ee36ecd40d8caf2754d848f531f41f073c9aaf540eb7b45c3ad1d4a4c2551f434464b3bbbe93cd23af8d4764fc13a1dd9aac1f0c21bc7879f639b7cdbfec67491ef9535a4f7c04397d0e0ecc83cb478b80932482ba0162a7c024a55d8985abcdcda3e261f361b4fb703c477754094c7c2bff8e9c7ee16bd229c235af6270c78fb02c7bc806431617678eef729c4f3bed1a067bf4d499ff88ea8f37d384e9a90e76fd04e6f37f89ac3e8aed9dc2fa153431f155b6e2c59c6015350acd6968b651b78a9283d5d1013456c0c6c38d2b504f359e1da0b5c38d9dc657ce9ca23e1e4a637207f910611a5a8791993bc80ebc9adedc5d9c5f8b616eabd3c25ad3be65dfbe2bfee1e0a78276edd48df026451a832cff21e73768350fd0beeb3bb1fd73f8f5e3e8c60974ed28463769aefc0d2a1a814505cdd6e2e16485a022d5c0a3ed6842e36262a8a87ae2afeb7898ba1de6ef8599bb18366e64b3712d832ca37f610355b0d6fe51a7d3febf699a5cb5c4ed0b4d9704141f3d736e36787f9358f396e0c38b139b34d2c4d248133acd1493f7c8f20ccdd770d906e6c2a710b7869121a67b3fe2ce16c77d13f75f17c735610c6a356a5621095110ef101fd7cdd505f2e63d24cdebd0f91790b4ae45658aa14eb13e0ce4dee39c23a8e29c5f32c67c5592264bcfb9eedab35f08c67da1e8b291fbcf2f9fbf3e04fd6faa7a9b183924aa34d384c418da698a188335366e3c0c8ad8a88c43572466c532aaed3f80c98fc5d383881973620da931e4c9b3c8d3eb41c11a253101ef95619e31c8860cf3bccab6c598751fc2fdcd56136be40d37eed973fa2af0e729a72b584a89b4b4b1f13aefdc0b83f3d72789fd260df19c5e630ca9b58808a9b55b0eee1ac116e3275587da8fc278eff145c4e44360e85d812d29a892a409ceb9f767c3fce3738b73270ecfcefeda958ee7e9a62b164849a7bbddbdc097b961866a2069a42f35c6fcb8061d8147542962a121258e24c506771dc394aacf1ad1aea4d520f8f0cee160f8fb009de90eaafae9c373735f3499f695d25513c8249debf75a0af3ea7cdcc15e34669ae9bb140e8e152e93f882f1f19a9267f9977befbb314f0934673aa074af999e9e40139fa167e8197a869ea1a781fe1ff62d25c53fef74880000000049454e44ae426082,100,100,1296,1282963058);
# --------------------------- /flip_content_image -------------------------- #


# --------------------------- flip_content_text --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_content_text`;
CREATE TABLE `$prefix$flip_content_text` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `group_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `view_right` int(11) NOT NULL DEFAULT '0',
  `edit_right` int(11) NOT NULL DEFAULT '0',
  `caption` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `text` mediumtext COLLATE utf8mb4_bin NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `edit_user_id` int(11) NOT NULL DEFAULT '0',
  `edit_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_name` (`name`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze:
INSERT INTO `$prefix$flip_content_text` VALUES (23,CURRENT_TIMESTAMP,3,'menu_my_settings',4,2,'Deine persönlichen Einstellungen','<p sytle="text-align:center">Hier kannst du deine Einstellungen bearbeiten.</p>\r\n\r\n<div style="text-align:justify">\r\nEin paar Daten (Name, Mail usw.) m&uuml;ssen angegeben werden, damit wir dich identifizieren k&ouml;nnen. Diese Daten hast du bereits beim Registrieren deines xxx-Accounts angegeben. Alle weiteren Angaben sind freiwillig. Deine Daten sind selbstverst&auml;ndlich vertraulich. Sie k&ouml;nnen nicht von Dritten eingesehen werden und werden nicht zu Werbezwecken missbraucht oder weitergegeben.\r\n<br/><br/>\r\nWir bitten dich nochmal, <b>unbedingt</b> vollst&auml;ndig deinen Namen und deine Adresse anzugeben, da wir diese Daten beim Einlass und beim Checkout ben&ouml;tigen um dich und deine Hardware zu identifizieren!\r\n</div>','',1296,1089017628);
INSERT INTO `$prefix$flip_content_text` VALUES (27,CURRENT_TIMESTAMP,3,'user_subject_user',3,15,'Subjects -> User','Dies ist eine Liste aller User.<br/>\r\nKlicke auf den Namen um weitere Details &uuml;ber den User heraus zu finden oder um ihn zu bearbeiten.','',1296,1096827562);
INSERT INTO `$prefix$flip_content_text` VALUES (28,CURRENT_TIMESTAMP,3,'user_subject_group',3,15,'Subjects -> Group','Dies ist eine Liste aller Gruppen.<br/>\r\nGruppen werden verwendet um die Rechte der User komfortabler verwalten zu k&ouml;nnen.','',4,1061329173);
INSERT INTO `$prefix$flip_content_text` VALUES (29,CURRENT_TIMESTAMP,3,'user_subject_turnierteam',3,15,'Subjects -> Turnierteam','Diese Gruppen werden w&auml;hrend des Turnieres verwendet, um die Spieler zu Teams zusammen zu fassen.','',4,1061329159);
INSERT INTO `$prefix$flip_content_text` VALUES (30,CURRENT_TIMESTAMP,3,'user_columns',3,15,'Columns','Hier lassen sich die Datenfelder der Subjekte definieren.<br/>\r\n<br/>\r\n\r\nName und Bemerkung werden nur intern verwendet.<br/>\r\nTitel und Hinweis werden angezeigt, wenn z.B. der User sein Profil bearbeitet.','',4,1061329714);
INSERT INTO `$prefix$flip_content_text` VALUES (31,CURRENT_TIMESTAMP,3,'user_rights',3,15,'Rights','Dies ist eine Liste aller Rechte. Rechte k&ouml;nnen Subjekten verliehen werden.<br/>\r\nRechte werden von Subjekten ben&ouml;tigt um bestimmt Aktionen auszuf&uuml;hren.<br/>\r\n<br/>\r\nverg. ist die Anzahl der Subjekte, die ein bestimmtes Recht direkt (ohne Vererbung) besitzen.<br/>\r\nEin Recht kann erst gel&ouml;scht werden, wenn es von keinem Subjekt mehr besessen wird.','',4,1061330169);
INSERT INTO `$prefix$flip_content_text` VALUES (32,CURRENT_TIMESTAMP,3,'user_right',3,15,'','','',4,1061330019);
INSERT INTO `$prefix$flip_content_text` VALUES (33,CURRENT_TIMESTAMP,3,'user_register',0,2,'Account-Erstellung','Hier hast du die M&ouml;glichkeit einen xxx-Account zu erstellen. Dieser wird ben&ouml;tigt, damit du dich f&uuml;r die kommenden LAN-Parties anmelden kannst.\r\n<br /><br />\r\nWir bitten dich, m&ouml;glichst vollst&auml;ndige und richtige Angaben zu machen (insbesondere was deinen Namen und deine Adresse angeht), damit es auf der Party keine Probleme bei der Feststellung deiner Identit&auml;t gibt. Das ist sowohl beim Checkin als auch beim Checkout wichtig. Deine Adresse sowie weitere Daten kannst du &auml;ndern, wenn du dich mit deinem Account eingeloggt hast.\r\n','Hier kannst du dir einen xxx-Account erstellen',4,1062256764);
INSERT INTO `$prefix$flip_content_text` VALUES (37,CURRENT_TIMESTAMP,3,'archiv_eventlist',0,2,'Archiv','Hier im Archiv sammelt sich alles, was wir &uuml;ber die Jahre so angestellt haben. ','',1296,1089134973);
INSERT INTO `$prefix$flip_content_text` VALUES (49,CURRENT_TIMESTAMP,3,'sendmessage_sys',58,5,'Nachrichten -> System','Dies ist eine Liste von Nachrichten, die vom System zu verschiedenen Anl&auml;ssen automatisch verschickt werden.','',1296,1089280400);
INSERT INTO `$prefix$flip_content_text` VALUES (50,CURRENT_TIMESTAMP,3,'sendmessage_view_message',58,5,'Nachrichten -> Vorschau','','',1296,1089280426);
INSERT INTO `$prefix$flip_content_text` VALUES (51,CURRENT_TIMESTAMP,3,'sendmessage_edit_message',58,5,'Nachricht -> bearbeiten','','',1296,1089280377);
INSERT INTO `$prefix$flip_content_text` VALUES (52,CURRENT_TIMESTAMP,3,'user_registered_successfully',0,2,'Erfolgreich registriert','<br />\r\n<h2 style="text-align:center">Willkommen bei xxx!</h2>\r\n<br />\r\nDein Account wurde erfolgreich erstellt, und eine <b>Aktivierungsmail</b> wurde an deine Emailadresse versandt.\r\nUm deren Richtigkeit zu best&auml;tigen rufe bitte den darin enthaltenen Link auf.\r\nDanach hast du die M&ouml;glichkeit dich in das xxx-System einzuloggen, um dich beispielsweise f&uuml;r die kommende LAN-Party anzumelden.','',4,1073931373);
INSERT INTO `$prefix$flip_content_text` VALUES (54,CURRENT_TIMESTAMP,3,'user_activated',0,2,'Account aktiviert','Dein Account wurde soeben aktiviert. Ab sofort bist du Mitglied der Community und erh&auml;lst alle Infos und M&ouml;glichkeiten unserer Seite.\r\nDu brauchst dich nur noch mit deinem Benutzernamen und Passwort einzuloggen.','Dein Account wurde aktiviert',4,1073931343);
INSERT INTO `$prefix$flip_content_text` VALUES (55,CURRENT_TIMESTAMP,3,'user_status_nothing',0,2,'Nicht angemeldet','Wenn du die AGB akzeptierst, kannst du dich zur Party <a href="lanparty.php?frame=register">anmelden</a>.','Es gibt noch keinen anderen Partystatus',4,1073931422);
INSERT INTO `$prefix$flip_content_text` VALUES (56,CURRENT_TIMESTAMP,3,'user_status_registered',13,2,'Angemeldet','Du musst noch den Unkostenbeitrag auf das Konto &uuml;berweisen.','Du bist angemeldet',4,1073931812);
INSERT INTO `$prefix$flip_content_text` VALUES (57,CURRENT_TIMESTAMP,3,'user_status_paid',19,2,'Bezahlt','Du hast bezahlt und kannst dir einen Sitzplatz aussuchen.','Du hast bereits bezahlt',4,1062188002);
INSERT INTO `$prefix$flip_content_text` VALUES (58,CURRENT_TIMESTAMP,3,'user_status_checked_in',22,2,'Anwesend','Herzlich Willkommen auf der xxx','Du bist auf der xxx',4,1073931396);
INSERT INTO `$prefix$flip_content_text` VALUES (59,CURRENT_TIMESTAMP,3,'user_status_checked_out',4,2,'Abgereist','Du warst dabei!','Du hast die Party verlassen',4,1062188218);
INSERT INTO `$prefix$flip_content_text` VALUES (60,CURRENT_TIMESTAMP,3,'user_status_online',0,2,'Online','Du bist Online','Du bist Online',4,1062188181);
INSERT INTO `$prefix$flip_content_text` VALUES (61,CURRENT_TIMESTAMP,3,'lanparty_guestlist',0,2,'Teilnehmerliste','Hier sind alle Teilnehmer, die sich f&uuml;r die kommende xxx. angemeldet haben aufgelistet.\r\n<br />','Eine Liste aller bereits angemeldeten User',4,1073931046);
INSERT INTO `$prefix$flip_content_text` VALUES (62,CURRENT_TIMESTAMP,3,'lanparty_register',0,2,'Anmeldung für','Du willst dabei sein? Dann musst du &uuml;ber 18 Jahre alt sein und die AGB akzeptieren.','Anmeldung zur LANparty',1296,1092830816);
INSERT INTO `$prefix$flip_content_text` VALUES (64,CURRENT_TIMESTAMP,3,'user_party_agreement',0,2,'Einverständniserklärung','Ich m&ouml;chte mich f&uuml;r die Lanparty anmelden.<br/>\r\nIch bin mindestens 18 Jahre alt und bin mit den <b>Allgemeinen Gesch&auml;ftsbedingungen (AGB)</b>einverstanden.','Der User akzeptiert die AGB und versichert mindestens 18 Jahre zu sein',4,1062253946);
INSERT INTO `$prefix$flip_content_text` VALUES (65,CURRENT_TIMESTAMP,3,'user_logged_out',0,2,'Bitte einloggen','Wenn du bereits einen <a href="user.php?frame=register">Account</a> erstellt hast, musst du dich mit deinem Benutzernamen (oder deiner Emailadresse) einloggen.','',4,1073931295);
INSERT INTO `$prefix$flip_content_text` VALUES (66,CURRENT_TIMESTAMP,3,'sendmessage_send_message',58,5,'Nachricht versenden','Hier k&ouml;nnen Nachrichten an einen bestimmten User oder an Gruppen verschickt werden (Newsletter).','',1296,1089280449);
INSERT INTO `$prefix$flip_content_text` VALUES (67,CURRENT_TIMESTAMP,3,'sendmessage_user',58,5,'Nachrichten -> Benutzer','Dies ist eine Liste von Nachrichten, die an die User gesendet werden k&ouml;nnen.','',1296,1089280412);
INSERT INTO `$prefix$flip_content_text` VALUES (69,CURRENT_TIMESTAMP,3,'seats_block',0,2,'Sitzplan','Du kannst...\r\n<ul>\r\n  <li>den Mauszeiger &uuml;ber einen reservierten Platz bewegen, um in einem kleinen Fenster angezeigt zu bekommmen, wer dort sitzt.</li>\r\n  <li>die Suchfunktion verwenden, um schnell den Sitzplatz ein oder mehrerer Freunde ausfindig zu machen.</li>\r\n  <li>einen Sitzplatz anklicken, um ihn zu reservieren, vorzumerken oder wieder freizugeben.</li>\r\n  <li>an den blauen St&uuml;hlen schnell deine eigenen oder die Sitzpl&auml;tze des Suchergebnisses erkennen.</li>\r\n</ul>','',1296,1118074281);
INSERT INTO `$prefix$flip_content_text` VALUES (74,CURRENT_TIMESTAMP,3,'sponsor_list',3,2,'Sponsorenliste','Diese Liste zeigt alle Sponsoren...','',1296,1089017896);
INSERT INTO `$prefix$flip_content_text` VALUES (75,CURRENT_TIMESTAMP,3,'user_subject_forum',3,15,'Subjects -> Forum','Dies ist eine Liste aller Foren.<br/>\r\n<br/>\r\nUm ein Forum einsehen zu k&ouml;nnen, ben&ouml;tigt man das Recht <i>forum_view</i> &uuml;ber das entprechende Forum.<br/>\r\nUm in einem Forum posten zu k&ouml;nnen, ben&ouml;tigt man das Recht <i>forum_post</i> &uuml;ber das entsprechende Forum.<br/>\r\nUm Beitr&auml;ge zu bearbeiten, ben&ouml;tigt man das Recht <i>forum_moderate</i> &uuml;ber das Forum,\r\noder <i>forum_edit_own_posts</i> &uuml;ber den User dessen Post man editieren m&ouml;chte.<br/>\r\nDas Moderationsrecht <i>forum_moderate</i> erlaubt einem gleichzeitig, Posts zu l&ouml;schen.<br/>','',4,1062671784);
INSERT INTO `$prefix$flip_content_text` VALUES (78,CURRENT_TIMESTAMP,3,'forum_doku_fliptags',3,15,'Fliptags','sry, not yet implemented','',1296,1089017554);
INSERT INTO `$prefix$flip_content_text` VALUES (88,CURRENT_TIMESTAMP,3,'user_account_trouble',0,2,'Account-Wiederherstellung','<ul>\r\n\r\n  <li>\r\n    <b><a href="user.php?frame=resetpwd">Du hast dein Passwort vergessen?</a></b><br />\r\n    Kein Problem, hier kannst du ein Neues setzen.\r\n  </li>\r\n\r\n  <li>\r\n    <b><a href="user.php?frame=resendactivation">Eine Aktivierungsmail ist verlorengegangen?</a></b><br />\r\n    Hier kannst du sie dir erneut zuschicken lassen.\r\n  </li>\r\n\r\n  <li>\r\n    <b><a href="user.php?frame=changeemail">Eine Aktivierungsmail kommt nicht an?</a></b><br />\r\n    Hier kannst du eine neue Email-Adresse setzen.    \r\n  </li>\r\n\r\n  <li>\r\n    <b>Das alles hilft auch nicht weiter?</b><br />\r\n    Dann wende dich bitte direkt an uns!<br />\r\n    Wir werden dir gerne helfen deinen Account wieder herzustellen.\r\n  </li>\r\n\r\n</ul>','',4,1073931327);
INSERT INTO `$prefix$flip_content_text` VALUES (89,CURRENT_TIMESTAMP,3,'user_account_resendactivation',0,2,'Aktivierungsmail anfordern','Wenn dein Account deaktiviert und die Aktivierungsmail verloren<br />\r\ngegangen ist, kannst du sie dir hier erneut zuschicken lassen.<br />\r\nBitte gib im Feld "Ident" deinen Nicknamen, deine Email-Adresse, oder<br />\r\ndeine User-ID ein.','',4,1063819572);
INSERT INTO `$prefix$flip_content_text` VALUES (90,CURRENT_TIMESTAMP,3,'user_account_changeemail',0,2,'Email-Adresse ändern','Dein Account ist deaktiviert, aber die Aktivierungsmail kommt nicht an?<br />\r\nM&ouml;glicherweise l&auml;sst sich dieses Problem durch ein &Auml;ndern oder erneutes<br />\r\nAngeben (Tippfehler?) der Email-Adresse l&ouml;sen.<br />\r\nEs wird eine Aktivierungsmail an die neue Email-Adresse versandt werden,<br />\r\n&uuml;ber die du deinen Account aktivieren kannst.<br />\r\n<br />\r\nBitte gib im Feld "Ident" deinen Nicknamen, deine Email-Adresse oder diene User-ID ein.<br/>','',4,1063820143);
INSERT INTO `$prefix$flip_content_text` VALUES (91,CURRENT_TIMESTAMP,3,'user_account_resetpassword',0,2,'Vergessenes Passwort ändern','<b>Du hast dein Password vergessen?</b><br />\r\nKein Problem: wir k&ouml;nnen es dir zwar nicht zur Erinnerung zuschicken, da es verschl&uuml;sselt ist, \r\naber du kannst dein altes Passwort &uuml;berschreiben, indem du ein Neues setzt:<br />\r\nDazu gibst du im Feld "Ident" deinen Nicknamen, deine UserID oder deine Email-Adresse ein.\r\nDu bekommst dann von uns eine Email zugeschickt in der du einen Link findest, \r\nunter dem du ein neues Password setzen kannst. (Falls deine bei der Registrierung angegebene Emailadresse nicht mehr existiert, so wende dich im Quakenet unter #xxx.de an einen der Organisatoren, oder schreibe eine Email an info@xxx.de !)','',4,1063820422);
INSERT INTO `$prefix$flip_content_text` VALUES (93,CURRENT_TIMESTAMP,3,'poll_main',0,2,'Umfragearchiv','Hier ist <b>deine</b> Meinung gefragt, um demokratisch mitzubestimmen, was auf der kommenden xxx <b>wie</b> passieren soll.','',1296,1091599099);
INSERT INTO `$prefix$flip_content_text` VALUES (94,CURRENT_TIMESTAMP,3,'news_archiv',0,2,'Newsarchiv','Hier sind alle News aufgelistet die je auf xxx gepostet wurden.','',1296,1089017762);
INSERT INTO `$prefix$flip_content_text` VALUES (99,CURRENT_TIMESTAMP,3,'server_myserver',0,2,'Mein(e) Server','Hier besteht f&uuml;r dich die M&ouml;glichkeit deine(n) Server anzumelden.','',1296,1089017239);
INSERT INTO `$prefix$flip_content_text` VALUES (112,CURRENT_TIMESTAMP,3,'checkininfo_info',0,2,'Userausweis','Dieser Zettel ist dein vorläufiger Ausweis, um beim Checkin schneller eingecheckt zu werden.<br />\r\nBitte bring ihn zu nächsten Veranstaltung mit!','',1296,1282868189);
INSERT INTO `$prefix$flip_content_text` VALUES (113,CURRENT_TIMESTAMP,3,'menu_lanparty',0,2,'Lanparty','Jo... wir machen ne LAN-Party. und wenn wir wissen wie das aussehen wird,\r\nwerden wir dich hier informieren.','',1296,1089017748);
INSERT INTO `$prefix$flip_content_text` VALUES (114,CURRENT_TIMESTAMP,3,'menu_admin',5,15,'Admins only!','admin? admin! *fg*','',1296,1096384060);
INSERT INTO `$prefix$flip_content_text` VALUES (115,CURRENT_TIMESTAMP,3,'lanparty_netzwerk',0,2,'Netzwerk','3m BNC-Kabel, zwei Terminatoren, und ein Nullmodemkabel ;-)','',4,1073933279);
INSERT INTO `$prefix$flip_content_text` VALUES (116,CURRENT_TIMESTAMP,3,'sponsor',0,2,'Sponsoren','Von folgenden Organisationen werden wir gesponsort:','',1296,1089018024);
INSERT INTO `$prefix$flip_content_text` VALUES (117,CURRENT_TIMESTAMP,3,'catering_title',0,2,'Catering','Wir bestellen alle halbe Stunde bei Luigi Pizza. Nach der Bestellung bitte bei der Orga bezahlen. Danke.','Hier bekommt ihr was zu mampfen!',1296,1282606070);
INSERT INTO `$prefix$flip_content_text` VALUES (118,CURRENT_TIMESTAMP,3,'catering_verwaltung',3,15,'Cateringverwaltung','Hier wird das Catering verwaltet, das heisst es werden Angebote und Bestellungen bearbeitet.<br/>\r\nDie Verwaltung ist in drei Bereiche aufgeteilt: Bestellungen der User, bestellte Artikel, Angebot-/Artikelverwaltung.<br/>\r\nDie Bestell&uuml;bersichten k&ouml;nnen nach Kategorie und Status gefiltert werden. F&uuml;r die User f&uuml;hrt ein Link zu seinen Details.','Bestell&uuml;bersicht, Angebot bearbeiten',4,1081270756);
INSERT INTO `$prefix$flip_content_text` VALUES (119,CURRENT_TIMESTAMP,3,'catering_edit',3,15,'Kategorienverwaltung','Hier werden Kategorien f&uuml;r Angebote erstellt und gef&uuml;llt','Kategorienverwaltung',4,1080748463);
INSERT INTO `$prefix$flip_content_text` VALUES (120,CURRENT_TIMESTAMP,3,'user_subject_turnier',0,2,'Subjects -> Turnier','Dies ist eine Liste aller Turniere.\r\nDiese Subjekte sind z.Z. nur f&uuml;r die Rechte&uuml;berpr&uuml;fung der Turnierorgas mittels right_over vorhanden.\r\n','',1296,1082288083);
INSERT INTO `$prefix$flip_content_text` VALUES (121,CURRENT_TIMESTAMP,3,'user_subject_server',0,2,'Server','Hier finden sich alle Server wieder, die für die Turniere etc. bereit gestellt werden.','Eine Liste der Server im System',1296,1282236386);
INSERT INTO `$prefix$flip_content_text` VALUES (122,CURRENT_TIMESTAMP,3,'user_registered_successfully_nom',0,2,'Erfolgreich registriert','<br />\r\n<h2 style="text-align:center">Willkommen bei xxx!</h2>\r\n<br />\r\nDein Account wurde erfolgreich erstellt, ist aber noch <strong>deaktiviert</strong>. Bitte wende dich an einen Orga um ihn aktivieren zu lassen.<br/>\r\nDanach hast du die M&ouml;glichkeit dich in das xxx-System einzuloggen, um dich beispielsweise f&uuml;r die kommende LAN-Party anzumelden.','',1296,1098430307);
INSERT INTO `$prefix$flip_content_text` VALUES (123,CURRENT_TIMESTAMP,3,'webmessage_contact',0,2,'Kontakt','<p style="text-align:justify">\r\nWenn du Fragen, Kritik oder Anregungen hast, tu dir keinen Zwang an und Teil uns deine Meinung mit!<br/>\r\nWir sind gerne bereit darauf ein zu gehen und eventuelle Probleme aus der Welt zu r&auml;umen.\r\n<p>','',1296,1105372231);
INSERT INTO `$prefix$flip_content_text` VALUES (124,CURRENT_TIMESTAMP,3,'banktransfer_main',0,2,'Überweisung','<p>\r\n  Bitte &uuml;berweise den Unkostenbeitrag f&uuml;r die Lanparty an:\r\n</p>\r\n\r\n  ','',1296,1111368792);
INSERT INTO `$prefix$flip_content_text` VALUES (125,CURRENT_TIMESTAMP,3,'banktransfer_not_allowed',0,2,'Überweisung - Zugriff verweigert','<p>\r\n  An dieser Stelle findest du &uuml;blicher Weise die Kontodaten f&uuml;r den Unkostenbeitrag.<br/>\r\n  Diese Daten werden \r\n  allerdings erst angezeigt, sobald du dich <a href="user.php?frame=login">eingeloggt</a> \r\n  und zu der <a href="lanparty.php?frame=register">Lanparty angemeldet</a> hast.\r\n</p>',NULL,0,0);
INSERT INTO `$prefix$flip_content_text` VALUES (126,CURRENT_TIMESTAMP,3,'seats_afterlegend',0,2,'','','',1296,1112447273);
INSERT INTO `$prefix$flip_content_text` VALUES (127,CURRENT_TIMESTAMP,3,'lanparty_unregister',0,2,'Abmeldung','Ich m&ouml;chte doch nicht mehr teilnehmen und melde mich hiermit von der LAN-Party ab.<br />\r\n<br />\r\nDer Account bleibt bestehen.','Hier kannst du dich von der LAN-Party abmelden',1296,1115056071);
INSERT INTO `$prefix$flip_content_text` VALUES (128,CURRENT_TIMESTAMP,3,'seats_main',0,2,'Sitzplanübersicht','Wir haben die ganze Sporthalle zur Verf&uuml;gung. Ihr k&ouml;nnt euch auf dem <a href="seats.php?frame=block&amp;blockid=1">detailierten Sitzplan</a> einen Platz reservieren und sehen, wer wo sitzt.','',1296,1118074547);
INSERT INTO `$prefix$flip_content_text` VALUES (129,CURRENT_TIMESTAMP,3,'paypal_cancelled',0,2,'Vorgang abgebrochen','Du hast den Zahlungsvorgang abgebrochen!<br />\r\nLeider k&ouml;nnen wir deinen Account nicht freischalten, solange wir den Unkostenbeitrag von dir nicht erhalten haben!','',1296,1157413795);
INSERT INTO `$prefix$flip_content_text` VALUES (130,CURRENT_TIMESTAMP,3,'paypal_no_access',0,2,'Kein Zugriff','Du hast den Unkostenbeitrag bereits bezahlt oder du besitzt nicht das Recht, um diese Seite anzuzeigen!','',1296,1157413883);
INSERT INTO `$prefix$flip_content_text` VALUES (131,CURRENT_TIMESTAMP,3,'paypal_paid',0,2,'Vielen Dank!','Du hast soeben deinen Unkostenbeitrag per PayPal &uuml;berwiesen!<br />\r\nBitte warte einen Augenblick bis dich das System freischaltet. <br /><br />\r\nSollte sich in n&auml;chster Zeit dennoch nichts tun, dann wende dich bitte umgehend an die Orgas!','',1296,1157414032);
INSERT INTO `$prefix$flip_content_text` VALUES (132,CURRENT_TIMESTAMP,3,'paypal_pay',0,2,'Mit PayPal bezahlen','Wir bieten dir hier auch die M&ouml;glichkeit den Unkostenbeitrag per PayPal zu &uuml;berweisen.\r\nNach Eingang der Zahlungsbest&auml;tigung innerhalb weniger Sekunden nach der Transaktion wird dein Account dann vom System freigeschalten werden!','',1296,1157414224);
INSERT INTO `$prefix$flip_content_text` VALUES (133,CURRENT_TIMESTAMP,3,'clan_join',0,2,'Anfrage stellen an','Hier kannst du eine Anfrage an den Clan auf Mitgliedschaft stellen.',NULL,4,1173103907);
INSERT INTO `$prefix$flip_content_text` VALUES (134,CURRENT_TIMESTAMP,3,'inet_main',0,2,'Internet freischalten','Hier kannst du dir den Internetzugang auf der LAN freischalten, damit du\r\nz.B. dein Steam updaten kannst.<br />\r\nFalls du sofort freigeschalten wirst, erhälst du umgehend eine Webmessage, der\r\ndu weitere Infos entnehmen kannst.<br />\r\nSollten bereits User vor dir freigeschalten worden sein, dann wirst du automatisch<br />\r\nin eine Warteschlange eingereiht und wirst freigeschalten, wenn du an der Reihe bist.',NULL,0,0);
INSERT INTO `$prefix$flip_content_text` VALUES (135,CURRENT_TIMESTAMP,3,'user_subject_type',0,2,'Subjekttypen','Hier sind alle Subjekttypen, die im FLIP vorhanden sind zusammengefasst','Alle Subjekttypen im FLIP',1296,1191789778);
INSERT INTO `$prefix$flip_content_text` VALUES (137,CURRENT_TIMESTAMP,3,'sysinfo_modules',0,2,'Modules','Hier kannst du die von PHP geladenen Module in Kurzfassung einsehen und außerdem erkennen, ob alle vom FLIP benötigten\r\nModule geladen worden sind.\r\n<br /><br />\r\n<span style="color: #FF0000;">Rot </span> sind Module, die unbedingt erforderlich sind, damit einige Funktionen korrekt ausgeführt werden.<br />\r\n<span style="color: #AAAA00;">Gelb</span> sind Module, ohne die das FLIP auch noch funktioniert, aber dafür mit verminderter Qualität und/oder Leistung. ','Beschreibung der geladenen und vom FLIP benötigten PHP Module',4,1280349766);
INSERT INTO `$prefix$flip_content_text` VALUES (138,CURRENT_TIMESTAMP,3,'user_subject_clan',0,2,'Clans','Hier sind alle Clans in diesem System aufgelistet.','',1296,1282005366);
INSERT INTO `$prefix$flip_content_text` VALUES (139,CURRENT_TIMESTAMP,3,'user_subject_systemuser',0,2,'Systemuser','System-Subjekte sind wie ganz normale User - da sie allerdings ständig vom FLIP selbst gebraucht werden, können diese nicht gelöscht werden.\r\n<br />\r\nZudem haben sie normalerweise keine E-Mail - eine Ausnahme bildet hier "System", da seine E-Mail als Absender von Rundmails etc. verwendet wird.','Eine Liste der User, die vom System benötigt werden',1296,1282496507);
INSERT INTO `$prefix$flip_content_text` VALUES (140,CURRENT_TIMESTAMP,1,'catering_edit_item',0,2,'Artikel bearbeiten','Hier siehst du alle verhandenen Artikel in der aktuellen Catering Kategorie.','Die Artikel des Caterings bearbeiten',1296,1282607392);
INSERT INTO `$prefix$flip_content_text` VALUES (141,CURRENT_TIMESTAMP,3,'shop_default_page',0,2,'Catering','<div style="text-align: center">\r\nWillkommen im Shopsystem unseres Caterings!\r\n<br /><br />\r\nHier findest du eine Übersicht über unser aktuelles Angebot.\r\nNimm dir die Zeit und such\' dir was leckeres aus - wir freuen uns auf deine Bestellung!\r\n</div>','Zeigt dem User eine Übersicht aller Kategorien und deren Beschreibungen',1296,1282963899);
INSERT INTO `$prefix$flip_content_text` VALUES (142,CURRENT_TIMESTAMP,1,'shop_shoppingcart_page',0,2,'Mein Warenkorb','Alles was in der Tabelle unten zu sehen ist, liegt in deinem Warenkorb.</br >\r\nWie in der Realität hast du bereits Anspruch auf den Artikel in der angegebenen Menge, d.h.<br />\r\nkeiner kann ihn dir vor deiner Nase wegschnappen, es sei denn du löschst ihn wieder heraus.','Zeigt eine Übersicht über die aktuell im Warenkob liegenden Artikel',1296,1283298917);
INSERT INTO `$prefix$flip_content_text` VALUES (143,CURRENT_TIMESTAMP,1,'shop_mybalance_page',0,2,'Mein Guthaben','<div style="text-align: center">\r\nHier kannst du das Guthaben sehen, welches du für unser Cateringsystem nutzen kannst.<br />\r\nFalls du noch nichts hinauf geladen hast, oder du erneut aufladen willst, wende dich einfach an jemandem vom Cateringteam!\r\n</div>','Zeigt das Guthaben des aktuellen Benutzers an',1296,1282862472);
INSERT INTO `$prefix$flip_content_text` VALUES (144,CURRENT_TIMESTAMP,1,'shop_admin_default_page',0,2,'Cateringverwaltung','Hier hast du die Möglichkeit, das Cateringmodul einzustellen und die Bestellungen zu verwalten.','Verwaltet das Catering',1296,1282926429);
INSERT INTO `$prefix$flip_content_text` VALUES (145,CURRENT_TIMESTAMP,3,'shop_admin_groups_page',0,2,'Artikelgruppen','Die unten liegenden Gruppen enthalten die im System vorhandenen Artikel und erleichtern den Usern sich zu orientieren.','Text bei der Verwaltung der Artikelgruppen',1296,1282955291);
INSERT INTO `$prefix$flip_content_text` VALUES (146,CURRENT_TIMESTAMP,1,'shop_admin_items_page',0,2,'Artikel','Hier kannst du alle Artikel des Shops verwalten.','Text bei der Verwaltung der Artikel',1296,1282966672);
INSERT INTO `$prefix$flip_content_text` VALUES (147,CURRENT_TIMESTAMP,3,'shop_admin_users_page',0,2,'Bestellungen pro User','Hier können die User eingesehen werden, die im System bereits bestellt haben.<br />\r\nKlicke auf den Usernamen, um die Details einer Bestellung zu sehen - mit einem Klick auf sein Guthaben kannst du dieses verändern.','Beschreibung der Adminfunktionen auf der Bestellungen pro User Seite',1296,1283034085);
INSERT INTO `$prefix$flip_content_text` VALUES (148,CURRENT_TIMESTAMP,1,'shop_admin_user_page',0,2,'Bestellungen eines Users','Hier sind sämtliche Bestellungen (Transaktionen) des betreffenden Users aufgelistet.<br />\r\nFalls nötig, lassen sich die Daten hier auch ändern oder löschen.','Text bei der Bestellauflistung eines Users im Adminbereich',1296,1283043600);
INSERT INTO `$prefix$flip_content_text` VALUES (149,CURRENT_TIMESTAMP,1,'shop_admin_order_by_item',0,2,'Bestellungen pro Artikel','<center>\r\nHier siehst du, wie oft ein Artikel von einem User bestellt wurde, sowie einige Statistische Daten.<br />\r\nAußerdem kann hier der Umsatz entnommen werden.\r\n<br /><br />\r\n<u>Ein Artikel wird mitgezählt, sobald er bezahlt wurde.</u>\r\n</center>','Text bei der Statistiktabelle über die Bestellungen',1296,1283128225);
INSERT INTO `$prefix$flip_content_text` VALUES (150,CURRENT_TIMESTAMP,1,'shop_admin_balance_accounts',0,2,'Guhaben verwalten','<center>\r\nDie unten aufgelisteten Benutzer haben bereits Geld auf ihr Konto im Shop aufgeladen und möglicherweise auch ausgegeben.<br />\r\nHier kannst du das Guthaben eines Users einsehen und ggf. verändern, falls es erforderlich wird.\r\n</center>','Beschreibung der Guthabenverwaltung',1296,1283189970);
INSERT INTO `$prefix$flip_content_text` VALUES (151,CURRENT_TIMESTAMP,3,'menu_intern',3,15,'Orga Team only!','Orga Event Mgmt!','',1296,1361377317);
INSERT INTO `$prefix$flip_content_text` VALUES (152,CURRENT_TIMESTAMP, 3, 'user_status_paid_clan', 107, 2, 'Bezahlt','Der CLAN hat bezahlt und du kannst dir einen Sitzplatz aussuchen.','Der CLAN hat fuer seine CLAN-Member bereits bezahlt', 1296, 1526424680);
INSERT INTO `$prefix$flip_content_text` VALUES (153,CURRENT_TIMESTAMP, 1, 'manual_clan_seats', 0, 2, 'Anleitung - Clan-Sitzpl&auml;tze','<div class="row"><div class="col-xs-12 col-sm-12 col-md-10 col-lg-8"><table class="table table-striped table-hover"><tbody><tr><td><button class="btn btn-primary btn-block" type="button"><i class="fas fa-stop"></i></button></td><td><strong><span style="font-size: 1em;">Sitzpl&auml;tze f&uuml;r den Clan zuordnen</span></strong><br><span style="font-size: 1em;">Auf unserem Sitzplan ist es m&ouml;glich, dass der Clan Leader f&uuml;r seinem Clan eine bestimmte Anzahl von Pl&auml;tzen (Standard 8 Pl&auml;tze) zuordnen kann. Dazu m&uuml;sst ihr vorab einen Clan anlegen falls noch nicht geschehen. Diese Clan Sitzpl&auml;tze werden danach "blau" dargestellt.</span></td></tr><tr><td><button class="btn btn-warning btn-block" type="button"><i class="fas fa-hand-paper" style="color: blue;"></i></button><td><td><strong><span style="font-size: 1em;">Vormerken von Clan Pl&auml;tzen</span></strong><br /><span style="font-size: 1em;">Jedes Clan Mitglied kann sich bis zu zwei Clan &ndash; Pl&auml;tze (blau) vormerken, ohne daf&uuml;r bezahlt zu haben. Nicht-Mitglieder k&ouml;nnen klarerweise Pl&auml;tze innnerhalb des Clans nicht vormerken.</span><br /><span style="font-size: 1em;">Es k&ouml;nnen von jedem LAN-Teilnehmer weiterhin Pl&auml;tze (gr&uuml;n) wie gehabt vorgemerkt werden. Vorgemerkte Pl&auml;tze werden "gelb" dargestellt, gelten aber nicht als reservierte Pl&auml;tze, sondern zeigen nur die Absicht der Teilnehmer, diese in Anspruch nehmen zu wollen.</span></td></tr><tr><td><button class="btn btn-danger btn-block" type="button"><i class="fas fa-stop"></i></button></td><td><strong><span style="font-size: 1em;">Reservieren von Pl&auml;tzen und Clan-Pl&auml;tzen</span></strong><br /><span style="font-size: 1em;">Reservierte Pl&auml;tze werden im Sitzplan "rot" dargestellt. Um Sitzpl&auml;tze fix reservieren zu k&ouml;nnen, muss daf&uuml;r vorab bezahlt werden. Dazu gibt es zwei Varianten:</span><br /><ul><li style="padding-left: 30px;"><span style="font-size: 1em;">Variante A &ndash; Solo-Trip<br /></span><span style="font-size: 1em;">Jeder Teilnehmer &uuml;berweist f&uuml;r sich selbst den Teilnahmebetrag. Danach erfolgt von uns die Freischaltung f&uuml;r die Sitzplatzreservierung. Damit k&ouml;nnen sowohl Sitzpl&auml;tze innerhalb des eigenen Clans reserviert werden, als auch Sitzpl&auml;tze, die keinem Clan zugeordnet sind.</span></li><li style="padding-left: 30px;"><span style="font-size: 1em;">Variante B &ndash; Clan-Trip<br /></span><span style="font-size: 1em;">Der Clan-Leader oder der daf&uuml;r Zust&auml;ndige im Clan, &uuml;berweist den Teilnahmebetrag f&uuml;r eine bestimmte Anzahl an Clan-Sitzpl&auml;tzen. Danach wird dem Clan diese Anzahl an Sitzpl&auml;tzen zugeordnet. Das k&ouml;nnen weniger oder auch mehr als die per Standard definierten 8 Clan-Sitzpl&auml;tze sein.<br /></span><span style="font-size: 1em;">Alle Clan-Mitglieder k&ouml;nnen mit dieser Variante Pl&auml;tze, die dem Clan zugeordnet sind, fix f&uuml;r sich reservieren (rot). Sitzpl&auml;tze die keinem Clan zugeordnet sind, k&ouml;nnen damit nicht reserviert werden. Der Clan selbst kann durchaus mehr Mitglieder haben, als insgesamt Pl&auml;tze bezahlt worden sind. Wer also von den Mitgliedern die Sitzpl&auml;tze in Anspruch nehmen wird, bleibt euch &uuml;berlassen.</span></li></ul><span style="font-size: 1em;"><span style="color: #ff0000;">Achtung:</span> Ihr m&uuml;sst euch f&uuml;r eine der beiden Varianten entscheiden. Eine Kombination davon ist nicht m&ouml;glich.</span></td></tr><tr><td><button class="btn btn-primary seatbutton" style="padding: 0; margin: 0;" type="button"> <img alt="" src="tpl/VulkanLAN_bstrap4/images/icon.png" width="32" height="32" /> <button></td><td><strong><span style="font-size: 1em;">Optischer Aufputz</span></strong><br /><span style="font-size: 1em;">Jeder Platz, der einem Clan zugeordnet ist, wird mit dem Bild des jeweiligen Clans markiert, sofern die Clan-Leader ein Bild hochgeladen haben. Falls dieses Bild speziell f&uuml;r den Sitzplan ungeeignet ist, k&ouml;nnen die Clan-Leader ein weiteres Bild names "Clan-Icon" hochladen. Dieses mu&szlig; eine Aufl&ouml;sung von 32x32 Pixel haben.</span></td></tr><tbody></table><span style="font-size: 1em;"></span><hr /><span style="font-size: 1.2em;"><strong>FAQ:</strong></span><br /><br /><strong><span style="font-size: 1em;">Ich m&ouml;chte vorab mehr als 8 Sitzpl&auml;tze meinem Clan zuordnen. Was kann ich tun?</span></strong><br /><span style="font-size: 1em;">Dazu halte bitte per <a href="mailto:info@event.local">E-Mail</a> R&uuml;cksprache <a href="#" target="_blank" rel="noopener">mit uns</a>. Wir werden dann das Limit entsprechend anpassen.</span><br /><br /><strong><span style="font-size: 1em;">Kann es f&uuml;r einen Clan mehrere Leader geben?</span></strong><br /><span style="font-size: 1em;">Ja, es k&ouml;nnen auch mehrere Clan Mitglieder zu Leadern ernannt werden.</span><br /><br /><strong><span style="font-size: 1em;">Kann ich auch Leader von mehreren Clans sein?</span></strong><br /><span style="font-size: 1em;">Obwohl das zwar in der Regel kaum zutreffen wird, ist diese Funktion in unserem Verwaltungssystem abgebildet. Beim Reservieren von Clan-Sitzpl&auml;tzen musst du ausw&auml;hlen, f&uuml;r welchen Clan dies geschehen soll.</span><br /><br /><strong><span style="font-size: 1em;">Kann ich Mitglied von mehreren Clans sein?</span></strong><br /><span style="font-size: 1em;">Ja, allerdings solltest du das dann mit den jeweiligen Clan-Leadern abkl&auml;ren speziell im Hinblick zur Bezahlung der Sitzpl&auml;tze.</span><br /><br /><strong><span style="font-size: 1em;">Muss ich als Clan-Mitglied die vom Clan reservierten Pl&auml;tze verwenden?</span></strong><br /><span style="font-size: 1em;">Du kannst zus&auml;tzlich zu den Pl&auml;tzen deines Clans auch andere Pl&auml;tze vormerken bzw. reservieren, die noch keinem Clan zugeordnet sind. Falls du jedoch nicht selbst f&uuml;r deinen Platz bezahlt hast, sondern dein Clan dies gesammelt f&uuml;r alle Clan-Pl&auml;tze gemacht hat, dann kannst du nur Sitzpl&auml;tze reservieren, die deinem Clan zugeordnet sind.</span><br /><br /><strong><span style="font-size: 1em;">Die reservierten Sitzpl&auml;tze meines Clans haben sich ohne mein Zutun ge&auml;ndert?</span></strong><br /><span style="font-size: 1em;">Um eine zu l&ouml;chrige Sitzplatzverteilung zu vermeiden, kann es vorkommen, dass wir die Zuteilung der Clan-Sitzpl&auml;tze entsprechend optimieren.</span><br /><br /><strong><span style="font-size: 1em;">Ich habe bereits gesammelt f&uuml;r alle Clan Pl&auml;tze bezahlt, ben&ouml;tige jedoch noch zus&auml;tzliche Pl&auml;tze. Was soll ich tun?</span></strong><br /></div><div class="col-md-2 col-lg-4"></div></div>','', 1296, 1525132107);
INSERT INTO `$prefix$flip_content_text` VALUES (154,CURRENT_TIMESTAMP, 3, 'dsgvo_datenverarbeitung', 0, 2, 'DSGVO - Information zur Datenverarbeitung','<p>Vereinsname</p><p>\r\n</p><p>\r\n</p><p>Anschrift und Links des Vereins</p><p>\r\n</p><p>Der Schutz deiner pers&ouml;nlichen Daten ist uns ein besonderes Anliegen. Wir verarbeiten deine Daten daher ausschlie&szlig;lich auf Grundlage der gesetzlichen Bestimmungen (DSGVO, TKG 2003). In diesen Datenschutzinformationen informieren wir Sie &uuml;ber die wichtigsten Aspekte der Datenverarbeitung im Rahmen unserer Website.</p><p>\r\n</p><p>Die ausf&uuml;hrlichen Informationen zum Datenschutz und Haftungshinweisen findest du in unserem Impressum.</p><p>\r\n</p>','DSGVO - Datenschutzgrungverordnung', 1296, 1526659494);
INSERT INTO `$prefix$flip_content_text` VALUES (155,CURRENT_TIMESTAMP, 3, 'dsgvo_einwilligung', 0, 2, 'Einwilligung zur Datenverarbeitung','Datenschutz ist Vertrauenssache und deine Daten sind uns wichtig!</p> <p>\r\n</p> <p>Ich stimme insbesonder im Sinne des Datenschutzrechts und Telekomunikationsgesetzes zu, dass meine Pers&ouml;nlichen Daten, n&auml;mlich; Name, Nikname, Adresse, IP-Adresse, E-Mail-Adresse, zu folgenden Zwecken: der Mitgliederverwaltung und Eventteilnahmeabwicklung wie Zahlungsart, Zahlungsdaten, Zahlungsstatus und Mitgliedsstatus, weiters der E-Sportergebnismanagement (zb.Turnierverwaltung), Information, Aussch&uuml;ttung von Turnierpreisen, Buchhaltung, Abrechnung und Zusendung von Informationen, verarbeitet und automationsunterst&uuml;tz verwendet werden d&uuml;rfen. Ohne diese Daten ist die Erbringung unserer Dienstleistungen unm&ouml;glich.</p> <p>\r\n</p> <p>Ich stimme weiters zu, das die notwendigen Daten auf Anfrage an die zust&auml;ndigen Beh&ouml;rden weitergeleitet werden.</p> <p>\r\n</p> <p>&Uuml;berdies stimme ich zu, das Personenidentifikations- und Vertragsdaten zur Information und Beratung &uuml;ber Neuigkeiten verwendet werden d&uuml;rfen.</p> <p>\r\n</p> <p>Keinesfalls von dieser Zustimmung erfasst ist allerdings die Verwendung von Sensiblen Daten (rassistische oder ethnische Herkunft, politische, religi&ouml;se oder philosophische Weltanschauung, Sexualleben, Gewerkschaftszugeh&ouml;rigkeit, Gesundheitsdaten).</p> <p>\r\n</p> <p>Es besteht keine Absicht Ihre Daten an ein Drittland oder eine internationale Organisation zu &uuml;bermitteln. Es besteht keine Absicht Ihre Daten f&uuml;r automatisierte Entscheidungsfindung einschlie&szlig;lich Profiling (Datenanalyse zu Verhalten, Gewohnheiten, Pr&auml;ferenzen) zu verarbeiten.</p> <p>\r\n</p> <p>Diese Zustimmung kann ich jederzeit mittels Brief und per E-Mail (info@event.local) widerrufen, wobei ich auch berechtigt bin, bezogen auf die Daten Richtigstellung, Auskunft und L&ouml;schung zu verlangen.</p>','Einwilligung zur Datenberarbeitung', 1296, 1526869845);
INSERT INTO `$prefix$flip_content_text` VALUES (156,CURRENT_TIMESTAMP, 3, 'dsgvo_information', 4, 2, 'DSGVO - Informationspflicht','Datenschutz ist Vertrauenssache und deine Daten sind uns wichtig!</p> <p style="margin-bottom: 0cm">\r\n</p> <p>Die Daten werden vom Verein &quot;Dagobert Duck&quot;, zum Zweck der Mitgliederverwaltung und Veranstaltungesabwicklung, E-Sportergebnismanagement (zb.Turnierverwaltung), Information, Aussch&uuml;ttung von Turnierpreisen, Buchhaltung, Abrechnung und Zusendung von Informationen, verarbeitet und automationsunterst&uuml;tz verwendet. Es handelt sich dabei um die Datenkategorien Name, Nikname, Adresse, IP-Adresse, E-Mail-Adresse, Zahlungsart, Zahlungsdaten, Zahlungsstatus und Mitgliedsstatus.</p> <p>\r\n</p> <p>Es besteht keine Absicht Ihre Daten an ein Drittland oder eine internationale Organisation zu &uuml;bermitteln.</p> <ul> 	<p style="margin-bottom: 0cm">\r\n</p> 	<li><p style="margin-bottom: 0cm">Die personenbezogenen Daten, Name, 	Nikname, Adresse, E-Mail-Adresse, werden f&uuml;r die Dauer der 	Mitgliedschaft gespeichert.\r\n</p> 	<li><p style="margin-bottom: 0cm">Die Daten, wie Zahlungsart, 	Zahlungsdaten, Zahlungsstatus und Teilnahmestatus, f&uuml;r die 	Abwickung und Abrechnung der Veranstaltungen und 	Mitgliederverwaltung werden drei Jahre gespeichert.\r\n</p> 	<li><p style="margin-bottom: 0cm">Der Nickname wird im &ouml;ffentlichen 	Interesse liegenden Archivzwecken und berechtigte Interessen des 	Verantwortlichen &ouml;ffentlich zug&auml;nglich gemacht.\r\n</p> 	<li><p>Der Nickname wird bei Teilnahme an den entsprechenden 	Turnieren f&uuml;r die Aussch&uuml;ttung von Turnierpreisen an 	dritte weitergeleitet.\r\n</p> </ul> <p>\r\n</p> <p>Ab dem Zeitpunkt an dem du dich f&uuml;r die aktuelle LAN-Party oder Esport Veranstaltung angemeldet hast, bis zur Durchf&uuml;hrung und Beendigung, sind von dir folgende Daten; <br/> Nikname, Status, Sitzplatz, Homepage und Beschreibung von jedem einsehbar. <br/> Alle oben aufgef&uuml;hrten Daten als auch die personenbezogenen Daten sind nur von dir und dem Organisations Team f&uuml;r die Dauer deiner Mitgliedschaft und oder f&uuml;r die Dauer der Teilnahme an der aktuell ausgetragenen Veranstaltung einsehbar.</p> <p>\r\n</p> <p>Du hast jederzeit das Recht auf Auskunft &uuml;ber die Daten, Berichtigung, L&ouml;schung und Einschr&auml;nkung der Verarbeitung der Daten sowie ein Widerspruchsrecht gegen die Verarbeitung der Daten und das Recht auf Daten&uuml;bertragbarkeit.</p> <p>\r\n</p> <p>Du hast das Recht auf Beschwerde bei der Aufsichtsbeh&ouml;rde.</p> <p>\r\n</p> <p>Die Bereitstellung der Daten Name, Nikname, Adresse, Geburtsdatum, IP-Adresse, E-Mail-Adresse, ist f&uuml;r die Erf&uuml;llung der Mitgliederverwaltung und der Abwicklung der Veranstaltungen notwendig. Ohne diese Daten ist eine Erbringung unserer Dienstleistung unm&ouml;glich.</p> <p>\r\n</p> <p>Es besteht keine Absicht Ihre Daten f&uuml;r automatisierte Entscheidungsfindung einschlie&szlig;lich Profiling (Datenanalyse zu Verhalten, Gewohnheiten, Pr&auml;ferenzen&hellip;) zu verarbeiten.</p> <p>\r\n</p> <p>Du hast das Recht deine gegebene Einwilligung jederzeit per Brief und per E-Mail zu widerrufen. (falls die Verarbeitung auf der Rechtsgrundlage einer gegebenen Einwilligung beruht)</p> <p>\r\n</p> <p><b>Information &uuml;ber E-Sportergebnismanagement </b> </p> <p>\r\n</p> <p>Aufgrund der Einwilligung der betroffenen Person nach Art. 6 Abs. 1 lit. a DSGVO bzw. der Erf&uuml;llung einer vertraglichen bzw. rechtlichen Verpflichtung des Verantwortlichen nach Art. 6 Abs. 1 lit. b und c bzw. f DSGVO werden die personenbezogenen Daten der betroffenen Person, soweit diese f&uuml;r die Leistungs-/Ergebniserfassung bzw. Ergebnismanagement im Zusammenhang mit der Anmeldung oder Teilnahme an (E-Sportlichen) Veranstaltungen oder Wettk&auml;mpfen erforderlich sind, gespeichert und auch nach Art. 17 Abs. 3 in Verbindung mit Art. 89 DSGVO f&uuml;r im &ouml;ffentlichen Interesse liegende Archivzwecke und berechtigte Interessen des Verantwortlichen gespeichert und &ouml;ffentlich zug&auml;nglich gemacht. Dies wird von der betroffenen Person ausdr&uuml;cklich zur Kenntnis genommen.</p>','Information über die automatiosnunterstütze Datenverarbeitung', 1306, 1526869856);
INSERT INTO `$prefix$flip_content_text` VALUES (157,CURRENT_TIMESTAMP, 3, 'user_status_registered_console', 0, 2, 'Angemeldet als Konsolenspieler','Du musst noch den Teilnahmebetrag auf das Konto &uuml;berweisen.','Du bist angemeldet als Konsolenspieler', 1296, 1537984515);
INSERT INTO `$prefix$flip_content_text` VALUES (158,CURRENT_TIMESTAMP, 3, 'lanparty_registerconsole', 0, 2, 'Als Konsolenspieler anmelden','<p>Du willst bei der Konsolenmeisterschaft dabei sein?</p>\r\n<p>Dann musst du &uuml;ber 14 Jahre alt sein und die <b>Teilnamebestimmungen (AGB)</b> akzeptieren.</p>','Konsolenspieler ohne Sitzplatz anmelden', 1296, 1537984575);
INSERT INTO `$prefix$flip_content_text` VALUES (159,CURRENT_TIMESTAMP, 3, 'lanparty_unregisterconsole', 0, 2, 'Abmeldung Konsolenspieler','Ich m&ouml;chte doch <b> nicht mehr teilnehmen </b> und melde mich hiermit von dem E-Sport Event / LAN-Party ab.<br /><br />Der Account bleibt bestehen.','Hier kannst du dich von der E-Sport Event / LAN-Party abmelden', 1296, 1537984784);
INSERT INTO `$prefix$flip_content_text` VALUES (160,CURRENT_TIMESTAMP, 1, 'Turnierpreise', 0, 2, 'Turnierpreise', 'Turnierpreise auf dem Event:', 'Auflistung aller Turnierpreise', 1296, 1550082790);
INSERT INTO `$prefix$flip_content_text` VALUES (161,CURRENT_TIMESTAMP, 1, 'faq', 0, 2, 'FAQ', 'Event FAQ', 'Event FAQ', 1296, 1550082819);
INSERT INTO `$prefix$flip_content_text` VALUES (162,CURRENT_TIMESTAMP, 3, 'lanparty_agb', 0, 2, 'lanparty_agb', 'Teilnahmebedinungen / AGB', 'Teilnahmebedinungen / AGB', 1296, 1550082991);
INSERT INTO `$prefix$flip_content_text` VALUES (163,CURRENT_TIMESTAMP, 3, 'impressum', 0, 2, 'impressum', 'Impressum', 'Impressum Text', 1296, 1550082991);
INSERT INTO `$prefix$flip_content_text` VALUES (164,CURRENT_TIMESTAMP, 3, 'dsgvo_revoke', 4, 2, 'DSGVO - Widerrufen', '<a href=\"mailto:info@event.local?subject=Widerrufen der Datenverarbeitung und beenden der Mitgliedschaft&amp;body=Hallo LAN Orga Team , Hiermit widerrufe ich die Datenverarbeitung, bende meine Mitgliedschaft und veranlasse die L&amp;aumlung meines LAN Member Accounts.\" class=\"btn btn-danger ml-1\" role=\"button\"><i class="far fa-minus-square"></i>&nbsp; Widerrufen </a>', 'Widerrufen der automatiosnunterstütze Datenverarbeitung', 1296, 1557774851);
INSERT INTO `$prefix$flip_content_text` VALUES (165,CURRENT_TIMESTAMP, 3, 'user_account_revoke', 0, 2, 'Account-unwiederbringlich-loeschen', '<a href=\"mailto:info@event.local\" class=\"btn btn-danger ml-1\" role=\"button\"><i class=\"far fa-minus-square\"></i>&nbsp; Widerrufen </a> Widerrufen und loeschen des LAN Member Accounts.', 'Veranlassen das der Account und alle Backups nach DSGVO geloescht wird.', 1296, 1557775742);
INSERT INTO `$prefix$flip_content_text` VALUES (166,CURRENT_TIMESTAMP,3,'banktransfer_paid',0,2,'Überweisung - Bereits erfolgt','<p>\r\n  Vielen Dank, dass du bereits überwiesen hast! \r\n Wir freuen uns jetzt schon auf dein Kommen!\r\n</p>',NULL,0,0);
# --------------------------- /flip_content_text -------------------------- #


# --------------------------- flip_featuresys_accounts --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_featuresys_accounts`;
CREATE TABLE `$prefix$flip_featuresys_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `status` enum('requested','accepted','denied') COLLATE utf8mb4_bin DEFAULT NULL,
  `accounttype` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_featuresys_accounts -------------------------- #


# --------------------------- flip_featuresys_types --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_featuresys_types`;
CREATE TABLE `$prefix$flip_featuresys_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `maxcount` int(11) NOT NULL DEFAULT '0',
  `msg_accepted` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `msg_denied` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `msg_requested` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_featuresys_types -------------------------- #


# --------------------------- flip_forum_groups --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_forum_groups`;
CREATE TABLE `$prefix$flip_forum_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `caption` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `sort_index` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `i_sort` (`sort_index`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze:
INSERT INTO `$prefix$flip_forum_groups` VALUES (1,CURRENT_TIMESTAMP,'Internes Forum',1062423739);
INSERT INTO `$prefix$flip_forum_groups` VALUES (2,CURRENT_TIMESTAMP,'Öffentliches Forum',1062423840);
INSERT INTO `$prefix$flip_forum_groups` VALUES (3,CURRENT_TIMESTAMP,'Internes Forum - Sonstiges',1062423771);
INSERT INTO `$prefix$flip_forum_groups` VALUES (4,CURRENT_TIMESTAMP,'Sonstiges',1062423869);
# --------------------------- /flip_forum_groups -------------------------- #


# --------------------------- flip_forum_posts --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_forum_posts`;
CREATE TABLE `$prefix$flip_forum_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `poster_id` int(11) NOT NULL DEFAULT '0',
  `post_time` int(11) NOT NULL DEFAULT '0',
  `changer_id` int(11) NOT NULL DEFAULT '0',
  `change_time` int(11) NOT NULL DEFAULT '0',
  `allow_html` tinyint(1) NOT NULL DEFAULT '0',
  `allow_fliptags` tinyint(1) NOT NULL DEFAULT '0',
  `text` mediumtext COLLATE utf8mb4_bin NOT NULL,
  `thread_id` int(11) NOT NULL DEFAULT '0',
  `allow_nl2br` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `i_time` (`post_time`),
  KEY `i_thread` (`thread_id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin AUTO_INCREMENT=1;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_forum_posts -------------------------- #


# --------------------------- flip_forum_threads --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_forum_threads`;
CREATE TABLE `$prefix$flip_forum_threads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `forum_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `posts` int(11) NOT NULL DEFAULT '0',
  `last_change` int(11) DEFAULT '0',
  `last_poster` int(11) DEFAULT '0',
  `poster_id` int(11) NOT NULL DEFAULT '0',
  `sticky` tinyint(1) NOT NULL DEFAULT '0',
  `post_right` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `i_change` (`last_change`),
  KEY `i_forum` (`forum_id`),
  KEY `i_sticky` (`sticky`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_forum_threads -------------------------- #


# --------------------------- flip_forum_watches --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_forum_watches`;
CREATE TABLE `$prefix$flip_forum_watches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` enum('forum','thread') COLLATE utf8mb4_bin NOT NULL DEFAULT 'forum',
  `target_id` int(11) NOT NULL DEFAULT '0',
  `owner_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_forum_watches -------------------------- #


# --------------------------- flip_gallery_albums --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_gallery_albums`;
CREATE TABLE `$prefix$flip_gallery_albums` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(60) CHARACTER SET latin1 NOT NULL,
  `catid` int(11) NOT NULL,
  `titlepicid` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `position` (`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_gallery_albums -------------------------- #


# --------------------------- flip_gallery_categories --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_gallery_categories`;
CREATE TABLE `$prefix$flip_gallery_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `parent_id` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `position` (`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin AUTO_INCREMENT=1;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_gallery_categories -------------------------- #


# --------------------------- flip_gallery_pics --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_gallery_pics`;
CREATE TABLE `$prefix$flip_gallery_pics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `filename` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `album_id` int(11) NOT NULL,
  `clicks` int(60) NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `position` (`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin AUTO_INCREMENT=1;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_gallery_pics -------------------------- #


# --------------------------- flip_gb_entry --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_gb_entry`;
CREATE TABLE `$prefix$flip_gb_entry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date` int(11) NOT NULL DEFAULT '0',
  `author` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `title` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `text` mediumtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`),
  KEY `i_date` (`date`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_gb_entry -------------------------- #


# --------------------------- flip_gb_icons --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_gb_icons`;
CREATE TABLE `$prefix$flip_gb_icons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `text` varchar(20) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `icon` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze:
INSERT INTO `$prefix$flip_gb_icons` VALUES (1,CURRENT_TIMESTAMP,':)','images/smilies/smile.gif');
INSERT INTO `$prefix$flip_gb_icons` VALUES (2,CURRENT_TIMESTAMP,':(','images/smilies/sad.gif');
INSERT INTO `$prefix$flip_gb_icons` VALUES (3,CURRENT_TIMESTAMP,';)','images/smilies/zwinker.gif');
INSERT INTO `$prefix$flip_gb_icons` VALUES (4,CURRENT_TIMESTAMP,':D','images/smilies/grins.gif');
# --------------------------- /flip_gb_icons -------------------------- #


# --------------------------- flip_i18n_countrycodes --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_i18n_countrycodes`;
CREATE TABLE `$prefix$flip_i18n_countrycodes` (
  `iso` char(2) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(80) COLLATE utf8mb4_bin NOT NULL,
  `printable_name` varchar(80) COLLATE utf8mb4_bin NOT NULL,
  `iso3` char(3) COLLATE utf8mb4_bin DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`iso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze:
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('AF','AFGHANISTAN','Afghanistan','AFG',4);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('AL','ALBANIA','Albania','ALB',8);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('DZ','ALGERIA','Algeria','DZA',12);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('AS','AMERICAN SAMOA','American Samoa','ASM',16);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('AD','ANDORRA','Andorra','AND',20);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('AO','ANGOLA','Angola','AGO',24);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('AI','ANGUILLA','Anguilla','AIA',660);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('AQ','ANTARCTICA','Antarctica',NULL,NULL);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('AG','ANTIGUA AND BARBUDA','Antigua and Barbuda','ATG',28);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('AR','ARGENTINA','Argentina','ARG',32);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('AM','ARMENIA','Armenia','ARM',51);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('AW','ARUBA','Aruba','ABW',533);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('AU','AUSTRALIA','Australia','AUS',36);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('AT','AUSTRIA','Austria','AUT',40);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('AZ','AZERBAIJAN','Azerbaijan','AZE',31);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('BS','BAHAMAS','Bahamas','BHS',44);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('BH','BAHRAIN','Bahrain','BHR',48);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('BD','BANGLADESH','Bangladesh','BGD',50);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('BB','BARBADOS','Barbados','BRB',52);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('BY','BELARUS','Belarus','BLR',112);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('BE','BELGIUM','Belgium','BEL',56);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('BZ','BELIZE','Belize','BLZ',84);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('BJ','BENIN','Benin','BEN',204);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('BM','BERMUDA','Bermuda','BMU',60);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('BT','BHUTAN','Bhutan','BTN',64);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('BO','BOLIVIA','Bolivia','BOL',68);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('BA','BOSNIA AND HERZEGOVINA','Bosnia and Herzegovina','BIH',70);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('BW','BOTSWANA','Botswana','BWA',72);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('BV','BOUVET ISLAND','Bouvet Island',NULL,NULL);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('BR','BRAZIL','Brazil','BRA',76);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('IO','BRITISH INDIAN OCEAN TERRITORY','British Indian Ocean Territory',NULL,NULL);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('BN','BRUNEI DARUSSALAM','Brunei Darussalam','BRN',96);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('BG','BULGARIA','Bulgaria','BGR',100);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('BF','BURKINA FASO','Burkina Faso','BFA',854);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('BI','BURUNDI','Burundi','BDI',108);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('KH','CAMBODIA','Cambodia','KHM',116);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('CM','CAMEROON','Cameroon','CMR',120);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('CA','CANADA','Canada','CAN',124);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('CV','CAPE VERDE','Cape Verde','CPV',132);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('KY','CAYMAN ISLANDS','Cayman Islands','CYM',136);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('CF','CENTRAL AFRICAN REPUBLIC','Central African Republic','CAF',140);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('TD','CHAD','Chad','TCD',148);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('CL','CHILE','Chile','CHL',152);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('CN','CHINA','China','CHN',156);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('CX','CHRISTMAS ISLAND','Christmas Island',NULL,NULL);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('CC','COCOS (KEELING) ISLANDS','Cocos (Keeling) Islands',NULL,NULL);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('CO','COLOMBIA','Colombia','COL',170);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('KM','COMOROS','Comoros','COM',174);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('CG','CONGO','Congo','COG',178);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('CD','CONGO, THE DEMOCRATIC REPUBLIC OF THE','Congo, the Democratic Republic of the','COD',180);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('CK','COOK ISLANDS','Cook Islands','COK',184);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('CR','COSTA RICA','Costa Rica','CRI',188);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('CI','COTE D\'IVOIRE','Cote D\'Ivoire','CIV',384);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('HR','CROATIA','Croatia','HRV',191);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('CU','CUBA','Cuba','CUB',192);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('CY','CYPRUS','Cyprus','CYP',196);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('CZ','CZECH REPUBLIC','Czech Republic','CZE',203);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('DK','DENMARK','Denmark','DNK',208);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('DJ','DJIBOUTI','Djibouti','DJI',262);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('DM','DOMINICA','Dominica','DMA',212);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('DO','DOMINICAN REPUBLIC','Dominican Republic','DOM',214);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('EC','ECUADOR','Ecuador','ECU',218);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('EG','EGYPT','Egypt','EGY',818);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('SV','EL SALVADOR','El Salvador','SLV',222);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('GQ','EQUATORIAL GUINEA','Equatorial Guinea','GNQ',226);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('ER','ERITREA','Eritrea','ERI',232);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('EE','ESTONIA','Estonia','EST',233);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('ET','ETHIOPIA','Ethiopia','ETH',231);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('FK','FALKLAND ISLANDS (MALVINAS)','Falkland Islands (Malvinas)','FLK',238);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('FO','FAROE ISLANDS','Faroe Islands','FRO',234);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('FJ','FIJI','Fiji','FJI',242);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('FI','FINLAND','Finland','FIN',246);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('FR','FRANCE','France','FRA',250);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('GF','FRENCH GUIANA','French Guiana','GUF',254);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('PF','FRENCH POLYNESIA','French Polynesia','PYF',258);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('TF','FRENCH SOUTHERN TERRITORIES','French Southern Territories',NULL,NULL);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('GA','GABON','Gabon','GAB',266);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('GM','GAMBIA','Gambia','GMB',270);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('GE','GEORGIA','Georgia','GEO',268);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('DE','GERMANY','Germany','DEU',276);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('GH','GHANA','Ghana','GHA',288);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('GI','GIBRALTAR','Gibraltar','GIB',292);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('GR','GREECE','Greece','GRC',300);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('GL','GREENLAND','Greenland','GRL',304);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('GD','GRENADA','Grenada','GRD',308);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('GP','GUADELOUPE','Guadeloupe','GLP',312);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('GU','GUAM','Guam','GUM',316);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('GT','GUATEMALA','Guatemala','GTM',320);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('GN','GUINEA','Guinea','GIN',324);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('GW','GUINEA-BISSAU','Guinea-Bissau','GNB',624);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('GY','GUYANA','Guyana','GUY',328);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('HT','HAITI','Haiti','HTI',332);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('HM','HEARD ISLAND AND MCDONALD ISLANDS','Heard Island and Mcdonald Islands',NULL,NULL);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('VA','HOLY SEE (VATICAN CITY STATE)','Holy See (Vatican City State)','VAT',336);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('HN','HONDURAS','Honduras','HND',340);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('HK','HONG KONG','Hong Kong','HKG',344);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('HU','HUNGARY','Hungary','HUN',348);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('IS','ICELAND','Iceland','ISL',352);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('IN','INDIA','India','IND',356);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('ID','INDONESIA','Indonesia','IDN',360);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('IR','IRAN, ISLAMIC REPUBLIC OF','Iran, Islamic Republic of','IRN',364);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('IQ','IRAQ','Iraq','IRQ',368);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('IE','IRELAND','Ireland','IRL',372);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('IL','ISRAEL','Israel','ISR',376);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('IT','ITALY','Italy','ITA',380);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('JM','JAMAICA','Jamaica','JAM',388);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('JP','JAPAN','Japan','JPN',392);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('JO','JORDAN','Jordan','JOR',400);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('KZ','KAZAKHSTAN','Kazakhstan','KAZ',398);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('KE','KENYA','Kenya','KEN',404);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('KI','KIRIBATI','Kiribati','KIR',296);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('KP','KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF','Korea, Democratic People\'s Republic of','PRK',408);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('KR','KOREA, REPUBLIC OF','Korea, Republic of','KOR',410);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('KW','KUWAIT','Kuwait','KWT',414);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('KG','KYRGYZSTAN','Kyrgyzstan','KGZ',417);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('LA','LAO PEOPLE\'S DEMOCRATIC REPUBLIC','Lao People\'s Democratic Republic','LAO',418);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('LV','LATVIA','Latvia','LVA',428);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('LB','LEBANON','Lebanon','LBN',422);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('LS','LESOTHO','Lesotho','LSO',426);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('LR','LIBERIA','Liberia','LBR',430);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('LY','LIBYAN ARAB JAMAHIRIYA','Libyan Arab Jamahiriya','LBY',434);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('LI','LIECHTENSTEIN','Liechtenstein','LIE',438);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('LT','LITHUANIA','Lithuania','LTU',440);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('LU','LUXEMBOURG','Luxembourg','LUX',442);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('MO','MACAO','Macao','MAC',446);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('MK','MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF','Macedonia, the Former Yugoslav Republic of','MKD',807);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('MG','MADAGASCAR','Madagascar','MDG',450);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('MW','MALAWI','Malawi','MWI',454);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('MY','MALAYSIA','Malaysia','MYS',458);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('MV','MALDIVES','Maldives','MDV',462);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('ML','MALI','Mali','MLI',466);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('MT','MALTA','Malta','MLT',470);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('MH','MARSHALL ISLANDS','Marshall Islands','MHL',584);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('MQ','MARTINIQUE','Martinique','MTQ',474);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('MR','MAURITANIA','Mauritania','MRT',478);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('MU','MAURITIUS','Mauritius','MUS',480);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('YT','MAYOTTE','Mayotte',NULL,NULL);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('MX','MEXICO','Mexico','MEX',484);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('FM','MICRONESIA, FEDERATED STATES OF','Micronesia, Federated States of','FSM',583);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('MD','MOLDOVA, REPUBLIC OF','Moldova, Republic of','MDA',498);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('MC','MONACO','Monaco','MCO',492);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('MN','MONGOLIA','Mongolia','MNG',496);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('MS','MONTSERRAT','Montserrat','MSR',500);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('MA','MOROCCO','Morocco','MAR',504);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('MZ','MOZAMBIQUE','Mozambique','MOZ',508);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('MM','MYANMAR','Myanmar','MMR',104);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('NA','NAMIBIA','Namibia','NAM',516);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('NR','NAURU','Nauru','NRU',520);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('NP','NEPAL','Nepal','NPL',524);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('NL','NETHERLANDS','Netherlands','NLD',528);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('AN','NETHERLANDS ANTILLES','Netherlands Antilles','ANT',530);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('NC','NEW CALEDONIA','New Caledonia','NCL',540);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('NZ','NEW ZEALAND','New Zealand','NZL',554);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('NI','NICARAGUA','Nicaragua','NIC',558);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('NE','NIGER','Niger','NER',562);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('NG','NIGERIA','Nigeria','NGA',566);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('NU','NIUE','Niue','NIU',570);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('NF','NORFOLK ISLAND','Norfolk Island','NFK',574);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('MP','NORTHERN MARIANA ISLANDS','Northern Mariana Islands','MNP',580);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('NO','NORWAY','Norway','NOR',578);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('OM','OMAN','Oman','OMN',512);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('PK','PAKISTAN','Pakistan','PAK',586);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('PW','PALAU','Palau','PLW',585);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('PS','PALESTINIAN TERRITORY, OCCUPIED','Palestinian Territory, Occupied',NULL,NULL);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('PA','PANAMA','Panama','PAN',591);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('PG','PAPUA NEW GUINEA','Papua New Guinea','PNG',598);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('PY','PARAGUAY','Paraguay','PRY',600);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('PE','PERU','Peru','PER',604);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('PH','PHILIPPINES','Philippines','PHL',608);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('PN','PITCAIRN','Pitcairn','PCN',612);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('PL','POLAND','Poland','POL',616);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('PT','PORTUGAL','Portugal','PRT',620);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('PR','PUERTO RICO','Puerto Rico','PRI',630);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('QA','QATAR','Qatar','QAT',634);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('RE','REUNION','Reunion','REU',638);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('RO','ROMANIA','Romania','ROM',642);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('RU','RUSSIAN FEDERATION','Russian Federation','RUS',643);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('RW','RWANDA','Rwanda','RWA',646);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('SH','SAINT HELENA','Saint Helena','SHN',654);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('KN','SAINT KITTS AND NEVIS','Saint Kitts and Nevis','KNA',659);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('LC','SAINT LUCIA','Saint Lucia','LCA',662);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('PM','SAINT PIERRE AND MIQUELON','Saint Pierre and Miquelon','SPM',666);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('VC','SAINT VINCENT AND THE GRENADINES','Saint Vincent and the Grenadines','VCT',670);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('WS','SAMOA','Samoa','WSM',882);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('SM','SAN MARINO','San Marino','SMR',674);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('ST','SAO TOME AND PRINCIPE','Sao Tome and Principe','STP',678);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('SA','SAUDI ARABIA','Saudi Arabia','SAU',682);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('SN','SENEGAL','Senegal','SEN',686);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('CS','SERBIA AND MONTENEGRO','Serbia and Montenegro',NULL,NULL);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('SC','SEYCHELLES','Seychelles','SYC',690);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('SL','SIERRA LEONE','Sierra Leone','SLE',694);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('SG','SINGAPORE','Singapore','SGP',702);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('SK','SLOVAKIA','Slovakia','SVK',703);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('SI','SLOVENIA','Slovenia','SVN',705);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('SB','SOLOMON ISLANDS','Solomon Islands','SLB',90);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('SO','SOMALIA','Somalia','SOM',706);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('ZA','SOUTH AFRICA','South Africa','ZAF',710);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('GS','SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS','South Georgia and the South Sandwich Islands',NULL,NULL);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('ES','SPAIN','Spain','ESP',724);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('LK','SRI LANKA','Sri Lanka','LKA',144);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('SD','SUDAN','Sudan','SDN',736);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('SR','SURINAME','Suriname','SUR',740);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('SJ','SVALBARD AND JAN MAYEN','Svalbard and Jan Mayen','SJM',744);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('SZ','SWAZILAND','Swaziland','SWZ',748);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('SE','SWEDEN','Sweden','SWE',752);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('CH','SWITZERLAND','Switzerland','CHE',756);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('SY','SYRIAN ARAB REPUBLIC','Syrian Arab Republic','SYR',760);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('TW','TAIWAN, PROVINCE OF CHINA','Taiwan, Province of China','TWN',158);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('TJ','TAJIKISTAN','Tajikistan','TJK',762);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('TZ','TANZANIA, UNITED REPUBLIC OF','Tanzania, United Republic of','TZA',834);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('TH','THAILAND','Thailand','THA',764);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('TL','TIMOR-LESTE','Timor-Leste',NULL,NULL);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('TG','TOGO','Togo','TGO',768);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('TK','TOKELAU','Tokelau','TKL',772);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('TO','TONGA','Tonga','TON',776);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('TT','TRINIDAD AND TOBAGO','Trinidad and Tobago','TTO',780);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('TN','TUNISIA','Tunisia','TUN',788);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('TR','TURKEY','Turkey','TUR',792);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('TM','TURKMENISTAN','Turkmenistan','TKM',795);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('TC','TURKS AND CAICOS ISLANDS','Turks and Caicos Islands','TCA',796);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('TV','TUVALU','Tuvalu','TUV',798);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('UG','UGANDA','Uganda','UGA',800);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('UA','UKRAINE','Ukraine','UKR',804);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('AE','UNITED ARAB EMIRATES','United Arab Emirates','ARE',784);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('GB','UNITED KINGDOM','United Kingdom','GBR',826);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('US','UNITED STATES','United States','USA',840);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('UM','UNITED STATES MINOR OUTLYING ISLANDS','United States Minor Outlying Islands',NULL,NULL);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('UY','URUGUAY','Uruguay','URY',858);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('UZ','UZBEKISTAN','Uzbekistan','UZB',860);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('VU','VANUATU','Vanuatu','VUT',548);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('VE','VENEZUELA','Venezuela','VEN',862);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('VN','VIET NAM','Viet Nam','VNM',704);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('VG','VIRGIN ISLANDS, BRITISH','Virgin Islands, British','VGB',92);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('VI','VIRGIN ISLANDS, U.S.','Virgin Islands, U.s.','VIR',850);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('WF','WALLIS AND FUTUNA','Wallis and Futuna','WLF',876);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('EH','WESTERN SAHARA','Western Sahara','ESH',732);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('YE','YEMEN','Yemen','YEM',887);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('ZM','ZAMBIA','Zambia','ZMB',894);
INSERT INTO `$prefix$flip_i18n_countrycodes` VALUES ('ZW','ZIMBABWE','Zimbabwe','ZWE',716);
# --------------------------- /flip_i18n_countrycodes -------------------------- #


# --------------------------- flip_inet_groups --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_inet_groups`;
CREATE TABLE `$prefix$flip_inet_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `subject_group_id` int(11) NOT NULL DEFAULT '0',
  `priority` int(11) NOT NULL DEFAULT '1',
  `online_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_inet_groups -------------------------- #


# --------------------------- flip_inet_requests --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_inet_requests`;
CREATE TABLE `$prefix$flip_inet_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `status` enum('waiting','granted','expired','noexpiry') COLLATE utf8mb4_bin NOT NULL DEFAULT 'waiting',
  `online_since` int(11) NOT NULL DEFAULT '0',
  `online_time` int(11) NOT NULL DEFAULT '0',
  `priority` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_inet_requests -------------------------- #


# --------------------------- flip_konten_data --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_konten_data`;
CREATE TABLE `$prefix$flip_konten_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `list_id` int(11) NOT NULL DEFAULT '0',
  `date` int(11) NOT NULL DEFAULT '0',
  `value` float NOT NULL DEFAULT '0',
  `text` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_konten_data -------------------------- #


# --------------------------- flip_konten_list --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_konten_list`;
CREATE TABLE `$prefix$flip_konten_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `title` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_konten_list -------------------------- #


# --------------------------- flip_konto_data --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_konto_data`;

# Diese Tabelle wird nicht weiter verwendet.
# --------------------------- /flip_konto_data -------------------------- #


# --------------------------- flip_konto_list --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_konto_list`;

# Diese Tabelle wird nicht weiter verwendet.
# --------------------------- /flip_konto_list -------------------------- #


# --------------------------- flip_lastvisit --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_lastvisit`;
CREATE TABLE `$prefix$flip_lastvisit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_name` (`name`,`user_id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Diese Datensaetze sind nur Temporaer.
# --------------------------- /flip_lastvisit -------------------------- #


# --------------------------- flip_log --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_log`;

# Diese Tabelle wird nicht weiter verwendet.
# --------------------------- /flip_log -------------------------- #


# --------------------------- flip_log_log --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_log_log`;
CREATE TABLE `$prefix$flip_log_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `time` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `session_id` varchar(8) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `message` mediumtext COLLATE utf8mb4_bin NOT NULL,
  `type` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=246 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Diese Datensaetze sind nur Temporaer.
# --------------------------- /flip_log_log -------------------------- #


# --------------------------- flip_menu --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_menu`;

# Diese Tabelle wird nicht weiter verwendet.
# --------------------------- /flip_menu -------------------------- #


# --------------------------- flip_menu_blocks --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_menu_blocks`;
CREATE TABLE `$prefix$flip_menu_blocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `caption` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8mb4_bin NOT NULL,
  `view_right` int(11) NOT NULL DEFAULT '0',
  `parent_item_id` int(11) NOT NULL DEFAULT '0',
  `level_index` int(11) NOT NULL DEFAULT '0',
  `callback_items` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `image_title` longblob NOT NULL,
  `image_bg` longblob NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`,`order`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze:
INSERT INTO `$prefix$flip_menu_blocks` VALUES (1,CURRENT_TIMESTAMP,'default','',0,0,0,'',1,'','',0);
INSERT INTO `$prefix$flip_menu_blocks` VALUES (6,CURRENT_TIMESTAMP,'News','Neuigkeiten',0,7,0,'',1,'','',1096487626);
INSERT INTO `$prefix$flip_menu_blocks` VALUES (7,CURRENT_TIMESTAMP,'Login','',0,0,1,'',1,'','',1117117604);
INSERT INTO `$prefix$flip_menu_blocks` VALUES (8,CURRENT_TIMESTAMP,'Archiv','',0,8,0,'',1,'','',1096290508);
INSERT INTO `$prefix$flip_menu_blocks` VALUES (9,CURRENT_TIMESTAMP,'Info','Informationen über die aktuelle Lanparty',0,8,0,'',1,'','',1096290758);
INSERT INTO `$prefix$flip_menu_blocks` VALUES (10,CURRENT_TIMESTAMP,'Teilnehmen','',0,8,0,'',1,'','',1096478116);
INSERT INTO `$prefix$flip_menu_blocks` VALUES (11,CURRENT_TIMESTAMP,'Meine Daten','',4,9,0,'',1,'','',1096291356);
INSERT INTO `$prefix$flip_menu_blocks` VALUES (12,CURRENT_TIMESTAMP,'Support','',0,9,0,'',1,'','',1105374586);
INSERT INTO `$prefix$flip_menu_blocks` VALUES (13,CURRENT_TIMESTAMP,'Systemprotokoll','',0,11,0,'',1,'','',1096292801);
INSERT INTO `$prefix$flip_menu_blocks` VALUES (14,CURRENT_TIMESTAMP,'Orga-Tools','Organisationswerkzeuge :)',0,11,0,'',1,'','',1096295486);
INSERT INTO `$prefix$flip_menu_blocks` VALUES (15,CURRENT_TIMESTAMP,'Anpassen','',0,12,0,'',1,'','',1096295965);
INSERT INTO `$prefix$flip_menu_blocks` VALUES (16,CURRENT_TIMESTAMP,'Subjekte','Damit sind User, Gruppen usw. gemeint',0,12,0,'',1,'','',1096296197);
INSERT INTO `$prefix$flip_menu_blocks` VALUES (17,CURRENT_TIMESTAMP,'System','',0,12,0,'',1,'','',1096318450);
INSERT INTO `$prefix$flip_menu_blocks` VALUES (18,CURRENT_TIMESTAMP,'Datenbank','Wartungsmöglichkeiten für die Datenbank des FLIPs ',0,12,0,'',1,'','',1096318591);
INSERT INTO `$prefix$flip_menu_blocks` VALUES (19,CURRENT_TIMESTAMP,'Netzwerk','',0,12,0,'',1,'','',1096318767);
INSERT INTO `$prefix$flip_menu_blocks` VALUES (21,CURRENT_TIMESTAMP,'Login II','',0,7,0,'',1,'','',1096289778);
INSERT INTO `$prefix$flip_menu_blocks` VALUES (22,CURRENT_TIMESTAMP,'Partner','Unsere Partner',0,0,1,'',1,'','',1282869433);
INSERT INTO `$prefix$flip_menu_blocks` VALUES (23,CURRENT_TIMESTAMP,'Kontakt','',0,7,0,'',1,'','',1105374586);
INSERT INTO `$prefix$flip_menu_blocks` VALUES (24,CURRENT_TIMESTAMP,'User online','Zeigt wieviele User eingeloggt sind und die ersten 5 (Config->statistics_numberonlineusers) längsten Onliner.',4,10,0,'',1,'','',1106543670);
INSERT INTO `$prefix$flip_menu_blocks` VALUES (25,CURRENT_TIMESTAMP,'Checklist','',0,8,0,'',1,'','',1096290409);
INSERT INTO `$prefix$flip_menu_blocks` VALUES (26,CURRENT_TIMESTAMP,'Webmessages','',44,0,1,'',1,'','',1234);
INSERT INTO `$prefix$flip_menu_blocks` VALUES (27,CURRENT_TIMESTAMP,'Menüpoll','',0,7,0,'',1,'','',1100542804);
INSERT INTO `$prefix$flip_menu_blocks` VALUES (28,CURRENT_TIMESTAMP,'Catering','Zeigt dir verschiedene Bereiche des Caterings an',0,9,0,'',1,'','',1096291803);
INSERT INTO `$prefix$flip_menu_blocks` VALUES (29,CURRENT_TIMESTAMP,'Registrieren','',0,47,0,'',1,'','',1096478116);
# --------------------------- /flip_menu_blocks -------------------------- #


# --------------------------- flip_menu_items --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_menu_items`;

# Diese Tabelle wird nicht weiter verwendet.
# --------------------------- /flip_menu_items -------------------------- #


# --------------------------- flip_menu_links --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_menu_links`;
CREATE TABLE `$prefix$flip_menu_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `block_id` int(11) NOT NULL DEFAULT '0',
  `caption` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8mb4_bin NOT NULL,
  `view_right` int(11) NOT NULL DEFAULT '0',
  `link` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `use_new_wnd` tinyint(4) NOT NULL DEFAULT '0',
  `frameurl` varchar(254) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `text` mediumtext COLLATE utf8mb4_bin NOT NULL,
  `image` longblob NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0',
  `font_image` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`,`block_id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze:
INSERT INTO `$prefix$flip_menu_links` VALUES (7,CURRENT_TIMESTAMP, 1, 'Startseite', 'Neuigkeiten', 0, '#', 0, '', '', '', 1, 1096289310, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (8,CURRENT_TIMESTAMP, 1, 'lanparty', 'Alles mit und über unsere aktuelle Lanparty', 0, '#', 0, '', '', '', 1, 1096289000, 'fas fa-home');
INSERT INTO `$prefix$flip_menu_links` VALUES (9,CURRENT_TIMESTAMP, 1, 'meine daten', 'Meine Einstellungen,Daten,Nachrichten,usw.', 4, '#', 0, '', '', '', 1, 1096289208, 'fas fa-user');
INSERT INTO `$prefix$flip_menu_links` VALUES (10,CURRENT_TIMESTAMP, 1, 'foren', 'Die Diskussionsforen', 0, '#', 0, '', '', '', 1, 1096289360, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (11,CURRENT_TIMESTAMP, 1, 'intern', 'Zutritt nur f&uuml;r Orgas!', 3, '#', 0, '', '', '', 1, 1096289451, 'far fa-check-square');
INSERT INTO `$prefix$flip_menu_links` VALUES (12,CURRENT_TIMESTAMP, 1, 'admin', 'Zutritt nur für Administratoren!', 109, '#', 0, '', '', '', 1, 1096385956, 'fas fa-wrench');
INSERT INTO `$prefix$flip_menu_links` VALUES (13,CURRENT_TIMESTAMP, 6, 'Newsarchiv', 'alte News', 0, 'news.php?frame=archiv', 0, '', '', '', 1, 1096289690, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (14,CURRENT_TIMESTAMP, 6, 'News', 'Neuigkeiten', 0, 'news.php', 0, '', '', '', 1, 1096289744, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (15,CURRENT_TIMESTAMP, 7, 'Login', 'Einloggen', 30, 'user.php?frame=login', 0, '', '', '', 0, 1096289882, 'fas fa-sign-in-alt');
INSERT INTO `$prefix$flip_menu_links` VALUES (16,CURRENT_TIMESTAMP, 7, 'Logout', 'Ausloggen', 4, 'user.php?frame=login&amp;ident=Anonymous&amp;password=x', 0, '', '', '', 0, 1382024001, 'fas fa-sign-out-alt');
INSERT INTO `$prefix$flip_menu_links` VALUES (78,CURRENT_TIMESTAMP, 15, 'Config (Kat.)', 'Kategorisierte Einstellungen', 0, 'config.php?frame=categories', 0, '', '', '', 1, 1096296081, 'fas fa-cog fa-spin');
INSERT INTO `$prefix$flip_menu_links` VALUES (18,CURRENT_TIMESTAMP, 8, 'Lanparty-Archiv', 'vergangene Lanpartys', 0, 'archiv.php', 0, '', '', '', 1, 1096290439, 'fas fa-archive');
INSERT INTO `$prefix$flip_menu_links` VALUES (19,CURRENT_TIMESTAMP, 9, 'Netzwerk', '<p>die verwendete Netzwerktechnik und die G&auml;steserver</p>', 43, 'server.php', 0, '', '', '', 1, 1097055212, 'fas fa-sitemap');
INSERT INTO `$prefix$flip_menu_links` VALUES (20,CURRENT_TIMESTAMP, 9, 'Sponsoren', 'we''re powered by...', 0, 'sponsor.php', 0, '', '', '', 1, 1102946969, 'fas fa-child');
INSERT INTO `$prefix$flip_menu_links` VALUES (21,CURRENT_TIMESTAMP, 10, 'Anmeldung', 'die Ameldung für die nächste Lanparty', 0, 'lanparty.php?frame=register', 0, '', '', '', 1, 1096290842, 'fas fa-sign-in-alt');
INSERT INTO `$prefix$flip_menu_links` VALUES (22,CURRENT_TIMESTAMP, 10, 'Turniere', 'das Turniersystem', 0, 'tournament.php', 0, '', '', '', 1, 1173103907, 'fas fa-trophy');
INSERT INTO `$prefix$flip_menu_links` VALUES (23,CURRENT_TIMESTAMP, 10, 'Teilnehmer', 'die Liste der Teilnehmer', 0, 'lanparty.php', 0, '', '', '', 1, 1096290973, 'fas fa-users');
INSERT INTO `$prefix$flip_menu_links` VALUES (24,CURRENT_TIMESTAMP, 10, 'Sitzplan', 'Sitzplan und Sitzplatzreservierung', 0, 'seats.php', 0, '', '', '', 1, 1096291055, 'fas fa-sitemap');
INSERT INTO `$prefix$flip_menu_links` VALUES (25,CURRENT_TIMESTAMP, 11, 'mein Profil', 'Name,Anschrift,Kontaktmöglichkeiten und eigene Einstellungen', 0, 'user.php?frame=editproperties', 0, '', '', '', 1, 1096291392, 'fas fa-edit');
INSERT INTO `$prefix$flip_menu_links` VALUES (26,CURRENT_TIMESTAMP, 11, 'mein(e) Server', 'Server,die ich zur LAN mitbringen werde', 43, 'server.php?frame=add', 0, '', '', '', 1, 1096291484, 'fas fa-desktop');
INSERT INTO `$prefix$flip_menu_links` VALUES (27,CURRENT_TIMESTAMP, 11, 'WebMessaging', 'Nachrichten über die Webseite senden und empfangen', 0, 'webmessage.php?frame=owner', 0, '', '', '', 1, 1096291550, 'fas fa-comments');
INSERT INTO `$prefix$flip_menu_links` VALUES (28,CURRENT_TIMESTAMP, 11, 'CheckinInfo', 'informationen,um dich schnell durch den Checkin zu bringen', 19, 'checkininfo.php', 0, '', '', '', 1, 1096291641, 'fas fa-barcode');
INSERT INTO `$prefix$flip_menu_links` VALUES (29,CURRENT_TIMESTAMP, 12, 'meine Tickets', '', 63, 'ticket.php?f_resp=me', 0, '', '', '', 1, 1096291895, 'fas fa-ticket-alt');
INSERT INTO `$prefix$flip_menu_links` VALUES (30,CURRENT_TIMESTAMP, 13, 'Changelog', '', 59, 'log.php?frame=viewdblog', 0, '', '', '', 1, 1096292836, 'far fa-file-alt');
INSERT INTO `$prefix$flip_menu_links` VALUES (31,CURRENT_TIMESTAMP, 14, 'Tickets', 'das Ticketsystem zur Aufgabenverwaltung', 23, 'ticket.php', 0, '', '', '', 1, 1096295557, 'fas fa-ticket-alt');
INSERT INTO `$prefix$flip_menu_links` VALUES (32,CURRENT_TIMESTAMP, 14, 'Konten', 'die Geldverwaltung', 67, 'konten.php', 0, '', '', '', 1, 1096295644, 'fas fa-piggy-bank');
INSERT INTO `$prefix$flip_menu_links` VALUES (33,CURRENT_TIMESTAMP, 14, 'Sponsoren', 'die Sponsorenverwaltung', 12, 'sponsor.php?frame=list', 0, '', '', '', 1, 1096295700, 'fas fa-hand-holding-usd');
INSERT INTO `$prefix$flip_menu_links` VALUES (34,CURRENT_TIMESTAMP, 14, 'Kalender', 'die Terminverwaltung', 52, 'calender.php?frame=yeargraph', 0, '', '', '', 1, 1105374139, 'far fa-calendar-alt');
INSERT INTO `$prefix$flip_menu_links` VALUES (35,CURRENT_TIMESTAMP, 15, 'Config', 'Die allgemeine Konfiguration', 57, 'config.php', 0, '', '', '', 1, 1096295997, 'fas fa-cogs');
INSERT INTO `$prefix$flip_menu_links` VALUES (36,CURRENT_TIMESTAMP, 15, 'Menü', 'die Verwaltung des Menüs', 6, 'menu.php', 0, '', '', '', 1, 1096296128, 'fas fa-bars');
INSERT INTO `$prefix$flip_menu_links` VALUES (37,CURRENT_TIMESTAMP, 15, 'Content/Images', 'Verwalten von Texten und Bildern', 25, 'content.php', 0, '', '', '', 1, 1097761724, 'far fa-images');
INSERT INTO `$prefix$flip_menu_links` VALUES (38,CURRENT_TIMESTAMP, 16, 'Checkin', 'User suchen und einchecken', 48, 'checkin.php', 0, '', '', '', 0, 1096296269, 'fas fa-info');
INSERT INTO `$prefix$flip_menu_links` VALUES (39,CURRENT_TIMESTAMP, 16, 'User', 'User verwalten', 26, 'user.php?type=user', 0, '', '', '', 1, 1096296393, 'fas fa-user');
INSERT INTO `$prefix$flip_menu_links` VALUES (40,CURRENT_TIMESTAMP, 16, 'Groups', 'Gruppen verwalten', 26, 'user.php?type=group', 0, '', '', '', 1, 1096296483, 'fas fa-users');
INSERT INTO `$prefix$flip_menu_links` VALUES (41,CURRENT_TIMESTAMP, 16, 'Columns', 'die Felder des Userprofils definieren', 27, 'user.php?frame=viewcolumns', 0, '', '', '', 1, 1096296559, 'fas fa-columns');
INSERT INTO `$prefix$flip_menu_links` VALUES (42,CURRENT_TIMESTAMP, 16, 'Rights', 'Rechte verwalten', 24, 'user.php?frame=viewrights', 0, '', '', '', 1, 1204311005, 'fas fa-copyright');
INSERT INTO `$prefix$flip_menu_links` VALUES (43,CURRENT_TIMESTAMP, 17, 'Nachrichten', 'Nachrichten,die bei verschiedenen Gelegenheiten vom System automatisch verschickt werden.', 58, 'sendmessage.php?type=sys', 0, '', '', '', 1, 1096318493, 'far fa-comments');
INSERT INTO `$prefix$flip_menu_links` VALUES (44,CURRENT_TIMESTAMP, 18, 'DBXFer', 'erlaubt es,die Datenbank aus einem anderen FLIP-System zu importieren', 29, 'dbxfer.php', 0, '', '', '', 1, 1096318605, 'fas fa-upload ');
INSERT INTO `$prefix$flip_menu_links` VALUES (45,CURRENT_TIMESTAMP, 19, 'DNS', 'automatisch für die User und Gästeserver generierte DNS-Tabellen', 11, 'dns.php', 0, '', '', '', 1, 1096318781, 'fas fa-map-signs ');
INSERT INTO `$prefix$flip_menu_links` VALUES (46,CURRENT_TIMESTAMP, 19, 'NetLog', 'log aller Adressänderungen im Netzwerk und Logins im FLIP', 60, 'netlog.php', 0, '', '', '', 1, 1096318859, 'far fa-file-alt');
INSERT INTO `$prefix$flip_menu_links` VALUES (47,CURRENT_TIMESTAMP, 1, 'registrieren', 'einen Member-Account auf dieser Seite erstellen', 30, '#', 0, '', '', '', 1, 1096289025, 'fas fa-plus');
INSERT INTO `$prefix$flip_menu_links` VALUES (48,CURRENT_TIMESTAMP, 9, 'Catering1', '', 46, 'catering.php', 0, '', '', '', 1, 1204287282, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (49,CURRENT_TIMESTAMP, 6, 'Umfragen', 'was sagt ihr zu ...?', 0, 'poll.php', 0, '', '', '', 1, 1097055281, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (50,CURRENT_TIMESTAMP, 15, 'Tables', '', 0, 'table.php', 0, '', '', '', 1, 1360766725, 'fa fa-table');
INSERT INTO `$prefix$flip_menu_links` VALUES (51,CURRENT_TIMESTAMP, 18, 'Sysinfo', 'Zeigt Systeminformationen an.', 74, 'sysinfo.php', 0, '', '', '', 1, 1099068203, 'fas fa-tachometer-alt');
INSERT INTO `$prefix$flip_menu_links` VALUES (52,CURRENT_TIMESTAMP, 9, 'OrgaTeam', '', 78, 'user.php?frame=viewsubject&name=orga', 0, '', '', '', 1, 1303125631, 'far fa-smile');
INSERT INTO `$prefix$flip_menu_links` VALUES (53,CURRENT_TIMESTAMP, 14, 'Nachrichten', 'Nachrichten des System-Users.', 0, 'webmessage.php?frame=owner&amp;id=4', 0, '', '', '', 1, 1108570050, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (54,CURRENT_TIMESTAMP, 23, 'Kontakt', '', 0, 'webmessage.php?frame=contact', 0, '', '', '', 1, 1105374596, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (75,CURRENT_TIMESTAMP, 9, 'gallery', 'Bildergalerie', 0, 'gallery.php', 0, '', '', '', 1, 1360768728, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (56,CURRENT_TIMESTAMP, 10, 'Überweisung', 'Das Konto für den Unkostenbeitrag', 0, 'banktransfer.php', 0, '', '', '', 1, 1096290911, 'far fa-credit-card');
INSERT INTO `$prefix$flip_menu_links` VALUES (57,CURRENT_TIMESTAMP, 14, 'Rundbriefe', 'Nachrichten an Gruppen senden.', 44, 'sendmessage.php', 0, '', '', '', 1, 1360774306, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (58,CURRENT_TIMESTAMP, 21, 'login2', '', 0, '', 0, 'user.php?frame=login', '', '', 1, 0, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (59,CURRENT_TIMESTAMP, 22, 'Sponsor', '', 0, '', 0, 'sponsor.php?frame=smallbuttons', '', '', 0, 1303397513, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (60,CURRENT_TIMESTAMP, 24, '', '', 0, '', 0, 'statistic.php?frame=online', '', '', 1, 0, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (61,CURRENT_TIMESTAMP, 25, '', '', 0, '', 0, 'lanparty.php?frame=smalluserstatus', '', '', 0, 0, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (62,CURRENT_TIMESTAMP, 26, 'Webmessages', '', 0, '', 0, 'webmessage.php?frame=smallnewmessages', '', '', 1, 34334232, 'far fa-comments');
INSERT INTO `$prefix$flip_menu_links` VALUES (63,CURRENT_TIMESTAMP, 25, 'status', '', 0, '', 0, 'lanparty.php?frame=smallstatusbar', '', '', 0, 1116350390, 'far fa-chart-bar');
INSERT INTO `$prefix$flip_menu_links` VALUES (64,CURRENT_TIMESTAMP, 25, 'spacer', '', 0, '', 0, '', '<hr />', '', 0, 123324323, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (65,CURRENT_TIMESTAMP, 25, 'Countdown', 'So lange musst du noch warten.', 0, '', 0, 'lanparty.php?frame=countdown_small', '', '', 0, 123324324, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (68,CURRENT_TIMESTAMP, 27, 'menüpoll', '', 0, '', 0, 'poll.php?frame=menuvote&votefor=newest', '', '', 1, 1117117628, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (69,CURRENT_TIMESTAMP, 18, 'DBExport', 'Der DBExport kann einen SQL-Dump der gesamten Datenbank erstellen.', 81, 'dbexport.php', 0, '', '', '', 1, 1127574677, 'fas fa-download');
INSERT INTO `$prefix$flip_menu_links` VALUES (70,CURRENT_TIMESTAMP, 10, 'Clans', 'Clanverwaltung', 0, 'clan.php', 0, '', '', '', 1, 1303397625, 'fas fa-crosshairs');
INSERT INTO `$prefix$flip_menu_links` VALUES (71,CURRENT_TIMESTAMP, 23, 'Gästebuch', 'Gästebuch', 0, 'gb.php', 0, '', '', '', 1, 1173882858, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (72,CURRENT_TIMESTAMP, 10, 'Internet', 'Internet-Modul', 0, 'inet.php', 0, '', '', '', 1, 1381160063, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (73,CURRENT_TIMESTAMP, 9, 'News', '', 0, 'news.php', 0, '', '', '', 1, 1096290641, 'far fa-newspaper');
INSERT INTO `$prefix$flip_menu_links` VALUES (83,CURRENT_TIMESTAMP, 25, 'Trennlinie II', '', 103, '', 0, '', '<hr />', '', 0, 1361210468, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (77,CURRENT_TIMESTAMP, 10, 'Sitzplatz', '', 0, 'seats.php?frame=block&blockid=1', 0, '', '', '', 1, 1107294492, 'fas fa-map-marker-alt');
INSERT INTO `$prefix$flip_menu_links` VALUES (79,CURRENT_TIMESTAMP, 9, 'Catering', '<p>Das neue,verbesserte Cateringmodul</p>', 46, 'shop.php', 0, '', '', '', 1, 1381160130, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (80,CURRENT_TIMESTAMP, 14, 'Checkin', '<p>User vor und w&auml;hrend der LAN ein- und auschecken</p>', 48, 'checkin.php', 0, '', '', '', 1, 1096295508, 'far fa-check-square');
INSERT INTO `$prefix$flip_menu_links` VALUES (81,CURRENT_TIMESTAMP, 14, 'Cateringverwaltung', '<p>Hier kannst du das Catering verwalten</p>', 101, 'shop.php?frame=adminoverview', 0, '', '', '', 1, 1360774370, 'fas fa-beer');
INSERT INTO `$prefix$flip_menu_links` VALUES (82,CURRENT_TIMESTAMP, 14, 'Sitzplaneditor', '<p>&Ouml;ffnet TableDancer,den interaktiven Java Sitzplaneditor des FLIPs</p>', 31, 'seatsadm.php', 0, '', '', '', 1, 1360774423, 'fas fa-puzzle-piece');
INSERT INTO `$prefix$flip_menu_links` VALUES (84,CURRENT_TIMESTAMP, 25, 'Einkaufswagen', '', 103, '', 0, 'shop.php?frame=menushoppingcart', '', '', 1, 1361210516, 'fas fa-shopping-cart');
INSERT INTO `$prefix$flip_menu_links` VALUES (86,CURRENT_TIMESTAMP, 9, 'Turnierpreise', '', 0, 'text.php?name=Turnierpreise', 0, '', '', '', 1, 1472333522, 'fas fa-gift');
INSERT INTO `$prefix$flip_menu_links` VALUES (87,CURRENT_TIMESTAMP, 7, 'l', '', 0, '', 0, 'user.php?frame=login', '', '', 0, 1474471370, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (88,CURRENT_TIMESTAMP, 11, 'Meine Bestellungen', '', 3, 'shop.php?frame=showmybalance', 0, '', '', '', 1, 1443389249, 'fas fa-shopping-basket');
INSERT INTO `$prefix$flip_menu_links` VALUES (93,CURRENT_TIMESTAMP, 9, 'LAN-Info', '', 0, 'text.php?name=menu_lanparty', 0, '', '', '', 1, 1096290543, 'fas fa-info');
INSERT INTO `$prefix$flip_menu_links` VALUES (96,CURRENT_TIMESTAMP, 8, 'Impressum', '', 0, 'text.php?name=impressum', 1, '', '', '', 1, 1474471680, 'fas fa-info-circle');
INSERT INTO `$prefix$flip_menu_links` VALUES (103,CURRENT_TIMESTAMP, 29, 'Account Wiederherstellung', '', 30, 'text.php?name=user_account_trouble', 0, '', '', '', 1, 1474472965, 'fas fa-user-md');
INSERT INTO `$prefix$flip_menu_links` VALUES (101,CURRENT_TIMESTAMP, 29, 'Registrieren', '', 30, 'user.php?frame=register', 0, '', '', '', 1, 1474472830, 'fas fa-user-plus');
INSERT INTO `$prefix$flip_menu_links` VALUES (102,CURRENT_TIMESTAMP, 29, 'Login', '', 30, 'user.php?frame=login', 0, '', '', '', 1, 1474472926, 'fas fa-sign-in-alt');
INSERT INTO `$prefix$flip_menu_links` VALUES (104,CURRENT_TIMESTAMP, 11, 'DSGVO', '', 0, 'user.php?frame=dsgvo', 0, '', '', '', 1, 1526326014, 'fa fa-balance-scale');
INSERT INTO `$prefix$flip_menu_links` VALUES (108,CURRENT_TIMESTAMP, 24, 'Forum', '', 0, 'forum.php', 0, '', '', '', 1, 1528714967, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (109,CURRENT_TIMESTAMP, 21, 'news', '', 0, 'news.php', 0, '', '', '', 1, 1528715083, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (106,CURRENT_TIMESTAMP, 9, 'FAQ', '', 0, 'text.php?name=faq', 0, '', '', '', 1, 1527683759, 'fa fa-question-circle');
INSERT INTO `$prefix$flip_menu_links` VALUES (107,CURRENT_TIMESTAMP, 11, 'Event-An/Abmeldung', '', 0, 'lanparty.php?frame=register', 0, '', '', '', 1, 1526799641, 'fa fa-sign-in-alt');
INSERT INTO `$prefix$flip_menu_links` VALUES (111,CURRENT_TIMESTAMP, 11, 'mein Eventstatus', '', 0, '', 0, 'lanparty.php?frame=smalluserstatus', '', '', 1, 1528931456, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (112,CURRENT_TIMESTAMP, 10, 'Anmeldung Konsole', '', 0, 'lanparty.php?frame=registerconsole', 0, '', '', '', 1, 1535962843, 'fa fa-sign-in-alt');
INSERT INTO `$prefix$flip_menu_links` VALUES (113,CURRENT_TIMESTAMP, 11, 'Anmeldung Konsole', '', 0, 'lanparty.php?frame=registerconsole', 0, '', '', '', 1, 1535962924, 'fa fa-sign-in-alt');
INSERT INTO `$prefix$flip_menu_links` VALUES (115,CURRENT_TIMESTAMP, 16, 'Suche', 'Erm&ouml;glicht,das System gezielt nach Informationen zu durchsuchen.', 0, 'search.php', 0, '', '', '', 1, 1550084140, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (90,CURRENT_TIMESTAMP, 11, 'Meine Watches', 'Zeigt dir deine Forenwatches an', 0, 'watches.php', 0, '', '', '', 1, 1096291641, 'fa fa-check-square-o');
INSERT INTO `$prefix$flip_menu_links` VALUES (91,CURRENT_TIMESTAMP, 17, 'Versionslog', 'Zeigt an,welche Version dieses System hat und was die aktuellste Version der Entwickler ist', 0, 'changelog.php', 0, '', '', '', 1, 1280265470, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (92,CURRENT_TIMESTAMP, 10, 'PayPal', 'Mit PayPal bezahlen', 0, 'paypal.php', 0, '', '', '', 1, 1284250991, '');
INSERT INTO `$prefix$flip_menu_links` VALUES (94,CURRENT_TIMESTAMP, 12, 'Meine Anfragen', 'Zeigt dir deine Supportanfragen an', 0, 'ticket.php?frame=showmyrequests', 0, '', '', '', 1, 1284499560, '');
# --------------------------- /flip_menu_links -------------------------- #


# --------------------------- flip_message --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_message`;

# Diese Tabelle wird nicht weiter verwendet.
# --------------------------- /flip_message -------------------------- #


# --------------------------- flip_messaging --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_messaging`;

# Diese Tabelle wird nicht weiter verwendet.
# --------------------------- /flip_messaging -------------------------- #


# --------------------------- flip_messaging_messages --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_messaging_messages`;

# Diese Tabelle wird nicht weiter verwendet.
# --------------------------- /flip_messaging_messages -------------------------- #


# --------------------------- flip_messaging_webmessaging --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_messaging_webmessaging`;

# Diese Tabelle wird nicht weiter verwendet.
# --------------------------- /flip_messaging_webmessaging -------------------------- #


# --------------------------- flip_netlog --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_netlog`;

# Diese Tabelle wird nicht weiter verwendet.
# --------------------------- /flip_netlog -------------------------- #


# --------------------------- flip_netlog_log --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_netlog_log`;
CREATE TABLE `$prefix$flip_netlog_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `host_os` varchar(255) COLLATE utf8mb4_bin DEFAULT '',
  `nic_vendor` varchar(255) COLLATE utf8mb4_bin DEFAULT '',
  `dnsname` varchar(255) COLLATE utf8mb4_bin DEFAULT '',
  `nbgroup` varchar(100) COLLATE utf8mb4_bin DEFAULT '',
  `nbname` varchar(100) COLLATE utf8mb4_bin DEFAULT '',
  `nbunames` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `useragent` varchar(255) COLLATE utf8mb4_bin DEFAULT '',
  `hostname` varchar(255) COLLATE utf8mb4_bin DEFAULT '',
  `ipv4` varchar(15) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `mac` varchar(25) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  `user_ip` varchar(15) COLLATE utf8mb4_bin DEFAULT '',
  `user_seat` varchar(100) COLLATE utf8mb4_bin DEFAULT '',
  `session_id` varchar(20) DEFAULT '0',
  `action` varchar(100) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8mb4_bin,
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Diese Datensaetze sind nur Temporaer.
# --------------------------- /flip_netlog_log -------------------------- #


# --------------------------- flip_netlog_status --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_netlog_status`;
CREATE TABLE `$prefix$flip_netlog_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `log_id` int(11) NOT NULL DEFAULT '0',
  `key` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_uni` (`key`,`value`),
  KEY `log_id` (`key`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Diese Datensaetze sind nur Temporaer.
# --------------------------- /flip_netlog_status -------------------------- #


# --------------------------- flip_news --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_news`;

# Diese Tabelle wird nicht weiter verwendet.
# --------------------------- /flip_news -------------------------- #


# --------------------------- flip_news_comments --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_news_comments`;
CREATE TABLE `$prefix$flip_news_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `news_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `timestamp` int(11) NOT NULL DEFAULT '0',
  `text` mediumtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `i_newsid` (`news_id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin AUTO_INCREMENT=1;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_news_comments -------------------------- #


# --------------------------- flip_news_news --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_news_news`;
CREATE TABLE `$prefix$flip_news_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `on_top` enum('1','0') COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `date` int(11) NOT NULL DEFAULT '0',
  `view_right` int(32) DEFAULT NULL,
  `author` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `title` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `text` mediumtext COLLATE utf8mb4_bin NOT NULL,
  `category` varchar(100) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`),
  KEY `i_category` (`category`),
  KEY `i_date` (`date`),
  KEY `i_ontop` (`on_top`),
  KEY `i_viewright` (`view_right`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_news_news -------------------------- #


# --------------------------- flip_paypal_buttons --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_paypal_buttons`;
CREATE TABLE `$prefix$flip_paypal_buttons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `item_name` mediumtext COLLATE utf8mb4_bin NOT NULL,
  `item_number` mediumtext COLLATE utf8mb4_bin NOT NULL,
  `amount` float NOT NULL DEFAULT '0',
  `return` mediumtext COLLATE utf8mb4_bin NOT NULL,
  `cancel_return` mediumtext COLLATE utf8mb4_bin NOT NULL,
  `notify_url` mediumtext COLLATE utf8mb4_bin NOT NULL,
  `currency_code` mediumtext COLLATE utf8mb4_bin NOT NULL,
  `business` mediumtext COLLATE utf8mb4_bin NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `ppurl` mediumtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_paypal_buttons -------------------------- #


# --------------------------- flip_paypal_transactions --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_paypal_transactions`;
CREATE TABLE `$prefix$flip_paypal_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tx_id` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `formstring` mediumtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tx_id` (`tx_id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_paypal_transactions -------------------------- #


# --------------------------- flip_poll_list --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_poll_list`;
CREATE TABLE `$prefix$flip_poll_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `view_right` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  `titel` varchar(40) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `text` mediumtext COLLATE utf8mb4_bin,
  PRIMARY KEY (`id`),
  UNIQUE KEY `titel` (`titel`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin AUTO_INCREMENT=1;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_poll_list -------------------------- #


# --------------------------- flip_poll_users --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_poll_users`;
CREATE TABLE `$prefix$flip_poll_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `poll_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`),
  KEY `i_poll_user` (`poll_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin AUTO_INCREMENT=1;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_poll_users -------------------------- #


# --------------------------- flip_poll_votes --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_poll_votes`;
CREATE TABLE `$prefix$flip_poll_votes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `poll_id` int(11) NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `text` varchar(240) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin AUTO_INCREMENT=1;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_poll_votes -------------------------- #


# --------------------------- flip_seats --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_seats`;

# Diese Tabelle wird nicht weiter verwendet.
# --------------------------- /flip_seats -------------------------- #


# --------------------------- flip_seats_blocks --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_seats_blocks`;
CREATE TABLE `$prefix$flip_seats_blocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `view_right` int(11) NOT NULL DEFAULT '0',
  `caption` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8mb4_bin NOT NULL,
  `background` longblob,
  `background_tmp` longblob,
  `topleft_x` int(11) NOT NULL DEFAULT '0',
  `topleft_y` int(11) NOT NULL DEFAULT '0',
  `scale` float NOT NULL DEFAULT '0',
  `imagedir_block` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `imagedir_overview` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `link` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `cords` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `is_adult` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_seats_blocks -------------------------- #


# --------------------------- flip_seats_seats --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_seats_seats`;
CREATE TABLE `$prefix$flip_seats_seats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `block_id` int(11) NOT NULL DEFAULT '1',
  `enabled` enum('Y','N') COLLATE utf8mb4_bin NOT NULL DEFAULT 'Y',
  `center_x` int(11) NOT NULL DEFAULT '0',
  `center_y` int(11) NOT NULL DEFAULT '0',
  `angle` int(11) NOT NULL DEFAULT '0',
  `cords` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `user_id` int(11) DEFAULT NULL,
  `user_id_clan` int(11) DEFAULT NULL,
  `user_status` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `reserved` enum('Y','N') COLLATE utf8mb4_bin NOT NULL DEFAULT 'N',
  `name` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `ip` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`),
  KEY `block_id` (`block_id`)
) ENGINE=InnoDB AUTO_INCREMENT=743 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_seats_seats -------------------------- #


# --------------------------- flip_sendmessage_message --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_sendmessage_message`;
CREATE TABLE `$prefix$flip_sendmessage_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` enum('sys','user') COLLATE utf8mb4_bin NOT NULL DEFAULT 'user',
  `name` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT '',
  `vars` varchar(255) COLLATE utf8mb4_bin DEFAULT '',
  `subject` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `message` mediumtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_name` (`name`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Die wichtigsten Datensaetze:
INSERT INTO `$prefix$flip_sendmessage_message` VALUES (2,CURRENT_TIMESTAMP,'sys','user_account_changepassword','','','Vergessenes Passwort auf {$server.HTTP_HOST}','Hallo {$name},\r\n\r\nJemand hat bei uns auf {$server.HTTP_HOST} diese Mail zum Ändern des Passworts angefordert. \r\nSollte dies nicht auf deinen Wunsch hin geschehen sein, so kannst du diese Nachricht ignorieren.\r\n\r\nUm dein Password zu ändern, folge bitte diesem Link:\r\nhttp://{$server.HTTP_HOST}{$server.SCRIPT_NAME}?frame=setpwd&id={$id}&code={$random_id}\r\n\r\nMit freundlichen Grüßen,\r\ndein Orga-Team');
INSERT INTO `$prefix$flip_sendmessage_message` VALUES (3,CURRENT_TIMESTAMP,'sys','user_account_reactivate','Reaktivierungsmail bei neuer EMail-Adresse','','Reaktivierung deines Accounts auf {$server.HTTP_HOST}','Hallo {$name},\r\n\r\ndu hast soeben deine Emailadresse geändert. \r\nÜber den folgenden Link kannst du deinen Account wieder aktivieren:\r\n\r\nhttp://{$server.HTTP_HOST}{$server.SCRIPT_NAME}?action=activate&id={$id}&code={$random_id}\r\n\r\n\r\nMit freundlichen Grüßen,\r\ndein Orga-Team');
INSERT INTO `$prefix$flip_sendmessage_message` VALUES (4,CURRENT_TIMESTAMP,'sys','user_account_activate','','','Aktivierung deines Accounts auf {$server.HTTP_HOST}','Hallo {$name},\r\n\r\nvielen Dank für die Erstellung eines Benutzers im Intranet auf {$server.HTTP_HOST}.\r\nDiesen Account benötigst du, um dich für die kommenden LAN-Parties anmelden zu können.\r\n\r\nVorher musst Du jedoch auf folgenden Link klicken, um deinen Account einmalig zu aktivieren:\r\nhttp://{$server.HTTP_HOST}{$server.SCRIPT_NAME}?frame=registered&action=activate&id={$id}&code={$random_id}\r\n\r\n\r\nMit freundlichen Grüßen,\r\ndein Orga-Team!');
INSERT INTO `$prefix$flip_sendmessage_message` VALUES (5,CURRENT_TIMESTAMP,'sys','messaging_notice','','','{$server.HTTP_HOST}: Neue Nachricht von {$sender}','Hallo {$name},\r\n\r\nin deinem Postfach auf {$server.HTTP_HOST} wartet eine neue WebMessage darauf, von dir gelesen zu werden.\r\n\r\nAbsender: {$sender}\r\nBetreff: {$subject}\r\nLesen: http://{$server.HTTP_HOST}{$server.SCRIPT_DIR}webmessage.php?frame=viewmessage&message={$message_id}&box=owner\r\n\r\n--\r\n{$server.HTTP_HOST}');
INSERT INTO `$prefix$flip_sendmessage_message` VALUES (6,CURRENT_TIMESTAMP,'sys','messaging_message','','','{$subject}','Von: {$sender}\r\n\r\nBtr: {$subject}\r\n\r\n\r\n{$message}\r\n\r\n--\r\nLesen: http://{$server.HTTP_HOST}{$server.SCRIPT_DIR}webmessage.php?frame=viewmessage&message={$message_id}&box=owner\r\nAntworten: http://{$server.HTTP_HOST}{$server.SCRIPT_DIR}webmessage.php?frame=sendmessage&type=replay&message_id={$message_id}\r\nWeiterleiten: http://{$server.HTTP_HOST}{$server.SCRIPT_DIR}webmessage.php?frame=sendmessage&type=forward&message_id={$message_id}\r\n\r\n--\r\nDiese Nachricht wurde als WebMessage über {$server.HTTP_HOST} an dich versandt.\r\n');
INSERT INTO `$prefix$flip_sendmessage_message` VALUES (7,CURRENT_TIMESTAMP,'sys','server_add','Ein Server wurde hinzugefügt bzw. die IP geändert','','Servereinstellungen','Hallo {$givenname},\r\n\r\nDie Konfiguration für deinen Server "{$name}" auf {$server.HTTP_HOST} hat sich geändert.\r\n{#IF $atparty!="Y"}Du hast eingestellt, dass du ihn zur kommenden Party nicht mitbringen wirst, ist das richtig?\r\n{#END}\r\nIP: {#WHEN empty($ip) "nicht vergeben" $ip}\r\n{#IF !empty($server_dns)}DNS-Name: {$server_dns}{#END}\r\n{#IF !empty($server_dns_request)}Wunsch-DNS: {$server_dns_request}{#END}\r\n{#IF !empty($server_gateway)}Gateway: {$server_gateway}{#END}\r\n{#IF !empty($server_subnetmask)}Subnetmask: {$server_subnetmask}{#END}\r\n{#IF !empty($server_dns_ip)}DNS-Server: {$server_dns_ip}{#END}\r\n{#IF !empty($server_wins_ip)}WINS-Server: {$server_wins_ip}{#END}\r\n\r\nServerdaten:\r\n(können unter "meine Daten" -> "meine Server" geändert werden)\r\nHardware:       {%hardware}\r\nBetriebssystem: {#TABLEDISPLAY server_oses $os}\r\nDienste:        {#TABLEDISPLAY server_services $services}\r\nSpiele:         {#TABLEDISPLAY server_games $games}\r\nBeschreibung: \r\n{$description}\r\n\r\nSchönen Gruß,\r\ndein Orga-Team');
INSERT INTO `$prefix$flip_sendmessage_message` VALUES (8,CURRENT_TIMESTAMP,'sys','user_status_paid','Benutzer wurde auf bezahlt gesetzt','','Dein Status hat sich geändert!','Hallo {$name},\r\n\r\ndein Status wurde soeben auf bezahlt geändert.\r\nDamit kannst du dir jetzt einen festen Sitzplatz reservieren.\r\n\r\nMit freundlichen Grüßen,\r\ndein Orga-Team!\r\n');
INSERT INTO `$prefix$flip_sendmessage_message` VALUES (9,CURRENT_TIMESTAMP,'sys','tournament_matchready','Ein Match kann gespielt werden. Die Teams werden hierüber informiert','','Match im {$tournament_name}','Hallo {$nickname},\r\n\r\nein {$game}-Match gegen "{$opponent}" ist zu spielen. Bitte bereite dein Team vor und trage es als bereit ein, indem du auf den Link unten klickst.\r\nWenn dein Gegner bereit ist, bekommst du noch eine Nachricht.\r\n\r\nBereit: {$link}');
INSERT INTO `$prefix$flip_sendmessage_message` VALUES (10,CURRENT_TIMESTAMP,'sys','tournament_teamready','Wenn ein Team bereit ist, bekommt der Gegner diese Nachricht','','Turniergegner im {$tournament_name} ist bereit','Hi {$nickname},\r\n\r\ndein {$game} Gegner "{$opponent}" ist bereit zum spielen.');
INSERT INTO `$prefix$flip_sendmessage_message` VALUES (11,CURRENT_TIMESTAMP,'sys','clan_join','Wird verschickt, wernn jemand eine Anfrage an einen Clan schickt','','Anfrage für Clan {$clanname}','Hallo {$name},\r\n\r\nin deinem Clan \'{$clanname}\' möchte {$member} aufgenommen werden.\r\n\r\nAkzeptieren: {$link}\r\n\r\n\r\nDein\r\nOrga-Team');
INSERT INTO `$prefix$flip_sendmessage_message` VALUES (12,CURRENT_TIMESTAMP,'sys','flip_paypal_paymentsuccess','Wird bei einer erfolgten PayPal-Zahlung gesendet','','PayPal: Freigeschalten','Hallo {$name}!\r\n\r\n{#IF $name == $paypal_targetname}\r\nDas PayPal-System hat deine Zahlung registriert und dich freigeschalten!\r\nDu kannst dir jetzt auf dem Sitzplan einen Sitzplatz reservieren.\r\n\r\nViel Spaß wünscht dir das Orga-Team!\r\n{#ELSE}\r\nDer User {$paypal_targetname} wurde mit dem PayPal-System freigeschalten.\r\nDie Daten vom IPN lauten wie folgt:\r\n\r\n{$paypal_ipn_info}\r\n\r\nDie Freischaltung erfolgte am {$paypal_ipn_date}\r\n{#END}\r\n');
INSERT INTO `$prefix$flip_sendmessage_message` VALUES (13,CURRENT_TIMESTAMP,'sys','flip_paypal_paymentfailure','Wird bei einer fehlgeschlagenen PayPal-Zahlung gesendet','','PayPal: Fehler','Hallo {$name}!\n\n{#IF $name == $paypal_targetname}\nDas PayPal-System hat bei deiner Zahlung folgenden Fehler festgestellt:\n\n{$paypal_ipn_errormsg}\n\nDein Account wurde deshalb nicht freigeschalten - bitte wende dich an die Orgas!\n\n{#ELSE}\nBeim Versuch den User {$paypal_targetname} freizuschalten trat ein Fehler auf!\nDas System meldete:\n\n{$paypal_ipn_errormsg}\n\nDie Daten vom IPN lauten wie folgt:\n\n{$paypal_ipn_info}\n\n{#END}');
INSERT INTO `$prefix$flip_sendmessage_message` VALUES (14,CURRENT_TIMESTAMP,'sys','watches_notify','Watches-Meldungen','','Watches','Hallo {$name}!\r\n\r\nDu besitzt einen oder mehrere Watches im Forum und folgendes ist passiert:\r\n\r\n- {$actionmsg}\r\n');
INSERT INTO `$prefix$flip_sendmessage_message` VALUES (15,CURRENT_TIMESTAMP,'sys','inet_notification','Nachricht vom INet-Sys','','INet: Nachricht','Hallo {$name}!\r\n{#IF $inet_status == \'waiting\'}\r\nDu hast einen Antrag auf Internet gestellt, bist aber in die Warteschlange eingereiht worden. Bitte gedulde dich, bis du an der Reihe bist - du erhälst dann automatisch eine weitere Webmessage!\r\n{#ELSE}{#IF $inet_status == \'granted\'}\r\nDas Internet wurde für dich am {$inet_granttime} freigeschalten! Von da an hast du {$inet_accesstimespan} Zeit um deine Updates etc. durchzuführen.\r\n{#ELSE}{#IF $inet_status == \'expired\'}\r\nDein Internetzugang wurde wieder gesperrt, da deine Online-Zeit abgelaufen ist. Falls es deswegen zu Problemen gekommen ist, wende dich bitte an die Organisation!\r\n{#ELSE}\r\nDas Internet wurde für dich auf unbegrenzte Zeit freigeschalten.\r\n{#END}{#END}{#END}\r\nGruß,\r\nDas Orga-Team');
# --------------------------- /flip_sendmessage_message -------------------------- #


# --------------------------- flip_session --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_session`;

# Diese Tabelle wird nicht weiter verwendet.
# --------------------------- /flip_session -------------------------- #


# --------------------------- flip_session_cache --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_session_cache`;
CREATE TABLE `$prefix$flip_session_cache` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `session_id` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `type` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `data` mediumtext COLLATE utf8mb4_bin NOT NULL,
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `i_session` (`session_id`),
  KEY `i_type` (`type`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Diese Datensaetze sind nur Temporaer.
# --------------------------- /flip_session_cache -------------------------- #


# --------------------------- flip_session_data --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_session_data`;
CREATE TABLE `$prefix$flip_session_data` (
  `id` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `useragent` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `remoteaddr` varchar(20) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `data` mediumtext COLLATE utf8mb4_bin NOT NULL,
  `time_create` int(11) NOT NULL DEFAULT '0',
  `time_lastseen` int(11) NOT NULL DEFAULT '0',
  `donext` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `i_userid` (`user_id`),
  KEY `i_lastseen` (`time_lastseen`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Diese Datensaetze sind nur Temporaer.
# --------------------------- /flip_session_data -------------------------- #


# --------------------------- flip_setpaid_history --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_setpaid_history`;
CREATE TABLE `$prefix$flip_setpaid_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date` int(11) NOT NULL DEFAULT '0',
  `orga_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_setpaid_history -------------------------- #


# --------------------------- flip_shop_groups --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_shop_groups`;
CREATE TABLE `$prefix$flip_shop_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` text COLLATE utf8mb4_bin NOT NULL,
  `description` text COLLATE utf8mb4_bin NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `imageid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_shop_groups -------------------------- #


# --------------------------- flip_shop_items --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_shop_items`;
CREATE TABLE `$prefix$flip_shop_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `groupid` int(11) NOT NULL,
  `name` text COLLATE utf8mb4_bin NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `description` text COLLATE utf8mb4_bin NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `stockenabled` tinyint(1) NOT NULL DEFAULT '1',
  `stockamount` int(11) NOT NULL DEFAULT '0',
  `queued` tinyint(1) NOT NULL DEFAULT '0',
  `imageid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_shop_items -------------------------- #


# --------------------------- flip_shop_order_items --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_shop_order_items`;
CREATE TABLE `$prefix$flip_shop_order_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `orderid` int(11) NOT NULL,
  `itemid` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_shop_order_items -------------------------- #


# --------------------------- flip_shop_orders --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_shop_orders`;
CREATE TABLE `$prefix$flip_shop_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ordertime` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `status` enum('shoppingcart','cashdesk','ordered','waiting','readyforpickup','completed') COLLATE utf8mb4_bin NOT NULL DEFAULT 'shoppingcart',
  `paid` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_shop_orders -------------------------- #


# --------------------------- flip_shop_user_balance --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_shop_user_balance`;
CREATE TABLE `$prefix$flip_shop_user_balance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `userid` int(11) NOT NULL,
  `balance` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_shop_user_balance -------------------------- #


# --------------------------- flip_sponsor_ads --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_sponsor_ads`;
CREATE TABLE `$prefix$flip_sponsor_ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` enum('banner','button') COLLATE utf8mb4_bin NOT NULL DEFAULT 'banner',
  `sponsor_id` int(11) NOT NULL DEFAULT '0',
  `image` longblob NOT NULL,
  `image_mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `html` mediumtext COLLATE utf8mb4_bin NOT NULL,
  `show_as_ad` tinyint(4) NOT NULL DEFAULT '0',
  `show_in_list` tinyint(4) NOT NULL DEFAULT '0',
  `priority` int(11) NOT NULL DEFAULT '0',
  `link` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `clicks` int(11) NOT NULL DEFAULT '0',
  `views` int(11) NOT NULL DEFAULT '0',
  `sessions` int(11) NOT NULL DEFAULT '0',
  `enable_view_log` tinyint(4) NOT NULL DEFAULT '0',
  `enable_click_log` tinyint(4) NOT NULL DEFAULT '0',
  `enable_session_log` tinyint(4) NOT NULL DEFAULT '0',
  `enable_counter` tinyint(4) NOT NULL DEFAULT '0',
  `last_counter_reset` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`),
  KEY `type` (`type`),
  KEY `i_asad_type` (`show_as_ad`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_sponsor_ads -------------------------- #


# --------------------------- flip_sponsor_log --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_sponsor_log`;
CREATE TABLE `$prefix$flip_sponsor_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reason` enum('view','session','click') COLLATE utf8mb4_bin NOT NULL DEFAULT 'view',
  `ad_id` int(11) NOT NULL DEFAULT '0',
  `session_id` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `ip` varchar(15) COLLATE utf8mb4_bin DEFAULT NULL,
  `location` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_sponsor_log -------------------------- #


# --------------------------- flip_sponsor_sponsors --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_sponsor_sponsors`;
CREATE TABLE `$prefix$flip_sponsor_sponsors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `public` tinyint(4) NOT NULL DEFAULT '0',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `homepage` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `status` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `description` mediumtext COLLATE utf8mb4_bin,
  `comment` mediumtext COLLATE utf8mb4_bin,
  `priority` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '5',
  `category` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `phone` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `street` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `city` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `contact` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `responsible` int(11) NOT NULL DEFAULT '0',
  `amount` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `last_edit_user` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_sponsor_sponsors -------------------------- #


# --------------------------- flip_sponsors --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_sponsors`;

# Diese Tabelle wird nicht weiter verwendet.
# --------------------------- /flip_sponsors -------------------------- #


# --------------------------- flip_table --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_table`;

# Diese Tabelle wird nicht weiter verwendet.
# --------------------------- /flip_table -------------------------- #


# --------------------------- flip_table_entry --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_table_entry`;
CREATE TABLE `$prefix$flip_table_entry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `table_id` int(11) NOT NULL DEFAULT '0',
  `key` varchar(100) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8mb4_bin DEFAULT '',
  `display` mediumtext COLLATE utf8mb4_bin NOT NULL,
  `display_code` mediumtext COLLATE utf8mb4_bin NOT NULL,
  `code_exec` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_key` (`table_id`,`key`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=196 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze:
INSERT INTO `$prefix$flip_table_entry` VALUES (1,CURRENT_TIMESTAMP,1,0,'eröffnet','<span style="color:blue">eröffnet</span>','<span style="color:blue">eröffnet</span>',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (2,CURRENT_TIMESTAMP,1,1,'in Arbeit','<span style="color:orange; font-weight:bold;">in Arbeit</span>','<span style="color:orange; font-weight:bold;">in Arbeit</span>',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (3,CURRENT_TIMESTAMP,2,0,'0 - keine','0 - keine','0 - keine',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (4,CURRENT_TIMESTAMP,2,3,'3 - niedrig','3 - niedrig','3 - niedrig',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (5,CURRENT_TIMESTAMP,2,5,'5 - mittlere','5 - mittlere','5 - mittlere',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (6,CURRENT_TIMESTAMP,2,9,'9 - sehr wichtig','9 - sehr wichtig','9 - sehr wichtig',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (7,CURRENT_TIMESTAMP,3,0,'alle Kategorien','alle Kategorien','alle Kategorien',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (8,CURRENT_TIMESTAMP,3,1,'Web','Web','Web',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (9,CURRENT_TIMESTAMP,3,2,'Catering/Stände','Catering/Stände','Catering/Stände',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (10,CURRENT_TIMESTAMP,3,3,'DMZ','DMZ','DMZ',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (11,CURRENT_TIMESTAMP,1,2,'testbereit','<span style="color:green; font-weight:bold;">testbereit</span>','<span style="color:green; font-weight:bold;">testbereit</span>',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (12,CURRENT_TIMESTAMP,1,101,'realisiert','<span style="color:gray">realisiert</span>','<span style="color:gray">realisiert</span>',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (13,CURRENT_TIMESTAMP,3,4,'Allgemein','Allgemein','Allgemein',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (14,CURRENT_TIMESTAMP,3,5,'Anfahrt/Parken','Anfahrt/Parken','Anfahrt/Parken',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (15,CURRENT_TIMESTAMP,3,6,'Einlass','Einlass','Einlass',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (16,CURRENT_TIMESTAMP,3,7,'Turniere','Turniere','Turniere',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (17,CURRENT_TIMESTAMP,3,8,'(Rahmen)Programm','(Rahmen)Programm','(Rahmen)Programm',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (18,CURRENT_TIMESTAMP,3,9,'Abbau/Nachbereitung','Abbau/Nachbereitung','Abbau/Nachbereitung',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (19,CURRENT_TIMESTAMP,3,10,'Licht/Tontechnik & Atmosphäre','Licht/Tontechnik & Atmosphäre','Licht/Tontechnik & Atmosphäre',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (20,CURRENT_TIMESTAMP,3,11,'Benötigtes Equipment','benötigtes Equipment','benötigtes Equipment',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (21,CURRENT_TIMESTAMP,3,12,'Bugs','Bugs','Bugs',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (22,CURRENT_TIMESTAMP,2,1,'1 - unwichtig','1 - unwichtig','1 - unwichtig',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (23,CURRENT_TIMESTAMP,2,2,'2 - gering','2 - gering','2 - gering',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (24,CURRENT_TIMESTAMP,2,4,'4 - gemäßigt','4 - gemäßigt','4 - gemäßigt',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (25,CURRENT_TIMESTAMP,2,6,'6 - gehobene','6 - gehobene','6 - gehobene',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (26,CURRENT_TIMESTAMP,2,7,'7 - hoch','7 - hoch','7 - hoch',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (27,CURRENT_TIMESTAMP,2,8,'8 - sehr hoch','8 - sehr hoch','8 - sehr hoch',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (28,CURRENT_TIMESTAMP,4,'aom','Age of Mythology','Age of Mythology','Age of Mythology',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (29,CURRENT_TIMESTAMP,4,'bf42','Battlefield 1942','Battlefield 1942','Battlefield 1942',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (30,CURRENT_TIMESTAMP,4,'bfdc','Battlefield Desert Combat','Battlefield Desert Combat','Battlefield Desert Combat',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (31,CURRENT_TIMESTAMP,4,'bfv','BF: Vietnam','BF: Vietnam','BF: Vietnam',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (32,CURRENT_TIMESTAMP,4,'bv','Blobby Volley','Blobby Volley','Blobby Volley',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (33,CURRENT_TIMESTAMP,4,'ccg','CnC Generäle','CnC Generäle','CnC Generäle',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (34,CURRENT_TIMESTAMP,4,'cod2','CoD2 TDM','CoD2 TDM','CoD2 TDM',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (35,CURRENT_TIMESTAMP,4,'cs','Counter-Strike','Counter-Strike','Counter-Strike',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (36,CURRENT_TIMESTAMP,4,'d2l','Diablo 2 LoD','Diablo 2 LoD','Diablo 2 LoD',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (37,CURRENT_TIMESTAMP,4,'dn3d','Duke Nukem 3D','Duke Nukem 3D','Duke Nukem 3D',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (38,CURRENT_TIMESTAMP,4,'ee','Empire Earth II','Empire Earth II','Empire Earth II',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (39,CURRENT_TIMESTAMP,4,'et','Enemy Territory','Enemy Territory','Enemy Territory',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (40,CURRENT_TIMESTAMP,4,'fc','Far Cry','Far Cry','Far Cry',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (41,CURRENT_TIMESTAMP,4,'fifa','FIFA 2006','FIFA 2006','FIFA 2006',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (42,CURRENT_TIMESTAMP,4,'hl','Half-Life','Half-Life','Half-Life',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (43,CURRENT_TIMESTAMP,4,'hldm','Half-Life DeathMatch','Half-Life DeathMatch','Half-Life DeathMatch',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (44,CURRENT_TIMESTAMP,4,'jk2','Jedi Knight II','Jedi Knight II','Jedi Knight II',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (45,CURRENT_TIMESTAMP,4,'jkja','Jedi Knight: Jedi Academy','Jedi Knight: Jedi Academy','Jedi Knight: Jedi Academy',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (46,CURRENT_TIMESTAMP,4,'moh','Medal of Honor','Medal of Honor','Medal of Honor',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (47,CURRENT_TIMESTAMP,4,'nfmc','NFSMW Circuit','NFSMW Circuit','NFSMW Circuit',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (48,CURRENT_TIMESTAMP,4,'q2','Quake II','Quake II','Quake II',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (49,CURRENT_TIMESTAMP,4,'q3','Quake III','Quake III','Quake III',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (50,CURRENT_TIMESTAMP,4,'q3a','Quake III Arena','Quake III Arena','Quake III Arena',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (51,CURRENT_TIMESTAMP,4,'rtcw','Return to Castle Wolfenstein','Return to Castle Wolfenstein','Return to Castle Wolfenstein',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (52,CURRENT_TIMESTAMP,4,'savg','Savage','Savage','Savage',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (53,CURRENT_TIMESTAMP,4,'sc','StarCraft','StarCraft','StarCraft',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (54,CURRENT_TIMESTAMP,4,'scbw','Broodwar','Broodwar','Broodwar',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (55,CURRENT_TIMESTAMP,4,'sf2','Soldier of Fortune 2','Soldier of Fortune 2','Soldier of Fortune 2',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (56,CURRENT_TIMESTAMP,4,'soa','Soldiers of Anarchy','Soldiers of Anarchy','Soldiers of Anarchy',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (57,CURRENT_TIMESTAMP,4,'ss2','Serious Sam 2','Serious Sam 2','Serious Sam 2',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (58,CURRENT_TIMESTAMP,4,'tetr','TetriNet','TetriNet','TetriNet',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (59,CURRENT_TIMESTAMP,4,'to','Tactical Ops','Tactical Ops','Tactical Ops',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (60,CURRENT_TIMESTAMP,4,'ut','Unreal Tournament','Unreal Tournament','Unreal Tournament',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (61,CURRENT_TIMESTAMP,4,'ut04','Unreal Tournament 2004','Unreal Tournament 2004','Unreal Tournament 2004',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (62,CURRENT_TIMESTAMP,4,'wc3','WarCraft 3 TFT','WarCraft 3 TFT','WarCraft 3 TFT',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (63,CURRENT_TIMESTAMP,4,'sldn','Söldner','Söldner','Söldner',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (64,CURRENT_TIMESTAMP,4,'sold','Soldat','Soldat','Soldat',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (65,CURRENT_TIMESTAMP,5,1,'freilos','freilos','freilos',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (66,CURRENT_TIMESTAMP,5,2,'frei|os','frei|os','frei|os',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (67,CURRENT_TIMESTAMP,5,3,'freiIos','freiIos','freiIos',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (68,CURRENT_TIMESTAMP,4,'nfmd','NFSMW Drag','NFSMW Drag','NFSMW Drag',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (69,CURRENT_TIMESTAMP,4,'nfs','Need for Speed Underground','Need for Speed Underground','Need for Speed Underground',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (70,CURRENT_TIMESTAMP,4,'gc2','Ground Control 2','Ground Control 2','Ground Control 2',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (71,CURRENT_TIMESTAMP,4,'d3','DOOM 3','DOOM 3','DOOM 3',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (72,CURRENT_TIMESTAMP,4,'tm','Trackmania','Trackmania','Trackmania',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (73,CURRENT_TIMESTAMP,6,0,'Nicht bearbeitet','Nicht bearbeitet','Nicht bearbeitet',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (74,CURRENT_TIMESTAMP,6,1,'Angefragt','Angefragt','Angefragt',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (75,CURRENT_TIMESTAMP,6,2,'Absage','Absage','Absage',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (76,CURRENT_TIMESTAMP,6,3,'Zusage','Zusage','Zusage',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (77,CURRENT_TIMESTAMP,6,4,'in Verhandlung','in Verhandlung','in Verhandlung',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (78,CURRENT_TIMESTAMP,7,0,'0 - keine','0 - keine','0 - keine',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (79,CURRENT_TIMESTAMP,7,1,'1 - unwichtig','1 - unwichtig','1 - unwichtig',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (80,CURRENT_TIMESTAMP,7,2,'2 - gering','2 - gering','2 - gering',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (81,CURRENT_TIMESTAMP,7,3,'3 - niedrig','3 - niedrig','3 - niedrig',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (82,CURRENT_TIMESTAMP,7,4,'4 - gemäßigt','4 - gemäßigt','4 - gemäßigt',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (83,CURRENT_TIMESTAMP,7,5,'5 - mittlere','5 - mittlere','5 - mittlere',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (84,CURRENT_TIMESTAMP,7,6,'6 - gehobene','6 - gehobene','6 - gehobene',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (85,CURRENT_TIMESTAMP,7,7,'7 - hoch','7 - hoch','7 - hoch',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (86,CURRENT_TIMESTAMP,7,8,'8 - sehr hoch','8 - sehr hoch','8 - sehr hoch',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (87,CURRENT_TIMESTAMP,7,9,'9 - sehr wichtig','9 - sehr wichtig','9 - sehr wichtig',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (88,CURRENT_TIMESTAMP,1,102,'abgelehnt','<span style="color:gray">abgelehnt</span>','<span style="color:gray">abgelehnt</span>',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (89,CURRENT_TIMESTAMP,1,103,'doppelt','<span style="color:gray">doppelt</span>','<span style="color:gray">doppelt</span>',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (90,CURRENT_TIMESTAMP,8,1,'HL/CS','HL/CS','HL/CS',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (91,CURRENT_TIMESTAMP,8,2,'BF1942','BF1942','BF1942',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (92,CURRENT_TIMESTAMP,8,3,'Quake3','Quake3','Quake3',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (93,CURRENT_TIMESTAMP,8,4,'UT2004','UT2004','UT2004',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (94,CURRENT_TIMESTAMP,8,5,'CoD','CoD','CoD',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (95,CURRENT_TIMESTAMP,8,6,'ET','ET','ET',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (96,CURRENT_TIMESTAMP,9,1,'HTTP','HTTP','HTTP',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (97,CURRENT_TIMESTAMP,9,2,'FTP','FTP','FTP',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (98,CURRENT_TIMESTAMP,9,3,'SMB','SMB','SMB',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (99,CURRENT_TIMESTAMP,9,4,'IRC','IRC','IRC',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (100,CURRENT_TIMESTAMP,9,5,'TeamSpeak','TeamSpeak','TeamSpeak',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (101,CURRENT_TIMESTAMP,10,1,'Linux','Linux','Linux',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (102,CURRENT_TIMESTAMP,10,2,'Windows 2003','Windows 2003','Windows 2003',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (103,CURRENT_TIMESTAMP,10,3,'Windows XP','Windows XP','Windows XP',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (104,CURRENT_TIMESTAMP,10,4,'Windows 2000','Windows 2000','Windows 2000',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (105,CURRENT_TIMESTAMP,10,5,'Windows 9x','Windows 9x','Windows 9x',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (106,CURRENT_TIMESTAMP,10,6,'BSD','BSD','BSD',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (107,CURRENT_TIMESTAMP,10,7,'Solaris','Solaris','Solaris',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (108,CURRENT_TIMESTAMP,10,8,'Anderes','Anderes','Anderes',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (109,CURRENT_TIMESTAMP,4,'dow','Dawn of War','Dawn of War','Dawn of War',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (110,CURRENT_TIMESTAMP,4,'pes','Pro Evolution Soccer5','Pro Evolution Soccer5','Pro Evolution Soccer5',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (111,CURRENT_TIMESTAMP,4,'bfme','Schlacht um Mittelerde','Schlacht um Mittelerde','Schlacht um Mittelerde',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (112,CURRENT_TIMESTAMP,4,'css','CStrike: Source','CStrike: Source','CStrike: Source',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (113,CURRENT_TIMESTAMP,4,'hl2','Half-Life 2','Half-Life 2','Half-Life 2',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (114,CURRENT_TIMESTAMP,4,'dod','Day of Defeat','Day of Defeat','Day of Defeat',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (115,CURRENT_TIMESTAMP,4,'ut03','Unreal Tournament 2003','Unreal Tournament 2003','Unreal Tournament 2003',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (116,CURRENT_TIMESTAMP,11,'status_registered','angemeldet','<span style="color:#bb0000">angemeldet</span>','<span style="color:#bb0000">angemeldet</span>',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (117,CURRENT_TIMESTAMP,11,'status_paid','bezahlt','<span style="color:#007700">bezahlt</span>','<span style="color:#007700">bezahlt</span>',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (118,CURRENT_TIMESTAMP,11,'status_checked_in','eingecheckt','<span style="color:#bb880b">eingecheckt</span>','<span style="color:#bb880b">eingecheckt</span>',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (119,CURRENT_TIMESTAMP,11,'status_checked_out','ausgechecket','<span style="color:#0000bb">ausgecheckt</span>','<span style="color:#0000bb">ausgecheckt</span>',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (120,CURRENT_TIMESTAMP,11,'status_online','online','online','online',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (121,CURRENT_TIMESTAMP,11,'status_offline','offline','offline','offline',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (122,CURRENT_TIMESTAMP,11,'orga','orga','orga','orga',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (123,CURRENT_TIMESTAMP,12,'','alle Kategorien','alle Kategorien','alle Kategorien',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (124,CURRENT_TIMESTAMP,12,'lanparty','LAN-Party','LAN-Party','LAN-Party',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (125,CURRENT_TIMESTAMP,12,'community','Community','Community','Community',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (126,CURRENT_TIMESTAMP,12,'esport','eSport','eSport','eSport',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (127,CURRENT_TIMESTAMP,12,'games','Spiele','Spiele','Spiele',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (128,CURRENT_TIMESTAMP,12,'hardware','Hardware','Hardware','Hardware',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (129,CURRENT_TIMESTAMP,12,'software','Software','Software','Software',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (130,CURRENT_TIMESTAMP,12,'party','Party','Party','Party',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (131,CURRENT_TIMESTAMP,12,'misc','Verschiedenes','Verschiedenes','Verschiedenes',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (132,CURRENT_TIMESTAMP,4,'cods','CoD2 SD','CoD2 SD','CoD2 SD',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (133,CURRENT_TIMESTAMP,4,'cod','Call of Duty S&D','Call of Duty S&D','Call of Duty S&D',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (134,CURRENT_TIMESTAMP,4,'q4','Quake 4','Quake 4','Quake 4',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (135,CURRENT_TIMESTAMP,4,'q4mx','Quake4 Max','Quake4 Max','Quake4 Max',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (136,CURRENT_TIMESTAMP,4,'bf','Battlefield 2','Battlefield 2','Battlefield 2',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (137,CURRENT_TIMESTAMP,4,'dsum','DSuM2','DSuM2','DSuM2',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (138,CURRENT_TIMESTAMP,4,'nfsu','NFSU2 Circuit','NFSU2 Circuit','NFSU2 Circuit',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (139,CURRENT_TIMESTAMP,4,'nfsd','NFSU2 Drag','NFSU2 Drag','NFSU2 Drag',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (140,CURRENT_TIMESTAMP,4,'pw','ParaWorld','ParaWorld','ParaWorld',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (141,CURRENT_TIMESTAMP,4,'pwa','ParaWorld Arena','ParaWorld Arena','ParaWorld Arena',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (142,CURRENT_TIMESTAMP,4,'pwd','ParaWorld Domination','ParaWorld Domination','ParaWorld Domination',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (143,CURRENT_TIMESTAMP,4,'q4xb','Quake 4 X-Battle Mod','Quake 4 X-Battle Mod','Quake 4 X-Battle Mod',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (144,CURRENT_TIMESTAMP,4,'rfbd','RfB MPDemo','RfB MPDemo','RfB MPDemo',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (145,CURRENT_TIMESTAMP,4,'tmn','Trackmania Nations','Trackmania Nations','Trackmania Nations',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (146,CURRENT_TIMESTAMP,4,'prey','Prey','Prey','Prey',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (147,CURRENT_TIMESTAMP,4,'me2','Medieval 2','Medieval 2','Medieval 2',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (148,CURRENT_TIMESTAMP,4,'mmv4','Micro MachinesV4','Micro MachinesV4','Micro MachinesV4',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (149,CURRENT_TIMESTAMP,13,'unrequested','Kein Antrag gestellt','<strong>Du hast keinen Antrag gestellt.</strong><br /><br />\\nKlicke auf den Button unten um einen zu stellen!','<b>Du hast keinen Antrag gestellt.</b><br /><br />\r\nKlicke auf den Button unten um einen zu stellen!',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (150,CURRENT_TIMESTAMP,13,'waiting','Antrag gestellt','<span style="color:#bbbb00;">Du bist in der Warteschlange (Platz {$inet_queueposition})!</span><br /><br /> Bitte gedulde dich, bis du an der Reihe bist<br /> oder nehme deinen Antrag zur&uuml;ck, indem du auf den Button unten klickst.','<font color="#bbbb00">Du bist in der Warteschlange (Platz <?php echo $var0[\'inet_queueposition\'] ?>)!</font><br /><br />\r\nBitte gedulde dich, bis du an der Reihe bist<br /> oder nehme deinen Antrag zur&uuml;ck, indem du auf den Button unten klickst.',1);
INSERT INTO `$prefix$flip_table_entry` VALUES (151,CURRENT_TIMESTAMP,13,'granted','Der User wurde freigeschalten','{#IF !empty($inet_timeleft)} <span style="color:green;">Dein Internetzugang wurde freigeschaltet!</span><br /><br /> Du hast noch<br /><br /><strong>{$inet_timeleft}</strong><br /><br /> Zeit bis dein Zugang wieder gesperrt wird. {#ELSE} <span style="color:red;">Deine Zeit ist leider um.</span><br /><br />Bitte beende alle Downloads, da dein Zugang jederzeit gesperrt werden kann!{#END}','<?php if(!empty($var0[\'inet_timeleft\'])) { ?>\r\n\r\n<font color="green">Dein Internetzugang ist freigeschalten worden!</font><br /><br />\r\nDu hast noch<br /><br /><b><?php echo $var0[\'inet_timeleft\'] ?></b><br /><br /> Zeit bis dein Zugang wieder gesperrt wird.\r\n<?php } else { ?>\r\n\r\n<font color="red">Deine Zeit ist leider um.</font><br /><br />Bitte beende alle Downloads, da dein Zugang jederzeit gesperrt werden kann!\r\n<?php } ?>',1);
INSERT INTO `$prefix$flip_table_entry` VALUES (152,CURRENT_TIMESTAMP,13,'expired','Der Zugang ist wieder gesperrt worden','<span style="color:red;">Dein Internetzugang ist wieder gesperrt worden!</span><br /><br /> Falls ein Download l&auml;nger als erwartet braucht und durch die Sperrung unterbrochen wurde, wende dich bitte an die Orga.','<font color="red">Dein Internetzugang ist wieder gesperrt worden!</font><br /><br />\r\nFalls ein Download länger als erwartet braucht und durch die Sperrung unterbrochen wurde, wende dich bitte an die Orga.',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (153,CURRENT_TIMESTAMP,13,'noexpiry','Der User wurde auf unbegrenzte Zeit freigeschalten','<span style="color:blue;">Dein Internetzugang wurde auf unbegrenzte Zeit freigeschaltet.</span>','<font color="blue">Dein Internetzugang wurde auf unbegrenzte Zeit freigeschalten.</font>',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (154,CURRENT_TIMESTAMP,14,'waiting','Wartet','<span style="color:#bbbb00;"><strong>Wartet<strong></span>','<font color="#bbbb00"><b>Wartet<b></font>',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (155,CURRENT_TIMESTAMP,14,'granted','Freigeschalten','<span style="color:green;"><strong>Freigeschaltet<strong></span>','<font color="green"><b>Freigeschalten<b></font>',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (156,CURRENT_TIMESTAMP,14,'expired','Abgelaufen','<span style="color:red;"><strong>Abgelaufen</strong></span>','<font color="red"><b>Abgelau fen</b></font>',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (157,CURRENT_TIMESTAMP,14,'noexpiry','Dauerhaft freigeschalten','<span style="color:blue;"><strong>Dauerhaft freigeschalten</strong></span>','<font color="blue"><b>Dauerhaft freigeschalten</b></font>',0);
INSERT INTO `$prefix$flip_table_entry` VALUES (158,CURRENT_TIMESTAMP, 4, 'cod4','Call of Duty 4','Call of Duty 4','Call of Duty 4', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (159,CURRENT_TIMESTAMP, 4, 'pcw','PC weitwerfen','PC weitwerfen','PC weitwerfen', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (160,CURRENT_TIMESTAMP, 4, 'wc3d','DOTA','Warcraft 3 Mod','Warcraft 3 Mod', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (161,CURRENT_TIMESTAMP, 4, 'cod5','Call of Duty 5','Call of Duty 5','Call of Duty 5', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (162,CURRENT_TIMESTAMP, 4, 'fot2','Flatout 2','Flatout 2','Flatout 2', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (163,CURRENT_TIMESTAMP, 4, 'grid','Race Driver Grid','Race Driver Grid','Race Driver Grid', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (164,CURRENT_TIMESTAMP, 10, '9','Windows 2008','Windows 2008','Windows 2008', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (165,CURRENT_TIMESTAMP, 4, 'blur','Blur','Blur','Blur', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (166,CURRENT_TIMESTAMP, 4, 'sc2','StarCraft 2','StarCraft 2','StarCraft 2', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (167,CURRENT_TIMESTAMP, 4, 'lol','League of Legends','League of Legends','League of Legends', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (168,CURRENT_TIMESTAMP, 4, 'codb','Call Of Duty Black Ops','Call Of Duty Black Ops','Call Of Duty Black Ops', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (169,CURRENT_TIMESTAMP, 15, 'completed','<b><span style="color: #0000FF">Abgeschlossen</span></b>','<b><span style="color: #0000FF">Abgeschlossen</span></b>','<b><span style="color: #0000FF">Abgeschlossen</span></b>', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (170,CURRENT_TIMESTAMP, 15, 'ordered','<b><span style="color: #FF0000">Bestellt</span></b>','<b><span style="color: #FF0000">Bestellt</span></b>','<b><span style="color: #FF0000">Bestellt</span></b>', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (171,CURRENT_TIMESTAMP, 15, 'readyforpickup','<b><span style="color: #00AA00">Kann abgeholt werden</span></b>','<b><span style="color: #00AA00">Kann abgeholt werden</span></b>','<b><span style="color: #00AA00">Kann abgeholt werden</span></b>', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (172,CURRENT_TIMESTAMP, 15, 'waiting','<b><span style="color: #888800">Wartet</span></b>','<b><span style="color: #888800">Wartet</span></b>','<b><span style="color: #888800">Wartet</span></b>', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (173,CURRENT_TIMESTAMP, 4, 'flip','Flipper','Flipper','Flipper', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (174,CURRENT_TIMESTAMP, 4, '5er','5er Wettkampf','PC weitwerfen, RAM-Dart, Headset werfen, Maus abschlag, Quiz','PC weitwerfen, RAM-Dart, Headset werfen, Maus abschlag, Quiz', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (177,CURRENT_TIMESTAMP, 4, 'csgo','Counter Strike Global Offensive','Counter Strike Global Offensive','Counter Strike Global Offensive', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (176,CURRENT_TIMESTAMP, 10, '10','Windows2012','Windows2012','Windows2012', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (178,CURRENT_TIMESTAMP, 4, 'abm','Atomic Bomberman','Atomic Bomberman','Atomic Bomberman', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (179,CURRENT_TIMESTAMP, 4, 'hsw','Hearthstone: Heroes of Warcraft','Hearthstone: Heroes of Warcraft','Hearthstone: Heroes of Warcraft', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (180,CURRENT_TIMESTAMP, 4, 'rol','Rocket League','Rocket League','Rocket League', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (181,CURRENT_TIMESTAMP, 4, 'pokr','Poker Texas Hold''em','Poker Texas Hold''em','Poker Texas Hold''em', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (182,CURRENT_TIMESTAMP, 4, 'ovw','Blizzard Overwatch Community','Blizzard Overwatch Community','Blizzard Overwatch Community', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (183,CURRENT_TIMESTAMP, 4, 'mkat','Mario Kart','Mario Kart','Mario Kart', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (184,CURRENT_TIMESTAMP, 11, 'status_paid_clan','bezahlt','<span style="color: #007700;">bezahlt</span>','<span style="color: #007700;">bezahlt</span>', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (185,CURRENT_TIMESTAMP, 16, 'eventpartner','Eventpartner','Eventpartner','Eventpartner', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (186,CURRENT_TIMESTAMP, 16, 'hauptsponsor','Hauptsponsor','Hauptsponsor','Hauptsponsor', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (187,CURRENT_TIMESTAMP, 16, 'regionalesponsoren','Regionale Sponsoren','Regionale Unterst&uuml;tzer','Regionale Unterst&uuml;tzer', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (188,CURRENT_TIMESTAMP, 16, 'hardwarepartner','Hardware Partner','Hardware und Software Partner','Hardware und Software Partner', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (189,CURRENT_TIMESTAMP, 4, 'pubg','PUBG','Playerunknown''s Battlegrounds','Playerunknown''s Battlegrounds', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (190,CURRENT_TIMESTAMP, 16, 'partner','Partner','Partner','Partner', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (191,CURRENT_TIMESTAMP, 5, '4','Freilos','Freilos','Freilos', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (192,CURRENT_TIMESTAMP, 5, '5','FreiLos','FreiLos','FreiLos', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (193,CURRENT_TIMESTAMP, 5, '6','FreiIos','FreiIos','FreiIos', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (194,CURRENT_TIMESTAMP, 5, '7','freil0s','freil0s','freil0s', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (195,CURRENT_TIMESTAMP, 11, 'status_registered_console','angemeldet_console','<span style="color: #bb0000;">angemeldet_console</span>','<span style="color: #bb0000;">angemeldet_console</span>', 0);
INSERT INTO `$prefix$flip_table_entry` VALUES (196,CURRENT_TIMESTAMP,3,13,'Support','Support','Support',0);
# --------------------------- /flip_table_entry -------------------------- #


# --------------------------- flip_table_tables --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_table_tables`;
CREATE TABLE `$prefix$flip_table_tables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(100) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `edit_right` int(11) NOT NULL DEFAULT '0',
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `flip_tabellen_index_1` (`name`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze:
INSERT INTO `$prefix$flip_table_tables` VALUES (1,CURRENT_TIMESTAMP,'ticket_status',71,'Verschiedene Stadis für die Tickets (Stadis mit einem Key > 100 werden in der Standardansicht ausgeblendet)');
INSERT INTO `$prefix$flip_table_tables` VALUES (2,CURRENT_TIMESTAMP,'ticket_priority',71,'Prioritäten der Tickets');
INSERT INTO `$prefix$flip_table_tables` VALUES (3,CURRENT_TIMESTAMP,'ticket_category',71,'Kategorien der Tickets');
INSERT INTO `$prefix$flip_table_tables` VALUES (4,CURRENT_TIMESTAMP,'TOURNAMENT_GAMES',17,'Spielkürzel im Turniersystem zu vollem Spielenamen');
INSERT INTO `$prefix$flip_table_tables` VALUES (5,CURRENT_TIMESTAMP,'TOURNAMENT_BADNAMES',17,'Verbotene Teamnamen');
INSERT INTO `$prefix$flip_table_tables` VALUES (6,CURRENT_TIMESTAMP,'sponsor_status',12,'Bearbeitung-Status in der Sponsorenliste');
INSERT INTO `$prefix$flip_table_tables` VALUES (7,CURRENT_TIMESTAMP,'sponsor_priority',12,'Die Prioritäten der Sponsoren');
INSERT INTO `$prefix$flip_table_tables` VALUES (8,CURRENT_TIMESTAMP,'server_games',51,'Spiele die für "mein(e) Server" zur Auswahl stehen');
INSERT INTO `$prefix$flip_table_tables` VALUES (9,CURRENT_TIMESTAMP,'server_services',51,'Dienste die für "mein(e) Server" zur Auswahl stehen');
INSERT INTO `$prefix$flip_table_tables` VALUES (10,CURRENT_TIMESTAMP,'server_oses',51,'Betriebssysteme die für "mein(e) Server" zur Auswahl stehen');
INSERT INTO `$prefix$flip_table_tables` VALUES (11,CURRENT_TIMESTAMP,'lanparty_status',2,'Der Anmeldestatus zu der Lanparty (Achtung: zu jedem Status muss eine gleichnamige Gruppe existieren!)');
INSERT INTO `$prefix$flip_table_tables` VALUES (12,CURRENT_TIMESTAMP,'news_category',72,'Kategorien für die News.');
INSERT INTO `$prefix$flip_table_tables` VALUES (13,CURRENT_TIMESTAMP,'inet_status_messages',70,'Die Statusbeschreibungen des INet-Moduls');
INSERT INTO `$prefix$flip_table_tables` VALUES (14,CURRENT_TIMESTAMP,'inet_status_names',70,'Die Statusnamen des INet-Moduls (kurzform)');
INSERT INTO `$prefix$flip_table_tables` VALUES (15,CURRENT_TIMESTAMP,'shop_status_names',5,'Zeigt die Statusnamen des Shops schöner an');
INSERT INTO `$prefix$flip_table_tables` VALUES (16,CURRENT_TIMESTAMP,'sponsor_category', 12, 'Sponsoren Kategory');
# --------------------------- /flip_table_tables -------------------------- #


# --------------------------- flip_textdata --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_textdata`;
CREATE TABLE `$prefix$flip_textdata` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` text COLLATE utf8mb4_bin NOT NULL,
  `locale` varchar(5) COLLATE utf8mb4_bin NOT NULL DEFAULT 'de_DE',
  `text` text COLLATE utf8mb4_bin NOT NULL,
  `hash` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_textdata -------------------------- #


# --------------------------- flip_ticket --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_ticket`;

# Diese Tabelle wird nicht weiter verwendet.
# --------------------------- /flip_ticket -------------------------- #


# --------------------------- flip_ticket_event --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_ticket_event`;
CREATE TABLE `$prefix$flip_ticket_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ticket_id` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `create_user` int(11) NOT NULL DEFAULT '0',
  `description` mediumtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_ticket_event -------------------------- #


# --------------------------- flip_ticket_tickets --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_ticket_tickets`;
CREATE TABLE `$prefix$flip_ticket_tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ticket_id` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `category` varchar(20) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `opening_time` int(11) NOT NULL DEFAULT '0',
  `opening_user` int(11) NOT NULL DEFAULT '0',
  `create_user` int(11) NOT NULL DEFAULT '0',
  `resp_user` int(11) DEFAULT '0',
  `title` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `deadline_time` int(11) NOT NULL DEFAULT '0',
  `priority` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ticket_id` (`ticket_id`,`version`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_ticket_tickets -------------------------- #


# --------------------------- flip_tinfo_downloads --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_tinfo_downloads`;
CREATE TABLE `$prefix$flip_tinfo_downloads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tournament_id` int(11) NOT NULL DEFAULT '0',
  `description` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `link` mediumtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_tinfo_downloads -------------------------- #


# --------------------------- flip_tinfo_news --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_tinfo_news`;
CREATE TABLE `$prefix$flip_tinfo_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date` int(11) NOT NULL DEFAULT '0',
  `caption` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `content` mediumtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_tinfo_news -------------------------- #


# --------------------------- flip_tournament_coins --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_tournament_coins`;
CREATE TABLE `$prefix$flip_tournament_coins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `maxcoins` mediumint(9) NOT NULL DEFAULT '0',
  `currency` varchar(20) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Coins',
  PRIMARY KEY (`id`),
  UNIQUE KEY `currency` (`currency`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Die wichtigsten Datensaetze:
INSERT INTO `$prefix$flip_tournament_coins` VALUES (1,CURRENT_TIMESTAMP,30,'Coins');
# --------------------------- /flip_tournament_coins -------------------------- #


# --------------------------- flip_tournament_combatant --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_tournament_combatant`;
CREATE TABLE `$prefix$flip_tournament_combatant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `team_id` int(11) NOT NULL DEFAULT '0',
  `tournament_id` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `team_tournament` (`team_id`,`tournament_id`),
  KEY `i_tournamentid` (`tournament_id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_tournament_combatant -------------------------- #


# --------------------------- flip_tournament_groups --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_tournament_groups`;
CREATE TABLE `$prefix$flip_tournament_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order` mediumint(9) NOT NULL DEFAULT '0',
  `name` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `image` blob,
  PRIMARY KEY (`id`),
  UNIQUE KEY `order` (`order`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Die wichtigsten Datensaetze:
INSERT INTO `$prefix$flip_tournament_groups` VALUES (1,CURRENT_TIMESTAMP,1,'FUN-Turniere',NULL);
# --------------------------- /flip_tournament_groups -------------------------- #


# --------------------------- flip_tournament_invite --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_tournament_invite`;
CREATE TABLE `$prefix$flip_tournament_invite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `from` enum('team','user') COLLATE utf8mb4_bin NOT NULL DEFAULT 'team',
  `tournaments_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `team_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userperteam` (`user_id`,`team_id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_tournament_invite -------------------------- #


# --------------------------- flip_tournament_matches --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_tournament_matches`;
CREATE TABLE `$prefix$flip_tournament_matches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tournament_id` mediumint(9) NOT NULL DEFAULT '0',
  `team1` int(11) DEFAULT NULL,
  `team2` int(11) DEFAULT NULL,
  `points1` smallint(6) DEFAULT NULL,
  `points2` smallint(6) DEFAULT NULL,
  `ready1` int(11) DEFAULT NULL,
  `ready2` int(11) DEFAULT NULL,
  `endtime` int(11) NOT NULL DEFAULT '0',
  `level` float NOT NULL DEFAULT '0',
  `levelid` smallint(6) NOT NULL DEFAULT '0',
  `editor` int(11) NOT NULL DEFAULT '0',
  `comment` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `i_tournamentid` (`tournament_id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_tournament_matches -------------------------- #


# --------------------------- flip_tournament_ranking --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_tournament_ranking`;
CREATE TABLE `$prefix$flip_tournament_ranking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `combatant_id` int(11) NOT NULL DEFAULT '0',
  `tournament_id` int(11) NOT NULL DEFAULT '0',
  `rank` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `team_tournament` (`combatant_id`,`tournament_id`),
  KEY `i_tournamentid` (`tournament_id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_tournament_ranking -------------------------- #


# --------------------------- flip_tournament_servers --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_tournament_servers`;
CREATE TABLE `$prefix$flip_tournament_servers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `game` varchar(20) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `ip` varchar(21) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `name` varchar(63) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_game_name` (`game`,`name`),
  UNIQUE KEY `ip` (`ip`),
  KEY `mtime` (`mtime`),
  KEY `game` (`game`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_tournament_servers -------------------------- #


# --------------------------- flip_tournament_serverusage --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_tournament_serverusage`;
CREATE TABLE `$prefix$flip_tournament_serverusage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `server_id` int(11) NOT NULL DEFAULT '0',
  `match_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `match_id` (`match_id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin AUTO_INCREMENT=7;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_tournament_serverusage -------------------------- #


# --------------------------- flip_tournament_tournaments --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_tournament_tournaments`;
CREATE TABLE `$prefix$flip_tournament_tournaments` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` enum('ko','group','dm','points') COLLATE utf8mb4_bin NOT NULL DEFAULT 'ko',
  `looser` enum('N','Y') COLLATE utf8mb4_bin NOT NULL DEFAULT 'N',
  `liga` enum('Keine','WWCL','NGL') COLLATE utf8mb4_bin NOT NULL DEFAULT 'Keine',
  `u18` enum('Y','N') COLLATE utf8mb4_bin NOT NULL DEFAULT 'Y',
  `screenshots` enum('N','Y') COLLATE utf8mb4_bin NOT NULL DEFAULT 'N',
  `status` enum('closed','open','start','grpgames','games','end') COLLATE utf8mb4_bin NOT NULL DEFAULT 'closed',
  `teamsize` tinyint(4) NOT NULL DEFAULT '1',
  `maximum` smallint(6) NOT NULL DEFAULT '64',
  `roundtime` tinyint(4) NOT NULL DEFAULT '0',
  `coins` tinyint(4) NOT NULL DEFAULT '0',
  `start` int(11) NOT NULL DEFAULT '0',
  `groups` smallint(6) DEFAULT NULL,
  `game` varchar(20) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `treetype` enum('default','small','narrow') COLLATE utf8mb4_bin NOT NULL DEFAULT 'default',
  `group_id` int(11) NOT NULL DEFAULT '1',
  `coin_id` int(11) DEFAULT NULL,
  `description` mediumtext COLLATE utf8mb4_bin,
  `icon` longblob,
  `icon_small` blob,
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_tournament_tournaments -------------------------- #


# --------------------------- flip_user_binary --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_user_binary`;
CREATE TABLE `$prefix$flip_user_binary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `subject_id` int(11) NOT NULL DEFAULT '0',
  `column_id` int(11) NOT NULL DEFAULT '0',
  `mimetype` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `data` MEDIUMBLOB,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_ids` (`subject_id`,`column_id`),
  KEY `i_column` (`column_id`),
  KEY `i_subject` (`subject_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_user_binary -------------------------- #


# --------------------------- flip_user_column --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_user_column`;
CREATE TABLE `$prefix$flip_user_column` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` varchar(12) COLLATE utf8mb4_bin NOT NULL DEFAULT 'user',
  `access` enum('subject','data','binary','callback','method') COLLATE utf8mb4_bin NOT NULL DEFAULT 'data',
  `name` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `min_length` int(11) NOT NULL DEFAULT '0',
  `max_length` int(11) NOT NULL DEFAULT '0',
  `val_type` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `required` enum('Y','N') COLLATE utf8mb4_bin NOT NULL DEFAULT 'N',
  `caption` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `required_view_right` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `required_edit_right` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `hint` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `sort_index` int(11) NOT NULL DEFAULT '5',
  `callback_read` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL,
  `callback_write` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `typename_unique` (`type`,`name`),
  KEY `i_type` (`type`),
  KEY `i_sort` (`sort_index`),
  KEY `i_name` (`name`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=177 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze:
INSERT INTO `$prefix$flip_user_column` VALUES (2,CURRENT_TIMESTAMP,'user','data','enabled',0,0,'YesNo','N','Aktiviert',35,34,'','',3,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (3,CURRENT_TIMESTAMP,'user','subject','name',0,0,'String','Y','Nickname','',1,'','',4,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (4,CURRENT_TIMESTAMP,'user','data','givenname',0,0,'String','Y','Vorname',16,1,'','',5,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (5,CURRENT_TIMESTAMP,'user','subject','email',0,0,'EMail','Y','EMail-Adresse',16,1,'','',9,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (6,CURRENT_TIMESTAMP,'user','data','familyname',0,0,'String','Y','Nachname',16,1,'','',6,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (7,CURRENT_TIMESTAMP,'user','data','password',0,0,'PasswordEdit','N','Passwort',16,1,'','',10,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (8,CURRENT_TIMESTAMP,'user','data','homepage',0,0,'String','N','Homepage',16,1,'','',11,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (9,CURRENT_TIMESTAMP,'user','data','street',0,0,'String','N','Strasse',16,1,'','',15,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (10,CURRENT_TIMESTAMP,'user','data','city',0,0,'String','N','Stadt',16,1,'','',16,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (11,CURRENT_TIMESTAMP,'user','data','plz',5,5,'Integer','N','PLZ',16,1,'','',17,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (12,CURRENT_TIMESTAMP,'user','data','tele',0,0,'Phone','N','Telefon',16,1,'','',18,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (13,CURRENT_TIMESTAMP,'user','data','mobil',0,0,'Phone','N','Handy',16,1,'','',19,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (14,CURRENT_TIMESTAMP,'user','data','description',0,0,'Text','N','Bemerkung','',1,'','',1063927068,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (15,CURRENT_TIMESTAMP,'user','data','registration_time',0,0,'Date','N','Zeitpunkt der Registrierung',35,11,'Zeitpunkt zu dem der User registriert wurde','',20,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (16,CURRENT_TIMESTAMP,'user','data','last_login_time',0,0,'Date','N','Letzter Login',35,11,'Zeitpunkt zu dem sich der User das letzte mal eingeloggt hat','',21,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (17,CURRENT_TIMESTAMP,'user','data','icq',0,0,'Integer','N','ICQ',16,1,'','',25,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (18,CURRENT_TIMESTAMP,'user','data','aim',0,0,'String','N','AIM',16,1,'AOL Instant Messenger','',26,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (19,CURRENT_TIMESTAMP,'user','data','msn',0,0,'String','N','MSN',16,1,'Microsoft Network','',27,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (20,CURRENT_TIMESTAMP,'user','data','yahoo',0,0,'String','N','Yahoo',16,1,'Yahoo Messenger','',28,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (21,CURRENT_TIMESTAMP,'user','subject','id',0,0,'Integer','N','ID',16,-1,'','',2,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (23,CURRENT_TIMESTAMP,'group','subject','name',0,0,'String','N','Name','',11,'','',23,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (24,CURRENT_TIMESTAMP,'user','data','tournament_wwcl_player_id',0,0,'String','N','WWCL-Player-ID',16,1,'Die zum WWCL-Turnier gehörige Spieler-ID','',30,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (25,CURRENT_TIMESTAMP,'user','data','is_adult',0,0,'YesNo','N','Volljährig',35,34,'','',5345345,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (28,CURRENT_TIMESTAMP,'group','data','description',0,0,'Text','N','Beschreibung',NULL,11,NULL,NULL,200,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (29,CURRENT_TIMESTAMP,'user','data','birthday',0,0,'Date','Y','Geburtsdatum',16,1,'','',8,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (30,CURRENT_TIMESTAMP,'user','data','random_id',0,0,'String','N','ZufallsID',11,11,'','',1063571857,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (31,CURRENT_TIMESTAMP,'turnierteam','data','tournament_wwcl_id',0,0,'String','N','WWCL-ID',16,17,NULL,NULL,5345345,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (32,CURRENT_TIMESTAMP,'turnierteam','data','wwcl_email',0,0,'String','N','E-Mailaddr für neues WWCL-Team',16,1,NULL,NULL,32,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (33,CURRENT_TIMESTAMP,'turnierteam','data','fixed_size',0,0,'Integer','N','Feste Teamgröße',NULL,17,NULL,NULL,33,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (34,CURRENT_TIMESTAMP,'user','data','jid',0,0,'EMail','N','Jabber-ID (JID)',16,1,'Die Jabber-ID','<a href="http://www.jabber.org/" target="_blank">www.jabber.org</a>',24,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (35,CURRENT_TIMESTAMP,'turnierteam','subject','name',0,0,'String','Y','Nickname','',1,'','',534534,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (36,CURRENT_TIMESTAMP,'turnierteam','subject','email',0,0,'EMail','Y','EMail-Adresse',16,1,'','',9,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (38,CURRENT_TIMESTAMP,'forum','subject','name',0,0,'String','Y','Name',16,1,'','',1062426182,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (39,CURRENT_TIMESTAMP,'forum','data','sort_index',0,0,'Integer','Y','Sortierungsindex',16,1,'','',1062523361,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (40,CURRENT_TIMESTAMP,'forum','data','thread_count',0,0,'Integer','N','ThreadCount',16,11,'','',1062523434,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (41,CURRENT_TIMESTAMP,'forum','data','post_count',0,0,'Integer','N','PostCount',16,11,'','',1062523518,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (42,CURRENT_TIMESTAMP,'forum','data','last_post_time',0,0,'Date','N','letzte Post-Zeit',16,11,'','',1062523594,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (43,CURRENT_TIMESTAMP,'forum','data','last_post_user',0,0,'Subjects','N','letzter Post-User',16,11,'','',1062527840,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (44,CURRENT_TIMESTAMP,'forum','data','group_id',0,0,'Integer','Y','Gruppe',16,1,'','',1062426160,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (46,CURRENT_TIMESTAMP,'forum','data','description',0,0,'Text','N','Beschreibung',16,1,'','',1062523328,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (47,CURRENT_TIMESTAMP,'forum','subject','id',0,0,'Integer','N','ID',16,11,'','',1062426120,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (48,CURRENT_TIMESTAMP,'turnierteam','data','teamname',0,0,'String','Y','Name des Teams',NULL,NULL,NULL,NULL,5,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (49,CURRENT_TIMESTAMP,'user','data','template_customname',0,0,'Template','N','Template',16,1,'','',1068307043,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (50,CURRENT_TIMESTAMP,'user','data','forum_postsperpage',0,0,'Integer','N','Forum: Posts/Seite',16,1,'','',1096575253,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (51,CURRENT_TIMESTAMP,'user','callback','status',0,0,'String','N','Status',35,-1,'','',1068223320,'mod/mod.user.php|UserGetStatus',NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (52,CURRENT_TIMESTAMP,'user','callback','status_name',0,0,'String','N','StatusName',0,-1,'','',1068224504,'mod/mod.user.php|UserGetStatusName',NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (54,CURRENT_TIMESTAMP,'user','callback','seat',0,0,'String','N','Sitzplatz',0,-1,'','',1068223310,'mod/mod.seats.php|SeatGetSeatByUser',NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (55,CURRENT_TIMESTAMP,'user','callback','ip',0,0,'String','N','IP-Adresse',16,-1,'','',1093297497,'mod/mod.user.php|UserGetIP',NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (56,CURRENT_TIMESTAMP,'user','data','tournament_wwcl_clan_id',0,0,'String','N','WWCL-Clan-ID',16,1,'Die zum WWCL-Turnier gehörige Clan-ID','',150,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (57,CURRENT_TIMESTAMP,'server','subject','name',0,0,'String','Y','Computername',0,43,'','',534534,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (58,CURRENT_TIMESTAMP,'server','data','description',0,0,'Text','N','Beschreibung',0,43,'','',1127761947,'','');
INSERT INTO `$prefix$flip_user_column` VALUES (59,CURRENT_TIMESTAMP,'server','data','hardware',0,0,'String','N','Hardware',0,43,'','',1086776694,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (60,CURRENT_TIMESTAMP,'server','data','services',0,0,'TableMultisel|server_services','N','Dienste',0,43,'','',1127761945,'','');
INSERT INTO `$prefix$flip_user_column` VALUES (61,CURRENT_TIMESTAMP,'server','data','owner_id',0,0,'Subjects|user','N','Besitzer',51,26,'','Achtung: Wer diesen Server bearbeiten darf, wird &uuml;ber das Recht server_edit_own geregelt, nicht &uuml;ber seinen Besitzer.',1127771108,'','');
INSERT INTO `$prefix$flip_user_column` VALUES (62,CURRENT_TIMESTAMP,'server','callback','ip',0,0,'IP','N','IP-Adresse',16,-1,'','',1127761948,'mod/mod.subject.server.php|ServerGetIP',NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (63,CURRENT_TIMESTAMP,'server','data','os',0,0,'TableDropdown|server_oses','N','Betriebssystem',0,43,'','',1127761944,'','');
INSERT INTO `$prefix$flip_user_column` VALUES (64,CURRENT_TIMESTAMP,'server','subject','id',0,0,'Integer','N','ID',16,-1,NULL,NULL,4,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (65,CURRENT_TIMESTAMP,'user','data','hw_barcode',0,0,'String','N','Hardware-Barcode',16,34,'','',1101723285,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (66,CURRENT_TIMESTAMP,'user','data','sendmessage_receive_type',0,0,'MessageType','N','Nachrichtenempfang via',16,1,'','',1101724233,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (67,CURRENT_TIMESTAMP,'user','data','skype_username',0,0,'String','N','Skype-Benutzername',16,1,'','<a href="http://www.skype.com/" target="_blank">www.skype.com</a>',29,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (68,CURRENT_TIMESTAMP,'user','callback','unread_messages',0,0,'Integer','N','Ungelesene Nachrichten',-1,-1,NULL,NULL,1061404876,'mod/mod.webmessage.php|WebMessageUnreadMessagesCount',NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (69,CURRENT_TIMESTAMP,'type','subject','id',0,0,'Integer','Y','ID',5,11,'','',1074521840,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (70,CURRENT_TIMESTAMP,'type','subject','name',0,0,'String','Y','Name',5,11,'','',1074521899,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (71,CURRENT_TIMESTAMP,'type','data','rights_over_for_properties',0,0,'YesNo','N','"RightsOver" für Eigenschaften?',5,11,'','',1074521994,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (72,CURRENT_TIMESTAMP,'type','data','can_be_child',0,0,'YesNo','N','Kann Child sein',5,11,'','',1074522119,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (73,CURRENT_TIMESTAMP,'type','data','can_be_parent',0,0,'YesNo','N','Kann Parent sein',5,11,'','',1074522173,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (74,CURRENT_TIMESTAMP,'user','data','tournament_ngl_player_id',0,0,'String','N','NGL-Player-ID',16,1,'Die zum NGL-Turnier geh&ouml;rige Spieler-ID','',200,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (75,CURRENT_TIMESTAMP,'user','data','tournament_ngl_clan_id',0,0,'String','N','NGL-Team-ID',16,1,'Die zum NGL-Turnier geh&ouml;rige Clan-ID','',353453,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (76,CURRENT_TIMESTAMP,'turnierteam','data','tournament_ngl_id',0,0,'String','N','NGL-ID',16,17,NULL,NULL,31,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (77,CURRENT_TIMESTAMP,'turnierteam','data','ngl_email',0,0,'String','N','E-Mailaddresse für neues NGL-Tea',16,1,NULL,NULL,34,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (78,CURRENT_TIMESTAMP,'server','data','atparty',0,0,'YesNo','N','Mitbringen',0,43,'wird zum n&auml;chsten Event mitgebracht','',1068223330,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (79,CURRENT_TIMESTAMP,'server','data','ip_fix',0,0,'IP','N','Festgelegte IP',51,51,'wird vergeben wenn in der Config der Wert "server_iptype" auf "manuell" gesetzt wird','wird vergeben wenn in der Config der Wert "server_iptype" auf "manuell" gesetzt wird',1127761949,'',NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (80,CURRENT_TIMESTAMP,'type','data','can_be_controled',0,0,'YesNo','N','Kann kontrolliert werden',5,11,'','',1089394599,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (81,CURRENT_TIMESTAMP,'user','data','page_compress_output',0,0,'YesNo','N','Daten komprimieren',16,1,'','bei langsamer Netzwerkanbindung',1101731839,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (82,CURRENT_TIMESTAMP,'user','data','clan',0,0,'String','N','Clan',0,1,'','',13,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (86,CURRENT_TIMESTAMP,'user','data','internal_comment',0,0,'Text','N','interne Bemerkung',35,34,'','',1101731893,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (87,CURRENT_TIMESTAMP,'user','data','sex',0,0,'Sex','Y','Geschlecht',16,1,'','',7,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (88,CURRENT_TIMESTAMP,'user','data','bank_name',0,0,'String','N','Kreditinstitut',16,1,'','',1101731935,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (89,CURRENT_TIMESTAMP,'user','data','bank_blz',0,0,'String','N','Bankleitzahl',16,1,'','',1101731961,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (90,CURRENT_TIMESTAMP,'user','data','bank_knr',0,0,'String','N','Kontonummer',16,1,'','',1101732006,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (91,CURRENT_TIMESTAMP,'user','data','bank_owner',0,0,'String','N','Kontoinhaber',16,1,'','',1103520357,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (92,CURRENT_TIMESTAMP,'user','data','clan_url',0,0,'String','N','Clan Homepage',16,1,'','',14,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (93,CURRENT_TIMESTAMP,'group','data','public_member_columns',0,0,'String','N','Öffentl. Spalten der Mitglieder',3,2,'','Beispiel: "givenname;familyname;job" Siehe: <a href="user.php?frame=viewcolumns&type=user">Columns</a>',1108200281,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (94,CURRENT_TIMESTAMP,'user','data','job',0,0,'String','N','Aufgabenbereich',16,1,'','',534535,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (95,CURRENT_TIMESTAMP,'user','data','ip_fix',0,0,'IP','N','Festgelegte IP',5,26,'wird vergeben wenn in der Config der Wert "user_iptype" auf "fix" gesetzt wird','',1112446673,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (96,CURRENT_TIMESTAMP,'group','data','caption',0,0,'String','N','Caption',3,2,'','Wenn angegeben, ist dies die &Uuml;berschrift der Gruppen&uuml;bersicht',1108200282,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (97,CURRENT_TIMESTAMP,'user','data','statistic_hide_status',0,0,'YesNo','N','In Onlineliste verbergen',16,1,NULL,'',1115380565,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (98,CURRENT_TIMESTAMP,'user','data','forum_signature',0,0,'String','N','Deine Signatur im Forum',16,1,NULL,'',1126951403,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (99,CURRENT_TIMESTAMP,'forum','data','signature_allow',0,0,'YesNo','N','Signaturen erlauben',16,1,NULL,'',1115380566,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (101,CURRENT_TIMESTAMP,'user','callback','age',0,0,'Integer','N','Alter',16,-1,'berechnet aus dem Geburtsdatum','',12,'mod/mod.user.php|UserGetAge',NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (102,CURRENT_TIMESTAMP,'user','data','dbxfer_userdata',0,0,'String','N','DBXFer Userdata',-1,-1,'','Wird intern vom DBXFer verwedet, um Eingabewerte von der letzten Verwendung zu speichern.',1127412285,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (103,CURRENT_TIMESTAMP,'user','callback','seats_is_adult',0,0,'Checkbox','N','Sitzt im ü18-Bereich',35,-1,NULL,'',1127520560,'mod/mod.seats.php|SeatsIsAdult',NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (104,CURRENT_TIMESTAMP,'server','data','games',0,0,'TableMultisel|server_games','N','Spiele',0,43,NULL,'',1127761946,'',NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (105,CURRENT_TIMESTAMP,'server','data','server_gateway',0,0,'IP','N','Gateway-IP',43,51,NULL,'',1127761952,'',NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (106,CURRENT_TIMESTAMP,'server','data','server_subnetmask',0,0,'IP','N','Subnetmask',43,51,NULL,'',1127761953,'',NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (107,CURRENT_TIMESTAMP,'server','data','server_dns_request',0,0,'Domain','N','Wunsch-DNS-Name',43,43,NULL,'Wenn der Admin ihn annimmt trägt er ihn als DNS-Namen ein.',1127761950,'',NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (108,CURRENT_TIMESTAMP,'server','data','server_dns',0,0,'Domain','N','DNS-Name',0,51,NULL,'Deine Domain',1127761951,'',NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (109,CURRENT_TIMESTAMP,'server','data','server_dns_ip',0,0,'IP','N','DNS-Server-IP',43,51,NULL,'',1127761954,'',NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (110,CURRENT_TIMESTAMP,'server','data','server_wins_ip',0,0,'IP','N','WINS-Server-IP',43,51,NULL,'',1127761955,'',NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (111,CURRENT_TIMESTAMP,'user','binary','user_image',0,0,'image','N','Userbild',0,1,'Userbild','Userbild',1157829426,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (112,CURRENT_TIMESTAMP,'clan','subject','id',0,0,'Integer','N','ID',16,-1,NULL,NULL,1172143593,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (113,CURRENT_TIMESTAMP,'clan','subject','name',0,0,'String','Y','Name',0,90,NULL,NULL,1172143625,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (114,CURRENT_TIMESTAMP,'clan','data','homepage',0,0,'String','N','Website',0,90,NULL,NULL,1172155284,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (115,CURRENT_TIMESTAMP,'clan','binary','image',0,0,'Image','N','Bild',0,90,NULL,NULL,1173099897,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (116,CURRENT_TIMESTAMP,'clan','data','description',0,0,'Text','N','Beschreibung',0,90,NULL,NULL,1173101701,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (117,CURRENT_TIMESTAMP,'user','data','webmessage_signature',0,0,'Text','N','Deine Signatur für Webmessages',16,1,NULL,NULL,1182630966,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (118,CURRENT_TIMESTAMP,'systemuser','data','enabled',0,0,'YesNo','N','Aktiviert',35,34,'','',3,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (119,CURRENT_TIMESTAMP,'systemuser','subject','name',0,0,'String','Y','Nickname','',1,'','',4,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (120,CURRENT_TIMESTAMP,'systemuser','data','givenname',0,0,'String','Y','Vorname',16,1,'','',5,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (121,CURRENT_TIMESTAMP,'systemuser','subject','email',0,0,'EMail','Y','EMail-Adresse',16,1,'','',9,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (122,CURRENT_TIMESTAMP,'systemuser','data','familyname',0,0,'String','Y','Nachname',16,1,'','',6,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (123,CURRENT_TIMESTAMP,'systemuser','data','password',0,0,'PasswordEdit','N','Passwort',16,1,'','',10,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (124,CURRENT_TIMESTAMP,'systemuser','data','homepage',0,0,'String','N','Homepage',16,1,'','',11,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (125,CURRENT_TIMESTAMP,'systemuser','data','street',0,0,'String','N','Strasse',16,1,'','',15,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (126,CURRENT_TIMESTAMP,'systemuser','data','city',0,0,'String','N','Stadt',16,1,'','',16,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (127,CURRENT_TIMESTAMP,'systemuser','data','plz',5,5,'Integer','N','PLZ',16,1,'','',17,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (128,CURRENT_TIMESTAMP,'systemuser','data','tele',0,0,'Phone','N','Telefon',16,1,'','',18,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (129,CURRENT_TIMESTAMP,'systemuser','data','mobil',0,0,'Phone','N','Handy',16,1,'','',19,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (130,CURRENT_TIMESTAMP,'systemuser','data','description',0,0,'Text','N','Bemerkung','',1,'','',1063927068,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (131,CURRENT_TIMESTAMP,'systemuser','data','registration_time',0,0,'Date','N','Zeitpunkt der Registrierung',35,11,'Zeitpunkt zu dem der User registriert wurde','',20,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (132,CURRENT_TIMESTAMP,'systemuser','data','last_login_time',0,0,'Date','N','Letzter Login',35,11,'Zeitpunkt zu dem sich der User das letzte mal eingeloggt hat','',21,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (133,CURRENT_TIMESTAMP,'systemuser','data','icq',0,0,'Integer','N','ICQ',16,1,'','',25,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (134,CURRENT_TIMESTAMP,'systemuser','data','aim',0,0,'String','N','AIM',16,1,'AOL Instant Messenger','',26,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (135,CURRENT_TIMESTAMP,'systemuser','data','msn',0,0,'String','N','MSN',16,1,'Microsoft Network','',27,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (136,CURRENT_TIMESTAMP,'systemuser','data','yahoo',0,0,'String','N','Yahoo',16,1,'Yahoo Messenger','',28,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (137,CURRENT_TIMESTAMP,'systemuser','subject','id',0,0,'Integer','N','ID',16,-1,'','',2,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (138,CURRENT_TIMESTAMP,'systemuser','data','tournament_wwcl_player_id',0,0,'String','N','WWCL-Player-ID',16,1,'Die zum WWCL-Turnier gehörige Spieler-ID','',30,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (139,CURRENT_TIMESTAMP,'systemuser','data','is_adult',0,0,'YesNo','N','Volljährig',35,34,'','',5345345,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (140,CURRENT_TIMESTAMP,'systemuser','data','birthday',0,0,'Date','Y','Geburtsdatum',16,1,'','',8,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (141,CURRENT_TIMESTAMP,'systemuser','data','random_id',0,0,'String','N','ZufallsID',11,11,'','',1063571857,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (142,CURRENT_TIMESTAMP,'systemuser','data','jid',0,0,'EMail','N','Jabber-ID (JID)',16,1,'Die Jabber-ID','<a href="http://www.jabber.org/" target="_blank">www.jabber.org</a>',24,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (143,CURRENT_TIMESTAMP,'systemuser','data','template_customname',0,0,'Template','N','Template',16,1,'','',1068307043,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (144,CURRENT_TIMESTAMP,'systemuser','data','forum_postsperpage',0,0,'Integer','N','Forum: Posts/Seite',16,1,'','',1096575253,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (145,CURRENT_TIMESTAMP,'systemuser','callback','status',0,0,'String','N','Status',35,-1,'','',1068223320,'mod/mod.user.php|UserGetStatus',NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (146,CURRENT_TIMESTAMP,'systemuser','callback','status_name',0,0,'String','N','StatusName',0,-1,'','',1068224504,'mod/mod.user.php|UserGetStatusName',NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (147,CURRENT_TIMESTAMP,'systemuser','callback','seat',0,0,'String','N','Sitzplatz',0,-1,'','',1068223310,'mod/mod.seats.php|SeatGetSeatByUser',NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (148,CURRENT_TIMESTAMP,'systemuser','callback','ip',0,0,'String','N','IP-Adresse',16,-1,'','',1093297497,'mod/mod.user.php|UserGetIP',NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (149,CURRENT_TIMESTAMP,'systemuser','data','tournament_wwcl_clan_id',0,0,'String','N','WWCL-Clan-ID',16,1,'Die zum WWCL-Turnier gehörige Clan-ID','',150,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (150,CURRENT_TIMESTAMP,'systemuser','data','hw_barcode',0,0,'String','N','Hardware-Barcode',16,34,'','',1101723285,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (151,CURRENT_TIMESTAMP,'systemuser','data','sendmessage_receive_type',0,0,'MessageType','N','Nachrichtenempfang via',16,1,'','',1101724233,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (152,CURRENT_TIMESTAMP,'systemuser','data','skype_username',0,0,'String','N','Skype-Benutzername',16,1,'','<a href="http://www.skype.com/" target="_blank">www.skype.com</a>',29,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (153,CURRENT_TIMESTAMP,'systemuser','callback','unread_messages',0,0,'Integer','N','Ungelesene Nachrichten',-1,-1,NULL,NULL,1061404876,'mod/mod.webmessage.php|WebMessageUnreadMessagesCount',NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (154,CURRENT_TIMESTAMP,'systemuser','data','tournament_ngl_player_id',0,0,'String','N','NGL-Player-ID',16,1,'Die zum NGL-Turnier geh&ouml;rige Spieler-ID','',200,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (155,CURRENT_TIMESTAMP,'systemuser','data','tournament_ngl_clan_id',0,0,'String','N','NGL-Team-ID',16,1,'Die zum NGL-Turnier geh&ouml;rige Clan-ID','',353453,NULL,'');
INSERT INTO `$prefix$flip_user_column` VALUES (156,CURRENT_TIMESTAMP,'systemuser','data','page_compress_output',0,0,'YesNo','N','Daten komprimieren',16,1,'','bei langsamer Netzwerkanbindung',1101731839,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (157,CURRENT_TIMESTAMP,'systemuser','data','clan',0,0,'String','N','Clan',0,1,'','',13,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (158,CURRENT_TIMESTAMP,'systemuser','data','internal_comment',0,0,'Text','N','interne Bemerkung',35,34,'','',1101731893,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (159,CURRENT_TIMESTAMP,'systemuser','data','sex',0,0,'Sex','Y','Geschlecht',16,1,'','',7,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (160,CURRENT_TIMESTAMP,'systemuser','data','bank_name',0,0,'String','N','Kreditinstitut',16,1,'','',1101731935,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (161,CURRENT_TIMESTAMP,'systemuser','data','bank_blz',0,0,'String','N','Bankleitzahl',16,1,'','',1101731961,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (162,CURRENT_TIMESTAMP,'systemuser','data','bank_knr',0,0,'String','N','Kontonummer',16,1,'','',1101732006,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (163,CURRENT_TIMESTAMP,'systemuser','data','bank_owner',0,0,'String','N','Kontoinhaber',16,1,'','',1103520357,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (164,CURRENT_TIMESTAMP,'systemuser','data','clan_url',0,0,'String','N','Clan Homepage',16,1,'','',14,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (165,CURRENT_TIMESTAMP,'systemuser','data','job',0,0,'String','N','Aufgabenbereich',16,1,'','',534535,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (166,CURRENT_TIMESTAMP,'systemuser','data','ip_fix',0,0,'IP','N','Festgelegte IP',5,26,'wird vergeben wenn in der Config der Wert "user_iptype" auf "fix" gesetzt wird','',1112446673,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (167,CURRENT_TIMESTAMP,'systemuser','data','statistic_hide_status',0,0,'YesNo','N','In Onlineliste verbergen',16,1,NULL,'',1115380565,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (168,CURRENT_TIMESTAMP,'systemuser','data','forum_signature',0,0,'String','N','Deine Signatur im Forum',16,1,NULL,'',1126951403,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (169,CURRENT_TIMESTAMP,'systemuser','callback','age',0,0,'Integer','N','Alter',16,-1,'berechnet aus dem Geburtsdatum','',12,'mod/mod.user.php|UserGetAge',NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (170,CURRENT_TIMESTAMP,'systemuser','data','dbxfer_userdata',0,0,'String','N','DBXFer Userdata',-1,-1,NULL,'Wird intern vom DBXFer verwedet, um Eingabewerte von der letzten Verwendung zu speichern.',1127412285,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (171,CURRENT_TIMESTAMP,'systemuser','callback','seats_is_adult',0,0,'Checkbox','N','Sitzt im ü18-Bereich',35,-1,NULL,'',1127520560,'mod/mod.seats.php|SeatsIsAdult',NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (172,CURRENT_TIMESTAMP,'systemuser','binary','user_image',500,1000000,'image','N','Userbild',0,1,'Userbild','max. 500px',1157829426,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (173,CURRENT_TIMESTAMP,'systemuser','data','webmessage_signature',0,0,'Text','N','Deine Signatur für Webmessages',16,1,NULL,NULL,1182630966,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (174,CURRENT_TIMESTAMP,'user','data','translate_language',0,0,'Dropdown','N','Systemsprache',16,1,'','',1284583262,'',NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (175,CURRENT_TIMESTAMP,'systemuser','data','translate_language',0,0,'Dropdown','N','Systemsprache',16,1,'','',1284583263,'',NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (176,CURRENT_TIMESTAMP,'user','data','country',0,0,'Country','N','Land',16,1,'Das Land, aus dem der User kommt','',1284675578,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (179,CURRENT_TIMESTAMP,'clan','data','clan_seat_limit', 0, 0, 'Decimal','N','Clan-Sitzplaetze','90','105','Maximale Anzahl der Clan Sitzplaetze ','', 1523305195, NULL, NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (178,CURRENT_TIMESTAMP,'clan','binary','clanseatimage', 32, 100000, 'Image','N','Clan-Icon','0','90','Bild fuer den Sitzplan','jpg, 32x32px', 1173101701, NULL, NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (180,CURRENT_TIMESTAMP,'user','data','dsgvo', 0, 0, 'YesNo','N','Zustimmung der Datenschutz-Grund','16','34','Zustimmung der Datenschutz-Grundverordnung','', 1526871297, NULL, NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (181,CURRENT_TIMESTAMP,'user','data','dsgvo_status', 0, 0, 'String','N','Status der Datenschutz-Grundvero','16','-1','Status der Datenschutz-Grundverordnung','ausdrücklich einverstanden oder widerrufen', 1526871328, NULL, NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (182,CURRENT_TIMESTAMP,'clan','data','clan_no_seat_count',0,0,'YesNo','Y','CLAN No Seat Count',90,105,'Für Orga Clans - zählt Sitzplaetze nicht zu den Teilnehmern hinzu','',1559597734,NULL,NULL);
INSERT INTO `$prefix$flip_user_column` VALUES (183,CURRENT_TIMESTAMP,'user','subject','ingame_nick_name',0,0,'String','N','ingame nickname','',1,'Dein Ingame verwendeter Nick Name.','',4,NULL,'');

# --------------------------- /flip_user_column -------------------------- #


# --------------------------- flip_user_data --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_user_data`;
CREATE TABLE `$prefix$flip_user_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `subject_id` int(11) NOT NULL DEFAULT '0',
  `column_id` int(11) NOT NULL DEFAULT '0',
  `val` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_ids` (`subject_id`,`column_id`),
  KEY `i_subject` (`subject_id`),
  KEY `i_column` (`column_id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=36488 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Die wichtigsten Datensaetze:
INSERT INTO `$prefix$flip_user_data` VALUES (5010,CURRENT_TIMESTAMP,1,2,'Y');
INSERT INTO `$prefix$flip_user_data` VALUES (5011,CURRENT_TIMESTAMP,1,14,'Der Gast-Account für alle nicht registrierten User.');
INSERT INTO `$prefix$flip_user_data` VALUES (5012,CURRENT_TIMESTAMP,1,30,'d511e4322c46bf9e3bca20cb4d168d81');
INSERT INTO `$prefix$flip_user_data` VALUES (5013,CURRENT_TIMESTAMP,2,28,'Alle bei xxx registrierten User, mit ausnahme von "Anonymous"');
INSERT INTO `$prefix$flip_user_data` VALUES (6921,CURRENT_TIMESTAMP,262,28,'Helfer sind nicht so mächtig wie orgas, aber immerhin.');
INSERT INTO `$prefix$flip_user_data` VALUES (10326,CURRENT_TIMESTAMP,881,28,'verwaltet Rechte, User und Gruppen.');
INSERT INTO `$prefix$flip_user_data` VALUES (10328,CURRENT_TIMESTAMP,81,28,'diverse Typen die sich für Gott halten ;-)');
INSERT INTO `$prefix$flip_user_data` VALUES (10330,CURRENT_TIMESTAMP,882,28,'Bearbeitet öffentliche Dokumente und Bilder. Er kann auch zugriffsrechte setzen.');
INSERT INTO `$prefix$flip_user_data` VALUES (10860,CURRENT_TIMESTAMP,13,28,'Alle die sich für die LanParty angemeldet, aber noch nicht bezahlt haben.');
INSERT INTO `$prefix$flip_user_data` VALUES (10861,CURRENT_TIMESTAMP,14,28,'Alle die sich für die LanParty angemeldet und bezahlt haben.');
INSERT INTO `$prefix$flip_user_data` VALUES (10862,CURRENT_TIMESTAMP,710,28,'Alle die sich nach der LanParty wieder ausgecheckt haben.');
INSERT INTO `$prefix$flip_user_data` VALUES (10863,CURRENT_TIMESTAMP,709,28,'Alle, die sich auf der Lanparty am Eingang eingecheckt haben.');
INSERT INTO `$prefix$flip_user_data` VALUES (10865,CURRENT_TIMESTAMP,883,28,'Alle User die sich auf der Lanparty befinden, und deren IPs per ping oder arp erreichbar sind');
INSERT INTO `$prefix$flip_user_data` VALUES (12125,CURRENT_TIMESTAMP,917,28,'In dieser Gruppe befinden sich alle öffentlichen Foren');
INSERT INTO `$prefix$flip_user_data` VALUES (12126,CURRENT_TIMESTAMP,918,28,'In dieser Gruppe befinden sich alle internen Foren');
INSERT INTO `$prefix$flip_user_data` VALUES (12249,CURRENT_TIMESTAMP,919,44,1);
INSERT INTO `$prefix$flip_user_data` VALUES (12250,CURRENT_TIMESTAMP,919,46,'Allgemeine Diskussionen über das Projekt "xxx"');
INSERT INTO `$prefix$flip_user_data` VALUES (12264,CURRENT_TIMESTAMP,925,44,1);
INSERT INTO `$prefix$flip_user_data` VALUES (12265,CURRENT_TIMESTAMP,925,46,'Turniere, Regeln, Turniersystem, Siegerehrung, Preise');
INSERT INTO `$prefix$flip_user_data` VALUES (12273,CURRENT_TIMESTAMP,919,39,253523);
INSERT INTO `$prefix$flip_user_data` VALUES (12309,CURRENT_TIMESTAMP,925,39,654326526);
INSERT INTO `$prefix$flip_user_data` VALUES (12325,CURRENT_TIMESTAMP,929,44,3);
INSERT INTO `$prefix$flip_user_data` VALUES (12326,CURRENT_TIMESTAMP,929,46,'Wenn mal jemand zuviel Zeit hat...');
INSERT INTO `$prefix$flip_user_data` VALUES (12327,CURRENT_TIMESTAMP,929,39,44444465);
INSERT INTO `$prefix$flip_user_data` VALUES (15789,CURRENT_TIMESTAMP,967,44,2);
INSERT INTO `$prefix$flip_user_data` VALUES (15790,CURRENT_TIMESTAMP,967,46,'Server, Netzwerk, WLAN, Strom, etc.');
INSERT INTO `$prefix$flip_user_data` VALUES (15791,CURRENT_TIMESTAMP,967,39,653353);
INSERT INTO `$prefix$flip_user_data` VALUES (15798,CURRENT_TIMESTAMP,970,44,4);
INSERT INTO `$prefix$flip_user_data` VALUES (15799,CURRENT_TIMESTAMP,970,46,'Es gibt auch noch was anderes als das hier!? *fg*');
INSERT INTO `$prefix$flip_user_data` VALUES (35161,CURRENT_TIMESTAMP,1097,28,'alle Gastserver');
INSERT INTO `$prefix$flip_user_data` VALUES (35264,CURRENT_TIMESTAMP,1296,7,'$2y$12$fuiuVIi.4mBMWWNRq3HcduVFJ2.Y4bEC4aJgloZpCQK6Sckkyyj0i');
INSERT INTO `$prefix$flip_user_data` VALUES (35323,CURRENT_TIMESTAMP,1299,28,'u.a. Serververwaltung');
INSERT INTO `$prefix$flip_user_data` VALUES (35342,CURRENT_TIMESTAMP,1300,28,'Turnieradministratoren');
INSERT INTO `$prefix$flip_user_data` VALUES (35352,CURRENT_TIMESTAMP,1294,71,'N');
INSERT INTO `$prefix$flip_user_data` VALUES (35353,CURRENT_TIMESTAMP,1294,72,'Y');
INSERT INTO `$prefix$flip_user_data` VALUES (35354,CURRENT_TIMESTAMP,1294,73,'N');
INSERT INTO `$prefix$flip_user_data` VALUES (35355,CURRENT_TIMESTAMP,1294,80,'Y');
INSERT INTO `$prefix$flip_user_data` VALUES (35356,CURRENT_TIMESTAMP,1292,71,'N');
INSERT INTO `$prefix$flip_user_data` VALUES (35357,CURRENT_TIMESTAMP,1292,72,'Y');
INSERT INTO `$prefix$flip_user_data` VALUES (35358,CURRENT_TIMESTAMP,1292,73,'Y');
INSERT INTO `$prefix$flip_user_data` VALUES (35359,CURRENT_TIMESTAMP,1292,80,'Y');
INSERT INTO `$prefix$flip_user_data` VALUES (35360,CURRENT_TIMESTAMP,1293,71,'Y');
INSERT INTO `$prefix$flip_user_data` VALUES (35361,CURRENT_TIMESTAMP,1293,72,'Y');
INSERT INTO `$prefix$flip_user_data` VALUES (35362,CURRENT_TIMESTAMP,1293,73,'N');
INSERT INTO `$prefix$flip_user_data` VALUES (35363,CURRENT_TIMESTAMP,1293,80,'Y');
INSERT INTO `$prefix$flip_user_data` VALUES (35364,CURRENT_TIMESTAMP,1297,71,'N');
INSERT INTO `$prefix$flip_user_data` VALUES (35365,CURRENT_TIMESTAMP,1297,72,'N');
INSERT INTO `$prefix$flip_user_data` VALUES (35366,CURRENT_TIMESTAMP,1297,73,'Y');
INSERT INTO `$prefix$flip_user_data` VALUES (35367,CURRENT_TIMESTAMP,1297,80,'N');
INSERT INTO `$prefix$flip_user_data` VALUES (35368,CURRENT_TIMESTAMP,1295,71,'N');
INSERT INTO `$prefix$flip_user_data` VALUES (35369,CURRENT_TIMESTAMP,1295,72,'N');
INSERT INTO `$prefix$flip_user_data` VALUES (35370,CURRENT_TIMESTAMP,1295,73,'Y');
INSERT INTO `$prefix$flip_user_data` VALUES (35371,CURRENT_TIMESTAMP,1295,80,'N');
INSERT INTO `$prefix$flip_user_data` VALUES (35372,CURRENT_TIMESTAMP,1290,71,'N');
INSERT INTO `$prefix$flip_user_data` VALUES (35373,CURRENT_TIMESTAMP,1290,72,'N');
INSERT INTO `$prefix$flip_user_data` VALUES (35374,CURRENT_TIMESTAMP,1290,73,'N');
INSERT INTO `$prefix$flip_user_data` VALUES (35375,CURRENT_TIMESTAMP,1290,80,'Y');
INSERT INTO `$prefix$flip_user_data` VALUES (35376,CURRENT_TIMESTAMP,1291,71,'Y');
INSERT INTO `$prefix$flip_user_data` VALUES (35377,CURRENT_TIMESTAMP,1291,72,'Y');
INSERT INTO `$prefix$flip_user_data` VALUES (35378,CURRENT_TIMESTAMP,1291,73,'N');
INSERT INTO `$prefix$flip_user_data` VALUES (35379,CURRENT_TIMESTAMP,1291,80,'Y');
INSERT INTO `$prefix$flip_user_data` VALUES (35496,CURRENT_TIMESTAMP,1301,28,'In dieser Gruppe befinden sich alle, die vom Netlog auf offline gesetzt werden.');
INSERT INTO `$prefix$flip_user_data` VALUES (35637,CURRENT_TIMESTAMP,1303,14,'Benutzer, die keine Personen sind. Beispielsweise ein Script, der den DBExport des FLIPs herunterlädt.');
INSERT INTO `$prefix$flip_user_data` VALUES (35697,CURRENT_TIMESTAMP,1304,71,'Y');
INSERT INTO `$prefix$flip_user_data` VALUES (35698,CURRENT_TIMESTAMP,1304,72,'Y');
INSERT INTO `$prefix$flip_user_data` VALUES (35699,CURRENT_TIMESTAMP,1304,73,'Y');
INSERT INTO `$prefix$flip_user_data` VALUES (35700,CURRENT_TIMESTAMP,1304,80,'N');
INSERT INTO `$prefix$flip_user_data` VALUES (35701,CURRENT_TIMESTAMP,1305,96,'Clanliste');
INSERT INTO `$prefix$flip_user_data` VALUES (35702,CURRENT_TIMESTAMP,1305,28,'Zusammenfassung aller Clans zu administrativen Zwecken.');
INSERT INTO `$prefix$flip_user_data` VALUES (35760,CURRENT_TIMESTAMP,970,39,1280865023);
INSERT INTO `$prefix$flip_user_data` VALUES (35878,CURRENT_TIMESTAMP,919,40,0);
INSERT INTO `$prefix$flip_user_data` VALUES (35879,CURRENT_TIMESTAMP,919,41,0);
INSERT INTO `$prefix$flip_user_data` VALUES (35880,CURRENT_TIMESTAMP,919,43,0);
INSERT INTO `$prefix$flip_user_data` VALUES (35881,CURRENT_TIMESTAMP,970,40,0);
INSERT INTO `$prefix$flip_user_data` VALUES (35882,CURRENT_TIMESTAMP,970,41,0);
INSERT INTO `$prefix$flip_user_data` VALUES (35883,CURRENT_TIMESTAMP,970,43,0);
INSERT INTO `$prefix$flip_user_data` VALUES (35884,CURRENT_TIMESTAMP,929,40,0);
INSERT INTO `$prefix$flip_user_data` VALUES (35885,CURRENT_TIMESTAMP,929,41,0);
INSERT INTO `$prefix$flip_user_data` VALUES (35886,CURRENT_TIMESTAMP,929,43,0);
INSERT INTO `$prefix$flip_user_data` VALUES (35887,CURRENT_TIMESTAMP,967,40,0);
INSERT INTO `$prefix$flip_user_data` VALUES (35888,CURRENT_TIMESTAMP,967,41,0);
INSERT INTO `$prefix$flip_user_data` VALUES (35889,CURRENT_TIMESTAMP,967,43,0);
INSERT INTO `$prefix$flip_user_data` VALUES (35890,CURRENT_TIMESTAMP,925,40,0);
INSERT INTO `$prefix$flip_user_data` VALUES (35891,CURRENT_TIMESTAMP,925,41,0);
INSERT INTO `$prefix$flip_user_data` VALUES (35892,CURRENT_TIMESTAMP,925,43,0);
INSERT INTO `$prefix$flip_user_data` VALUES (35982,CURRENT_TIMESTAMP,10,28,'Alle Organisatoren der aktuellen Veranstaltung');
INSERT INTO `$prefix$flip_user_data` VALUES (35983,CURRENT_TIMESTAMP,10,93,'givenname;familyname;job');
INSERT INTO `$prefix$flip_user_data` VALUES (35984,CURRENT_TIMESTAMP,10,96,'Organisatoren');
INSERT INTO `$prefix$flip_user_data` VALUES (35987,CURRENT_TIMESTAMP,1400,71,'Y');
INSERT INTO `$prefix$flip_user_data` VALUES (35988,CURRENT_TIMESTAMP,1400,72,'Y');
INSERT INTO `$prefix$flip_user_data` VALUES (35989,CURRENT_TIMESTAMP,1400,73,'N');
INSERT INTO `$prefix$flip_user_data` VALUES (35990,CURRENT_TIMESTAMP,1400,80,'Y');
INSERT INTO `$prefix$flip_user_data` VALUES (36254,CURRENT_TIMESTAMP,4,122,'System');
INSERT INTO `$prefix$flip_user_data` VALUES (36255,CURRENT_TIMESTAMP,4,159,'m');
INSERT INTO `$prefix$flip_user_data` VALUES (36256,CURRENT_TIMESTAMP,4,140,968104800);
INSERT INTO `$prefix$flip_user_data` VALUES (36257,CURRENT_TIMESTAMP,4,167,'N');
INSERT INTO `$prefix$flip_user_data` VALUES (36383,CURRENT_TIMESTAMP,1442,28,'Ein User, der die Übersetzung des Systems vornimmt gehört in diese Gruppe');
INSERT INTO `$prefix$flip_user_data` VALUES (36477,CURRENT_TIMESTAMP,1296,2,'Y');
INSERT INTO `$prefix$flip_user_data` VALUES (36478,CURRENT_TIMESTAMP,1296,4,'Max');
INSERT INTO `$prefix$flip_user_data` VALUES (36479,CURRENT_TIMESTAMP,1296,6,'Mustermann');
INSERT INTO `$prefix$flip_user_data` VALUES (36480,CURRENT_TIMESTAMP,1296,87,'m');
INSERT INTO `$prefix$flip_user_data` VALUES (36481,CURRENT_TIMESTAMP,1296,29,420415200);
INSERT INTO `$prefix$flip_user_data` VALUES (36482,CURRENT_TIMESTAMP,1296,15,0);
INSERT INTO `$prefix$flip_user_data` VALUES (36483,CURRENT_TIMESTAMP,1296,16,1284675480);
INSERT INTO `$prefix$flip_user_data` VALUES (36484,CURRENT_TIMESTAMP,1296,94,'Den Überblick behalten');
INSERT INTO `$prefix$flip_user_data` VALUES (36485,CURRENT_TIMESTAMP,1296,25,'Y');
INSERT INTO `$prefix$flip_user_data` VALUES (36486,CURRENT_TIMESTAMP,1296,97,'N');
INSERT INTO `$prefix$flip_user_data` VALUES (36487,CURRENT_TIMESTAMP,1296,176,'BRAZIL');
# --------------------------- /flip_user_data -------------------------- #


# --------------------------- flip_user_groups --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_user_groups`;
CREATE TABLE `$prefix$flip_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `child_id` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_unique` (`child_id`,`parent_id`),
  KEY `i_parent` (`parent_id`),
  KEY `i_child` (`child_id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=3725 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Die wichtigsten Datensaetze:
INSERT INTO `$prefix$flip_user_groups` VALUES (2190,CURRENT_TIMESTAMP,919,918);
INSERT INTO `$prefix$flip_user_groups` VALUES (2195,CURRENT_TIMESTAMP,925,918);
INSERT INTO `$prefix$flip_user_groups` VALUES (2199,CURRENT_TIMESTAMP,929,918);
INSERT INTO `$prefix$flip_user_groups` VALUES (2333,CURRENT_TIMESTAMP,970,917);
INSERT INTO `$prefix$flip_user_groups` VALUES (2337,CURRENT_TIMESTAMP,967,917);
INSERT INTO `$prefix$flip_user_groups` VALUES (3620,CURRENT_TIMESTAMP,1296,2);
INSERT INTO `$prefix$flip_user_groups` VALUES (3621,CURRENT_TIMESTAMP,1296,10);
INSERT INTO `$prefix$flip_user_groups` VALUES (3622,CURRENT_TIMESTAMP,1296,882);
INSERT INTO `$prefix$flip_user_groups` VALUES (3623,CURRENT_TIMESTAMP,1296,1013);
INSERT INTO `$prefix$flip_user_groups` VALUES (3624,CURRENT_TIMESTAMP,1296,881);
INSERT INTO `$prefix$flip_user_groups` VALUES (3625,CURRENT_TIMESTAMP,1296,81);
INSERT INTO `$prefix$flip_user_groups` VALUES (3626,CURRENT_TIMESTAMP,1296,1298);
INSERT INTO `$prefix$flip_user_groups` VALUES (3627,CURRENT_TIMESTAMP,1296,1299);
INSERT INTO `$prefix$flip_user_groups` VALUES (3628,CURRENT_TIMESTAMP,1296,1300);
INSERT INTO `$prefix$flip_user_groups` VALUES (3724,CURRENT_TIMESTAMP,1296,13);
# --------------------------- /flip_user_groups -------------------------- #


# --------------------------- flip_user_right --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_user_right`;
CREATE TABLE `$prefix$flip_user_right` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `right` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8mb4_bin,
  PRIMARY KEY (`id`),
  UNIQUE KEY `right` (`right`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin PACK_KEYS=0;
# Alle Datensaetze:
INSERT INTO `$prefix$flip_user_right` VALUES (-1,CURRENT_TIMESTAMP,'_infinite_','Wird von niemandem besessen.');
INSERT INTO `$prefix$flip_user_right` VALUES (1,CURRENT_TIMESTAMP,'edit_own_profile','Erlaubt es, das eigene Profil (Name, Adresse...) zu bearbeiten.');
INSERT INTO `$prefix$flip_user_right` VALUES (2,CURRENT_TIMESTAMP,'edit_public_text','Erlaubt es, Texte die der Öffentlichkeit zugänglich sind zu bearbeiten.');
INSERT INTO `$prefix$flip_user_right` VALUES (3,CURRENT_TIMESTAMP,'view_internal_informations','Erlaubt es, interne Dokumente einzusehen');
INSERT INTO `$prefix$flip_user_right` VALUES (4,CURRENT_TIMESTAMP,'logged_in','Ein allgemeines Recht welches jeder besitzt der sich eingeloggt hat.');
INSERT INTO `$prefix$flip_user_right` VALUES (5,CURRENT_TIMESTAMP,'admin','Ein allgemeines Administrationsrecht');
INSERT INTO `$prefix$flip_user_right` VALUES (6,CURRENT_TIMESTAMP,'menu_edit','Erlaubt es, das Menü zu bearbeiten.');
INSERT INTO `$prefix$flip_user_right` VALUES (9,CURRENT_TIMESTAMP,'edit_page_rights','Erlaubt es, festzulegen, wofür welche Rechte benötigt werden');
INSERT INTO `$prefix$flip_user_right` VALUES (10,CURRENT_TIMESTAMP,'view_debug_messages','Erlaubt es, Debugmessages und technische Deteils zu Fehlermeldungen zu sehen.');
INSERT INTO `$prefix$flip_user_right` VALUES (11,CURRENT_TIMESTAMP,'edit_db','Erlaube es die Datenbank "low-level" zu bearbeiten (zB mit phpMyAdmin)');
INSERT INTO `$prefix$flip_user_right` VALUES (12,CURRENT_TIMESTAMP,'sponsor_edit','Erlaubt es, Die Liste der Sponsoren zu bearbeiten');
INSERT INTO `$prefix$flip_user_right` VALUES (13,CURRENT_TIMESTAMP,'status_registered','Erhält jeder, der sich für die LAN-Party registriert hat.');
INSERT INTO `$prefix$flip_user_right` VALUES (15,CURRENT_TIMESTAMP,'edit_internal_text','Erlaubt es interne texte zu bearbeiten.');
INSERT INTO `$prefix$flip_user_right` VALUES (16,CURRENT_TIMESTAMP,'view_personal_informations','Erlaubt es, persönliche Informationen einzusehen.');
INSERT INTO `$prefix$flip_user_right` VALUES (17,CURRENT_TIMESTAMP,'tournament_admin','Turnieradministrator');
INSERT INTO `$prefix$flip_user_right` VALUES (19,CURRENT_TIMESTAMP,'status_paid','Erhält jeder, der Überwiesen hat.');
INSERT INTO `$prefix$flip_user_right` VALUES (22,CURRENT_TIMESTAMP,'status_checked_in','Jeder der sich auf der Lanparty befindet und sich am Eingang eingecheckt hat');
INSERT INTO `$prefix$flip_user_right` VALUES (23,CURRENT_TIMESTAMP,'ticket_view','Erlaubt es, die Tickets ein zu sehen.');
INSERT INTO `$prefix$flip_user_right` VALUES (24,CURRENT_TIMESTAMP,'user_admin_rights','Erlaubt es, Rechte zu erstellen und zu löschen, sowie auf alle Rechte zuzugreifen.');
INSERT INTO `$prefix$flip_user_right` VALUES (25,CURRENT_TIMESTAMP,'content_edit','Erlaubt es, auf die die Content-Verwaltung zuzugreifen');
INSERT INTO `$prefix$flip_user_right` VALUES (26,CURRENT_TIMESTAMP,'user_admin_subjects','');
INSERT INTO `$prefix$flip_user_right` VALUES (27,CURRENT_TIMESTAMP,'user_admin_columns','');
INSERT INTO `$prefix$flip_user_right` VALUES (28,CURRENT_TIMESTAMP,'user_admin_heredity','');
INSERT INTO `$prefix$flip_user_right` VALUES (29,CURRENT_TIMESTAMP,'dbxfer_dbtransfer','');
INSERT INTO `$prefix$flip_user_right` VALUES (30,CURRENT_TIMESTAMP,'logged_out','Das Recht von Anonymous');
INSERT INTO `$prefix$flip_user_right` VALUES (31,CURRENT_TIMESTAMP,'seats_admin','Sitzplanbearbeitung');
INSERT INTO `$prefix$flip_user_right` VALUES (32,CURRENT_TIMESTAMP,'lanparty_set_paid','Besitzer dieses Rechts dürfen Gäste auf bezahlt setzen');
INSERT INTO `$prefix$flip_user_right` VALUES (33,CURRENT_TIMESTAMP,'forum_admin','Erlaubt es, Foren und Foren-Gruppen zu verwalten (dies ist keine Moderationsrecht!)');
INSERT INTO `$prefix$flip_user_right` VALUES (34,CURRENT_TIMESTAMP,'edit_internal_profile','Erlaubt es, Userdaten zu bearbeiten, die dem User verborgen sind.');
INSERT INTO `$prefix$flip_user_right` VALUES (35,CURRENT_TIMESTAMP,'view_internal_profile','Erlaubt es, Userdaten zu betrachten, die dem normalen User verborgen sind.');
INSERT INTO `$prefix$flip_user_right` VALUES (36,CURRENT_TIMESTAMP,'forum_view','Erlaubt es, ein Forum einzusehen');
INSERT INTO `$prefix$flip_user_right` VALUES (37,CURRENT_TIMESTAMP,'forum_post','Erlaubt es, Nachrichten in ein Forum zu schreiben.');
INSERT INTO `$prefix$flip_user_right` VALUES (38,CURRENT_TIMESTAMP,'forum_edit_own_posts','Erlaubt es, die eigenen Beiträge zu bearbeiten');
INSERT INTO `$prefix$flip_user_right` VALUES (39,CURRENT_TIMESTAMP,'forum_moderate','Erlaubt es, ein einem Forum alle Beiträge zu bearbeiten oder zu löschen.');
INSERT INTO `$prefix$flip_user_right` VALUES (40,CURRENT_TIMESTAMP,'forum_post_fliptags','');
INSERT INTO `$prefix$flip_user_right` VALUES (41,CURRENT_TIMESTAMP,'forum_post_html','');
INSERT INTO `$prefix$flip_user_right` VALUES (42,CURRENT_TIMESTAMP,'news_post','(interne) News erstellen/bearbeiten');
INSERT INTO `$prefix$flip_user_right` VALUES (43,CURRENT_TIMESTAMP,'server_edit_own','Erlaubt es, die Server zu besitzen und deren Eigenschaften zu bearbeiten.');
INSERT INTO `$prefix$flip_user_right` VALUES (44,CURRENT_TIMESTAMP,'webmessage_use','Recht über sich selbst oder andere welches das Senden und Lesen von Webmessages erlaubt');
INSERT INTO `$prefix$flip_user_right` VALUES (45,CURRENT_TIMESTAMP,'catering_admin','Cateringverwaltungsrecht');
INSERT INTO `$prefix$flip_user_right` VALUES (46,CURRENT_TIMESTAMP,'catering_user','Cateringnutzungsrecht');
INSERT INTO `$prefix$flip_user_right` VALUES (47,CURRENT_TIMESTAMP,'tournament_orga','Turnierorga');
INSERT INTO `$prefix$flip_user_right` VALUES (48,CURRENT_TIMESTAMP,'checkin','Erlaubt es, User einzuchecken.');
INSERT INTO `$prefix$flip_user_right` VALUES (49,CURRENT_TIMESTAMP,'seats_manage_user','Erlaubt es, Usern einen Sitzplatz zuzuweisen.');
INSERT INTO `$prefix$flip_user_right` VALUES (50,CURRENT_TIMESTAMP,'user_adduser4turnier','Recht um User zu erstellen die sofort am Turnier teilnehmen können');
INSERT INTO `$prefix$flip_user_right` VALUES (51,CURRENT_TIMESTAMP,'server_admin','Serveradmin');
INSERT INTO `$prefix$flip_user_right` VALUES (52,CURRENT_TIMESTAMP,'calender_view','Erlaubt es, auf den Kalender zu zu greifen.');
INSERT INTO `$prefix$flip_user_right` VALUES (53,CURRENT_TIMESTAMP,'calender_create_events','Erlaubt es, im Kalender Ereignisse zu erstellen. Danach werden die Rechte um ein Ereignis zu bearbeiten individuell und über den Ereignistypen festgelegt.');
INSERT INTO `$prefix$flip_user_right` VALUES (54,CURRENT_TIMESTAMP,'calender_types_edit','Erlaubt es, die Ereignistypen zu bearbeiten.');
INSERT INTO `$prefix$flip_user_right` VALUES (55,CURRENT_TIMESTAMP,'archiv_createevents','Erlaubt es, im Archiv Ereignisse zu erstellen. Bearbeitunsrechte werden dann individuell festgelegt.');
INSERT INTO `$prefix$flip_user_right` VALUES (56,CURRENT_TIMESTAMP,'checkininfo_view','Erlaubt es das eigene Checkininfo (oder das eines Kontrollierten) ein zu sehen.');
INSERT INTO `$prefix$flip_user_right` VALUES (57,CURRENT_TIMESTAMP,'config_edit','Erlaubt es, die Config einzusehen und zu bearbeiten.');
INSERT INTO `$prefix$flip_user_right` VALUES (58,CURRENT_TIMESTAMP,'sendmessage_use','Erlaubt es, mittels Sendmessage Nachrichten an alle registrierten Accounts zu senden. (Newsletter usw.)');
INSERT INTO `$prefix$flip_user_right` VALUES (59,CURRENT_TIMESTAMP,'log_view','Erlaubt es, die Logs aus der Datenbank einzusehen. Ihr prominentester Vertreter ist der ChangeLog.');
INSERT INTO `$prefix$flip_user_right` VALUES (60,CURRENT_TIMESTAMP,'netlog_view','Erlaubt es, den NetLog einzusehen.');
INSERT INTO `$prefix$flip_user_right` VALUES (61,CURRENT_TIMESTAMP,'ticket_comment','Erlaubt es, Kommentare zu den Tickets zu schreiben.');
INSERT INTO `$prefix$flip_user_right` VALUES (62,CURRENT_TIMESTAMP,'ticket_edit','Erlaubt es, die Tickets zu bearbeiten (Status, Priorität, usw.)');
INSERT INTO `$prefix$flip_user_right` VALUES (63,CURRENT_TIMESTAMP,'ticket_own','Erlaubt es, Tickets zu besitzen, d.h. als ihr Verantwortlicher aufgeführt zu werden.');
INSERT INTO `$prefix$flip_user_right` VALUES (64,CURRENT_TIMESTAMP,'seats_note','Erlaubt es, sich einen Sitzplatz vorzumerken.');
INSERT INTO `$prefix$flip_user_right` VALUES (65,CURRENT_TIMESTAMP,'seats_reserve','Erlaubt es, sich einen Sitzplatz zu reservieren.');
INSERT INTO `$prefix$flip_user_right` VALUES (66,CURRENT_TIMESTAMP,'konten_admin','Darf Konten erstellen und bearbeiten');
INSERT INTO `$prefix$flip_user_right` VALUES (67,CURRENT_TIMESTAMP,'konten_view','Einsicht in die Konten');
INSERT INTO `$prefix$flip_user_right` VALUES (68,CURRENT_TIMESTAMP,'poll_admin','Recht um Umfragen zu erstellen und löschen');
INSERT INTO `$prefix$flip_user_right` VALUES (69,CURRENT_TIMESTAMP,'status_checked_out','Erhalten alle User, wenn sie die Party verlassen haben.');
INSERT INTO `$prefix$flip_user_right` VALUES (70,CURRENT_TIMESTAMP,'table_edit','Erlaubt es, Tabellen zu bearbeiten. Dies sollte jeder Orga besitzen, da jede Tabelle noch durch individuelle Rechte geschützt wird.');
INSERT INTO `$prefix$flip_user_right` VALUES (71,CURRENT_TIMESTAMP,'ticket_admin','Erlaubt es, die von den Tickets verwendeten Tabellen zu bearbeiten.');
INSERT INTO `$prefix$flip_user_right` VALUES (72,CURRENT_TIMESTAMP,'news_public','News public machen/freischalten');
INSERT INTO `$prefix$flip_user_right` VALUES (73,CURRENT_TIMESTAMP,'sponsor_view_internal','Erlaubt es, den internen Teil der Sponsorenliste ein zu sehen.');
INSERT INTO `$prefix$flip_user_right` VALUES (74,CURRENT_TIMESTAMP,'sysinfo_view','Erlaubt es, Sysinfo ein zu sehen.');
INSERT INTO `$prefix$flip_user_right` VALUES (75,CURRENT_TIMESTAMP,'sponsor_no_banner','Besitzer dieses Rechts sehen keine Sponsorenbanner');
INSERT INTO `$prefix$flip_user_right` VALUES (76,CURRENT_TIMESTAMP,'news_comments','Berechtigung um Newskommentare schreiben zu dürfen');
INSERT INTO `$prefix$flip_user_right` VALUES (77,CURRENT_TIMESTAMP,'news_editcomments','Recht um Kommentare bearbeiten/löschen zu dürfen');
INSERT INTO `$prefix$flip_user_right` VALUES (78,CURRENT_TIMESTAMP,'user_view_members','Wenn ein User dieses Recht Über eine Gruppe hat, kann er deren Mitgliederliste betrachten');
INSERT INTO `$prefix$flip_user_right` VALUES (79,CURRENT_TIMESTAMP,'banktransfer_view','Erlaubt es, die Kontodaten für den Unkostenbeitrag zur LANparty ein zu sehen.');
INSERT INTO `$prefix$flip_user_right` VALUES (80,CURRENT_TIMESTAMP,'checkin_edit','Erlaubt es, den Checkin zu bearbeiten');
INSERT INTO `$prefix$flip_user_right` VALUES (81,CURRENT_TIMESTAMP,'dbexport_create_dump','Erlaubt es, die gesamte Datenbank als SQL-Dump zu exportieren.');
INSERT INTO `$prefix$flip_user_right` VALUES (82,CURRENT_TIMESTAMP,'paypal_admin','Erlaubt es dem User, die PayPal-Buttons zu editieren');
INSERT INTO `$prefix$flip_user_right` VALUES (83,CURRENT_TIMESTAMP,'paypal_view','Erlaubt es dem User, die PayPal-Seite  einzusehen');
INSERT INTO `$prefix$flip_user_right` VALUES (84,CURRENT_TIMESTAMP,'lanparty_show_paidlog','Erlaubt es dem User den SetPaid-Log anzusehen.');
INSERT INTO `$prefix$flip_user_right` VALUES (85,CURRENT_TIMESTAMP,'tinfopage_view','Erlaubt dem Benutzer die Einsicht der Turnierinfo-Page');
INSERT INTO `$prefix$flip_user_right` VALUES (86,CURRENT_TIMESTAMP,'tinfopage_admin','Erlaubt dem Benutzer die Administration der Turnierinfo-Page');
INSERT INTO `$prefix$flip_user_right` VALUES (87,CURRENT_TIMESTAMP,'featuresystem_view','Berechtigt, das Featuresystem einzusehen');
INSERT INTO `$prefix$flip_user_right` VALUES (88,CURRENT_TIMESTAMP,'featuresystem_admin','Berechtigt, das Featuresystem zu administrieren');
INSERT INTO `$prefix$flip_user_right` VALUES (89,CURRENT_TIMESTAMP,'clan_join','Recht um an einem Clan teilnehmen zu dürfen');
INSERT INTO `$prefix$flip_user_right` VALUES (90,CURRENT_TIMESTAMP,'clan_leader','Recht um einen Clan zu bearbeiten');
INSERT INTO `$prefix$flip_user_right` VALUES (91,CURRENT_TIMESTAMP,'gb_add','Recht um ein Gästebucheintrag zu erstellen');
INSERT INTO `$prefix$flip_user_right` VALUES (92,CURRENT_TIMESTAMP,'gb_admin','Recht um das Gästebuch zu verwalten');
INSERT INTO `$prefix$flip_user_right` VALUES (93,CURRENT_TIMESTAMP,'translate','Recht um das FLIP übersetzen zu dürfen.');
INSERT INTO `$prefix$flip_user_right` VALUES (94,CURRENT_TIMESTAMP,'watches_use','Recht um die Foren-Watches nutzen zu können.');
INSERT INTO `$prefix$flip_user_right` VALUES (95,CURRENT_TIMESTAMP,'inet_view','Erlaubt es, das Internet-Modul einzusehen und zu benutzen');
INSERT INTO `$prefix$flip_user_right` VALUES (96,CURRENT_TIMESTAMP,'inet_admin','Berechtigt, das Internet-Modul zu administrieren');
INSERT INTO `$prefix$flip_user_right` VALUES (97,CURRENT_TIMESTAMP,'gallery_admin','Erlaubt es, die Bildergalerie zu administrieren');
INSERT INTO `$prefix$flip_user_right` VALUES (98,CURRENT_TIMESTAMP,'gallery_view','Erlaubt es, die Bildergalerie anzusehen');
INSERT INTO `$prefix$flip_user_right` VALUES (103,CURRENT_TIMESTAMP,'shop_use','Ermöglicht die Benutzung des Shop Moduls');
INSERT INTO `$prefix$flip_user_right` VALUES (100,CURRENT_TIMESTAMP,'shop_admin','Erlaubt seinem Besitzer die Verwaltungs des Shops');
INSERT INTO `$prefix$flip_user_right` VALUES (101,CURRENT_TIMESTAMP,'ticket_request_support','Berechtigt einen User, Supportanfragen zu stellen');
INSERT INTO `$prefix$flip_user_right` VALUES (105,CURRENT_TIMESTAMP,'clan_admin','Recht um bei allen CLANs die Mitglieder zu verwalten');
INSERT INTO `$prefix$flip_user_right` VALUES (107,CURRENT_TIMESTAMP,'status_paid_clan','Recht das der CLAN fuer seine Mitglieder bezahlt/ueberwiesen hat.');
INSERT INTO `$prefix$flip_user_right` VALUES (108,CURRENT_TIMESTAMP,'status_registered_console','Erhält jeder der sich als Konsolenspieler für das Event angemeldet hat.');
INSERT INTO `$prefix$flip_user_right` VALUES (109,CURRENT_TIMESTAMP,'admin_menue','Zugriff auf das Admin Menue');
# --------------------------- /flip_user_right -------------------------- #


# --------------------------- flip_user_rights --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_user_rights`;
CREATE TABLE `$prefix$flip_user_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `owner_id` int(11) NOT NULL DEFAULT '0',
  `right_id` int(11) NOT NULL DEFAULT '0',
  `controled_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `owner_right_controled` (`owner_id`,`right_id`,`controled_id`),
  KEY `i_right` (`right_id`),
  KEY `i_owner` (`owner_id`),
  KEY `i_controled` (`controled_id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=270 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Die wichtigsten Datensaetze:
INSERT INTO `$prefix$flip_user_rights` VALUES (43,CURRENT_TIMESTAMP,2,1,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (45,CURRENT_TIMESTAMP,2,4,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (46,CURRENT_TIMESTAMP,10,5,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (48,CURRENT_TIMESTAMP,10,3,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (51,CURRENT_TIMESTAMP,81,10,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (52,CURRENT_TIMESTAMP,81,11,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (53,CURRENT_TIMESTAMP,10,12,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (54,CURRENT_TIMESTAMP,13,13,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (55,CURRENT_TIMESTAMP,14,13,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (56,CURRENT_TIMESTAMP,262,3,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (58,CURRENT_TIMESTAMP,10,15,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (59,CURRENT_TIMESTAMP,262,15,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (60,CURRENT_TIMESTAMP,2,16,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (65,CURRENT_TIMESTAMP,14,19,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (67,CURRENT_TIMESTAMP,10,13,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (69,CURRENT_TIMESTAMP,10,19,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (70,CURRENT_TIMESTAMP,262,13,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (72,CURRENT_TIMESTAMP,262,19,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (75,CURRENT_TIMESTAMP,709,13,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (77,CURRENT_TIMESTAMP,709,19,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (78,CURRENT_TIMESTAMP,709,22,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (79,CURRENT_TIMESTAMP,710,13,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (81,CURRENT_TIMESTAMP,710,19,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (82,CURRENT_TIMESTAMP,10,22,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (83,CURRENT_TIMESTAMP,262,22,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (87,CURRENT_TIMESTAMP,10,23,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (88,CURRENT_TIMESTAMP,10,25,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (91,CURRENT_TIMESTAMP,881,27,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (92,CURRENT_TIMESTAMP,881,28,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (93,CURRENT_TIMESTAMP,881,24,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (94,CURRENT_TIMESTAMP,881,26,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (96,CURRENT_TIMESTAMP,10,16,2);
INSERT INTO `$prefix$flip_user_rights` VALUES (98,CURRENT_TIMESTAMP,882,6,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (99,CURRENT_TIMESTAMP,882,9,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (101,CURRENT_TIMESTAMP,882,2,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (102,CURRENT_TIMESTAMP,81,29,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (103,CURRENT_TIMESTAMP,1,30,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (104,CURRENT_TIMESTAMP,81,31,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (108,CURRENT_TIMESTAMP,81,33,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (109,CURRENT_TIMESTAMP,881,35,2);
INSERT INTO `$prefix$flip_user_rights` VALUES (110,CURRENT_TIMESTAMP,881,34,2);
INSERT INTO `$prefix$flip_user_rights` VALUES (111,CURRENT_TIMESTAMP,2,38,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (112,CURRENT_TIMESTAMP,2,37,917);
INSERT INTO `$prefix$flip_user_rights` VALUES (113,CURRENT_TIMESTAMP,2,36,917);
INSERT INTO `$prefix$flip_user_rights` VALUES (114,CURRENT_TIMESTAMP,10,37,918);
INSERT INTO `$prefix$flip_user_rights` VALUES (115,CURRENT_TIMESTAMP,10,36,918);
INSERT INTO `$prefix$flip_user_rights` VALUES (117,CURRENT_TIMESTAMP,10,41,918);
INSERT INTO `$prefix$flip_user_rights` VALUES (120,CURRENT_TIMESTAMP,10,41,917);
INSERT INTO `$prefix$flip_user_rights` VALUES (121,CURRENT_TIMESTAMP,10,39,917);
INSERT INTO `$prefix$flip_user_rights` VALUES (122,CURRENT_TIMESTAMP,1,36,917);
INSERT INTO `$prefix$flip_user_rights` VALUES (125,CURRENT_TIMESTAMP,1013,42,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (136,CURRENT_TIMESTAMP,2,44,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (158,CURRENT_TIMESTAMP,2,46,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (159,CURRENT_TIMESTAMP,1298,34,2);
INSERT INTO `$prefix$flip_user_rights` VALUES (160,CURRENT_TIMESTAMP,1298,35,2);
INSERT INTO `$prefix$flip_user_rights` VALUES (161,CURRENT_TIMESTAMP,1298,48,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (162,CURRENT_TIMESTAMP,1298,16,2);
INSERT INTO `$prefix$flip_user_rights` VALUES (163,CURRENT_TIMESTAMP,1298,1,2);
INSERT INTO `$prefix$flip_user_rights` VALUES (165,CURRENT_TIMESTAMP,10,35,2);
INSERT INTO `$prefix$flip_user_rights` VALUES (166,CURRENT_TIMESTAMP,1298,49,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (168,CURRENT_TIMESTAMP,1299,43,1097);
INSERT INTO `$prefix$flip_user_rights` VALUES (169,CURRENT_TIMESTAMP,1299,51,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (170,CURRENT_TIMESTAMP,1299,51,1097);
INSERT INTO `$prefix$flip_user_rights` VALUES (171,CURRENT_TIMESTAMP,10,52,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (172,CURRENT_TIMESTAMP,81,54,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (173,CURRENT_TIMESTAMP,10,53,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (174,CURRENT_TIMESTAMP,81,55,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (175,CURRENT_TIMESTAMP,14,56,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (176,CURRENT_TIMESTAMP,10,56,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (177,CURRENT_TIMESTAMP,81,57,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (178,CURRENT_TIMESTAMP,882,58,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (180,CURRENT_TIMESTAMP,1300,17,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (181,CURRENT_TIMESTAMP,10,59,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (182,CURRENT_TIMESTAMP,10,60,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (183,CURRENT_TIMESTAMP,10,61,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (184,CURRENT_TIMESTAMP,10,62,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (185,CURRENT_TIMESTAMP,10,63,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (186,CURRENT_TIMESTAMP,14,65,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (187,CURRENT_TIMESTAMP,13,64,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (188,CURRENT_TIMESTAMP,10,45,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (190,CURRENT_TIMESTAMP,10,67,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (191,CURRENT_TIMESTAMP,1296,32,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (192,CURRENT_TIMESTAMP,1296,66,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (193,CURRENT_TIMESTAMP,10,68,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (194,CURRENT_TIMESTAMP,14,64,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (195,CURRENT_TIMESTAMP,710,69,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (196,CURRENT_TIMESTAMP,10,70,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (197,CURRENT_TIMESTAMP,81,71,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (198,CURRENT_TIMESTAMP,1013,72,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (199,CURRENT_TIMESTAMP,10,42,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (200,CURRENT_TIMESTAMP,881,50,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (201,CURRENT_TIMESTAMP,709,56,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (202,CURRENT_TIMESTAMP,883,22,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (203,CURRENT_TIMESTAMP,883,19,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (204,CURRENT_TIMESTAMP,883,13,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (205,CURRENT_TIMESTAMP,883,56,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (206,CURRENT_TIMESTAMP,1301,22,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (207,CURRENT_TIMESTAMP,1301,19,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (208,CURRENT_TIMESTAMP,1301,13,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (209,CURRENT_TIMESTAMP,1301,56,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (210,CURRENT_TIMESTAMP,710,56,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (211,CURRENT_TIMESTAMP,10,73,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (212,CURRENT_TIMESTAMP,81,74,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (213,CURRENT_TIMESTAMP,2,76,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (214,CURRENT_TIMESTAMP,10,77,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (215,CURRENT_TIMESTAMP,1,78,10);
INSERT INTO `$prefix$flip_user_rights` VALUES (216,CURRENT_TIMESTAMP,2,78,10);
INSERT INTO `$prefix$flip_user_rights` VALUES (217,CURRENT_TIMESTAMP,1298,44,4);
INSERT INTO `$prefix$flip_user_rights` VALUES (218,CURRENT_TIMESTAMP,882,1,4);
INSERT INTO `$prefix$flip_user_rights` VALUES (219,CURRENT_TIMESTAMP,882,16,4);
INSERT INTO `$prefix$flip_user_rights` VALUES (220,CURRENT_TIMESTAMP,10,79,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (221,CURRENT_TIMESTAMP,13,79,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (222,CURRENT_TIMESTAMP,1299,16,1097);
INSERT INTO `$prefix$flip_user_rights` VALUES (223,CURRENT_TIMESTAMP,881,80,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (224,CURRENT_TIMESTAMP,81,81,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (226,CURRENT_TIMESTAMP,81,34,1303);
INSERT INTO `$prefix$flip_user_rights` VALUES (227,CURRENT_TIMESTAMP,81,1,1303);
INSERT INTO `$prefix$flip_user_rights` VALUES (228,CURRENT_TIMESTAMP,81,35,1303);
INSERT INTO `$prefix$flip_user_rights` VALUES (229,CURRENT_TIMESTAMP,81,16,1303);
INSERT INTO `$prefix$flip_user_rights` VALUES (233,CURRENT_TIMESTAMP,2,43,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (234,CURRENT_TIMESTAMP,2,85,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (235,CURRENT_TIMESTAMP,1300,86,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (236,CURRENT_TIMESTAMP,2,89,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (237,CURRENT_TIMESTAMP,1298,90,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (239,CURRENT_TIMESTAMP,2,90,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (240,CURRENT_TIMESTAMP,2,91,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (241,CURRENT_TIMESTAMP,1,91,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (242,CURRENT_TIMESTAMP,10,92,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (244,CURRENT_TIMESTAMP,1298,90,1305);
INSERT INTO `$prefix$flip_user_rights` VALUES (248,CURRENT_TIMESTAMP,10,95,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (249,CURRENT_TIMESTAMP,10,96,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (250,CURRENT_TIMESTAMP,709,95,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (253,CURRENT_TIMESTAMP,1296,87,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (254,CURRENT_TIMESTAMP,1296,88,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (255,CURRENT_TIMESTAMP,10,97,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (258,CURRENT_TIMESTAMP,2,94,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (261,CURRENT_TIMESTAMP,2,98,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (262,CURRENT_TIMESTAMP,10,99,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (263,CURRENT_TIMESTAMP,10,100,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (265,CURRENT_TIMESTAMP,10,83,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (266,CURRENT_TIMESTAMP,10,82,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (267,CURRENT_TIMESTAMP,1442,93,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (268,CURRENT_TIMESTAMP,2,101,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (269,CURRENT_TIMESTAMP,1296,101,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (270,CURRENT_TIMESTAMP,1296,109,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (271,CURRENT_TIMESTAMP,10,103,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (278,CURRENT_TIMESTAMP,1443,13,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (279,CURRENT_TIMESTAMP,1443,56,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (280,CURRENT_TIMESTAMP,1443,64,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (281,CURRENT_TIMESTAMP,10,109,0);
INSERT INTO `$prefix$flip_user_rights` VALUES (282,CURRENT_TIMESTAMP,881,105,0);
# --------------------------- /flip_user_rights -------------------------- #


# --------------------------- flip_user_subject --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_user_subject`;
CREATE TABLE `$prefix$flip_user_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` varchar(16) COLLATE utf8mb4_bin NOT NULL DEFAULT 'user',
  `name` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `email` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nickname` (`type`,`name`),
  UNIQUE KEY `email` (`email`,`type`),
  KEY `i_type` (`type`),
  KEY `mtime` (`mtime`),
  KEY `i_email` (`email`),
  KEY `i_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=1445 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze:
INSERT INTO `$prefix$flip_user_subject` VALUES (1,CURRENT_TIMESTAMP,'systemuser','Anonymous',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (2,CURRENT_TIMESTAMP,'group','registered',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (3,CURRENT_TIMESTAMP,'systemuser','Deleted',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (4,CURRENT_TIMESTAMP,'systemuser','System','donotreply@your-lanparty.de');
INSERT INTO `$prefix$flip_user_subject` VALUES (10,CURRENT_TIMESTAMP,'group','orga',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (13,CURRENT_TIMESTAMP,'group','status_registered',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (14,CURRENT_TIMESTAMP,'group','status_paid',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (81,CURRENT_TIMESTAMP,'group','webmaster',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (262,CURRENT_TIMESTAMP,'group','helper',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (709,CURRENT_TIMESTAMP,'group','status_checked_in',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (710,CURRENT_TIMESTAMP,'group','status_checked_out',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (881,CURRENT_TIMESTAMP,'group','useradmin',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (882,CURRENT_TIMESTAMP,'group','publisher',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (883,CURRENT_TIMESTAMP,'group','status_online',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (917,CURRENT_TIMESTAMP,'group','forums_public',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (918,CURRENT_TIMESTAMP,'group','forums_internal',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (919,CURRENT_TIMESTAMP,'forum','Allgemeines',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (925,CURRENT_TIMESTAMP,'forum','Turniere',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (929,CURRENT_TIMESTAMP,'forum','Offtopic',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (967,CURRENT_TIMESTAMP,'forum','Technik',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (970,CURRENT_TIMESTAMP,'forum','Off-Topic',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (1013,CURRENT_TIMESTAMP,'group','newschecker',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (1097,CURRENT_TIMESTAMP,'group','servers',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (1290,CURRENT_TIMESTAMP,'type','type',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (1291,CURRENT_TIMESTAMP,'type','user',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (1292,CURRENT_TIMESTAMP,'type','group',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (1293,CURRENT_TIMESTAMP,'type','server',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (1294,CURRENT_TIMESTAMP,'type','forum',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (1295,CURRENT_TIMESTAMP,'type','turnierteam',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (1296,CURRENT_TIMESTAMP,'user','test','test@lanpary.local');
INSERT INTO `$prefix$flip_user_subject` VALUES (1297,CURRENT_TIMESTAMP,'type','turnier',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (1298,CURRENT_TIMESTAMP,'group','usermanager',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (1299,CURRENT_TIMESTAMP,'group','networkadmin',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (1300,CURRENT_TIMESTAMP,'group','tournament_admins',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (1301,CURRENT_TIMESTAMP,'group','status_offline',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (1302,CURRENT_TIMESTAMP,'systemuser','DBExport',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (1303,CURRENT_TIMESTAMP,'group','sysaccounts',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (1304,CURRENT_TIMESTAMP,'type','clan',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (1305,CURRENT_TIMESTAMP,'group','clans',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (1400,CURRENT_TIMESTAMP,'type','systemuser',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (1442,CURRENT_TIMESTAMP,'group','translator',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (1443,CURRENT_TIMESTAMP,'group','status_paid_clan',NULL);
INSERT INTO `$prefix$flip_user_subject` VALUES (1444,CURRENT_TIMESTAMP,'group','status_registered_console',NULL);
# --------------------------- /flip_user_subject -------------------------- #


# --------------------------- flip_user_type --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_user_type`;

# Diese Tabelle wird nicht weiter verwendet.
# --------------------------- /flip_user_type -------------------------- #


# --------------------------- flip_webmessage_folder --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_webmessage_folder`;
CREATE TABLE `$prefix$flip_webmessage_folder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_webmessage_folder -------------------------- #


# --------------------------- flip_webmessage_message --------------------------- #
DROP TABLE IF EXISTS `$prefix$flip_webmessage_message`;
CREATE TABLE `$prefix$flip_webmessage_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `owner_id` int(11) NOT NULL DEFAULT '0',
  `sender_id` int(11) NOT NULL DEFAULT '0',
  `processor_id` int(11) NOT NULL DEFAULT '0',
  `subject` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `status` enum('unread','read','processed') COLLATE utf8mb4_bin NOT NULL DEFAULT 'unread',
  `text` mediumtext COLLATE utf8mb4_bin NOT NULL,
  `date` int(11) NOT NULL DEFAULT '0',
  `source_type` enum('resend','replay','oforward','sforward') COLLATE utf8mb4_bin DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL,
  `owner_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `sender_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `folder_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mtime` (`mtime`),
  KEY `i_owner_status_deleted` (`owner_id`,`status`,`owner_deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
# Alle Datensaetze befinden sich in der Datei ./../../db/flip-testing.sql
# --------------------------- /flip_webmessage_message -------------------------- #



# --- EOF --- #
