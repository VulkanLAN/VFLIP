<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: mod.netlog.protocol.php 536 2004-10-12 22:38:17Z docfx $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** Versionsnummer */
define("RPC_HEADER", "FLIP-RPC-1.0");

class RPCData {
	var $errors;

	function readErrors() {
		global $User;
		$this->errors = array ();
		$err = getErrors();
		if (!is_array($err))
			$err = array ();
		$debug = $User->hasRight("view_debug_messages");
		foreach ($err as $e)
			$this->errors[] = $e->getMessage($debug);
	}

	function raiseErrors() {
		if (is_array($this->errors))
			foreach ($this->errors as $err)
				trigger_error_text("Serverfehler: $err", E_USER_WARNING);
	}

	function get() {
		$this->readErrors();
		return RPC_HEADER.":".gzcompress(serialize($this));
	}

	function out() {

		echo $this->get();
	}
}

class RPCFatalError extends RPCData {
	var $message;

	//php 7 public function __construct()
	//php 5 original function RPCFatalError()
	function __construct($msg) {
		global $User;
		$debug = $User->hasRight("view_debug_messages");
		$this->message = $msg->getMessage($debug);
	}

	function raiseErrors() {
		RPCData :: raiseErrors();
		trigger_error_text("Fataler Serverfehler: ".$this->message, E_USER_ERROR);
	}
}

function RPCRead($URL) {
	include_once ("mod/mod.httpclient.php");
	$dat = HTTPRequestByGet($URL);
	if (empty ($dat))
		trigger_error_text("Es wurden keine Daten vom Server empfangen.", E_USER_ERROR);

	list ($header, $obj) = explode(":", $dat, 2);
	if ($header != RPC_HEADER)
		trigger_error_text("Die Daten vom Server konnten nicht verarbeitet werden.|dat:$dat", E_USER_ERROR);

	$obj = unserialize(gzuncompress($obj));
	if (!is_a($obj, "RPCData") and !empty($obj))
		trigger_error_text("Die vom Server empfangenden Daten sind nicht vom Typ RPCData.", E_USER_ERROR);
	$obj->raiseErrors();
	return $obj;
}

function RPCFatalErrorHandler($msg) {
	$d = new RPCFatalError($msg);
	$d->out();
}

function RPCSetErrorHandler() {
	SetFatalErrorCallback("RPCFatalErrorHandler");
}
?>