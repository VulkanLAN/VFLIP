<?php
/**
 * @author Daniel Raap
 * @version $Id: mod.lanparty.php 1351 2007-01-30 13:36:09Z loom $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/**
 * Gibt einen Balken mit bis zu drei verschiedenen Werten aus
 * edit by naaux -> evt. loeschen aufgrund der integrierten bootstrap Funktion
 */
function LanpartyStatusBar($status1, $status2, $status3=false, $width=200, $height=16)
{
  header("Content-type: image/png");
  $status1 = (integer) $status1;
  $status2 = (integer) $status2;
  $status3 = (integer) $status3;
  //falls leere Parameter &uuml;bergeben werden
  if(empty($status3)) $status3 = false;
  if(empty($width)) $width = 200;
  if(empty($height)) $height = 16;
  
  //Pixelbreite der einzelnen Bereiche
  $stat1px = $status1*$width/($status1+$status2+$status3);
  $stat2px = $status2*$width/($status1+$status2+$status3);
  $stat3px = $status3*$width/($status1+$status2+$status3);
  
  //Bild erstellen und Farben allokieren
  $bild = @imagecreate($width, $height) or die("Kann keinen neuen GD-Bild-Stream erzeugen.");
  $rot     = ImageColorAllocate($bild, 255, 0, 0);
  $gruen   = ImageColorAllocate($bild, 0, 255, 0);
  $gelb    = ImageColorAllocate($bild, 255, 255, 0);
  $schwarz = ImageColorAllocate($bild, 0, 0, 0);
  //die verschiedenen Bereiche
  ImageFilledRectangle($bild, $stat1px, 0, $width, $height, $gelb);
  if($status3) ImageFilledRectangle($bild, $stat1px+$stat2px, 0, $width, $height, $gruen);
  //Schriftgr&ouml;&szlig;e w&auml;hlen und Text in die Balken schreiben
  if($height>=22)
    $font = 5;
  elseif($height>=16)
    $font = 4;
  elseif($height>=14)
    $font = 3;
  elseif($height>=12)
    $font = 2;
  else
    $font = 1;
  ImageString($bild, $font, ($stat1px/2-strlen($status1)*imagefontwidth($font)/2), ($height/2-imagefontheight($font)/2), $status1, $schwarz);
  ImageString($bild, $font, ($stat1px+$stat2px/2-strlen($status1)*imagefontwidth($font)/2), ($height/2-imagefontheight($font)/2), $status2, $schwarz);
  if($status3) ImageString($bild, $font, ($stat1px+$stat2px+$stat3px/2-strlen($status1)*imagefontwidth($font)/2), ($height/2-imagefontheight($font)/2), $status3, $schwarz);
  //Bild ausgeben
  ImagePNG($bild);
}


function Lanparty_updatelpde()
{
  $g = new Group("status_registered");
  $wait = count($g->getChilds());
  $g = new Group("status_paid");
  $guest = count($g->getChilds());
  unset($g);
  
  ob_start();
  include("ext/usercountupdate.php");
  $r = ob_get_contents();
  ob_end_clean();
  if(!empty($r)) trigger_error_text("Unbestimmte zeichen: $r");
}

function LanpartyGetStatusCount($status) {
  static $cache = array();
  if(!empty($cache[$status])) return $cache[$status];
  switch($status) {
    case("unpaid"): // die Anzahl der freien Pl&auml;tze auf der Party.
      $r = ConfigGet("lanparty_maxguests") - LanpartyGetStatusCount("paid");
      break;
      
    case("visiters"):
      $r = LanpartyGetStatusCount("paid") + LanpartyGetStatusCount("registered");
      break;
      
    case("allpaid"):
      $r = LanpartyGetStatusCount("paid") + 
           LanpartyGetStatusCount("checked_in") + 
           LanpartyGetStatusCount("checked_out") + 
           LanpartyGetStatusCount("online") + 
           LanpartyGetStatusCount("offline");
      break;
    
    case("max"): // die maximale Anzahl an G&auml;sten auf der Party.
      $r = ConfigGet("lanparty_maxguests");
      break;
      
    default:
      $g = new Group("status_$status");
      $r = $g->getChildsCountIgnoreDisabled();  
      break;
  }
  return $cache[$status] = $r;
}


?>