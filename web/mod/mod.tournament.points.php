<?php
/**
 * Klasse welche Gruppenspiele modelliert
 * @author loom
 * @version $Id$
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 * @since 1445 29.07.2007 
 **/

class PointsMatches {

	function setScore(&$turnier, $match = false, $points1 = false, $points2 = false, $comment = '', $freecall = false)
	{
		global $User;
		
		if(!is_a($turnier, 'Turniersystem'))
		{
			return text_translate('Fehler: Kein Turnier angegeben!');
		}
		if(!is_a($match, 'TournamentMatch'))
		{
			return text_translate('Fehler: Kein Match angegeben!');
		}
			
		//ID des Eintragenden
		$editor = ($freecall) ? 0 : $User->id;
		
		$done = 'Fehler: Es wurde nichts gemacht!?';
		if($turnier->status == 'games') {
			/* KO-Baum */
			require_once('mod/mod.tournament.ko.php');
			$komatches = new KoMatches();
			$done = $komatches->setScore($turnier, $match, $points1, $points2, $comment, $freecall);
		} elseif ($turnier->status == 'grpgames' || ($turnier->status == 'start' && $turnier->type == 'points')) {
			/********************/
			/*  Gruppen-System  */
			/********************/
	
			// Ergebnis eintragen
			$match->submitScore($points1, $points2, $editor);
			$match->setComment($comment);
			$done = "Ergebnis wurde eingetragen.";
	
			// KO-Runden erstellen wenn alle Gruppenspiele gespielt wurden
			if (MysqlReadField("SELECT COUNT(*) FROM ".TblPrefix()."flip_tournament_matches
		                         WHERE tournament_id='".$match->tournament_id."' AND points1 IS NULL", "COUNT(*)") < 1) {
				$start = time();
				//Punktestand ermitteln
				$points = array();
				foreach ($turnier->GetMatches(false, false, true) AS $amatch) {
					//hint: $amatch['level'] is the group
					if(!isset($points[$amatch['level']][$amatch['team1']]))
						$points[$amatch['level']][$amatch['team1']] = 0;
					if(!isset($points[$amatch['level']][$amatch['team2']]))
						$points[$amatch['level']][$amatch['team2']] = 0;
					if ($amatch['points1'] > $amatch['points2']) {
						$points[$amatch['level']][$amatch['team1']] += 3;
					}
					if ($amatch['points2'] > $amatch['points1']) {
						$points[$amatch['level']][$amatch['team2']] += 3;
					}
					if ($amatch['points1'] == $amatch['points2']) {
						$points[$amatch['level']][$amatch['team1']] += 1;
						$points[$amatch['level']][$amatch['team2']] += 1;
					}
				}
				//Gruppenerste in KO-Runde &uuml;bernehmen, Rest bekommt Rang
				$ranking = array();
				foreach ($points AS $ateam) {
					arsort($ateam);
					//ermitteln wieviele die meisten Punkte haben
					$first = true;
					$addtoko = true;
					$oldscore = null;
					foreach ($ateam AS $teamid => $score) {
						if ($first) {
							$first = false;
						}
						elseif ($score != $oldscore) {
							$addtoko = false;
							$ranking[$score][] = $teamid;
						}
						$oldscore = $score;
						if ($addtoko)
							$combatant[] = array ("team_id" => $teamid);
					}
				}
				$currentrank = count($combatant) + 1;
				foreach ($ranking AS $gotpoints => $teamarray) {
					foreach ($teamarray AS $ateam)
						$turnier->setRank($ateam, $currentrank, $match->tournament_id);
					$currentrank = $currentrank +count($teamarray);
				}
				//KO-Spiele erstellen
				$combatant = array_filter($combatant, "is_array");
				require_once("mod/mod.tournament.php");
				$turnier = new Turniersystem($match->tournament_id);
				require_once('mod/mod.tournament.ko.php');
				$komatches = new KoMatches();
				if ($komatches->_CreateKOTree($turnier, $combatant))
					$done .= "<br />KO-Runde wurde erstellt.";
				else
					TournamentError("KO-Runde konnte nicht erstellt werden.", E_USER_WARNING);
				unset ($turnier);
			}
		} else {
			TournamentError('Es werden keine Gruppenspiele ausgetragen, aber es soll ein Ergebnis eingetragen werden!|Turniertyp: '.$turnier->type.', Status: '. $turnier->status, E_USER_ERROR);
		}
		
		return $done;
	}

	/**
	 * Erstellt den Turnierbaum
	 */
	function GenerateMatches(&$turnier, $status = false) {
		global $User;		
		if(!$turnier->Orga)
			$User->requireRight(TournamentAdminright());
		$tournamentid = $turnier->id;
		if (empty ($tournamentid))
			return TournamentError("Spiele konnten nicht erstellt werden.|Es wurde keine ID angegeben.", E_USER_WARNING, __FILE__, __LINE__);
		/* Status &uuml;berpr&uuml;fen */
		if ($turnier->status != "open")
			return TournamentError("Turniere k&ouml;nnen nur erstellt werden wenn der Status auf 'Anmeldung' steht.|Nur w&auml;hrend der Anmeldung k&ouml;nnen Spiele erstellt werden.", E_USER_WARNING, __FILE__, __LINE__);

		/* vorhandene Spiele entfernen */
		$turnier->_DeleteGames();

		settimelimit(0);
		/* neue Spiele erstellen */
		// Spieler des Turniers auslesen
		$combatant = $turnier->GetCombatants();
		$combatant = array_filter($combatant, "is_array");
		srand((double) microtime() * 1000000);

		// Gruppenspiele
		if (empty ($turnier->groups) || $turnier->groups < 1)
			return TournamentError("Die Anzahl der Gruppen wurde nicht festgelegt!", E_USER_WARNING, __FILE__, __LINE__);
		$num = count($combatant);
		if ($num < 1)
			return TournamentError("Keine Spieler vorhanden.", E_USER_WARNING, __FILE__, __LINE__);

		// Anzahl Spieler je Gruppe
		$start = time();
		$groupsize = ceil($num / $turnier->groups);
		if ($groupsize % 2 != 0)
			$groupsize ++;
		$maxplayers = $groupsize * $turnier->groups;
		$add = $maxplayers - $num;

		// Gruppen mit Teams F&uuml;llen
		$groupNo = TournamentGroupmatchestartlevel() + 1; //Gruppennummerierung = 'level' in DB; bei groupmatchestartlevel beginnen.
		$match = array();
		while (!empty ($combatant) || !empty ($match)) {
			$newgroup = array ();
			$remaining = $groupsize;

			// ggf. Freilos hinzuf&uuml;gen
			if ($add > 0) {
				$newgroup[] = "0";
				$add --;
				$remaining --;
			}

			// Teams hinzuf&uuml;gen
			$players = array_splice($combatant, 0, $remaining);
			for ($i = 0; $i < $remaining; $i ++) {
				$newgroup[] = $players[$i]['team_id'];
			}
			//shuffle($newgroup);

			
			// Paarungen erstellen (jeder gegen jeden)
			while (count($newgroup) > 0) {
				//$player aus $newgroup nehmen
				$player = array_shift($newgroup);
				//$player spielt gegen alle in $newgroup verbeliebenen teams
				foreach ($newgroup AS $pl) {
					$paarungen[] = array ($player, $pl);
				}
			}

			// Paarungen Gruppieren (gleichzeitige Spiele)
			$groups = array ();
			while (!empty ($paarungen) || !empty ($match)) // solange es noch spiele gibt m&uuml;ssen diese in eine gruppierung passen
			{
				for ($i = 0; $i < $groupsize -1; $i ++) //$i+1 = Gruppierung = levelid
				{
					if (empty ($match)) {
						$match = array_shift($paarungen);
					}
					/*
					if (!empty ($match) && !(is_array($groups[$i]) && (recursive_in_array($match[0], $groups[$i]) || recursive_in_array($match[1], $groups[$i])))) {
						$groups[$i][] = $match;
						$endtime = ($turnier->roundtime > 0) ? ($start + $turnier->roundtime * 60 * ($i +1)) : 0;
						$new_matchid = MysqlWriteByID(TblPrefix()."flip_tournament_matches", array ("tournament_id" => $tournamentid, "endtime" => $endtime, "team1" => $match[0], "team2" => $match[1], "level" => $groupNo, "levelid" => ($i +1)));
						$turnier->readyUp(new TournamentMatch($new_matchid));
						//unset ($match);
						$match = array();
					}*/
					print "Group: " . $groupNo . ": ";
					print "$match[0]" .  " vs " . "$match[1]" . "   ";
					$match = array();
				}
			}
			$groupNo ++;
		}
		print "Debug";
		return TournamentError("Abbruch Debug.", E_USER_WARNING, __FILE__, __LINE__);
		$newstatus = (!$status) ? 'grpgames' : $status;
		$turnier->_Log("Status von ".$turnier->status." auf $newstatus gesetzt.");
		$turnier->status = $newstatus;
		MysqlWriteByID(TblPrefix().'flip_tournament_tournaments', array ('status' => $turnier->status, 'start' => $start), $tournamentid);

		$turnier->_PlayFreeGames();

		$turnier->_Log("Gruppenspiele wurden erstellt.");
		
		TournamentError("Spiele wurden erstellt und Status auf \"".$turnier->GetStatusString()."\" gesetzt.");

		return true;
	}
}

function recursive_in_array($search, $array) {
	if (!is_array($array)) {
		trigger_error_text("recursive_in_array(): Kein Array!",E_USER_WARNING);
		return false;
	}
	if (in_array($search, $array))
		return true;
	foreach ($array AS $val) {
		if ($search == $val)
			return true;
		elseif(is_array($val) && recursive_in_array($search, $val))
			return true;
	}
	return false;
}
?>
