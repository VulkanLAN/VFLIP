<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: mod.parse.php 1395 2007-06-15 13:59:13Z loom $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/**
* Die Funktion ParseParam() parst einen Parameter in seine einzelnen Schl&uuml;ssel => Wert Verh&auml;ltnisse.
* Der Parameter kann dabei die Form haben, welche bei den Attributen eines HTML-Tags &uuml;blich ist:
* <code>
*   paramname=test p1name="ein langer inhalt"
* </code>
* Genauso k&ouml;nnen die Werte den Schl&uuml;sseln auch durch ihre Reihenfolge zugeordnet werden. (anstatt 
* durch das vorangehende "schluesselname="):
* <code>
*   test "ein langer inhalt"
* </code>
* beide M&ouml;glichkeiten lassen sich kombinieren, wobei es sich nicht empfielt nach einem 
* durch einen Schl&uuml;sselnamen idetifizierten Wert noch einen durch seine Reihenfolge idetifizierten 
* unter zu bringen. Also:
* <code>
*   gut:
*   test test2 seltenesflag=12 meldung="so dies und jenes"
*   schlecht:
*   test test2 meldung="so dies und jenes" 12
* </code>
* @param string $Param der zu parsende Parameter
* @param array $ParamNames Die Namen der Parameter in der Reihenfolge in der sie identifiziert werden.
* @return array Verl&auml;uft das parsen erfolgreich wird ein Array im Format Schl&uuml;ssel => Wert zur&uuml;ckgegeben,
*   ansonsten ein string mit einer Fehlermeldung.
* 
*/
function ParseParam($Param,$ParamNames,$FixedParams=true)
{
  $r = array();
  foreach($ParamNames AS $pn)
  	$r[$pn] = null;
  $str = $key = "";
  $ai = $i = 0;
  $Param = trim($Param)." ";
  while($i < strlen($Param))
  {
    switch($Param{$i})
    {
      case("\""): 
        $i++;
        while(($Param{$i} != "\"") or ($Param{$i-1} == "\\"))
        {
          if(($Param{$i} != "\\")) $str .= $Param{$i};
          $i++;
          if($i >= strlen($Param)) return text_translate("Der letzte Parameter wurde nicht mit einem \"\"\" geschlossen.");
        }
        break;
      case("="):
        $key = $str;
        $str = "";
        break;
      case(" "):        
        if($key === "")
        { 
          if(!isset($ParamNames[$ai]))
          {
            if($FixedParams) return text_translate("Es wurden zu viele Parameter angegeben. maximal sind ?1? m&ouml;glich.", count($ParamNames));
            else return text_translate("Der Parameter \"?1?\" hat keinen Schl&uuml;sselnamen", $str);
          }
          $key = $ParamNames[$ai];
        }
        elseif($FixedParams and !in_array($key,$ParamNames)) return text_translate("Der Parametername \"?1?\" ist ung&uuml;ltig.", $key);
        if(isset($r[$key]) && !is_null($r[$key])) return text_translate("Der Parameter \"?1?\" wurde doppelt angegeben.", $key);
        $r[$key] = $str;
        $key = $str = "";
        $ai++;
        break;
      default: 
        $str .= $Param{$i};
    }
    $i++;
  }
  return $r;
}

/*
* Die Funktion JoinParam() arbeitet wie die Funktion ParseParam, blos r&uuml;ckw&auml;rts.
* @param array $Params ein Parameter-Array (z.B. array("href" => "index.php", "align" => "right"))
* @return string Die Parameter als String. (z.B. href="index.php" align="right")
*/

function JoinParam($Params)
{
  $p = array();
  foreach($Params as $k => $v) $p[] = "$k=\"".addcslashes($v,"\"")."\"";
  return implode(" ",$p);
}

?>