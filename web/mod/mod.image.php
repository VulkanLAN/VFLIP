<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: mod.image.php 1702 2019-01-09 09:01:12Z loom $ edit by VulkanLAN
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** Die Datei nur einmal includen */
if (defined("MOD.IMAGE.PHP"))
	return 0;
define("MOD.IMAGE.PHP", 1);

/** FLIP-Kern */
require_once ("core/core.php");

function GetSupportedImageTypes() {
	$types = array ();
	$t = imagetypes();
	if ($t & IMG_GIF)
		$types[] = "gif";
	if ($t & IMG_JPG)
		$types[] = "jpeg";
	if ($t & IMG_PNG)
		$types[] = "png";
	if ($t & IMG_WBMP)
		$types[] = "wbmp";
	return implode(", ", $types);
}

/* obsolete! */
function GetImageInfo($aImageFile) {
	$r = array ();
	$i = getimagesize($aImageFile);
	if (!is_array($i))
		return trigger_error_text("Fehler beim lesen von Bildinformationen.|file: $aImageFile", E_USER_WARNING);
	list ($r["width"], $r["height"], $r["format"]) = $i;
	if (empty ($i["mime"])) // der mime-type wird von php erst ab version 4.3 unterst&uuml;tzt
		{
		// um diese mime-liste zu erstellen, verwende folgenden code:
		// foreach(get_defined_constants() as $name => $id) 
		//   if(preg_match("/^IMAGETYPE.*$/i",$name)) $c[$id] = $name;    
		// for($i = 0; $i < 100; $i++) if(isset($c[$i]))
		//   echo "      case($i): \$r[\"mime\"] = \"".image_type_to_mime_type($i)."\"; break; // {$c[$i]}\n";
		switch ($r["format"]) {
			case (1) :
				$r["mime"] = "image/gif";
				break; // IMAGETYPE_GIF
			case (2) :
				$r["mime"] = "image/jpeg";
				break; // IMAGETYPE_JPEG
			case (3) :
				$r["mime"] = "image/png";
				break; // IMAGETYPE_PNG
			case (4) :
				$r["mime"] = "application/x-shockwave-flash";
				break; // IMAGETYPE_SWF
			case (5) :
				$r["mime"] = "image/psd";
				break; // IMAGETYPE_PSD
			case (6) :
				$r["mime"] = "image/bmp";
				break; // IMAGETYPE_BMP
			case (7) :
				$r["mime"] = "image/tiff";
				break; // IMAGETYPE_TIFF_II
			case (8) :
				$r["mime"] = "image/tiff";
				break; // IMAGETYPE_TIFF_MM
			case (9) :
				$r["mime"] = "image/jpeg";
				break; // IMAGETYPE_JPC
			case (10) :
				$r["mime"] = "application/octet-stream";
				break; // IMAGETYPE_JP2
			case (11) :
				$r["mime"] = "application/octet-stream";
				break; // IMAGETYPE_JPX
			case (12) :
				$r["mime"] = "application/octet-stream";
				break; // IMAGETYPE_JB2
			case (13) :
				$r["mime"] = "application/x-shockwave-flash";
				break; // IMAGETYPE_SWC
			case (14) :
				$r["mime"] = "image/iff";
				break; // IMAGETYPE_IFF
		}
	} else
		$r["mime"] = $i["mime"];
	return $r;
}

/* obsolete! */
function CreateImageFromFile($aFileName) {
	$a = GetImageInfo($aFileName);
	if (!is_array($a))
		return false;

	$res = NULL;
	$t = imagetypes();
	switch ($a["format"]) {
		case (1) :
			if ($t & IMG_GIF)
				$res = ImageCreateFromGIF($aFileName);
			break;
		case (2) :
			if ($t & IMG_JPEG)
				$res = ImageCreateFromJPEG($aFileName);
			break;
		case (3) :
			if ($t & IMG_PNG)
				$res = ImageCreateFromPNG($aFileName);
			break;
		case (15) :
			if ($t & IMG_WBMP)
				$res = ImageCreateFromWBMP($aFileName);
			break;
		default :
			return trigger_error_text("Unbekanntes Bildformat|file: $aFileName", E_USER_WARNING);
	}

	if (is_resource($res))
		return new Image($res);
	return trigger_error_text("Nicht unterst&uuml;tztes Bildformat|file: $aFileName", E_USER_WARNING);
}

/* obsolete! */
function CreateThumbnail($aImage, $MaxWidth, $MaxHeight) {
	if (!is_a($aImage, "image"))
		$aImage = CreateImageFromFile($isFile = $aImage);
	if (!is_a($aImage, "image"))
		return false;

	$h = $aImage->Height * (($w = $MaxWidth) / $aImage->Width);
	if ($h > $MaxHeight)
		$w = $aImage->Width * (($h = $MaxHeight) / $aImage->Height);

	$img = NULL;
	if (function_exists("imagecreatetruecolor") and function_exists("imagecopyresampled")) {
		old_Try();
		$img = @ imagecreatetruecolor($w, $h);
		if (is_resource($img))
			@ imagecopyresampled($img, $aImage->Res, 0, 0, 0, 0, $w, $h, $aImage->Width, $aImage->Height);
		if (old_Except(false))
			$img = NULL;
	}
	if (!is_resource($img)) {
		$img = imagecreate($w, $h);
		imagecopyresized($img, $aImage->Res, 0, 0, 0, 0, $w, $h, $aImage->Width, $aImage->Height);
	}
	if ($isFile)
		$aImage->destroy();
	return new Image($img);
}

function ImageExists($Ident) {
	$Ident = addslashes($Ident);
	$typ = (is_posDigit($Ident)) ? "id" : "name";
	$r = MysqlReadRow("SELECT `id` FROM `" . TblPrefix() . "flip_content_image` WHERE (`$typ` = '$Ident');", true);
	return (is_array($r)) ? true : false;
}

function IsGD2() {
	static $cache = NULL;
	if (!is_null($cache))
		return $cache;
	old_Try(); // versuche eine true-color bild zu erstellen, dies funzt nur mit gd2.
	$r = imagecreatetruecolor(10, 10);
	return $cache = (is_array(old_Except(false))) ? false : true;
}

function IsValidImage($FileName) {
	if (!is_file($FileName))
		return false;
	$i = @ getimagesize($FileName);
	if (!is_array($i))
		return false;
	return true;
}

class Image {
	var $Res = NULL;
	var $Width = 0;
	var $Height = 0;

	//php 7 public function __construct()
	//php 5 original function Image()
	function __construct($aResource) {
		$this->Res = $aResource;
		$this->Width = imagesx($aResource);
		$this->Height = imagesy($aResource);
	}

	function getPngData() {
		ob_start();
		imagepng($this->Res);
		$r = ob_get_contents();
		ob_end_clean();
		return $r;
	}

	function destroy() {
		imagedestroy($this->Res);
	}
}

class _AbstractImage {
	var $_Loader = NULL;

	//php 7 public function __construct()
	//php 5 original function _AbstractImage()
	function __construct($Img) {
		if (is_string($Img)) {
			if (preg_match("/^[0-9a-z_]+$/i", $Img))
				$this->_Loader = new ImageLoaderDB($Img);
			else
				$this->_Loader = new ImageLoaderFile($Img);
		}
		elseif (is_a($Img, "_AbstractImageLoader")) $this->_Loader = $Img;
		else
			trigger_error_text("Ein Imageloader ist ungueltig.|img: $Img(" . gettype($Img) . ")", E_USER_WARNING);
	}

	function getInfo($RaiseErrors = true) {
		return $this->_Loader->getInfo($RaiseErrors);
	}

	function getWidth() {
		$r = $this->_Loader->getInfo();
		return $r["width"];
	}

	function getHeight() {
		$r = $this->_Loader->getInfo();
		return $r["height"];
	}

	function getMime() {
		$r = $this->_Loader->getInfo();
		return $r["mime"];
	}

	function saveToDB($aIdent, $Param = array (), $CheckRights = true) {}
	function saveToFile($aFileName) {}
	function printData() {}
	function getData() {}
}

class DataImage extends _AbstractImage {
	//php 7 public function __construct()
	//php 5 original function DataImage()
	function __construct($Img) {
		//php 5:
		//parent :: _AbstractImage();
		//php7 neu:		
		parent :: __construct($Img);
	}

	function saveToDB($aIdent, $Param = array (), $CheckRights = true) {
		include_once ("mod/mod.imageedit.php");
		$i = new ResImage($this->_Loader);
		return _SaveImageToDB($aIdent, $this, $i->createThumbnail(100, 100), $Param, $CheckRights);
	}

	function saveToFile($aFileName) {
		return file_put_contents($aFileName, $this->_Loader->getData());
	}

	function printData() {
		$r = $this->_Loader->getInfo();
		header("Content-Type: $r[mime]");
		echo $this->_Loader->getData();
	}

	function getData() {
		return $this->_Loader->getData();
	}
}

class _AbstractImageLoader {
	var $_Info = array ();

	function getInfo($RaiseErrors = true) {
		if (empty ($this->_Info))
			$this->_Info = $this->loadInfo();
		if ($RaiseErrors and !empty ($this->_Info["error"]))
			trigger_error_text($this->_Info["error"], E_USER_WARNING);
		return $this->_Info;
	}

	function loadInfo() {}
	function getRes() {}
	function getData() {}
}

function _ErrorInfo($Msg, $Name = "", $Width = 0, $Height = 0, $Mime = "image/png") {
	return array (
		"error" => $Msg,
		"width" => $Width,
		"height" => $Height,
		"mime" => $Mime,
		"view_right" => 0,
		"edit_right" => 0,
	"edit_time" => time(), "caption" => "Error Image", "name" => escapeHtml($Name), "link" => "");
}

function _getPngData($Res) {
	ob_start();
	imagepng($Res);
	$r = ob_get_contents();
	ob_end_clean();
	return $r;
}

class ImageLoaderDB extends _AbstractImageLoader {
	var $_Name;
	var $_CheckViewRight;

	//php 7 public function __construct()
	//php 5 original function ImageLoaderDB()
	function __construct($aName, $CheckViewRight = true) {
		$this->_Name = $aName;
		$this->_CheckViewRight = $CheckViewRight;
	}

	function loadInfo() {
		global $User;
		$ident = addslashes($this->_Name);
		$dat = CacheGet("imageinfo_$ident");
		$typ = (is_posDigit($ident)) ? "id" : "name";
		if (!is_array($dat))
			$dat = CacheSet("imageinfo_$ident", MysqlReadRow("
			        SELECT `caption`,`name`,`width`,`height`,`view_right`,`edit_right`,`edit_time`,`mime` FROM `" . TblPrefix() . "flip_content_image` 
			        WHERE (`$typ` = '$ident');
			      ", true), array (
				"flip_content_image"
			));

		if ($this->_CheckViewRight)
			if (!$User->hasRight($dat["view_right"]))
				return _ErrorInfo("Zugriff verweigert.", $this->_Name, 150, 100, "image/png");
		if (is_array($dat)) {
			$dat["link"] = "image.php?name={$this->_Name}";
			return $dat;
		}
		return _ErrorInfo("Das Bild mit dem Namen \"{$this->_Name}\" existiert nicht.", $this->_Name, 150, 100, "image/png");
	}

	function _getErrorRes() {
		$im = ImageCreate(150, 100);
		imagecolortransparent($im, ImageColorAllocate($im, 255, 255, 255));
		$text_color = ImageColorAllocate($im, 255, 0, 0);
		ImageString($im, 5, 5, 5, "Fehler: Das Bild mit\ndem Namen", $text_color);
		ImageString($im, 5, 5, 20, "\"{$this->_Name}\"", $text_color);
		ImageString($im, 5, 5, 35, "existiert nicht.\"", $text_color);
		return $im;
	}

	function _loadData() {
		$ident = addslashes($this->_Name);
		$info = $this->getInfo(false);
		if (isset ($info["error"]))
			return false;
		$dat = CacheGet("image_$ident", $info["edit_time"]);
		$typ = (is_posDigit($ident)) ? "id" : "name";
		if (!is_array($dat))
			$dat = CacheSet("image_$ident", MysqlReadRow("
			        SELECT `data` FROM `" . TblPrefix() . "flip_content_image` 
			          WHERE (`$typ` = '$ident');
			      ", true));
		if (is_array($dat))
			return $dat["data"];
		return false;
	}

	function getRes() {
		$dat = $this->_loadData();
		if ($dat)
			return imagecreatefromstring($dat);
		else
			return $this->_getErrorRes();
	}

	function getData() {
		$dat = $this->_loadData();
		if ($dat)
			return $dat;
		else
			return _getPngData($this->_getErrorRes());
	}
}

class ImageLoaderFile extends _AbstractImageLoader {
	var $_FileName;

	//php 7 public function __construct()
	//php 5 original function ImageLoaderFile()
	function __construct($aFileName) {
		$this->_FileName = $aFileName;
	}

	function loadInfo() {
		$r = array (
			'edit_time' => filemtime($this->_FileName
		), 'view_right' => 0, 'edit_right' => 0, 'caption' => '', 'name' => $this->_FileName, 'link' => $this->_FileName, 'format' => 0, 'isswf' => false);
		$i = getimagesize($this->_FileName);
		if (!is_array($i))
			return _ErrorInfo("Fehler beim lesen von Bildinformationen.|file: {$this->_FileName}");
		list ($r['width'], $r['height'], $r['format']) = $i;
		if (empty ($i['mime'])) // der mime-type wird von php erst ab version 4.3 unterst&uuml;tzt
			{
			// um diese mime-liste zu erstellen, verwende folgenden code:
			// foreach(get_defined_constants() as $name => $id) 
			//   if(preg_match("/^IMAGETYPE.*$/i",$name)) $c[$id] = $name;    
			// for($i = 0; $i < 100; $i++) if(isset($c[$i]))
			//   echo "      case({$c[$i]}): \$r[\"mime\"] = \"".image_type_to_mime_type($i)."\"; break; // $i\n";
			switch ($r['format']) {
				case (IMAGETYPE_GIF) :
					$r['mime'] = 'image/gif';
					break; // 1
				case (IMAGETYPE_JPEG) :
					$r['mime'] = 'image/jpeg';
					break; // 2
				case (IMAGETYPE_PNG) :
					$r['mime'] = 'image/png';
					break; // 3
				case (IMAGETYPE_SWF) :
					$r['mime'] = 'application/x-shockwave-flash';
					break; // 4
				case (IMAGETYPE_PSD) :
					$r['mime'] = 'image/psd';
					break; // 5
				case (IMAGETYPE_BMP) :
					$r['mime'] = 'image/bmp';
					break; // 6
				case (IMAGETYPE_TIFF_II) :
					$r['mime'] = 'image/tiff';
					break; // 7
				case (IMAGETYPE_TIFF_MM) :
					$r['mime'] = 'image/tiff';
					break; // 8
				case (IMAGETYPE_JPEG2000) :
					$r['mime'] = 'application/octet-stream';
					break; // 9
				case (IMAGETYPE_JP2) :
					$r['mime'] = 'image/jp2';
					break; // 10
				case (IMAGETYPE_JPX) :
					$r['mime'] = 'application/octet-stream';
					break; // 11
				case (IMAGETYPE_JB2) :
					$r['mime'] = 'application/octet-stream';
					break; // 12
				case (IMAGETYPE_SWC) :
					$r['mime'] = 'application/x-shockwave-flash';
					break; // 13
				case (IMAGETYPE_IFF) :
					$r['mime'] = 'image/iff';
					break; // 14
				case (IMAGETYPE_WBMP) :
					$r['mime'] = 'image/vnd.wap.wbmp';
					break; // 15
				case (IMAGETYPE_XBM) :
					$r['mime'] = 'image/xbm';
					break; // 16
				default :
					$r['mime'] = 'application/octet-stream';
			}
		} else
			$r['mime'] = $i['mime'];
		$r['isswf'] = (in_array($r['format'], array (IMAGETYPE_SWF, IMAGETYPE_SWC))) ? true : false;
		return $r;
	}

	function getRes() {
		$i = $this->getInfo();
		if (!empty ($i['error'])) {
			trigger_error_text("Fehler beim lesen von Bildinformationen um Resourcentyp festzustellen.|file: $this->_FileName", E_USER_WARNING);
			return false;
		}
		$res = NULL;
		$t = imagetypes();
		switch ($i["format"]) {
			case (IMAGETYPE_GIF) :
				if ($t & IMG_GIF)
					$res = ImageCreateFromGIF($this->_FileName);
				break;
			case (IMAGETYPE_JPEG) :
				if ($t & IMG_JPEG)
					$res = ImageCreateFromJPEG($this->_FileName);
				break;
			case (IMAGETYPE_PNG) :
				if ($t & IMG_PNG)
					$res = ImageCreateFromPNG($this->_FileName);
				break;
			case (IMAGETYPE_WBMP) :
				if ($t & IMG_WBMP)
					$res = ImageCreateFromWBMP($this->_FileName);
				break;
		}
		if (is_resource($res))
			return $res;
		trigger_error_text('Unbekanntes Bildformat oder beschaedigte Bilddatei|file:' . $this->_FileName, E_USER_WARNING);
		return NULL;
	}

	function getData() {
		return file_get_contents($this->_FileName);
	}
}

class ImageLoaderDBField extends _AbstractImageLoader {
	var $_Loader = false;
	var $_TableName;
	var $_ColName;
	var $_ID;
	var $_MTime;
	var $_Processor;

	//php 7 public function __construct()
	//php 5 original function ImageLoaderDBField()
	function __construct($TableName, $ColName, $ID, $MTime, $Processor = null) {
		global $CoreConfig;
		if (empty ($CoreConfig["image_tmp"]))
			trigger_error_text("Ein Eintrag in der CoreConfig fehlt.| Eintrag: \$CoreConfig[\"image_tmp\"], siehe core.config.default.php", E_USER_ERROR);
		$this->_TableName = $TableName;
		$this->_ColName = $ColName;
		$this->_ID = $ID;
		$this->_MTime = $MTime;
		$this->_Processor = $Processor;
	}

	function _GetTmpNameIfExists($Name, $MTime) {
		global $CoreConfig;
		static $files = array ();
		if (empty ($files)) {
			$d = opendir($CoreConfig["image_tmp"]);
			while ($f = readdir($d)) {
				$a = explode(".", $f);
				$files[$a[0]] = $CoreConfig["image_tmp"] . $f;
			}
			closedir($d);
		}
		$file = (isset ($files[$Name])) ? $files[$Name] : "";
		clearstatcache();
		if (empty ($file) or !is_file($file))
			return false; // die Datei existiert nicht
		if ((filemtime($file) <= toTimestamp($MTime)) and (toTimestamp($MTime) != false)) {
			unlink($file);
			unset ($files[$Name]);
			return false; // die Version in der Datenbank ist aktueller als die Datei
		}
		return $file;
	}

	function _GetExtByType($FileName) {
		$i = getimagesize($FileName);
		if (!is_array($i))
			return false;
		switch ($i[2]) {
			case (1) :
				return "gif"; // IMAGETYPE_GIF
			case (2) :
				return "jpg"; // IMAGETYPE_JPEG
			case (3) :
				return "png"; // IMAGETYPE_PNG
			case (4) :
				return "swf"; // IMAGETYPE_SWF
			case (5) :
				return "psd"; // IMAGETYPE_PSD
			case (6) :
				return "bmp"; // IMAGETYPE_BMP
			case (7) :
				return "tif"; // IMAGETYPE_TIFF_II
			case (8) :
				return "tif"; // IMAGETYPE_TIFF_MM
			case (9) :
				return "jpg"; // IMAGETYPE_JPC
			case (10) :
				return "jp2"; // IMAGETYPE_JP2
			case (11) :
				return "jpx"; // IMAGETYPE_JPX
			case (12) :
				return "jb2"; // IMAGETYPE_JB2
			case (13) :
				return "swf"; // IMAGETYPE_SWC
			case (14) :
				return "iff"; // IMAGETYPE_IFF
			default :
				return false;
		}
	}

	function _loadLoader() {
		global $CoreConfig;
		$isproc = (is_a($this->_Processor, "AbstractImageProcessor")) ? true : false;
		$proc = ($isproc) ? $this->_Processor->getHash() : "";
		$tmpname = md5($this->_TableName . $this->_ColName . $this->_ID . $proc) . "-img";
		$file = $this->_GetTmpNameIfExists($tmpname, $this->_MTime);
		if (empty ($file)) {
			$tmp = $CoreConfig["image_tmp"] . $tmpname;
			$dat = MysqlReadFieldByID($this->_TableName, $this->_ColName, $this->_ID);
			if (empty ($dat)) // leere Dateien f&uuml;r leere Datens&auml;tze
				{
				$file = "$tmp.empty";
				file_put_contents($file, "\x00");
			} else {
				if ($isproc)
					$dat = $this->_Processor->processData($dat);
				$written = file_put_contents($tmp, $dat);
				clearstatcache();
				if (($written > 0) and is_file($tmp)) {
					$ext = $this->_GetExtByType($tmp);
					if (empty ($ext)) {
						unlink($tmp);
						return "Ein Bildformat ist unbekannt oder besch&auml;digt.|table:{$this->_TableName} col:{$this->_ColName} id:{$this->_ID}";
					}
					$file = "$tmp.$ext";
					if (is_file($file))
						unlink($file);
					rename($tmp, $file);
				} else
					return "Eine Bilddatei konnte nicht auf der Festplatte zwischengespeichert werden.|file:$tmp table:{$this->_TableName} col:{$this->_ColName} id:{$this->_ID} size:" . strlen($dat);
			}
		}
		if (filesize($file) > 1)
			$this->_Loader = new ImageLoaderFile($file);
		return "";
	}

	function loadInfo() {
		if (!is_object($this->_Loader))
			$r = $this->_loadLoader();
		if (!empty ($r))
			return _ErrorInfo($r);
		if (is_object($this->_Loader))
			return $this->_Loader->loadInfo();
		else
			return array (
				"width" => 0,
				"height" => 0
			);
	}

	function getRes() {
		if (!is_object($this->_Loader))
			$r = $this->_loadLoader();
		if (!empty ($r)) {
			trigger_error_text($r, E_USER_WARING);
			return NULL;
		}
		if (is_object($this->_Loader))
			return $this->_Loader->getRes();
		else
			return NULL;
	}

	function getData() {
		if (!is_object($this->_Loader))
			$r = $this->_loadLoader();
		if (!empty ($r)) {
			trigger_error_text($r, E_USER_WARING);
			return NULL;
		}
		if (is_object($this->_Loader))
			return $this->_Loader->getData();
		else
			return NULL;
	}
}

function sqlImage($Table, $Field) {
	return "IF(LENGTH(`$Field`) > 0, CONCAT('$Table:$Field:',`id`,':',`mtime`), 0) AS `$Field`";
}

class ImageLoaderNew extends _AbstractImageLoader {
	var $_Width;
	var $_Height;
	var $_Res;

	//php 7 public function __construct()
	//php 5 original function ImageLoaderNew()
	function __construct($aWidth, $aHeight, $TrueColor = true) {
		if (IsGD2() and $TrueColor)
			$this->_Res = imagecreatetruecolor($aWidth, $aHeight);
		else
			$this->_Res = imagecreate($aWidth, $aHeight);
		$this->_Width = $aWidth;
		$this->_Height = $aHeight;
	}

	function loadInfo() {
		return array (
			"width" => $this->_Width,
			"height" => $this->_Height,
			"mime" => "image/png",
			"view_right" => 0,
			"edit_right" => 0,
		"edit_time" => time(), "caption" => "New Image", "name" => "new_image");
	}

	function getRes() {
		return $this->_Res;
	}

	function getData() {
		return _getPngData($this->_Res);
	}
}

class ImageLoaderRes extends _AbstractImageLoader {
	var $_Width;
	var $_Height;
	var $_Res;

	//php 7 public function __construct()
	//php 5 original function ImageLoaderRes()
	function __construct($aResource) {
		$this->_Res = $aResource;
		$this->_Width = imagesx($aResource);
		$this->_Height = imagesy($aResource);
	}

	function loadInfo() {
		return array (
			"width" => $this->_Width,
			"height" => $this->_Height,
			"mime" => "image/png",
			"view_right" => 0,
			"edit_right" => 0,
		"edit_time" => time(), "caption" => "Res Image", "name" => "res_image");
	}

	function getRes() {
		return $this->_Res;
	}

	function getData() {
		return _getPngData($this->_Res);
	}
}

class AbstractImageProcessor {
	function processData($data) {
		include_once ("mod/mod.imageedit.php");
		$img = new ResImage(new ImageLoaderRes(imagecreatefromstring($data)));
		$img = $this->processImage($img);
		return $img->getData();
	}

	function processImage($image) {
		return $image;
	}

	function getHash() {
		trigger_error_text("Ein ImageProcessor implementiert nicht die getHash()-Methode", E_USER_ERROR);
		return "";
	}
}

class ImageProcessorCode extends AbstractImageProcessor {

	var $_Code;

	//php 7 public function __construct()
	//php 5 original function ImageProcessorCode()
	function __construct($code) {
		$this->_Code = $code;
	}

	function processImage($image) {
		$code = explode(';', $this->_Code);
		foreach ($code as $cmd) {
			eval ("\$image=\$image->$cmd;");
		}
		return $image;
	}

	function getHash() {
		return $this->_Code;
	}
}
?>
