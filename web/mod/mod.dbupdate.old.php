<?php
/**
 * DBUpdate
 * Aktualisiert die Datenbank
 * Von der bestehenden Version ausgehend wird auf die in core.php
 * mittels DB_REQUIRED_VERSION geforderte Version geupdatet.
 *
 * Stichs&auml;tze:
 * - jede case(xxx) Anweisung gibt einen Stand der flip.sql wieder und was nach
 *   dieser ge&auml;ndert wurde
 * - das letzte case(xxx) beinhaltet _DBUUpdateEnd(xxy) mit der aktuellen
 *   versionsnumer (der flip.sql)
 * - somit sollte jedes neue case(xxx) mit dem vorherigen _DBUUpdateEnd(xxx)
 *   &uuml;bereinstimmen
 *
 * @author Moritz Eysholdt
 * @version $Id: mod.time.php 536 2004-10-12 22:38:17Z docfx $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** Funktionen f&uuml;r das DBUpdate */
require_once("mod/mod.dbupdate.utils.php");

function DBUpdateToRev1506()
{
	$DBRevision = _DBUUpdateStart();

	// updatecode hier...
	switch($DBRevision)
	{
		case(573):
			_DBUExecQuery("
            CREATE TABLE `".TblPrefix()."flip_sponsor_ads` (
              `id` int(11) NOT NULL auto_increment,
              `mtime` timestamp(14) NOT NULL,
              `type` enum('banner','button') NOT NULL default 'banner',
              `sponsor_id` int(11) NOT NULL default '0',
              `image` varchar(255) NOT NULL default '',
              `enabled` tinyint(4) NOT NULL default '0',
              `priority` int(11) NOT NULL default '0',
              `clicks` int(11) NOT NULL default '0',
              `views` int(11) NOT NULL default '0',
              `enable_view_log` tinyint(4) NOT NULL default '0',
              `enable_click_log` tinyint(4) NOT NULL default '0',
              `last_click_reset` int(11) NOT NULL default '0',
              `last_view_reset` int(11) NOT NULL default '0',
              PRIMARY KEY  (`id`),
              KEY `mtime` (`mtime`)
            ) TYPE=MyISAM;
            ", "Tabelle ".TblPrefix()."flip_sponsor_ads hinzugef&uuml;gt.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_sponsors` CHANGE `public` `public` TINYINT NOT NULL", "In ".TblPrefix()."flip_sponsor_sponsors Spalte 'public' zu Tinyint gemacht.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_sponsors` CHANGE `name` `name` VARCHAR( 128 ) NOT NULL", "In ".TblPrefix()."flip_sponsor_sponsors Spalte 'name' zu VarChar(128) gemacht.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_sponsors` CHANGE `homepage` `homepage` VARCHAR( 255 ) DEFAULT NULL", "In ".TblPrefix()."flip_sponsor_sponsors Spalte 'homepage' zu VarChar(255) gemacht.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_sponsors` CHANGE `email` `email` VARCHAR( 255 ) DEFAULT NULL", "In ".TblPrefix()."flip_sponsor_sponsors Spalte 'email' zu VarChar(255) gemacht.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_sponsors` CHANGE `status` `status` VARCHAR( 32 ) DEFAULT '0' NOT NULL", "In ".TblPrefix()."flip_sponsor_sponsors Spalte 'status' zu VarChar(32) gemacht.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_sponsors` CHANGE `priority` `priority` VARCHAR( 32 ) DEFAULT '5' NOT NULL ", "In ".TblPrefix()."flip_sponsor_sponsors Spalte 'priority' zu VarChar(32) gemacht.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_sponsors` DROP `logo`", "Spalte 'logo' aus ".TblPrefix()."flip_sponsor_sponsors entfernt.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_sponsors` ADD `image` BLOB", "In ".TblPrefix()."flip_sponsor_sponsors Spalte 'image' hinzugef&uuml;gt.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_sponsors` ADD `phone` VARCHAR( 64 ) NOT NULL", "In ".TblPrefix()."flip_sponsor_sponsors Spalte 'phone' hinzugef&uuml;gt.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_sponsors` ADD `street` VARCHAR( 128 ) NOT NULL", "In ".TblPrefix()."flip_sponsor_sponsors Spalte 'phone' hinzugef&uuml;gt.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_sponsors` ADD `city` VARCHAR( 128 ) NOT NULL", "In ".TblPrefix()."flip_sponsor_sponsors Spalte 'phone' hinzugef&uuml;gt.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_sponsors` ADD `contact` VARCHAR( 128 ) NOT NULL", "In ".TblPrefix()."flip_sponsor_sponsors Spalte 'phone' hinzugef&uuml;gt.");
			$stat = _DBUExecInsert(TblPrefix()."flip_table_tables",array("name"        =>"sponsor_status",
						"edit_right"  =>12,
						"description" =>"Bearbeitung-Status in der Sponsorenliste"
						), "Tabelle sponsor_status in ".TblPrefix()."flip_table_tables hinzugef&uuml;gt");
			$prio = _DBUExecInsert(TblPrefix()."flip_table_tables",array("name"        =>"sponsor_priority",
						"edit_right"  =>12,
						"description" =>"Die Priorit&auml;ten der Sponsoren"
						), "Tabelle sponsor_priority in flip_table_tables hinzugef&uuml;gt");
			_DBUExecInsert(TblPrefix()."flip_table_entry",array("table_id"     => $stat,
						"key"          => "0",
						"value"        => "Nicht bearbeitet",
						"display"      => "Nicht bearbeitet",
						"display_code" => "Nicht bearbeitet"
						), "Sponsorstatus Nicht bearbeitet hinzugef&uuml;gt.");
			_DBUExecInsert(TblPrefix()."flip_table_entry",array("table_id"     => $stat,
						"key"          => "1",
						"value"        => "Angefragt",
						"display"      => "Angefragt",
						"display_code" => "Angefragt"
						), "Sponsorstatus Angefragt hinzugef&uuml;gt.");
			_DBUExecInsert(TblPrefix()."flip_table_entry",array("table_id"     => $stat,
						"key"          => "2",
						"value"        => "Absage",
						"display"      => "Absage",
						"display_code" => "Absage"
						), "Sponsorstatus Absage hinzugef&uuml;gt.");
			_DBUExecInsert(TblPrefix()."flip_table_entry",array("table_id"     => $stat,
						"key"          => "3",
						"value"        => "Zusage",
						"display"      => "Zusage",
						"display_code" => "Zusage"
						), "Sponsorstatus Zusage hinzugef&uuml;gt.");
			_DBUExecInsert(TblPrefix()."flip_table_entry",array("table_id"     => $stat,
						"key"          => "4",
						"value"        => "in Verhandlung",
						"display"      => "in Verhandlung",
						"display_code" => "in Verhandlung"
						), "Sponsorstatus in Verhandlung hinzugef&uuml;gt.");
			_DBUExecInsert(TblPrefix()."flip_table_entry",array("table_id"     => $prio,
						"key"          => "0",
						"value"        => "0 - keine",
						"display"      => "0 - keine",
						"display_code" => "0 - keine"
						), "Sponsorpriorit&auml;t 0 - keine hinzugef&uuml;gt.");
			_DBUExecInsert(TblPrefix()."flip_table_entry",array("table_id"     => $prio,
						"key"          => "1",
						"value"        => "1 - unwichtig",
						"display"      => "1 - unwichtig",
						"display_code" => "1 - unwichtig"
						), "Sponsorpriorit&auml;t 1 - unwichtig hinzugef&uuml;gt.");
			_DBUExecInsert(TblPrefix()."flip_table_entry",array("table_id"     => $prio,
						"key"          => "2",
						"value"        => "2 - gering",
						"display"      => "2 - gering",
						"display_code" => "2 - gering"
						), "Sponsorpriorit&auml;t 2 - gering hinzugef&uuml;gt.");
			_DBUExecInsert(TblPrefix()."flip_table_entry",array("table_id"     => $prio,
						"key"          => "3",
						"value"        => "3 - niedrig",
						"display"      => "3 - niedrig",
						"display_code" => "3 - niedrig"
						), "Sponsorpriorit&auml;t 3 - niedrig hinzugef&uuml;gt.");
			_DBUExecInsert(TblPrefix()."flip_table_entry",array("table_id"     => $prio,
						"key"          => "4",
						"value"        => "4 - gem&auml;&szlig;igt",
						"display"      => "4 - gem&auml;&szlig;igt",
						"display_code" => "4 - gem&auml;&szlig;igt"
						), "Sponsorpriorit&auml;t 4 - gem&auml;&szlig;igt hinzugef&uuml;gt.");
			_DBUExecInsert(TblPrefix()."flip_table_entry",array("table_id"     => $prio,
						"key"          => "5",
						"value"        => "5 - mittlere",
						"display"      => "5 - mittlere",
						"display_code" => "5 - mittlere"
						), "Sponsorpriorit&auml;t 5 - mittlere hinzugef&uuml;gt.");
			_DBUExecInsert(TblPrefix()."flip_table_entry",array("table_id"     => $prio,
						"key"          => "6",
						"value"        => "6 - gehobene",
						"display"      => "6 - gehobene",
						"display_code" => "6 - gehobene"
						), "Sponsorpriorit&auml;t 6 - gehobene hinzugef&uuml;gt.");
			_DBUExecInsert(TblPrefix()."flip_table_entry",array("table_id"     => $prio,
						"key"          => "7",
						"value"        => "7 - hoch",
						"display"      => "7 - hoch",
						"display_code" => "7 - hoch"
						), "Sponsorpriorit&auml;t 7 - hoch hinzugef&uuml;gt.");
			_DBUExecInsert(TblPrefix()."flip_table_entry",array("table_id"     => $prio,
						"key"          => "8",
						"value"        => "8 - sehr hoch",
						"display"      => "8 - sehr hoch",
						"display_code" => "8 - sehr hoch"
						), "Sponsorpriorit&auml;t 8 - sehr hoch hinzugef&uuml;gt.");
			_DBUExecInsert(TblPrefix()."flip_table_entry",array("table_id"     => $prio,
						"key"          => "9",
						"value"        => "9 - sehr wichtig",
						"display"      => "9 - sehr wichtig",
						"display_code" => "9 - sehr wichtig"
						), "Sponsorpriorit&auml;t 9 - sehr wichtig hinzugef&uuml;gt.");
			_DBUAddRight("sponsor_view_internal", "Erlaubt es, den internen Teil der Sponsorenliste ein zu sehen.", "orga");

		case(581):
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_tournament_tournaments` ADD `icon` BLOB", "In ".TblPrefix()."flip_tournament_tournaments Spalte 'icon' hinzugef&uuml;gt.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_tournament_tournaments` ADD `icon_small` BLOB", "In ".TblPrefix()."flip_tournament_tournaments Spalte 'icon_small' hinzugef&uuml;gt.");

		case(583):
			_DBUAddRight("sysinfo_view", "Erlaubt es, Sysinfo ein zu sehen.", "webmaster");
			_DBUExecQuery("DELETE FROM ".TblPrefix()."flip_user_rights WHERE id='164'","Recht view_internal_profil &uuml;ber Anonymous entfernt.");

		case(590):
			_DBUUpdateMenu("webmaster","Sysinfo","sysinfo.php","sysinfo_view","Zeigt Systeminformationen an.");
			 
		case(591):
			_DBUExecQuery("DELETE FROM ".TblPrefix()."flip_config WHERE `key`='tournament_defaultgroup'", "ConfigOption tournament_defaultgroup entfernt.");
			_DBUSetConfig("tournament_defaultwinbytimeout",0,"soll wenn die Zeit abgelaufen ist automatisch ein Team zuf&auml;llig als Gewinner eingetragen werden? (0/1)");

		case(601):
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_menu_links` ADD `enabled` TINYINT DEFAULT '1' NOT NULL AFTER `use_new_wnd`", "Spalte 'enabled' zu ".TblPrefix()."flip_menu_links hinzugef&uuml;gt.");

		case(602):
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_ads` CHANGE `image` `image` BLOB NOT NULL ", "In ".TblPrefix()."flip_sponsor_ads Spalte 'image' zu blob gemacht.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_ads` ADD `link` VARCHAR( 255 ) AFTER `priority`", "Spalte 'link' zu ".TblPrefix()."flip_sponsor_ads hinzugef&uuml;gt.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_ads` ADD `sessions` INT NOT NULL AFTER `views`", "Spalte 'sessions' zu ".TblPrefix()."flip_sponsor_ads hinzugef&uuml;gt.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_ads` CHANGE `last_click_reset` `enable_session_log` TINYINT DEFAULT '0' NOT NULL", "In ".TblPrefix()."flip_sponsor_ads Spalte 'last_click_reset' in 'enable_session_log' umbenannt.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_ads` CHANGE `last_view_reset` `last_counter_reset` INT( 11 ) DEFAULT '0' NOT NULL", "In ".TblPrefix()."flip_sponsor_ads Spalte 'last_view_reset' in 'last_counter_reset' umbenannt.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_ads` ADD INDEX ( `type` )", "In ".TblPrefix()."flip_sponsor_ads Spalte 'type' zu Index gemacht.");
			_DBUExecQuery("DROP TABLE IF EXISTS `".TblPrefix()."flip_sponsor_log`", "Tabelle ".TblPrefix()."flip_sponsor_log gel&ouml;scht falls vorhanden.");
			_DBUExecQuery("
            CREATE TABLE `".TblPrefix()."flip_sponsor_log` (
              `id` int(11) NOT NULL auto_increment,
              `mtime` timestamp(14) NOT NULL,
              `type` enum('view','session','click') NOT NULL default 'view',
              `ad_id` int(11) NOT NULL default '0',
              `ip` varchar(15) NOT NULL default '',
              PRIMARY KEY  (`id`)
            ) TYPE=MyISAM;
            ", "Tabelle ".TblPrefix()."flip_sponsor_log hinzugef&uuml;gt.");
		case(606):

			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_log` CHANGE `type` `reason` ENUM( 'view', 'session', 'click' ) DEFAULT 'view' NOT NULL", "In ".TblPrefix()."flip_sponsor_log Spalte 'type' in 'reason' umbenannt.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_log` ADD `session_id` VARCHAR( 64 ) AFTER `ad_id`", "Spalte 'session_id' zu ".TblPrefix()."flip_sponsor_log hinzugef&uuml;gt.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_log` CHANGE `ip` `ip` VARCHAR( 15 )", "In ".TblPrefix()."flip_sponsor_log der Spalte 'ip' NULL erlaubt.");
			_DBUAddRight("sponsor_no_banner", "Besitzer dieses Rechts sehen keine Sponsorenbanner", null);

		case(607):
			_DBUExecQuery("DROP TABLE IF EXISTS `".TblPrefix()."flip_tournament_servers`", "Tabelle ".TblPrefix()."flip_tournament_servers gel&ouml;scht falls vorhanden.");
			_DBUExecQuery("
            CREATE TABLE `".TblPrefix()."flip_tournament_servers` (
              `id` int(11) NOT NULL auto_increment,
              `game` varchar(4) NOT NULL default '',
              `ip` varchar(21) NOT NULL default '',
              `name` varchar(63) NOT NULL default '',
              PRIMARY KEY  (`id`),
              KEY `game` (`game`)
            ) TYPE=MyISAM;
            ", "Tabelle ".TblPrefix()."flip_tournament_servers hinzugef&uuml;gt.");
			_DBUExecQuery("DROP TABLE IF EXISTS `".TblPrefix()."flip_tournament_serverusage`", "Tabelle ".TblPrefix()."flip_tournament_serverusage gel&ouml;scht falls vorhanden.");
			_DBUExecQuery("
            CREATE TABLE `".TblPrefix()."flip_tournament_serverusage` (
              `id` int(11) NOT NULL auto_increment,
              `server_id` int(11) NOT NULL default '0',
              `match_id` int(11) NOT NULL default '0',
              PRIMARY KEY  (`id`),
              KEY `match_id` (`match_id`)
            ) TYPE=MyISAM;
            ", "Tabelle ".TblPrefix()."flip_tournament_servers hinzugef&uuml;gt.");

		case(611):
			_DBUExecInsert(TblPrefix()."flip_menu_blocks",array("caption"        =>"Partner",
						"description"    =>"unsere Partner",
						"view_right"    =>0,
						"parent_item_id"    =>0,
						"level_index"    =>1,
						"frameurl"    =>"sponsor.php?frame=smallbuttons",
						"order"        =>1100542804
			), "Men&uuml;block Partner hinzugef&uuml;gt.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_ads` DROP `enabled`", "Spalte 'enabled' aus ".TblPrefix()."flip_sponsor_eds entfernt.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_ads` ADD `show_as_ad` TINYINT NOT NULL AFTER `image`", "Spalte 'show_as_ad' zu ".TblPrefix()."flip_sponsor_ads hinzugef&uuml;gt.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_ads` ADD `show_in_list` TINYINT NOT NULL AFTER `show_as_ad`", "Spalte 'show_in_list' zu ".TblPrefix()."flip_sponsor_ads hinzugef&uuml;gt.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_log` ADD `location` VARCHAR(32)", "Spalte 'location' zu ".TblPrefix()."flip_sponsor_log hinzugef&uuml;gt.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_sponsors` DROP `image`", "Spalte 'image' aus ".TblPrefix()."flip_sponsor_sponsors entfernt.");

		case(612):
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_ads` ADD `enable_counter` TINYINT NOT NULL AFTER `enable_session_log`", "Spalte 'enable_counter' zu ".TblPrefix()."flip_sponsor_ads hinzugef&uuml;gt.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_sponsors` ADD `last_edit_user` INT NOT NULL", "Spalte 'last_edit_user' zu ".TblPrefix()."flip_sponsor_sponsors hinzugef&uuml;gt.");

		case(613):
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_user_subject` DROP INDEX `nickname` , ADD UNIQUE `nickname` ( `type` , `name` )","'type' zu Index nickname in ".TblPrefix()."flip_user_subject hinzugef&uuml;gt.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_user_subject` DROP INDEX `email` , ADD UNIQUE `email` ( `email` , `type` )","'type' zu Index email in ".TblPrefix()."flip_user_subject hinzugef&uuml;gt.");

		case(622):
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_user_subject` DROP INDEX `nickname` , ADD UNIQUE `nickname` ( `name` )","'type' aus Index nickname in ".TblPrefix()."flip_user_subject entfernt.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_user_subject` DROP INDEX `email` , ADD UNIQUE `email` ( `email` )","'type' aus Index email in ".TblPrefix()."flip_user_subject entfernt.");

		case(623):
			_DBUSetConfig("page_compress_output",1,"(1|0) Komprimiert die HTML-Seiten, bevor sie an den Browser gesendet werden. (Wenn der Browser dies unterst&uuml;tzt)");
			_DBUUpdateByKey(TblPrefix()."flip_config",array("default_value"=>0),array("key"=>"page_compress_output"),"Defaultwert des Configeintrags page_compress_output auf 0 gesetzt.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>8),array("id"=>"5"),"'sort_index' von Email-Adresse angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>9),array("id"=>"7"),"'sort_index' von Passwort angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>10),array("id"=>"8"),"'sort_index' von Homepage angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>13),array("id"=>"9"),"'sort_index' von Strasse angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>14),array("id"=>"10"),"'sort_index' von Stadt angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>15),array("id"=>"11"),"'sort_index' von PLZ angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>16),array("id"=>"12"),"'sort_index' von Telefon angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>17),array("id"=>"13"),"'sort_index' von Handy angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>1061404876),array("id"=>"14"),"'sort_index' von Bemerkung angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>18),array("id"=>"15"),"'sort_index' von Zeitpunkt der Registrierung angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>19),array("id"=>"16"),"'sort_index' von Letzter Login angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>21),array("id"=>"17"),"'sort_index' von ICQ angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>24),array("id"=>"18"),"'sort_index' von AIM angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>25),array("id"=>"19"),"'sort_index' von MSN angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>26),array("id"=>"20"),"'sort_index' von Yahoo angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>28),array("id"=>"24"),"'sort_index' von WWCL-Player-ID angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>353453),array("id"=>"25"),"'sort_index' von vollj&auml;hrig angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>30),array("id"=>"28"),"'sort_index' von Beschreibung(Gruppe) angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>11),array("id"=>"29"),"'sort_index' von Geburtsdatum angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>5345345),array("id"=>"30"),"'sort_index' von ZufallsID angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>353453),array("id"=>"31"),"'sort_index' von WWCL-ID(Turnierteam) angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>20),array("id"=>"34"),"'sort_index' von Jabber-ID (JID) angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>8),array("id"=>"36"),"'sort_index' von E-Mail Adresse angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>1068224504),array("id"=>"49"),"'sort_index' von Template angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>106830704),array("id"=>"50"),"'sort_index' von Posts/Seite angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>1063927068),array("id"=>"51"),"'sort_index' von Status angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>1068223310),array("id"=>"52"),"'sort_index' von StatusName angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>1063571857),array("id"=>"54"),"'sort_index' von Sitzplatz angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>1068223320),array("id"=>"55"),"'sort_index' von IP-Adresse angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>29),array("id"=>"56"),"'sort_index' von WWCL-Clan-ID angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>1061404876),array("id"=>"58"),"'sort_index' von Bezeichnung(Server) angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>13),array("id"=>"60"),"'sort_index' von Diensts(Server) angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>1093297497),array("id"=>"65"),"'sort_index' von Hardware-Barcode angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>1096575253),array("id"=>"66"),"'sort_index' von Nachrichtenempfang via angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>27),array("id"=>"67"),"'sort_index' von Skype-Benutzername angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>534535),array("id"=>"68"),"'sort_index' von Ungelesene Nachrichten angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>30),array("id"=>"74"),"'sort_index' von NGL-Player-ID angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>150),array("id"=>"75"),"'sort_index' von NGL-Team-ID angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>1101723285),array("id"=>"81"),"'sort_index' von Daten komprimieren angepasst.");
			_DBUUpdateByKey(TblPrefix()."flip_user_column",array("sort_index"=>12),array("id"=>"82"),"'sort_index' von Clan angepasst.");
			_DBUExecQuery("DELETE FROM flip_user_column WHERE id IN ('83','84','85')", "Usereigenschaften M&uuml;llpfand kassiert, Essensgutschein ausgeh&auml;ndigt und Erwachsen (nachgepr&uuml;ft) gel&ouml;scht.");
			_DBUExecQuery("DELETE FROM flip_user_data WHERE column_id IN ('83','84','85')", "Werte von M&uuml;llpfand kassiert, Essensgutschein ausgeh&auml;ndigt und Erwachsen (nachgepr&uuml;ft) gel&ouml;scht.");
			_DBUExecInsert("flip_user_column",array("name"                => "internal_comment",
						"val_type"            => "text",
						"caption"             => "interne Bemerkung",
						"required_view_right" => GetRightID("view_internal_profile"),
						"required_edit_right" => GetRightID("edit_internal_profile"),
						"sort_index"          => 1101724233
			), "Usereigenschaft interne Bemerkung hinzugef&uuml;gt.");
			_DBUExecInsert("flip_user_column",array("name"                => "sex",
						"val_type"            => "sex",
						"required"            => "Y",
						"caption"             => "Geschlecht",
						"required_view_right" => GetRightID("view_personal_informations"),
						"required_edit_right" => GetRightID("edit_own_profile"),
						"sort_index"          => 7
			), "Usereigenschaft Geschlecht hinzugef&uuml;gt.");

		case(624):
			_DBUExecQuery("ALTER TABLE `flip_user_subject` DROP INDEX `nickname` , ADD UNIQUE `nickname` ( `type` , `name` )","'type' zu Index nickname in flip_user_subject hinzugef&uuml;gt.");
			_DBUExecQuery("ALTER TABLE `flip_user_subject` DROP INDEX `email` , ADD UNIQUE `email` ( `email` , `type` )","'type' zu Index email in flip_user_subject hinzugef&uuml;gt.");

		case(625):
			_DBUUpdateByKey("flip_user_subject",array("email"=>null),array("name"=>"orga"),"Emailadresse f&uuml;r Gruppe orga entfernt.");
			_DBUUpdateByKey("flip_user_subject",array("email"=>null),array("name"=>"status_registered"),"Emailadresse f&uuml;r Gruppe status_registered entfernt.");
			_DBUUpdateByKey("flip_user_subject",array("email"=>null),array("name"=>"status_paid"),"Emailadresse f&uuml;r Gruppe status_paid entfernt.");
			_DBUUpdateByKey("flip_user_subject",array("email"=>null),array("name"=>"helper"),"Emailadresse f&uuml;r Gruppe helper entfernt.");
			_DBUUpdateByKey("flip_user_subject",array("email"=>null),array("name"=>"status_checked_in"),"Emailadresse f&uuml;r Gruppe status_checked_in entfernt.");
			_DBUUpdateByKey("flip_user_subject",array("email"=>null),array("name"=>"status_checked_out"),"Emailadresse f&uuml;r Gruppe status_checked_out entfernt.");

		case(626):
			_DBUUpdateByKey("flip_table_entry",array("value"=>"er&ouml;ffnet", "display"=>"er&ouml;ffnet", "display_code"=>"er&ouml;ffnet"),array("id"=>"1"),"Ticketstatusname 1 auf 'er&ouml;ffnet' gesetzt.");
			_DBUUpdateByKey("flip_table_entry",array("value"=>"in Arbeit", "display"=>"in Arbeit", "display_code"=>"in Arbeit"),array("id"=>"2"),"Ticketstatusname 2 auf 'in Arbeit' gesetzt.");
			_DBUExecQuery("UPDATE `flip_ticket_tickets` SET `status` = 101 WHERE (`status` = 3);", "Die stadis der realisierten Tickets angepasst.");
			_DBUUpdateByKey("flip_table_entry",array("key"=>101),array("id"=>"12"),"Schl&uuml;ssel von Ticketstatus realisiert auf 101 gesetzt.");
			_DBUExecInsert("flip_table_entry",array("table_id"=>"1",
						"key"    =>"102",
						"value"    =>"abgelehnt",
						"display"    =>"abgelehnt",
						"display_code"=>"abgelehnt"
						), "Ticketstatus abgelehnt hinzugef&uuml;gt.");
			_DBUExecInsert("flip_table_entry",array("table_id"=>"1",
						"key"    =>"103",
						"value"    =>"doppelt",
						"display"    =>"doppelt",
						"display_code"=>"doppelt"
						), "Ticketstatus doppelt hinzugef&uuml;gt.");
			_DBUUpdateByKey("flip_table_tables",array("description"=>"Verschiedene Stadis f&uuml;r die Tickets (Stadis mit einem Key > 100 werden in der Standardansicht ausgeblendet)"),array("name"=>"ticket_status"),"Beschreibung von flip_table ticket_status ge&auml;ndert.");

		case(627):
			_DBUSetConfig("ticket_force_owner",0,"[0|1] bestimmt, ob nur Tickets mit ausgef&uuml;lltem \"Verantwortlich f&uuml;r\"-Feld erstellt werden k&ouml;nnen.");
			_DBUUpdateByKey("flip_table_entry",array("display"=>"<span style=\"color:blue\">er&ouml;ffnet</span>", "display_code"=>"<span style=\"color:blue\">er&ouml;ffnet</span>"),array("value"=>"er&ouml;ffnet"),"Ticketstatus er&ouml;ffnet eingef&uuml;rbt.");
			_DBUUpdateByKey("flip_table_entry",array("display"=>"<span style=\"color:orange; font-weight:bold;\">in Arbeit</span>", "display_code"=>"<span style=\"color:orange; font-weight:bold;\">in Arbeit</span>"),array("value"=>"in Arbeit"),"Ticketstatus in Arbeit eingef&uuml;rbt.");
			_DBUUpdateByKey("flip_table_entry",array("display"=>"<span style=\"color:green; font-weight:bold;\">testbereit</span>", "display_code"=>"<span style=\"color:green; font-weight:bold;\">testbereit</span>"),array("id"=>"11"),"Ticketstatus testbereit(ID11) eingef&uuml;rbt.");
			_DBUUpdateByKey("flip_table_entry",array("display"=>"<span style=\"color:gray\">realisiert</span>", "display_code"=>"<span style=\"color:gray\">realisiert</span>"),array("id"=>"12"),"Ticketstatus realisiert(ID12) eingef&uuml;rbt.");
			_DBUUpdateByKey("flip_table_entry",array("display"=>"<span style=\"color:gray\">abgelehnt</span>", "display_code"=>"<span style=\"color:gray\">abgelehnt</span>"),array("value"=>"abgelehnt"),"Ticketstatus abgelehnt eingef&uuml;rbt.");
			_DBUUpdateByKey("flip_table_entry",array("display"=>"<span style=\"color:gray\">doppelt</span>", "display_code"=>"<span style=\"color:gray\">doppelt</span>"),array("value"=>"doppelt"),"Ticketstatus doppelt eingef&uuml;rbt.");

		case(629):
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>14),array("id"=>"9"),"'sort_index' von Strasse angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>15),array("id"=>"10"),"'sort_index' von Stadt angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>16),array("id"=>"11"),"'sort_index' von PLZ angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>17),array("id"=>"12"),"'sort_index' von Telefon angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>18),array("id"=>"13"),"'sort_index' von Handy angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>1063571857),array("id"=>"14"),"'sort_index' von Bemerkung angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>19),array("id"=>"15"),"'sort_index' von Zeitpunkt der Registrierung angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>20),array("id"=>"16"),"'sort_index' von Letzter Login angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>24),array("id"=>"17"),"'sort_index' von ICQ angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>25),array("id"=>"18"),"'sort_index' von AIM angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>26),array("id"=>"19"),"'sort_index' von MSN angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>27),array("id"=>"20"),"'sort_index' von Yahoo angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>29),array("id"=>"24"),"'sort_index' von WWCL-Player-ID angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>534535),array("id"=>"25"),"'sort_index' von vollj&auml;hrig angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>150),array("id"=>"28"),"'sort_index' von Beschreibung(Gruppe) angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>1061404876),array("id"=>"30"),"'sort_index' von ZufallsID angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>534535),array("id"=>"31"),"'sort_index' von WWCL-ID(Turnierteam) angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>21),array("id"=>"34"),"'sort_index' von Jabber-ID angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>1093297497),array("id"=>"50"),"'sort_index' von Posts/Seite angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>1068223310),array("id"=>"51"),"'sort_index' von Status angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>1068223320),array("id"=>"52"),"'sort_index' von StatusName angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>1063927068),array("id"=>"54"),"'sort_index' von Sitzplatz angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>1068307043),array("id"=>"55"),"'sort_index' von IP-Adresse angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>30),array("id"=>"56"),"'sort_index' von WWCL-Clan-ID angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>1063571857),array("id"=>"58"),"'sort_index' von Bezeichnung(Server) angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>14),array("id"=>"60"),"'sort_index' von Dienste(Server) angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>1096575253),array("id"=>"65"),"'sort_index' von Hardware-Barcode angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>1101723285),array("id"=>"66"),"'sort_index' von Nachrichtenempfang via angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>28),array("id"=>"67"),"'sort_index' von Skype-Benutzername angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>5345345),array("id"=>"68"),"'sort_index' von Ungelesene Nachrichten angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>150),array("id"=>"74"),"'sort_index' von NGL-Player-ID angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>353453),array("id"=>"75"),"'sort_index' von NGL-Team-ID angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>1101724233),array("id"=>"81"),"'sort_index' von Daten komprimieren angepasst.");
			_DBUUpdateByKey("flip_user_column",array("sort_index"=>1101731839),array("id"=>"86"),"'sort_index' von interne Bemerkung angepasst.");
			_DBUExecInsert("flip_user_column",array("name"                => "bank_name",
						"val_type"            => "string",
						"caption"             => "Kreditinstitut",
						"required_view_right" => GetRightID("view_personal_informations"),
						"required_edit_right" => GetRightID("edit_own_profile"),
						"sort_index"          => 1101731893
			), "Usereigenschaft Kreditinstitut hinzugef&uuml;gt.");
			_DBUExecInsert("flip_user_column",array("name"                => "bank_blz",
						"val_type"            => "string",
						"caption"             => "Bankleitzahl",
						"required_view_right" => GetRightID("view_personal_informations"),
						"required_edit_right" => GetRightID("edit_own_profile"),
						"sort_index"          => 1101731935
			), "Usereigenschaft Bankleitzahl hinzugef&uuml;gt.");
			_DBUExecInsert("flip_user_column",array("name"                => "bank_knr",
						"val_type"            => "string",
						"caption"             => "Kontonummer",
						"required_view_right" => GetRightID("view_personal_informations"),
						"required_edit_right" => GetRightID("edit_own_profile"),
						"sort_index"          => 1101731961
			), "Usereigenschaft Kontonummer hinzugef&uuml;gt.");
			_DBUExecInsert("flip_user_column",array("name"                => "bank_owner",
						"val_type"            => "string",
						"caption"             => "Kontoinhaber",
						"required_view_right" => GetRightID("view_personal_informations"),
						"required_edit_right" => GetRightID("edit_own_profile"),
						"sort_index"          => 1101732006
			), "Usereigenschaft Kontoinhaber hinzugef&uuml;gt.");
			_DBUExecInsert("flip_user_column",array("name"                => "clan_url",
						"val_type"            => "string",
						"caption"             => "Clan Homepage",
						"required_view_right" => GetRightID("view_personal_informations"),
						"required_edit_right" => GetRightID("edit_own_profile"),
						"sort_index"          => 13
			), "Usereigenschaft Clan Homepage hinzugef&uuml;gt.");

		case(631):
			_DBUExecQuery("DROP TABLE IF EXISTS `flip_news_comments`", "Tabelle flip_news_comments gel&ouml;scht falls vorhanden.");
			_DBUExecQuery("
              CREATE TABLE `flip_news_comments` (
              `id` int(11) NOT NULL auto_increment,
              `news_id` int(11) NOT NULL default '0',
              `user_id` int(11) NOT NULL default '0',
              `timestamp` int(11) NOT NULL default '0',
              `text` text NOT NULL,
              PRIMARY KEY  (`id`),
              KEY `i_newsid` (`news_id`)
            ) TYPE=MyISAM;
            ", "Tabelle flip_news_comments hinzugef&uuml;gt.");
			_DBUAddRight("news_comments","Berechtigung um Newskommentare schreiben zu d&uuml;rfen","registered");

		case(632):
		case(633):
			_DBUExecQuery("ALTER TABLE `flip_news_comments` ADD `mtime` TIMESTAMP NOT NULL AFTER `id`;","Spalte `mtime` zu `flip_news_comments` hinzugef&uuml;gt.");
			_DBUExecQuery("ALTER TABLE `flip_news_comments` ADD INDEX `mtime` ( `mtime` );","In flip_news_comments Spalte `mtime` zu Key gemacht.");

		case(634):
			_DBUAddRight("news_editcomments","Recht um Kommentare bearbeiten/l&ouml;schen zu d&uuml;rfen","orga");

		case(636):
			//_DBUSetConfig("tournament_comment_orgaonly",1,"Darf jeder Kommentare zu Matches abgeben oder ist dies den Orgas vorbehalten? (0/1)");

		case(646):
			_DBUUpdateMenu("info","Team","user.php?frame=viewsubject&amp;name=orga","","");
			_DBUExecInsert("flip_user_column",array("type"        => "group",
						"name"        => "public_member_columns",
						"val_type"    => "string",
						"caption"     => "&ouml;ffentl. Spalten der Mitglieder",
						"required_view_right" => GetRightID("view_personal_informations"),
						"required_edit_right" => GetRightID("edit_own_profile"),
						"hint"        => "Beispiel: \"givenname;familyname;job\" Siehe: <a href=\"user.php?frame=viewcolumns&amp;type=user\">Columns</a>",
						"sort_index"  => 1102940008
			),"Column '&ouml;ffentliche Spalten' zu Gruppen hinzugef&uuml;gt.'");
			_DBUExecInsert("flip_user_column",array("type"        => "user",
						"name"        => "job",
						"val_type"    => "string",
						"caption"     => "Aufgabenbereich",
						"required_view_right" => GetRightID("view_personal_informations"),
						"required_edit_right" => GetRightID("edit_own_profile"),
						"sort_index"  => 200
			),"Column '&ouml;ffentliche Spalten' zu Gruppen hinzugef&uuml;gt.'");
			/*      _DBUExecQuery("DELETE FROM flip_user_data WHERE `id`='5116'", "Gruppenbeschreibung von orga entfernt.");
			 _DBUExecQuery("DELETE FROM flip_user_data WHERE `id` IN ('35474','35475','35476','35477','35478','35480','35532')", "Eigenschaften von Max Mustermann entfernt.");
			 _DBUExecInsert("flip_user_data",array("subject_id"    =>10,
			 "column_id"        =>28,
			 "val"        =>"Alle Organisatoren von xxx"
			 ), "Beschreibung der Gruppe orga hinzugef&uuml;gt.");
			 _DBUExecInsert("flip_user_data",array("subject_id"    =>10,
			 "column_id"        =>93,
			 "val"        =>"givenname;familyname;job"
			 ), "&ouml;ffentl. Spalten der Mitglieder der Gruppe orga hinzugef&uuml;gt.");
			 _DBUExecInsert("flip_user_data",array("subject_id"    =>1296,
			 "column_id"        =>2,
			 "val"        =>"Y"
			 ), "Eigenschaft Aktiviert von Max Mustermann gesetzt.");
			 _DBUExecInsert("flip_user_data",array("subject_id"    =>1296,
			 "column_id"        =>4,
			 "val"        =>"Max"
			 ), "Eigenschaft Vorname von Max Mustermann gesetzt.");
			 _DBUExecInsert("flip_user_data",array("subject_id"    =>1296,
			 "column_id"        =>6,
			 "val"        =>"Mustermann"
			 ), "Eigenschaft Nachname von Max Mustermann gesetzt.");
			 _DBUExecInsert("flip_user_data",array("subject_id"    =>1296,
			 "column_id"        =>87,
			 "val"        =>"m"
			 ), "Eigenschaft Geschlecht von Max Mustermann gesetzt.");
			 _DBUExecInsert("flip_user_data",array("subject_id"    =>1296,
			 "column_id"        =>29,
			 "val"        =>420415200
			 ), "Eigenschaft Geburtsdatum von Max Mustermann gesetzt.");
			 _DBUExecInsert("flip_user_data",array("subject_id"    =>1296,
			 "column_id"        =>15,
			 "val"        =>0
			 ), "Eigenschaft Zeitpunkt der Registrierung von Max Mustermann gesetzt.");
			 _DBUExecInsert("flip_user_data",array("subject_id"    =>1296,
			 "column_id"        =>94,
			 "val"        =>"den &uuml;berblick behalten"
			 ), "Eigenschaft Aufgabenbereich von Max Mustermann gesetzt.");
			 _DBUExecInsert("flip_user_data",array("subject_id"    =>1296,
			 "column_id"        =>25,
			 "val"        =>"N"
			 ), "Eigenschaft vollj&auml;hrig von Max Mustermann gesetzt.");
			 _DBUExecQuery("DELETE FROM flip_use_data WHERE subject_id='1296' AND column_id='16'", "Letzer Login von Max Mustermann gel&ouml;scht.");
			 _DBUExecInsert("flip_user_data",array("subject_id"    =>1296,
			 "column_id"        =>16,
			 "val"        =>1102948148
			 ), "Eigenschaft Letzter Login von Max Mustermann gesetzt.");*/
			_DBUAddRight("user_view_members","Wenn ein User dieses Recht &uuml;ber eine Gruppe hat, kann er deren Mitgliederliste betrachten",array("Anonymous" => "orga", "registered" => "orga"));

		case(649):
			_DBUSetConfig("tournament_comment_orgaonly","1","Darf jeder Kommentare zu Matches abgeben oder ist dies den Orgas vorbehalten? (0/1)");

		case(650):
			$c = MysqlReadCol("SELECT `caption`,`timespan` FROM `".TblPrefix()."flip_archiv_events`;","timespan","caption");
			if(!empty($c)) LogChange("ACHTUNG: Das Datumsformat der Ereignisse im Archiv wurde ge&auml;ndert. Die Zeitpunkte m&uuml;ssen erneut eingegeben werden. Hier ein Backup: <br><pre>".var_export($c,true)."</pre>");
			_DBUExecQuery("ALTER TABLE `flip_archiv_events` DROP `timespan`;","Archiv: Spalte 'timespan' entfernt.");
			_DBUExecQuery("ALTER TABLE `flip_archiv_events` ADD `time_start` INT NOT NULL AFTER `view_right` ,ADD `time_end` INT NOT NULL AFTER `time_start` ;","Spalten 'time_start' und 'time_end' hinzugef&uuml;gt.");

		case(653):
			_DBUExecQuery("ALTER TABLE `flip_tournament_combatant` CHANGE `group_id` `team_id` INT( 11 ) DEFAULT '0' NOT NULL","flip_tournament_combatant: Spalte group_id in team_id umbenannt.");
			_DBUExecQuery("ALTER TABLE `flip_tournament_invite` CHANGE `from` `from` ENUM( 'team', 'user' ) DEFAULT 'team' NOT NULL","flip_tournament_combatant: In Enum von from group in team umbenannt.");
			_DBUExecQuery("ALTER TABLE `flip_tournament_invite` CHANGE `group_id` `team_id` INT( 11 ) DEFAULT '0' NOT NULL","flip_tournament_invite: Spalte group_id in team_id umbenannt.");
			_DBUExecQuery("ALTER TABLE `flip_tournament_matches` CHANGE `group1` `team1` INT( 11 ) DEFAULT NULL","flip_tournament_combatant: Spalte group1 in team1 umbenannt.");
			_DBUExecQuery("ALTER TABLE `flip_tournament_matches` CHANGE `group2` `team2` INT( 11 ) DEFAULT NULL","flip_tournament_combatant: Spalte group2 in team2 umbenannt.");

		case(660):
			_DBUExecInsert("flip_user_column",array(
				"name"                => "ip_fix",
				"val_type"            => "IP",
				"caption"             => "Festgelegte IP",
				"required_view_right" => GetRightID("view_personal_informations"),
				"required_edit_right" => GetRightID("user_admin_subjects"),
				"comment"             => "wird vergeben wenn in der Config der Wert \"server_iptype\" auf \"manuell\" gesetzt wird",
				"sort_index"          => 1103520357
			), "Usereigenschaft Festgelegte IP hinzugef&uuml;gt.");
			_DBUUpdateByKey("flip_user_right",array("description"=>"Erh&auml;lt jeder der sich f&uuml;r die LAN-Party registriert hat."),array("right"=>"status_registered"),"Beschreibung der Gruppe status_registered angepasst.");

		case(661):
			_DBUSetConfig("changelog_sourcexml","http://www.flipdev.org/changelog.xml","Gibt an, wo sich die XML-Datei befindet, welche den FLIP-Changelog enth&auml;lt.");

		case(681):
			_DBUExecInsert("flip_content_text", array(
				"group_id"   => 3,
				"name"       => "webmessage_contact",
				"view_right" => 0,
				"edit_right" => GetRightID("edit_public_text"),
				"caption"    => "Kontakt",
				"text"       => "<p align=\"justify\">\r\nWenn du Fragen, Kritik oder Anregungen hast, tu dir keinen Zwang an und Teil uns deine Meinung mit!<br>\r\nWir sind gerne bereit darauf ein zu gehen und eventuelle Probleme aus der Welt zu r&auml;umen.\r\n<p>",
			),"Content-Text webmessage_contact hinzugef&uuml;gt.");
			_DBUAddRight("webmessage_use","Recht &uuml;ber sich selbst oder andere welches das Senden und Lesen von Webmessages erlaubt",array("usermanager" => SYSTEM_USER_ID));
			_DBUExecQuery("ALTER TABLE `flip_webmessage_message` CHANGE `type` `source_type` ENUM( 'resend', 'replay', 'oforward', 'sforward' ) DEFAULT NULL;","flip_webmessage_message: Spalte source_type bearbeitet.");
			_DBUExecQuery("ALTER TABLE `flip_webmessage_message` ADD `source_id` INT AFTER `source_type`;","flip_webmessage_message: Spalte source_id erstellt.");
			_DBUExecQuery("ALTER TABLE `flip_webmessage_message` ADD `processor_id` INT NOT NULL AFTER `sender_id`;","flip_webmessage_message: Spalte processor_id erstellt.");
			_DBUExecQuery("ALTER TABLE `flip_webmessage_message` CHANGE `read` `status` ENUM( 'unread', 'read', 'processed' ) DEFAULT 'unread' NOT NULL;","flip_webmessage_message: Spalte status bearbeitet.");
			_DBUUpdateMenu("action","Nachrichten","webmessage.php?frame=owner&amp;id=4","","Nachrichten des System-Users.");
			_DBUUpdateMenu("info","Kontakt","webmessage.php?frame=contact","","");

		case(685):
			_DBUSetConfig("translate_language","de_DE","Sprache des Systems.");
			_DBUExecQuery("
        CREATE TABLE `flip_textdata` (
          `id` int(7) NOT NULL auto_increment,
          `mtime` timestamp(14) NOT NULL,
          `name` text NOT NULL,
          `locale` varchar(5) NOT NULL default 'de_DE',
          `text` text NOT NULL,
          PRIMARY KEY  (`id`),
          KEY `mtime` (`mtime`)
        ) TYPE=MyISAM;
        ","Tabelle flip_textdata hinzugef&uuml;gt.");
			_DBUExecQuery("INSERT INTO `flip_textdata` VALUES (1,NULL,'Kommentar zu','en_GB','comment to')","&uuml;bersetzungeintrag hinzugef&uuml;gt.");
			_DBUExecQuery("INSERT INTO `flip_textdata` VALUES (2,NULL,'Noch kein Newseintrag vorhanden.','en_GB','no news exsisting.')","&uuml;bersetzungeintrag hinzugef&uuml;gt.");
			_DBUExecQuery("INSERT INTO `flip_textdata` VALUES (3,NULL,'News','en_GB','news')","&uuml;bersetzungeintrag hinzugef&uuml;gt.");
			_DBUExecQuery("INSERT INTO `flip_textdata` VALUES (4,NULL,'Bisher wurde noch kein Newseintrag erstellt.','en_GB','this time no news exsisting')","&uuml;bersetzungeintrag hinzugef&uuml;gt.");
			_DBUExecQuery("INSERT INTO `flip_textdata` VALUES (5,NULL,'Newseintrag erstellen','en_GB','create news')","&uuml;bersetzungeintrag hinzugef&uuml;gt.");
			_DBUExecQuery("INSERT INTO `flip_textdata` VALUES (6,NULL,'Kommentare','en_GB','comments')","&uuml;bersetzungeintrag hinzugef&uuml;gt.");
			_DBUExecQuery("INSERT INTO `flip_textdata` VALUES (7,NULL,'Datum','en_GB','date')","&uuml;bersetzungeintrag hinzugef&uuml;gt.");

		case(694):
		case(708):
			_DBUExecQuery("ALTER TABLE `flip_menu_blocks` ADD `enabled` TINYINT( 1 ) DEFAULT '1' NOT NULL AFTER `callback_items` ;", "flip_menu_blocks: Spalte enabled hinzugef&uuml;gt.");
			_DBUExecQuery("ALTER TABLE `flip_menu_links` CHANGE `enabled` `enabled` TINYINT( 1 ) DEFAULT '1' NOT NULL", "flip_menu_links: Spalte enabled auf Tinyint(1) gesetzt.");

		case(724):
			_DBUUpdateMenu("webmaster","FLIP-Changelog","changelog.php","","Der FLIP-SVN-Commit-Log");

		case(730):
			//Spiele, Dienste und Betriebssysteme f&uuml;r Server unter flip_table
			$newtableid = _DBUExecInsert("flip_table_tables", array("name"       => "server_games",
                                                "edit_right" => GetRightID("server_admin"),
                                                "description"=> "Spiele die f&uuml;r \"mein(e) Server\" zur Auswahl stehen",
			),"server_games zu flip_table_tables hinzugef&uuml;gt.");
			if($newtableid > 0)
			{
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $newtableid,
					"key"		=> "1",
					"value"		=> "HL/CS",
					"display"	=> "HL/CS",
					"display_code"	=> "HL/CS",
				), "Das Spiel HL/CS wurde in tables->server_games eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $newtableid,
					"key"		=> "2",
					"value"		=> "BF1942",
					"display"	=> "BF1942",
					"display_code"	=> "BF1942",
				), "Das Spiel BF1942 wurde in tables->server_games eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $newtableid,
					"key"		=> "3",
					"value"		=> "Quake3",
					"display"	=> "Quake3",
					"display_code"	=> "Quake3",
				), "Das Spiel Quake3 wurde in tables->server_games eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $newtableid,
					"key"		=> "4",
					"value"		=> "UT2004",
					"display"	=> "UT2004",
					"display_code"	=> "UT2004",
				), "Das Spiel UT2004 wurde in tables->server_games eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $newtableid,
					"key"		=> "5",
					"value"		=> "CoD",
					"display"	=> "CoD",
					"display_code"	=> "CoD",
				), "Das Spiel CoD wurde in tables->server_games eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $newtableid,
					"key"		=> "6",
					"value"		=> "ET",
					"display"	=> "ET",
					"display_code"	=> "ET",
				), "Das Spiel ET wurde in tables->server_games eingetragen.");
			}
			$newtableid = _DBUExecInsert("flip_table_tables", array("name"       => "server_services",
					"edit_right" => GetRightID("server_admin"),
					"description"=> "Dienste die f&uuml;r \"mein(e) Server\" zur Auswahl stehen",
			),"server_services zu flip_table_tables hinzugef&uuml;gt.");
			if($newtableid > 0)
			{
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $newtableid,
					"key"		=> "1",
					"value"		=> "HTTP",
					"display"	=> "HTTP",
					"display_code"	=> "HTTP",
				), "Der Dienst HTTP wurde in tables->server_services eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $newtableid,
					"key"		=> "2",
					"value"		=> "FTP",
					"display"	=> "FTP",
					"display_code"	=> "FTP",
				), "Der Dienst FTP wurde in tables->server_services eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $newtableid,
					"key"		=> "3",
					"value"		=> "SMB",
					"display"	=> "SMB",
					"display_code"	=> "SMB",
				), "Der Dienst SMB wurde in tables->server_services eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $newtableid,
					"key"		=> "4",
					"value"		=> "IRC",
					"display"	=> "IRC",
					"display_code"	=> "IRC",
				), "Der Dienst IRC wurde in tables->server_services eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $newtableid,
					"key"		=> "5",
					"value"		=> "TeamSpeak",
					"display"	=> "TeamSpeak",
					"display_code"	=> "TeamSpeak",
				), "Der Dienst TeamSpeak wurde in tables->server_services eingetragen.");
			}
			$newtableid = _DBUExecInsert("flip_table_tables", array("name"       => "server_oses",
					"edit_right" => GetRightID("server_admin"),
					"description"=> "Betriebssysteme die f&uuml;r \"mein(e) Server\" zur Auswahl stehen",
			),"server_oses zu flip_table_tables hinzugef&uuml;gt.");
			if($newtableid > 0)
			{
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $newtableid,
					"key"		=> "1",
					"value"		=> "Linux",
					"display"	=> "Linux",
					"display_code"	=> "Linux",
				), "Das Betriebssystem Linux wurde in tables->server_oses eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $newtableid,
					"key"		=> "2",
					"value"		=> "Windows 2003",
					"display"	=> "Windows 2003",
					"display_code"	=> "Windows 2003",
				), "Das Betriebssystem Windows 2003 wurde in tables->server_oses eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $newtableid,
					"key"		=> "3",
					"value"		=> "Windows XP",
					"display"	=> "Windows XP",
					"display_code"	=> "Windows XP",
				), "Das Betriebssystem Windows XP wurde in tables->server_oses eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $newtableid,
					"key"		=> "4",
					"value"		=> "Windows 2000",
					"display"	=> "Windows 2000",
					"display_code"	=> "Windows 2000",
				), "Das Betriebssystem Windows 2000 wurde in tables->server_oses eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $newtableid,
					"key"		=> "5",
					"value"		=> "Windows 9x",
					"display"	=> "Windows 9x",
					"display_code"	=> "Windows 9x",
				), "Das Betriebssystem Windows 9x wurde in tables->server_oses eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $newtableid,
					"key"		=> "6",
					"value"		=> "BSD",
					"display"	=> "BSD",
					"display_code"	=> "BSD",
				), "Das Betriebssystem BSD wurde in tables->server_oses eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $newtableid,
					"key"		=> "7",
					"value"		=> "Solaris",
					"display"	=> "Solaris",
					"display_code"	=> "Solaris",
				), "Das Betriebssystem Solaris wurde in tables->server_oses eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $newtableid,
					"key"		=> "8",
					"value"		=> "Anderes",
					"display"	=> "Anderes",
					"display_code"	=> "Anderes",
				), "Das Betriebssystem Anderes wurde in tables->server_oses eingetragen.");
			}
		case(745):
			_DBUSetConfig("checkininfo_subnetmask","255.255.0.0","Die Subnetzmaske die im Clientnetz verwendet wird.");
			_DBUSetConfig("checkininfo_dnsserver","","Die Adresse des oder der DNS-Server die die User verwenden sollen.");
			_DBUSetConfig("checkininfo_winsserver","","Die Adresse des oder der WINS-Server die die User verwenden sollen.");
			_DBUSetConfig("checkininfo_webserver","","Die Adresse bzw. URL des oder der Web-Server die die User verwenden sollen.");
			_DBUSetConfig("checkininfo_ftpserver","","Die Adresse des oder der FTP-Server die die User verwenden sollen.");

		case(746):
			_DBUExecQuery("ALTER TABLE `flip_lastvisit` DROP `last_visit`, DROP `current_visit`, DROP `session_id` ;","flip_lastvisit angepasst.");

		case(755):
			_DBUSetConfig("lastvisit_timespan","50","Die Zeitspanne in Tagen f&uuml;r die der Status \"gelesen\" (z.B. im Forum) gespeichert wird. Alles was &auml;lter ist, ist immer \"gelesen\".");

		case(756):

		case(759):
			_DBUSetConfig("tournament_coinname","Coins","Name der W&auml;hrung f&uuml;r Turniere (z.B. Coins, Punkte, €)");

		case(761):
			_DBUSetConfig("statistic_numberonlineusers",5,"So viele Usernamen werden als online angezeigt.");

		case(769):
			if($menuid = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_menu_links WHERE caption='foren'", "id"))
			_DBUExecInsert("flip_menu_blocks", array("caption"	=>"User online",
					"description"	=>"Zeigt wieviele User eingeloggt sind und die ersten 5 (Config->statistics_numberonlineusers) l&auml;ngsten Onliner.",
					"view_right"	=>GetRightID("logged_in"),
					"parent_item_id"=>$menuid,
					"frameurl"	=>"statistic.php?frame=online",
					"order"	=>1106543670
			),"Men&uuml;block 'Users online' unter Foren eingef&uuml;gt.");

		case(771):
			_DBUSetConfig("statistic_onlinemaxlastseen",5,"Zeit in Minuten die maximal seit letztem Besuch vergangen sind damit User als Online angezeigt wird.");

		case(776):
		case(778):
			_DBUAddRight("edit_own_profile", "", array("publisher" => SYSTEM_USER_ID));
			_DBUAddRight("view_personal_informations", "", array("publisher" => SYSTEM_USER_ID));

		case(779):
			_DBUExecInsert("flip_content_text", array("group_id"   => 3,
					"name"       => "lanparty_banktransfer",
					"view_right" => 0,
					"edit_right" => GetRightID("edit_public_text"),
					"caption"    => "&uuml;berweisung",
					"text"       => "<p>\r\n  Bitte &uuml;berweise den Unkostenbeitrag f&uuml;r die Lanparty an:<br>\r\n  <br>\r\n  Dagobert Duck<br>\r\n  Blz. 123456 GSBS (Geldspeicherbankstadt) Entenhausen<br>\r\n  KontoNr. 0000001<br>\r\n  Verwendungszweck: Lan xxx; Nick: FooBar<br>\r\n</p>\r\n\r\n  ",
			),"Content-Text lanparty_banktransfer hinzugef&uuml;gt.");
			if($menuid = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_menu_links WHERE caption='lanparty'", "id"))
			_DBUExecInsert("flip_menu_blocks", array("caption"	=>"Checklist",
					"description"	=>"",
					"parent_item_id"=>$menuid,
					"frameurl"	=>"lanparty.php?frame=smalluserstatus",
					"order"	=>1106543670
			),"Men&uuml;block 'Checklist' unter Lanparty eingef&uuml;gt.");
			_DBUUpdateMenu("join","&uuml;berweisung","text.php?name=lanparty_banktransfer","","Das Konto f&uuml;r den Unkostenbeitrag");

		case(791):
			_DBUUpdateSubjectColumn("group","caption","string","Caption","view_personal_informations","edit_own_profile","Wenn angegeben, ist dies die &uuml;berschrift der Gruppen&uuml;bersicht");
			_DBUUpdateSubjectProperties("orga", array("caption" => "Organisatoren"));

		case(801):
			_DBUUpdateSubjectColumn("user","ip_fix","IP","Festgelegte IP","admin","user_admin_subjects","wird vergeben wenn in der Config der Wert \"user_iptype\" auf \"fix\" gesetzt wird");

		case(803):
		case(804):
			_DBUSetConfig("forum_additionaluserdata","clan","Eigenschaften (Columns) von Usern die zu jedem Post angezeigt werden sollen. Trennzeichen ;");
			_DBUSetConfig("forum_additionaluserdata_spacer","<br />","Trennzeichen von Zusatzinfos je Poster (z.B. Komma, Bindestrich oder Zeilenumbruch)");

		case(809):
			// nix

		case(810):
			_DBUUpdateSubjectColumn("group", "public_member_columns", "String", "&ouml;ffentl. Spalten der Mitglieder", "view_internal_informations", "edit_public_text", "Beispiel: \"givenname;familyname;job\" Siehe: <a href=\"user.php?frame=viewcolumns&amp;type=user\">Columns</a>");
			_DBUUpdateSubjectColumn("group", "caption", "String", "Caption", "view_internal_informations", "edit_public_text", "Wenn angegeben, ist dies die &uuml;berschrift der Gruppen&uuml;bersicht");

		case(819):
			_DBUExecQuery("
        CREATE TABLE `flip_config_values` (
          `id` int(11) NOT NULL auto_increment,
          `config_id` int(11) NOT NULL default '0',
          `value` varchar(255) NOT NULL default '',
          PRIMARY KEY  (`id`),
          KEY `config_id` (`config_id`)
        ) TYPE=MyISAM;
        ", "Tabelle flip_config_values hinzugef&uuml;gt");
			//Optionen f&uuml;r vorhandene Configeintraege werden nicht angelegt!
			LogDBUpdate("f&uuml;r die Config-Eintr&auml;ge sind nun Werte vordefinierbar (z.B.: 1/0). Es wurden aber keine Eintr&auml;ge angefasst, so dass dies manuell f&uuml;r die entsprechenden Optionen gemacht werden muss!");

		case(820):
			$tournament_games_id = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_table_tables WHERE `name`='TOURNAMENT_GAMES'", "id");
			if($tournament_games_id > 0)
			{
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $tournament_games_id,
					"key"		=> "dow",
					"value"	=> "Dawn of War",
					"display"	=> "Dawn of War",
					"display_code"	=> "Dawn of War",
				), "Das Spiel \"Dawn of War\" wurde in tables->TOURNAMENT_GAMES eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $tournament_games_id,
					"key"		=> "pes4",
					"value"	=> "Pro Evolution Soccer4",
					"display"	=> "Pro Evolution Soccer4",
					"display_code"	=> "Pro Evolution Soccer4",
				), "Das Spiel \"Pro Evolution Soccer4\" wurde in tables->TOURNAMENT_GAMES eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $tournament_games_id,
					"key"		=> "dsum",
					"value"	=> "DSuM",
					"display"	=> "DSuM",
					"display_code"	=> "DSuM",
				), "Das Spiel DSuM wurde in tables->TOURNAMENT_GAMES eingetragen.");
			}

		case(821):
			_DBUSetConfig("page_index","news.php","Die Startseite welche aufgerufen wird, wenn keine Datei angegeben wurde (Weiterleitung von index.php)");
			_DBUUpdateMenu("intern","Rundbriefe","sendmessage.php",GetRightID("webmessage_use"),"Nachrichten an Gruppen senden.");

		case(828):
			/* type zu Config hinzuf&uuml;gen */
			_DBUExecQuery("ALTER TABLE `flip_config` ADD `type` VARCHAR( 32 ) DEFAULT 'LongString' NOT NULL AFTER `key` ;", "Spalte type zu flip_config hinzugef&uuml;gt.");
			//dbupdate_autoupdate
			$oldvalue = (ConfigGet("dbupdate_autoupdate")) ? "Y" : "N";
			_DBUSetConfig("dbupdate_autoupdate",$oldvalue,"(Y|N) wenn Ja, wird die DB automatisch an neu eingespielte Dateien angepasst.","YesNo");
			$configid = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_config WHERE `key`='dbupdate_autoupdate'", "id");
			_DBUExecQuery("DELETE FROM flip_config_values WHERE config_id = '$configid'", "alte Werte von dbupdate_autoupdate gel&ouml;scht");
			//session_timeout
			$oldvalue = ConfigGet("session_timeout");
			_DBUSetConfig("session_timeout",$oldvalue,"Der Timout einer Session in Minuten. d.h.: Wenn sich der Client nach max x min nicht beim Server gemeldet hat, wird die Session gel&ouml;scht.","Integer");
			//session_checkip
			$oldvalue = (ConfigGet("session_checkip")) ? "Y" : "N";
			_DBUSetConfig("session_checkip",$oldvalue,"Soll neben dem der ID im Cookie auch die IP des HTTP-Clients auf gleichheit zwischen den einzelnen Seiteaufrufen &uuml;berpr&uuml;ft werden?","YesNo");
			$configid = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_config WHERE `key`='session_checkip'", "id");
			_DBUExecQuery("DELETE FROM flip_config_values WHERE config_id = '$configid'", "alte Werte von session_checkip gel&ouml;scht");
			//session_checkagent
			$oldvalue = (ConfigGet("session_checkagent")) ? "Y" : "N";
			_DBUSetConfig("session_checkagent",$oldvalue,"...und auch der UserAgent-Name des Browsers?","YesNo");
			$configid = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_config WHERE `key`='session_checkagent'", "id");
			_DBUExecQuery("DELETE FROM flip_config_values WHERE config_id = '$configid'", "alte Werte von session_checkagent gel&ouml;scht");
			//page_errorsinline
			$oldvalue = (ConfigGet("page_errorsinline")) ? "Y" : "N";
			_DBUSetConfig("page_errorsinline",$oldvalue,"Sollen die Fehler in oder unter der Webseite ausgegeben werden? (N|Y)","YesNo");
			$configid = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_config WHERE `key`='page_errorsinline'", "id");
			_DBUExecQuery("DELETE FROM flip_config_values WHERE config_id = '$configid'", "alte Werte von page_errorsinline gel&ouml;scht");
			//forum_postsperpage
			$oldvalue = ConfigGet("forum_postsperpage");
			_DBUSetConfig("forum_postsperpage",$oldvalue,"Default, wie viele Posts per Page der User auf einer Seite zu sehen bekommt.","Integer");
			//subject_min_namelen
			$oldvalue = ConfigGet("subject_min_namelen");
			_DBUSetConfig("subject_min_namelen",$oldvalue,"Die minimale L&auml;nge f&uuml;r einen Subjekt-Namen. (Usernamen, Gruppennamen...)","Integer");
			//netlog_enabled
			$oldvalue = (ConfigGet("netlog_enabled")) ? "Y" : "N";
			_DBUSetConfig("netlog_enabled",$oldvalue,"(Y|N) In den Netlog werden beim erstellen einer Session, Login und Logout alle Daten geschrieben, die &uuml;ber den User in erfahrung zu bringen sind.","YesNo");
			$configid = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_config WHERE `key`='netlog_enabled'", "id");
			_DBUExecQuery("DELETE FROM flip_config_values WHERE config_id = '$configid'", "alte Werte von netlog_enabled gel&ouml;scht");
			//seats_max_notes
			$oldvalue = ConfigGet("seats_max_notes");
			_DBUSetConfig("seats_max_notes",$oldvalue,"Die Anzahl der Pl&auml;tze, die ein User f&uuml;r sich vormerken kann.","Integer");
			//lanparty_maxguests
			$oldvalue = ConfigGet("lanparty_maxguests");
			_DBUSetConfig("lanparty_maxguests",$oldvalue,"Die Anzahl der Teilnehmer","Integer");
			//lanparty_date
			if(($oldvalue = strtotime(ConfigGet("lanparty_date"))) == -1)
			{
				$msg = "Fehler beim &uuml;bernehmen des Datums '".ConfigGet("lanparty_date")."' ins Datumsformat. Bitte geben sie das Datum unter Config erneut ein.";
				LogDBUpdate($msg);
				$oldvalue = 0;
			}
			_DBUSetConfig("lanparty_date",$oldvalue,"Datum der LAN-Party","Date");
			//tournament_losersubmit
			$oldvalue = ConfigGet("tournament_losersubmit");
			_DBUSetConfig("tournament_losersubmit",$oldvalue,"Nur Verlierer darf Ergebnis eintragen (Y/N)","YesNo");
			$configid = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_config WHERE `key`='tournament_losersubmit'", "id");
			_DBUExecQuery("DELETE FROM flip_config_values WHERE config_id = '$configid'", "alte Werte von tournament_losersubmit gel&ouml;scht");
			//forum_max_post_length
			$oldvalue = ConfigGet("forum_max_post_length");
			_DBUSetConfig("forum_max_post_length",$oldvalue,"Legt fest, wie viele Zeichen ein Text im Forum maximal lang sein darf.","Integer");
			//sendmessage_jabber_port
			$oldvalue = ConfigGet("sendmessage_jabber_port");
			_DBUSetConfig("sendmessage_jabber_port",$oldvalue,"","Integer");
			//poll_duration
			$oldvalue = ConfigGet("poll_duration");
			_DBUSetConfig("poll_duration",$oldvalue,"Dauer einer Umfrage in Tagen","Integer");
			//page_compress_output
			$oldvalue = (ConfigGet("page_compress_output")) ? "Y" : "N";
			_DBUSetConfig("page_compress_output",$oldvalue,"(Y|N) Komprimiert die HTML-Seiten, bevor sie an den Browser gesendet werden. (Wenn der Browser dies unterst&uuml;tzt)","YesNo");
			$configid = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_config WHERE `key`='page_compress_output'", "id");
			_DBUExecQuery("DELETE FROM flip_config_values WHERE config_id = '$configid'", "alte Werte von page_compress_output gel&ouml;scht");
			//seats_show_ip
			$oldvalue = (ConfigGet("seats_show_ip")) ? "Y" : "N";
			_DBUSetConfig("seats_show_ip",$oldvalue,"Legt fest, ob im Sitzplan die IP-Adresse eines Platzes angezeigt werden soll.","YesNo");
			$configid = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_config WHERE `key`='seats_show_ip'", "id");
			_DBUExecQuery("DELETE FROM flip_config_values WHERE config_id = '$configid'", "alte Werte von seats_show_ip gel&ouml;scht");
			//tournament_playright
			$oldvalue = ConfigGet("tournament_playright");
			_DBUSetConfig("tournament_playright",$oldvalue,"Recht um an Turnieren teilnehmen zu d&uuml;rfen.","Rights");
			//network_nslookup
			$oldvalue = ConfigGet("network_nslookup");
			_DBUSetConfig("network_nslookup",$oldvalue,"Sollen DNS-Anfragen gemacht werden? (Y/N)","YesNo");
			$configid = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_config WHERE `key`='network_nslookup'", "id");
			_DBUExecQuery("DELETE FROM flip_config_values WHERE config_id = '$configid'", "alte Werte von network_nslookup gel&ouml;scht");
			//sendmessage_disable_email
			$oldvalue = (ConfigGet("sendmessage_disable_email")) ? "Y" : "N";
			_DBUSetConfig("sendmessage_disable_email",$oldvalue,"Deaktiviert das Versenden von Emails. (was im LAN sinnvoll ist)","YesNo");
			$configid = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_config WHERE `key`='sendmessage_disable_email'", "id");
			_DBUExecQuery("DELETE FROM flip_config_values WHERE config_id = '$configid'", "alte Werte von sendmessage_disable_email gel&ouml;scht");
			//dbupdate_autoupdate
			$oldvalue = (ConfigGet("dbupdate_autoupdate")) ? "Y" : "N";
			_DBUSetConfig("dbupdate_autoupdate",$oldvalue,"(Y|N) wenn Ja, wird die DB automatisch an neu eingespielte Dateien angepasst.","YesNo");
			$configid = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_config WHERE `key`='dbupdate_autoupdate'", "id");
			_DBUExecQuery("DELETE FROM flip_config_values WHERE config_id = '$configid'", "alte Werte von dbupdate_autoupdate gel&ouml;scht");
			//tournament_coins
			$oldvalue = ConfigGet("tournament_coins");
			_DBUSetConfig("tournament_coins",$oldvalue,"Guthaben eines jeden Spielers f&uuml;r Turniere","Integer");
			//ticket_force_owner
			$oldvalue = (ConfigGet("ticket_force_owner")) ? "Y" : "N";
			_DBUSetConfig("ticket_force_owner",$oldvalue,"[N|Y] bestimmt, ob nur Tickets mit ausgef&uuml;lltem \"Verantwortlich f&uuml;r\"-Feld erstellt werden k&ouml;nnen.","YesNo");
			$configid = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_config WHERE `key`='ticket_force_owner'", "id");
			_DBUExecQuery("DELETE FROM ".TblPrefix()."flip_config_values WHERE config_id = '$configid'", "alte Werte von ticket_force_owner gel&ouml;scht");
			//lastvisit_timespan
			$oldvalue = ConfigGet("lastvisit_timespan");
			_DBUSetConfig("lastvisit_timespan",$oldvalue,"Die Zeitspanne in Tagen f&uuml;r die der Status \"gelesen\" (z.B. im Forum) gespeichert wird. Alles was &auml;lter ist, ist immer \"gelesen\".","Integer");
			//statistic_numberonlineusers
			$oldvalue = ConfigGet("statistic_numberonlineusers");
			_DBUSetConfig("statistic_numberonlineusers",$oldvalue,"So viele Usernamen werden als online angezeigt.","Integer");
			//statistic_onlinemaxlastseen
			$oldvalue = ConfigGet("statistic_onlinemaxlastseen");
			_DBUSetConfig("statistic_onlinemaxlastseen",$oldvalue,"Zeit in Minuten die maximal seit letztem Besuch vergangen sind damit User als Online angezeigt wird.","Integer");

		case(829):
			//nix

		case(832):
			$tournament_games_id = MysqlReadField("SELECT `id` FROM ".TblPrefix()."flip_table_tables WHERE `name`='TOURNAMENT_GAMES'", "id");
			if($tournament_games_id > 0)
			{
				_DBUExecQuery("UPDATE ".TblPrefix()."flip_table_entry SET `key`='pes' WHERE `key`='pes4' AND table_id='$tournament_games_id'", "flip_tables_entry (TOURNAMENT_GAMES): Abk&uuml;rzung pes4 in pes ge&auml;ndert um NGL-kompatibel zu sein.");
				_DBUExecQuery("UPDATE flip_table_entry SET value='Schlacht um Mittelerde', display='Schlacht um Mittelerde', display_code='Schlacht um Mittelerde'
                         WHERE `key`='dsum' AND value='DSuM' AND table_id='$tournament_games_id'", "flip_tables_entry (TOURNAMENT_GAMES): Beschreibung von DSuM auf \"Schlacht um Mittelerde\" ge&auml;ndert.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $tournament_games_id,
					"key"		=> "css",
					"value"	=> "Counter-Strike Source",
					"display"	=> "Counter-Strike Source",
					"display_code"	=> "Counter-Strike Source",
				), "Das Spiel \"Counter-Strike Source\" wurde in tables->TOURNAMENT_GAMES eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $tournament_games_id,
					"key"		=> "hl2",
					"value"	=> "Half-Life 2 DeathMatch",
					"display"	=> "Half-Life 2 DeathMatch",
					"display_code"	=> "Half-Life 2 DeathMatch",
				), "Das Spiel \"Half-Life 2 DeathMatch\" wurde in tables->TOURNAMENT_GAMES eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $tournament_games_id,
					"key"		=> "dod",
					"value"	=> "Day of Defeat",
					"display"	=> "Day of Defeat",
					"display_code"	=> "Day of Defeat",
				), "Das Spiel \"Day of Defeat\" wurde in tables->TOURNAMENT_GAMES eingetragen.");
			}


		case(833):
			_DBUUpdateByKey("flip_config",array("type" => "template"),array("key" => "template_basename"),"Type f&uuml;r template_basename auf template gesetzt.");
			_DBUUpdateByKey("flip_config",array("type" => "template"),array("key" => "template_customname"),"Type f&uuml;r template_customname auf template gesetzt.");
			_DBUUpdateByKey("flip_config",array("type" => "messagetype"),array("key" => "sendmessage_receive_type"),"Type f&uuml;r sendmessage_receive_type auf messagetype gesetzt.");

			_DBUUpdateByKey("flip_user_column",array("val_type" => "yesno"),array("type" => "user", "name" => "page_compress_output"),"val_type f&uuml;r page_compress_output auf yesno gesetzt.");

		case(836):
			_DBUExecQuery("ALTER TABLE `flip_menu_links` ADD `frameurl` VARCHAR( 254 ) NOT NULL AFTER `use_new_wnd`;","Spalte frameurl zu flip_menu_links hinzugef&uuml;gt.");
			$frames = MysqlReadCol("SELECT `id`, `frameurl` FROM `".TblPrefix()."flip_menu_blocks` WHERE (LENGTH(`frameurl`) > 0);","frameurl","id");
			foreach($frames as $id => $frame)
			_DBUExecInsert("flip_menu_links",array("block_id" => $id, "frameurl" => $frame, "enabled" => 1),"Frameurl $frame &uuml;bernommen.");
			_DBUExecQuery("ALTER TABLE `flip_menu_blocks` DROP `frameurl`","Spalte frameurl aus flip_menu_blocks entfernt.");

			_DBUExecQuery("ALTER TABLE `flip_menu_blocks` ADD `image_title` BLOB NOT NULL AFTER `enabled`,ADD `image_bg` BLOB NOT NULL AFTER `image_title`;","Spalten image_bd und image_title zu flip_menu_blocks hinzugef&uuml;gt.");
			_DBUExecQuery("ALTER TABLE `flip_menu_links` ADD `image` BLOB NOT NULL AFTER `frameurl`;","Spalte image zu flip_menu_links hinzugef&uuml;gt.");
			_DBUExecQuery("ALTER TABLE `flip_menu_links` ADD `text` TEXT NOT NULL AFTER `frameurl`;","Spalte text zu flip_menu_links hinzugef&uuml;gt.");

		case(848):
			_DBUExecQuery("UPDATE `flip_config` SET `type`=LOWER(`type`)","Config-Typen lowercase gemacht.");

		case(856):
			$id = _DBUExecInsert("flip_menu_blocks",array("level_index" => 1, "caption" => "Webmessages", "enabled" => 1, "view_right" => GetRightID("webmessage_use"), "order" => 1234),"Men&uuml;block f&uuml;r Webmesssages erstellt.");
			_DBUExecInsert("flip_menu_links",array("block_id" => $id, "order" => 34334232, "caption" => "Webmessages", "enabled" => 1, "frameurl" => "webmessage.php?frame=smallnewmessages"),"Men&uuml;eintrag f&uuml;r Webmessages erstellt.");

		case(861):
			_DBUExecQuery("ALTER TABLE `flip_menu_blocks` CHANGE `image_title` `image_title` LONGBLOB NOT NULL ,CHANGE `image_bg` `image_bg` LONGBLOB NOT NULL","Typ von Spalten image_title und image_bg in flip_menu_blocks von blob auf lonblob ge&auml;ndert.");
			_DBUExecQuery("ALTER TABLE `flip_menu_links` CHANGE `image` `image` LONGBLOB NOT NULL","Typ von Spalte image in flip_menu_links von blob auf longblob ge&auml;ndert.");
			_DBUExecQuery("ALTER TABLE `flip_sponsor_ads` CHANGE `image` `image` LONGBLOB NOT NULL","Typ von Spalte image in flip_sponsor_ads von blob auf longblob ge&auml;ndert.");
			_DBUExecQuery("ALTER TABLE `flip_tournament_tournaments` CHANGE `icon` `icon` LONGBLOB DEFAULT NULL","Typ von Spalte icon in flip_tournament_tournaments von blob auf longblob ge&auml;ndert.");

		case(867):
			_DBUSetConfig("checkininfo_show_ip","Y","Ob die IP-Adresse aus dem Userprofil im Checkininfo angezeitg werden soll.","yesno");
			$id = MysqlReadField("SELECT `id` FROM `".TblPrefix()."flip_config` WHERE `key` = 'checkininfo_subnetmask';");
			_DBUExecQuery("DELETE FROM `flip_config_values` WHERE (`config_id` = '$id');","Dropdownoptionen f&uuml;r die Subnetmask entfernt.");

		case(868):
			$message = MysqlReadField("SELECT `message` FROM ".TblPrefix()."flip_sendmessage_message WHERE (`name`='server_add')", "message");
			_DBUExecQuery("UPDATE ".TblPrefix()."flip_sendmessage_message SET `message`='".str_replace("\$at_party", "\$atparty", $message)."' WHERE (`name`='server_add')", "In Nachricht 'server_add' $at_party durch $atparty ersetzt.");

		case(871):
			$tournament_games_id = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_table_tables WHERE `name`='TOURNAMENT_GAMES'", "id");
			_DBUExecQuery("UPDATE flip_table_entry SET `key`='cc' WHERE `key`='ccg' AND table_id='$tournament_games_id'", "TOURNAMENT_GAMES: Den Key 'ccg' in 'cc' ge&auml;ndert um einen korrekten NGL-Export zu erm&ouml;glichen.");
			_DBUExecQuery("UPDATE flip_table_entry SET `value`='FIFA 2005', `display`='FIFA 2005', `display_code`='FIFA 2005' WHERE `key`='fifa' AND table_id='$tournament_games_id'", "TOURNAMENT_GAMES: Dem Key 'fifa' den Wert 'FIFA 2005' zugewiesen um einen korrekten NGL-Export zu erm&ouml;glichen.");
			_DBUExecQuery("UPDATE flip_table_entry SET `value`='NFSU2 Circuit', `display`='NFSU2 Circuit', `display_code`='NFSU2 Circuit' WHERE `key`='nfsu' AND table_id='$tournament_games_id'", "TOURNAMENT_GAMES: Dem Key 'nfsu' den Wert 'NFSU2 Circuit' zugewiesen um einen korrekten NGL-Export zu erm&ouml;glichen.");
			_DBUExecQuery("UPDATE flip_table_entry SET `key`='sum' WHERE `key`='dsum' AND table_id='$tournament_games_id'", "TOURNAMENT_GAMES: Den Key 'dsum' in 'sum' ge&auml;ndert um einen korrekten NGL-Export zu erm&ouml;glichen.");
			_DBUExecQuery("UPDATE flip_table_entry SET `key`='hl2_dm' WHERE `key`='hl2' AND table_id='$tournament_games_id'", "TOURNAMENT_GAMES: Den Key 'hl2' in 'hl2_dm' ge&auml;ndert um einen korrekten NGL-Export zu erm&ouml;glichen.");

		case(875):
		case(877):
			_DBUSetConfig('lanparty_partlist_addgroups','orga','Eine mit Komma getrennte Liste der Gruppen, deren Mitglieder zus&auml;tzlich in der Teilnehmerliste aufgelistet werden.','string');

			$id = _DBUExecInsert('flip_table_tables', array('name' => 'lanparty_status',
					'edit_right' => GetRightID('edit_public_text'),
					'description' => 'Der Anmeldestatus zu der Lanparty (Achtung: zu jedem Status muss eine gleichnamige Gruppe existieren!)'
					), 'Tabelle lanparty_status in flip_table_tables hinzugef&uuml;gt.');
			_DBUExecInsert('flip_table_entry', array('table_id'	  => $id,
					'key'		  => 'status_registered',
					'value'	      => 'angemeldet',
					'display'	  => '<span style="color:#bb0000">angemeldet</span>',
					'display_code' => '<span style="color:#bb0000">angemeldet</span>',
					), 'status_registered zu lanparty_status hinzugef&uuml;gt.');
			_DBUExecInsert('flip_table_entry', array('table_id'	  => $id,
					'key'		  => 'status_paid',
					'value'	      => 'bezahlt',
					'display'	  => '<span style="color:#007700">bezahlt</span>',
					'display_code' => '<span style="color:#007700">bezahlt</span>',
					), 'status_paid zu lanparty_status hinzugef&uuml;gt.');
			_DBUExecInsert('flip_table_entry', array('table_id'	  => $id,
					'key'		  => 'status_checked_in',
					'value'	      => 'eingecheckt',
					'display'	  => '<span style="color:#bb880b">eingecheckt</span>',
					'display_code' => '<span style="color:#bb880b">eingecheckt</span>',
					), 'status_checked_in zu lanparty_status hinzugef&uuml;gt.');
			_DBUExecInsert('flip_table_entry', array('table_id'	  => $id,
					'key'		  => 'status_checked_out',
					'value'	      => 'ausgechecket',
					'display'	  => '<span style="color:#0000bb">ausgecheckt</span>',
					'display_code' => '<span style="color:#0000bb">ausgecheckt</span>',
					), 'status_checked_out zu lanparty_status hinzugef&uuml;gt.');
			_DBUExecInsert('flip_table_entry', array('table_id'	  => $id,
					'key'		  => 'status_online',
					'value'	      => 'online',
					'display'	  => 'online',
					'display_code' => 'online',
					), 'status_online zu lanparty_status hinzugef&uuml;gt.');
			_DBUExecInsert('flip_table_entry', array('table_id'	  => $id,
					'key'		  => 'status_offline',
					'value'	      => 'offline',
					'display'	  => 'offline',
					'display_code' => 'offline',
					), 'status_offline zu lanparty_status hinzugef&uuml;gt.');
			_DBUExecInsert('flip_table_entry', array('table_id'	  => $id,
					'key'		  => 'orga',
					'value'	      => 'orga',
					'display'	  => 'orga',
					'display_code' => 'orga',
					), 'orga zu lanparty_status hinzugef&uuml;gt.');

			$id = MysqlReadField("SELECT `id` FROM `".TblPrefix()."flip_menu_blocks` WHERE `caption` = \'Checklist\';",0,true);
			if($id)
			{
				_DBUExecInsert('flip_menu_links',array('block_id' => $id,
					'caption' => 'status',
					'frameurl' => 'lanparty.php?frame=smallstatusbar',
					'order' => '123324324',
					'enabled' => 1
					),'Statusbar ins Men&uuml; hinzugef&uuml;gt.');
				_DBUExecInsert('flip_menu_links',array('block_id' => $id,
					'caption' => 'spacer',
					'text' => '<hr />',
					'order' => '123324323',
					'enabled' => 1
					),'Statusbar ins Men&uuml; hinzugef&uuml;gt.');
			}

		case(882):
			_DBUExecQuery("UPDATE `flip_content_text` SET `name` = 'banktransfer_main' WHERE (`name` = 'lanparty_banktransfer');","Contenttext lanparty_banktransfer in banktransfer_main umbenannt.");
			_DBUExecInsert("flip_content_text", array("group_id"   => 3,
					"name"       => "banktransfer_not_allowed",
					"view_right" => 0,
					"edit_right" => GetRightID("edit_public_text"),
					"caption"    => "&uuml;berweisung - Zugriff verweigert",
					"text"       => "<p>\r\n  An dieser Stelle findest du &uuml;blicher Weise die Kontodaten f&uuml;r den Unkostenbeitrag.<br>\r\n  Diese Daten werden \r\n  allerdings erst angezeigt, sobald du dich <a href=\"user.php?frame=login\">eingeloggt</a> \r\n  und zu der <a href=\"lanparty.php?frame=register\">Lanparty angemeldet</a> hast.\r\n</p>",
			),"Content-Text banktransfer_not_allowed hinzugef&uuml;gt.");
			 
			_DBUAddRight("banktransfer_view", "Erlaubt es, die Kontodaten f&uuml;r den Unkostenbeitrag zur lanparty ein zu sehen.", array("orga", "status_registered"));

			_DBUSetConfig('banktransfer_account_no',        '1234567','Die Kontonummer des Kontos.','string');
			_DBUSetConfig('banktransfer_account_owner_name','Dagobert Duck','Der Name des Inhabers des Kontos.','string');
			_DBUSetConfig('banktransfer_account_bank_code', '66600023','Die Bankleitzahl des Kontos.','string');
			_DBUSetConfig('banktransfer_account_bank_name', 'Geldspeicherbank','Der Name der Bank des Kontos.','string');
			_DBUSetConfig('banktransfer_currency',          'Taler','Die W&auml;hrung in welcher der Beitrag auf der Lanpartykonto &uuml;berwiesen werden soll.','string');
			_DBUSetConfig('banktransfer_subject_prefix',    'Lanparty','Ein Prefix, welcher in den Betreff der &uuml;berweisung &uuml;bernommen wird.','string');
			_DBUSetConfig('banktransfer_money_amount',      '13,00','Der Geldbetrag, der &uuml;berwiesen werden soll.','string');

			_DBUExecQuery("UPDATE `flip_menu_links` SET `link` = 'banktransfer.php' WHERE (`link` = 'text.php?name=lanparty_banktransfer');","Men&uuml;link text.php?name=lanparty_banktransfer auf banktransfer.php umgestellt.");

		case(891):
			//column leserechte ge&auml;ndert

		case(894):
			$tournament_games_id = MysqlReadField("SELECT `id` FROM ".TblPrefix()."flip_table_tables WHERE `name`='TOURNAMENT_GAMES'", "id");
			if($tournament_games_id > 0)
			{
				_DBUExecQuery("UPDATE flip_table_entry SET `key`='fc' WHERE `key`='fcry' AND table_id='$tournament_games_id'", "flip_tables_entry (TOURNAMENT_GAMES): Abk&uuml;rzung fcry in fc ge&auml;ndert um NGL-kompatibel zu sein.");
				_DBUExecQuery("UPDATE flip_table_entry SET `key`='tm' WHERE `key`='trak' AND table_id='$tournament_games_id'", "flip_tables_entry (TOURNAMENT_GAMES): Abk&uuml;rzung trak in tm ge&auml;ndert um NGL-kompatibel zu sein.");
				_DBUExecQuery("UPDATE flip_table_entry SET `key`='bfme' WHERE `key`='sum' AND table_id='$tournament_games_id'", "flip_tables_entry (TOURNAMENT_GAMES): Abk&uuml;rzung trak in tm ge&auml;ndert um NGL-kompatibel zu sein.");
				_DBUExecQuery("UPDATE flip_table_entry SET `key`='hl2', value='Half-Life 2', display='Half-Life 2', display_code='Half-Life 2'
                         WHERE `key`='hl2_dm' AND table_id='$tournament_games_id'", "flip_tables_entry (TOURNAMENT_GAMES): Beschreibung von \"Half-Life 2 DeathMatch\" auf \"Half-Life 2\" ge&auml;ndert.");
			}

		case(896):
			_DBUSetConfig("news_perpage","10","Die Anzahl der Newseintr&auml;ge pro Seite.","integer");
			_DBUExecQuery("ALTER TABLE flip_config CHANGE `type` `type` VARCHAR( 32 ) DEFAULT 'longstring' NOT NULL","flip_config: default f&uuml;r type auf longstring gesetzt (kleingeschrieben).");

		case(905):
			_DBUExecInsert("flip_content_text", array("group_id"      => 1,
					"name"          => "seats_afterlegend",
					"edit_right"    => 2,
					"edit_user_id"  => SYSTEM_USER_ID,
					"edit_time"     => time()
			), "ContentText seats_afterlegend hinzugef&uuml;gt.");


		case(908):
			_DBUUpdateSubjectColumn("user","statistic_hide_status","yesno","In Onlineliste verbergen","view_personal_informations","edit_own_profile");

		case(909):
			LogDBUpdate("<span class=\"important\">neu</span>: In der Turnierliste werden die Turniere nach Liga sortiert. f&uuml;r die &uuml;berschriften sind Bilder (Logos) m&ouml;glich. Diese k&ouml;nnen in der DB (flip_content_image) unter dem Namen tournament_icon_NGL bzw. _WWCL oder _FUN vorhanden sein.");

		case(929):
			_DBUExecQuery("ALTER TABLE `flip_tournament_tournaments` ADD `treetype` ENUM( 'default', 'small', 'narrow' ) DEFAULT 'default' NOT NULL AFTER `game` ;", "Spalte 'treetype' zu flip_tournament_tournaments hinzugef&uuml;gt.");

		case(931):
		case(933):
			_DBUSetConfig("external_accessabledirs","","Die Verzeichnisse und ihre Modulnamen welche der Script external.php einbinden kann. Beispiel: 'mod1=/var/www/ mod2=tpl/default/'. Der Aufruf external.php?mod=mod1&amp;file=index.html w&uuml;rde jetzt die Datei /var/www/index.html einbinden.","longstring");

		case(940):
			_DBUExecQuery("ALTER TABLE `flip_tournament_servers` ADD UNIQUE `u_game_name` ( `game` , `name` ) ","Unique-Index &uuml;ber 'game' und 'name' in flip_tournament_servers angelegt.");
			_DBUExecQuery("ALTER TABLE `flip_tournament_servers` ADD UNIQUE (`ip`)","Unique-Index &uuml;ber 'ip' in flip_tournament_servers angelegt.");
			_DBUExecQuery("ALTER TABLE `flip_tournament_serverusage` DROP INDEX `match_id` , ADD UNIQUE `match_id` ( `match_id` ) ","Index 'match_id' in flip_tournament_serverusage auf UNIQUE ge&auml;ndert.");
			_DBUExecQuery("ALTER TABLE `flip_tournament_invite` ADD UNIQUE `userperteam` ( `user_id` , `team_id` ) ","Unique-Index &uuml;ber 'user_id' und 'team_id' in flip_tournament_invite angelegt.");
			_DBUExecQuery("ALTER TABLE `flip_tournament_invite` DROP INDEX `i_userid` ","Index 'i_userid' aus flip_tournament_invite entfernt.");

		case(946):
			_DBUExecQuery("ALTER TABLE `flip_tournament_tournaments` CHANGE `status` `status` ENUM( 'closed', 'open', 'start', 'grpgames', 'games', 'end' ) DEFAULT 'closed' NOT NULL ","Status 'start' (Aufw&auml;rmphase) der Spalte 'status' in flip_tournament_tournaments hinzugef&uuml;gt.");

		case(948):
			_DBUExecQuery("ALTER TABLE `flip_catering_kategorien` ADD `sichtbar` ENUM( '1', '0' ) NOT NULL AFTER `aktiv` ;","Spalte 'sichtbar' zu flip_catering_kategorien hinnzugef&uuml;gt.");
			_DBUExecQuery("ALTER TABLE `flip_catering_artikel` ADD `sichtbar` ENUM( '1', '0' ) NOT NULL AFTER `aktiv` ;","Spalte 'sichtbar' zu flip_catering_artikel hinzugef&uuml;gt.");

		case(952):
			_DBUExecInsert("flip_content_text", array("group_id"   => 3,
					"name"       => "lanparty_unregister",
					"view_right" => 0,
					"edit_right" => GetRightID("edit_public_text"),
					"caption"    => "Abmeldung",
					"text"       => "Ich m&ouml;chte doch nicht mehr teilnehmen und melde mich hiermit von der LAN-Party ab.<br />\r\n<br />\r\nDer Account bleibt bestehen.",
					"description"=> "Hier kannst du dich von der LAN-Party abmelden"
					),"Content-Text lanparty_unregister hinzugef&uuml;gt.");


		case(968):
			//Forumupdates -> Signaturen
			_DBUSetConfig("forum_signature_show","Y","Die Signaturen in den Foren anzeigen.","yesno");
			_DBUUpdateSubjectColumn("user","forum_signature","string","Deine Signatur im Forum","view_personal_informations","edit_own_profile");
			_DBUUpdateSubjectColumn("forum","signature_allow","yesno","Signaturen erlauben","view_personal_informations","edit_own_profile");
			LogDBUpdate("<span class=\"important\">Forum</span>: Im Forum gibt es jetzt Signaturen, die sich global oder einzel im Forum an/ausschalten lassen.");

		case(985):
			_DBUExecQuery("ALTER TABLE `flip_news_news` ADD `category` VARCHAR( 100 ) NOT NULL ;","Spalte 'category' zu flip_news_news hinzugef&uuml;gt");
			_DBUSetConfig("news_use_categories","N","Soll in den News eine Einordung der Artikel in Kategorien m&ouml;glich sein?","yesno");

			$id = _DBUExecInsert("flip_table_tables", array("name" => "news_category",
                                                "edit_right" => GetRightID("news_public"),
                                                "description"=> "Kategorien f&uuml;r die News.",
			),"news_category zu flip_table_tables hinzugef&uuml;gt.");
			if($id > 0)
			{
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $id,
					"key"		=> "",
					"value"		=> "keine Kategorie",
					"display"	=> "keine Kategorie",
					"display_code"	=> "keine Kategorie",
				), "Die Kategorie keine Kategorie wurde in tables->news_category eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $id,
					"key"		=> "lanparty",
					"value"		=> "LAN-Party",
					"display"	=> "LAN-Party",
					"display_code"	=> "LAN-Party",
				), "Die Kategorie LAN-Party wurde in tables->news_category eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $id,
					"key"		=> "community",
					"value"		=> "Community",
					"display"	=> "Community",
					"display_code"	=> "Community",
				), "Die Kategorie Community wurde in tables->news_category eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $id,
					"key"		=> "esport",
					"value"		=> "eSport",
					"display"	=> "eSport",
					"display_code"	=> "eSport",
				), "Die Kategorie eSport wurde in tables->news_category eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $id,
					"key"		=> "games",
					"value"		=> "Spiele",
					"display"	=> "Spiele",
					"display_code"	=> "Spiele",
				), "Die Kategorie Spiele wurde in tables->news_category eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $id,
					"key"		=> "hardware",
					"value"		=> "Hardware",
					"display"	=> "Hardware",
					"display_code"	=> "Hardware",
				), "Die Kategorie Hardware wurde in tables->news_category eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $id,
					"key"		=> "software",
					"value"		=> "Software",
					"display"	=> "Software",
					"display_code"	=> "Software",
				), "Die Kategorie Software wurde in tables->news_category eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $id,
					"key"		=> "party",
					"value"		=> "Party",
					"display"	=> "Party",
					"display_code"	=> "Party",
				), "Die Kategorie Party wurde in tables->news_category eingetragen.");
				_DBUExecInsert("flip_table_entry", array("table_id"	=> $id,
					"key"		=> "misc",
					"value"		=> "Verschiedenes",
					"display"	=> "Verschiedenes",
					"display_code"	=> "Verschiedenes",
				), "Die Kategorie Verschiedenes wurde in tables->news_category eingetragen.");
			}

		case(994):
			_DBUSetConfig("webmessage_menu_max_len","12","Wenn gr&ouml;&szlig;er 0, werden die Titel von Webmessages im Men&uuml; auf diese L&auml;nge beschnitten.","Integer");

		case(1003):
			_DBUSetConfig("forum_signatures_html_for","niemand","Sollen die Signaturen im Forum HTML beinhalten? (Alle/HTML-Poster/Niemand)","Dropdown");
			$id = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_config WHERE `key`='forum_signatures_html_for'", "id");
			if($id > 0)
			{
				_DBUExecInsert("flip_config_values", array("config_id"	=> $id,
					"value"	=> "niemand"
				), "Configwert \"niemand\" f&uuml;r forum_signatures_html_for hinzugef&uuml;gt.");
				_DBUExecInsert("flip_config_values", array("config_id"	=> $id,
					"value"	=> "html-poster"
				), "Configwert \"html-poster\" f&uuml;r forum_signatures_html_for hinzugef&uuml;gt.");
				_DBUExecInsert("flip_config_values", array("config_id"	=> $id,
					"value"	=> "alle"
				), "Configwert \"alle\" f&uuml;r forum_signatures_html_for hinzugef&uuml;gt.");
			}

		case(1005):
			_DBUUpdateMenu("Checklist","Countdown","lanparty.php?frame=countdown_small",0,"So lange musst du noch warten.",true);

		case(1006):
			_DBUExecQuery("ALTER TABLE `flip_news_news` ADD `on_top` ENUM( '0', '1' ) NOT NULL AFTER `mtime` ;","Spalte 'on_top' zur Tabelle 'flip_news_news' hinzugef&uuml;gt.");
			_DBUSetConfig("statistic_usersonline_show_idle","Y","Soll die Zeit angezeigt werden, seit dem der User zu letzt die Seite aufgerufen hat (\"Ja\") oder seit dem er eingeloggt ist (\"Nein\")?","YesNo");

		case(1008):
			//Poll im Men&uuml;
		case(1012):

		case(1013):
			_DBUUpdateSubjectColumn("server","owner_id","Integer","Besitzer",GetRightID("view_personal_informations"),GetRightID("server_admin")); //Schreibrecht ge&auml;ndert
			_DBUUpdateSubjectColumn("server","ipcount","Integer","Anzahl Adressen",GetRightID("view_personal_informations"),GetRightID("server_edit"));
			_AddRight("networkadmin","view_personal_informations","servers");
			LogDBUpdate("Der Gruppe 'networkadmin' wurde das Recht 'view_personal_informations' &uuml;ber die Gruppe 'servers' gegeben.");

		case(1015):
			_DBUExecQuery("CREATE TABLE `flip_webmessage_folder` (
                      `id` INT NOT NULL AUTO_INCREMENT ,
                      `mtime` TIMESTAMP NOT NULL ,
                      `user_id` INT NOT NULL ,
                      `name` VARCHAR( 32 ) NOT NULL ,
                      PRIMARY KEY ( `id` ) ,
                      INDEX ( `mtime` )
                     );
                    ","Tabelle flip_webmessage_folder wurde erstellt.");
			_DBUExecQuery("ALTER TABLE `flip_webmessage_message` ADD `folder_id` INT;","Spalte 'folder_id' zur Tabelle flip_webmessage_message hinzugef&uuml;gt.");

		case(1022):
			_DBUExecQuery("CREATE TABLE `flip_seats_blocks` (
                      `id` INT NOT NULL AUTO_INCREMENT ,
                      `mtime` TIMESTAMP NOT NULL ,
                      `view_right` int(11) NOT NULL default '0',
                      `caption` VARCHAR( 32 ) NOT NULL ,
                      `description` TEXT NOT NULL ,
                      `background` LONGBLOB,
                      PRIMARY KEY ( `id` ) ,
                      INDEX ( `mtime` )
                     );
                    ","Tabelle flip_seats_blocks wurde erstellt.");
			_DBUExecQuery("ALTER TABLE `flip_seats_seats` ADD `block_id` INT DEFAULT '1' NOT NULL AFTER `mtime` ;","Spalte 'block_id' zu flip_seats_seats hinzugef&uuml;gt.");
			_DBUExecQuery("ALTER TABLE `flip_seats_seats` ADD INDEX ( `block_id` ) ;","Index f&uuml;r die Spalte 'block_id' in flip_seats_seats angelegt.");
			_DBUUpdateByKey("flip_content_text",array("name"=>"seats_block"),array("name"=>"seats_main"),"Der ContentText 'seats_main' wurde umbenannt in 'seats_block'.");
			_DBUExecInsert("flip_content_text", array("group_id"   => 3,
				"name"       => "seats_main",
				"view_right" => 0,
				"edit_right" => GetRightID("edit_public_text"),
				"caption"    => "Sitzplan&uuml;bersicht",
				"text"       => "Wir haben die ganze Halle zur Verf&uuml;gung. Ihr k&ouml;nnt euch auf dem <a href=\"seats.php?frame=block&amp;blockid=1\">detailierten Sitzplan</a> einen Platz reservieren und sehen, wer wo sitzt.",
			),"Content-Text seats_main hinzugef&uuml;gt.");
			/* vorhandenen Sitzplan als Block erstellen */
			//$User muss vorhanden sein
			InitSession();SetGlobalUser();
			require_once("mod/mod.image.php");
			$background = new DataImage(ConfigGet("seats_baseplan"));
			//search and replace from phpmyadmin v2.7.0 libraries/export/sql.php|PMA_exportData()
			$search       = array("\x00", "\x0a", "\x0d", "\x1a");
			$replace      = array('\0', '\n', '\r', '\Z');
			$background = str_replace($search, $replace, mysql_escape_string( $background->getData() ));
			_DBUExecQuery("INSERT INTO `flip_seats_blocks` ( `id` , `mtime` , `view_right` , `caption` , `description` , `background`)
				VALUES ('', NOW( ) , '0', 'Sitzplan der Halle', 'Die gesamte Halle steht euch zur Verf&uuml;gung.', '$background');"
				,"Ein Sitzblock 'Sitzplan der Halle' wurde erstellt.");
			$seatsblockfromold = true;

		case(1023):
			_DBUExecQuery("ALTER TABLE `flip_seats_blocks`
      		 ADD `background_tmp` LONGBLOB, 
             ADD `topleft_x` INT NOT NULL,
             ADD `topleft_y` INT NOT NULL ,
             ADD `scale` FLOAT NOT NULL ,
             ADD `imagedir` varchar(255) NOT NULL default '',
             ADD `link` VARCHAR( 255 ) NOT NULL,
             ADD `cords` VARCHAR( 255 ) NOT NULL ;","Diverse Spalten zur Tabelle flip_seats_blocks hinzugef&uuml;gt");
			_DBUSetConfig("seats_images_large","tpl/default/images/seats/classic/","Das Verzeichnis, in dem sich die \"grossen\" Bilder fuer den Sitzplan befinden. Diese werden z.B. in der Ansicht fuer einen einzelnen Sitzplatz angezeigt.","longstring");
			_DBUUpdateByKey("flip_config",array("default_value"=>"tpl/default/images/seats/simple_8x5/"),array("key"=>"seats_imagedir"),"Der Defaultwert von seats_imagedir ist jetzt tpl/default/images/seats/simple_8x5/.");

		case(1029):
			_DBUExecQuery("ALTER TABLE `flip_seats_blocks` CHANGE `imagedir` `imagedir_block` VARCHAR( 255 ) NOT NULL","Spalte imagedir in flip_seats_blocks zu imagedir_block umbenannt.");
			_DBUExecQuery("ALTER TABLE `flip_seats_blocks` ADD `imagedir_overview` VARCHAR( 255 ) NOT NULL AFTER `imagedir_block`","Spalte imagedir_overview zu flip_seats_blocks hinzugef&uuml;gt");
			_DBUExecQuery("UPDATE `flip_seats_blocks` SET `imagedir_block` = 'tpl/default/images/seats/classic/' WHERE(LENGTH(`imagedir_block`) = 0);","imagedir_block auf 'tpl/default/images/seats/classic/' gesetzt, wenn leer.");
			_DBUExecQuery("UPDATE `flip_seats_blocks` SET `imagedir_overview` = 'tpl/default/images/seats/simple_8x5/' WHERE(LENGTH(`imagedir_overview`) = 0);","imagedir_overview auf 'tpl/default/images/seats/simple_8x5/' gesetzt, wenn leer.");
			//_DBUExecQuery("DELETE FROM `flip_config` WHERE (`key` = 'seats_imagedir');","Configeintrag seats_imagedir gel&ouml;scht");
			if($seatsblockfromold) {
				require_once("mod/mod.seats.php");
				_DBUUpdateByKey("flip_seats_blocks", array("scale"=>1), array("id"=>1), "Bisheriger Sitzplan wird 1:1 als Block dargestellt.");
				RedrawSeats();
			}

		case(1037):
			_DBUExecQuery("ALTER TABLE `flip_poll_list` CHANGE `public` `view_right` INT DEFAULT '0' NOT NULL","Spalte 'public' in 'view_right' (INT) ge&auml;ndert von Tabelle 'flip_poll_list'.");
			_DBUExecQuery("UPDATE `flip_poll_list` SET `view_right`='0' WHERE `view_right`='2'","'view_right' von public-Polls auf 0 gesetzt.");
			_DBUExecQuery("UPDATE `flip_poll_list` SET `view_right`='".GetRightID("view_internal_informations")."' WHERE `view_right`='1'","'view_right' von non-public-Polls auf 'view_internal_informations' gesetzt.");

		case(1049):
			$links = MysqlReadArea("SELECT id, link FROM ".TblPrefix()."flip_menu_links WHERE `link` LIKE '%&%' AND `link` NOT LIKE '%&amp;%'");
			foreach($links AS $alink)
			MysqlWriteByID(TblPrefix()."flip_menu_links", array("link"=>str_replace("&", "&amp;", $alink["link"])), $alink["id"]);

		case(1054):
			//changed content_texts to htmlentities

		case(1055):
			_DBUSetConfig("banktransfer_last_check","","Das Datum an dem zu letzt die &uuml;berweisungen gepr&uuml;ft wurden.","date");

		case(1058):
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_session_data` ADD PRIMARY KEY ( `id` )", "Spalte 'id' in flip_session_data zum Prim&auml;rSchl&uuml;ssel gemacht.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_session_data` DROP INDEX `i_id`", "Index f&uuml;r Spalte 'id' aus flip_session_data entfernt.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_session_data` ADD `donext` VARCHAR( 128 );", "Spalte 'donext' in flip_session_data hinzugef&uuml;gt.");

		case(1064):
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_user_column` (`type`, `access`, `name`, `val_type`, `required`, `caption`, `required_view_right`, `required_edit_right`, `comment`, `hint`, `sort_index`, `callback_read`, `callback_write`) VALUES ('user', 'callback', 'age', 'Integer', 'N', 'Alter', '".GetRightID("view_personal_informations")."', '-1', 'berechnet aus dem Geburtsdatum', '', 11, 'mod/mod.user.php|UserGetAge', NULL);","Die Usereigenschaft 'age' [callback] hinzugef&uuml;gt.");
			_DBUAddRight("user_checkin_edit", "Erlaubt es, den Checkin zu bearbeiten", "useradmin");
			_DBUExecQuery("CREATE TABLE `".TblPrefix()."flip_checkin_list` (
                       `id` INT NOT NULL AUTO_INCREMENT ,
                       `order` MEDIUMINT NOT NULL ,
                       `check_not` ENUM( '0', '1' ) NOT NULL ,
                       `status` ENUM( 'aktiv', 'optional', 'deaktiviert' ) NOT NULL ,
                       `button_color` varchar( 7 ),
                       `input_type` VARCHAR( 32 ) NOT NULL ,
                       `action_type` VARCHAR( 32 ) NOT NULL ,
                       `action_name` VARCHAR( 50 ) NOT NULL ,
                       `action_value` VARCHAR( 255 ) NOT NULL ,
                       `check_type` VARCHAR( 32 ) NOT NULL ,
                       `check_name` VARCHAR( 50 ) NOT NULL ,
                       `check_value` VARCHAR( 255 ) NOT NULL ,
                       PRIMARY KEY ( `id` )
                     );", "Tabelle 'flip_checkin_list' erstellt.");
			_DBUExecQuery("UPDATE `".TblPrefix()."flip_menu_links` SET link='checkin.php' WHERE link='user.php?frame=checkin'","Men&uuml;links von user.php?frame=checkin auf checkin.php ge&auml;ndert.");
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_checkin_list` VALUES (1, 1, '0', 'aktiv', NULL, 'text', 'SetProperty', 'status_name', '', 'none', '', '');","CheckinItem hinzugef&uuml;gt.");
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_checkin_list` VALUES (2, 1, '0', 'aktiv', NULL, 'text', 'SetProperty', 'birthday', '', 'none', '', '');","CheckinItem hinzugef&uuml;gt.");
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_checkin_list` VALUES (3, 1, '0', 'aktiv', NULL, 'text', 'SetProperty', 'clan', '', 'none', '', '');","CheckinItem hinzugef&uuml;gt.");
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_checkin_list` VALUES (4, 1, '0', 'aktiv', NULL, 'text', 'SetProperty', 'email', '', 'none', '', '');","CheckinItem hinzugef&uuml;gt.");
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_checkin_list` VALUES (5, 1, '0', 'aktiv', '#FFFF88', 'button', 'SetStatus', 'registered', '', 'UserRight', 'status_registered', '');","CheckinItem hinzugef&uuml;gt.");
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_checkin_list` VALUES (6, 2, '0', 'aktiv', NULL, 'button', 'SetProperty', 'is_adult', 'Y', 'UserProperty', 'is_adult', 'Y');","CheckinItem hinzugef&uuml;gt.");
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_checkin_list` VALUES (7, 3, '0', 'aktiv', NULL, 'button', 'SetStatus', 'paid', '', 'UserRight', 'status_paid', '');","CheckinItem hinzugef&uuml;gt.");
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_checkin_list` VALUES (8, 4, '1', 'aktiv', NULL, 'warning', 'none', '', '', 'UserProperty', 'seat', '');","CheckinItem hinzugef&uuml;gt.");
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_checkin_list` VALUES (9, 5, '0', 'aktiv', NULL, 'text', 'SetProperty', 'seat', '', 'none', '', '');","CheckinItem hinzugef&uuml;gt.");
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_checkin_list` VALUES (10, 5, '0', 'aktiv', '#88FF88', 'button', 'SetStatus', 'checked_in', '', 'UserRight', 'status_checked_in', '');","CheckinItem hinzugef&uuml;gt.");
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_checkin_list` VALUES (11, 6, '0', 'aktiv', '#8888FF', 'button', 'SetStatus', 'checked_out', '', 'UserRight', 'status_checked_out', '');","CheckinItem hinzugef&uuml;gt.");
			LogDBUpdate("Der Checkin ist nun unter 'checkin.php' und nicht mehr unter 'user.php?frame=checkin' zu finden. Bitte entsprechende Links anpassen. (Der alte Checkin funktioniert weiterhin)");


		case(1073):
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_session_data` CHANGE `donext` `donext` VARCHAR( 255 ) NOT NULL","donext in flip_session_data angepasst");

		case(1077):
			_DBUExecQuery("UPDATE flip_content_text SET caption='&uuml;berweisung' WHERE caption='&Uuml;berweisung'","Die &uuml;berschrift von banktransfer.php (&Uuml;berschrift) in '&uuml;berschrift' ge&auml;ndert.");
			_DBUExecQuery("UPDATE flip_content_text SET caption='&uuml;berweisung - Zugriff verweigert' WHERE caption='&Uuml;berweisung - Zugriff verweigert'","Die &uuml;berschrift von banktransfer.php (&Uuml;berweisung - Zugriff verweigert) in '&uuml;berweisung - Zugriff verweigert' ge&auml;ndert.");

		case(1078):
		case(1082):
			_DBUExecQuery("UPDATE flip_user_right SET `right`='checkin' WHERE `right`='user_checkin'", "Das Recht 'user_checkin' in 'checkin' umbenannt");
			_DBUExecQuery("UPDATE flip_user_right SET `right`='checkin_edit' WHERE `right`='user_checkin_edit'", "Das Recht 'user_checkin_edit' in 'checkin_edit' umbenannt");

		case(1086):
			_DBUExecQuery("ALTER TABLE `flip_checkin_list` ADD `mtime` TIMESTAMP NOT NULL AFTER `id` ;","Spalte 'mtime' zu flip_checkin_list hinzugef&uuml;gt.");
			_DBUExecQuery("ALTER TABLE `flip_config_values` ADD `mtime` TIMESTAMP NOT NULL AFTER `id` ;","Spalte 'mtime' zu flip_config_values hinzugef&uuml;gt.");

		case(1091):
			_DBUExecQuery("UPDATE flip_checkin_list SET check_not='1' WHERE check_name='seat'", "Checkin: Sitzplatzpr&uuml;fung negiert.");
			_DBUExecQuery("UPDATE flip_checkin_list SET action_name='age' WHERE action_name='birthday' AND input_type='text'", "Checkin: Geburtstagsanzeige durch Altersanzeige ausgetauscht.");

		case(1092):
			_DBUUpdateSubjectColumn("user","dbxfer_userdata","String","DBXFer Userdata",GetRightID("_infinite_"),GetRightID("_infinite_"),"Wird intern vom DBXFer verwedet, um Eingabewerte von der letzten Verwendung zu speichern.");

		case(1115):
			_DBUExecQuery("ALTER TABLE `flip_seats_blocks` ADD `is_adult` TINYINT NOT NULL;", "Spalte is_adult zu flip_seats_blocks hinzugef&uuml;gt.");
			_DBUUpdateSubjectColumn("user","seats_is_adult","checkbox","Sitzt im &uuml;18-Bereich",GetRightID("view_internal_profile"),GetRightID("_infinite_"),"","mod/mod.seats.php|SeatsIsAdult");
			if($seatsblockfromold) {
				require_once("mod/mod.seats.php");
				SeatsCalcBlockCords(1); //kann erst gemacht werden, wenn die Tabelle flip_seats_blocks die aktuelle Struktur hat!
			}

		case(1123):
			_DBUExecQuery("ALTER TABLE `flip_tournament_tournaments` DROP `fun`","Spalte 'fun' aus flip_tournament_tournaments entfernt.");
			_DBUExecQuery("ALTER TABLE `flip_tournament_tournaments` ADD `group_id` INT( 11 ) NOT NULL AFTER `treetype` ;","Spalte 'group' zu flip_tournament_tournaments hinzugef&uuml;gt.");
			_DBUExecQuery("CREATE TABLE `flip_tournament_groups` (
                       `id` INT NOT NULL AUTO_INCREMENT ,
                       `mtime` timestamp(14) NOT NULL,
                       `order` smallint(6) NOT NULL default '0',
                       `coins` int(11) NOT NULL default '0',
                       `name` VARCHAR( 32 ) NOT NULL ,
                       `image` BLOB,
                       PRIMARY KEY ( `id` ),
                       UNIQUE KEY `order` (`order`),
                       KEY `mtime` (`mtime`)
                     );","Tabelle flip_tournament_groups hinzugef&uuml;gt.");

		case(1124):
			$dbexport  = _DBUExecInsert("flip_user_subject",array("type" => "user", "name" => "dbexport", "email" => "tux@cronjob.de"),"Einen Benutzer namens dbexport erstellt.");
			$sysacc    = _DBUExecInsert("flip_user_subject",array("type" => "group", "name" => "sysaccounts", "email" => null),"Eine Gruppe namens sysaccounts erstellt.");
			$webmaster = GetSubjectID("webmaster");
			$desc = _GetColumnInfo("user");
			$desc = $desc["description"]["id"];

			_DBUExecInsert("flip_user_data",array("subject_id" => $sysacc,   "column_id" => $desc, "val" => "Benutzer, die keine Personen sind. Beispielsweise ein Script, der den DBExport des FLIPs herunterl&auml;d."),"Beschreibung zur Gruppe sysaccounts hinzugef&uuml;gt");
			_DBUExecInsert("flip_user_data",array("subject_id" => $dbexport, "column_id" => $desc, "val" => "&uuml;ber diesen Account kann sich z.B. ein &uuml;ber einen Cronjob angesto&szlig;enes Script ins FLIP einloggen, um einen SQL-Datenbankdump herunter zu laden."),"Beschreibung zum User sdbexport hinzugef&uuml;gt");

			_DBUAddRight("dbexport_create_dump", "Erlaubt es, die gesamte Datenbank als SQL-Dump zu exportieren.", array("webmaster","dbexport"));
			_DBUUpdateMenu("webmaster","DBExport","dbexport.php",GetRightID("dbexport_create_dump"),"Der DBExport kann einen SQL-Dump der gesamten Datenbank erstellen.");

			_DBUExecInsert("flip_user_groups", array("child_id" => $dbexport, "parent_id" => $sysacc),"User dbexport zur Gruppe sysaccounts hinzugef&uuml;gt."); 

			_DBUExecInsert("flip_user_rights", array("owner_id" => $webmaster, "controled_id" => $sysacc, "right_id" => GetRightID("edit_internal_profile")),"Recht edit_internal_profile zu webmaster &uuml;ber sysaccounts hinzugef&uuml;gt.");
			_DBUExecInsert("flip_user_rights", array("owner_id" => $webmaster, "controled_id" => $sysacc, "right_id" => GetRightID("edit_own_profile")),"Recht edit_own_profile zu webmaster &uuml;ber sysaccounts hinzugef&uuml;gt.");
			_DBUExecInsert("flip_user_rights", array("owner_id" => $webmaster, "controled_id" => $sysacc, "right_id" => GetRightID("view_internal_profile")),"Recht view_internal_profile zu webmaster &uuml;ber sysaccounts hinzugef&uuml;gt.");
			_DBUExecInsert("flip_user_rights", array("owner_id" => $webmaster, "controled_id" => $sysacc, "right_id" => GetRightID("view_personal_informations")),"Recht view_personal_informations zu webmaster &uuml;ber sysaccounts hinzugef&uuml;gt.");

		case(1131):
			_DBUExecQuery("ALTER TABLE `flip_checkin_list` ADD INDEX ( `mtime` )","Index f&uuml;r 'mtime' in flip_checkin_list hinzugef&uuml;gt"); 
			_DBUExecQuery("ALTER TABLE `flip_config_values` ADD INDEX ( `mtime` )","Index f&uuml;r 'mtime' in flip_config_values hinzugef&uuml;gt"); 
			_DBUExecQuery("ALTER TABLE `flip_tournament_servers` ADD `mtime` TIMESTAMP NOT NULL AFTER `id` ;","Spalte 'mtime' zu flip_tournament_servers hinzugef&uuml;gt.");
			_DBUExecQuery("ALTER TABLE `flip_tournament_servers` ADD INDEX ( `mtime` )","Index f&uuml;r 'mtime' in flip_tournament_servers hinzugef&uuml;gt"); 
			_DBUExecQuery("ALTER TABLE `flip_tournament_serverusage` ADD `mtime` TIMESTAMP NOT NULL AFTER `id` ;","Spalte 'mtime' zu flip_tournament_serverusage hinzugef&uuml;gt.");
			_DBUExecQuery("ALTER TABLE `flip_tournament_serverusage` ADD INDEX ( `mtime` )","Index f&uuml;r 'mtime' in flip_tournament_serverusage hinzugef&uuml;gt");
			_DBUSetConfig("tournament_losersubmit","Y","Nur Verlierer darf Ergebnis eintragen (Y/N)","YesNo");
			if(!MysqlReadRow("SELECT * FROM ".TblPrefix()."flip_tournament_groups", true))
			{
				$gid = _DBUExecInsert(TblPrefix()."flip_tournament_groups", array("order"=>1,"coins"=>0,"name"=>"FUN-Turniere"), "Eine Turniergruppe 'FUN-Turniere' hinzugef&uuml;gt.");
				_DBUExecQuery("UPDATE `".TblPrefix()."flip_tournament_tournaments` SET group_id='$gid'","Alle Turniere der Gruppe 'FUN-Turniere' zugeordnet.");
			}

		case(1132):
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_ads` ADD `image_mtime` TIMESTAMP NOT NULL AFTER `image`;","Spalte image_mtime zu flip_sponsor_ads hinzugef&uuml;gt.");
			_DBUExecQuery("UPDATE `".TblPrefix()."flip_sponsor_ads` SET `image_mtime` = null;","Spalte image_mtime mit dem aktuellen Datum initialisiert.");

		case(1137):
			$badnames = MysqlReadCol("SELECT e.id, e.value FROM ".TblPrefix()."flip_table_tables t, ".TblPrefix()."flip_table_entry e WHERE t.name='TOURNAMENT_BADNAMES' AND e.table_id=t.id", "value", "id");
			foreach($badnames AS $id=>$badname)
			{
				$badname = addcslashes($badname, "|.[()+?*\\"); //escape regexp-chars
				_DBUExecQuery("UPDATE `".TblPrefix()."flip_table_entry` SET value='$badname' WHERE id='$id'","Den Wert in flip_table TOURNAMENT_BADNAMES '$badname' regexp-escaped.");
			}

		case(1138):
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_user_subject` ADD INDEX `i_email` ( `email` )","Tabellenindex optimiert");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_user_subject` ADD INDEX `i_name` ( `name` ) ","Tabellenindex optimiert");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_session_data` DROP INDEX `i_create` ","Tabellenindex optimiert");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_session_data` DROP INDEX `mtime` ","Tabellenindex optimiert");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_ads` ADD INDEX `i_asad_type` ( `show_as_ad` , `type` ) ","Tabellenindex optimiert");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_webmessage_message` ADD INDEX `i_owner_status_deleted` ( `owner_id` , `status` , `owner_deleted` )","Tabellenindex optimiert");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_poll_users` ADD INDEX `i_poll_user` ( `poll_id` , `user_id` )  ","Tabellenindex optimiert");

		case(1144):
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_tournament_groups` DROP `coins` ","Spalte 'coins' aus flip_tournament_groups entfernt.");
			_DBUExecQuery("CREATE TABLE `".TblPrefix()."flip_tournament_coins` (
                       `id` INT NOT NULL AUTO_INCREMENT ,
                       `mtime` timestamp(14) NOT NULL,
                       `maxcoins` MEDIUMINT NOT NULL ,
                       `currency` VARCHAR( 20 ) DEFAULT 'Coins' NOT NULL ,
                       PRIMARY KEY ( `id` ),
                       UNIQUE KEY `currency` (`currency`),
                       KEY `mtime` (`mtime`)
                     );","Tabelle flip_tournament_coins hinzugef&uuml;gt.");
			$coin_defaultname = (ConfigGet("tournament_coinname") != "") ? ConfigGet("tournament_coinname") : "Coins" ;
			$coin_id = _DBUExecInsert(TblPrefix()."flip_tournament_coins", array("maxcoins"=>ConfigGet("tournament_coins"), "currency"=>$coin_defaultname), "Coins ('$coin_defaultname') in flip_tournament_coins hinzugef&uuml;gt");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_tournament_tournaments` ADD `coin_id` INT AFTER `group_id` ;","Spalte 'coin_id' zu flip_tournament_tournaments hinzugef&uuml;gt.");
			_DBUExecQuery("UPDATE `".TblPrefix()."flip_tournament_tournaments` SET coin_id='$coin_id'","Alle Turniere der neuen Coingruppe hinzugef&uuml;gt.");

		case(1152):
			_DBUExecQuery("UPDATE `".TblPrefix()."flip_user_right` SET `right`='server_edit_own', `description`='Erlaubt es, die Server zu besitzen und deren Eigenschaften zu bearbeiten.' WHERE (`right`='server_edit');","Recht server_edit in server_edit_own umbenannt.");
			GetRights(true); // cache leeren
			$g = new Group("registered");
			$g->addRight("server_edit_own");

			_DBUSetConfig("server_gateway","10.10.0.1","Dies ist der Defaultwert f&uuml;r die Gateway-IP der G&auml;steserver.","IP");
			_DBUSetConfig("server_subnetmask","255.255.0.0","Die Default-Subnetmask f&uuml;r die G&auml;steserver.","IP");
			_DBUSetConfig("server_dns_ip","10.10.0.1","Die Default-DNS-Server-IP der G&auml;steserver.","IP");
			_DBUSetConfig("server_wins_ip","10.10.0.1","Die Default-WINS-Server-IP der G&auml;steserver.","IP");
			 
			_DBUExecQuery("DELETE FROM `".TblPrefix()."flip_user_column` WHERE (`name` = 'ipcount' AND `type` = 'server');","Server-Column ipcount gel&ouml;scht.");
			 
			_DBUUpdateSubjectColumn("server","ip_fix","IP","Festgelegte IP","server_admin","server_admin","wird vergeben wenn in der Config der Wert \"server_iptype\" auf \"manuell\" gesetzt wird");
			_DBUUpdateSubjectColumn("server","os","TableDropdown|server_oses","Betriebssystem",0,"server_edit_own");
			_DBUUpdateSubjectColumn("server","description","text","Beschreibung",0,"server_edit_own");
			_DBUUpdateSubjectColumn("server","services","TableMultisel|server_services","Dienste",0,"server_edit_own");
			_DBUUpdateSubjectColumn("server","owner_id","Subjects","Besitzer","server_admin","user_admin_subjects","Achtung: Wer diesen Server bearbeiten darf, wird ber das Recht server_edit_own geregelt, nicht ber seinen Besitzer.");
			_DBUUpdateSubjectColumn("server","games","TableMultisel|server_games","Spiele",0,"server_edit_own");
			_DBUUpdateSubjectColumn("server","server_gateway","IP","Gateway-IP","server_edit_own","server_admin");
			_DBUUpdateSubjectColumn("server","server_subnetmask","IP","Subnetmask","server_edit_own","server_admin");
			_DBUUpdateSubjectColumn("server","server_dns_request","Domain","Wunsch-DNS-Name","server_edit_own","server_edit_own","Wenn der Admin ihn annimmt tr&auml;gt er ihn als DNS-Namen ein.");
			_DBUUpdateSubjectColumn("server","server_dns","Domain","DNS-Name",0,"server_admin","Deine Domain");
			_DBUUpdateSubjectColumn("server","server_dns_ip","IP","DNS-Server-IP","server_edit_own","server_admin");
			_DBUUpdateSubjectColumn("server","server_wins_ip","IP","WINS-Server-IP","server_edit_own","server_admin");

			 
		case(1153):
			_DBUUpdateSubjectColumn("server","owner_id","Subjects|user","Besitzer","server_admin","user_admin_subjects","Achtung: Wer diesen Server bearbeiten darf, wird &uuml;ber das Recht server_edit_own geregelt, nicht &uuml;ber seinen Besitzer.");
			$old = MysqlReadField("SELECT `message` FROM ".TblPrefix()."flip_sendmessage_message WHERE (`name`='server_add');");
			$txt = "Hallo {\$givenname},\r\n\r\ndie Konfiguration f&uuml;r deinen Server \"{\$name}\" hat sich ge&auml;ndert.\r\n{#IF \$atparty!=\"Y\"}Du hast eingestellt, dass du ihn zur kommenden Party nicht mitbringen wirst, ist das richtig?\r\n{#END}\r\nIP: {#WHEN empty(\$ip) \"nicht vergeben\" \$ip}\r\n{#IF !empty(\$server_dns)}DNS-Name: {\$server_dns}{#END}\r\n{#IF !empty(\$server_dns_request)}Wunsch-DNS: {\$server_dns_request}{#END}\r\n{#IF !empty(\$server_gateway)}Gateway: {\$server_gateway}{#END}\r\n{#IF !empty(\$server_subnetmask)}Subnetmask: {\$server_subnetmask}{#END}\r\n{#IF !empty(\$server_dns_ip)}DNS-Server: {\$server_dns_ip}{#END}\r\n{#IF !empty(\$server_wins_ip)}WINS-Server: {\$server_wins_ip}{#END}\r\n\r\nServerdaten:\r\n(k&ouml;nnen unter \"meine Daten\" -> \"meine Server\" ge&auml;ndert werden)\r\nHardware:       {%hardware}\r\nBetriebssystem: {#TABLEDISPLAY server_oses \$os}\r\nDienste:        {#TABLEDISPLAY server_services \$services}\r\nSpiele:         {#TABLEDISPLAY server_games \$games}\r\nBeschreibung: \r\n{\$description}";
			_DBUUpdateByKey(TblPrefix()."flip_sendmessage_message",array("message" => $txt),array("name" => "server_add"),"Der Text server_add wurde aktualisiert. Hier ein Backup des alten: <br/>\n<pre>$old</pre>");

			include_once("inc/inc.table.php");
			$services = array_flip(TableGetTableValues("server_services"));
			$games = array_flip(TableGetTableValues("server_games"));
			$os = array_flip(TableGetTableValues("server_oses"));
			foreach(GetSubjects("server",array("id","type","name","services","os")) as $a) {
				$s = CreateSubjectInstance($a);
				$ng = $ns = array();
				foreach(explode(",",$a["services"]) as $ser)
				if(!empty($services[trim($ser)]))
				$ns[] = $services[trim($ser)];
				elseif(!empty($games[trim($ser)]))
				$ng[] = $games[trim($ser)];
				$s->setProperties(array(
					"services" => implode(",",$ns), 
					"games" => implode(",",$ng), 
					"os" => $os[$a["os"]]));      	
			}

		case(1157):
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_news_news` ADD INDEX `i_category` ( `category` )","Index zu flip_news_news hinzugef&uuml;gt"); 
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_news_news` ADD INDEX `i_date` ( `date` )","Index zu flip_news_news hinzugef&uuml;gt");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_news_news` ADD INDEX `i_ontop` ( `on_top` )","Index zu flip_news_news hinzugef&uuml;gt");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_news_news` ADD INDEX `i_viewright` ( `view_right` )","Index zu flip_news_news hinzugef&uuml;gt"); 

		case(1163):
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_sponsor_ads` ADD `html` TEXT NOT NULL AFTER `image_mtime`;","Spalte html zu flip_sponsor_ads hinzugef&uuml;gt.");

		case(1204):
			_DBUExecInsert(TblPrefix()."flip_sendmessage_message", array("type"=>"sys","name"=>"user_status_paid","description"=>"Benutzer wurde auf bezahlt gesetzt","subject"=>"Status: bezahlt","message"=>"Hallo {$name},\r\n\r\ndein Status wurde auf bezahlt ge&auml;ndert.\r\n\r\nMfG\r\nxxx-Team\r\n\r\n(diesen Text hat noch kein Orga angepasst ;-)"), "Die Nachricht 'user_status_paid' wurde erstellt.");

		case(1219): //irgendwann vor 1220!?
			//last update added to flip.sql
		 
		case(1220):
			//WWCL-&Auml;nderungen
			$tournament_games_id = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_table_tables WHERE name='TOURNAMENT_GAMES'", "id");
			if($tournament_games_id > 0) {
				_DBUExecQuery("UPDATE `".TblPrefix()."flip_table_entry` SET value='CoD2 TDM', display='CoD2 TDM', display_code='CoD2 TDM' WHERE table_id='$tournament_games_id' AND `key`='cod'", "flip_tables TOURNAMENT_GAMES: Wert von cod auf 'CoD2 TDM' gesetzt.");
				_DBUExecQuery("UPDATE `".TblPrefix()."flip_table_entry` SET value='NFSMW Circuit', display='NFSMW Circuit', display_code='NFSMW Circuit' WHERE table_id='$tournament_games_id' AND `key`='nfsu'", "flip_tables TOURNAMENT_GAMES: Wert von nfsu auf 'NFSMW Circuit' gesetzt.");
				_DBUExecQuery("UPDATE `".TblPrefix()."flip_table_entry` SET value='NFSMW Drag', display='NFSMW Drag', display_code='NFSMW Drag' WHERE table_id='$tournament_games_id' AND `key`='nfsud'", "flip_tables TOURNAMENT_GAMES: Wert von nfsud auf 'NFSMW Drag' gesetzt.");
				_DBUExecQuery("UPDATE `".TblPrefix()."flip_table_entry` SET value='Pro Evolution Soccer5', display='Pro Evolution Soccer5', display_code='Pro Evolution Soccer5' WHERE table_id='$tournament_games_id' AND `key`='pes'", "flip_tables TOURNAMENT_GAMES: Wert von pes auf 'Pro Evolution Soccer5' gesetzt.");
				_DBUExecInsert(TblPrefix()."flip_table_entry", array("table_id"	=> $tournament_games_id,
					"key"		=> "cods",
					"value"	=> "CoD2 SD",
					"display"	=> "CoD2 SD",
					"display_code"	=> "CoD2 SD",
				), "Das Spiel \"CoD2 SD\" wurde in tables->TOURNAMENT_GAMES eingetragen.");
			}
			 
		case(1229):
			//added update from v1204 to flip.sql

		case(1242):
			//missing data (coingroup)
		 
		case(1248):
			//config seats_baseplan and seats_imagedir removed
		 
		case(1250):
			//config seats_baseplan readded
		 
		case(1253):
			//order of nickname in user_columns in flip.sql corrected

		case(1255):
			_DBUExecQuery("DELETE FROM `".TblPrefix()."flip_config` WHERE `key`='tournament_coins' OR `key`='tournament_coinname'", "Configeintr&auml;ge 'tournament_coinname' und 'tournament_coins' gel&ouml;scht.");

		case(1256):
			//^^coins-Config entfernt

		case(1266):
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_table_entry` ADD UNIQUE `unique_key` ( `table_id` , `key` )", "Schl&uuml;ssel in flip_table_entry m&uuml;ssen UNIQUE sein!");
			_DBUSetConfig("tournament_u18_check","user_property","Soll der &uuml;18-Status des Users f&uuml;r ein Turnier nach dem Sitzplatz oder der Eigenschaft 'vollj&auml;hrig' entschieden werden?","String");
			$tournament_u18_check_id = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_config WHERE `key`='tournament_u18_check'");
			_DBUExecInsert(TblPrefix()."flip_config_values", array("config_id"=>$tournament_u18_check_id, "value"=>"user_property"), "Configoption 'user_property' wurde f&uuml;r 'tournament_u18_check' hinzugef&uuml;gt.");
			_DBUExecInsert(TblPrefix()."flip_config_values", array("config_id"=>$tournament_u18_check_id, "value"=>"seat"), "Configoption 'seat' wurde f&uuml;r 'tournament_u18_check' hinzugef&uuml;gt.");
			//NGL-Games-Anpassungen
			$tournament_games_id = MysqlReadField("SELECT `id` FROM ".TblPrefix()."flip_table_tables WHERE `name`='TOURNAMENT_GAMES'", "id");
			if($tournament_games_id > 0)
			{
				_DBUUpdateByKey(TblPrefix()."flip_table_entry", array("key"		=> "cod2"),
				array("key"		=> "cod", "table_id"=>$tournament_games_id),
					"flip_tables_entry (TOURNAMENT_GAMES): Abk&uuml;rzung cod in cod2 ge&auml;ndert um NGL-kompatibel zu sein.", true);
				_DBUUpdateByKey(TblPrefix()."flip_table_entry", array("value"	=> "Call of Duty S&D",
					"display"	=> "Call of Duty S&D",
					"display_code"	=> "Call of Duty S&D"),
				array("key"		=> "cod", "table_id"=>$tournament_games_id),
					"flip_tables_entry (TOURNAMENT_GAMES): Das Spiel \"Call of Duty S&D\" wurde mit dem Schl&uuml;ssel 'cod' eingetragen um NGL-kompatibel zu sein.", true);
				_DBUUpdateByKey(TblPrefix()."flip_table_entry", array("value"	=> "Quake4",
					"display"	=> "Quake4",
					"display_code"	=> "Quake4"),
				array("key"		=> "q4", "table_id"=>$tournament_games_id),
					"flip_tables_entry (TOURNAMENT_GAMES): Das Spiel \"Quake4\" wurde mit dem Schl&uuml;ssel 'q4' eingetragen um NGL-kompatibel zu sein.", true);
				_DBUUpdateByKey(TblPrefix()."flip_table_entry", array("value"	=> "Quake4 Max",
					"display"	=> "Quake4 Max",
					"display_code"	=> "Quake4 Max"),
				array("key"		=> "q4mx", "table_id"=>$tournament_games_id),
					"flip_tables_entry (TOURNAMENT_GAMES): Das Spiel \"Quake4 Max\" wurde mit dem Schl&uuml;ssel 'q4mx' eingetragen um NGL-kompatibel zu sein.", true);
				_DBUUpdateByKey(TblPrefix()."flip_table_entry", array("value"	=> "Battlefield 2",
					"display"	=> "Battlefield 2",
					"display_code"	=> "Battlefield 2"),
				array("key"		=> "bf2", "table_id"=>$tournament_games_id),
					"flip_tables_entry (TOURNAMENT_GAMES): Das Spiel \"Battlefield 2\" wurde mit dem Schl&uuml;ssel 'bf2' eingetragen um NGL-kompatibel zu sein.", true);
			}

		case(1268):
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_tournament_combatant` ADD UNIQUE `team_tournament` ( `team_id` , `tournament_id` )", "flip_tournament_combatant: Je Turnier darf ein Team nur einmal teilnehmen (UniqueIndex).");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_tournament_ranking` ADD UNIQUE `team_tournament` ( `combatant_id` , `tournament_id` )", "flip_tournament_ranking: Je Turnier darf ein Team nur eine Platzierung haben (UniqueIndex).");
			$tournament_games_id = MysqlReadField("SELECT `id` FROM ".TblPrefix()."flip_table_tables WHERE `name`='TOURNAMENT_GAMES'", "id");
			if($tournament_games_id > 0)
			{
				_DBUUpdateByKey(TblPrefix()."flip_table_entry", array("key"		=> "nfsd"),
				array("key"		=> "nfsu_d", "table_id"=>$tournament_games_id),
					"flip_tables_entry (TOURNAMENT_GAMES): Abk&uuml;rzung nfsu_d in nfsd ge&auml;ndert um 4 Zeichen lang zu sein.", true);
			}

		case(1269):
			_DBUUpdateByKey(TblPrefix()."flip_table_entry",array("value"=>"alle Kategorien", "display"=>"alle Kategorien", "display_code"=>"alle Kategorien"),array("id"=>"7"),"keine Kategorie in alle Kategorien umbenannt");
			_DBUUpdateByKey(TblPrefix()."flip_table_entry",array("value"=>"alle Kategorien", "display"=>"alle Kategorien", "display_code"=>"alle Kategorien"),array("id"=>"123"),"keine Kategorie in alle Kategorien umbenannt");

		case(1270):
			_DBUUpdateByKey(TblPrefix()."flip_table_entry", array("value"=>"Quake 4", "display"=>"Quake 4", "display_code"=>"Quake 4"), array("value"=>"Quake4"),"TOURNAMENT_GAMES: 'Quake4' in 'Quake 4' umbenannt um WWCL konform zu sein.");

		case(1274):
			_DBUExecQuery("CREATE TABLE `".TblPrefix()."flip_user_binary` (\r\n" .
      		"  `id` INT NOT NULL AUTO_INCREMENT ,\r\n" . 
      		"  `mtime` TIMESTAMP NOT NULL ,\r\n" . 
      		"  `subject_id` INT NOT NULL ,\r\n" . 
      		"  `column_id` INT NOT NULL ,\r\n" .
      		"  `mimetype` varchar(64) NOT NULL ,\r\n" . 
      		"  `data` BLOB NULL ,\r\n" . 
      		"  PRIMARY KEY  (`id`),\r\n" . 
      		"  KEY `mtime` (`mtime`),\r\n" . 
      		"  UNIQUE KEY `data_ids` (`subject_id`,`column_id`),\r\n" . 
      		"  KEY `i_column` (`column_id`),\r\n" . 
      		"  KEY `i_subject` (`subject_id`)\r\n" .
			");", "Tabelle 'flip_user_binary' erstellt.");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_user_column` CHANGE `access` `access` ENUM( 'subject', 'data', 'binary', 'callback', 'method' ) NOT NULL DEFAULT 'data'", "flip_user_columns: Der Spalte 'access' den Typ 'binary' hinzugef&uuml;gt.");

		case(1283): //added tableprefix
		case(1284):
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_tournament_matches` ADD `editor` INT NOT NULL DEFAULT '0' AFTER `levelid` ;","Spalte editor zu flip_tournament_matches hinzugefuegt..");

		case(1285): //WWCL and NGL update
			$tournament_games_id = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_table_tables WHERE `name`='TOURNAMENT_GAMES'", "id");
			_DBUUpdateByKey(TblPrefix()."flip_table_entry",
			array("key"=>"bf42"),
			array("value"=>"Battlefield 1942", "table_id"=>$tournament_games_id),
					"TOURNAMENT_GAMES: Schluessel von 'Battlefield 1942' in 'bf42' geaendert fuer NGL-Export.");
			_DBUUpdateByKey(TblPrefix()."flip_table_entry",
			array("key"=>"bf"),
			array("value"=>"Battlefield 2", "table_id"=>$tournament_games_id),
					"TOURNAMENT_GAMES: Schluessel von 'Battlefield 2' in 'bf' geaendert fuer NGL-Export.");
			_DBUExecQuery("UPDATE `".TblPrefix()."flip_table_entry` SET `key`='ccg' WHERE `key`='cc' AND value LIKE 'CnC Gener%' AND table_id='$tournament_games_id'", "TOURNAMENT_GAMES: Schluessel 'cc' in 'ccg' umbenannt fuer NGL-Export.");
			_DBUUpdateByKey(TblPrefix()."flip_table_entry",
			array("key"=>"css", "value"=>"CStrike: Source", "display"=>"CStrike: Source", "display_code"=>"CStrike: Source", "table_id"=>$tournament_games_id),
			array("key"=>"css", "table_id"=>$tournament_games_id),
					"TOURNAMENT_GAMES: 'css' in 'CStrike: Source' umbenannt fuer WWCL-Export.",
			true);
			_DBUUpdateByKey(TblPrefix()."flip_table_entry",
			array("key"=>"dsum", "value"=>"DSuM2", "display"=>"DSuM2", "display_code"=>"DSuM2", "table_id"=>$tournament_games_id),
			array("key"=>"dsum", "table_id"=>$tournament_games_id),
					"TOURNAMENT_GAMES: 'dsum' mit 'DSuM2' erstellt fuer WWCL-Export.",
			true);
			_DBUUpdateByKey(TblPrefix()."flip_table_entry",
			array("value"=>"Empire Earth II", "display"=>"Empire Earth II", "display_code"=>"Empire Earth II"),
			array("value"=>"Empire Earth", "table_id"=>$tournament_games_id),
					"TOURNAMENT_GAMES: 'Empire Earth' in 'Empire Earth II' umbenannt fuer WWCL-Export.");
			_DBUUpdateByKey(TblPrefix()."flip_table_entry",
			array("value"=>"FIFA 2006", "display"=>"FIFA 2006", "display_code"=>"FIFA 2006"),
			array("value"=>"FIFA 2005", "table_id"=>$tournament_games_id),
					"TOURNAMENT_GAMES: 'FIFA 2005' in 'FIFA 2006' umbenannt fuer WWCL-Export.");
			_DBUUpdateByKey(TblPrefix()."flip_table_entry",
			array("key"=>"nfmc", "value"=>"NFSMW Circuit", "display"=>"NFSMW Circuit", "display_code"=>"NFSMW Circuit", "table_id"=>$tournament_games_id),
			array("key"=>"nfsu", "value"=>"NFSMW Circuit", "table_id"=>$tournament_games_id),
					"TOURNAMENT_GAMES: Schluessel von 'NFSMW Circuit' in 'nfmc' geaendert fuer WWCL-Export.",
			true);
			_DBUUpdateByKey(TblPrefix()."flip_table_entry",
			array("key"=>"nfsu", "value"=>"NFSU2 Circuit", "display"=>"NFSU2 Circuit", "display_code"=>"NFSU2 Circuit", "table_id"=>$tournament_games_id),
			array("value"=>"NFSU2 Circuit", "table_id"=>$tournament_games_id),
					"TOURNAMENT_GAMES: 'nfsu' mit 'NFSU2 Circuit' erstellt fuer NGL-Export.",
			true);
			_DBUUpdateByKey(TblPrefix()."flip_table_entry",
			array("key"=>"nfmd", "value"=>"NFSMW Drag", "display"=>"NFSMW Drag", "display_code"=>"NFSMW Drag", "table_id"=>$tournament_games_id),
			array("key"=>"nfsd", "value"=>"NFSMW Drag", "table_id"=>$tournament_games_id),
					"TOURNAMENT_GAMES: Schluessel von 'NFSMW Drag' in 'nfmd' geaendert fuer NGL-Export.",
			true);
			_DBUUpdateByKey(TblPrefix()."flip_table_entry",
			array("key"=>"nfsd", "value"=>"NFSU2 Drag", "display"=>"NFSU2 Drag", "display_code"=>"NFSU2 Drag", "table_id"=>$tournament_games_id),
			array("value"=>"NFSU2 Drag", "table_id"=>$tournament_games_id),
					"TOURNAMENT_GAMES: 'nfsd' mit 'NFSU2 Drag' erstellt fuer NGL-Export.",
			true);
			_DBUUpdateByKey(TblPrefix()."flip_table_entry",
			array("key"=>"pw", "value"=>"ParaWorld", "display"=>"ParaWorld", "display_code"=>"ParaWorld", "table_id"=>$tournament_games_id),
			array("key"=>"pw", "table_id"=>$tournament_games_id),
					"TOURNAMENT_GAMES: 'pw' mit 'ParaWorld' erstellt fuer NGL-Export.",
			true);
			_DBUUpdateByKey(TblPrefix()."flip_table_entry",
			array("key"=>"pwa", "value"=>"ParaWorld Arena", "display"=>"ParaWorld Arena", "display_code"=>"ParaWorld Arena", "table_id"=>$tournament_games_id),
			array("key"=>"pwa", "table_id"=>$tournament_games_id),
					"TOURNAMENT_GAMES: 'pwa' mit 'ParaWorld Arena' erstellt fuer NGL-Export.",
			true);
			_DBUUpdateByKey(TblPrefix()."flip_table_entry",
			array("key"=>"pwd", "value"=>"ParaWorld Domination", "display"=>"ParaWorld Domination", "display_code"=>"ParaWorld Domination", "table_id"=>$tournament_games_id),
			array("key"=>"pwd", "table_id"=>$tournament_games_id),
					"TOURNAMENT_GAMES: 'pwd' mit 'ParaWorld Domination' erstellt fuer NGL-Export.",
			true);
			_DBUUpdateByKey(TblPrefix()."flip_table_entry",
			array("key"=>"q4xb", "value"=>"Quake 4 X-Battle Mod", "display"=>"Quake 4 X-Battle Mod", "display_code"=>"Quake 4 X-Battle Mod", "table_id"=>$tournament_games_id),
			array("key"=>"q4xb", "table_id"=>$tournament_games_id),
					"TOURNAMENT_GAMES: 'q4xb' mit 'Quake 4 X-Battle Mod' erstellt fuer NGL-Export.",
			true);
			_DBUUpdateByKey(TblPrefix()."flip_table_entry",
			array("key"=>"rfbd", "value"=>"RfB MPDemo", "display"=>"RfB MPDemo", "display_code"=>"RfB MPDemo", "table_id"=>$tournament_games_id),
			array("key"=>"rfbd", "table_id"=>$tournament_games_id),
					"TOURNAMENT_GAMES: 'rfbd' mit 'RfB MPDemo' erstellt fuer WWCL-Export.",
			true);
			_DBUUpdateByKey(TblPrefix()."flip_table_entry",
			array("key"=>"tmn", "value"=>"Trackmania Nations", "display"=>"Trackmania Nations", "display_code"=>"Trackmania Nations", "table_id"=>$tournament_games_id),
			array("key"=>"tmn", "table_id"=>$tournament_games_id),
					"TOURNAMENT_GAMES: 'tmn' mit 'Trackmania Nations' erstellt fuer NGL-Export.",
			true);

		case(1289): //PayPal-Update
			_DBUExecQuery("CREATE TABLE `".TblPrefix()."flip_paypal_buttons` (
                      `id` int(11) NOT NULL auto_increment,
                      `mtime` TIMESTAMP NOT NULL ,
                      `item_name` text NOT NULL,
                      `item_number` text NOT NULL,
                      `amount` float NOT NULL default '0',
                      `return` text NOT NULL,
                      `cancel_return` text NOT NULL,
                      `notify_url` text NOT NULL,
                      `currency_code` text NOT NULL,
                      `business` text NOT NULL,
                      `quantity` int(11) NOT NULL default '0',
                      `ppurl` text NOT NULL,
                      PRIMARY KEY  (`id`),
                      KEY `mtime` (`mtime`)
                    ) TYPE=MyISAM;","Tabelle 'flip_paypal_buttons' hinzugef&uuml;gt.");      
			_DBUSetConfig("paypal_notice_user","1296","Die ID des Users der die PayPal-IPN-Nachrichten empfangen soll.","integer");
			_DBUSetConfig("paypal_notice_type","webmessage","Art der Benachrichtigung des PayPal-IPN-Systems.","messagetype");
			_DBUSetConfig("paypal_ipn_server","https://www.paypal.com/cgi-bin/webscr","Der Server, der die IPN-Daten auf g&uuml;ltigkeit Pr&uuml;ft","longstring");
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_content_text`
                     VALUES ('',NULL,3,'paypal_cancelled',0,2,'Vorgang abgebrochen','<div align=\"center\">\r\nDu hast den Zahlungsvorgang abgebrochen!<br />\r\nLeider k&ouml;nnen wir deinen Account nicht freischalten, solange wir den Unkostenbeitrag von dir nicht erhalten haben!\r\n</div>',
                     '',1296,1157413795);","Content-Text paypal_cancelled eingef&uuml;gt!");
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_content_text`
                     VALUES ('',NULL,3,'paypal_no_access',0,2,'Kein Zugriff','<div align=\"center\">\r\nDu hast den Unkostenbeitrag bereits bezahlt oder du besitzt nicht das Recht, um diese Seite anzuzeigen!\r\n</div>',
                     '',1296,1157413883);","Content-Text paypal_no_access eingef&uuml;gt!");
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_content_text`
                     VALUES ('',NULL,3,'paypal_paid',0,2,'Vielen Dank!','<div align=\"center\">\r\nDu hast soeben deinen Unkostenbeitrag per PayPal &uuml;berwiesen!<br />\r\nBitte warte einen Augenblick bis dich das System freischaltet. <br /><br />\r\nSollte sich in n&auml;chster Zeit dennoch nichts tun, dann wende dich bitte umgehend an die Orgas!\r\n</div>',
                     '',1296,1157414032);","Content-Text paypal_paid eingef&uuml;gt!");
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_content_text`
                     VALUES ('',NULL,3,'paypal_pay',0,2,'Mit PayPal bezahlen','<div align=\"center\">\r\nWir bieten die hier auch die M&ouml;glichkeit den Unkostenbeitrag per PayPal zu &uuml;berweisen.\r\nNach Eingang der ZahlungsBest&auml;tigung innerhalb weniger Sekunden nach der Transaktion wird dein Account dann vom System freigeschalten werden!\r\n</div>',
                     '',1296,1157414224);","Content-Text paypal_pay eingef&uuml;gt!");
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_user_right`
                     VALUES ('',NULL,'paypal_admin','Erlaubt es dem User, die PayPal-Buttons zu editieren');",
                     "User-Recht paypal_admin eingef&uuml;gt!");
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_user_right`
                     VALUES ('',NULL,'paypal_view','Erlaubt es dem User, die PayPal-Seite  einzusehen');",
                     "User-Recht paypal_view eingef&uuml;gt!");

		case(1290): // Userbilder
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_user_column`
                     VALUES ('', '', 'user', 'binary', 'user_image', 'image', 'N', 'Userbild', '0', '1', 'Userbild', 'Userbild', 1157829426, NULL, NULL);",
                     "Die Userspalte f&uuml;r das Avatar wurde eingef&uuml;gt!");
			_DBUSetConfig("forum_avatars_allow","Y","Sollen Avatars im Forum angezeigt werden?","YesNo");
			_DBUSetConfig("forum_avatars_maxheight","100","Die maximal dargestellte H&ouml;he/Breite der Avatars","integer");
			_DBUSetConfig("forum_avatars_maxwidth","100","","integer");

		case(1300):
			_DBUExecQuery("CREATE TABLE `".TblPrefix()."flip_setpaid_history` (
                     `id` int(11) NOT NULL auto_increment,
                     `mtime` TIMESTAMP NOT NULL , 
                     `date` int(11) NOT NULL default '0',
                     `orga_id` int(11) NOT NULL default '0',
                     `user_id` int(11) NOT NULL default '0',
                     PRIMARY KEY  (`id`), KEY `mtime` (`mtime`)) TYPE=MyISAM;",
                     "Tabelle flip_setpaid_history f&uuml;r die Bezahlt-History hinzugef&uuml;gt...");
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_user_right`
                     VALUES ('',NULL,'lanparty_show_paidlog','Erlaubt es dem User den SetPaid-Log anzusehen.');",
                     "User-Recht lanparty_show_paidright eingef&uuml;gt!");                   

		case(1303): //case-&Auml;nderung in flip.sql

		case(1306):
			_DBUExecQuery("CREATE TABLE `".TblPrefix()."flip_featuresys_types` (
                     `id` int(11) NOT NULL auto_increment,
                     `mtime` TIMESTAMP NOT NULL ,
                     `name` varchar(255) NOT NULL,
                     `description` varchar(255),
                     `maxcount` int(11) NOT NULL,
                     PRIMARY KEY  (`id`), KEY `mtime` (`mtime`)) TYPE=MyISAM;",
                     "Tabelle flip_featuresys_types f&uuml;r das Featuresystem hinzugef&uuml;gt");
			_DBUExecQuery("CREATE TABLE `".TblPrefix()."flip_featuresys_accounts` (
                     `id` int(11) NOT NULL auto_increment,
                     `mtime` TIMESTAMP NOT NULL ,
                     `user_id` int(11) NOT NULL,
                     `status` ENUM('requested','accepted','denied'),
                     `accounttype` int(11) NOT NULL,
                     PRIMARY KEY  (`id`),
                     KEY `mtime` (`mtime`)) TYPE=MyISAM;",
                     "Tabelle flip_featuresys_accounts f&uuml;r das Featuresystem hinzugef&uuml;gt");
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_user_right`
                     VALUES ('',NULL,'featuresystem_view','Erlaubt dem Benutzer die Einsicht ins Feature-System');",
                     "User-Recht featuresystem_view eingef&uuml;gt!");
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_user_right`
                     VALUES ('',NULL,'featuresystem_admin','Erlaubt dem Benutzer die Administration des Feature-Systems');",
                     "User-Recht featuresystem_admin eingef&uuml;gt!");
		case(1312):
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_featuresys_types`
                     ADD `msg_accepted` varchar(255) NOT NULL,
                     ADD `msg_denied` varchar(255) NOT NULL,
                     ADD `msg_requested` varchar(255) NOT NULL;",
                     "Der Tabelle flip_featuresys_types wurden die Spalten f&uuml;r die WebMsg hinzugef&uuml;gt!");

		case(1314):
			_DBUSetConfig("user_import_lansurfer","Y","Soll bei der Registrierung der Import von Lansurfer m&ouml;glich sein?","YesNo");

		case(1323):
		case(1324):
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_tournament_matches` ADD `ready1` INT NULL AFTER `points2` , ADD `ready2` INT NULL AFTER `ready1` ;","Spalte ready1/2 zu flip_tournament_matches hinzugef&uuml;gt.");
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_sendmessage_message` (`type`, `name`, `description`, `vars`, `subject`, `message`) VALUES ('sys', 'tournament_matchready', 'Ein Match kann gespielt werden. Die Teams werden hier&uuml;ber informiert', '', 'Match im {\$tournament_name}', 'Hallo {\$nickname},\r\n\r\nein {\$game}-Match gegen \"{\$opponent}\" ist zu spielen. Bitte bereite dein Team vor und trage es als bereit ein, indem du auf den Link unten klickst.\r\nWenn dein Gegner bereit ist, bekommst du noch eine Nachricht.\r\n\r\nhttp://www.xxx.test/{\$link}\r\n\r\nzum Turnier:\r\n{\$gamelink}');","Nachricht 'tournament_matchready' erstellt.");
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_sendmessage_message` (`type`, `name`, `description`, `vars`, `subject`, `message`) VALUES ('sys', 'tournament_teamready', 'Wenn ein Team bereit ist, bekommt der Gegner diese Nachricht', '', 'Turniergegner im {\$tournament_name} ist bereit', 'Hi {\$nickname},\r\n\r\ndein {\$game} Gegner \"{\$opponent}\" ist bereit zum spielen.\r\n\r\nzum Turnier:\r\n{\$gamelink}');","Nachricht 'tournament_teamready' erstellt.");

		case(1328): //flip.sql angepasst

		case(1329):
			$table_id = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_table_tables WHERE name = 'TOURNAMENT_BADNAMES'", "id");
			_DBUExecQuery("UPDATE ".TblPrefix()."flip_table_entry SET value='frei\\\|os', display='frei\\\|os', display_code='frei\\\|os' WHERE value='frei\|os' AND table_id='$table_id'","Turniersystem 'Badnames': das 'frei|os' RegEx-escaped.");

		case(1332): //mtime-Spalten nachgetragen
			$altertables = array("flip_featuresys_accounts","flip_featuresys_types","flip_paypal_buttons","flip_setpaid_history");
			foreach($altertables AS $tablename) {
				_DBUExecQuery("ALTER TABLE `".TblPrefix()."$tablename` ADD `mtime` TIMESTAMP NOT NULL AFTER `id` ;","Spalte mtime zu $tablename hinzugef&uuml;gt (falls noch nicht vorhanden).");
				_DBUExecQuery("ALTER TABLE `".TblPrefix()."$tablename` ADD INDEX mtime ( `mtime` ) ;","Index f&uuml;r mtime in $tablename hinzugef&uuml;gt (falls noch nicht vorhanden).");
			}

		case(1335): //WWCL-Update
			if(empty($tournament_games_id))
			$tournament_games_id = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_table_tables WHERE `name`='TOURNAMENT_GAMES'", "id");
			_DBUUpdateByKey(TblPrefix()."flip_table_entry",
			array("key"=>"me2", "value"=>"Medieval", "display"=>"Medieval", "display_code"=>"Medieval", "table_id"=>$tournament_games_id),
			array("key"=>"me2", "table_id"=>$tournament_games_id),
				"TOURNAMENT_GAMES: 'me2' mit 'Medieval' erstellt fuer WWCL-Export.",
			true);
			_DBUUpdateByKey(TblPrefix()."flip_table_entry",
			array("key"=>"mmv4", "value"=>"Micro MachinesV4", "display"=>"Micro MachinesV4", "display_code"=>"Micro MachinesV4", "table_id"=>$tournament_games_id),
			array("key"=>"mmv4", "table_id"=>$tournament_games_id),
				"TOURNAMENT_GAMES: 'mmv4' mit 'Micro MachinesV4' erstellt fuer WWCL-Export.",
			true);
			_DBUUpdateByKey(TblPrefix()."flip_table_entry",
			array("key"=>"prey", "value"=>"Prey", "display"=>"Prey", "display_code"=>"Prey", "table_id"=>$tournament_games_id),
			array("key"=>"prey", "table_id"=>$tournament_games_id),
				"TOURNAMENT_GAMES: 'prey' mit 'Prey' erstellt fuer WWCL-Export.",
			true);
			 
		case(1340): //XHTML-Anpassungen in flip.sql f&uuml;r flip_content_text

		case(1341):  // Turnierinfopage eingef&uuml;gt
			_DBUExecQuery("CREATE TABLE `".TblPrefix()."flip_tinfo_news` (
                      `id` int(11) NOT NULL auto_increment,
                      `mtime` TIMESTAMP NOT NULL ,
                      `date` int(11) NOT NULL,
                      `caption` varchar(255) NOT NULL,
                      `content` text NOT NULL,
                      PRIMARY KEY  (`id`),
                      KEY `mtime` (`mtime`)
                    ) TYPE=MyISAM;","Tabelle 'flip_tinfo_news' hinzugef&uuml;gt."); 
			_DBUExecQuery("CREATE TABLE `".TblPrefix()."flip_tinfo_downloads` (
                      `id` int(11) NOT NULL auto_increment,
                      `mtime` TIMESTAMP NOT NULL ,
                      `tournament_id` int(11) NOT NULL,
                      `description` varchar(255) NOT NULL,
                      `link` text NOT NULL,
                      PRIMARY KEY  (`id`),
                      KEY `mtime` (`mtime`)
                    ) TYPE=MyISAM;","Tabelle 'flip_tinfo_downloads' hinzugef&uuml;gt."); 
			_DBUAddRight("tinfopage_view","Erlaubt dem Benutzer die Einsicht der Turnierinfo-Page", "registered");
			_DBUAddRight("tinfopage_admin","Erlaubt dem Benutzer die Administration der Turnierinfo-Page","tournament_admins");

		case(1349):
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_tournament_tournaments` CHANGE `game` `game` VARCHAR( 20 ) NOT NULL;","Spalte 'game' in flip_tournament_tournaments verl&auml;ngert (auf 20 Zeichen).");

		case(1350)://Versionsnummer hochgez&auml;hlt

		case(1351):
			_DBUUpdateByKey(TblPrefix()."flip_sendmessage_message", array("type"=>"sys", "description"=>"Ein Match kann gespielt werden. Die Teams werden hier&uuml;ber informiert", "subject"=>"Match im {\$tournament_name}", "message"=>"Hallo {\$nickname},\r\n\r\nein {\$game}-Match gegen \"{\$opponent}\" ist zu spielen. Bitte bereite dein Team vor und trage es als bereit ein, indem du auf den Link unten klickst.\r\nWenn dein Gegner bereit ist, bekommst du noch eine Nachricht.\r\n\r\n{\$link}",), array("name"=>"tournament_matchready"),"Nachricht 'tournament_matchready' &uuml;berschrieben.", true);
			_DBUUpdateByKey(TblPrefix()."flip_sendmessage_message", array("type"=>"sys", "description"=>"Wenn ein Team bereit ist, bekommt der Gegner diese Nachricht", "subject"=>"Turniergegner im {\$tournament_name} ist bereit", "message"=>"Hi {\$nickname},\r\n\r\ndein {\$game} Gegner \"{\$opponent}\" ist bereit zum spielen.",), array("name"=>"tournament_teamready"),"Nachricht 'tournament_teamready' &uuml;berschrieben.", true);

		case(1356): // Feature ;) von Mysql...
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_catering_kategorien` CHANGE `warteliste` `warteliste` ENUM('1','0') NOT NULL DEFAULT '0';","Enum-Reihenfolge vertauscht...");

		case(1365): // ENUM-Reihenfolge vertauschen
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_news_news` CHANGE `on_top` `on_top` ENUM( '1', '0' ) NOT NULL DEFAULT '0';","Spalte flip_news_news.on_top - ENUM-Werte vertauscht");

		case(1368):
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_user_column` ADD `min_length` INT NOT NULL AFTER `name` ;","Spalte 'min_length' zu flip_user_column hinzugef&uuml;gt");
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_user_column` ADD `max_length` INT NOT NULL AFTER `min_length` ;","Spalte 'max_length' zu flip_user_column hinzugef&uuml;gt");
			_DBUUpdateByKey(TblPrefix()."flip_user_column", array("min_length"=>5, "max_length"=>5), array("name"=>"plz"), "L&auml;nge von PLZ auf 5 gesetzt.");

		case(1373): //Clan-Verwaltung
			//Subjekt-Typ
			$clanSubjectId = _DBUUpdateByKey(TblPrefix()."flip_user_subject", array("type"=>"type", "name"=>"clan"), array("type"=>"type", "name"=>"clan"), "Subjekt-Typ 'clan' hinzugef&uuml;gt.", true);
			_DBUUpdateSubjectProperties($clanSubjectId, array("rights_over_for_properties"=>"Y", "can_be_child"=>"Y", "can_be_parent"=>"Y", "can_be_controled"=>"N"));
			//Gruppe
			$clanGroupId = _DBUUpdateByKey(TblPrefix()."flip_user_subject", array("type"=>"group", "name"=>"clans"), array("type"=>"group", "name"=>"clans"), "Gruppe 'clans' hinzugef&uuml;gt.", true);
			_DBUUpdateSubjectProperties($clanGroupId, array("caption"=>"Clanliste", "description"=>"Zusammenfassung aller Clans zu administrativen Zwecken."));
			//Rechte
			_DBUAddRight("clan_join","Recht um an einem Clan teilnehmen zu d&uuml;rfen","registered");
			_DBUAddRight("clan_leader","Recht um einen Clan zu bearbeiten",array("usermanager", "usermanager"=>"clans", "registered"));
			//Columns
			_DBUUpdateByKey(TblPrefix()."flip_user_column", array("type"=>"clan", "name"=>"id", "access"=>"subject", "val_type"=>"Integer", "required"=>"N", "caption"=>"ID", "required_view_right"=>GetRightID("view_personal_informations"), "required_edit_right"=>"-1", "sort_index"=>1172143593), array("type"=>"clan", "name"=>"name"),"f&uuml;r Clan die Eigenschaft 'id' gesetzt.", true);
			_DBUUpdateByKey(TblPrefix()."flip_user_column", array("type"=>"clan", "name"=>"name", "access"=>"subject", "val_type"=>"String", "required"=>"Y", "caption"=>"Name", "required_view_right"=>0, "required_edit_right"=>GetRightID("clan_leader"), "sort_index"=>1172143625), array("type"=>"clan", "name"=>"name"),"f&uuml;r Clan die Eigenschaft 'name' gesetzt.", true);
			_DBUUpdateByKey(TblPrefix()."flip_user_column", array("type"=>"clan", "name"=>"homepage", "val_type"=>"String", "required"=>"N", "caption"=>"Website", "required_view_right"=>0, "required_edit_right"=>GetRightID("clan_leader"), "sort_index"=>1172155284), array("type"=>"clan", "name"=>"homepage"),"f&uuml;r Clan die Eigenschaft 'homepage' gesetzt.", true);
			_DBUUpdateByKey(TblPrefix()."flip_user_column", array("type"=>"clan", "name"=>"image", "access"=>"binary", "val_type"=>"Image", "required"=>"N", "caption"=>"Bild", "required_view_right"=>0, "required_edit_right"=>GetRightID("clan_leader"), "sort_index"=>1173099897), array("type"=>"clan", "name"=>"image"),"f&uuml;r Clan die Eigenschaft 'image' gesetzt.", true);
			_DBUUpdateByKey(TblPrefix()."flip_user_column", array("type"=>"clan", "name"=>"description", "val_type"=>"Text", "required"=>"N", "caption"=>"Beschreibung", "required_view_right"=>0, "required_edit_right"=>GetRightID("clan_leader"), "sort_index"=>1173101701), array("type"=>"clan", "name"=>"description"),"f&uuml;r Clan die Eigenschaft 'description' gesetzt.", true);
			//Text
			$groupid = MysqlReadField("SELECT id FROM `".TblPrefix()."flip_content_groups` WHERE name='system'","id");
			_DBUUpdateByKey(TblPrefix()."flip_content_text", array("group_id"=>$groupid, "name"=>"clan_join", "view_right"=>0, "edit_right"=>GetRightID("edit_public_text"), "caption"=>"Anfrage stellen an", "text"=>"Hier kannst du eine Anfrage an den Clan auf Mitgliedschaft stellen.", "edit_user_id"=>SYSTEM_USER_ID, "edit_time"=>time()), array("name"=>"clan_join"), "Text 'clan_join' bearbeitet.", true);
			//SQL-Tabelle
			_DBUExecQuery("CREATE TABLE `".TblPrefix()."flip_clan_join` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`mtime` TIMESTAMP(14) NOT NULL,
			`clan_id` INT(11) NOT NULL,
			`user_id` INT(11) NOT NULL,
			PRIMARY KEY  (`id`),
			KEY `mtime` (`mtime`)
			);","Tabelle flip_clan_join hinzugef&uuml;gt.");
			//Men&uuml;eintrag
			_DBUUpdateMenu("join", "Clans", "clan.php", 0, "Clanverwaltung");
			//Webmessage
			_DBUUpdateByKey(TblPrefix()."flip_sendmessage_message",array("type"=>"sys", "name"=>"clan_join", "description"=>"Wird verschickt, wernn jemand eine Anfrage an einen Clan schickt", "subject"=>"Anfrage f&uuml;r Clan {\$clanname}", "message"=>"Hallo {\$name},\n\nin deinem Clan '{\$clanname}' m&ouml;chte {\$member} aufgenommen werden.\n\nAkzeptieren: {\$link}\n\n\nDein\nxxx-Team"), array("name"=>"clan_join"), "Die Nachricht 'clan_join' wurde gesetzt.", true);

		case(1378): //G&auml;stebuch
			_DBUExecQuery("CREATE TABLE `".TblPrefix()."flip_gb_entry` (
                       `id` int(11) NOT NULL auto_increment,
                       `mtime` timestamp(14) NOT NULL,
                       `date` int(11) NOT NULL default '0',
                       `author` varchar(32) NOT NULL default '',
                       `title` varchar(64) NOT NULL default '',
                       `text` text NOT NULL,
                       PRIMARY KEY  (`id`),
                       KEY `mtime` (`mtime`),
                       KEY `i_date` (`date`)
                     ) TYPE=MyISAM;"
				,"Tabelle flip_gb_entry erstellt.");
			_DBUExecQuery("CREATE TABLE `".TblPrefix()."flip_gb_icons` (
                       `id` int(11) NOT NULL auto_increment,
                       `mtime` timestamp(14) NOT NULL,
                       `text` varchar(20) NOT NULL default '',
                       `icon` varchar(255) NOT NULL default '',
                       PRIMARY KEY  (`id`),
                       KEY `mtime` (`mtime`)
                     ) TYPE=MyISAM;","Tabelle flip_gb_icons erstellt.");
			_DBUAddRight("gb_add","Recht um ein G&auml;stebucheintrag zu erstellen",array("registered","Anonymous"));
			_DBUAddRight("gb_admin","Recht um das G&auml;stebuch zu verwalten","orga");
			_DBUSetConfig("gb_perpage", "20", "Anzahl G&auml;stebucheintr&auml;ge je Seite", "Integer");
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_gb_icons` (`text`, `icon`) VALUES
      		(':)', 'images/smilies/smile.gif'),
     		(':(', 'images/smilies/sad.gif'),
      		(';)', 'images/smilies/zwinker.gif'),
      		(':D', 'images/smilies/grins.gif')
      		;","Icons f&uuml;r das G&auml;stebuch hinzugef&uuml;gt.");

		case(1382):
			_DBUUpdateMenu("Kontakt", "G&auml;stebuch", "gb.php", 0, "G&auml;stebuch");

		case(1383):
			_DBUAddRight("translate", "Recht um FLIP &uuml;bersetzen zu d&uuml;rfen.", "publisher");

		case(1389): //basic translations in sql-file

		case(1390):
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_archiv_events` CHANGE `participant_group_name` `participant_group_id` INT( 11 ) NULL DEFAULT NULL","Spalte flip_archiv_events.participant_group_name in participant_group_id umbenannt.");

		case(1396):
			_DBUExecQuery("CREATE TABLE `".TblPrefix()."flip_archiv_trees` (
			`id` INT NOT NULL auto_increment,
			`mtime` TIMESTAMP NOT NULL ,
			`event_id` INT NOT NULL ,
			`caption` VARCHAR( 255 ) NOT NULL ,
			`data` TEXT NOT NULL ,
			PRIMARY KEY  (`id`),
			KEY `mtime` (`mtime`),
			KEY `i_event_id` (`event_id`)
			) ENGINE = MYISAM ;", "Tabelle flip_archiv_trees erstellt");

		case(1397): //changed webmessage tournament_matchready in flip.sql

		case(1398): // PayPal-System &uuml;berarbeitet
			_DBUExecQuery("CREATE TABLE `".TblPrefix()."flip_paypal_transactions` (
  					 `tx_id` char(32) NOT NULL,
  					 `mtime` timestamp(14) NOT NULL,
  					 `formstring` text NOT NULL,
  					 PRIMARY KEY  (`tx_id`),
  					 KEY `mtime` (`mtime`)
					 ) TYPE=MyISAM;", "Tabelle flip_paypal_transactions erstellt");
			_DBUExecQuery("DELETE FROM `".TblPrefix()."flip_config` WHERE `key` IN('paypal_notice_user','paypal_notice_type')","Config-Eintr&auml;ge f&uuml;r das alte PayPal-Messaging entfernt.");

		case(1404): // PayPal-Sys: Messaging &uuml;berarbeitet 
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_sendmessage_message` (`type`,`name`,`description`,`subject`,`message`) VALUES ('sys','flip_paypal_paymentsuccess','Wird bei einer erfolgten PayPal-Zahlung gesendet','PayPal: Freigeschalten','Hallo {\$name}!\n\n{#IF \$name == \$paypal_targetname}\nDas PayPal-System hat deine Zahlung registriert und dich freigeschalten!\nDu kannst dir jetzt auf dem Sitzplan einen Sitzplatz reservieren.\n\nViel Spa&szlig; w&uuml;nscht dir das Orga-Team!\n{#ELSE}\nDer User {\$paypal_targetname} wurde mit dem PayPal-System freigeschalten.\nDie Daten vom IPN lauten wie folgt:\n\n{\$paypal_ipn_info}\n\nDie Freischaltung erfolgte am {\$paypal_ipn_date}\n{#END}\n')","System-Message flip_paypal_paymentsuccess hinzugef&uuml;gt");
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_sendmessage_message` (`type`,`name`,`description`,`subject`,`message`) VALUES ('sys','flip_paypal_paymentfailure','Wird bei einer fehlgeschlagenen PayPal-Zahlung gesendet','PayPal: Fehler','Hallo {\$name}!\n\n{#IF \$name == \$paypal_targetname}\nDas PayPal-System hat bei deiner Zahlung folgenden Fehler festgestellt:\n\n{\$paypal_ipn_errormsg}\n\nDein Account wurde deshalb nicht freigeschalten - bitte wende dich an die Orgas!\n\n{#ELSE}\nBeim Versuch den User {\$paypal_targetname} freizuschalten trat ein Fehler auf!\nDas System meldete:\n\n{\$paypal_ipn_errormsg}\n\nDie Daten vom IPN lauten wie folgt:\n\n{\$paypal_ipn_info}\n\n{#END}')","System-Message flip_paypal_paymentfailure hinzugef&uuml;gt");

		case(1405): // Config-Kategorien hinzugef&uuml;gt
			_DBUExecQuery("CREATE TABLE `".TblPrefix()."flip_config_categories` (
  					`id` int(11) NOT NULL auto_increment,
  					`mtime` timestamp NOT NULL,
  					`name` char(255) NOT NULL,
  					`custom_name` char(255) NOT NULL,
  					`description` text NOT NULL,
  					PRIMARY KEY  (`id`),
  					KEY `mtime` (`mtime`)
  					) TYPE=MyISAM;","Tabelle flip_config_categories hinzugef&uuml;gt");

		case(1407): //verschollene Config wiedergefunden ;)
			_DBUSetConfig('dns_user_domain', 'xxx.lan', 'Die Domain-Zone f&uuml;r die User-Domains.', 'String');
			_DBUSetConfig('dns_server_domain', 'xxx.lan', 'Die Domain-Zone f&uuml;r die Server-Domains.', 'String');

		case(1409): // Forum-Watches
			_DBUExecQuery("CREATE TABLE `".TblPrefix()."flip_forum_watches` (
					   `id` int(11) NOT NULL auto_increment,
					   `mtime` timestamp(14) NOT NULL,
					   `type` enum('forum','thread') NOT NULL,
					   `target_id` int(11) NOT NULL,
					   `owner_id` int(11) NOT NULL,
					   PRIMARY KEY  (`id`)
					  ) TYPE=MyISAM;","Tabelle flip_forum_watches hinzugef&uuml;gt");
			_DBUSetConfig('watches_enabled','Y','Bestimmt, ob Watches global aktiviert werden oder nicht.','YesNo');
			_DBUAddRight('watches_use', 'Recht um die Foren-Watches nutzen zu k&ouml;nnen.', 'registered');
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_sendmessage_message` (`type`,`name`,`description`,`subject`,`message`) VALUES ('sys', 'watches_notify', 'Watches-Meldungen', 'Watches', 'Hallo {\$name}!\r\n\r\nDu besitzt einen oder mehrere Watches und folgendes ist passiert:\r\n\r\n- {\$actionmsg}\r\n');",'Systemmessage f&uuml;r die Watches hinzugef&uuml;gt');

		case(1410): // mtime nachgetragen @flip.sql und hier
			_DBUExecQuery("ALTER TABLE `".TblPrefix()."flip_forum_watches` ADD `mtime` TIMESTAMP NOT NULL AFTER `id`;","Spalte mtime bei flip_forum_watches hinzugef&uuml;gt");

		case(1395): //alte revision 1411 ist nach svn wiederherstellung 1395
		case(1411):
			_DBUExecQuery('DELETE FROM `'.TblPrefix()."flip_user_rights` WHERE right_id='".GetRightID('clan_leader')."' AND owner_id='".GetSubjectID('clans')."'", 'Recht clan_leader von clans entfernt.');
			_DBUAddRight("clan_leader","Recht um einen Clan zu bearbeiten",array("usermanager"=>"clans"));

		case(1415):
			_DBUSetConfig('server_ip_start','10.10.200.1','Letzte IP f&uuml;r Server bei IP-Typ "byID".', 'IP');
			_DBUSetConfig('server_ip_end','10.10.210.250','Letzte IP f&uuml;r Server bei IP-Typ "byID".', 'IP');
			_DBUSetConfig('server_iptype','fix','Gibt an wie die IP festgelegt wird: "fix"=>manuelle Vergabe, "id"=>die IP wird aus dem Bereich "server_ip_start"-"server_ip_end" zugewiesen, "seat"=>Sitzplatz des Servers, "user=>IP wird wie bei Usern aus der ID gebildet','String');

		case(1417): // Signaturen f&uuml;r Webmessages
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_user_column` (type,access,name,val_type,caption,required_view_right,required_edit_right,sort_index) VALUES ('user', 'data', 'webmessage_signature','Text', 'Deine Signatur f&uuml;r Webmessages', '16', '1', 1182630966);",'Profilspalte f&uuml;r die Webmessage-Signatur hinzugef&uuml;gt');

		case(1418): // keine &Auml;nderung Release 1.2

		case(1426): // INet-Modul &uuml;berarbeitet
			_DBUSetConfig('inet_allowed_hosts','127.0.0.1;127.0.1.1','Die Adressen der Ger&auml;te, die sich die IPs der User zum Freischalten des Internets holen d&uuml;rfen.','String');
			_DBUSetConfig('inet_address_seperator',';\n','Ein Trennzeichen, das zum Trennen der IP-Adressen in der formatierten Ausgabe dient.','String');
			_DBUSetConfig('inet_address_header','## INET START ##\n','Eine Zeichenfolge, die als Header vor die IP-Adressen geh&auml;ngt wird.','string');
			_DBUSetConfig('inet_address_footer','\n## INET END ##\n\n','Eine Zeichenfolge, die als Footer ans Ende der IP-Adressen geh&auml;ngt wird','string');
			_DBUSetConfig('inet_max_requests','500','Die H&ouml;chstanzahl der User, die INet beantragen (in der Warteschlange sein) k&ouml;nnen ','Integer');
			_DBUSetConfig('inet_max_online','3','Die Anzahl an Usern, die max. gleichzeitig f&uuml;rs Internet freigeschaltet werden.','Integer');
			_DBUSetConfig('inet_default_priority','1','Die Standardpriorit&auml;t f&uuml;r User, falls sie in keiner der definierten Gruppen Mitglied sind.','integer');
			_DBUSetConfig('inet_default_onlinetime','25','Die Standardonlinezeit der User der Defaultgruppe in Minuten.','integer');
			_DBUSetConfig('inet_noexpiry_as_online','N','Sollen dauernd als online freigeschaltete User ebenfalls zu der Menge an max. Online geschalteten Usern z&auml;hlen oder werden sie seperat behandelt?','YesNo');
			_DBUSetConfig('inet_allow_multiple_requests','Y','Bestimmt, ob das Internet nur einmal beantragt werden kann oder nicht. (Neu Anstellen)','YesNo');
	   
			_DBUExecQuery("CREATE TABLE `".TblPrefix()."flip_inet_groups` (
				    `id` int(11) NOT NULL auto_increment,
				    `mtime` timestamp(14) NOT NULL,
				    `subject_group_id` int(11) NOT NULL,
				    `priority` int(11) NOT NULL default '1',
				    `online_time` int(11) NOT NULL,
				    PRIMARY KEY  (`id`))TYPE=MyISAM;",'Tabelle flip_inet_groups hinzugef&uuml;gt');
	   
			_DBUExecQuery("CREATE TABLE `".TblPrefix()."flip_inet_requests` (
				    `id` int(11) NOT NULL auto_increment,
				    `mtime` timestamp(14) NOT NULL,
				    `user_id` int(11) NOT NULL,
				    `status` enum('waiting','granted','expired','noexpiry') NOT NULL,
				    `online_since` int(11) NOT NULL default '0',
				    `online_time` int(11) NOT NULL default '0',
				    `priority` int(11) NOT NULL,
				     PRIMARY KEY  (`id`)) TYPE=MyISAM;",'Tabelle flip_inet_requests hinzugef&uuml;gt');

			_DBUUpdateMenu('Join', 'Internet', 'inet.php', 0, 'Internet-Modul');

			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_sendmessage_message` VALUES (NULL,NULL,'sys','inet_notification','Nachricht vom INet-Sys','','INet: Nachricht','Hallo {\$name}!\r\n{#IF \$inet_status == \'waiting\'}\r\nDu hast einen Antrag auf Internet gestellt, bist aber in die Warteschlange eingereiht worden. Bitte gedulde dich, bis du an der Reihe bist - du erh&auml;lst dann automatisch eine weitere Webmessage!\r\n{#ELSE}{#IF \$inet_status == \'granted\'}\r\nDas Internet wurde f&uuml;r dich am {\$inet_granttime} freigeschalten! Von da an hast du {\$inet_accesstimespan} Zeit um deine Updates etc. durchzuf&uuml;hren.\r\n{#ELSE}{#IF \$inet_status == \'expired\'}\r\nDein Internetzugang wurde wieder gesperrt, da deine Online-Zeit abgelaufen ist. Falls es deswegen zu Problemen gekommen ist, wende dich bitte an die Organisation!\r\n{#ELSE}\r\nDas Internet wurde f&uuml;r dich auf unbegrenzte Zeit freigeschalten.\r\n{#END}{#END}{#END}\r\nGru&szlig;,\r\nDas Orga-Team');",'Systemmessage f&uuml;rs INet-Modul hinzugef&uuml;gt');

			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_table_tables` VALUES (NULL,NULL,'inet_status_messages',".GetRightID('table_edit').",'Die Statusbeschreibungen des INet-Moduls');",'F&uuml;ge Content-Tabellen hinzu...');
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_table_tables` VALUES (NULL,NULL,'inet_status_names',".GetRightID('table_edit').",'Die Statusnamen des INet-Moduls (kurzform)');",'...');
			$inet_messages_id = MysqlReadField('SELECT `id` FROM `'.TblPrefix()."flip_table_tables` WHERE `name`='inet_status_messages'");
			$inet_names_id = MysqlReadField('SELECT `id` FROM `'.TblPrefix()."flip_table_tables` WHERE `name`='inet_status_names'");
	   
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_table_entry` VALUES (NULL,NULL,$inet_messages_id,'unrequested','Kein Antrag gestellt','<b>Du hast keinen Antrag gestellt.</b><br /><br />\r\nKlicke auf den Button unten um einen zu stellen!','<b>Du hast keinen Antrag gestellt.</b><br /><br />\r\nKlicke auf den Button unten um einen zu stellen!',0);",'... F&uuml;lle Content-Tabellen mit Eintr&auml;gen ...');
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_table_entry` VALUES (NULL,NULL,$inet_messages_id,'waiting','Antrag gestellt','<font color=\"#bbbb00\">Du bist in der Warteschlange (Platz {\$inet_queueposition})!</font><br /><br />\r\nBitte gedulde dich, bis du an der Reihe bist<br /> oder nehme deinen Antrag zur&uuml;ck, indem du auf den Button unten klickst.','<font color=\"#bbbb00\">Du bist in der Warteschlange (Platz <?php echo \$var0[\'inet_queueposition\'] ?>)!</font><br /><br />\r\nBitte gedulde dich, bis du an der Reihe bist<br /> oder nehme deinen Antrag zur&uuml;ck, indem du auf den Button unten klickst.',1);",'...');
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_table_entry` VALUES (NULL,NULL,$inet_messages_id,'granted','Der User wurde freigeschalten','{#IF !empty(\$inet_timeleft)}\r\n<font color=\"green\">Dein Internetzugang ist freigeschalten worden!</font><br /><br />\r\nDu hast noch<br /><br /><b>{\$inet_timeleft}</b><br /><br /> Zeit bis dein Zugang wieder gesperrt wird.\r\n{#ELSE}\r\n<font color=\"red\">Deine Zeit ist leider um.</font><br /><br />Bitte beende alle Downloads, da dein Zugang jederzeit gesperrt werden kann!\r\n{#END}','<?php if(!empty(\$var0[\'inet_timeleft\'])) { ?>\r\n\r\n<font color=\"green\">Dein Internetzugang ist freigeschalten worden!</font><br /><br />\r\nDu hast noch<br /><br /><b><?php echo \$var0[\'inet_timeleft\'] ?></b><br /><br /> Zeit bis dein Zugang wieder gesperrt wird.\r\n<?php } else { ?>\r\n\r\n<font color=\"red\">Deine Zeit ist leider um.</font><br /><br />Bitte beende alle Downloads, da dein Zugang jederzeit gesperrt werden kann!\r\n<?php } ?>',1);",'...');
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_table_entry` VALUES (NULL,NULL,$inet_messages_id,'expired','Der Zugang ist wieder gesperrt worden','<font color=\"red\">Dein Internetzugang ist wieder gesperrt worden!</font><br /><br />\r\nFalls ein Download l&auml;nger als erwartet braucht und durch die Sperrung unterbrochen wurde, wende dich bitte an die Orga.','<font color=\"red\">Dein Internetzugang ist wieder gesperrt worden!</font><br /><br />\r\nFalls ein Download l&auml;nger als erwartet braucht und durch die Sperrung unterbrochen wurde, wende dich bitte an die Orga.',0);",'...');
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_table_entry` VALUES (NULL,NULL,$inet_messages_id,'noexpiry','Der User wurde auf unbegrenzte Zeit freigeschalten','<font color=\"blue\">Dein Internetzugang wurde auf unbegrenzte Zeit freigeschalten.</font>','<font color=\"blue\">Dein Internetzugang wurde auf unbegrenzte Zeit freigeschalten.</font>',0);",'...');
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_table_entry` VALUES (NULL,NULL,$inet_names_id,'waiting','Wartet','<font color=\"#bbbb00\"><b>Wartet<b></font>','<font color=\"#bbbb00\"><b>Wartet<b></font>',0);",'...');
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_table_entry` VALUES (NULL,NULL,$inet_names_id,'granted','Freigeschalten','<font color=\"green\"><b>Freigeschalten<b></font>','<font color=\"green\"><b>Freigeschalten<b></font>',0);",'...');
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_table_entry` VALUES (NULL,NULL,$inet_names_id,'expired','Abgelaufen','<font color=\"red\"><b>Abgelaufen</b></font>','<font color=\"red\"><b>Abgelaufen</b></font>',0);",'...');
			_DBUExecQuery("INSERT INTO `".TblPrefix()."flip_table_entry` VALUES (NULL,NULL,$inet_names_id,'noexpiry','Dauerhaft freigeschalten','<font color=\"blue\"><b>Dauerhaft freigeschalten</b></font>','<font color=\"blue\"><b>Dauerhaft freigeschalten</b></font>',0);",'... Content-Tabellen erg&auml;nzt!');

			_DBUAddRight('inet_view','Erlaubt es, das Internet-Modul einzusehen und zu benutzen','orga');
			_DBUAddRight('inet_admin','Berechtigt, das Internet-Modul zu administrieren','orga');

		case(1428): // Korrektur in der flip.sql -> Siehe Commitlog
		 
		case(1430):
			$inet_messages_id = MysqlReadField('SELECT `id` FROM `'.TblPrefix().'flip_table_tables` WHERE `name`=\'inet_status_messages\'', 'id');
			$inet_names_id = MysqlReadField('SELECT `id` FROM `'.TblPrefix().'flip_table_tables` WHERE `name`=\'inet_status_names\'', 'id');
			_DBUUpdateByKey(TblPrefix().'flip_table_entry', array('display'=>'<strong>Du hast keinen Antrag gestellt.</strong><br /><br />\nKlicke auf den Button unten um einen zu stellen!'), array('table_id'=>$inet_messages_id, 'key'=>'unrequested'), 'Table inet_status_messages: "unrequested" Textformatierung auf CSS ge&auml;ndert.');
			_DBUUpdateByKey(TblPrefix().'flip_table_entry', array('display'=>'{#IF !empty($inet_timeleft)} <span style="color:green;">Dein Internetzugang ist freigeschalten worden!</span><br /><br /> Du hast noch<br /><br /><strong>{$inet_timeleft}</strong><br /><br /> Zeit bis dein Zugang wieder gesperrt wird. {#ELSE} <span style="color:red;">Deine Zeit ist leider um.</span><br /><br />Bitte beende alle Downloads, da dein Zugang jederzeit gesperrt werden kann!{#END}'), array('table_id'=>$inet_messages_id, 'key'=>'granted'), 'Table inet_status_messages: "granted" Textformatierung auf CSS ge&auml;ndert.');
			_DBUUpdateByKey(TblPrefix().'flip_table_entry', array('display'=>'<span style="color:#bbbb00;">Du bist in der Warteschlange (Platz {$inet_queueposition})!</span><br /><br /> Bitte gedulde dich, bis du an der Reihe bist<br /> oder nehme deinen Antrag zur&uuml;ck, indem du auf den Button unten klickst.'), array('table_id'=>$inet_messages_id, 'key'=>'waiting'), 'Table inet_status_messages: "waiting" Textformatierung auf CSS ge&auml;ndert.');
			_DBUUpdateByKey(TblPrefix().'flip_table_entry', array('display'=>'<span style="color:red;">Dein Internetzugang ist wieder gesperrt worden!</span><br /><br /> Falls ein Download l&auml;nger als erwartet braucht und durch die Sperrung unterbrochen wurde, wende dich bitte an die Orga.'), array('table_id'=>$inet_messages_id, 'key'=>'expired'), 'Table inet_status_messages: "expired" Textformatierung auf CSS ge&auml;ndert.');
			_DBUUpdateByKey(TblPrefix().'flip_table_entry', array('display'=>'<span style="color:blue;">Dein Internetzugang wurde auf unbegrenzte Zeit freigeschalten.</span>'), array('table_id'=>$inet_messages_id, 'key'=>'noexpiry'), 'Table inet_status_messages: "noexpiry" Textformatierung auf CSS ge&auml;ndert.');
			_DBUUpdateByKey(TblPrefix().'flip_table_entry', array('display'=>'<span style="color:red;"><strong>Abgelaufen</strong></span>'), array('table_id'=>$inet_names_id, 'key'=>'expired'), 'Table inet_status_names: "expired" Textformatierung auf CSS ge&auml;ndert.');
			_DBUUpdateByKey(TblPrefix().'flip_table_entry', array('display'=>'<span style="color:green;"><strong>Freigeschalten<strong></span>'), array('table_id'=>$inet_names_id, 'key'=>'granted'), 'Table inet_status_names: "granted" Textformatierung auf CSS ge&auml;ndert.');
			_DBUUpdateByKey(TblPrefix().'flip_table_entry', array('display'=>'<span style="color:blue;"><strong>Dauerhaft freigeschalten</strong></span>'), array('table_id'=>$inet_names_id, 'key'=>'noexpiry'), 'Table inet_status_names: "noexpiry" Textformatierung auf CSS ge&auml;ndert.');
			_DBUUpdateByKey(TblPrefix().'flip_table_entry', array('display'=>'<span style="color:#bbbb00;"><strong>Wartet<strong></span>'), array('table_id'=>$inet_names_id, 'key'=>'waiting'), 'Table inet_status_names: "waiting" Textformatierung auf CSS ge&auml;ndert.');
	   
		case(1438):
			$contentGroupID = MysqlReadField('SELECT id FROM `'.TblPrefix().'flip_content_groups` WHERE name=\'system\'', 'id');
			_DBUExecInsert(TblPrefix().'flip_content_text', array('group_id'   => $contentGroupID,
				'name'      => 'inet_main',
				'view_right' => 0,
				'edit_right' => GetRightID('edit_public_text'),
				'caption'    => 'Internet freischalten',
				'text'       => "Hier kannst du dir den Internetzugang auf der LAN freischalten, damit du\nz.B. dein Steam updaten kannst.<br />\nFalls du sofort freigeschalten wirst, erh&auml;lst du umgehend eine Webmessage, der\ndu weitere Infos entnehmen kannst.<br />\nSollten bereits User vor dir freigeschalten worden sein, dann wirst du automatisch<br />\nin eine Warteschlange eingereiht und wirst freigeschalten, wenn du an der Reihe bist.",
			),'Content-Text inet_main hinzugef&uuml;gt.');

		case(1440):
			_DBUExecQuery('ALTER TABLE `'.TblPrefix().'flip_tournament_tournaments` CHANGE `liga` `liga` ENUM( \'Keine\', \'WWCL\', \'NGL\' ) NOT NULL', 'Wert von \'liga\' in flip_tournament_tournaments von "" zu "Keine" ge&auml;ndert.');

		case(1465):
			_DBUExecQuery('ALTER TABLE `'.TblPrefix().'flip_tournament_tournaments` CHANGE `type` `type` ENUM( \'ko\', \'group\', \'dm\' ) NOT NULL DEFAULT \'ko\'', 'flip_tournament_tournaments: \'dm\' zu Spalte `type` hinzugef&uuml;gt.');


		case(1466):
			_DBUExecQuery('ALTER TABLE `'.TblPrefix().'flip_tournament_servers` CHANGE `game` `game` VARCHAR( 20 ) NOT NULL','flip_tournament_servers: "game" auf 20 Zeichen verl&auml;ngert');
			
		case(1469):
			// Foren-Bug, bei dem sich die Foren nicht sortieren lie&szlig;en
			$sort_id = MysqlReadField('SELECT `id` FROM `'.TblPrefix().'flip_user_column` WHERE `type` = \'forum\' AND `name` = \'sort_index\';');
			_DBUExecQuery('UPDATE `'.TblPrefix().'flip_user_column` SET `required` = \'Y\' WHERE `id` = \''.$sort_id.'\';','Sort-Index der Foreneingenschaft sort_index zum Pflichtfeld gemacht');
			
			// Indexe fixen
			include('mod/mod.forum.php');
			ForumFixSortIndexes();
			
			// Content-Text
			_DBUExecQuery('INSERT INTO `'.TblPrefix().'flip_content_text` VALUES (\'\',NULL,1,\'user_subject_type\',0,2,\'Subjekttypen\',\'Hier sind alle Subjekttypen, die im FLIP vorhanden sind zusammengefasst\',\'Alle Subjekttypen im FLIP\',1296,1191789778);','Content-Text f&uuml;r die Subjekttypen erg&auml;nzt');
		case(1471): // Korrektur...
		case(1472): // Kalender: Anmeldung zu Events eigebaut
			// DB Erstellen
			_DBUExecQuery('CREATE TABLE `'.TblPrefix().'flip_calender_signups` (' .
					'`id` int(11) NOT NULL auto_increment,' .
					'`mtime` timestamp(14) NOT NULL,' .
					'`event_id` int(11) NOT NULL,' .
					'`user_id` int(11) NOT NULL,' .
					'PRIMARY KEY  (`id`),' .
					'UNIQUE KEY `event_id` (`event_id`,`user_id`)' .
					') TYPE=MyISAM;','Tabelle flip_calender_signons erstellt');
			// +Col can_signup @ types
			_DBUExecQuery('ALTER TABLE `'.TblPrefix().'flip_calender_types` ' .
					'ADD `can_signup` tinyint(1) NOT NULL default \'0\' ' .
					'AFTER `is_point`; ','Spalte can_signup zur Tabelle flip_calender_types hinzugef&uuml;gt');
			// +Col signup_right @ events
			_DBUExecQuery('ALTER TABLE `'.TblPrefix().'flip_calender_events` ' .
					'ADD `signup_right` int(11) NOT NULL default \'0\' ' .
					'AFTER `edit_right`; ','Spalte signup_right zur Tabelle flip_calender_events hinzugef&uuml;gt');
		case(1477):
			_DBUSetConfig('lanparty_stats_regasnonfree','Y','Bestimmt, ob zur LAN angemeldete User mit zur Menge der belegten Pl&auml;tze im Statusbalken gez&auml;hlt werden oder nicht','YesNo');
		case(1485): // 3 Querys. Alles in einem String will MySQL irgendwie nicht
			_DBUExecQuery('ALTER TABLE `'.TblPrefix().'flip_paypal_transactions` DROP PRIMARY KEY;','PayPal-Tabelle angepasst - I');
			_DBUExecQuery('ALTER TABLE `'.TblPrefix().'flip_paypal_transactions` ADD UNIQUE (`tx_id`);','PayPal-Tabelle angepasst - II');
			_DBUExecQuery('ALTER TABLE `'.TblPrefix().'flip_paypal_transactions` ADD COLUMN `id` int(11) NOT NULL auto_increment PRIMARY KEY FIRST;','PayPal-Tabelle angepasst - III');
			
		case(1486):
		case(1490): // UTF8
		case(1492): // UTF8
			 _DBUSetConfig('lanparty_password','','Gibt ein Passwort an, dass der Nutzer vor dem Anmelden zur LAN eingeben muss.','String');
			 _DBUSetConfig('lanparty_password_required', 'N', 'Gibt an, ob zur Anmeldung das oben genannte Passwort benötigt wird oder nicht', 'YesNo');
			 
		case(1498): // Turniertyp points
			_DBUExecQuery('ALTER TABLE `'.TblPrefix().'flip_tournament_tournaments` CHANGE `type` `type` ENUM(\'ko\', \'group\', \'dm\', \'points\') NOT NULL DEFAULT \'ko\' ', 'tournament_tournaments: Spalte types um "points" ergänzt.');
			
		case(1501):
			
		//****** Galerie hinzugefügt*********
			
		//Rechte
		_DBUAddRight('gallery_admin', 'Erlaubt es, die Bildergalerie zu administrieren','orga');
			
		//Tabellen
		_DBUExecQuery("CREATE TABLE `".TblPrefix()."flip_gallery_categories` (
      `id` int(11) NOT NULL auto_increment,
      `name` varchar(255) NOT NULL,
      `beschreibung` varchar(100)NOT NULL,
      `Parentid` int(11) NOT NULL,
      `position` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY  (`id`)) TYPE=MyISAM;", "Tabelle ".TblPrefix()."flip_gallery_categories erstellt.");

		_DBUExecQuery("CREATE TABLE `".TblPrefix()."flip_gallery_pics` (
      `id` int(11) NOT NULL auto_increment,
      `dateiname` varchar(60) NOT NULL,
      `name` varchar(60) NOT NULL,
      `catid` int(11) NOT NULL,
      `clicks` int(60) NOT NULL,
      PRIMARY KEY  (`id`)) TYPE=MyISAM;", "Tabelle ".TblPrefix()."flip_gallery_pics erstellt.");
			
			//Config-Einträge
			_DBUSetConfig('gallery_file_ends', 'jpg,gif,png','Nur Dateien mit diesen Endungen können den Kategorien hinzugefügt werden. Durch Komma trennen und ohne Punkt davor.', 'string');
			_DBUSetConfig('gallery_hits', 'Y', 'Soll gezählt und angezeigt werden, wie oft ein Bild angesehen wurde?', 'YesNo');
			_DBUSetConfig('gallery_maxheight', '600', 'Bestimmt die maximale Anzeigehöhe eines Bildes. Größere Bilder werden automatisch skaliert.', 'Integer');
			_DBUSetConfig('gallery_maxpics','5','Die Anzahl der Bilder in einer Reihe','Dropdown', 'Integer');
			_DBUSetConfig('gallery_maxrows', '4', 'Die maximale Anzahl Bilderreihen pro Seite', 'Integer');
			_DBUSetConfig('gallery_maxwidth', '800', 'Bestimmt die maximale Breite eines Bildes. Zu große Bilder weden beim Anzeigen automatisch skaliert.', 'Integer');
			_DBUSetConfig('gallery_picpath', 'media/pics', 'Gibt den Ort an, wo die Bild-Dateien liegen werden. Dieser Ordner muss beschreibbar sein!', 'string');
			_DBUSetConfig('gallery_thmaxheight', '75', 'Bestimmt die maximale Höhe der Thumbnails in der Kategorieansicht.', 'Integer');
			_DBUSetConfig('gallery_thmaxwidth', '100', 'Bestimmt die maximale Breite der Thumbnails in der Kategorieansicht.', 'Integer');
			_DBUSetConfig('gallery_thumbpath', 'media/thumbnails', 'Gibt den Ort an, wo die Thumbnails der Galerie liegen werden. Dieser Ordner muss beschreibbar sein!', 'string');
			
			//Config Values
			$id = MysqlReadField('SELECT `id` FROM '.TblPrefix().'flip_config WHERE `key`=\'gallery_maxpics\'','id');
			if ($id > 0) {
				_DBUExecInsert(TblPrefix().'flip_config_values', array('config_id' => $id, 'value' => 1),'Value für gallery_maxpics');
				_DBUExecInsert(TblPrefix().'flip_config_values', array('config_id' => $id, 'value' => 2),'Value für gallery_maxpics');
				_DBUExecInsert(TblPrefix().'flip_config_values', array('config_id' => $id, 'value' => 3),'Value für gallery_maxpics');
				_DBUExecInsert(TblPrefix().'flip_config_values', array('config_id' => $id, 'value' => 4),'Value für gallery_maxpics');
				_DBUExecInsert(TblPrefix().'flip_config_values', array('config_id' => $id, 'value' => 5),'Value für gallery_maxpics');
				_DBUExecInsert(TblPrefix().'flip_config_values', array('config_id' => $id, 'value' => 6),'Value für gallery_maxpics');
			}
			
			//Menueintrag
			_DBUUpdateMenu('info', 'gallery', 'gallery.php', '', 'Bildergalerie');
				 
			_DBUUpdateEnd(1506);
			break;

		default:
			$msg = 'Fehler: Die Datenbank konnte nicht automatisch geupdated werden, da f&uuml;r diese '.
             "Version <b>($DBRevision)</b> kein automatisches Update zur Verf&uuml;gung steht. ".
             'Damit das FLIP funktioniert, wird Version <b>'.REQUIRED_DB_VERSION.'</b> ben&ouml;tigt. '.
             "Du kannst wieder Version $DBRevision vom FLIP installieren, selbst die Datenbank ".
             'aktualisieren oder die Entwickler von www.flipdev.org darum bitten, ein Datenbankupdate zur Verf&uuml;gung zu stellen.';
			LogDBUpdate($msg);
			trigger_error($msg,E_USER_WARNING);
			_DBUUpdateEnd($DBRevision);
	}

}
?>
