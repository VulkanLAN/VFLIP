<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: mod.seats.php 1702 2019-01-09 09:01:12Z loom $ edit by VulkanLAN
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** Die Datei nur einmal includen */
if (defined("MOD.SEATS.PHP"))
	return 0;
define("MOD.SEATS.PHP", 1);

function GetSeat($id) {
	$id = escape_sqlData($id);
	$r = MysqlReadRow("
	    SELECT s.*,u.name AS nickname FROM (".TblPrefix()."flip_seats_seats s)
	    LEFT JOIN ".TblPrefix()."flip_user_subject u ON (s.user_id = u.id)
	    WHERE (s.id = $id); ");
	if (empty ($r))
		return trigger_error_text("Der Sitzplatz mit der ID \"$id\" existiert nicht.", E_USER_ERROR);
	return $r;
}

function GetSeats($block = false) {
	if ($block)
		$w = "s.block_id='".escape_sqlData_without_quotes($block)."'";
	else
		$w = "1";
	return MysqlReadArea("
	    SELECT s.*,u.name AS nickname FROM (".TblPrefix()."flip_seats_seats s)
	    LEFT JOIN ".TblPrefix()."flip_user_subject u ON (s.user_id = u.id)
	    WHERE $w;", "id");
}

function DrawSeat($row, $img, $overview, $drawblock = true) {
	include_once ("mod/mod.user.php");
	include_once ("mod/mod.seats.draw.php");

	$aStatus = (empty ($row["user_id"]) or ($row["reserved"] != "Y")) ? "" : GetValidSeatStatus(UserGetStatus($row["user_id"]));
	$block = SeatGetBlock($row["block_id"], false);
	$blockseat = $row;
	$blockseat["user_status"] = $aStatus;
	if ($drawblock) {
		// den im block sitz zeichnen
		if (empty ($block["imagedir_block"]))
			return trigger_error_text("Der Block ".$block["caption"]." konnte nicht neu gezeichnet werden, da kein Verzeichnis f&uuml;r seine Block-Tischbilder angegeben wurde.", E_USER_WARNING);
		$cords = DrawSeatUnselected($block["imagedir_block"], $img, $row);
		if (!$cords)
			$cords = DrawDummySeat($img, $row);
		$cords = serialize($cords);
		// wenn sich Daten ge&auml;ndert haben, diese speichern.
		if (($cords != $row["cords"]) or ($aStatus != $row["user_status"]));
		MysqlWriteByID(TblPrefix()."flip_seats_seats", array ("cords" => $cords, "user_status" => $aStatus), $row["id"]);
	}
	// den sitz in der &uuml;bersicht zeichnen
	// edit VulkanLAN: wenn der Sitzplan in der overview neu gezeichnet wird
	$seats_new_template_global = ConfigGet("seats_new_template");
			if ($seats_new_template_global != 'Y'){ 
	if (!empty ($block["scale"])) {
		if (empty ($block["imagedir_overview"]))
			return trigger_error_text("Der Block ".$block["caption"]." konnte nicht neu gezeichnet werden, da kein Verzeichnis f&uuml;r seine &uuml;bersichts-Tischbilder angegeben wurde.", E_USER_WARNING);
		$over = $blockseat;
		$over["center_x"] = $block["topleft_x"] + ($block["scale"] * $row["center_x"]);
		$over["center_y"] = $block["topleft_y"] + ($block["scale"] * $row["center_y"]);
		if (!DrawSeatUnselected($block["imagedir_overview"], $overview, $over))
						DrawDummySeat($overview, $over);}
	}
}

function GetValidSeatStatus($Satus) {
	return (in_array($Satus, array ("checked_in", "checked_out", "online", "offline"))) ? $Satus : "";
}

function DrawDummySeat($image, $seat) {
	include_once ("mod/mod.imageedit.php");
	$i = new ResImage(new ImageLoaderNew(15, 11, false));
	imagecolortransparent($i->Res, ImageColorAllocate($i->Res, 255, 255, 255));
	imagefill($i->Res, 1, 1, imagecolorallocate($i->Res, 0, 255, 0));
	$text_color = ImageColorAllocate($i->Res, 255, 0, 0);
	imagestring($i->Res, 3, 0, 0, "XZ", $text_color);
	return $image->drawRotated($i, $seat["center_x"], $seat["center_y"], $seat["angle"], true);

}

/**
 * Zeichnet ein oder alle Bl&ouml;cke neu.
 * @param int $block Die ID des Blockes, der neu gezeichnet werden soll. Ist sie 0, 
 *                   werden alle Blcke neu gezeichnet.
 */
function RedrawSeats($block = 0) {
	include_once ("mod/mod.imageedit.php");
	include_once ("mod/mod.user.php");
	$query = "";
	if ($block > 0) {
		//ein Block wurde angegeben
		$query .= " WHERE block_id='$block'";
		$blockids = array ($block);
		$over = new ResImage(SeatsGetOverviewImageTmpLoader()); #bisheriger &uuml;bersichtsplan
	} else {
		//alle Bl&ouml;cke sollen neu gezeichnet werden
		$blockids = MysqlReadCol("SELECT block_id FROM ".TblPrefix()."flip_seats_seats GROUP BY block_id;", "block_id");
		$over = new ResImage(SeatsGetOverviewImageLoader()); #bisheriger &uuml;bersichtsplan
	}
	$imgs = array();
	foreach ($blockids AS $blockid)
		$imgs[$blockid] = new ResImage(SeatsGetBackgroundLoader($blockid));
	//alle SitzPl&auml;tze einzeln neu zeichnen
	$query = "SELECT * FROM ".TblPrefix()."flip_seats_seats WHERE block_id IN (".implode_sqlIn($blockids).");";
	foreach (MysqlReadArea($query) AS $seat) {
		$img = $imgs[$seat["block_id"]];
		// edit VulkanLAN: wenn der Sitzplan neu gezeichnet wird (admin funktion - sitzplan erstellen) wird auf diese funktion zugegriffen
		DrawSeat($seat, $img, $over, true);
	}
	//speichern
	foreach ($imgs AS $blockid => $img)
		SaveSeats($img, $blockid);
	SeatsSaveOverviewTmp($over);
}

function SaveSeats($Img, $block) {
	$Img->setFormat("png", 0);
	MysqlWriteByID(TblPrefix()."flip_seats_blocks", array ("background_tmp" => $Img->getData()), $block);
}

function SeatsGetBackgroundTmpLoader($block) {
	$mtime = MysqlReadFieldByID(TblPrefix()."flip_seats_blocks", "mtime", $block);
	// Vulkan-LAN Mod
			$seats_new_template_global = ConfigGet("seats_new_template");
			if ($seats_new_template_global != 'Y'){ 
	return new ImageLoaderDBField(TblPrefix()."flip_seats_blocks", "background_tmp", $block, $mtime);
}
			else {
	return new ImageLoaderDBField(TblPrefix()."flip_seats_blocks", "background", $block, $mtime);
}
}

function SeatsGetBackgroundLoader($block) {
	$mtime = MysqlReadFieldByID(TblPrefix()."flip_seats_blocks", "mtime", $block);
	return new ImageLoaderDBField(TblPrefix()."flip_seats_blocks", "background", $block, $mtime);
}

function SeatsGetOverviewImageLoader() {
	return new ImageLoaderDB(ConfigGet("seats_baseplan"));
}

function SeatsGetOverviewImageTmpLoader() {
	if (ImageExists(ConfigGet("seats_tmpplan")))
		return new ImageLoaderDB(ConfigGet("seats_tmpplan"));
	else
		return SeatsGetOverviewImageLoader();
}

function SeatsGetOverviewImageTmp() {
	include_once ("mod/mod.imageedit.php");
	static $cache = null;
	if (!is_null($cache))
		return $cache;
	return $cache = new ResImage(SeatsGetOverviewImageTmpLoader());
}

function SeatsSaveOverviewTmp($image) {
	$image->setFormat("png", 0);
	$image->saveToDB(ConfigGet("seats_tmpplan"), array (), false, false);
}

function SeatsSetUserStatus($User, $Status) {
	$id = GetSubjectID($User);
	$s = addslashes($Status);
	$r = MysqlReadArea("SELECT * FROM `".TblPrefix()."flip_seats_seats` 
					    WHERE ((`reserved` = 'Y') AND (`user_id` = '$id') AND (`user_status` != '$s'))
					    ORDER BY `block_id`;
					  ");
	if (empty ($r))
		return;

	include_once ("mod/mod.imageedit.php");
	$over = new ResImage(SeatsGetOverviewImageTmpLoader());
	$oldblock = $img = null; //damit Eclipse nicht meckert ;)
	foreach ($r as $v) {
		if ($oldblock != $v["block_id"]) {
			if (isset ($oldblock))
				SaveSeats($img, $oldblock);
			$oldblock = $v["block_id"];
			$img = new ResImage(SeatsGetBackgroundTmpLoader($v["block_id"]));
		}
		// edit VulkanLAN: 
		$seats_new_template_global = ConfigGet("seats_new_template");
			if ($seats_new_template_global != 'Y'){
		DrawSeat($v, $img, $over);
			}
		MysqlWriteByID(TblPrefix()."flip_seats_seats", array ("user_status" => $Status), $v["id"]);
	}
	SaveSeats($img, $v["block_id"]);
	SeatsSaveOverviewTmp($over);
}

function GetUserIPBySeat($User) {
	$id = GetSubjectID($User);
	return MysqlReadField("SELECT `ip` FROM `".TblPrefix()."flip_seats_seats` WHERE ((`reserved` = 'Y') AND (`user_id` = '$id'));", 0, true);
}

function SeatGetUserIDByIP($IP) {
	$ip = addslashes($IP);
	return MysqlReadField("SELECT `user_id` FROM `".TblPrefix()."flip_seats_seats` WHERE ((`reserved` = 'Y') AND (`ip` = '$ip'));", 0, true);
}

function SeatGetSeatByUser($aUser) {
	$id = GetSubjectID($aUser);
	return MysqlReadField("SELECT `name` FROM `".TblPrefix()."flip_seats_seats` WHERE ((`user_id` = '$id') AND (`reserved` = 'Y'));", 0, true);
}

function SeatGetBlockByUser($aUser) {
	$id = GetSubjectID($aUser);
	return MysqlReadField("SELECT `block_id` FROM `".TblPrefix()."flip_seats_seats` WHERE ((`user_id` = '$id') AND (`reserved` = 'Y'));", 0, true);
}

function SeatFreeSeatByUser($aUser) {
	$id = GetSubjectID($aUser);
	$blocks = MysqlReadCol("SELECT block_id FROM ".TblPrefix()."flip_seats_seats WHERE user_id='$id'", "block_id");
	$r = MysqlWrite("UPDATE `".TblPrefix()."flip_seats_seats` SET `reserved` = 'N' ,`user_id` = 0 WHERE (`user_id` = '$id');");
	foreach ($blocks AS $bid)
		RedrawSeats($bid);
	return $r;
}

function SeatUnreserveSeatByUser($aUser) {
	$id = GetSubjectID($aUser);
	$blocks = MysqlReadCol("SELECT block_id FROM ".TblPrefix()."flip_seats_seats WHERE user_id='$id'", "block_id");
	$r = MysqlWrite("UPDATE `".TblPrefix()."flip_seats_seats` SET `reserved` = 'N' WHERE (`user_id` = '$id' AND `reserved`='Y');");
	foreach ($blocks AS $bid)
		RedrawSeats($bid);
	return $r;
}

function SeatsGetReservedSeats() {
	return MysqlReadArea("SELECT `block_id`, `user_id`, `name`, `reserved`, `id` FROM ".TblPrefix()."flip_seats_seats WHERE reserved='Y'", "user_id");
}

function SeatsGetCounters() {
	return array ("max" => MysqlReadField("SELECT COUNT(*) AS max FROM ".TblPrefix()."flip_seats_seats", "max"), "nonfree" => MysqlReadField("SELECT COUNT(*) AS nonfree FROM ".TblPrefix()."flip_seats_seats WHERE user_id <> 0", "nonfree"), "enabled" => MysqlReadField("SELECT COUNT(*) AS enabled FROM ".TblPrefix()."flip_seats_seats WHERE enabled = 'Y'", "enabled"), "taken" => MysqlReadField("SELECT COUNT(*) AS taken FROM ".TblPrefix()."flip_seats_seats WHERE reserved = 'Y'", "taken"));
}

function SeatsUpdateByArray($Seats) {
	$oldSeatIDs = MysqlReadCol("SELECT `id` FROM `".TblPrefix()."flip_seats_seats`;", "id", "id");
	// alte tische updaten und neue hinzuf&uuml;gen.
	$blocks = array ();
	foreach ($Seats as $seat) {
		if (!in_array($seat["block_id"], $blocks))
			$blocks[] = $seat["block_id"];
		if (empty ($oldSeatIDs[$seat["id"]])) {
			MysqlWriteByID(TblPrefix()."flip_seats_seats", $seat);
		} else {
			unset ($oldSeatIDs[$seat["id"]]);
			MysqlWriteByID(TblPrefix()."flip_seats_seats", $seat, $seat["id"]);
		}
	}
	// tische, die nicht in $Seats enthalten waren, entfernen  
	foreach ($oldSeatIDs as $id)
		MysqlDeleteByID(TblPrefix()."flip_seats_seats", $id);
	// Sitzplan neu zeichnen
	$seats_new_template_global = ConfigGet("seats_new_template");
		if ($seats_new_template_global != 'Y'){ }
	RedrawSeats();
			
	foreach ($blocks As $aBlockID)
		SeatsCalcBlockCords($aBlockID);
}

function _getBlockCols() {
	return "`id`,`mtime`,`view_right`,`caption`,`description`,`topleft_x`,`topleft_y`,`scale`,`imagedir_block`,`imagedir_overview`,`cords`,`link`,`is_adult`,".sqlImage(TblPrefix()."flip_seats_blocks", "background").", ".sqlImage(TblPrefix()."flip_seats_blocks", "background_tmp").", IF(LENGTH(`link`) > 0, `link`, CONCAT('seats.php?frame=block&blockid=',`id`)) AS `href`";
}

function SeatGetBlocks() {
	global $User;
	return MysqlReadArea("SELECT "._getBlockCols()." FROM ".TblPrefix()."flip_seats_blocks WHERE ".$User->sqlHasRight("view_right")." ORDER BY `caption`");
}

/**
 * Liefert Informationen &uuml;ber einen Block zur&uuml;ck. 
 * Zu diesen Informationen geh&ouml;ren alle Spalten der Tabelle `flip_seats_blocks`, 
 * mit Ausnahme von `background` und `background_tmp`. Wenn der Block noch 
 * keinen background_tmp hat, wir dieser neu gezeichnet.
 */
function SeatGetBlock($Ident, $allowredraw = true) {
	static $cache = array ();
	if (empty ($Ident))
		return false;
	$Ident = escape_sqlData_without_quotes($Ident);

	if (isset ($cache[$Ident]))
		return $cache[$Ident];
	$r = MysqlReadRow("SELECT "._getBlockCols()."FROM ".TblPrefix()."flip_seats_blocks WHERE `id`='$Ident' OR `caption`='$Ident'", false, "Der Sitzblock mit der ID $Ident existiert nicht. Wenn du ihn nicht explizit angefordert hat, gibt es m&ouml;glicher weise noch Tische, die diesem Block zugeordnet sind.");
	if (empty ($r["background_tmp"]) and $allowredraw and !empty ($r["background"]))
		RedrawSeats($r["id"]);
	return $cache[$Ident] = $r;
}

function SeatsGetOverviewHeader() {
	global $User;
	$r = array ();
	$r["frame"] = (isset($_GET["frame"])) ? $_GET["frame"] : "";
	$r["showadmin"] = $User->hasRight("seats_admin");
	return $r;
}

function SeatsCalcBlockCords($id) {
	include_once ("mod/mod.seats.draw.php");
	$block = SeatGetBlock($id);
	$cords = MysqlReadArea("SELECT `center_x`,`center_y` FROM `".TblPrefix()."flip_seats_seats` WHERE `block_id`='".addslashes($id)."'");
	if (empty ($cords))
		return trigger_error_text("Die Link-Koordinaten vom Block ".$block["caption"]." konnten nicht berechnet werden, da dieser Block keine Tische enth&auml;lt.", E_USER_WARNING);

	$table = GetSeatImage($block["imagedir_block"], "default");
	$minx = $miny = 0xFFFFFFFF;
	$maxx = $maxy = 0;
	$scale = $block["scale"];
	foreach ($cords as $c) {
		$minx = min($minx, $c["center_x"]);
		$miny = min($miny, $c["center_y"]);
		$maxx = max($maxx, $c["center_x"]);
		$maxy = max($maxy, $c["center_y"]);
	}
	$tablew = $table["width"] / 2;
	$tableh = $table["height"] / 2;
	$top = ceil($block["topleft_y"] + (($miny - $tableh) * $scale));
	$left = ceil($block["topleft_x"] + (($minx - $tablew) * $scale));
	$bottom = ceil($block["topleft_y"] + (($maxy + $tableh) * $scale));
	$right = ceil($block["topleft_x"] + (($maxx + $tablew) * $scale));

	$cords = "shape=\"rect\" coords=\"$left,$top,$right,$bottom\"";
	MysqlWriteByID(TblPrefix()."flip_seats_blocks", array ("cords" => $cords), $id);
}

function SeatsDeleteBlock($id) {
	$id = (int) $id;
	MysqlDeleteByID(TblPrefix()."flip_seats_blocks", $id);
	MysqlWrite("DELETE FROM ".TblPrefix()."flip_seats_seats WHERE (`block_id` = $id)");
}

function SeatsIsAdult($user) {
	$id = GetSubjectID($user);
	return MysqlReadField("SELECT b.is_adult FROM ".TblPrefix()."flip_seats_seats s, ".TblPrefix()."flip_seats_blocks b WHERE (b.id = s.block_id) AND (s.user_id = $id);", 0, true);
}
?>
