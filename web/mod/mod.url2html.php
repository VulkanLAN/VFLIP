<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: mod.lanparty.php 882 2005-03-16 12:04:17Z docfx $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** regul&auml;re Ausdr&uuml;cke */
//TLDREGEX ist in core.utils (f&uuml;r Email ben&ouml;tigt)
define('IPREGEX','(localhost|(25[0-5]|2[0-4][0-9]|[01]?[0-9]{1,2})(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9]{1,2})){3})');

function IsValidURL($url)
{
  // die url muss ein protokoll und eine topleveldomain enthalten, alles weitere ist egal.
  if(preg_match('/^[\w]{2,}:\/\/[\w\d\.\-]+'.TLDREGEX.'($|\/)/i',$url)) return true; // domain-url
  if(preg_match('/^[\w]{2,}:\/\/'.IPREGEX.'($|\/)/i',$url)) return true; // ip-url
  return false;
}

function IsValidDomain($domain) 
{
  return preg_match('/^[\d\w\.\-]+'.TLDREGEX.'$/',$domain);
}

function _urlcallback($a)
{
  $text = $a[0];
  if(strpos("\n",$text) !== false) return $text;
  elseif(IsValidEmail($text)) return "<a href=\"mailto:$text\">$text</a>";
  elseif(IsValidURL($text)) return "<a ".((stristr($text, ServerURL())===false)?"(target=\"_blank\"":"")."href=\"$text\">$text</a>";
  elseif(IsValidDomain($text)) return "<a ".((stristr(ServerURL(), $text)===false)?"(target=\"_blank\"":"")."href=\"http://$text\">$text</a>";
  else return "$text";
}

function url2html($Text) 
{
  return preg_replace_callback('/[^\s]+\.[\w\d]+[^\s]*/','_urlcallback',$Text);
}
  
 
?>