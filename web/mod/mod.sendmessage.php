<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: mod.sendmessage.php 1702 2019-01-09 09:01:12Z loom $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** Kernel */
include_once 'core/core.php';
include_once ("mod/mod.template.php");

// Checks if quoted_printable_encode exists since it is 
// part of PHP 5 >= 5.3.0
if(!function_exists('quoted_printable_encode')) {
	function quoted_printable_encode($str) {
		$lines = array ();
		foreach (explode("\n", $str) as $line) {
			$r = "";
			for ($i = 0; $i < strlen(rtrim($line)); $i++) {
				$o = ord($s = substr($line, $i, 1));
				if ((($o < 32) and ($o != 9)) or ($o > 126) or ($o == 61))
					$r .= sprintf("=%02X", $o);
				else
					$r .= $s;
				if ((($i % 76) == 0) and ($i > 0))
					$r .= "=\n";
			}
			$lines[] = $r;
		}
		return implode("\n", $lines);
	}
}

function iso_header_encode($str) {
	$s = quoted_printable_encode($str);
	if ($s != $str)
		$s = "=?iso-8859-1?Q?$s?=";
	return addcslashes($s, "\r\n");
}

function GetMessageTypes($AddCustom = true) {
	$NoticeTypes = array (
		"webmessage" => "WebMessage (keine Benachrichtigung)",
		"webmessage_emailnotice" => "WebMessage (Benachrichtigung via Email)",
		"webmessage_jabbernotice" => "WebMessage (Benachrichtigung via Jabber)",
		"webmessage_emailmessage" => "WebMessage (ganze Nachricht via Email)",
		"webmessage_jabbermessage" => "WebMessage (ganze Nachricht via Jabber)"
	);

	$r = ($AddCustom) ? array (
		"custom" => "Custom"
	) : array ();
	$a = array ();
	foreach (get_class_methods("message") as $n)
		if (preg_match("/^_send(.*)$/i", strtolower($n), $a)) {
			if ($AddCustom)
				$r[$a[1]] = (isset ($NoticeTypes[$a[1]])) ? $NoticeTypes[$a[1]] : $a[1];
			elseif (isset ($NoticeTypes[$a[1]])) $r[$a[1]] = $NoticeTypes[$a[1]];
		}
	return $r;
}

function GetMessageVars() {
	return array_keys(GetColumns("user"));
}

function LoadMessageFromDB($aMessageName) {
	$i = addslashes($aMessageName);
	$typ = (is_posDigit($i)) ? "id" : "name";
	$m = MysqlReadRow("SELECT * FROM `" . TblPrefix() . "flip_sendmessage_message` WHERE (`$typ` = '$i');");
	$v = null;
	if (is_array($m) && isset($m["vars"]) && !empty ($m["vars"]))
		$v = unserialize($m["vars"]);
	if (!is_array($v))
		$v = array ();
	$m["vars"] = implode(", ", array_merge($v, GetMessageVars()));
	return $m;
}

function SendSysMessage($MessageName, $Receiver = NULL, $aType = "email") {
	$m = new ExecMessage();
	$m->Type = $aType;
	if (!$m->messageFromDB($MessageName))
		return false;
	return $m->sendMessage($Receiver);
}

/**
 * Sendet eine Nachricht
 * 
 * @param mixed $Receiver Empf&auml;nger Ident (muss User sein)
 * @param object $Sender Sender (Instanz von User, sonst System-User)
 * @param String $Subject Betreff
 * @param String $Message Nachrichtentext
 * @param int $SourceMessageID ID einer Bezugsnachricht
 * @param String $SourceSendType Typ der Bezugsnachricht
 * @param String $Type Typ der Nachricht, wenn nicht angegeben wird die Usereinstellung verwendet
 * @param boolean $Signature Wenn true, wird der Nachricht eine Signatur aus dem Profil des Senders angeh&auml;ngt
 * 
 */
function SendMessageSendMessage($Receiver, $Sender, $Subject, $Message, $SourceMessageID = 0, $SourceSendType = "", $Type = "custom", $Signature = false) {
	if (!SubjectExists($Receiver, "user")) {
		trigger_error("Die Nachricht konnte nicht gesendet werden:<br />\nDer Empf&auml;nger ist unbekannt.|Empf&auml;nger=" . $Receiver, E_USER_WARNING);
		return false;
	}
	$m = new Message();
	$m->Sender = $Sender; //UserInstance
	$m->Subject = $Subject;
	$m->Message = $Message;
	$m->WebMsgSourceID = $SourceMessageID;
	$m->WebMsgSourceType = $SourceSendType;
	$m->Type = $Type;
	$m->AppendSignature = $Signature;
	return $m->sendMessage(CreateSubjectInstance($Receiver));
}

class ExecMessage extends Message {
	var $Params = array ();

	function messageFromDB($aMessageName) {
		$i = addslashes($aMessageName);
		$m = MysqlReadRow("SELECT `message`,`subject` FROM `" . TblPrefix() . "flip_sendmessage_message` WHERE ((`id` = '$i') OR (`name` = '$i'));");
		return $this->setMessage($m["subject"], $m["message"]);
	}

	function setMessage($aSubject, $aMessage) {
		$s = TemplateCompile($aSubject);
		$m = TemplateCompile($aMessage);
		if (empty ($s) or empty ($m))
			return false;
		$this->Subject = $s;
		$this->Message = $m;
		return true;
	}

	function getMessage(& $aSubject, & $aMessage) {
		$p = array_merge($this->Receiver->getProperties(), $this->Params);
		$p["server"] = $_SERVER;
		$p["server"]["SCRIPT_DIR"] = dirname($_SERVER["PHP_SELF"]);
		$aSubject = addcslashes(trim(TemplateExecute($this->Subject, $p)), "\r\n");
		if (empty ($aSubject))
			return false;
		$aMessage = TemplateExecute($this->Message, $p);
		if (empty ($aMessage))
			return false;
		$aMessage = wordwrap(trim($aMessage), 75);
		return true;
	}
}

class Message {
	var $Subject = "";
	var $Message = "";
	var $Type = "custom";
	var $Sender = NULL;
	var $Receiver = NULL;
	var $_LastWebMsgID = 0;
	var $IsHtml = true;
	var $AppendSignature = false;

	var $WebMsgSourceID = 0;
	var $WebMsgSourceType = "";

	function _prepareVars(& $Obj, $Subj, $Msg) {
		$Obj->Params["subject"] = $Subj;
		$Obj->Params["message"] = $Msg;
		$Obj->Params["sender"] = $this->Sender->name;
		$Obj->Params["receiver"] = $this->Receiver->name;
		$Obj->Params["message_id"] = $this->_LastWebMsgID;
		//    $Obj->Params["receiver_id"] = $this->Receiver->id;
	}

	function _getNotice(& $Subj, & $Msg) {
		$m = new ExecMessage();
		$m->messageFromDB("messaging_notice");
		$this->_prepareVars($m, $Subj, $Msg);
		return $m;
	}

	function _getMessage(& $Subj, & $Msg) {
		$m = new ExecMessage();
		$m->messageFromDB("messaging_message");
		$this->_prepareVars($m, $Subj, $Msg);
		return $m;
	}

	function _sendWebMessage_EmailNotice($Subj, $Msg) {
		$r = $this->_sendWebMessage($Subj, $Msg);
		$m = $this->_getNotice($Subj, $Msg);
		$m->Type = "email";
		$m->sendMessage($this->Receiver);
		return $r;
	}

	function _sendWebMessage_JabberNotice($Subj, $Msg) {
		$r = $this->_sendWebMessage($Subj, $Msg);
		$m = $this->_getNotice($Subj, $Msg);
		$m->Type = "jabber";
		$m->sendMessage($this->Receiver);
		return $r;
	}

	function _sendWebMessage_EmailMessage($Subj, $Msg) {
		$r = $this->_sendWebMessage($Subj, $Msg);
		$m = $this->_getMessage($Subj, $Msg);
		$m->Type = "email";
		$m->sendMessage($this->Receiver);
		return $r;
	}

	function _sendWebMessage_JabberMessage($Subj, $Msg) {
		$r = $this->_sendWebMessage($Subj, $Msg);
		$m = $this->_getMessage($Subj, $Msg);
		$m->Type = "jabber";
		$m->sendMessage($this->Receiver);
		return $r;
	}

	function _sendEmail($Subj, $Msg) {
		if (ConfigGet("sendmessage_disable_email") == "Y")
			return "";

		$header = "From: " . iso_header_encode($this->Sender->name) . " <{$this->Sender->email}>\n";
		$header .= "MIME-Version: 1.0\n";
		$header .= "X-Mailer: FLIP v" . FLIP_VERSION_LONG ."\n";
		$header .= "Content-Type: " . (($this->IsHtml) ? "text/html;" : "text/plain;") . " charset=\"utf-8\"\n";
		$header .= "Content-Transfer-Encoding: quoted-printable";

		$msg = quoted_printable_encode($Msg); #@UTF8 if it's ISO there's nothing to decode so this should be safe
		$subj = iso_header_encode($Subj);

		if (mail($this->Receiver->email, $subj, $msg, $header))
			return "";
		return "Type:email From:{$this->Sender->email} To:{$this->Receiver->email}";
	}

	function _sendWebMessage($Subj, $Msg) {
		include_once ("mod/mod.webmessage.php");
		if ($this->_LastWebMsgID = WebMessageSendMessage($this->Receiver, $this->Sender, $Subj, $Msg, $this->WebMsgSourceID, $this->WebMsgSourceType))
			return "";
		return "Type:webmessage From:{$this->Sender->name} To:{$this->Receiver->name}";
	}

	function _sendJabber($Subj, $Msg) {
		$jid = $this->Receiver->getProperty("jid");
		if (empty ($jid))
			"JabberFehler: Empf&auml;nger hat keine JabberID Empf&auml;nger: {$this->Receiver->name}";

		include_once ("ext/class.jabber.php");
		$j = new Jabber();
		$j->enable_logging = true;
		$j->server = ConfigGet("sendmessage_jabber_server");
		$j->port = ConfigGet("sendmessage_jabber_port");
		$j->username = ConfigGet("sendmessage_jabber_name");
		$j->password = ConfigGet("sendmessage_jabber_pass");
		$j->resource = ConfigGet("sendmessage_jabber_resource");

		if (!$j->Connect())
			return "JabberFehler: Can't Connect: <br>\n" . implode("<br>\n", $j->log_array);
		if (!$j->SendAuth())
			return "JabberFehler: Can't Authentiate: <br>\n" . implode("<br>\n", $j->log_array);

		if (!$j->SendMessage($jid, "normal", NULL, array (
				"subject" => utf8_encode(escapeHtml($Subj
			)), "body" => utf8_encode(escapeHtml($Msg)))))
			return "JabberFehler: Can't send Message: <br>\n" . implode("<br>\n", $j->log_array);

		// logging aus, sonst wird der log beim disconnecten ausgegeben.
		$j->enable_logging = false;
		$j->Disconnect();
		return "";
	}

	function getMessage(& $aSubject, & $aMessage) {
		$aSubject = $this->Subject;
		$aMessage = $this->Message;
		// Signatur rankleben, wenn erw&uuml;nscht
		if($this->AppendSignature)
			$aMessage .= "\n\n" . $this->Receiver->getProperty("webmessage_signature");
		return true;
	}

	function sendMessage($aReceiver = NULL) {
		global $User;
		if (is_a($aReceiver, "user"))
			$this->Receiver = $aReceiver;
		if (!is_a($this->Receiver, "user"))
			$this->Receiver = & $User;

		// SYSTEM_USER_ID ist die ID vom User "system". Er kann in diesem Falle weder &uuml;ber seinen Namen noch &uuml;ber seine 
		// Emailadresse angesprochen werden, da die Lanpartys diese anpassen m&uuml;ssen.
		if (!is_a($this->Sender, "user"))
			$this->Sender = new User(SYSTEM_USER_ID);

		$type = ($this->Type == "custom") ? $this->Receiver->getConfig("sendmessage_receive_type") : $this->Type;
		//if(empty($this->Type)) trigger_//$this->Type = "webmessage";

		$subj = $msg = null;
		if (!$this->getMessage($subj, $msg))
			return false;
		$meth = "_send$type";
		if (method_exists($this, $meth)) {
			$err = $this-> $meth ($subj, $msg);
			if (empty ($err))
				return LogAction("Es wurde eine Nachricht mit dem Betreff \"{$this->Subject}\" von {$this->Sender->name} an {$this->Receiver->name} gesendet.");
			trigger_error("Es konnte keine Nachricht vom Typ \"$type\" an {$this->Receiver->name} gesendet werden.|$err", E_USER_WARNING);
		} else
			trigger_error("Ein Send-Type ist ung&uuml;ltig|Type:{$this->Type} To:{$this->Receiver->name}", E_USER_WARNING);
		return false;
	}
}
?>
