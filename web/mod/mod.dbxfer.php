<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: dbxfer.php 1054 2005-08-03 09:21:59Z loom $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** Konstanten */
define("MAX_BLOCK_SIZE", 409600);
define("EXCLUDE_TABLES", "flip_session_data,flip_session_cache");
define("DBXFER_HEADER", "DBXFer3"); 

require_once ("mod/mod.rpc.php");

class DBXFerTableStatus extends RPCData {
	var $status;

	//php 7 public function __construct()
	//php 5 original function DBXFerTableStatus()
	function __construct() {
		$this->status = array ();
		foreach (MysqlReadArea("SHOW TABLE STATUS") as $row) {
			$i = array ();
			$i["id"] = $row["Name"];
			$i["size"] = $row["Data_length"];
			$i["rows"] = $row["Rows"];
			$i["date"] = strtotime($row["Update_time"]);
			$this->status[] = $i;
		}
	}

	function getTables() {
		$r = array ();
		foreach ($this->status as $t)
			$r[] = $t["id"];
		return $r;
	}

	function removeEmpty() {
		foreach ($this->status as $k => $v)
			if ($v["size"] == 0)
				unset ($this->status[$k]);
	}
}

class DBXFerDBInfo extends RPCData {
	var $tables;
	//php 7 public function __construct()
	//php 5 original function DBXFerDBInfo()
	function __construct($names = array ()) {
		$this->_loadTables($names);
	}

	function _loadTables($names) {
		$exclude = explode(",", EXCLUDE_TABLES);
		foreach ($names as $table) {
			if (!in_array($table, $exclude))
				$this->tables[$table] = new DBXFerTableInfo($table);
		}
	}

	function getComparison($compare) {
		$r = array ();
		foreach ($this->tables as $tname => $table)
			$r[$tname] = $table->getComparison($compare);
		return $r;
	}

	function getCreateTables() {
		$r = array ();
		foreach ($this->tables as $t)
			$r[$t->tableName] = $t->createTable;
		return $r;
	}
}

class DBXFerTableInfo {
	var $tableName;
	var $createTable;
	var $blocks;

	//php 7 public function __construct()
	//php 5 original function DBXFerTableInfo()
	function __construct($aTableName) {
		$this->tableName = $aTableName;
		$this->_readCreateTable();
		$this->_generateBlocks();
	}

	function _readCreateTable() {
		$this->createTable = MysqlReadField("SHOW CREATE TABLE {$this->tableName};", "Create Table");
	}

	function _getSqlSize() {
		$const = $a = 0;
		$cols = array ();
		foreach (MysqlReadCol("SHOW COLUMNS FROM {$this->tableName};", "Type", "Field") as $name => $type) {
			if (strpos($type, "enum(") !== false)
				$const += 12;
			elseif (preg_match("/^[\w_\-]+\((\d*)\)$/", $type, $a)) $const += $a[1];
			else
				$cols[] = "LENGTH(`$name`)";
		}
		if (empty ($cols))
			return $const;
		return implode("+", $cols)."+".$const;
	}

	function _getRowSizes() {
		return MysqlReadCol("SELECT ".$this->_getSqlSize()." AS size, id FROM {$this->tableName} ORDER BY id;", "size", "id");
	}

	function _generateBlocks() {
		$lastid = -0xFFFFFFFF;
		$size = $thisid = 0;
		foreach ($this->_getRowSizes() as $id => $s) {
			$thisid = $id;
			$size += $s;
			if ($size > MAX_BLOCK_SIZE) {
				$this->blocks["$lastid-$id"] = new DBXFerBlockInfo($this->tableName, $lastid, $id, $size);
				$lastid = $id +1;
				$size = 0;
			}
		}
		$this->blocks["$lastid-$thisid"] = new DBXFerBlockInfo($this->tableName, $lastid, 0xFFFFFFFF, $size);
	}

	function _tableExists() {
		$n = MysqlReadField("SHOW TABLE STATUS LIKE '{$this->tableName}';", "Name", true);
		return (empty ($n)) ? false : true;
	}

	function getComparison($compare) {
		$b = array ();
		$size = 0;
		if (!$this->_tableExists())
			$compare = false;
		foreach ($this->blocks as $bname => $block) {
			$b[$bname] = $block->getComparison($this->tableName, $compare);
			if (!$b[$bname]["equal"])
				$size += $block->size;
		}
		return array ("name" => $this->tableName, "blocks" => $b, "size" => $size);
	}
}

class DBXFerBlockInfo {
	var $firstID; // Die ID des ersten Datensatzes, der zu diesem Block geh&ouml;rt.
	var $lastID; // Die ID des letzten Datensatzes, der zu diesem Block geh&ouml;rt.
	var $checksum;
	var $size;
	var $count;

	//php 7 public function __construct()
	//php 5 original function DBXFerBlockInfo()
	function __construct($tableName, $aFirstID, $aLastID, $aSize) {
		$this->firstID = $aFirstID;
		$this->lastID = $aLastID;
		list ($this->checksum, $this->count) = $this->_readDB($tableName);
		$this->size = $aSize;
	}

	function _readDB($tableName) {
		$r = MysqlReadRow("SELECT UNIX_TIMESTAMP(MAX(mtime)) AS s, COUNT(id) AS c FROM $tableName WHERE (id >= {$this->firstID} AND id <= {$this->lastID});");
		return array ($r["s"], $r["c"]);
	}

	function getComparison($tablename, $compare) {
		$b = array ();
		$b["size"] = $this->size;
		$b["firstid"] = $this->firstID;
		$b["lastid"] = $this->lastID;
		$b["count"] = $this->count;
		if ($compare)
			$b["equal"] = $this->_dbIsUpToDate($tablename);
		else
			$b["equal"] = false;
		return $b;
	}

	function _dbIsUpToDate($tablename) {
		list ($check, $count) = $this->_readDB($tablename);
		return (($check == $this->checksum) and ($count == $this->count)) ? true : false;
	}

}

class DBXFerBlockData extends RPCData {
	var $rows;

	//php 7 public function __construct()
	//php 5 original function DBXFerBlockData()
	function __construct($tableName, $firstID, $lastID) {
		$f = (int) $firstID;
		$l = (int) $lastID;
		$t = addslashes($tableName);
		$this->rows = MysqlReadArea("SELECT * FROM $t WHERE ($f <= id AND id <= $l);");
	}

	function execute($tableName, $firstID, $lastID) {
		$f = (int) $firstID;
		$l = (int) $lastID;
		$t = addslashes($tableName);
		MysqlWrite("DELETE FROM $t WHERE ($f <= id AND id <= $l);");
		foreach ($this->rows as $r)
			MysqlWriteByID($tableName, $r, 0, "", true);
	}
}

/**
 * Regelt das Ausf&uuml;hren der Create-Table-Statements.
 * 
 * Beim Erstellen l&auml;dt das Objekt eine Liste der Zeitpunkte, zu denen die vorhandenen 
 * Tabellen in der Datenbank erstellt wurden. Beim Aufruf von checkCreateTable() &uuml;berpr&uuml;ft es,
 * ob sich der timestamp f&uuml;r die Tabelle ver&auml;ndert hat: Wenn ja, wird das Create-Table-Statement
 * ausgef&uuml;hrt. So ist sicher gestellt, dass selbst wenn der Datenbankinhalt in einzelnen Blocks kommt,
 * wird das Create-Table-Statement nur vor dem ersten Block ausgef&uuml;hrt wird und sonst nicht.
 */
class CreateTableWatcher {
	var $createTables;
	var $createTimes;

	//php 7 public function __construct()
	//php 5 original function CreateTableWatcher()
	function __construct($dbinfo) {
		$this->createTable = $dbinfo->getCreateTables();
		$this->createTimes = MysqlReadCol("SHOW TABLE STATUS;", "Create_time", "Name");
	}

	function checkCreateTable($tableName) {
		$n = addslashes($tableName);
		$time = MysqlReadField("SHOW TABLE STATUS LIKE '$tableName';", "Create_time", true);
		if (($time == $this->createTimes[$tableName]) or empty ($time)) {
			MysqlWrite("DROP TABLE IF EXISTS `$n`");
			MysqlWrite($this->createTable[$tableName]);
		}
	}

	function tableExists($tableName) {
		return (empty ($this->createTimes[$tableName])) ? false : true;
	}
}

function DBXFerLoadUserdata() {
	global $User;
	$dat = $User->getProperty("dbxfer_userdata");
	if (empty ($dat))
		return array ();
	return unserialize(gzuncompress($dat));
}

function DBXFerSaveUserdata($dat) {
	global $User;
	$dat = gzcompress(serialize($dat));
	return $User->setProperty("dbxfer_userdata", $dat);
}

function DBXFerSetUserProperty($key, $val) {
	$dat = DBXFerLoadUserdata();
	$dat[$key] = $val;
	DBXFerSaveUserdata($dat);
}
?>