<?php
/**
 * @author Daniel Raap
 * @version $Id: mod.subject.turnierteam.php 1702 2019-01-09 09:01:12Z loom $ edit by VulkanLAN
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** FLIP-Kern */
require_once ("core/core.php");

class Turnierteam extends ParentSubject {
	
	//php 7 public function __construct()
	//php 5 original function Turnierteam($Ident)
	function __construct($Ident) {
		//php 5:
		//parent :: ParentSubject($Ident);
		//php7 neu:		
		parent::__construct($Ident);
		$this->name = $this->getProperty("teamname");
	}

	function CountTeam() {
		return $this->getChildsCount();
	}

	function requireAllowCreate() {
		// TODO: rechte pr&uuml;fen
	}

	/**
	 * Liefert ein assoziatives Array der Teammitglieder mit Nickname und UserID
	 * @return Array array(UID => array("userid"=>UID, "nickname" =><nickname>))
	 */
	function members() {
		$childs = $this->getChilds('user');
		$members = array();
		foreach($childs AS $id => $name) {
			$members[$id] = array("userid" => $id, "nickname" => $name);
		}
		return $members;
	}

	function delete() {
			if (MysqlDeleteRow("DELETE FROM ".TblPrefix()."flip_tournament_combatant WHERE team_id='".$this->id."'") &&
				MysqlWrite("UPDATE ".TblPrefix()."flip_tournament_matches SET team1='0' WHERE team1='".$this->id."'") &&
				MysqlWrite("UPDATE ".TblPrefix()."flip_tournament_matches SET team2='0' WHERE team2='".$this->id."'") &&
				MysqlWrite("DELETE FROM ".TblPrefix()."flip_tournament_ranking WHERE combatant_id='".$this->id."'")) {
				return true;
			} else {
				return false;
			}
	}
	
	function setProperties($Prop) {
		if(isset($Prop['teamname'])) {
			if(!TurnierteamValidTeamname($Prop['teamname'])) {
				return trigger_error('Daten konnten nicht gespeichert werden!', E_USER_ERROR);
			}
		}
		return parent :: setProperties($Prop);
	}
}


/**
 * Pr&uuml;ft, ob der Name verboten ist
 * (mit regul&auml;ren Ausdr&uuml;cken!)
 * @param String $name Teamname
 * @return bool Ist der Name nicht verboten wird true zur&uuml;ckgegeben, sonst false
 */
function TurnierteamIsBadName($name) {
	static $badnames;
	if (!isset ($badnames)) {
		include_once('mod/mod.tournament.php');
		$tablename_badnames = TournamentTablename_Badnames();
		$badnames = MysqlReadCol('SELECT e.value FROM `'.TblPrefix().'flip_table_entry` e LEFT JOIN `'.TblPrefix().'flip_table_tables` t ON e.table_id=t.id WHERE t.name=\''.escape_sqlData_without_quotes($tablename_badnames).'\'', 'value');
	}
	foreach ($badnames AS $badname) {
		if (preg_match("/$badname/i", $name))
			return true;
	}
	return false;
}

/**
 * Pr&uuml;ft, ob ein Teamname g&uuml;ltig ist und gibt eine entsprechende Fehlermeldung aus
 * (nicht verboten oder bereits vorhanden)
 * @param String $aTeamname Name des Teams
 * @return bool Name erlaubt oder nicht? Bei false wird ein Fehler ausgegeben
 */
function TurnierteamValidTeamname($aTeamname) {
	if (empty ($aTeamname)) {
		trigger_error_text('Es muss ein Teamname eingegeben werden.', E_USER_WARNING, __FILE__, __LINE__);
		return false;
	}
	if (TurnierteamIsBadName($aTeamname)) {
		trigger_error_text("Der Name '$aTeamname' ist nicht erlaubt!", E_USER_WARNING, __FILE__, __LINE__);
		return false;
	}
	return true;
}

?>
