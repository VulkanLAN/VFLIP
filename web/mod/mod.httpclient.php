<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: mod.netlog.protocol.php 1703 2019-05-10 13:34:17Z docfx $ edit by naaux
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/**
 * Erstellt ein HTTP-(GET-)Request
 */
function HTTPBuildGetRequest($Host, $File, $Cookie="disable-flip-cookie=1")
{   
  $r  = "GET $File HTTP/1.0\r\n";
  $r .= "Host: $Host\r\n";
  $r .= "User-Agent: Mozilla/4.0 FLIP/HTTP-Client_0.2\r\n";
  $r .= "Cache-Control: no-cache\r\n";
  $r .= "Accept: *"."/*\r\n";
  if(!is_null($Cookie)) $r .= "Cookie: $Cookie\r\n";
  $r .= "\r\n";
  return $r;
}

function HTTPBuildPostRequest($Host,$File,$Data, $Cookie="")
{   
  if(!is_array($Data)) return trigger_error_text("Keine Daten fuer POST angegeben!|host=$Host, file=$File", E_USER_ERROR);
  $data_urlencoded = array();
  foreach($Data AS $key => $value)
    $data_urlencoded[] = urlencode($key) ."=". urlencode($value);
  $data_urlencoded = implode("&", $data_urlencoded);

  $r  = "POST $File HTTP/1.0\r\n";
  $r .= "Host: $Host\r\n";
  $r .= "User-Agent: Mozilla/4.0 FLIP/HTTP-Client_0.2\r\n";
  if($Cookie != "") $r .= "Cookie: $Cookie\r\n";
  $r .= "Content-Type: application/x-www-form-urlencoded\r\n";
  $r .= "Content-Length: ". strlen($data_urlencoded)."\r\n";
  $r .= "\r\n";
  $r .= "$data_urlencoded";
  return $r;
}

function HTTPParseResponse($Data) {
	list($head,$body) = HTTPParseFullResponse($Data);
	return $body;
}


function HTTPParseFullResponse($Data)
{
	// midestens zwei \n oder vier [\r\n]
  list($head,$body) = preg_split('/(\\r\\n){2,}|\\n{2,}/mi',$Data,2);
  return array($head, $body);
}

function HTTPRequestByGet($URL)
{
  $url_elements = HTTP_getURLData($URL);
  $request = HTTPBuildGetRequest($url_elements["host"], $url_elements["dir"]);
  $response = SocketPutGetContents($url_elements["adr"], $request, $url_elements["port"], 10);
  return HTTPParseResponse($response);
}

function HTTPRequestByPost($URL, $postdata)
{
  $url_elements = HTTP_getURLData($URL);
  $request = HTTPBuildPostRequest($url_elements["host"], $url_elements["dir"], $postdata);
  $response = SocketPutGetContents($url_elements["adr"], $request, $url_elements["port"], 10);
  return HTTPParseResponse($response);
}

function HTTP_getURLData($URL) {
  $u = parse_url($URL);
  $u["adr"] = (empty($u["port"])) ? $u["host"] : "$u[host]:$u[port]";
  if(empty($u["port"])) $u["port"] = 80; 
  $u["dir"] = (empty($u["query"])) ? $u["path"] : "$u[path]?$u[query]";
  return $u;
}

function SocketPutGetContents($Address,$Data,$DefaultPort = 80,$Timeout = 30)
{
  list($host,$port) = array_pad(explode(":",$Address,2),2,$DefaultPort);
  $errno = $errstr = 0;
  $f = fsockopen($host,$port,$errno,$errstr,$Timeout);
  if(!$f) 
  {
    trigger_error_text("Eine Socketverbindung konnte nicht hergestellt werden.|Host:$host:$port Err: $errno:$errstr",E_USER_WARNING);
    return false;
  }
  if(!fwrite($f,$Data))
  {
    fclose($f);
    trigger_error_text("In eine Socketverbindung konnte nicht geschrieben werden.|Host:$host:$port Err: $errno:$errstr",E_USER_ERROR);
    return false;
  }
  $r = "";
  while($line = fgets($f)) {
  	$r .= $line;
  }
  fclose($f);
  
  return $r;
}

class HTTPCookieSession {
	var $cookie_name;
	var $cookie_value = null;
  
  /**
	 * Konstruktor
	 */
	 //php 7 public function __construct()
	//php 5 original function HTTPCookieSession($cookie_name)
	function __construct($cookie_name) {
		$this->cookie_name = $cookie_name;
	}
	
	function Request($URL, $data=null) {
		return HTTPParseResponse($this->_Request($URL, $data)); 
	}
	
	function _Request($URL, $postdata=null) {
	  $url_elements = HTTP_getURLData($URL);
	  $cookie = $this->getURLCookie();
	  if(is_null($postdata))
	  	$request = HTTPBuildGetRequest($url_elements["host"], $url_elements["dir"], $cookie);
	  else
	  	$request = HTTPBuildPostRequest($url_elements["host"], $url_elements["dir"], $postdata, $cookie);
	  $r = SocketPutGetContents($url_elements["adr"], $request, 80, 5);
	  return $r;
	}
	
	function RequestSession($URL, $data=null) {
	  return $this->_setCookie($this->_Request($URL, $data));
	}

	function getURLCookie() {
	  if(!is_null($this->cookie_value))
	  	return $this->cookie_name."=".$this->cookie_value;
	  return null;
	}
	
	function _setCookie($response) {
	  list($head, $body) = HTTPParseFullResponse($response);
	  //check if we got a cookie
	  if(!($setcookie_pos = strpos($head, "Set-Cookie: "))) return false;
	  
	  $cookieline = substr($head, $setcookie_pos+12, 250); #12=strlen("Set-Cookie: "), cut after 250 characters
	  
	  //extract value
	  $cvalue_start = strpos($cookieline, $this->cookie_name) + strlen($this->cookie_name) + 1; # after "cookie_name="
	  $cvalue_length = strpos($cookieline, ";") - $cvalue_start;
	  $this->cookie_value = substr($cookieline, $cvalue_start, $cvalue_length);
	  
	  return $body;
	}
}  
?>
