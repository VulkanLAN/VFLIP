<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: mod.user.php 1702 2019-01-09 09:01:12Z loom $ edit by naaux
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** FLIP-Kern */
require_once ("core/core.php");

function UserChangeEmail(& $aUser, $Email) {
	if (empty ($Email)) {
		trigger_error_text("Es wurde keine Email-Adresse angegeben. Eine leere Email-Adresse kann nicht gespeichert werden.", E_USER_WARNING);
		return false;
	}

	$u = CreateSubjectInstance($aUser, "user");

	$old = $u->getProperties(array ("email", "enabled", "random_id"));
	if ($old["email"] == $Email)
		return true;

	$u->setRandomID();
	if (!$u->setProperty("email", $Email)) {
		trigger_error_text("Die Email-Adresse konnte nicht gespeichert werden. m&ouml;glicherweise ist bereits ein User mit dieser Adresse registriert.", E_USER_WARNING);
		$u->setProperties($old);
		return false;
	}

	if (ConfigGet("sendmessage_disable_email") == "N") {
		include_once ("mod/mod.sendmessage.php");
		if (SendSysMessage("user_account_reactivate", $u)) {
			trigger_error_text("Dein Account wird deaktiviert bis deine neue Email-Adresse &uuml;berpr&uuml;ft wurde. Du bekommst per Mail eine Aktivierungs-URL zugeschickt mit der du ihn wieder Aktivieren kannst.");
			$u->setProperty("enabled", "N");
			return true;
		}
	} else {
		trigger_error_text("Das versenden von Emails wurde deaktiviert.", E_USER_WARNING);
	}
	trigger_error_text("Da an deine Email-Adresse keine Mail versendet werden konnte wird deine Email-Adesse nicht gespeichert.", E_USER_WARNING);
	$u->setProperties($old);
	return false;
}

function UserGetStatus($aUser) {
	$u = CreateSubjectInstance($aUser, "user");
	$a = array();
	foreach ($u->getParents() as $v)
		if (preg_match("/^status_(.*)$/i", $v, $a))
			return $a[1];
	return false;
}

function UserGetStatusName($aUser, $aStatus = NULL) {
	if (is_null($aStatus)) {
		$u = CreateSubjectInstance($aUser, "user");
		$aStatus = UserGetStatus($u);
	}

	switch ($aStatus) {
		case ("registered") :
			return "angemeldet";
		case ("registered_console") :
			return "angemeldet(consolen Spieler)";
		case ("paid") :
			return "bezahlt";
		case ("paid_clan") :
			return "bezahlt(clan)";
		case ("checked_in") :
			return "eingecheckt";
		case ("checked_out") :
			return "ausgecheckt";
		case ("online") :
			return "online";
		case ("offline") :
			return "offline";
		default :
			if (!isset ($u))
				$u = CreateSubjectInstance($aUser, "user");
			if ($u->isChildOf("orga"))
				return "orga";
			elseif ($u->isChildOf("helper")) return "helfer";
			elseif (empty ($aStatus)) return "unregistriert";
			else
				return $aStatus;
	}
}

function UserValidSetStatus($aStatus) {
	static $stats;
	if (!isset ($stats)) {
		$stats = MysqlReadCol("SELECT name FROM ".TblPrefix()."flip_user_subject WHERE type='group' AND name LIKE 'status_%'", "name");
	}
	if (in_array("status_$aStatus", $stats))
		return true;
	else
		return false;
}

function UserSetStatus(& $aUser, $aStatus) {
	if (!UserValidSetStatus($aStatus)) {
		trigger_error_text("Ein Status kann nicht gesetzt werden, da die Statusgruppe nicht existiert.|Status: $aStatus", E_USER_WARNING);
		return false;
	}

	$u = & CreateSubjectInstance($aUser, "user");
	foreach ($u->getParents() as $v)
		if (preg_match("/^status_.*/i", $v))
			$u->remParent($v);

	include_once ("mod/mod.seats.php");
	if (empty ($aStatus)) {
		//reset (no status)
		SeatFreeSeatByUser($aUser);
		return true;
	}
	$stat = GetValidSeatStatus($aStatus);
	if (!empty ($stat))
		SeatsSetUserStatus($u, $stat);
	if ($aStatus == "registered")
    SeatFreeSeatByUser($aUser);

  include_once("mod/mod.paidlog.php");
  if($aStatus == "paid") {
    global $User;
    LogPaid($u->id, $User->id);   
  }elseif($aStatus == "registered") {
    UnLogPaid($u->id);
  }
	return $u->addParent("status_$aStatus");
}

function UserGetAge($aUser) {
	$u = CreateSubjectInstance($aUser, "user");
	$birthday = $u->getProperty("birthday");
	if (empty ($birthday))
		return "";
	$now = time();
	$years = date("Y", $now) - date("Y", $birthday);
	if (date("z", $now) < date("z", $birthday)) //day of the year
		return $years -1;
	return $years;
}

function UserGetIP($aUser) {
	switch (ConfigGet("user_iptype")) {
		case "fix" :
			if (is_a($aUser, "user"))
				$u = $aUser;
			else
				$u = CreateSubjectInstance($aUser);
			return $u->GetProperty("ip_fix");
		case "id" :
			return GetIPByID($aUser);
		case "seat" :
		default :
			include_once ("mod/mod.seats.php");
			return GetUserIPBySeat($aUser);
	}
}

//Beschr&auml;nkung:
//UserIDs: 1 - 9999
//UserIPs:  10.10.0.0/16   - 10.10.99.0/16  je 100-199
//AdminIDs: 1 - 25499
//AdminIPs: 10.10.100.0/16 - 10.10.254.0/16 je 100-199
function GetIPByID($aUser) {
	$u = CreateSubjectInstance($aUser);
	$c = floor($u->id / 100);
	if ($u->hasRight("admin"))
		$c += 100;
	$d = 100 + ($u->id % 100);
	return "10.10.$c.$d";
}

function UserGetUserIDByIP($IP) {
	switch (ConfigGet("user_iptype")) {
		case "id" :
			$ip = explode($IP);
			$c = $ip[2];
			$d = $ip[3];
			if ($c > 100)
				$c -= 100;
			$id = $c * 100 + ($d -100);
			return $id;
		case "seat" :
		default :
			// bis jetzt nur f&uuml;r sitzPl&auml;tze
			include_once ("mod/mod.seats.php");
			return SeatGetUserIDByIP($IP);
	}
}

function UserIsAdultBySeat($aUser) {
	$adultseatnameparts = array ("Block 1", "Block 2", "Block 3");

	include_once ("mod/mod.seats.php");
	$seat = SeatGetSeatByUser($aUser);
	foreach ($adultseatnameparts AS $part)
		if (strstr($seat, $part))
			return "Y";
	return "N";
}

function UserRefreshRights() {
	include_once ("inc/inc.menu.php");
	global $User;
	unset ($User->_InheritedIDs);
	unset ($User->_Rights);
	MenuFlushCache();
	trigger_error_text("Deine Rechte, Gruppenzugeh&ouml;rigkeit oder dein Lanpartystatus hat sich ge&auml;ndert.");
}

function UserGetLansurfer($username, $password) {
	require("mod/mod.httpclient.php");
	$lansurfer_url  = "http://lansurfer.com/";
	$userdata_file	= "user/edit.phtml";
	$logout_file    = "user.phtml";
	$logout_parameter    = "action=logout";
	$cookie_name    = "LS_Session";
	
	# Formdata
	$login_data = array("username"=>$username, "password"=>$password, "submit"=>"Login");
	
	//GET session
	$ls_session = new HTTPCookieSession($cookie_name);
	$userdata_url = $lansurfer_url.$userdata_file. ((strpos($userdata_file, '?'))? '&' :'?') .$ls_session->getURLCookie();
	$ls_session->RequestSession($userdata_url);
	//GET loginpage
	$ls_session->Request($userdata_url);
	//POST login
	$user_page = $ls_session->Request($userdata_url, $login_data);
	//GET logout
	$ls_session->Request($lansurfer_url.$logout_file. ((strpos($logout_file, '?'))? '&' :'?') .$ls_session->getURLCookie());
	
	// HTML-Daten auswerten
	$lansurfer_data = array();
	if(strpos($user_page, "failed login")) {
		trigger_error_text("Die angegebenen Lansurferdaten sind fehlerhaft.|username=$username", E_USER_WARNING);
	} else {
		preg_match_all("/<input ([^>]+)>/", $user_page, $inputs);
		foreach($inputs[1] AS $inputstring) { #[1] = erste Klammer im RegEx
			$elements = explode("\"", $inputstring);
			/* $elements = array(7) {
			 *  [0]=> string(5)  "type="
			 *  [1]=> string(6)  "hidden"
			 *  [2]=> string(6)  " name="
			 *  [3]=> string(8)  "submited"
			 *  [4]=> string(7)  " value="
			 *  [5]=> string(1)  "1"
			 *  [6]=> string(0)  ""
			 * }
			 */
			for($i=0;$i<count($elements);$i+=2) {
				$input_name = trim(substr($elements[$i], 0, strlen($elements[$i])-1)); #letztes Zeichen (=) abschneiden und Whitespace entfernen
				switch($input_name) {
					case "name":
						$name = $elements[$i+1];
						break;
					case "value":
						$value = $elements[$i+1];
						break;
				}
			}
			$lansurfer_data[$name] = $value;
		}
	}
	//Lansurfer => FLIP
	$lf2flip = UserLS2FLIPArray();
	$flip_data = array();
	
	foreach($lansurfer_data AS $key=>$value) {
		if(isset($lf2flip[$key]) && $value != "")
			$flip_data[$lf2flip[$key]] = $value;
	}
	
	return $flip_data;
}

function UserLS2FLIPArray() {
	//Lansurfer => FLIP
	return array(
				"f_nick"		=> "name",
				"f_email"		=> "email",
				"f_name1"		=> "givenname",
				"f_name2"		=> "familyname",
				"f_clan"		=> "clan",
				"f_wwclid"		=> "tournament_wwcl_player_id",
				"f_wwclclanid"	=> "tournament_wwcl_clan_id",
				"f_zipcode"		=> "plz",
				"f_town"		=> "city",
				"f_homepage"	=> "homepage"
				#"f_birthyear"	=> "birthday" //TODO: Jahr muss in Datum umgewandelt werden
				);
}

?>
