<?php
/**
 * allgemeine Funktionen rund um die Config
 * 
 * @author Daniel Raap
 * @version $Id$
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** FLIP-Kern */
require_once ("core/core.php");

/**
 * Recht um die Config zu bearbeiten
 * @return String Name des Rechtes um die Konfiguration zu bearbeiten
 */
function ConfigEditRight() {
	return "config_edit";
}

/**
 * Gibt an ob ein User die Konfiguration bearbeiten darf
 * @param variant $aUser Benutzer der gepr&uuml;ft werden soll (alles was CreateSubjectInstance verwertet (z.B. ID, Objekt))
 * @return bool True wenn der Benutzer das Recht zum editieren besitzt
 */
function ConfigCanEdit($aUser = false) {
	if(!$aUser) {
		global $User;
		$aUser = $User;
	}
	$u = CreateSubjectInstance($aUser);
	
	return $aUser->hasRight(ConfigEditRight());
}

?>
