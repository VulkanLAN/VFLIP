<?php
/**
 * Suche
 * Sucht mittels Modulen, welche mod.search.MODUL.php heissen
 * in Suchmodul muss Klasse Search_MODUL vorhanden sein mit den Methoden Search() und SearchTitle()
 * diese Methoden liefern ein Array mit jeweils folgendne Elementen zur&uuml;ck: Titel, Link, (Teil von) Text
 * der Text wird vom jeweiligen modul entsprechend formatiert und angepasst (z.B. hervorhebungen, HTML-escaping)
 * @author Daniel Raap
 * @version $Id$ 1702 2019-01-09 09:01:12Z
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** FLIP-Kern */
require_once ("core/core.php");

class Search
{
  var $modes = array("default"  => "wie angegeben",
                     "AND"      => "UND",
                     "OR"       => "ODER",
                     "begin"    => "beginnt mit",
                     "end"      => "endet mit"
                    );
  var $titleonly = true;
  var $modules = array();
  var $usedmodules = array();
  var $mode;
  
	//php 7 public function __construct()
	//php 5 original function Search($titleonly=true)
  function __construct($titleonly=true)
  {
    $this->titleonly = (bool) $titleonly;
    $this->modules = $this->GetModules();
  }
  
  /**
   * Gibt eine Liste aller vorhandenen Suchmodule zur&uuml;ck
   *
   * Dabei wird im Verzeichnis './mod' nach Dateien mit
   * dem Namen 'mod.search.MODUL.php' gesucht
   *
   * @return array Array mit jeweils Modulbezeichnung => Dateiname
   **/
  function GetModules()
  {
    $mods = array();
    $dh  = opendir("mod");
    while ($filename = readdir($dh))
    {
      if(stristr($filename, "mod.search."))
      {
        $mod = str_replace("mod.search.", "", strtolower($filename));
        if(!strpos($mod, ".")) continue;
        $mod = explode(".php", $mod);
        if($mod[1]=="")
          $mods[$mod[0]] = $filename; //mod is lowercase, filename may be case-sensitive
      }
    }
    ksort($mods);
    return $mods;
  }
  
  function SetModules($modarray)
  {
    $this->usedmodules = array();
    foreach($modarray AS $mod)
      if(isset($this->modules[$mod]))
      {
        require_once("mod/".$this->modules[$mod]);
        if(class_exists("Search_$mod"))
          $this->usedmodules[$mod] = "Search_$mod";
      }
  }
  
  function GetResult($searchstring, $mode="default")
  {
    $searchstring = addcslashes(escape_sqlData_without_quotes($searchstring), "%");
    $r = $searcharray = array();
    $seperator = "AND";
    switch($mode)
    {
      case "AND":
        $searcharray = explode(" ", $searchstring);
        foreach($searcharray AS $k=>$s)
          $searcharray[$k] = "%$s%";
        $seperator = "AND";
        break;
      case "OR":
        $searcharray = explode(" ", $searchstring);
        foreach($searcharray AS $k=>$s)
          $searcharray[$k] = "%$s%";
        $seperator = "OR";
        break;
      case "begin":
        $searcharray = array("$searchstring%");
        break;
      case "end":
        $searcharray = array("%$searchstring");
        break;
      case "default":
      default:
        $searcharray = array("%$searchstring%");
        break;
    }
    
    foreach($this->usedmodules AS $mod=>$class)
    {
      $class = new $class();
      if($this->titleonly)
        $r[$mod] = $class->SearchTitle($searcharray, $seperator);
      else
        $r[$mod] = $class->Search($searcharray, $seperator);
    }
    
    return $r;
  }
  
  function _format($text, $searchtexts)
  {
    //erste 40 Zeiche und Suchbegriff hervorheben
    $text = escapeHtml(substr($text, 0, 40));
    foreach($searchtexts AS $s)
    {
      $s = str_replace("%", "", $s); //Mysql-Jokerzeichen entfernen (entfernt auch escapte)
      escapeHtml($s);
      $text = str_replace($s, "<strong>$s</strong>", $text);
    }
    return $text;
  }
}

?>