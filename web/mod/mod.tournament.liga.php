<?php
/**
 * Description
 * @author Name
 * @version $Id$ 1702 2019-01-09 09:01:12Z tominator $
 * @copyright © 2001-2007 The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package 
 * @since xxxx 03.09.2007
 **/

/** Die Datei nur einmal includen */
if (defined("MOD.TOURNAMENT.LIGA.PHP"))
	return 0;
define("MOD.TOURNAMENT.LIGA.PHP", 1);

/** FLIP-Kern */
require_once("core/core.php");

class BasicLiga {
	/** Wert von `type` in der DB und Suffix der Klasse */
	var $dbAndClassName = null;
	/** Prefix der Spiele in der DB (zur Zuordnung Spiel->Liga) */
	var $dbPrefix = null;
		
	/**
	 * Liest die Zuweisung von Spielk&uuml;rzeln zu ausgeschriebene Spielnamen aus.
	 *
	 * Dieses stehen in der Tabelle 'TOURNAMENT_GAMES' in flip_table_tables.
	 * Die Spielk&uuml;rzel werden in der Datenbank verwendet; die ausgeschriebenen
	 * Spielnamen sind f&uuml;r die Ausgabe gedacht.
	 *
	 * @return array Enth&auml;lt die Zuweisung von Spielk&uuml;rzel zu ausgeschriebenen
	 * Spielnamen
	 * <code> Array (
	 *      [aom] => Age of Mythology
	 *      [bf] => Battlefield 1942
	 *      [bfdc] => Battlefield Desert Combat
	 *      [bfv] => Battlefield Vietnam
	 *      [bv] => Blobby Volley
	 *      [scbw] => BroodWar
	 *      [cod] => Call of Duty
	 *  )
	 * </code>
	 **/
	function getGameStringArray() {
		$prefix = strtolower($this->dbPrefix);
		
		//read db only once
		static $games = array ();
		if (empty ($games[$this->dbAndClassName])) {
			if (empty ($prefix)) {
				$notliga = array ();
				foreach (TournamentLigen() AS $liga => $caption)
					if ($liga != $this->dbAndClassName)
						$notliga[] = $liga . "_%";
				$prefix_where = '(e.key NOT LIKE ' . implode_sql(' AND e.key NOT LIKE ', $notliga) . ')';
			} else {
				$prefix_where = "e.key LIKE '$prefix" . "_%'";
			}
			$games[$this->dbAndClassName] = MysqlReadCol("SELECT e.key, e.value FROM `" . TblPrefix() . "flip_table_tables` t, `" . TblPrefix() . "flip_table_entry` e
			                         WHERE t.name='" . TournamentTablename_Games() . "'
			                           AND e.table_id=t.id
			                           AND $prefix_where
			                         ORDER BY value
			                        ", 'value', 'key');
		}
		return $games[$this->dbAndClassName];
	}
	
	/**
	 * Gibt den vollen Namen zu einem Spielk&uuml;rzel zur&uuml;ck
	 *
	 * @param string $game Spielk&uuml;rzel (max. 4 Zeichen)
	 * @return string ausgeschriebener Name des Spieles
	 **/
	function getGameString($game) {
		
		$game = strtolower($game);
		
		$gamearray = $this->getGameStringArray();
		if (isset ($gamearray[$game]))
			return $gamearray[$game];
		else
			return $game;
	}
	
	// Mod VulkanLAN - Tominator - Liefert zum langen Turniernamen den kurzen Namen
	function getGameStringKey($game) {
				
		$game = strtolower($game);
		$gamearray = $this->getGameStringArray();
		$GameStringKey = "";
		
		foreach($gamearray AS $key=>$val)
		{		
			if (strtolower($val) == $game)
			{			
				$GameStringKey = $key;
				break;
			}
		}		
		return $GameStringKey;
	}
	
	/**
	 * Aktualisiert die Spiele einer Liga in der Datenbank
	 * Dazu werden alle gel&ouml;scht und alle aus der Datei eingetragen
	 * Die Schl&uuml;ssel der Spiele haben den Prefix strtolower($liga)."_",
	 * z.B. wwcl_25
	 * 
	 * @since 1341 - 16.01.2007
	 * @param String $liga Liganame
	 */
	function updateGames() {
		$games = $this->readXMLFile();
		if(!empty($games)) {
			$tableid = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_table_tables WHERE name='".escapeHtml(TournamentTablename_Games())."'");
			//delete current entries
			$ids = MysqlReadCol("SELECT id FROM `".TblPrefix()."flip_table_entry` WHERE table_id='$tableid' AND `key` LIKE '".strtolower($this->dbPrefix)."_%'", "id");
			MysqlExecQuery("DELETE FROM `".TblPrefix()."flip_table_entry` WHERE id IN (".implode_sqlIn($ids).")");
			//insert games
			foreach($games AS $game)
				MysqlWriteByID(TblPrefix()."flip_table_entry", array("table_id"=>$tableid, "key"=>strtolower($this->dbPrefix)."_".$game["key"], "value"=>$game["title"], "display"=>$game["title"], "display_code"=>$game["title"]));
			return true;
		}
		return false;
	}
	
	/**
	 * Liest eine XML-Datei und liest relevante Daten in ein Array
	 * @see _recursiveReadObject()
	 * @access protected
	 */
	function _readTournamentXMLFile($filename, $recursive_tags, $value_tags) {
		$xmldata = $this->_readXMLFile($filename);
		return $this->_recursiveReadObject($xmldata, $recursive_tags, $value_tags);
	}
	
	/**
	 * Liest eine Verschachtelte Struktur in ein zweidimensionales Array mit
	 * angegebener zweiter Dimension ein
	 * 
	 * D.h. es werden alle Elemente aus $recursive_tags abgearbeitet und aus dem
	 * letzten werden die Werte von $value_tags ausgelesen. Dabei gilt f&uuml;r das
	 * Array $value_tags:
	 * array("key1"=>"tag1name", "key2"=>"tag2name" ...)
	 * 
	 * z.B. wird aus:
	 * $xmldata = "<xml>
	 *   <data>
	 *     <name>Name One</name>
	 *     <date>1970-01-01</date>
	 *   </data>
	 *   <data>
	 *     <name>Name Two</name>
	 *     <date>2000-01-01</date>
	 *   </data>
	 * </xml>";
	 * mittels
	 * _recursiveReadObject($xmldata, array("xml", "data"), array ("name"
	 * =>"name", "birthday"=>"date"));
	 * das array(
	 *   array ("name" => "Name One", "birthday"=>"1970-01-01"),
	 *   array  ("name") => "Name Two", "birthday"=>"2000-01-01"
	 * )
	 * 
	 * @since 1341 - 16.01.2007
	 * @access protected
	 * @param Object $object ein verschachteltes Objekt, z.B. von
	 * simplexml_load_string()
	 * @param Array $recursive_tags Name der Elemente welche traversiert werden
	 * sollen
	 * @param Array $value_tags assoziatives Array welches jedem Schl&uuml;ssel den
	 * Wert des Tags mit dem angegeben Wert hat (siehe oben)
	 * @return Array
	 */
	function _recursiveReadObject($object, $recursive_tags, $value_tags) {
		if(!is_array($recursive_tags) || !is_array($value_tags))
			return TournamentError('Keine Tags zum parsen des XML angegeben!|$recursive_tags: '.var_export($recursive_tags, true), E_USER_WARNING,__FILE__,__LINE__);
		$tag = array_shift($recursive_tags);
		$count = count($recursive_tags);
		$r = array();
		foreach($object->{$tag} AS $element) {
			if($count > 0) {
				$r = array_merge($r, $this->_recursiveReadObject($element, $recursive_tags, $value_tags));
			} else {
				foreach($value_tags AS $key=>$value_tag)
					//cast must be done for PHP5 which else returns an object
					$data_array[$key] =  (string) $element->{$value_tag};
				$r[] = $data_array;
			}
		}
		return $r;
	}
	
	/**
	 * Liest eine Datei ein und parst XML mittels simplexml
	 * 
	 * @since 1341 - 16.01.2007
	 * @access private
	 * @param String $filename Dateiname der einzulesenden XML-Datei
	 * @return Array
	 * @see simplexml_load_string()
	 */
	function _readXMLFile($filename) {
		$xml = file_get_contents($filename);
		if (empty($xml))
			return TournamentError("Schwerer Fehler: Die Datei \"".basename($filename)."\" konnte nicht ge&ouml;ffnet werden!|(Pfad: ".$filename.").", E_USER_ERROR, __FILE__, __LINE__);
		return simplexml_load_string($xml);
	}	
}

class TournamentLigaKeine extends BasicLiga {
	var $dbAndClassName = 'Keine';
	var $dbPrefix = '';
	
	function name() {
		return 'keine';
	}
}

class TournamentLigaNGL extends BasicLiga {
	var $dbAndClassName = 'NGL';
	var $dbPrefix = 'ngl';
	var $xmlfile = 'NGLGames.xml';
	
	function name() {
		return 'NGL (Netzstatt Gaming League)';
	}
	
	/**
	 * Liest die XML-Datei der NGL in ein Array
	 * 
	 * @since 1341 - 16.01.2007
	 * @return Array
	 */
	function readXMLFile() {
		return $this->_readTournamentXMLFile($this->xmlfile, array("league", "game"), array("key"=>"short", "players"=>"players", "title"=>"title"));
	}
}

class TournamentLigaWWCL extends BasicLiga {
	var $dbAndClassName = 'WWCL';
	var $dbPrefix = 'wwcl';
	var $xmlfile = 'gameini.xml';
	
	function name() {
		return 'WWCL (World Wide Championship of LAN-Gaming)';
	}
	
	/**
	 * Liest die XML-Datei der WWCL in ein Array
	 * 
	 * @since 1341 - 16.01.2007
	 * @return Array
	 */
	function readXMLFile() {
		return $this->_readTournamentXMLFile($this->xmlfile, array("game"), array("key"=>"id", "players"=>"player", "title"=>"name"));
	}
}
?>
