<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: mod.template.flipcompiler.php 1702 2019-01-09 09:01:12Z loom $ edit by VulkanLAN
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package tpl
 **/

/** Die Datei nur einmal includen */
if(defined("MOD.TEMPLATE.FLIPCOMPILER.PHP")) return 0;
define("MOD.TEMPLATE.FLIPCOMPILER.PHP",1);

require_once("mod/mod.template.php");
require_once("mod/mod.template.compiler.php");
require_once("mod/mod.parse.php");

function _JoinParam($Array,$NSIndex)
{
  $r = array();
  foreach($Array as $k => $v) $r[_getHtmlVar($k,$NSIndex)] = _getHtmlVar($v,$NSIndex);
  return JoinParam($r);
}

function _JoinHTMLParam($Array,$NSIndex,$Depth,$Defaults = array())
{
	if(empty($Array)) return _JoinParam($Defaults,$NSIndex);
	elseif(_isArray($Array)) return _JoinParam(_getArrayItems($Array) + $Defaults,$NSIndex);
	else
	{
	  $pk = "\$pk$Depth";
      $pv = "\$pv$Depth";
      $p = _getPhpVar($Array,$NSIndex);
      /*if(empty($Defaults)) $d = $p;
      else 
      {
        
      }$d = var_dump($Defaults,true)."+$p";*/
      $r = "<?php if(is_array($p)) { ";
      foreach($Defaults as $k => $v) 
      {
      	$kp = _getPhpVar($k,$NSIndex);
      	$ks = _getStrVar($k,$NSIndex);
      	$v = _getPhpVar($v,$NSIndex);
      	$r .= "if(!isset({$p}[$kp])) echo \" $ks=\\\"\".addslashes($v).\"\\\"\"; ";
      }
      $r .= " foreach($p as $pk => $pv) echo \" $pk=\\\"$pv\\\"\"; } ?>";
      return $r;
	}
}

/**
 * Erzeugt eine HTML-Formular
 *
 * <pre>
 * {#FORM submit=submit usesubmit=usesubmit keepparams=keepparams keepvals=keepvals confirmation=confirmation [key1=val1]}
 * {#END}
 * </pre>
 *
 * submit gibt den Namen der Submit-Methode an, die aufgerunfen wird, wenn das 
 * Formular an den Server gesendet wird.
 * 
 * usesubmit [0|1] gibt an, ob &uuml;berhaupt eine Submit-Methode aufgerufen werden soll. Wenn submit nicht gesetzt 
 * wurde oder das Formulat via GET submitted wird, ist usesubmit per default false (0).
 *
 * keepparams [0|1] gib an, ob die GET oder POST-Parameter, mit denen der Frame aufgerufen wurde, automatisch in hidden Inputs 
 * gespeichert werden sollen, damit sie beim Submit wieder mit an den Server gesendet werden. Diese Funktion ist
 * n&uuml;tzlich, wenn es auf einem Frame mehrere Filter oder Sortier-Formulare gibt.
 *
 * keepvals [1|0] gibt an, ob das Formular die vom User eingegebenen Werte beibehalten soll, wenn es mehrmals 
 * hintereinander aufgerufen wird.
 *
 * confirmation ist eine string, typischer Weise eine Frage die der User beim Submitten der Daten mit "Ja" beantworten muss,
 * damit die Action/Submit-Methode ausgef&uuml;hrt wird.
 *
 * Des weiteren kann der FORM-Befehl beliebige weitere Attribute haben, die einfach als HTML-Attribute an den <form ... >-Tag 
 * weiter geleitet werde.
 **/

class _Cmd_Form extends _CmdBlock
{
  var $_Name          = "";
  var $_Code          = array();
  var $_UseSubmit     = false;
  var $_KeepParams    = false;
  var $_KeepVals      = true;
  var $_Confirmation  = "";
  var $IsUpload       = false;
  var $Params         = array();
  
  function init($param)
  { 
    global $_isForm,$_isUploadForm,$_InputNames;
    include_once('inc/inc.form.php');
    
	$_isForm        = true;
	$_isUploadForm  = false;
    $_InputNames    = array();
    
    $p = ParseParam($param,array('submit','usesubmit','keepparams','keepvals','confirmation'),false);
    if(!is_array($p)) return $p;

    if(empty($p['method'])) $p['method'] = 'post';
    elseif(!in_array($p['method'],array('get','post'))) return 'Der Parameter "method" kann nur "get" oder "post" sein.';

    if(isset($p['usesubmit'])) $this->_UseSubmit = ($p['usesubmit']) ? true: false;
    elseif(empty($p['submit'])) $this->_UseSubmit = false;
    else $this->_UseSubmit = ($p['method'] == 'post') ? true : false;
    
    $this->_KeepParams = ($p['keepparams']) ? true : false;
    if(isset($p['keepvals'])) $this->_KeepVals = ($p['keepvals']) ? true : false;
    else $this->_KeepVals = true;
    
    $this->_Confirmation = is_null($p['confirmation']) ? null : text_translate($p['confirmation']);

    if($this->_UseSubmit)
      if(!_isVar($p['submit']) and !_isIdent($p['submit'])) return '"'.$p['submit'].'" ist weder eine g&uuml;ltige Variable noch ein g&uuml;ltiger Name';

	$this->_Name = $p['submit'];
    unset($p['submit']);
    unset($p['usesubmit']);
    unset($p['keepparams']);
    unset($p['keepvals']);
    unset($p['confirmation']);
    $this->Params = $p;
	return '';
  }
  
  function finish()
  {
    global $_isForm,$_isUploadForm,$_InputNames;
	$this->IsUpload = $_isUploadForm;
	$_isForm        = false;
	$_isUploadForm  = false;
    $_InputNames    = array();
  }

  function add($data)
  {
    $this->_Code[] = $data;
  }

  function compile($NSIndex,$Depth)
  {
    global $_isUploadForm;
    $p = $this->Params;
    if($this->IsUpload)
      $p['enctype'] = 'multipart/form-data';
    $ac = '';
    if(empty($p['action'])) 
    {
     /** $ac  = " action=\"<?php echo \$_SERVER['".(($p["method"] == "post") ? "REQUEST_URI" : "PHP_SELF")."'] ?>\"";*/
      $ac  = ' action="<?php echo '.(($p['method'] == 'post') ? 'escapeHtml(GetRequestURI())' : 'escapeHtml($_SERVER[\'PHP_SELF\'])').' ?>"';
      $backlink = '<?php echo(isset($_POST[\'backlink\'])) ? $_POST[\'backlink\'] : ((isset($_GET[\'backlink\'])) ? $_GET[\'backlink\'] : escapeHtml(GetHttpReferer())) ?>';
    }
    else $backlink = _getHtmlVar($p['action'],$NSIndex);

    $keepvals = ($this->_KeepVals) ? '' : 'array(),false';
    
	$r  = '<form accept-charset="utf-8" '._JoinParam($p,$NSIndex).$ac.'><div><?php include_once("inc/inc.form.php"); $form = new Form('.$keepvals.'); ?>';
	$r .= $this->_compileArray($this->_Code,$NSIndex,$Depth);
	
    if($this->_UseSubmit)
    {
      $val = '<?php echo "'._getStrVar($this->_Name,$NSIndex).'@".escapeHtml($form->getMetadata()) ?>';    
	  $r .= '<input type="hidden" name="submit" value="'.$val.'" />';
      $r .= '<input type="hidden" name="backlink" value="'.$backlink.'" />';
      $inputs = 'array_merge($form->getInputNames(),array("submit","backlink"))';
    }
    else $inputs = '$form->getInputNames()';
    if($this->_KeepParams)
    {
      $src = ($p['method'] == 'post') ? '$_POST' : '$_GET';
      $r .= '<?php foreach(array_diff(array_keys('.$src.'),'.$inputs.') as $k) { ?>';
      $r .= '<input type="hidden" name="<?php echo $k ?>" value="<?php echo escapeHtml(stripslashes('.$src.'[$k])) ?>" />';
      $r .= '<?php } ?>';
    }
    if($this->_Confirmation)
    {
      $conf_msg = '<?php echo base64_encode(' . _getPhpVar($this->_Confirmation, $NSIndex) . '); ?>';
      $r .= '<input type="hidden" name="confirmation" value="'.$conf_msg.'" />';
      if(!empty($p['action']))
      {
        $confuri = _getHtmlVar($p['action'],$NSIndex);
        $r .= '<input type="hidden" name="confirmation_uri" value="'.$confuri.'" />';      
      }    
    }
    $r .= '</div></form>';
    
	return $r;
  }
}

/**
 * Gibt einen HTML-Link aus, der auf die Seite verweist, von der aus ein Formular aufgerufen wurde.
 *
 * <pre>
 * {#BACKLINK caption}
 * </pre>
 *
 * caption ist der Titel des Links.
 * 
 * Der BACKLINK-Befehl kann nur innerhalb eines #FORM-Blocks verwendet werden.
 **/

class _Cmd_Backlink extends _Cmd
{
  var $_Caption = '';
  
  function init($param)
  {
    global $_isForm;
	if(!$_isForm) return text_translate('Ein Backlink darf sich nur innerhalb einer Form befinden.');
    $this->_Caption = text_translate($param);
    return '';
  }
  
  function compile($NSIndex,$Depth)
  {
    $caption = _getHtmlVar($this->_Caption,$NSIndex);
    return '<a href="<?php echo(isset($_POST[\'backlink\'])) ? $_POST[\'backlink\'] : ((isset($_GET[\'backlink\'])) ? $_GET[\'backlink\'] : escapeHtml(GetHttpReferer()) ) ?>" type="button" class="button btn btn-outline-dark"><i class="fas fa-arrow-left"></i> &nbsp; '.$caption.'</a>';
  }
}

/**
 * Erzeugt einen Submit-Button um ein Formular abzusenden.
 *
 * <pre>
 * {#SUBMIT caption=titel shortcut=s}
 * </pre>
 *
 * caption ist der Titel des Buttons.
 * 
 * shortcut ist ein einzelnes Zeichen, das das Absendenden des Formulesres mit Alt+ZEICHEN bewirkt. Default ist "s". 
 * Wenn "0" angegeben wird, hat der Submit keinen Shortcut.
 * 
 * Der SUBMIT-Befehl kann nur innerhalb eines #FORM-Blocks verwendet werden.
 **/

class _Cmd_Submit extends _Cmd
{
  var $_Caption  = '';
  var $_Shortcut = '';
  var $_Name = '';
  
  function init($param)
  {
    global $_isForm;
	if(!$_isForm) return text_translate('Ein Submit darf sich nur innerhalb einer Form befinden.');
    //$this->_Caption = $param;
    $p = ParseParam($param,array('caption','shortcut','name'));
    $this->_Caption = empty($p['caption']) ? text_translate('Speichern') : text_translate($p['caption']);
    $this->_Shortcut = (isset($p['shortcut'])) ? $p['shortcut'] : 's';
    if((ConfigGet('template_buttons_shortcut') == 'N')) $this->_Shortcut = null;
    $this->_Name = $p['name'];
    if(!empty($this->_Shortcut))
      if(!(strlen($this->_Shortcut))) return text_translate('Der Shortcut eines SUBMITs darf nur ein Zeichen sein.');    
    return '';    
  }
  
  function compile($NSIndex,$Depth)
  {
  	$name = '';
  	if(!empty($this->_Name)) {
  	  $name = _getPhpVar($this->_Name,$NSIndex);
  	  $pname = _getPhpVar($this->_Name,$NSIndex);
  	  $name = ' name="'.$name.'"<?php $form->allowValue('.$pname.') ?>';
  	}
    if(empty($this->_Shortcut)) {
    	$scapt = $skey = '';
    } else {
      $scapt = ' (Alt+'.strtoupper($this->_Shortcut).')';
      $skey = ' accesskey="'.strtolower($this->_Shortcut).'"';
    }
    return '<input type="submit" class="btn button btn-outline-dark" value="'._getHtmlVar($this->_Caption,$NSIndex).$scapt.'"'.$skey.$name.' />';
  }
}

/**
 * Similar to #SUBMIT, but allows the creation of more fancier sumbits
 *
 * <pre>
 * {#HTMLSUBMIT caption=code shortcut=s}
 * </pre>
 *
 * See the documentation of #SUBMIT for usage details
 **/

class _Cmd_Htmlsubmit extends _CmdBlock {
	
  var $_Shortcut = '';
  var $_Name = '';
  var $_Data = array();
  var $_ButtonStyle = '';
  
  function init($param)
  {
    global $_isForm;
	if(!$_isForm) return text_translate('Ein Submit darf sich nur innerhalb einer Form befinden.');
    //$this->_Caption = $param;
    $p = ParseParam($param,array('caption','shortcut','name', 'buttonstyle'));
    
    $this->_Shortcut = (isset($p['shortcut'])) ? $p['shortcut'] : 's';
    $this->_Name = $p['name'];
    $this->_ButtonStyle = $p['buttonstyle'];
    
    if(!empty($this->_Shortcut)) {
		if(!(strlen($this->_Shortcut))) return text_translate('Der Shortcut eines HTMLSUBMITs darf nur ein Zeichen sein.');
    }   
     
    return '';    
  }
  
  function add($data) {
  	$this->_Data[] = $data;
  }
  
  function compile($NSIndex,$Depth)
  {
  	$name = '';
  	if(!empty($this->_Name)) {
  	  $name = _getPhpVar($this->_Name,$NSIndex);
  	  $pname = _getPhpVar($this->_Name,$NSIndex);
  	  $name = ' name="'.$name.'"<?php $form->allowValue('.$pname.') ?>';
  	}
    if(empty($this->_Shortcut)) $scapt = $skey = '';
    else
    {
      $scapt = ' (Alt+'.strtoupper($this->_Shortcut).')';
      $skey = ' accesskey="'.strtolower($this->_Shortcut).'"';
    }
    
    $style = (strlen($this->_ButtonStyle) > 0) ? ('style="'. $this->_ButtonStyle .'"') : '';
    
    $code  = '<button type="submit" ' . $style . ' />';
    $code .= $this->_compileArray($this->_Data, $NSIndex, $Depth);
    $code .= '</button>';
    
    return $code;
  }
}


/**
 * Erzeugt ein Input-Element, in welches der User Daten eingeben kann.
 *
 * <pre>
 * {#INPUT name=name type=type val=val param=param caption=caption allowempty=allowempty enabled=enabled}
 * </pre>
 *
 * name ist der interne Name des Datenfeldes, der Name des GET oder POST-Parameters. &uuml;ber ihn kann der vom
 * User eingegebene Wert innerhalb der Submit-Methode oder Action-Methode zugreifen. ($val = $SubmitMethParam1[name])
 *
 * type bestimmt die Kombination aus Aussehen des Datenfeldes und Werten, die es annehmen darf. f&uuml;r Jeden Typ muss eine 
 * Klasse mit dem Prefix "_Input_" existieren, welche von der Klasse "_Input" erbt. Die mei&szlig;ten dieser Klassen
 * befinden sich in der Datei inc/inc.form.php. Wenn alle dortigen Klassen mit der Suchmaske "_Input_*" auflistet,
 * bekommt man einen guten &uuml;berblick, welche Typen es gibt.
 * Die wichtigsten sind: string, longstring, radio, yesno, checkbox, integer, decimal, dropdown, text, document,
 * documentwrap, phone, passwordedit, passwordquery, hidden, rights, name, domain, file, tabledropdown, userfromgroup,
 * date, subjects, content, dropdownedit.  
 * 
 * val gibt den Wert des Inputs an, des es inne hat, wenn das Formular geladen wird.
 *
 * param ist ein Wert, evtl. ein Array, welches eine Typenspezifische bedeutung hat. Beim Typen "dropdown" ist
 * es z.B. ein Key-Value Array, welches die Eintr&auml;ge der Box angibt.
 *
 * caption ist ein Titel der Inbutbox. Diser wird nicht direkt ausgegeben, sondern nur innerhalb von Fehlermeldungen verwendet, 
 * wenn der User Werte eingegeben hat, die f&uuml;r diesen Typn ung&uuml;ltig sind.
 *
 * allowempty [1,0] git an, ob der User einen Wert eingeben muss.
 *
 * enabled [1,0] gibt an, ob der User einen Wert eingeben kann. Wenn ein Input nicht enabled ist, befindet sich 
 * sein Wert auch nicht im Parameter der Submit-Methode.
 **/
 
class _Cmd_Input extends _Cmd
{
  var $_Param = array();
  var $_PList = array('name','type','val','param','caption','allowempty','enabled');
  
  function init($param)
  {    
	global $_isForm,$_isUploadForm,$_InputNames;
	if(!$_isForm) return text_translate('Ein Input darf sich nur innerhalb einer Form befinden.');
    $p = ParseParam($param,$this->_PList);
    if(!is_array($p)) return $p;
    
	$def = array('type' => 'string', 'allowempty' => '1', 'enabled' => '1');
	foreach($def as $k => $v) if(!isset($p[$k])) $p[$k] = $v;
    
    if(!_isVar($p['name']))
    {
	  if(!preg_match('/[\w\d_\[\]]+/i',$p['name'])) return text_translate('"?1?" ist kein g&uuml;ltiger Name f&uuml;r ein Input.', $p['name']);
      else $_InputNames[$p['name']] = 1;
    }
    if(!_isVar($p['type'])) if(!InputExists($p['type'])) return text_translate('Der InputType \'?1?\' ist ung&uuml;ltig.', $p['type']);
	if(in_array($p['type'],array('file','fileinline','image'))) $_isUploadForm = true;
	$this->_Param = $p;
    return "";
  }
  
  function compile($NSIndex,$Depth)
  {    
	$p = array();
	for($i = 0; $i < count($this->_PList); $i++)
	{
		$varVar = _getPHPVar($this->_Param[$this->_PList[$i]],$NSIndex);
		$p[] = $varVar;
	}
	
    return '<?php if($t = $form->getEdit('.implode(',',$p).')) $t->out();?>';
  }
}

/**
 * Gibt zu einer RightID den Namen aus und formatiert ihn.
 *
 * <pre>
 * {#RIGHT val=val type=type}
 * </pre>
 *
 * val ist die ID oder der Name eines Rechts. 
 *
 * type [view|edit] hat Einfluss auf die farbliche Formatierung.
 **/
 
class _Cmd_Right extends _Cmd
{
  var $_Right = '';
  var $_Type  = '';
  
  function init($param)
  {    
    $p = ParseParam($param,array('val','type'));
    if(!is_array($p)) return $p;
    $this->_Right = $p['val'];
    $this->_Type  = $p['type'];
    if(!_isVar($this->_Right)) return text_translate('Der Parameter "val" des RIGHT-Befehles ben&ouml;tigt eine Variable als Wert.');
    if(!empty($this->_Type)) if(!in_array($this->_Type,array('edit','view'))) return text_translate('Der Parameter "type" des RIGHT-Befehles darf, wenn angegeben, nur die Werte "edit" oder "view" haben.');
    return '';    
  }
  
  function compile($NSIndex,$Depth)
  {
    $r = _getVar($this->_Right,$NSIndex);
    $r = '<?php echo (empty('.$r.')) ? \'\' : GetRightName('.$r.') ?>';
    if(empty($this->_Right)) return $r;
    switch($this->_Type)
    {
      case('view'): return '<span style="color:#227722;">'.$r.'</span>'; 
      case('edit'): return '<span style="color:#772222;">'.$r.'</span>';      
    }    
  }
}

/**
 * Generiert einen Button inklusive Form, welcher beim Clicken eine Action ausf&uuml;hrt.
 * 
 * <pre>
 * {#ACTION caption=caption action=action uri=uri params=paramarray right=right condition=phpexpression else=else confirmation=conf formattrs=array buttonattrs=array}
 * </pre>
 * 
 * caption ist der Titel des Buttons.
 * 
 * action ist der Name der Action welche ausgef&uuml;hrt wird.
 * 
 * uri ist die URI der Seite, welche vom Button aufgerufen wird.
 * 
 * params ist ein key-value-array, welches der Action als Parameter &uuml;bergeben wird. (Sie werden als POST-Variablen gespeichert)
 * 
 * right ist ein recht, welches der Betrachter haben muss, um den Button sehen zu d&uuml;rfen.
 * 
 * condition ist eine PHP-Expression, welche erf&uuml;llt sein muss, damit der Button angezeigt wird.
 * 
 * else ist ein String, welcher ausgegeben wird, wenn der Button nicht angezeigt wird.
 * 
 * confirmation ist ein String welcher eine Frage enth&auml;lt, die der User mit Ja beantworten muss, um die Action aus zu f&uuml;hren.
 * 
 * formattrs ist ein key-value-array, dessen Keys weitere Attribute der HTML-Form sind.
 * 
 * buttonattrs ist ein key-value-array, dessen Keys weitere Attribute des HTML-Input-Tags (welches den Typ submit hat) sind.
 * 
 * shortcut ist ein einzelnes Zeichen, das das Absendenden des Formulesres mit Alt+ZEICHEN bewirkt.
 */
class _Cmd_Action extends _Cmd
{
  var $_Caption = '';
  var $_Action = '';
  var $_Uri = '';
  var $_Params = '';
  var $_Right = '';
  var $_Condition = '';
  var $_Else = '';
  var $_Confirmation = '';
  var $_FormAttrs = '';
  var $_ButtonAttrs = '';
  var $_ButtonClass = '';
  var $_Shortcut;

  function init($param)
  {
    $p = ParseParam($param,array('caption','action','uri','params','right','condition','else','confirmation','formattrs','buttonattrs','buttonclass','shortcut'));
    if(!is_array($p)) return $p;
    $this->_Caption      = is_null($p['caption']) ? null : text_translate($p['caption']);
    $this->_Action       = $p['action'];
    $this->_Uri          = $p['uri'];
    $this->_Params       = $p['params'];
    $this->_Right        = $p['right'];
    $this->_Condition    = $p['condition'];
    $this->_Else         = $p['else'];
    $this->_Confirmation = is_null($p['confirmation']) ? null : text_translate($p['confirmation']);
    $this->_FormAttrs    = $p['formattrs'];
    $this->_ButtonAttrs  = $p['buttonattrs'];
	  $this->_ButtonClass = $p['buttonclass'];
    $this->_Shortcut     = (isset($p['shortcut'])) ? $p['shortcut'] : '';
    if(!empty($this->_Shortcut))
      if(!(strlen($this->_Shortcut))) return text_translate('Der Shortcut einer ACTION darf nur ein Zeichen sein.');

    if(empty($this->_Caption)) return text_translate('Die Caption des ACTION-Befehls darf nicht leer sein.');
    return '';    
  }

  function compile($NSIndex,$Depth)
  {
    $formattrs = _JoinHTMLParam($this->_FormAttrs,$NSIndex,$Depth,
      array('method' => 'post',
      		'action' => ((empty($this->_Uri)) ? '$__URI__' : $this->_Uri),
      		'accept-charset' => 'utf-8'));
	//aenderung vulkanlan
	//bei allen ACTION das button desin geaendert werden. Beispiel: buttonclass="btn btn-danger btn-sm"
	if (empty($this->_ButtonClass))
	$this->_ButtonClass='button btn btn-outline-secondary btn-sm';
	$buttonattrs = array('type' => 'submit', 'class' => $this->_ButtonClass, 'value' => $this->_Caption);
	//old: $buttonattrs = array('type' => 'submit', 'class' => 'button', 'value' => $this->_Caption);

    if(!empty($this->_Shortcut)) 
    {
      $buttonattrs['value'] .= ' (Alt+'.strtoupper($this->_Shortcut).')';
      $buttonattrs['accesskey'] = strtolower($this->_Shortcut);
    }
    $buttonattrs = _JoinHTMLParam($this->_ButtonAttrs,$NSIndex,$Depth,$buttonattrs);
      
    if(empty($this->_Action)) $action = '';
    else $action  = '<input type="hidden" name="action" value="'._getHtmlVar($this->_Action,$NSIndex).'" />';
    
    $params = '';
    if(!empty($this->_Params)) 
    {
      if(_isArray($this->_Params)) 
      {
        foreach(_getArrayItems($this->_Params) as $k => $v)
        {
          $k = _GetHtmlVar($k,$NSIndex);
          $v = _GetHtmlVar($v,$NSIndex);
          $params .= '<input type="hidden" name="'.$k.'" value="'.$v.'" />';
        }
      }
      else
      {
        $pk = '$pk'.$Depth;
        $pv = '$pv'.$Depth;
        $p = _getPhpVar($this->_Params,$NSIndex);
        $params .= "<?php if(is_array($p)) foreach($p as $pk => $pv) { ?>";
        $params .= "<input type=\"hidden\" name=\"<?php echo $pk ?>\" value=\"<?php echo $pv ?>\" />";
        $params .= '<?php } ?>';
      }
    }
    if(empty($this->_Confirmation)) $conf = "";
    else {
      $conf_msg = '<?php echo base64_encode(' . _getPhpVar($this->_Confirmation, $NSIndex) . '); ?>';
      $conf_uri = _getHtmlVar((empty($this->_Uri)) ? "\$__URI__" : $this->_Uri,$NSIndex);
      $conf = "<input type=\"hidden\" name=\"confirmation\" value=\"$conf_msg\" />";
      $conf .= "<input type=\"hidden\" name=\"confirmation_uri\" value=\"$conf_uri\" />";
    }
    
    $right     = '$User->hasRight('._GetPhpVar($this->_Right,$NSIndex).')';
    $condition = _replaceVars($this->_Condition,$NSIndex);
    if(empty($this->_Right) and empty($this->_Condition)) $cond = '';
    elseif(empty($this->_Right)) $cond = $condition;
    elseif(empty($this->_Condition)) $cond = $right;
    else $cond = "($right) and ($condition)";
    
    $include = (empty($this->_Right)) ? '' : 'global $User; ';
    
    if(empty($this->_Else)) $else = '';
    else $else = ' else { ?>'._GetHtmlVar($this->_Else,$NSIndex).'<?php }';
    
    $form = '<form '.$formattrs.'>';
    $submit = '<input '.$buttonattrs.' />';
    if(empty($cond)) return "$form{$action}{$params}{$conf}{$submit}</form>";
    else return "<?php {$include}if($cond) { ?>{$form}{$action}{$params}{$conf}{$submit}</form><?php }$else ?>";
  }
}

/**
 * Generates a button embedded in a form, which performs a specified action when clicked.
 * 
 * <pre>
 * 	{#ACTION action=action uri=uri params=paramarray right=right condition=phpexpression else=else style=style confirmation=conf formattrs=array buttonattrs=array}
 * 		<p>Some Text</p>
 * 		<p><img src={#STATICFILE somefile}" alt="Also images are possible" /></p>
 *  {#END}
 * </pre>
 * 
 * Aguments are pretty much the same than _Cmd_Action above except for caption, which does not exist here, since HTMLAction is a block and no command.
 * 
 * There is also one additional argument:
 * 
 * style is a string, which is filled in the style argument of the button
 */
class _Cmd_HTMLAction extends _CmdBlock {
	
	/*
	 * Some local used variables
	 */
	var $_Action = '';
	var $_Uri = '';
	var $_Params = '';
	var $_Right = '';
	var $_Condition = '';
	var $_Else = '';
	var $_Confirmation = '';
	var $_Formattrs = '';
	var $_Buttonattrs = '';
	var $_Style = '';
	var $_Shortcut;

  	function init($param) {
  		$params = array('action','uri','style','params','right','condition','else','confirmation','formattrs','buttonattrs','shortcut');
	    $p = ParseParam($param,$params);
	    if(!is_array($p)) return $p;
	    foreach ($params as $pa) {
	    	$this->{'_'.ucfirst($pa)} = $p[$pa];
	    }
	    $this->_Confirmation = is_null($this->_Confirmation) ? null : text_translate($this->_Confirmation);
	    $this->_Shortcut     = (isset($this->_Shortcut)) ? $this->_Shortcut : '';
	    if(!empty($this->_Shortcut))
	    	if(!(strlen($this->_Shortcut))) return text_translate('Der Shortcut einer HTMLACTION darf nur ein Zeichen sein.');
	    return '';    
  	}
  	
	function add($data) {
    	$this->_Code[] = $data;
  	}
  	
  	function compile($NSIndex,$Depth) {
  		//Parse and set the give args for the form and button
  		$formattrs = _JoinHTMLParam($this->_Formattrs,$NSIndex,$Depth, array('method' => 'post','action' => ((empty($this->_Uri)) ? '$__URI__' : $this->_Uri)));
		$buttonattrs = array('type' => 'submit', 'class' => 'button', 'style' => $this->_Style);
    	if(!empty($this->_Shortcut)) $buttonattrs['accesskey'] = strtolower($this->_Shortcut);
 		$buttonattrs = _JoinHTMLParam($this->_Buttonattrs,$NSIndex,$Depth,$buttonattrs);
 		
 		//Action
    	if(empty($this->_Action)) $action = '';
    	else $action  = '<input type="hidden" name="action" value="'._getHtmlVar($this->_Action,$NSIndex).'" />';
    
    	//parse the given parameters for the action and return them as hidden input field
	    $params = '';
		if(!empty($this->_Params)) {
	    	if(_isArray($this->_Params)) {
	        	foreach(_getArrayItems($this->_Params) as $k => $v) {
					$k = _GetHtmlVar($k,$NSIndex);
					$v = _GetHtmlVar($v,$NSIndex);
					$params .= '<input type="hidden" name="'.$k.'" value="'.$v.'" />';
	        	}
	    	} else {
		        $pk = '$pk'.$Depth;
		        $pv = '$pv'.$Depth;
		        $p = _getPhpVar($this->_Params,$NSIndex);
		        $params .= "<?php if(is_array($p)) foreach($p as $pk => $pv) { ?>";
		        $params .= "<input type=\"hidden\" name=\"<?php echo $pk ?>\" value=\"<?php echo $pv ?>\" />";
		        $params .= '<?php } ?>';
	      	}
		}
		
		//Confirmation
	    if(empty($this->_Confirmation)) $conf = "";
	    else {
	    	$conf_msg = '<?php echo base64_encode(' . _getPhpVar($this->_Confirmation, $NSIndex) . '); ?>';
	    	$conf_uri = _getHtmlVar((empty($this->_Uri)) ? "\$__URI__" : $this->_Uri,$NSIndex);
	    	$conf = "<input type=\"hidden\" name=\"confirmation\" value=\"$conf_msg\" />";
	    	$conf .= "<input type=\"hidden\" name=\"confirmation_uri\" value=\"$conf_uri\" />";
		}
	    
		//Right and condition
	    $right     = '$User->hasRight('._GetPhpVar($this->_Right,$NSIndex).')';
	    $condition = _replaceVars($this->_Condition,$NSIndex);
	    if(empty($this->_Right) and empty($this->_Condition)) $cond = '';
	    elseif(empty($this->_Right)) $cond = $condition;
	    elseif(empty($this->_Condition)) $cond = $right;
	    else $cond = "($right) and ($condition)";
	    
	    $include = (empty($this->_Right)) ? '' : 'global $User; '; 
	    
	    if(empty($this->_Else)) $else = '';
	    else $else = ' else { ?>'._GetHtmlVar($this->_Else,$NSIndex).'<?php }';
	    
	    $form = '<form '.$formattrs.'>';
	    $submit = '<button '.$buttonattrs.'>'.$this->_compileArray($this->_Code,$NSIndex,$Depth).'</button>';
	    if(empty($cond)) return "$form{$action}{$params}{$conf}{$submit}</form>";
	    else return "<?php {$include}if($cond) { ?>{$form}{$action}{$params}{$conf}{$submit}</form><?php }$else ?>";
  	}
}

class _Cmd_DBImage extends _Cmd
{
  var $_Ident = '';
  var $_TableName = '';
  var $_ColName = '';
  var $_ID = 0;
  var $_MTime = 0;
  var $_Attrs = array();
  var $_Fileonly = false;
  var $_SizeOnlyForSWF = false;
  var $_Process = false;
  // $TableName,$ColName,$ID,$MTime

  	//php 7 public function __construct()
	//php 5 original function _Cmd_DBImage()
  function __construct() {
	//php 5:
	//parent :: _Cmd();
	//php7 neu:
	parent::__construct();

  	$this->_setAttrs(array('width'=>0, 'height'=>0));
  }

  function init($param)
  {
	$p = ParseParam($param,array('table_or_ident','col','id','mtime','fileonly','sizeonlyforswf','process'),false);
    if(!is_array($p)) return $p;
    if(!empty($p['table_or_ident']) and empty($p['col']) and empty($p['id']) and empty($p['mtime']))
    {
       // Das Bild wird &uuml;ber sein Ident angesprochen.
       $this->_Ident = $p['table_or_ident'];
    }
    else
    {
      // Das Bild wird die separate Angabe von Tabelle, Spalte, ID und mtime angesprochen.
      if(empty($p['table_or_ident'])) return text_translate('Die "table" des DBImage-Befehls darf nicht leer sein.');
      if(empty($p['col']))            return text_translate('Der "col" des DBImage-Befehls darf nicht leer sein.');
      if(empty($p['id']))             return text_translate('Die "id" des DBImage-Befehls darf nicht leer sein.');
      if(empty($p['mtime']))          return text_translate('Die "mtime" des DBImage-Befehls darf nicht leer sein.');
	  // Hier wird noch das Tabellen-Pr&auml;fix eingef&uuml;gt, damit auch Referenzen auf Tabellen innerhalb der Templates
	  // korrekt gehandhabt werden.
      $this->_TableName = TblPrefix().$p['table_or_ident'];
      $this->_ColName   = $p['col'];
      $this->_ID        = $p['id'];
      $this->_MTime     = $p['mtime'];
    }    
    $this->_Fileonly = ($p['fileonly']) ? true : false;
    $this->_SizeOnlyForSWF = ($p['sizeonlyforswf']) ? true : false;
    $this->_Process = $p['process'];
    unset($p['table_or_ident']);
    unset($p['col']);
    unset($p['id']);
    unset($p['mtime']);
    unset($p['fileonly']);
    unset($p['sizeonlyforswf']);
    unset($p['process']);
    $this->_setAttrs($p);
    return '';    
  }
  
  /**
   * Garantiert, dass in Attrs die Schl&uuml;ssel 'width' und 'height' vorkommen
   * 
   * @since 1400 - 31.05.2007
   * @author loom
   * @access private
   * @param Array $array
   */
  function _setAttrs($array) {
  	if(!is_array($array)) {
  		$array = array();
  	}
  	if(!isset($array['width'])) {
  		$array['width'] = 0;
  	}
  	if(!isset($array['height'])) {
  		$array['height'] = 0;
  	}
  	$this->_Attrs = $array;
  }

  function compile($NSIndex,$Depth)
  {
    $a = $this->_Attrs;
    // swfs m&uuml;ssen ihre gr&ouml;&szlig;e immer als parameter bekommen.
    $swfwidth  = _GetHtmlVar($a['width'],$NSIndex);
    $swfheight = _GetHtmlVar($a['height'],$NSIndex);
    // pngs/gifs/jpegs bekommen die parameter nur als gr&ouml;&szlig;e, wenn sozeonlyforswf false ist.
    $imgwidth  = (empty($a["width"]) or $this->_SizeOnlyForSWF)  ? "<?php echo \$img{$Depth}[\"width\"] ?>"  : $swfwidth;
    $imgheight = (empty($a["height"]) or $this->_SizeOnlyForSWF) ? "<?php echo \$img{$Depth}[\"height\"] ?>" : $swfheight;
    $alt       = (empty($a['alt'])) ? '' : _GetHtmlVar($a['alt'],$NSIndex);
    
    unset($a['width']);
    unset($a['height']);
    unset($a['alt']);
    unset($a['src']);
    $attrs = _JoinParam($a,$NSIndex,$Depth);
    
    if(empty($this->_Ident))
    {
      $table = _GetPhpVar($this->_TableName,$NSIndex);
      $col   = _GetPhpVar($this->_ColName,$NSIndex);
      $id    = _GetPhpVar($this->_ID,$NSIndex);
      $mtime = _GetPhpVar($this->_MTime,$NSIndex);
      $identtag = '';
      $if    = $id;
    }
    else
    {
      $table = "\$itable$Depth";
      $col   = "\$icol$Depth";
      $id    = "\$iid$Depth";
      $mtime = "\$imtime$Depth";
      $ident = _GetPhpVar($this->_Ident,$NSIndex);
      $dt1   = '$dt1';
      $dt2   = '$dt2';
      $identtag = "list($table,$col,$id,$mtime,$dt1,$dt2)=explode(\":\",$ident); if(!empty($dt1)) { $mtime = $mtime.\":\".$dt1.\":\".$dt2; } ";
      $if    = $ident;
    }
    
    $src       = "\$img{$Depth}[\"link\"].\"?anticache=\".urlencode($mtime)";
    $isswf     = "\$img{$Depth}[\"isswf\"]";

    if(empty($this->_Process)) $process = '';
    else $process = ',new ImageProcessorCode('._GetPhpVar($this->_Process,$NSIndex).')';    
    
    $imgcode = "<img src=\"<?php echo $src ?>\" width=\"$imgwidth\" height=\"$imgheight\" alt=\"$alt\" $attrs/>";
    
    $swfcode  = "<object width=\"$swfwidth\" height=\"$swfheight\" data=\"<?php echo $src ?>\" type=\"application/x-shockwave-flash\" $attrs>";
    $swfcode .= "<param name=\"movie\" value=\"<?php echo $src ?>\"></object>";
    
    $r  = "<?php if(!empty($if)) { include_once(\"mod/mod.image.php\"); $identtag";
    $r .= "\$img$Depth = new ImageLoaderDBField($table,$col,$id,$mtime$process); \$img$Depth = \$img{$Depth}->getInfo(); ";
    $r .= "if(!empty(\$img{$Depth}[\"height\"]) and !empty(\$img{$Depth}[\"width\"])) { ";
    if($this->_Fileonly) $r .= "echo $src; ";
    else $r .= " if($isswf) { ?>$swfcode<?php } else { ?>$imgcode<?php } ";
    $r .= '} }?>';
    return $r;
  }
}


class _CmdTable_Section
{
  var $Caption = '';
  var $Params = array();
  var $_Code = array();  
  var $_ParamVals = array();
  
  function init($param)
  {
    foreach($this->Params as $p) unset($param[$p]);
    $this->_ParamVals = $param;
  }
  
  function add($data)
  {  
    $this->_Code[] = $data;
  }
  
  function compile($NSIndex,$Depth)
  {
    return '';
  }
  
  function _compileArray($Array,$NSIndex,$Depth)
  {
    foreach($Array as $k => $v) if(is_object($v)) $Array[$k] = $v->compile($NSIndex,$Depth);
    return implode('',$Array);
  }   
  
  function useUser()
  {
    return false;
  }
  
  function getHeader($NSIndex)
  {
    return '<th class="tdedit">&nbsp;</th>';
  }
  
  function doOrder()
  {
    return false;
  }
}

class _CmdTable_Col extends _CmdTable_Section
{
  var $Params = array('caption','val');
  var $_Val = '';

  function init($param)
  {
    $this->Caption = text_translate($param['caption']);
    $this->_Val = $param['val'];
    if(empty($param['class'])) $param['class'] = 'tdcont';
    parent::init($param);
  }
  
  function compile($NSIndex,$Depth, $label=false)
  {
    $code = $this->_compileArray($this->_Code,$NSIndex,$Depth);
    if(preg_match("/^[\n\r\t ]*$/",$code) and !empty($this->_Val)) $code = _getHtmlVar($this->_Val,$NSIndex);
    if($label) {
      return '<td '._JoinParam($this->_ParamVals,$NSIndex).'><label for="'.$label.'" class="hand fillcell">'.$code.'</label></td>';
    }
    return '<td '._JoinParam($this->_ParamVals,$NSIndex).'>'.$code.'</td>';
  }

  function getHeader($NSIndex)
  {
    $capt = _getHtmlVar($this->Caption,$NSIndex);
    $order = _getVarName($this->_Val);
    if(empty($this->Caption)) return '<th class="tdedit">&nbsp;</th>';
    if(empty($order)) return '<th class="tdedit">'.$capt.'</th>';
    $r  = '<th class="tdedit thedit">';
    $r .= "<?php if(\$_GET['orderby'] == \"$order\") echo (\$_GET['order'] == \"ASC\") ? \" /\" : \" \\\\\"; ?>";
    $r .= "<a href=\"<?php echo EditURL(array(\"orderby\" => \"$order\",\"order\" => (\$_GET['order'] == 'ASC') ? \"DESC\" : \"ASC\")) ?>\">$capt</a>";
    $r .= "<?php if(\$_GET['orderby'] == \"$order\") echo (\$_GET['order'] == \"ASC\") ? \"\\\\ \" : \"/ \"; ?>";
    $r .= '</th>';
    return $r;
  }
  
  function doOrder()
  {
    return (empty($this->_Val) or empty($this->Caption)) ? false : true;
  }
  
}

class _CmdTable_Frame extends _CmdTable_Section
{
  var $Params = array('caption','frame','link','right','condition');
  var $_Caption = '';
  var $_Frame = '';
  var $_Link = '';
  var $_Right = '';
  var $_Condition = '';
  var $_ButtonClass = '';

  function init($param)
  {
    $this->_Caption   = text_translate($param['caption']);
    $this->_Frame     = $param['frame'];
    $this->_Link      = $param['link'];
    $this->_Right     = $param['right'];
    $this->_Condition = $param['condition'];
	$this->_ButtonClass = $param['buttonclass'];

    if(!empty($this->_Link) and !empty($this->_Frame)) return text_translate('Es k&ouml;nnen nicht Link und Frame angegeben werden.');
    if(empty($this->_Link) and empty($this->_Frame)) return text_translate('Entweder Link oder Frame muss angegeben werden.');
    if(empty($param['class'])) $param['class'] = 'tdframe';
    parent::init($param);
  }
  
  function useUser()
  {
    return !empty($this->_Right);
  }
  
  function compile($NSIndex,$Depth)
  {
    $code = $this->_compileArray($this->_Code,$NSIndex,$Depth);
    $param = _JoinParam($this->_ParamVals,$NSIndex);
    $caption = _GetHtmlVar($this->_Caption,$NSIndex);
    
    if(empty($this->_Link)) $link = "<?php echo\""._getStrVar("\$__SCRIPT__",$NSIndex)."?frame="._GetStrVar($this->_Frame,$NSIndex)."&amp;id="._getStrVar("\$id",$NSIndex)."\" ?>";
    else $link = _GetHtmlVar($this->_Link,$NSIndex);
    
    $right     = '$User->hasRight('._GetPhpVar($this->_Right,$NSIndex).')';
    $condition = _replaceVars($this->_Condition,$NSIndex);
    if(empty($this->_Right) and empty($this->_Condition)) $cond = "";
    elseif(empty($this->_Right)) $cond = $condition;
    elseif(empty($this->_Condition)) $cond = $right;
    else $cond = "($right) and ($cond)";
    
	//aenderung VulkanLAN Beispiel buttonclass="btn btn-danger btn-sm"
	$buttonclass = $this->_ButtonClass;
	if (empty($this->_ButtonClass))$buttonclass='button btn btn-outline-secondary btn-sm';

    if(empty($cond)) return "<td $param><a href=\"$link\"  class=\"$buttonclass\">$caption</a></td>";
    else return "<td $param><?php if($cond) { ?><a href=\"$link\"  class=\"$buttonclass\">$caption</a><?php } ?></td>";
  }
}

class _CmdTable_Action extends _CmdTable_Section
{
  var $Params = array('caption','action','uri','params','right','condition','confirmation');
  var $_Caption = '';
  var $_Action = '';
  var $_Uri = '';
  var $_Params = '';
  var $_Right = '';
  var $_Condition = '';
  var $_Confirmation = '';
  var $_ButtonClass = '';

  function init($param)
  {
    $this->_Caption   = text_translate($param['caption']);
    $this->_Action    = $param['action'];
    $this->_Uri       = $param['uri'];
    $this->_Params    = $param['params'];
    $this->_Right     = $param['right'];
    $this->_Condition = $param['condition'];
    $this->_Confirmation = text_translate($param['confirmation']);
	$this->_ButtonClass = $param['buttonclass'];
    if(empty($param['class'])) $param['class'] = 'tdaction';
    parent::init($param);
  }

  function useUser()
  {
    return !empty($this->_Right);
  }
  
  function compile($NSIndex,$Depth)
  {
    $code = $this->_compileArray($this->_Code,$NSIndex,$Depth);
    $param = _JoinParam($this->_ParamVals,$NSIndex);
    $caption = _GetHtmlVar($this->_Caption,$NSIndex);
    
    if(empty($this->_Action)) $action = "";
    else
    {
      $action  = "<input type=\"hidden\" name=\"action\" value=\""._getHtmlVar($this->_Action,$NSIndex)."\" />";
      $action .= "<input type=\"hidden\" name=\"id\" value=\""._getHtmlVar("\$id",$NSIndex)."\" />";
    }
    if(empty($this->_Params)) $params = "";
    else
    {
      $pk = "\$pk$Depth";
      $pv = "\$pv$Depth";
      $p = _getPhpVar($this->_Params,$NSIndex);
      $params  = "<?php if(is_array($p)) foreach($p as $pk => $pv) { ?>";
      $params .= "<input type=\"hidden\" name=\"<?php echo $pk ?>\" value=\"<?php echo $pv ?>\" />";
      $params .= '<?php } ?>';
    }
    if(empty($this->_Confirmation)) $conf = '';
    else {
    	$conf_msg = '<?php echo base64_encode(' . _getPhpVar($this->_Confirmation, $NSIndex) . '); ?>';
    	$conf = '<input type="hidden" name="confirmation" value="' . $conf_msg . '" />';
    }
    
    if(empty($this->_Uri)) $uri = '<?php echo GetRequestURI() ?>';
    else $uri = _getHtmlVar($this->_Uri,$NSIndex);
    
    $right     = '$User->hasRight('._GetPhpVar($this->_Right,$NSIndex).')';
    $condition = _replaceVars($this->_Condition,$NSIndex);
    if(empty($this->_Right) and empty($this->_Condition)) $cond = '';
    elseif(empty($this->_Right)) $cond = $condition;
    elseif(empty($this->_Condition)) $cond = $right;
    else $cond = "($right) and ($condition)";
    
    $form = '<form method="post" action="'.$uri.'">';
    $submit = '<input type="submit" class="btn button btn-outline-dark" value="'.$caption.'" />';
    if(empty($cond)) return "$form<td $param>{$action}{$params}{$conf}{$submit}</td></form>";
    else return "$form<td $param><?php if($cond) { ?>{$action}{$params}{$conf}{$submit}<?php } ?></td></form>";
  }
}

class _CmdTable_Option extends _CmdTable_Section
{
  var $Params = array('caption','action','right','condition','confirmation');
  var $_Caption = '';
  var $_Action = '';
  var $_Right = '';
  var $_Condition = '';
  var $_Confirmation = '';
  
  function init($param)
  {
    $this->_Caption   = text_translate($param['caption']);
    $this->_Action    = $param['action'];
    $this->_Right     = $param['right'];
    $this->_Condition = $param['condition'];
    $this->_Confirmation = text_translate($param['confirmation']);
    if(empty($this->_Action)) return text_translate('In einer TableSecion vom Typ Option wurde keine Aktion angegeben.');
    if(empty($this->_Caption)) return text_translate('In einer TableSecion vom Typ Option wurde keine Caption angegeben.');
    parent::init($param);
  }

  function useUser()
  {
    return !empty($this->_Right);
  }
 
  function getCondition($NSIndex)
  {
    $right     = '$User->hasRight('._GetPhpVar($this->_Right,$NSIndex).')';
    $condition = _replaceVars($this->_Condition,$NSIndex);
    if(empty($this->_Right) and empty($this->_Condition)) return '';
    elseif(empty($this->_Right)) return $condition;
    elseif(empty($this->_Condition)) return $right;
    else return "($right) and ($condition)";
  }
  
  function compile($NSIndex,$Depth)
  {
    if(empty($this->_Confirmation)) $conf = '';
    else {
    	$conf_msg = '<?php echo base64_encode(' . _getPhpVar($this->_Confirmation, $NSIndex) . '); ?>';
    	$conf = '<input type="hidden" name="confirmation" value="'. $conf_msg .'" />';
    }
    //$cond = $this->getCondition($NSIndex);
    $capt = _getHtmlVar($this->_Caption,$NSIndex);
    $code = $this->_compileArray($this->_Code,$NSIndex,$Depth);
    $sel = '<input type="radio" name="action" value="'._getHtmlVar($this->_Action,$NSIndex).'" />';
    
    return "<tr><td><label class=\"hand fillcell\">$sel{$conf} $capt $code</label></td></tr>";
  }
}



/**
 * Generiert eine HTML-Tabelle. 
 * 
 * <pre>
 * {#TABLE items=$items maxrows=maxrows foundrows=foundrows dborder=dborder}
 *  belowtablecontent
 * {#COL caption=caption val=val} cellcontent
 * {#FRAME  caption frame link right condition}
 * {#ACTION caption action uri params right condition confirmation}
 * {#OPTION caption action right condition confirmation} optioninputs
 * {#END}
 * </pre>
 *
 * Der Table-Befehl generiert eine Tabelle, die...
 * ...vom User sortiert werden kann.
 * ...ihren Inhalt automatisch auf mehrere Seiten aufteilen kann.
 * ...grundlegende Interfaces zur bearbeitun ihrer Daten zur Verf&uuml;gung stellt.
 *
 * Sowohl der TABLE-Befehl als auch fast alle seiner Sektionen k&ouml;nnen weitere Attribute als die 
 * unten beschriebenen enthalten, diese werden dann als HTML-Attribute verwendet.
 *
 * items ist ein Array, welches die Zeilen der Tabelle (wiederum ein Array) enth&auml;lt.
 *
 * maxrows gibt an, wie viele Datens&auml;tze maximal in einer Ansicht angezeit werden.
 *
 * foundrows: wenn foundrows gesetzt ist, darf $items maximal maxrows Datens&auml;tze enthalten
 * und foundrows gibt die gesamte Anzahl der Datens&auml;tze an. Diese Funktion ist f&uuml;r gr&ouml;&szlig;ere Datenmengen
 * gedacht, bei denen gar nicht erst alle Daten ausgelesen werden.
 *
 * dborder [0,1] gibt an, dass die Sortierung nicht von der Tabelle sondern von der
 * Anwendungslogik oder Datenbank &uuml;bernommen wird. Dies muss geschehen, wenn mit foundrows gearbeitet wird. 
 *
 * belowtablecontent ist content, welcher unten rechts in der Tabelle dargestellt wird. Typischer Weise ein Link, 
 * um einen neuen Datensatz zu erstellen.
 *
 *
 * Sektion #COL:
 * Formatiert die Werte einer Spalte der Tabelle.
 *
 * caption ist der Titel der Spalte.
 *
 * val ist der Wert des jeweiligen Datensatzes. Nach ihm wird diese Spalte sortiert und, wenn cellcontent angegeben ist,
 * wird er als Zellinhalt ausgegeben.
 *
 *
 * Sektion #FRAME:
 * Generiert einen Link auf einen anderen, datensatzspezifischen Frame. An diesen wird die ID des 
 * Datensatzes mit &uuml;bergeben.
 * 
 * caption ist der Titel des Links.
 * 
 * frame ist der name des Frames auf den verlinkt wird.
 * 
 * link kann alternativ zu frame angegeben werden, z.B wenn mehrere Parameter angegeben werden 
 * m&uuml;ssen oder der aufzurufende Frame in einer anderen Page liegt.
 *
 * right ist ein Rechtename oder eine RechteID. Hat der User dieses Recht nicht, so wird der FrameLink ausgeblendet.
 *
 * condition ist eine phpexpression (siehe #IF-Befehl), welche erf&uuml;llt sein muss, damit der FrameLink angezeigt wird.
 *
 *
 * Sektion #ACTION:
 * generiert einen Button f&uuml;r jeden Datensatz, welcher beim clicken eine Action ausl&ouml;st.
 *
 * caption ist der Titel des Buttons.
 *
 * action ist die Action, die beim Clicken des Buttons aufgerufen wird.
 *
 * uri ist die Adresse, die nach dem Clicken vom Browser aufgerufen wird.
 *
 * params ist ein Array mit weiteren Parametern f&uuml;r die Action-Methode.
 *
 * right ist ein Rechtename oder eine RechteID. Hat der User dieses Recht nicht, so wird der Button ausgeblendet.
 *
 * condition ist eine phpexpression (siehe #IF-Befehl), welche erf&uuml;llt sein muss, damit der Button angezeigt wird.
 *
 * confirmation ist eine Frage, die der User mit "Ja" beantworten muss, bevor die Action ausgef&uuml;hrt wird.
 *
 *
 * {#OPTION caption action right condition confirmation} optioninputs
 * Sektion #OPTION:
 * Sind ein oder mehrere OPTIONs aktiv, so werden vor allen Datens&auml;tzen Checkboxen angezeigt. Unter der Tabelle kann der User dann
 * die Option ausw&auml;hlen, um eine Operation mit den ausgew&auml;hlten Datens&auml;tzen durch zu f&uuml;hren. Die OPTION ist 
 * also eine Action, die f&uuml;r mehrere Datens&auml;tze aufgerufen wird.
 *
 * caption ist der Titel des Buttons.
 *
 * action ist die Action-Methode, beim aufrufen der OPTION ausgef&uuml;hrt wird.
 *
 * right ist ein Rechtename oder eine RechteID. Hat der User dieses Recht nicht, so wird die OPTION ausgeblendet.
 *
 * condition ist eine phpexpression (siehe #IF-Befehl), welche erf&uuml;llt sein muss, damit die OPTION angezeigt wird.
 *
 * confirmation ist eine Frage, die der User mit "Ja" beantworten muss, bevor die Action ausgef&uuml;hrt wird.
 *
 * optioninputs kann zB weitere INPUT-Befehle enthalten, um weitere Parameter der Action vom User bestimmen zu lassen.
 **/


class _Cmd_Table extends _CmdBlock
{
  var $_Params    = array();
  var $_Items     = '';
  var $_Cols      = array();
  var $_Footer    = array();
  var $_Form      = NULL;
  var $_MaxRows   = '';
  var $_FoundRows = '';
  var $_DBOrder   = '';
  var $_ID    = 0;  
  
  function init($param)
  {    
    global $_TableID;
    $this->_Params = ParseParam($param,array('items','maxrows','foundrows','dborder'),false);
    if(!is_array($this->_Params)) return $this->_Params;
    $this->_Items   = $this->_Params['items'];
    $this->_MaxRows = $this->_Params['maxrows'];
    $this->_FoundRows = $this->_Params['foundrows'];
    $this->_DBOrder = $this->_Params['dborder'];
    unset($this->_Params['items']);
    unset($this->_Params['maxrows']);
    unset($this->_Params['foundrows']);
    unset($this->_Params['dborder']);
    if(empty($this->_Params['class'])) $this->_Params['class'] = 'table';
    if(!isset($this->_Params['cellspacing'])) $this->_Params['cellspacing'] = 1;
    if(!isset($this->_Params['cellpadding'])) $this->_Params['cellpadding'] = 1;
    if(!_isVar($this->_Items)) return text_translate('Der Parameter "Items" des Befehles TABLE muss eine g&uuml;ltige Variable sein.');
    if(empty($this->_DBOrder)) $this->_DBOrder = 0;
    elseif(!in_array($this->_DBOrder,array('1','0'))) return text_translate('Der Parameter "dborder" des Befehls TABLE darf nur 1 oder 0 sein.');
    
    if(isset($_TableID)) $_TableID++;
    else $_TableID = 0;
    $this->_ID = $_TableID;
    
    $this->_Form = new _Cmd_Form();
    $this->_Form->init("name=\"table{$this->_ID}\" keepvals=0");
  }
  
  function newSection($aName,$aParam)
  {
    switch($aName)
    {
      case('col'):    $c = new _CmdTable_Col(); break;
      case('frame'):  $c = new _CmdTable_Frame(); break;
      case('action'): $c = new _CmdTable_Action(); break;
      case('option'): $c = new _CmdTable_Option(); break;
      default: return 'Der TABLE-Befehl hat keine Sektion namens "'.$aName.'"';    
    }
    $p = ParseParam($aParam,$c->Params,false);
    if(!is_array($p)) return $p;
    if($err = $c->init($p)) return $err;
    array_unshift($this->_Cols,$c);
  }
  
  function isValidSection($aName)
  {                             
    return (in_array($aName,array('col','frame','action','option'))) ? true : false;
  }
  
  function add($data)
  { 
    if(isset($this->_Cols[0])) $this->_Cols[0]->add($data);
    else $this->_Footer[] = $data;
  }
  
  function finish()
  {
    if(is_object($this->_Form)) {
    	$this->_Form->finish();
    }
  }
  
  function compile($NSIndex,$Depth)
  {
    $cols = $options = array();
    $useuser = $doorder = false;
    foreach(array_reverse($this->_Cols) as $v)
    {
      if(is_a($v,'_CmdTable_Option')) $options[] = $v;
      else $cols[] = $v;
      if($v->useUser()) $useuser = true;
      if($v->doOrder()) $doorder = true;
    }
    if($this->_DBOrder) $doorder = false;

    $footer = $this->_compileArray($this->_Footer,$NSIndex,$Depth+1);
    $items = _getPhpVar($this->_Items,$NSIndex);

    $dooptions = (count($options) > 0) ? true : false;
    $dofooter  = !preg_match("/^[\n\r\t ]*$/",$footer);

    $row = '$row'.$Depth;
    $oco = '$oco'.$Depth;
    $doo = '$doo'.$Depth;
    
    if($useuser) $r = '<?php global $User; ?>';
    else $r = '';
    if($dooptions)
    {
      $optionconds = array();
      $dovaroptions = false;
      foreach($options as $k => $o)
        if($optionconds[$k] = $o->getCondition($NSIndex)) $dovaroptions = true;
      
      if($dovaroptions)
      {
        $a = array();
        foreach($optionconds as $k => $c) $a[] = "\"$k\" => ".((empty($c)) ? "1" : "($c) ? 1 : 0");
        $r .= "<?php $oco = array(".implode(", ",$a)."); $doo = (array_sum($oco) > 0) ? true : false; ?>";
      }
    }
    if($dooptions) $colspan = ($dovaroptions) ? "<?php echo ".count($cols)." + (($doo) ? 1 : 0) ?>" : count($cols) + 1;
    else $colspan = count($cols);
    
    // order by
    if($doorder)
    {
      //$ob = '$_GET[\\\'orderby\\\']';
      //$o = '$_GET[\\\'order\\\']';
      //$func = "create_function('\$a,\$b','\$r = strnatcasecmp(\$a[$ob],\$b[$ob]); if($o == \'DESC\') \$r *= -1; return \$r;')";
      /*$func = function ($a, $b)
      { 
        $r = strnatcasecmp($a[$ob],$b[$ob]); 
        if ($o == 'DESC') $r *= -1;
        return $r; 
      };*/

      /*$order = "<?php if(isset(\$_GET['orderby']) && !empty($items)) usort($items,$func); ?>";*/
      $order = "<?php if(isset(\$_GET['orderby']) && !empty($items)) usort($items,function (\$a, \$b) {
        \$ob = \$_GET[\"orderby\"];
        \$o = \$_GET[\"order\"];
        \$r = strnatcasecmp(\$a[\$ob],\$b[\$ob]);
        if(\$o == 'DESC') \$r *= -1;
        return \$r;
      }); ?>"; 
    }
    else $order = '';
    
    // pageselect
    $pagecalc = '';
    $pageselect = '';
    if(!empty($this->_MaxRows))
    {
      $page = "\$page$Depth";
      $rows = "\$rows$Depth";
      $index = "\$i$Depth";
      $foundrows = "\$fr$Depth";
      $maxrows = _getPhpVar($this->_MaxRows,$NSIndex);
      $pagecalc .= "<?php list($rows) = (isset(\$_GET['limit'])) ? explode(',',\$_GET['limit']) : 0; $page = round($rows/$maxrows); ";
      if(!empty($this->_FoundRows)) $foundrows = _getPhpVar($this->_FoundRows,$NSIndex);
      else $pagecalc .= "if(is_array($items)) { $foundrows = count($items); $items = array_slice($items,$rows,$maxrows); } else $foundrows = 0; ";
      $pagecalc .= '?>';

      $pageselect = "<?php if($foundrows > $maxrows) { ?><tr><th class=\"tdedit\" colspan=\"$colspan\">";
      $pageselect .= "<?php for($index = 0; $index < ceil($foundrows / $maxrows); $index++) { ";
      $pageselect .= "if($index != 0) echo \" &middot; \"; ?>";
      $pageselect .= "<a href=\"<?php echo EditURL(array(\"limit\" => ($index*$maxrows).\",\".($maxrows))) ?>\" class=\"button btn btn-sm btn-outline-secondary\"";
      $pageselect .= "<?php echo ($page == $index) ? \" class=\\\"important\\\"\" : \"\" ?> class=\"button btn btn-sm btn-secondary\"><?php echo $index+1 ?></a>";
      $pageselect .= '<?php } ?></th></tr>'."\n".'<?php } ?>';
    }
    
    $r .= "$order$pagecalc<table "._JoinParam($this->_Params,$NSIndex).">$pageselect";
    
    // header
    $r .= '<thead><tr>';
    if($dooptions)
    {
      $jsname = "document.forms['table{$this->_ID}'].elements['ids[]']";
      $sel_all_or_none_header = "<script type=\"text/javascript\">\nfunction setchecked_h{$this->_ID}(chk) { ";
      $sel_all_or_none_header .= "if(typeof $jsname.length === 'undefined') { {$jsname}.checked = chk; } else { ";
      $sel_all_or_none_header .= "for(var i=0;i<$jsname.length;i++) {$jsname}[i].checked = chk; ";
      $sel_all_or_none_header .= "} return true; ";
      $sel_all_or_none_header .= "}</script>";
      $cell = '<th class="tdedit" style="width:18px">'.$sel_all_or_none_header.'<input type="checkbox" name="chkallbox'.$this->_ID.'" onchange="if(this.checked == true) { setchecked_h'.$this->_ID.'(true); } else { setchecked_h'.$this->_ID.'(false); }" /></th>';
      if($dovaroptions) $r .= '<?php if('.$doo.') { ?>'.$cell.'<?php } ?>';
      else $r .= $cell;
    }
    foreach($cols as $c) $r .= $c->getHeader($NSIndex);//$r .= "<th class=\"tdedit\">"._getHtmlVar($c->Caption,$NSIndex)."</th>";
    $r .= '</tr></thead>'."\n";
   
    // rows
    $ns = $NSIndex+1;
    $id = _getPhpVar('$id',$ns);
    $r .= "<?php if(is_array($items)) foreach($items as $row) { \$var".($NSIndex+1)." = array_merge(\$var$NSIndex,$row); ?>";
    $r .= '<tr>';
    
    if($dooptions) {
      $selall = "<?php echo (empty(\$_GET[\"selall\"])) ? \"\" : \"checked=\\\"1\\\"\" ?>";
      $addlabel = "ids_".$NSIndex."_<?php echo $id ?>";
      $cell = "<td class=\"tdedit\" width=\"18\"><input type=\"checkbox\" class=\"checkbox hand\" name=\"ids[]\" id=\"$addlabel\" value=\"<?php echo $id ?>\" $selall /></td>";
      if($dovaroptions) $r .= "<?php if($doo) { ?>$cell<?php } ?>";
      else $r .= $cell;
    }
    
    foreach($cols as $c) {
    	$r .= $c->compile($ns, $Depth+1, $addlabel);
    }
    
    $r .= '</tr>'."\n";
    $r .= "<?php } if(count($items) < 1) { ?>";
    $r .= "<tr><td class=\"tdcont\" colspan=\"$colspan\" align=\"center\"><i>".text_translate("keine Eintr&auml;ge vorhanden")."</i></td></tr>\n";
    $r .= '<?php } ?>';
    
    // footer
    $footer = trim($footer);
    if(empty($footer)) $footer = '&nbsp;';
    if($dooptions or $dofooter)
    {
      $r .= '<tr><td class="tdedit" colspan="'.$colspan.'" align="right">';
      if($dooptions)
      {
        $jsname = "document.forms['table{$this->_ID}'].elements['ids[]']";
       // $jsname = "document.table{$this->_ID}.ids";
        $sel_all_or_none = "<script type=\"text/javascript\">function setchecked{$this->_ID}(chk) { ";
        $sel_all_or_none .= "if(typeof $jsname.length === 'undefined') { {$jsname}.checked = chk; } else { ";
        $sel_all_or_none .= "for(var i=0;i<$jsname.length;i++) {$jsname}[i].checked = chk; ";
        $sel_all_or_none .= '} return true; ';
        $sel_all_or_none .= '}</script>';
		    $sel_all_or_none .= "<a href=\"<?php echo EditURL(array(\"selall\"=>1)) ?>\" onclick=\"setchecked{$this->_ID}(true); return false;\" class=\"btn btn-outline-secondary btn-sm\" role=\"button\">".text_translate("Alle")."</a> / ";
        $sel_all_or_none .= "<a href=\"<?php echo EditURL(array(\"selall\"=>0)) ?>\" onclick=\"setchecked{$this->_ID}(false); return false;\" class=\"btn btn-outline-secondary btn-sm\" role=\"button\">".text_translate("Keine")."</a>";
        if($dovaroptions)
        {
          $r .= "<?php if($doo) { ?>";
          $r .= '<table width="100%">';
          $r .= "<tr><td>$sel_all_or_none</td>";
          $r .= "<td rowspan=\"<?php echo array_sum($oco) + 1 ?>\"><input type=\"submit\" class=\"button btn btn-sm btn-secondary\" value=\"".text_translate("Ausf&uuml;hren")."\" /></td>";
          $r .= "<td rowspan=\"<?php echo array_sum($oco) + 1 ?>\" valign=\"top\" align=\"right\">$footer</td>";
          $r .= '</tr>';
          foreach($options as $k => $o)
          {
            if(empty($optionconds[$k])) $r .= $o->compile($NSIndex,$Depth+1);
            else $r .= "<?php if({$oco}[$k]) { ?>".$o->compile($NSIndex,$Depth+1)."<?php } ?>";
          }
          $r .= '</table>';
          $r .= '<?php } else { ?>'.$footer.'<?php } ?>';        
        }
        else
        {
          $r .= '<table width="100%">';
          $r .= "<tr><td>$sel_all_or_none</td>";
          $r .= "<td rowspan=\"".(count($options) + 1)."\"><input type=\"submit\" class=\"button btn btn-sm btn-secondary\" value=\"".text_translate("Ausf&uuml;hren")."\" /></td>";
          $r .= "<td rowspan=\"".(count($options) + 1)."\" valign=\"top\" align=\"right\" >$footer</td>";
          $r .= '</tr>';
          foreach($options as $o) $r .= $o->compile($NSIndex,$Depth+1);
          $r .= '</table>';
        }        
      }
      else $r .= $footer;
      $r .= '</td></tr>';
    }
    $r .= $pageselect.'</table>';
    if($dooptions) 
    {
      $this->_Form->add($r);
      return $this->_Form->compile($NSIndex,$Depth);
    }
    else return $r;
  }
}

/**
 * Gibt zu einem Key aus einer Tabelle (flip_table_*) den Display-Wert aus.
 * 
 * <pre>
 *   {#TABLEDISPLAY table=tablename key=key [param1=xx ...]}
 * </pre>
 * 
 * Der Befehl kann und soll auch in Schleifen verwendet werden, da er seine R&uuml;ckgabewerte cached. 
 * f&uuml;r jede Tabelle entsteht maximal ein Datenbankquery pro Seitenaufruf.
 * 
 * table ist der Name der Tabelle zu welcher der Key geh&ouml;rt.
 * 
 * key idetifiziert den Display-Wert.
 * 
 * param1 usw. werden dem Display-Wert &uuml;bergebn, wenn dieser Templatecode enth&auml;lt.
 */

class _Cmd_TableDisplay extends _Cmd
{
  var $_Table = '';
  var $_Key = '';
  var $_Vars = array();
  
  function init($param)
  {
	$p = ParseParam($param,array('table','key'),false);
    if(!is_array($p)) return $p;
    $this->_Table = $p['table'];
    $this->_Key = $p['key'];
    unset($p['table']);
    unset($p['key']);
    $this->_Vars = $p;
    return "";    
  }
  
  function compile($NSIndex,$Depth)
  {
    $t = _getPHPVar($this->_Table,$NSIndex);
    $k = _getPHPVar($this->_Key,$NSIndex);
    $v = _getArray($this->_Vars,$NSIndex);
    return '<?php include_once("inc/inc.table.php"); echo TableGetDisplay('."$t,$k,$v".'); ?>';
  }
}



?>
