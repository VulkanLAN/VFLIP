<?php
/**
 * @author Matthias Gro&szlig;
 * @version $Id: lanparty.php 1303 2006-09-17 16:21:17Z scope $
 * @copyright ? 2001-2007 The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** FLIP-Kern */
require_once ("core/core.php");

function LogPaid($uid = 0, $oid = 0) {
	$data = array (
		"user_id" => $uid,
		"orga_id" => $oid,
	"date" => time());
	if (($uid > 0) && ($oid > 0))
		return MysqlWriteByID(TblPrefix() . "flip_setpaid_history", $data);
	else
		return false;
}

function UnLogPaid($uid) {
	return MysqlExecQuery("DELETE FROM `" . TblPrefix() . "flip_setpaid_history` WHERE `user_id` = '$uid';");
}
?>
