<?php

/**
 * @author Daniel Raap
 * @version $Id: mod.tournament.php 1702 2019-01-09 09:01:12Z loom $ edit by naaux
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** Die Datei nur einmal includen */
if (defined('MOD.TOURNAMENT.PHP'))
	return 0;
define('MOD.TOURNAMENT.PHP', 1);

/** FLIP-Kern */
require_once ('core/core.php');
require_once ('inc/inc.sqlrowobject.php');
require_once ('mod/mod.tournament.match.php');

function TournamentTablename_Badnames() {
	return "TOURNAMENT_BADNAMES";
}

function TournamentTablename_Games() {
	return "TOURNAMENT_GAMES";
}

/**
 * Eigenschaft der Team-Subjekte, welche den Namen angibt
 */
function TournamentTeamNameColumn() {
	return "teamname";

}

/**
 * matches with levels above this are group-matches with 'group = level -
 * groupmatchestart' (level=10001 is group 1)
 */
function TournamentGroupmatchestartlevel() {
	return 10000;
}

/**
 * vorgesehene Ligen
 * liefert alle Ligen f&uuml;r die es Klassen gibt
 * 
 * @return Array 'DB-Wert'=>'Beschreibung'
 */
function TournamentLigen() {
	static $ligen = null;
	if (is_null($ligen)) {
		$ligen = array();
		require_once('mod/mod.tournament.liga.php');
		foreach(get_declared_classes() AS $class) {
			if(preg_match('/^TournamentLiga(.*)/i', $class, $patterns)) {
				$liga = new $class();
				$ligen[$patterns[1]] = $liga->name();
			}
		}
	}
	return $ligen;
}

/**
 * Liefert die Liga zu der ID/k&uuml;rzel/DB-Wert
 *
 * @param String Name der Liga in der Spalte `type` von flip_tournament_tournaments
 * @return object ein Liga-Objekt
 */
function TournamentGetLiga($dbName) {
	require_once('mod/mod.tournament.liga.php');
	$class = 'TournamentLiga'.$dbName;
	if(!class_exists($class)) {
		return TournamentError('Es gibt die Liga "'.escapeHtml($dbName).'" nicht!', E_USER_ERROR, __FILE__,__LINE__);
	}
	return new $class();
}

/**
 * Pr&uuml;ft ob die Liga g&uuml;ltig ist
 * 
 * @since 1356 - 01.02.2007
 * @param String $liga Liga (-k&uuml;rzel aus DB)
 * @return bool
 */
function TournamentValidLiga($liga) {
	//case-sensitive: return in_array($liga, array_keys(TournamentLigen()));
	foreach(array_keys(TournamentLigen()) AS $aLiga) {
		if(strtolower($liga) == strtolower($aLiga)) {
			return true;
		}
	}
	return false;
}

/**
 * m&ouml;gliche Staten und deren Anzeigenamen
 * enum in flip_tournament_tournaments.status
 * @return Array
 */
function TournamentStatuses() {
	$statuses = array (
		'closed' => 'geschlossen',
		'open' => 'Anmeldung',
		'start' => 'Aufw&auml;rmphase',
		'grpgames' => 'Gruppenspiele',
		'games' => 'l&auml;uft',
		'end' => 'beendet'
	);
	$statuses = _TournamentCheckEnumData(TblPrefix() . 'flip_tournament_tournaments', 'status', $statuses);
	return $statuses;
}

/**
 * Gibt eine Turnier-Fehlermeldung aus.
 *
 * Dem Fehlertext wird der Dateiname und die Zeile des Aufrufs angeh&auml;ngt.
 * Wenn es sich um einen Fehler handelt (kein Notice) wird vor der Fehlermeldung
 * "Tournamenterror: " angezeigt um in den Logdateien eine entsprechende
 * &uuml;bersichtlichkeit zu gew&auml;hrleisten.
 *
 * @param string $msg Die Fehlermeldung, welche ausgegeben werden soll.
 * @param string $lvl Error-Level (E_USER_NOTICE, E_USER_WARNING oder E_USER_ERROR)
 * @param string $file Dateiname in der der Fehler aufgetreten ist
 * @param integer $line Zeilennummer wo der Fehler aufgetreten ist (bzw. wo Error() aufgerufen wurde)
 * @return bool false
 **/
function TournamentError($msg, $lvl = E_USER_NOTICE, $file = false, $line = false) {
	if ($lvl != E_USER_NOTICE)
		$b = 'TournamentError: ';
	else
		$b = '';
	trigger_error($b.$msg.'| (aufgerufen von ' . basename($file) . '[line '.$line.'])', $lvl);
	return false;
}

/**
 * Pr&uuml;ft ob eine ID ein vorhandenes Turnier bezeichnet
 * 
 * @param int $aID TurnierID
 * @return boolean
 */
function TournamentIsValidID($aID) {
	$Id = (integer) $aID;
	if (!MysqlReadFieldByID(TblPrefix() . "flip_tournament_tournaments", "id", $Id, true)) {
		return false;
	}
	return true;
}

function TournamentRequireValidID($aID) {
	if(!TournamentIsValidID($aID)) {
		return TournamentError("Die angegebene Turnier-ID ist ung&uuml;ltig.|ID=$aID", E_USER_ERROR, __FILE__, __LINE__);
	}
	return true;
}

function TournamentAdminright() {
	return "tournament_admin";
}

/**
 * Pr&uuml;ft, ob der aktuelle User ein Turnieradmin ist
 */
function TournamentIsAdmin() {
	$adminright = TournamentAdminright();
	global $User;
	return $User->hasRight($adminright);
}

/**
 * Pr&uuml;ft, ob der aktuelle User ein Orga des angegebenen Turnieres ist
 * 
 * @param int $tournament_id TurnierID
 * @return boolean
 * 
 * @require $tournament_id gibt gueltiges Turnier an
 */
function TournamentIsOrga($tournament_id) {
	$orgaright = "tournament_orga";

	TournamentRequireValidID($tournament_id);

	global $User;
	if (TournamentIsAdmin() || $User->hasRightOver($orgaright, "Turniersubjekt" . $tournament_id)) {
		return true;
	} else {
		return false;
	}
}

/**
 * Gibt den Namen zu einer TeamID zur&uuml;ck
 *
 * @param integer $combatantid Die ID des Teams von dem der Name gesucht wird
 * @return string (htmlescapter) Name des Teams
 **/
function TournamentCombatantID2Name($combatantid) {
	static $Combatants = false;
	switch ($combatantid) {
		case "" :
			return "-";
		case "0" :
			return "Freilos";
		default :
			if (!$Combatants)
				if (!($Combatants = CacheGet("Combatants")))
					$Combatants = CacheSet("Combatants", MysqlReadArea("SELECT d.subject_id, d.val FROM (" . TblPrefix() . "flip_user_column c)
					                                        LEFT JOIN " . TblPrefix() . "flip_user_data d ON (d.column_id = c.id)
					                                        WHERE (c.name IN ('teamname')
					                                        AND (c.type = 'turnierteam'));
					                                        ", "subject_id"), array (
						"flip_tournament_combatant"
					));
			//not perfect, but the flip_user_x tables may change too often, so that caching isn't 
			//effectiv. Therefore this cache is updated in tournament.php, actionrenTeam()
			return escapeHtml($Combatants[$combatantid]["val"]);
	}
}

/**
 * Liefert die Turniergruppen
 * Liefert eine Liste mit allen Turniergruppen inkl.
 * Bilddaten f&uuml;r #DBIMAGE nach 'order' sortiert.
 * 
 * @return array zweidimensionales Array mit allen Zeilen und jeweils alle
 * Spalten
 */
function TournamentGetGroups() {
	static $groups = null;
	if (is_null($groups)) {
		include_once ('mod/mod.image.php');
		$query = 'SELECT `id`, `order`, `name`, ' . sqlImage(TblPrefix() . 'flip_tournament_groups', 'image') . ' FROM `' . TblPrefix() . 'flip_tournament_groups`';
		$groups = MysqlReadArea($query . ' ORDER BY `order`', 'id');
		if (!is_array($groups)) {
			$groups = array ();
		}
	}
	return $groups;
}

/**
 * Liefert eine Turniergruppe inkl. Bilddaten f&uuml;r #DBIMAGE.
 *
 * @param integer $gid ID einer Turniergruppe
 * @return array eindimensionales Array mit allen Spalten
 */
function TournamentGetGroup($gid) {
	if (is_posDigit($gid)) {
		$groups = TournamentGetGroups();
		return isset ($groups[$gid]) ? $groups[$gid] : null;
	}
	trigger_error_text(text_translate('Es wurde keine ID angegeben') . '|$gid = ' . var_export($gid, true), E_USER_WARNING);
	return false;
}

/**
 * Liefert den Namen einer Gruppe
 * @param int $groupid ID der Gruppe
 * @return String Name der Gruppe
 */
function TournamentGetGroupName($groupid) {
	$groups = TournamentGetGroups();
	return isset ($groups[$groupid]) ? $groups[$groupid]['name'] : text_translate('unbekannt');
}

/**
 * Liest die Datenbankspalten aller Turniere aus und sortiert diese nach der Startzeit
 *
 * @return array Ein nummerisches Array welches f&uuml;r jedes Turnier ein Array mit
 * den Spalten der Datenbank + W&auml;hrungsname enth&auml;lt. 
 **/
function TournamentGetTournaments($forceLoad = false) {
	static $tdata = null;
	if (is_null($tdata) || $forceLoad) {
		$tdata = MysqlReadArea('SELECT t.*, c.currency FROM `' . TblPrefix() . 'flip_tournament_tournaments` t LEFT JOIN `' . TblPrefix() . 'flip_tournament_coins` c ON c.id=t.coin_id ORDER BY `start` DESC', 'id');
		if (!is_array($tdata)) {
			$tdata = array ();
		}
	}
	return $tdata;
}

/**
 * Liefert alle Datenbankspalten eines Turnieres
 * @param integer $tournamentid ID eines Turnieres
 * @return Array mit allen Spalten der Datenbank
 */
function TournamentGetTournament($tournamentid) {
	$tournaments = TournamentGetTournaments();
	return isset ($tournaments[$tournamentid]) ? $tournaments[$tournamentid] : null;
}

/**
 * Gibt einen Text zur&uuml;ck, welcher das Turnier beschreibt.
 * Ausser dem Namen des Spieles und der Teamgr&ouml;&szlig;e (XonX) steht evtl. die Liga
 * oder FUN vor dem Turniernamen.
 * @param integer $tournamentid Die ID des Turnieres von dem man den Text haben m&ouml;chte
 * @return string "[\[Gruppe\]] [LIGA] full gamename XonX" ([...] is optional)
 **/
function TournamentGetTournamentString($tournamentid) {
	if (!is_posDigit($tournamentid)) {
		return TournamentError('Fehler in TournamentGetTournamentString().| Es wurde keine TurnierID angegeben: $tournamentid = ' . var_export($tournamentid, true), E_USER_WARNING, __FILE__, __LINE__);
	}

	$t = TournamentGetTournament($tournamentid);
	$liga = TournamentGetLiga($t['liga']); 
	$gamedescr = $liga->getGameString($t['game']);
	if (!TournamentGameIncludeTeamsize($gamedescr))
		$gamedescr .= ' ' . $t['teamsize'] . 'on' . $t['teamsize'];
	$gamedescr = trim(($t['liga'] != 'Keine' ? $t['liga'] : '') . ' ' . $gamedescr);

	return $gamedescr;
}

/**
 * Pr&uuml;ft welche Werte in einer Spalte erlaubt sind und schreibt diese als
 * Schluessel in ein Array
 * Sind in dem Array die Schluessel bereits vorhanden, werden diese
 * beibehalten. Ungueltige Schluessel werden geloescht. Nicht vorhandene
 * hinzugefuegt.
 * 
 * @since 1348 - 26.01.2007
 * @access private
 * @param String $table Tabelle
 * @param String $col Spalte
 * @param Array $data Assoziatives Array, Werte sind Titel der Schluessel
 * z.B. array('col'=>'Beschreibung')
 * @return Array das &uuml;bergebene Array
 */
function _TournamentCheckEnumData($table, $col, $data) {
	$r = array ();
	foreach (MysqlColEnum($table, $col) AS $val) {
		if (isset ($data[$val]))
			$r[$val] = $data[$val];
		else
			$r[$val] = $val;
	}
	return $r;
}

/**
 * Pr&uuml;ft ob in allen Spielnamen die Turniergroesse enthalten ist
 * vor allem fuer importierte Ligaspiele wichtig
 * Es wird gepr&uuml;ft, ob in allen Ligaspielen die Teamgr&ouml;&szlig;e (XonX) angegeben
 * ist.
 * 
 * @since 1341 - 19.01.2007
 * @param Object $liga ein Ligaobjekt
 * @return boolean
 */
function TournamentGamesIncludeTeamsize($liga) {
	static $teamsizeInAllGames = null;
	if(!is_a($liga, 'BasicLiga')) {
		return TournamentError('Es wurde keine Liga angegeben!', E_USER_ERROR, __FILE__,__LINE__);
	}
	if (is_null($teamsizeInAllGames)) {
		$teamsizeInAllGames = true;
		$games = $liga->getGameStringArray();
		if (empty ($games)) {
			$teamsizeInAllGames = false;
		} else {
			foreach ($games AS $game) {
				if (!TournamentGameIncludeTeamsize($game))
					$teamsizeInAllGames = false;
			}
		}
	}
	return $teamsizeInAllGames;
}

/**
 * Pr&uuml;ft ob in einem Spielname die Turniergroesse enthalten ist
 * Es wird gepr&uuml;ft, ob in die Teamgr&ouml;&szlig;e (XonX) angegeben ist.
 * 
 * @since 1356 - 03.02.2007
 * @return integer Teamgr&ouml;&szlig;e bzw. false
 */
function TournamentGameIncludeTeamsize($game) {
	if (preg_match("/.*([\d]+)on([\d]+).*/", $game, $result))
		if ($result[1] == $result[2]) //Problem: "Bla 2on3 and 2on2 and 3on4", da nur erster Eintrag auf Gleichheit gepr&uuml;ft wird
			return $result[1];
	return false;
}

/**
 * Liest die Runde mit dem h&ouml;chsten Level im KO-Baum aus
 *
 * @access private
 * @param integer $tournamentid ID des Turnieres zu dem die erste Runde ausgelesen werden soll
 **/
function _TournamentGetFirstRound($tournamentid) {
	static $tournaments;
	if (!isset ($tournaments[$tournamentid])) {
		if (!is_posDigit($tournamentid)) {
			TournamentError(text_translate('Falscher Parameter'), E_USER_ERROR, __FILE__, __LINE__);
			return null;
		}
		$tournaments[$tournamentid] = MysqlReadField("SELECT level FROM `" . TblPrefix() . "flip_tournament_matches`
	                                                      WHERE tournament_id='$tournamentid'
	                                                        AND level<'" . TournamentGroupmatchestartlevel() . "'
	                                                      ORDER BY level DESC
	                                                     ", "level", true);
	}
	return $tournaments[$tournamentid];
}

/**
 * Gibt den Namen der Runde zur&uuml;ck, welche anhand einer Kommazahl angegeben wird
 * Dabei gilt folgendes f&uuml;r die Level:
 * 0 = OverallFinale (letztes Match)
 * 1 = Finale (nur DoubleElimination)
 * 2 = Halbfinale (WinnerBracket)
 * 0.75 = ConsolidationFinale im Loserbracket (Winner aus LB und Verlierer WB (Level 1))
 * 1.25 = Finale im Loserbracket
 * 1.75 = Gewinner aus 2.25 gegen Verlierer aus 2
 *
 * Alle weiteren Runden werden einfach hochgez&auml;hlt (1. Runde, 2. Runde, Halbfinale...)
 *
 * @param float $level Nummer Runde im Turnier
 * @param float $toplevel Lever der 1. Runde. Wenn nicht angegeben wird der des aktuellen Turnieres benutzt.
 * @return string Name der Runde
 **/
function TournamentLvl2txt($level, $tournamentid) {
	$toplevel = _TournamentGetFirstRound($tournamentid);
	switch ($level) {
		case "0" :
			return "Overall Finale";
		case "1" :
			return "Finale";
			//      case "0.25": return "Loserbracket Finale (Fehler?)"; break;
			//      case "0.75": return "Loserbracket Finale"; break;
		case "0.75" :
			return "Consolidation Finale";
		case "2" :
			return "Halbfinale";
		case "1.25" :
			return "Loserbracket Finale";
		case "1.75" :
			return "Loserbracket Halbfinale";
			//      case "1.25": return "Loserbracket Halbfinale"; break;
			//      case "1.75": return "Loserbracket Halbfinalgegner"; break;
			//      case "3": return "Viertelfinale"; break;
			//      case "2.25": return "Loserbracket Viertelfinale"; break;
			//      case "2.75": return "Loserbracket Viertelfinalgegner"; break;
			//      case "4": return "Achtelfinale"; break;
			//      case "3.25": return "Loserbracket Achtelfinale"; break;
			//      case "3.75": return "Loserbracket Achtelfinalgegner"; break;
		default :
			if ($level > TournamentGroupmatchestartlevel()) {
				return "Gruppe " . ($level -TournamentGroupmatchestartlevel());
			} else {
				if (($level * 100) % 100 == 0)
					return ($toplevel - $level +1) . ".&nbsp;Runde";
				else
					return ($toplevel - $level +0.25 + 1) . ".&nbsp;Runde Loserbracket";
			}
	}
}

/**
 * Turnierliste aufbereiten (Gruppieren, Turniername, Anzahl Teams)
 */
function TournamentGetTournamentDetails($list = false) {
	if ($list === false)
		$list = TournamentGetTournaments();
	if (!is_array($list))
		return array ();

	$tournaments = array ();
	$i = (integer) 0;

	foreach ($list AS $item) {
		/* Gruppierung */
		if (!empty ($item['group_id']))
			$class = $item['group_id'];
		elseif ($item['liga'] != '') $class = 'liga_' . $item['liga'];
		else
			$class = 'liga_Sonstige';

		/* vorhandene Daten */
		$tournaments[$class][$i] = $item;

		/* Spielart */
		$liga = TournamentGetLiga($item['liga']);
		$tournaments[$class][$i]['game'] = $liga->getGameString($item['game']);
		$tournaments[$class][$i]['gamelink'] = '<a href="tournament.php?frame=overview&amp;id=' . $item['id'] . '">' . $liga->getGameString($item['game']) . '</a>';
		$tournaments[$class][$i]['count'] = count(TournamentGetCombatants($item['id']));

		/* Zeit seit/bis Start */
		$tournaments[$class][$i]['timestr'] = TournamentGetStartString($item['start'], $item['status']);

		/*  weitere Turnierdaten */
		$tournaments[$class][$i]['status'] = TournamentGetStatusString($item['status'], $item['id']);
		$i++;
	}
	ksort($tournaments);
	return $tournaments;
}

/**
 * Liest alle teilnehmenden Teams eines Turnieres aus.
 * Weiterhin k&ouml;nnen die Namen der Teammitglieder ausgelesen werden.
 *
 * @param integer $tournamentid eine Turnier-ID
 * @param bool $withfreilos gibt an, ob auch Freilose zur&uuml;ckgegeben werden
 * @param bool $withuser gibt an ob zu den Teams auch die Mitglieder ausgelesen
 * werden sollen
 * @return array Alle Zeilen aus flip_tournament_combatant welche zu dem aktuellen bzw. $tournamentid Turnier geh&ouml;ren.
 **/
function TournamentGetCombatants($tournamentid, $withfreilos = false, $withuser = false) {
	if (!is_posDigit($tournamentid)) {
		trigger_error(text_translate('Falscher Parameter'), E_USER_ERROR);
		return null;
	}
	if (!$withfreilos)
		$withoutfree = ' AND c.team_id<>0';
	else
		$withoutfree = '';

	if ($withuser) {
		return MysqlReadArea("SELECT c.*, d.val AS name, t.name AS username 
				FROM " . TblPrefix() . "flip_tournament_combatant c, " . TblPrefix() . "flip_user_groups g, " . TblPrefix() . "flip_user_column m, " . TblPrefix() . "flip_user_data d, " . TblPrefix() . "flip_user_subject s, " . TblPrefix() . "flip_user_subject t
				WHERE c.tournament_id='$tournamentid'$withoutfree
				AND c.team_id=g.parent_id
				AND g.child_id=t.id
				AND c.team_id=d.subject_id
				AND d.column_id=m.id
				AND m.name='" . TournamentTeamNameColumn() . "'
				AND m.type='turnierteam'
				GROUP BY t.name
				ORDER BY c.team_id");
	} else {
		return MysqlReadArea('SELECT * FROM `' . TblPrefix() . 'flip_tournament_combatant` c WHERE c.tournament_id=\'' .escape_sqlData_without_quotes($tournamentid) . '\'' . $withoutfree, 'team_id');
	}
}

/**
 * Gibt die Startzeit in abh&auml;ngig des Abstandes zu jetzt zur&uuml;ck
 *
 * @param integer $time PHP-Timestamp
 * @param string $status Der Status wird dazu benutzt um festzustellen ob ein Turnier Versp&auml;tung hat.
 * @return string "Freitag, 13:37" oder "in 4 Std. 32 Min."
 **/
function TournamentGetStartString($time, $status) {
	static $LocalConfig = "calender_locale";
	$reltime = false;
	if ($time > time()) {
		//Zukunft
		if ($time > time() + 2 * 3600 * 24) {
			//mehr als 2 Tage: absolut
			foreach (explode(";", ConfigGet($LocalConfig)) as $locale)
				if (@ setlocale(LC_TIME, trim($locale)))
					break;
			$timestr = strftime("%A, %d.%m. - %H:%M", $time);
			//$timestr = date("D, H:i", $time);
		} else {
			//weniger als 2 Tage: relativ
			$time = $time -time();
			$timestr = "in ";
			$reltime = true;
		}
	} else {
		//Vergangenheit
		if ($time < time() - 3 * 3600 * 24) {
			//mehr als 3 Tage: absolut
			foreach (explode(";", ConfigGet($LocalConfig)) as $locale)
				if (@ setlocale(LC_TIME, trim($locale)))
					break;
			$timestr = strftime("%A, %d.%m. - %H:%M", $time);
			//$timestr = date("D, H:i", $time);
		} else {
			//weniger als 3 Tag: relativ
			$time = time() - $time;
			if ($status == "open" || $status == "start")
				$timestr = "Verz&ouml;gerung: ";
			else
				$timestr = "vor ";
			$reltime = true;
		}
	}

	if ($reltime) //relative Zeit
		{
		include_once ("mod/mod.time.php");
		$timestr .= FormatTimeDist($time);
	}

	return $timestr;
}

/**
 * converts the DB-status-enum strings into (german) descriptions
 *
 * Der Status "(grp)games" bekommt die aktuelle (weiteste) Runde angeh&auml;ngt,
 * der status "end" bekommt den Sieger angeh&auml;ngt.
 *
 * @param string $status Status welcher "&uuml;bersetzt" werden soll (closed, open, grpgames, games oder end)
 * @return string Beschreibung des Turnierstatuses
 **/
function TournamentGetStatusString($status, $tournamentid = false) {
	$statuses = TournamentStatuses();
	switch ($status) {
		case "grpgames" :
			// Rundenname auslesen
			$round = '';
			if (is_posDigit($tournamentid)) {
				$round = TournamentLvl2txt(MysqlReadField("SELECT MIN(level) AS level FROM " . TblPrefix() . "flip_tournament_matches
					                                                WHERE tournament_id='$tournamentid'
					                                                  AND team1 IS NOT NULL
					                                                  AND team2 IS NOT NULL
					                                               ", "level"), $tournamentid);
			}
			return $statuses[$status] . " ($round)";

		case "games" :
			// Rundenname auslesen
			if (!$tournamentid)
				$tournamentid = $this->id;
			$round = MysqlReadField("SELECT MIN(level) AS level FROM " . TblPrefix() . "flip_tournament_matches
				                                 WHERE tournament_id='$tournamentid'
				                                   AND team1 IS NOT NULL
				                                   AND team2 IS NOT NULL
				                                ", "level");
			$round = TournamentLvl2txt($round, $tournamentid);
			return $statuses[$status] . " ($round)";

		case "end" :
			if (!$tournamentid)
				$tournamentid = $this->id;
			$winner = TournamentCombatantID2Name(MysqlReadField("SELECT combatant_id FROM " . TblPrefix() . "flip_tournament_ranking
				                                                          WHERE rank='1'
				                                                            AND tournament_id='$tournamentid'
				                                                         ", "combatant_id"));
			return $statuses[$status] . " (1. " . $winner . ")";

		default :
			if (isset ($statuses[$status]))
				return $statuses[$status];
			else
				return text_translate('unbekannt');
	}
}

/**
 * Liefert ein Array mit allen definierten Coins oder wenn eine ID angegeben wird nur von diesem Turnier
 * 
 * @param integer $tid optional eine TurnierID
 * @return array Ohne ID ein zweidimensionales Array mit allen Spalten, ansonsten ein Array mit 'currency'=>'max' Zuweisungen
 */
function TournamentGetCoins($tid = false, $forceLoad = false) {
	static $cache = null;
	if(is_null($cache) || $forceLoad) {
		$cache = MysqlReadArea('SELECT * FROM `' . TblPrefix() . 'flip_tournament_coins`', 'id');
	}
	if (!$tid) {
		return $cache;
	} else {
		$coinId = MysqlReadFieldByID(TblPrefix() . 'flip_tournament_tournaments', 'coin_id', $tid);
		if(!isset($cache[$coinId])) {
			TournamentError('Ung&uuml;ltige CoinId f&uuml;r Turnier '.TournamentGetTournamentString($tid), E_USER_WARNING, __FILE__,__LINE__);
		}
		return $cache[$coinId];
	}
}

/**
 * Liest das verbleibende Guthaben eines Users aus.
 * @param mixed $uid Bezeichnung eines Users (@see core/core.subject.php, CreateSubjectInstance())
 * @return Array verbleibende Coins in allen "W&auml;hrungen" (array(coins=>1, currency=>"Coins")
 **/
function TournamentGetUserCoins($uid = false) {
	if (!$uid) {
		global $User;
		$u = $User;
	} else {
		$u = CreateSubjectInstance($uid);
	}

	$r = array();
	foreach (TournamentGetCoins() AS $coingroup)
		$r[$coingroup['id']] = array (
			'coins' => $coingroup['maxcoins'] - TournamentUserSpentCoins($u,
			$coingroup['id']
		), 'currency' => $coingroup['currency']);
	return $r;
}

/**
 * Liefert die Anzahl der verbrauchent Coins eines Benutzers
 * @param Object $u User-Objekt
 * @param int $coinid ID einer Coingruppe
 * @return int Anzahl der verbrauchten Coins
 */
function TournamentUserSpentCoins($u, $coinid) {
	if (!is_a($u, 'User')) {
		TournamentError('Paramteter muss vom Typ "User" sein!|$u='.var_export($u, true), E_USER_ERROR, __FILE__,__LINE__);
	}
	$teams = $u->getParents('turnierteam');
	return MysqlReadField('SELECT SUM(t.coins) AS coins FROM (`' . TblPrefix() . 'flip_tournament_combatant` c) LEFT JOIN `' . TblPrefix() . 'flip_tournament_ranking` r ON c.team_id=r.combatant_id AND c.tournament_id = r.tournament_id LEFT JOIN `' . TblPrefix() . 'flip_tournament_tournaments` t ON t.id = c.tournament_id 
		                           WHERE c.team_id IN (' . implode_sqlIn(array_keys($teams)) . ')
		                             AND c.team_id!=\'0\'
		                             AND r.rank IS NULL
		                             AND t.coin_id = \''.$coinid.'\'
		                          ', 'coins');
}


/**
 * Ein Turnier
 */
class Turniersystem extends SqlRowObject {
	//config
	var $playrightconfig = 'tournament_playright';
	var $u18CheckConfig = 'tournament_u18_check';

	//Content-Gruppe in der Screenshots abgelegt werden
	var $ContentGroup = 'Screenshots';

	//files
	var $xmlfile_NGL = 'NGLGames.xml';
	var $xmlfile_WWCL = 'gameini.xml';

	//internal
	var $types = array (
		'ko' => 'KO',
		/*'koLOL' => 'KOLOL',*/
		'group' => 'Gruppen',
		'dm' => 'Deathmatch'
/*		'points' => 'Punkte' */
	); //enum in flip_tournament_tournaments.type;
	var $treetypes = array (
		'default' => 'normal',
		'small' => 'kompakt',
		'narrow' => 'schmal'
	); //enum in flip_tournament_tournaments.treetype
	#from config:
	var $FirstRound;
	//configvalues
	var $losersubmit = 'N';
	var $playright;
	var $maxcoins = 0;
	var $currency = 'Coins';

	//properties
	var $id = null;
	var $Orga = false;
	var $Admin = false;
	var $matches = null;

	//php 7 public function __construct()
	//php 5 original function Turniersystem()
	function __construct($id = false) {
		//Config
		$this->playright = ConfigGet($this->playrightconfig);

		//check enum-data
		$this->types = _TournamentCheckEnumData(TblPrefix() . "flip_tournament_tournaments", "type", $this->types);
		$this->treetypes = _TournamentCheckEnumData(TblPrefix() . "flip_tournament_tournaments", "treetype", $this->treetypes);

		//load tournament-data
		if ($id)
			$this->_setTournament($id);

		//Admin
		if (TournamentIsAdmin())
			$this->Admin = true;
		else
			$this->Admin = false;
	}

	/**
	 * Legt Eigenschaften der Klasse fest.
	 *
	 * Dazu z&auml;hlen alle Tabellenspalten der
	 * Tabelle 'flip_tournament_tournaments' sowie Config-Einstellungen und sich
	 * daraus ergebnde Werte. Auch die Berechtigung (Admin/Orga) wird hier als
	 * Eigenschaft festgehalten.
	 *
	 * @access private
	 * @param integer $Id Die ID eines Turnieres
	 * @return bool immer 'true'
	 **/
	function _setTournament($Id) {
		if (!TournamentRequireValidID($Id))
			return;
		//Spalten von flip_tournament_tournaments als Eigenschaften zuweisen
		$data = array ();
		
		parent::__construct(TblPrefix() . "flip_tournament_tournaments", $Id, $data);
		//$this->funcSqlRowObject(TblPrefix() . "flip_tournament_tournaments", $Id, $data);
		
		// $this->groupname = MysqlReadFieldByID(TblPrefix()."flip_tournament_groups", "name", $this->group_id);
		$coins = TournamentGetCoins();
		$this->maxcoins = $coins[$this->coin_id]['maxcoins'];
		$this->currency = $coins[$this->coin_id]['currency'];
		
		require_once('mod/mod.tournament.liga.php');
		$this->liga = TournamentGetLiga($this->liga);

		// Turniertyp entscheidet &uuml;ber das Verhalten der Spiele 
		/* $filename = 'mod/mod.tournament.' . strtolower($this->type) . '.php'; */
                $filename = realpath(dirname(__FILE__)).'/mod.tournament.' . strtolower($this->type) . '.php';
		require_once ($filename);
		$class = ucfirst($this->type) . 'Matches';
		if(version_compare('5.0', phpversion(), '>')) {
			$class = strtolower($class);
		}
		if(!in_array($class, get_declared_classes())) {
			return TournamentError(text_translate('Es gibt kein Modul f&uuml;r den Turniertyp "' . $this->type . '"!|class='.$class), E_USER_ERROR, __FILE__, __LINE__);
		}
		$this->matches = new $class ();

		//Orga des Turniers?
		$this->Orga = TournamentIsOrga($this->id);

		//*einmal* das erste Level auslesen (f&uuml;r KO)
		if ($this->status != "open")
			$this->FirstRound = _TournamentGetFirstRound($Id);
		return true;
	}

	/**
	 * Schreibt einen Eintrag in den Action-Log und optional auch ins Changelog
	 *
	 * Vor dem Text wird "Turniersystem: " sowie der Name des Turnieres und dessen ID angegeben.
	 *
	 * @access private
	 * @param string $text Der Logtext
	 * @param bool $changelog Gibt an ob zus&auml;tzlich auch ins Changelog geloggt werden soll
	 * @return bool true
	 **/
	function _Log($text, $changelog = false) {
		$turnier = "";
		if (!empty ($this->id))
			$turnier = $this->GetTournamentString() . "-Turnier (ID " . $this->id . ") ";
		$msg = "Turniersystem: $turnier$text";
		if ($changelog)
			LogChange($msg);
		return LogAction($msg);
	}

	/**
	 * Liefert alle Orgas des Turnieres
	 *
	 * @return Array IDs und Namen der Orgas (ID=>Name)
	 */
	function getOrgas() {
		return MysqlReadCol('SELECT s.id, s.name FROM `'.TblPrefix().'flip_user_subject` s, `'.TblPrefix().'flip_user_right` r, `'.TblPrefix().'flip_user_rights` t WHERE t.controled_id=\''.GetSubjectID('Turniersubjekt'.$this->id).'\' AND r.right=\'tournament_orga\' AND s.id=t.owner_id AND r.id=t.right_id', 'name', 'id');
	}

	/**
	 * Gibt jemanden das Recht 'tournament_orga' &uuml;ber das aktuelle Turniersubjekt
	 * Nur ein Subjekt vom Typ User kann als Orga eingetragen werden!
	 *
	 * @param mixed $aUser irgendwas was einen User identifiziert (siehe CreateSubjectInstance)
	 * @return bool
	 **/
	function addOrga($aUser) {
		global $User;
		//if (!$this->Orga)
			$User->requireRight(TournamentAdminright());
		if (empty ($this->id))
			return TournamentError("Fehler beim hinzuf&uuml;gen eines Orgas: Es wurde kein Turnier angegeben!|User=$aUser, ID=".$this->id, E_USER_WARNING, __FILE__, __LINE__);
		$u = CreateSubjectInstance($aUser, "user");
		if ($r = _AddRight($u->id, "tournament_orga", "Turniersubjekt".$this->id))
			$this->_Log($u->name." wurde als Orga eingetragen.");
		return $r;
	}
	
	/**
	 * Liest das verbleibende Guthaben eines Users aus.
	 * @param mixed $uid Bezeichnung eines Users (siehe core.subject.php, CreateSubjectInstance())
	 * @return integer verbleibende Coins
	 **/
	function GetUserCoins($uid = false) {
		if (!$uid) {
			global $User;
			$u = $User;
		} else {
			$u = CreateSubjectInstance($uid);
		}

		return $this->maxcoins - TournamentUserSpentCoins($u, $this->coin_id);
	}

	/**
	 * Gibt einen Text zur&uuml;ck, welcher das Turnier beschreibt.
	 * Ausser dem Namen des Spieles und der Teamgr&ouml;&szlig;e (XonX) steht evtl. die Liga
	 * oder FUN vor dem Turniernamen.
	 * @param integer $tournamentid Die ID des Turnieres von dem man den Text haben m&ouml;chte
	 * @return string "[\[Gruppe\]] [LIGA] full gamename XonX" ([...] is optional)
	 **/
	function GetTournamentString() {
		if (empty ($this->id))
			return TournamentError("Fehler in GetTournamentString().| Es wurde keine TurnierID angegeben.", E_USER_WARNING, __FILE__, __LINE__);

		return TournamentGetTournamentString($this->id);
	}

	/**
	 * Gibt den vollen Namen zu einem Spielk&uuml;rzel zur&uuml;ck
	 *
	 * @param string $game Spielk&uuml;rzel (max. 4 Zeichen)
	 * @return string ausgeschriebener Name des Spieles
	 **/
	function GetGameString() {
		return $this->liga->getGameString($this->game);
	}

	/**
	 * converts the DB-status-enum strings into (german) descriptions
	 **/
	function GetStatusString() {
		return TournamentGetStatusString($this->status, $this->id);
	}

	/**
	 * &auml;ndert den Status in der DB und im Objekt und erstellt einen Logeintrag
	 *
	 * @param string $status Name des Statuses welcher gesetzt werden soll
	 * @return bool true/false wenn Fehler beim Schreiben in die DB auftreten
	 **/
	function setStatus($status) {
		if (MysqlWriteByID(TblPrefix().'flip_tournament_tournaments', array ('status' => $status), $this->id)) {
			$this->_Log('Status von '.$this->status." auf $status gesetzt.");
			$this->status = $status;
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Gibt die Art des Turnieres zur&uuml;ck (Gruppen, Single- oder Doubleelimination)
	 * Die Parameter sind optional, es wird dann der Typ des aktuellen Turnieres genutzt.
	 *
	 * @param string $type Typ des Turnieres (group oder ko)
	 * @param string $looser gibt es ein Loserbracket (Y/N)
	 * @return string Art des Turnieres
	 **/
	function GetTypeString($type = false, $looser = false) {
		if (!$type)
			$type = $this->type;
		if (!$looser)
			$looser = $this->looser;

		if ($type == 'ko')
			if ($looser == "Y") return "Double Elimination";
			else return "Single Elimination";
		if(isset($this->types[$type]))
			return $this->types[$type];
		return "unbekannt";
	}

	/**
	 * Gibt die Startzeit in abh&auml;ngig des Abstandes zu jetzt zur&uuml;ck
	 *
	 * @param integer $time PHP-Timestamp
	 * @param string $status Der Status wird dazu benutzt um festzustellen ob ein Turnier Versp&auml;tung hat.
	 * @return string "Freitag, 13:37" oder "in 4 Std. 32 Min."
	 **/
	function GetStartString() {
		$time = $this->start;
		$status = $this->status;

		return TournamentGetStartString($time, $status);
	}

	/**
	 * Gibt das Datum und die relative Zeit zum Turnierstart an
	 *
	 * @param integer $time PHP-Timestamp
	 * @param string $status Der Status wird dazu benutzt um festzustellen ob ein Turnier Versp&auml;tung hat.
	 * @return string "16.04.2007 13:52 (in 3 Tagen 4 Std. 32 Min.)"
	 **/
	function GetFullStartString($time = false, $status = false) {
		if (!$time)
			$time = $this->start;
		if (!$status)
			$status = $this->status;
		$date = date("d.m.Y H:i", $time);
		if ($time > time()) {
			$time = ($time -time()) / 60;
			$timestr = "in ";
		} else {
			$time = (time() - $time) / 60;
			if ($status == "open" || $status == "start")
				$timestr = "Verz&ouml;gerung: ";
			else
				$timestr = "vor ";
		}
		$time_hr = floor($time / 60 / 24);
		$time_std = floor(($time % (60 * 24)) / 60);
		$time_min = $time % 60;
		if ($time_hr > 1)
			$timestr = $timestr . $time_hr . " Tagen ";
		elseif ($time_hr == 1) $timestr = $timestr . $time_hr . " Tag ";
		if ($time_std > 0)
			$timestr = $timestr . $time_std . " Std. ";
		return $date . " (" . $timestr . $time_min . " Min.)";
	}

	/**
	 * Gibt alle Daten des Turniers (auch formatiert) als Array zur&uuml;ck
	 * @param int $tournament_id optionale TurnierID
	 * @return Array assoziatives Array der Turnierdaten bzw. leeres Array
	 */
	function GetTournamentDetail($tournament_id = false) {
		if (!$tournament_id)
			$tournament_id = $this->id;

		$details = TournamentGetTournamentDetails(array (
			TournamentGetTournament($tournament_id
		)));

		$r = array_shift(array_shift($details));
		if (is_array($r))
			return $r;
		else
			return array ();
	}

	/**
	 * Liest die Datenbankspalten aller Matches eines Turnieres aus und sortiert diese nach der Runde und Position in der Runde
	 * Mit Angabe einer MatchID wird nur jenes Match zur&uuml;ckgegeben.
	 *
	 * @param integer $matchid Wenn angegeben werden nur die Daten des Matches zur&uuml;ckgegeben, sonst alle
	 * @param integer $tournamentid Wenn angegeben werden die Matches des angegebenen Turnieres zur&uuml;ckgegeben, sonst die des aktuellen ($_GET["id"])
	 * @param bool $clear Gibt an ob der Cache gel&ouml;scht werden soll um die Daten neu aus der DB zu lesen
	 * @return array Ein nummerisches Array welches f&uuml;r jedes Match ein Array mit den Spalten der Datenbank enth&auml;lt. Falls $matchid angegeben wird, wird ein eindimensionales Array zur&uuml;ckgegeben
	 **/
	function GetMatches($matchid = false, $tournamentid = false, $clear = false) {
		if (!$tournamentid)
			$tournamentid = $this->id;
		if (empty ($tournamentid))
			return TournamentError("Fehler in GetMatches().|Es wurde keine TurnierID angegeben.", E_USER_WARNING, __FILE__, __LINE__);
		static $Matches;
		//Cache l&ouml;schen
		if ($clear) {
			$Matches = array ();
		}
		//Spiele eines Turnieres auslesen
		if (empty ($Matches[$tournamentid])) {
			$Matches[$tournamentid] = MysqlReadArea("SELECT * FROM " . TblPrefix() . "flip_tournament_matches
				                                               WHERE tournament_id='$tournamentid'
				                                               ORDER BY `level` DESC, levelid
				                                              ", "id");
		}
		//ein Match zur&uuml;ckgeben
		if ($matchid) {
			return $Matches[$tournamentid][$matchid];
		}
		//alle Matches eines Turnieres zur&uuml;ckgeben
		else {
			return $Matches[$tournamentid];
		}
	}

	/**
	 * Liefert die Platzierungen eines Turnieres sortiert zur&uuml;ck
	 *
	 * @param integer $tournamentid Optional kann ein Turnier angegeben werden, standardm&auml;&szlig;ig wird das aktuelle genommen ($_GET["id"])
	 * @return array alle Platzierungen des Turnieres aufsteigend sortiert
	 **/
	function GetResults($tournamentid = false) {
		if (!$tournamentid)
			$tournamentid = $this->id;
		return MysqlReadArea("SELECT * FROM " . TblPrefix() . "flip_tournament_ranking
										WHERE tournament_id='$tournamentid'
										  AND combatant_id!='0'
										  AND rank IS NOT NULL
										ORDER BY rank ASC");
	}

	/**
	 * Liefert Anfragen von Spielern und Teams zur&uuml;ck
	 * Je nachdem ob die $subjectid die eines Spielers oder Teams ist,
	 * werden die anfragenden Teams bzw. Spieler angezeigt.
	 * Somit k&ouml;nnen alle Spieler die Anfragen an ein Team gestellt haben oder
	 * alle Teams die einen Spieler eingeladen haben aufgelistet werden.
	 * z.B. die Anfragen von Team 1303 im Turnier 6:
	 * <code>
	 * print_r(GetAsking(1303, 6));
	 *
	 * Array
	 * (
	 *     [1304] => Array
	 *         (
	 *             [id] => 1
	 *             [mtime] => 20041022132831
	 *             [from] => user
	 *             [tournaments_id] => 6
	 *             [user_id] => 1304
	 *             [team_id] => 1303
	 *         )
	 * 
	 * )
	 * </code>
	 *
	 * @param integer $subjectid Das Subjekt zu dem die Anfragen gesucht werden
	 * @param integer $tournamentid optional kann ein Turnier angegeben werden, standardm&auml;&szlig;ig wird das aktuelle genommen ($_GET["id"])
	 * @return array Spalten aus der DB der anfragenden Subjekte
	 **/
	function GetAsking($subjectid, $tournamentid = false) {
		if (!$tournamentid)
			$tournamentid = $this->id;
		$s = new Subject($subjectid);
		if ($s->type == "turnierteam")
			$from = "user";
		else
			$from = "team";
		return $this->_GetInvitation($tournamentid, $s->id, $from, $from . "_id");
	}

	/**
	 * &Auml;hnlich wie GetAsking(), gibt aber die angefragten Teams bzw. Spieler zur&uuml;ck
	 *
	 * @param integer $subjectid Das Subjekt welches die Anfragen gestellt hat
	 * @param integer $tournamentid optional kann ein Turnier angegeben werden, standardm&auml;&szlig;ig wird das aktuelle genommen ($_GET["id"])
	 * @return array Spalten aus der DB der angefragten Subjekte
	 **/
	function GetAsked($subjectid, $tournamentid = false) {
		if (!$tournamentid)
			$tournamentid = $this->id;
		$s = new Subject($subjectid);
		if ($s->type != "turnierteam") {
			$from = "user";
			$sort = "team_id";
		} else {
			$from = "team";
			$sort = "user_id";
		}
		return $this->_GetInvitation($tournamentid, $s->id, $from, $sort);
	}

	/**
	 * wird von GetAsking() und GetAsked() benutzt
	 *
	 * @access private
	 * @param integer $tournamentid TurnierID
	 * @param integer $sid SubjektID
	 * @param string $from von wem die Einladung ausgeht
	 * @param string $sort Feld welches MysqlReadArea als Key benutzen soll
	 * @return array
	 **/
	function _GetInvitation($tournamentid, $sid, $from, $sort = "id") {
		return MysqlReadArea("SELECT * FROM " . TblPrefix() . "flip_tournament_invite
			                          WHERE (team_id='$sid' OR user_id='$sid')
			                            AND `from`='$from'
			                            AND tournaments_id='$tournamentid'
			                         ", $sort);
	}

	/**
	 * Liest alle teilnehmenden Teams eines Turnieres aus.
	 * Optional kann auch nur ein bestimmtes Team ausgelesen werden.
	 * Weiterhin k&ouml;nnen die Namen der Teammitglieder ausgelsen werden.
	 *
	 * @param bool $withfreilos gibt an, ob auch Freilose zur&uuml;ckgegeben werden
	 * @param integer $tournamentid optional kann ein Turnier angegeben werden, standardm&auml;&szlig;ig wird das aktuelle genommen ($_GET["id"])
	 * @param integer $teamid gibt an ob nur ein bestimmtes Team ausgelesen werden soll
	 * @param bool $withuser gibt an ob zu den Teams auch die Mitglieder ausgelesen werden sollen
	 * @return array Alle Zeilen aus flip_tournament_combatant welche zu dem aktuellen bzw. $tournamentid Turnier geh&ouml;ren.
	 **/
	function GetCombatants($withfreilos = false, $withuser = false) {
		return TournamentGetCombatants($this->id, $withfreilos, $withuser);
	}

	/**
	 * F&uuml;gt ein Team einem Turnier hinzu
	 *
	 * @param integer $teamid ID des Turnierteams (Subjekt)
	 * @param integer $tournamentid optional kann ein Turnier angegeben werden, standardm&auml;&szlig;ig wird das aktuelle genommen ($_GET["id"])
	 * @return integer Die ID des neuen Datensatzes oder false, wenn ein Fehler auftritt
	 **/
	function AddCombatant($teamid, $tournamentid = false) {
		if (!$tournamentid)
			$tournamentid = $this->id;
		$turnier = " dem " . TournamentGetTournamentString($tournamentid) . "-Turnier (ID $tournamentid)";
		$combatants = TournamentGetCombatants($tournamentid);
		$count = count($combatants);
		if ($count < $this->maximum || $teamid == "0") {
			//Name bereits vorhanden?
			$teamname = TournamentCombatantID2Name($teamid);
			foreach ($combatants AS $aCombatant) {
				if ($teamname == TournamentCombatantID2Name($aCombatant["team_id"])) {
					return TournamentError("Ein Team mit diesem Namen nimmt bereits am Turnier teil!|name=$teamname, teamid=$teamid", E_USER_ERROR);
				}
			}

			if ($r = MysqlWriteByID(TblPrefix() . "flip_tournament_combatant", array (
					"team_id" => $teamid,
					"tournament_id" => $tournamentid
				))) {
				$this->_Log(TournamentCombatantID2Name($teamid) . " wurde$turnier hinzugef&uuml;gt.");
				return $r;
			} else {
				return TournamentError("Ein Team konnte dem Turnier nicht hinzugef&uuml;gt werden|TeamID=$teamid, TurnierID=$tournamentid", E_USER_ERROR, __FILE__, __LINE__);
			}
		} else {
			return TournamentError("Das Maximum an Teilnehmern ist bereits erreicht.", E_USER_ERROR, __FILE__, __LINE__);
		}
	}

	/**
	 * Best&auml;tigung einer Einladung und somit das hinzuf&uuml;gen eines Spielers zu einem Team
	 *
	 * @param integer $inviteid Die ID der Einladung
	 * @return integer bei erfolg, Die ID der Verkn&uuml;pfung, ansonsten false.
	 **/
	function ConfirmCombatant($inviteid) {
		global $User;
		if (empty ($inviteid))
			return TournamentError("Fehlender Parameter f&uuml;r Teilnahme.|in Turniersystem::ConfirmCombatant()", E_USER_WARNING, __FILE__, __LINE__);
		if (!($data = MysqlReadRowByID(TblPrefix() . "flip_tournament_invite", $inviteid, true)))
			return TournamentError("Einladung nicht g&uuml;ltig.|ID:$inviteid", E_USER_WARNING, __FILE__, __LINE__);

		$combatant = CreateSubjectInstance($data["team_id"], "turnierteam");
		$playercount = $combatant->CountTeam();
		$fixedsize = $combatant->getProperty("fixed_size");
		$tournamentsizes = MysqlReadCol("SELECT id, teamsize FROM " . TblPrefix() . "flip_tournament_tournaments", "teamsize", "id");

		foreach (MysqlReadArea("SELECT tournament_id AS id FROM " . TblPrefix() . "flip_tournament_combatant WHERE team_id='" . $data["team_id"] . "'") AS $atournament) {
			//team voll?
			if ($playercount >= $tournamentsizes[$atournament["id"]])
				return TournamentError("Das Team ist bereits voll!", E_USER_WARNING, __FILE__, __LINE__);
			//team in anderen Turnier voll?
			if ($fixedsize > 1 && $tournamentsizes[$atournament["id"]] < $fixedsize)
				return TournamentError("Das Team hat bereits die maximale Mitgliederzahl (z.B. wenn ein 2on2-Team in einem 5on5 mitspielt).", E_USER_WARNING, __FILE__, __LINE__);
			//spielt bereits mit?
			if ($this->isPlayer($data["user_id"], $atournament["id"])) {
				$u = CreateSubjectInstance($data["user_id"]);
				return TournamentError($u->name . " ist bereits Teilnehmer (\"" . TournamentGetTournamentString($atournament["id"]) . "\").", E_USER_WARNING);
			}
			//hat noch Guthaben?
			if ($this->GetUserCoins($data['user_id']) < $this->coins)
				return TournamentError('User hat nicht genug ' . $this->currency . ' um an diesem Turnier teilzunehmen.', E_USER_WARNING, __FILE__, __LINE__);
		}
		//check rights
		if ($data["from"] == "team") {
			if ($User->id != $data["user_id"])
				return TournamentError("addPlayer: Keine Berechtigung. Nur der Spieler selbst kann die Teilnahme best&auml;tigen.", E_USER_WARNING, __FILE__, __LINE__);
		} else {
			if (!$User->isChildOf($data["team_id"]) && !$this->Orga)
				return TournamentError("addPlayer: Keine Berechtigung. Nur Gruppenmitglieder k&ouml;nnen die Teilnahme best&auml;tigen.", E_USER_WARNING, __FILE__, __LINE__);
		}
		//add user to team
		if ($User->id == $data["user_id"]) {
			$r = $User->addParent($data["team_id"]);
		} else {
			$invited = new User($data["user_id"]);
			$r = $invited->addParent($data["team_id"]);
		}
		//delete invitations for this user and this tournament
		if ($r) {
			MysqlExecQuery("DELETE FROM " . TblPrefix() . "flip_tournament_invite
						                  WHERE user_id='" . $data["user_id"] . "'
						                    AND tournaments_id='" . $data["tournaments_id"] . "'
						                 ", "SQL-Fehler beim l&ouml;schen in " . basename(__FILE__) . " line " . __LINE__ . "<br>\n");
			if ($playercount +1 == $this->teamsize) //Team jetzt voll
				MysqlExecQuery("DELETE FROM " . TblPrefix() . "flip_tournament_invite
							                    WHERE team_id='" . $data["team_id"] . "'
							                      AND tournaments_id='" . $data["tournaments_id"] . "'
							                   ", "SQL-Fehler beim l&ouml;schen in " . basename(__FILE__) . " line " . __LINE__ . "<br>\n");
		}
		return $r;
	}

	/**
	 * Entfernt ein Team aus einem Turnier
	 * Dabei wird das Team aus der Teilnehmerliste gel&ouml;scht und
	 * alle Spielpaarungen und Platzierung durch ein Freilos ersetzt!
	 *
	 * @param integer $teamid ID des zu entfernenden Teams
	 * @param integer $tournamentid optional kann ein Turnier angegeben werden, standardm&auml;&szlig;ig wird das aktuelle genommen ($_GET["id"])
	 * @return bool false
	 **/
	function RemoveCombatant($teamid, $tournamentid = false) {
		global $User;
		if (!$tournamentid)
			$tournamentid = $this->id;
		if ($teamid == 0 || empty ($tournamentid))
			return false;
		$name = TournamentCombatantID2Name($teamid);
		if ($User->isChildOf($teamid) || $this->Orga) {
			if (MysqlDeleteRow("DELETE FROM " . TblPrefix() . "flip_tournament_combatant WHERE team_id='$teamid' AND tournament_id='$tournamentid'")) {
				MysqlWrite("UPDATE " . TblPrefix() . "flip_tournament_matches SET team1='0' WHERE team1='$teamid' AND tournament_id='$tournamentid'");
				MysqlWrite("UPDATE " . TblPrefix() . "flip_tournament_matches SET team2='0' WHERE team2='$teamid' AND tournament_id='$tournamentid'");
				MysqlWrite("UPDATE " . TblPrefix() . "flip_tournament_ranking SET combatant_id='0' WHERE combatant_id='$teamid' AND tournament_id='$tournamentid'");
				return TournamentError("Team " . escapeHtml($name) . " wurde aus dem Turnier genommen.");
			} else {
				return TournamentError("Es ist ein Fehler beim L&ouml;schen aufgetreten!|(" . mysqli_error() . ")", E_USER_WARNING, __FILE__, __LINE__);
			}
		}
		return TournamentError("Du bist nicht berechtig diese Aktion durchzuf&uuml;hren.|RemoveCombatant($teamid)", E_USER_WARNING, __FILE__, __LINE__);
	}

	/**
	 * L&ouml;scht ein Team
	 * Dabei wird das Team aus der Teilnehmerliste gel&ouml;scht und
	 * alle Spielpaarungen und Platzierung durch ein Freilos ersetzt!
	 * Das Subjekt wird komplett gel&ouml;scht!
	 *
	 * @param integer $teamid ID des zu l&ouml;schenden Teams
	 * @return bool false
	 **/
	function DeleteCombatant($teamid) {
		if (empty ($teamid))
			return false; //Freilos kann nicht gel&ouml;scht werden (ID=0)
		global $User;
		if ($User->isChildOf($teamid) || $this->Admin) {
			$teamname = TournamentCombatantID2Name($teamid);
			if (DeleteSubject($teamid)) {
				$this->_Log("Das Team \"$teamname\" wurde von " . $User->name . " gel&ouml;scht.");
				return TournamentError("Team wurde gel&ouml;scht.");
			} else {
				return TournamentError("Es ist ein Fehler beim L&ouml;schen aufgetreten!|ID=$teamid, mysqli_error(" . mysqli_error() . ")", E_USER_WARNING, __FILE__, __LINE__);
			}
		} else {
			return TournamentError("Du bist nicht berechtigt diese Aktion durchzuf&uuml;hren.", E_USER_WARNING, __FILE__, __LINE__);
		}
	}

	/**
	 * Sendet eine Message mit den angegeben Daten als Variablen an alle
	 * Mitglieder eines Teams
	 * 
	 * @access private
	 * @param String $messagename Name der (System-)nachricht (sendmessage.php?
	 * type=sys)
	 * @param int $teamid ID des Teams an welche die Nachricht geschickt werden
	 * soll
	 * @return bool true, wenn das verschicken fehlerfrei war
	 */
	function _webmessage($messagename, $teamid, $msg_vars = array ()) {
		if (empty ($teamid))
			return true; //skip Freilos
		require_once ("mod/mod.sendmessage.php");

		$message = new ExecMessage();
		$message->messageFromDB($messagename);
		$team = CreateSubjectInstance($teamid, "turnierteam");
		//join team-, tournament- and custom-data
		$msg_data = array (
			"tournament_name" => TournamentGetTournamentString($this->id
		));
		$msg_data = array_merge($msg_data, $team->getProperties());
		$msg_data = array_merge($msg_data, $this->getTournamentDetail($this->id));
		$msg_data = array_merge($msg_data, $msg_vars);
		//send message to teammembers
		foreach ($team->members() AS $uid => $udata) {
			// $udata = array("userid" => UID, "nickname" => <nickname>)
			$message->Params = array_merge($udata, $msg_data);
			$r[] = $message->sendMessage(CreateSubjectInstance($uid, "user"));
		}
		//check if sending was successful
		foreach ($r AS $ok) {
			if (!$ok)
				return false;
		}

		LogAction("Es wurden Webmessages '$messagename' an das Team '" . $team->name . "' gesendet.");
		return true;
	}

	/**
	 * Teilt den Teams mit, dass ein Match statt finden kann
	 * @param Array $match Daten des Matches welches zu bestreiten ist (Zeile
	 * aus flip_tournament_matches)
	 * @return bool false bei Fehler
	 */
	function readyUp($match) {
		$r = true;
		if(!is_a($match, 'TournamentMatch') && is_posDigit($match)) {
			$match = new TournamentMatch($match);
		}

		if (is_a($match, "TournamentMatch")) {
			//both teams must be set
			if (!empty ($match->team1) && !empty ($match->team2)) {
				$link = ServerURL() . EditUrl(array (
					"id" => $this->id,
					"action" => "ready",
					"matchid" => $match->id,
					"frame" => "tree"
				), "", false);
				$link = str_replace("turnieradm.php", "tournament.php", $link);
				$r1 = $this->_webmessage("tournament_matchready", $match->team1, array (
					"link" => $link,
					"opponent" => html_entity_decode(TournamentCombatantID2Name($match->team2
				))));
				$r2 = $this->_webmessage("tournament_matchready", $match->team2, array (
					"link" => $link,
					"opponent" => html_entity_decode(TournamentCombatantID2Name($match->team1
				))));
				$r = ($r1 && $r2) ? true : false;
			}
		} else {
			$r = TournamentError("keine Daten fuer Readymessage.|readyUp($match) hat Daten vom Typ: " . gettype($match), E_USER_ERROR, __FILE__, __LINE__);
		}

		return $r;
	}

	/**
	 * Pr&uuml;ft, ob ein User an einem Turnier teilnehmen darf und gibt einen entsprechenden Fehlertext zur&uuml;ck
	 * 
	 * @param integer $tid optionale TurnierID
	 * @return String Text warum die Teilnahme verwehrt wird oder false, wenn Teilnahme m&ouml;glich ist
	 */
	function UserNotJoinText($tid = false) {
		if (!$tid)
			$tid = $this->id;
		global $User;
		//Status = Anmeldung?
		if ($this->status == "open") {
			//bereits Teilnehmer?
			if (!$this->isPlayer($User->id)) {
				//Turnierberechtigt?
				if ($User->hasRight($this->playright)) {
					//&uuml;18?
					if (ConfigGet($this->u18CheckConfig) == "seat") {
						include_once ("mod/mod.seats.php");
						$isAdult = SeatsIsAdult($User);
					} else {
						$isAdult = ($User->getProperty("is_adult") == "Y");
					}
					if (!($this->u18 == "N" && !$isAdult)) {
						//Coins
						if ($this->GetUserCoins() >= $this->coins) {
							//Turnier voll?
							if (!$this->isFull($tid)) {
								return false;
							} else {
								return text_translate("Die maximale Teilnehmerzahl ist erreicht.");
							}
						} else {
							return text_translate("Du hast nicht genug ") . $this->currency . ".";
						}
					} else {
						return text_translate("Dieses Turnier ist nur f&uuml;r vollj&auml;hrige.");
					}
				} else {
					return text_translate("Du bist nicht berechtigt an Turnieren teilzunehmen.");
				}
			} else {
				return text_translate("Du bist bereits Teilnehmer.");
			}
		} else {
			return text_translate("Die Anmeldung ist geschlossen.");
		}
	}

	function isFull($tournamentid = false) {
		if ($tournamentid && $tournamentid != $this->id) {
			$t = new Turniersystem($tournamentid);
		} else {
			$t = $this;
		}
		if (count($t->GetCombatants()) >= $t->maximum)
			return true;
		else
			return false;
	}

	function isPlayer($userid = false, $tournamentid = false) {
		if (!$tournamentid)
			$tournamentid = $this->id;
		if (!$userid) {
			global $User;
			$userid = $User->id;
		}
		if (MysqlReadRow("SELECT g.id FROM " . TblPrefix() . "flip_user_groups g, " . TblPrefix() . "flip_tournament_combatant c
			                     WHERE c.tournament_id='$tournamentid'
			                       AND c.team_id=g.parent_id
			                       AND g.child_id='$userid'
			                    ", true))
			return true;
		else
			return false;
	}

	/**
	 * tr&auml;gt f&uuml;r alle Freilose Defaultloss ein
	 * @access private
	 * @return integer Anzahl Matches mit Freilosen ohne Ergebnis
	 **/
	function _PlayFreeGames() {
		//In der Aufw&auml;rmphase (Seeding) keine Freilose eintragen
		if ($this->status == 'start')
			return false;
		do {
			$freematches = MysqlReadCol('SELECT `id` FROM `' . TblPrefix() . 'flip_tournament_matches`
					                                  WHERE ( (team1=\'0\' AND team2 IS NOT NULL AND points2 IS NULL) OR (team2=\'0\' AND team1 IS NOT NULL AND points1 IS NULL) )
					                                    AND tournament_id=\'' . $this->id . '\'
					                                  GROUP BY id', 'id');
			foreach ($freematches AS $nullmatchid) {
				$nullmatch = new TournamentMatch($nullmatchid);
				if ($nullmatch->team1 == '0') {
					$points1 = '0';
					$points2 = '1';
				}
				elseif ($nullmatch->team2 == '0') {
					$points1 = '1';
					$points2 = '0';
				} else {
					//should never be executed
					continue;
				}
				if ($this->type == 'group')
					$comment = '';
				else
					$comment = 'Defaultwin';

				$this->InsertScore($nullmatch, $points1, $points2, $comment, true);
			}
		} while (0 < MysqlReadField('SELECT COUNT(*) FROM `' . TblPrefix() . 'flip_tournament_matches`
					                           WHERE ( (team1=\'0\' AND team2 IS NOT NULL AND points1 IS NULL) OR (team2=\'0\' AND team1 IS NOT NULL AND points2 IS NULL) )
					                             AND tournament_id=\'' . $this->id . '\''
					                          , 'COUNT(*)'));
	}

	/**
	 * Ergebnis eines Matches eintragen
	 * allgemeines hier, spezifisches in <type>Matches::setScore()
	 */
	function InsertScore(&$match, $points1 = false, $points2 = false, $comment = "", $freecall = false) {
		global $User;

		/* &uuml;berpr&uuml;fungen */
		if ($this->status != 'games' && $this->status != 'grpgames' && $this->status != 'start') {
			TournamentError('Laut Status werden Spiele nicht ausgetragen, deshalb kann z.Z. kein Ergebnis eingetragen werden.', E_USER_ERROR, __FILE__, __LINE__);
			return false;
		}
		if (!(is_a($match, 'TournamentMatch') OR $points1 !== false OR $points2 !== false) OR empty ($match->id) OR $points1 == "" OR $points2 == "")
			return TournamentError('Es wurden nicht alle Ergebnisdaten angegeben!|match=' . gettype($match) . " points1='$points1' points2='$points2'", E_USER_ERROR, __FILE__, __LINE__);
		$points1 = (integer) $points1;
		$points2 = (integer) $points2;

		if ($match->points1 != '' && !$this->Orga)
			TournamentError('Es existiert bereits ein Ergebnis!', E_USER_ERROR, __FILE__, __LINE__);

		//noch in der Zeit?
		if (!$match->inTime() && !$this->Orga)
			return TournamentError('Die Zeit f&uuml;r diese Runde ist bereits abgelaufen.<br />Nur Turnierorgas k&ouml;nnen dann noch Ergebnisse eintragen.', E_USER_ERROR, __FILE__, __LINE__);

		if (!is_object($this->matches)) {
			return TournamentError(text_translate('Es gibt kein Modul f&uuml;r den Turniertyp "' . $this->type . '"!'), E_USER_ERROR);
		}

		// Aufruf der Funktion zum setzen des Ergebnisses
		$done = $this->matches->setScore($this, $match, $points1, $points2, $comment, $freecall);
		
		$this->_Log($User->name . " hat " . TournamentCombatantID2Name($match->team1) . " $points1:$points2 " . TournamentCombatantID2Name($match->team2) . " [MatchID " . $match->id . "] eingetragen. ($done)");

		if (!$freecall) {
			if(!$this->type == 'points') // FIXME sollte eleganter moeglich sein! Evtl nach Typen verschieben?
				/* Freilose spielen */
				$this->_PlayFreeGames();

			$this->GetMatches($match->id, $match->tournament_id, true); //load new match into GetMatches()-Cache
			TournamentError($done);
		}
		return true;
	}

	function DeleteScore($matchid = false) {
		global $User;
		if (!$this->Orga)
			return TournamentError("Du bist nicht berechtigt Ergebnisse zu l&ouml;schen.", E_USER_ERROR, __FILE__, __LINE__);
		if ($matchid === false)
			return TournamentError("Fehler in DeleteScore().|MatchID wurde nicht &uuml;bergeben!", E_USER_WARNING, __FILE__, __LINE__);

		// Daten des aktuellen Spiels auslesen
		$match = $this->GetMatches($matchid);

		/* Pr&uuml;fungen f&uuml;r KO-Spiel */
		if ($this->status == "games" || $this->status == "start") {
			//n&auml;chste Runde ermitteln
			$newlevel = $this->getnextLevel($match["level"]);

			// n&auml;chste Runde pr&uuml;fen
			if ($this->isRoundplayed($newlevel['winner'], $match['team1'], $match['team2']) || ($newlevel['looser'] && $this->isRoundplayed($newlevel['looser'], $match['team1'], $match['team2'])))
				return TournamentError('n&auml;chstes Match wurde schon gespielt. Dieses muss zuerst zur&uuml;ckgesetzt werden.|nextlevel_w=' . $newlevel['winner'] . ', nextlevel_l=' . $newlevel['looser'], E_USER_WARNING, __FILE__, __LINE__);

			//offene Spiele (oder mit Freilosen) der n&auml;chsten Runde auslesen (von Teams aus dem zu l&ouml;schenden Spiel)
			$notplayed = MysqlReadArea("SELECT * FROM " . TblPrefix() . "flip_tournament_matches
											        WHERE tournament_id='" . $match["tournament_id"] . "'
											        AND (level='" . $newlevel["winner"] . "' OR level='" . $newlevel["looser"] . "')
											        AND (team1='" . $match["team1"] . "' OR team1='" . $match["team2"] . "' OR team2='" . $match["team1"] . "' OR team2='" . $match["team2"] . "')
											        AND (points1 IS NULL OR points1='' OR team1='0' OR team2='0')
											        ");
			foreach ($notplayed AS $nextmatch) {
				//Bei Freilosen auch &uuml;bern&auml;chstes Match ber&uuml;cksichtigen
				if ($nextmatch["team1"] == "0" || $nextmatch["team2"] == "0") {
					$nextlevel = $this->getnextLevel($nextmatch["level"]);
					if ($this->isRoundplayed($nextlevel["winner"], $match["team1"], $match["team2"]) || (isset ($nextlevel["looser"]) && $this->isRoundplayed($nextlevel["looser"], $match["team1"], $match["team2"])))
						return TournamentError("&uuml;bern&auml;chstes Match wurde schon gespielt. Dieses muss zuerst zur&uuml;ckgesetzt werden.", E_USER_NOTICE, __FILE__, __LINE__);
					$nextnotplayed = MysqlReadArea("SELECT * FROM " . TblPrefix() . "flip_tournament_matches
															            WHERE tournament_id='" . $nextmatch["tournament_id"] . "'
															            AND (level='" . $nextlevel["winner"] . "' OR level='" . $nextlevel["looser"] . "')
															            AND (team1='" . $nextmatch["team1"] . "' OR team1='" . $nextmatch["team2"] . "' OR team2='" . $nextmatch["team1"] . "' OR team2='" . $nextmatch["team2"] . "')
															            AND (points1 IS NULL OR points1='')
															            ");
					foreach ($nextnotplayed AS $nextnextmatch) {
						if ($nextnextmatch["team1"] == "0")
							$team = "team2";
						else
							$team = "team1";
						MysqlWriteByID(TblPrefix() . "flip_tournament_matches", array (
							$team => NULL,
							"points1" => NULL,
							"points2" => NULL
						), $nextnextmatch["id"]);
					}
				}
				//Spieler aus dem n&auml;chsten Match nehmen
				if ($nextmatch["team1"] == $match["team1"] || $nextmatch["team1"] == $match["team2"])
					$team = "team1";
				else
					$team = "team2";
				MysqlWriteByID(TblPrefix() . "flip_tournament_matches", array (
					$team => NULL,
					"points1" => NULL,
					"points2" => NULL
				), $nextmatch["id"]);
				//n&auml;chste Runde Finale in dem beide sind?
				if (($nextmatch["team1"] == $match["team1"] || $nextmatch["team1"] == $match["team2"]) && ($nextmatch["team2"] == $match["team1"] || $nextmatch["team2"] == $match["team2"])) {
					MysqlWriteByID(TblPrefix() . "flip_tournament_matches", array (
						"team1" => NULL,
						"team2" => NULL
					), $nextmatch["id"]);
				}
			}
			//Scrennshots l&ouml;schen
			$ids = MysqlReadCol("SELECT id FROM " . TblPrefix() . "flip_content_image WHERE `name` LIKE '%Match$matchid%'", "id");
			if (is_array($ids))
				foreach ($ids as $id) {
					if (MysqlDeleteByID(TblPrefix() . "flip_content_image", $id))
						LogChange("Der <strong>Screenshot</strong> mit der ID $id wurde gel&ouml;scht.");
				}
		}

		//Punkte zur&uuml;cksetzten
		if (MysqlWrite("UPDATE " . TblPrefix() . "flip_tournament_matches SET points1=NULL, points2=NULL, comment=NULL
			                   WHERE id='$matchid'", "SQL-Fehler in " . basename(__FILE__) . " line " . __LINE__ . "<br>\n"))
			$r = true;
		else
			$r = false;

		if ($r) {
			$this->GetMatches(0, false, true); //update cache
			$this->_Log("Punkte von " . TournamentCombatantID2Name($match["team1"]) . " vs. " . TournamentCombatantID2Name($match["team2"]) . " wurden von {$User->name} zur&uuml;ckgesetzt.");
			TournamentError("Punkte von " . TournamentCombatantID2Name($match["team1"]) . " vs. " . TournamentCombatantID2Name($match["team2"]) . " wurden zur&uuml;ckgesetzt.");
		} else {
			TournamentError("Fehler beim zur&uuml;cksetzen der Punkte.|Fehler beim Schreiben in die DB: \"" . mysqli_error() . "\"");
		}
		return $r;
	}

	/**
	 * L&ouml;scht alle Spiele eines Turnieres
	 * zus&auml;tzlich werden Serverzuweisungen und Screenshots gel&ouml;scht, Platzierungen zur&uuml;ckgesetzt
	 * @param integer $tournamentid Turnier-ID (optional)
	 * @return void nix
	 */
	function _DeleteGames($tournamentid = false) {
		if (!$tournamentid)
			$tournamentid = $this->id;
		$id = escape_sqlData_without_quotes($tournamentid);
		// zugewiesene Server des Turnieres auslesen und l&ouml;schen
		$servers = MysqlReadCol("SELECT u.server_id FROM ".TblPrefix()."flip_tournament_matches m LEFT JOIN ".TblPrefix()."flip_tournament_serverusage u ON u.match_id=m.id WHERE m.tournament_id='$id'", "server_id");
		MysqlExecQuery("DELETE FROM ".TblPrefix()."flip_tournament_serverusage WHERE server_id IN (".implode_sqlIn($servers).")", "SQL-Fehler beim l&ouml;schen in ".basename(__FILE__)." line ".__LINE__."<br>\n");
		// Screenshots des Turnieres l&ouml;schen
		MysqlExecQuery("DELETE FROM ".TblPrefix()."flip_content_image WHERE name LIKE '%_turnier_ID$id%'", "SQL-Fehler beim l&ouml;schen in ".basename(__FILE__)." line ".__LINE__."<br>\n");
		// Platzierungen des Turnieres zur&uuml;cksetzen
		MysqlExecQuery("DELETE FROM ".TblPrefix()."flip_tournament_ranking WHERE tournament_id='$id'", "SQL-Fehler beim l&ouml;schen in ".basename(__FILE__)." line ".__LINE__."<br>\n");
		// Spielpaarungen/Turnierbaum des Turnieres l&ouml;schen
		MysqlExecQuery("DELETE FROM ".TblPrefix()."flip_tournament_matches WHERE tournament_id='$id'", "SQL-Fehler beim l&ouml;schen in ".basename(__FILE__)." line ".__LINE__."<br>\n");
		// Logging und Nachricht
		TournamentError("Spielpaarungen, Platzierungen und Screenshots des ".TournamentGetTournamentString($tournamentid)."-Turnieres wurden gel&ouml;scht.<br><small>Wenn das Turnier bisher noch nicht gestartet wurde, kann diese Nachricht ignoriert werden (vorhandene Daten wurden auf jeden Fall gel&ouml;scht!).</small>", E_USER_NOTICE, __FILE__, __LINE__);
		$this->_Log("Alle Spielpaarungen, Platzierungen und Screenshots wurden gel&ouml;scht.");
	}
	
	function setRank($combatantid, $rank, $tournamentid = false) {
		if (!$tournamentid)
			$tournamentid = $this->id;
		if (!($tournamentid && is_posDigit($tournamentid))) {
			TournamentError('Keine TurnierID!?|id='.var_export($tournamentid, true), E_USER_WARNING, __FILE__,__LINE__);
			return false;
		}
		//skip Freilos
		if (empty ($combatantid))
			return false;

		if ($rank == NULL)
			$rank = 'NULL';
		elseif(is_posDigit($rank))
			$rank = "'$rank'";
		else {
			TournamentError('Kein g&uuml;ltiger Rang! ('.escapeHtml($rank).')', E_USER_WARNING, __FILE__,__LINE__);
			return false;
		}
		return MysqlWrite('UPDATE `' . TblPrefix() . 'flip_tournament_ranking` SET rank='.$rank.' WHERE combatant_id='.escape_sqlData_without_quotes($combatantid).' AND tournament_id='.escape_sqlData_without_quotes($tournamentid));
	}

	/**
	 * @since 1398 - 24.05.2007
	 * @author loom
	 * @see _cmp_ergebnis()
	 */
	function sortGroups($groups) {
		if(!function_exists('_cmp_ergebnis')) {
		/**
		 * Pr&uuml;ft ob ein Team "besser" ist als ein anderes (mehr Punkte, weniger
		 * Spiele, besseres Punkteverh&auml;ltnis)
		 * @access private
		 * @param Array $a assoziatives Array mit den Schl&uuml;sseln: points, matchcount
		 * pluspunkte und minuspunkte
		 * @param Array $b assoziatives Array mit den selben Schl&uuml;sseln wie $a
		 */
		function _cmp_ergebnis($a, $b) {
			if ($a["points"] == $b["points"]) {
				//mehr Spiele => schlechter
				if ($a["matchcount"] == $b["matchcount"]) {
					//Punkteverhaeltnis
					return ($a["pluspunkte"] - $a["minuspunkte"] > $b["pluspunkte"] - $b["minuspunkte"]) ? -1 : 1;
				} else {
					return ($a["matchcount"] < $b["matchcount"]) ? -1 : 1;
				}
			}
			return ($a["points"] > $b["points"]) ? -1 : 1;
		}
		}
		
		usort($groups, "_cmp_ergebnis");
		return $groups;
	}

	function isLoserBracket($level) {
		if (($level * 100 % 100) / 100 == 0.25 || ($level * 100 % 100) / 100 == 0.75)
			return true;
		else
			return false;
	}

	function isRoundplayed($newlevel = false, $team1 = "", $team2 = "", $antifreilos = true) {
		static $cache = array ();
		if ($newlevel === false || $newlevel === null) {
			return TournamentError("Fehler in isRoundplayed()|Variable wurde nicht &uuml;bergeben ($team1/$team2).", E_USER_WARNING, __FILE__, __LINE__);
		}
		//Bei erlaubten Freilosen diese als Gruppe nicht beachten
		if ($antifreilos) {
			if ($team1 == "0")
				$team1 = "1";
			if ($team2 == "0")
				$team2 = "1";
		} // 1 = Anonymous, ansonsten sehr unwahrscheinlich, dass es ein Turnierteam ist ;-)
		if (empty ($team1) && empty ($team2)) {
			//Spiele aus der n&auml;chsten Runde schon gespielt?
			if (isset ($cache[0][$this->id]["$newlevel"]))
				$nextplayed = $cache[0][$this->id]["$newlevel"];
			else
				$nextplayed = $cache[0][$this->id] = MysqlReadCol("SELECT id, level FROM " . TblPrefix() . "flip_tournament_matches
																					WHERE tournament_id='" . $this->id . "' AND points1 IS NOT NULL", "id", "level");
		}
		elseif ($antifreilos) {
			//Gruppe1 oder Gruppe2 Spiele aus der n&auml;chsten Runde schon gespielt (ausser gegen Freilos)?
			if (!isset ($cache[1][$this->id]["$newlevel"]["team1"]))
				$cache[1][$this->id]["$newlevel"]["team1"] = MysqlReadCol("SELECT id, team1 FROM " . TblPrefix() . "flip_tournament_matches
														                                   WHERE team2!='0'
														                                     AND level='" . $newlevel . "'
														                                     AND tournament_id='" . $this->id . "'
														                                     AND points1 IS NOT NULL
														                                  ", "id", "team1");
			if (!isset ($cache[1][$this->id]["$newlevel"]["team2"]))
				$cache[1][$this->id]["$newlevel"]["team2"] = MysqlReadCol("SELECT id, team2 FROM " . TblPrefix() . "flip_tournament_matches
																			               WHERE team1!='0'
																			                 AND level='" . $newlevel . "'
																			                 AND tournament_id='" . $this->id . "'
																			                 AND points1 IS NOT NULL
																			              ", "id", "team2");
			if (isset ($cache[1][$this->id]["$newlevel"]["team1"][$team1])) {
				$nextplayed[] = $cache[1][$this->id]["$newlevel"]["team1"][$team1];
			}
			if (isset ($cache[1][$this->id]["$newlevel"]["team1"][$team2])) {
				$nextplayed[] = $cache[1][$this->id]["$newlevel"]["team1"][$team2];
			}
			if (isset ($cache[1][$this->id]["$newlevel"]["team2"][$team1])) {
				$nextplayed[] = $cache[1][$this->id]["$newlevel"]["team2"][$team1];
			}
			if (isset ($cache[1][$this->id]["$newlevel"]["team2"][$team2])) {
				$nextplayed[] = $cache[1][$this->id]["$newlevel"]["team2"][$team2];
			}
		} else {
			//Gruppe1 oder Gruppe2 Spiele aus der n&auml;chsten Runde schon gespielt?
			if (!isset ($cache[2][$this->id]["$newlevel"]["team1"]))
				$cache[2][$this->id]["$newlevel"]["team1"] = MysqlReadCol("SELECT id, team1 FROM " . TblPrefix() . "flip_tournament_matches
														                                   WHERE level='" . $newlevel . "'
														                                     AND tournament_id='" . $this->id . "'
														                                     AND points1 IS NOT NULL
														                                  ", "id", "team1");
			if (!isset ($cache[2][$this->id]["$newlevel"]["team2"]))
				$cache[2][$this->id]["$newlevel"]["team2"] = MysqlReadCol("SELECT id, team2 FROM " . TblPrefix() . "flip_tournament_matches
														                                   WHERE level='" . $newlevel . "'
														                                     AND tournament_id='" . $this->id . "'
														                                     AND points1 IS NOT NULL
														                                  ", "id", "team2");
			if (isset ($cache[2][$this->id]["$newlevel"]["team1"][$team1])) {
				$nextplayed[] = $cache[2][$this->id]["$newlevel"]["team1"][$team1];
			}
			if (isset ($cache[2][$this->id]["$newlevel"]["team1"][$team2])) {
				$nextplayed[] = $cache[2][$this->id]["$newlevel"]["team1"][$team2];
			}
			if (isset ($cache[2][$this->id]["$newlevel"]["team2"][$team1])) {
				$nextplayed[] = $cache[2][$this->id]["$newlevel"]["team2"][$team1];
			}
			if (isset ($cache[2][$this->id]["$newlevel"]["team2"][$team2])) {
				$nextplayed[] = $cache[2][$this->id]["$newlevel"]["team2"][$team2];
			}
		}
		if (!empty ($nextplayed)) {
			$nextroundids = array ();
			foreach ($nextplayed AS $nextroundid) {
				$nextroundids[] = $nextroundid;
			}
			return $nextroundids;
		}
		return false;
	}

	/**
	 * TODO: verschieben (vmtl. nach TournamentMatch)
	 *
	 * @param unknown_type $level
	 * @return unknown
	 */
	function getnextLevel($level = false) {
		if ($level === false)
			return TournamentError("Fehler in getnextLevel()|Variable wurde nicht &uuml;bergeben bzw. ist false.", E_USER_WARNING, __FILE__, __LINE__);

		//Spiel aus dem Looserbracket?
		$islooserbracket = $this->isLoserBracket($level);
		//letzte Runde?
		if ($level == "0")
			return array (
				"winner" => "-1"
			);
		//erste Runde?
		if ($level == $this->FirstRound)
			$firstround = true;
		else
			$firstround = false;

		//n&auml;chste Runde ermitteln
		$newlevel = array (
			'winner' => null,
			'looser' => null
		);
		if (!$islooserbracket) {
			//Winnerbracket: n&auml;chstes Spiel Finale oder n&auml;chste Runde?
			if ($this->looser == 'N' && $level == '2')
				$newlevel['winner'] = 0;
			else
				$newlevel['winner'] = $level -1;
		}
		if ($this->looser == 'Y' && !$islooserbracket) {
			//Winnerbracket: n&auml;chstes Loserbracket-Match suchen
			if ($firstround)
				$newlevel['looser'] = $level -0.75;
			else
				$newlevel['looser'] = $level -0.25;
		}
		elseif ($islooserbracket) {
			//Loserbracket: n&auml;chstes Spiel Finale oder n&auml;chste Runde?
			if ($level != '0.75')
				$newlevel['winner'] = $level -0.5;
			else
				$newlevel['winner'] = 0;
		}

		ArrayWithKeys($newlevel, array (
			'winner',
			'looser'
		));
		return $newlevel;
	}

	/**
	 * Pr&uuml;ft, ob die angegebene ID in dem aktuellen Turnier erlaubt ist
	 * "none" ist immer g&uuml;ltig, sonst wird f&uuml;r die entsprechende Liga eine Pr&uuml;fung durchgef&uuml;hrt.
	 * @param String $aLigaid
	 * @return bool true wenn ID g&uuml;ltig, false wenn nicht
	 */
	function ValidLigaID(& $aLigaid) {
		if (empty ($aLigaid))
			$aLigaid = "none";
		if ($aLigaid != "none") {
			if (!$this-> {
				strtolower($this->liga->dbAndClassName) . "check" }
			($aLigaid))
			return false;
			if ($this->_ligaIdExists($aLigaid, strtolower($this->liga->dbAndClassName)))
				return false;
		}
		return true;
	}

	/**
	 * Pr&uuml;ft ob ein String eine g&uuml;ltige WWCL-ID ist und noch nicht vergeben ist
	 * f&uuml;r ein 1on1 Turnier wird eine PlayerID ben&ouml;tigt, ansonsten muss eine ClanID angegeben werden.
	 * Z.B: PlayerID:	P1234
	 *	  ClanID:	C2345
	 *
	 * @param string $id Der zu pr&uuml;fende String
	 * @param bool $showerror Gibt an ob eine Fehlermeldung ausgegeben und das Skript beendet wird
	 * @return bool true oder false, zus&auml;tzlich wird eine Fehlermeldung ausgegeben, wenn $showerror true ist
	 **/
	function wwclcheck($id, $showerror = true) {
		$id = strtoupper($id);
		//WWCL-ID auf g&uuml;ltigkeit pr&uuml;fen
		if ($this->teamsize > 1) {
			if ($id[0] != "C")
				return TournamentError("Es muss eine Clan-WWCL-ID eingegeben werden (beginnt mit \"C\").", E_USER_ERROR, __FILE__, __LINE__);
		} else {
			if ($id[0] != "P")
				return TournamentError("Es muss eine Player-WWCL-ID eingegeben werden (beginnt mit \"P\").", E_USER_ERROR, __FILE__, __LINE__);
		}
		if (preg_match("/^[PC][0-9]{4,6}$/", $id) < 1) {
			if ($showerror)
				return TournamentError("Es muss eine g&uuml;ltige WWCL-ID eingegeben werden.", E_USER_ERROR, __FILE__, __LINE__);
			else
				return false;
		}
		//in diesem Turnier bereits genutzt?
		if ($this->_ligaIdExists($id, "wwcl")) {
			if ($showerror)
				return TournamentError("Diese WWCL-ID hat bereits ein anderes Team" . (($this->id > 0) ? " in diesem Turnier" : "") . "!", E_USER_ERROR, __FILE__, __LINE__);
			else
				return false;
		}
		return true;
	}

	/**
	 * Pr&uuml;ft ob ein String eine g&uuml;ltige NGL-ID ist und noch nicht vergeben ist
	 * Hierbei wird gepr&uuml;ft ob es sich um eine Zahl mit 3-6 Stellen handelt
	 *
	 * @param string $id Der zu pr&uuml;fende String
	 * @param bool $showerror Gibt an ob eine Fehlermeldung ausgegeben und das Skript beendet wird
	 * @return bool true oder false, zus&auml;tzlich wird eine Fehlermeldung ausgegeben, wenn $showerror true ist
	 **/
	function nglcheck($id, $showerror = true) {
		//Syntax
		if (strtoupper($id) == "NONE")
			return true;
		if (preg_match("/^[0-9]{3,6}$/", $id) < 1 || $id <= 0) {
			if ($showerror)
				return TournamentError("Es muss eine g&uuml;ltige NGL-ID eingegeben werden.", E_USER_ERROR, __FILE__, __LINE__);
			else
				return false;
		}
		return true;
	}

	/**
	 * Pr&uuml;ft ob die Liga-ID bereits von einem Team in diesem Turnier genutzt
	 * wird
	 * 
	 * @access private
	 */
	function _ligaIdExists($ligaid, $liga = null) {
		if (is_null($liga))
			$liga = strtolower($this->liga->dbAndClassName);
		$tid = ($this->id > 0) ? "AND t.tournament_id='" . $this->id . "'" : "";
		return MysqlReadRow("SELECT d.id FROM " . TblPrefix() . "flip_user_column c
						           LEFT JOIN " . TblPrefix() . "flip_user_data d ON c.id=d.column_id
						           LEFT JOIN " . TblPrefix() . "flip_tournament_combatant t ON t.team_id=d.subject_id 
			                     WHERE c.name='tournament_" . $liga . "_id'
			                       AND c.type='turnierteam'
			                       AND d.val='" .escape_sqlData_without_quotes($ligaid) . "'
			                       $tid
			                    ", true);
	}
}

/**
 * Liest einmal alle Spiele aus der DB und gibt die Daten des angeforderten
 * Spieles zur&uuml;ck
 * 
 * @param int $id ID eines Spieles
 * @return Array mit den Schl&uuml;sseln aus flip_table_values
 */
function TournamentGetGame($id) {
	if(is_posDigit($id)) {
		$gamestablename = TournamentTablename_Games();
		static $tableid;
		if(!isset($tableid)) {
			$tableid = MysqlReadField('SELECT id FROM `'.TblPrefix().'flip_table_tables` WHERE name='.escape_sqlData_without_quotes($gamestablename).';', 'id');
		}
		
		static $games = array();
		if(empty($games)) {
			$games = MysqlReadArea('SELECT * FROM ´'.TblPrefix().'flip_table_entry´
									WHERE table_id='.escape_sqlData_without_quotes($tableid).'
									', 'key');
		}
		
		return $games[$id];
	}
	return false;
}

class TournamentGame extends SqlRowObject {
	var $id;
	var $name;
		
	/**
	 * Konstruktor
	 * 
	 * @param int $id ID des Spiels
	 */
	//php 7 public function __construct()
	//php 5 original function TournamentGame()
	function __construct($id) {
		$this->SqlRowObject(null, null, TournamentGetGame($id));
		$this->name = $this->value;
		unset($this->value);
	}
	
	/***************************** Getters *****************************/
	function getID() {
		return $this->id;
	}
	
	function getName() {
		return $this->name;
	}
	
	function getDisplay() {
		return $this->display;
	}
}
?>
