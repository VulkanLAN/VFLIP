<?php
/**
 * @author Daniel Raap
 * @version $Id$
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** Die Datei nur einmal includen */
if(defined("MOD.SEARCH.NEWS.PHP")) return 0;
define("MOD.SEARCH.NEWS.PHP",1);

/** FLIP-Kern */
require_once ("core/core.php");

class Search_news extends Search //part after Search_ should be the same as filenamepart after mod.search. (case!)
{
  function Search($searchtexts, $seperator="AND")
  {
    global $User;
    $r = array();
    if(!is_array($searchtexts)) $searchtexts = array($searchtexts);
    $r = $this->_searchNews("text", $searchtexts, $seperator);
    $r = array_merge($r, $this->SearchTitle($searchtexts, $seperator));
    
    return $r;
  }
  
  function SearchTitle($searchtexts, $seperator="AND")
  {
    return $this->_searchNews("title", $searchtexts, $seperator);
  }
  
  function _searchNews($col, $searchtexts, $seperator="AND")
  {
    global $User;
    $r = array();
    if(!is_array($searchtexts)) $searchtexts = array($searchtexts);
    $result = MysqlReadArea("SELECT `title`, `id`, `text`
                                FROM ".TblPrefix()."flip_news_news
                                WHERE (`$col` LIKE ".implode_sql(" $seperator `$col` LIKE ", $searchtexts).")
                                  AND ".$User->sqlHasRight("view_right"));
    foreach($result AS $row)
      $r["ID".$row["id"]] = array("title"  => $row["title"],
                                  "link"   => "news.php?newsid=".$row["id"],
                                  "text"   => $this->_format($row["text"], $searchtexts)
                                 );
    return $r;
  }
}

?>