<?php
/**
 * This class implements a system user.
 * System users have the same properties as normal users, though nobody can
 * log in with their credentials and they cannot be deleted. 
 * 
 * @author Matthias Gross
 * @version $Id$
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package mod
 **/

/** FLIP-Core */
require_once ("core/core.php");

class SystemUser extends User {
	
	function System($Ident) {
		parent::User($Ident);
	}
	
	/**
	 * Nobody can log in as a system user.
	 * @see web/core/User::checkPassword()
	 */
	function checkPassword($Password) {
		return false;
	}
	
	/**
	 * System users cannot be deleted.
	 * @see web/core/Subject::delete()
	 */
	function delete() {
		trigger_error('Ein Systemuser kann nicht gel&ouml;scht werden!', E_USER_ERROR);
	}
	
}

?>