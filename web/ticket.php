<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: ticket.php 1702 2019-01-09 09:01:12Z loom $ edit by naaux
 * @copyright (c) The FLIP Project Team Licensed under the GNU GPL. For
 * full terms see the file COPYING.
 * @license COPYING 
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");

function vergleiche($a, $b) {
	global $sortorder;
	$al = strtolower($a[$sortorder]);
	$bl = strtolower($b[$sortorder]);
	if ($al == $bl)
		return 0;
	return ($al > $bl) ? +1 : -1;
}

class TicketPage extends Page {
	var $ViewRight = "ticket_view";
	var $CommentRight = "ticket_comment";
	var $EditRight = "ticket_edit";
	var $EditSupportRight = 'ticket_request_support';
	var $OwnRight = "ticket_own";

	var $ConfigForceOwner = "ticket_force_owner";

	//php 7 public function __construct()
	//php 5 original function TicketPage()
	function __construct() {
		global $User;
		//php 5:
		//parent :: Page();
		//php7 neu:
		parent::__construct();
		$User->requireRight($this->ViewRight);
	}

	function _getTicketOwners() {
		$r = (ConfigGet($this->ConfigForceOwner) == "Y") ? array () : array (
			"" => "<nobody>"
		);
		foreach (GetRightOwners($this->OwnRight) as $id => $v)
			if ($v["type"] == "user")
				$r[$id] = $v["name"];
		return $r;
	}

	function queryTickets($id = 0) {
		if (empty ($id)) {
			$where = "1";
			$orderby = "t.create_time DESC";
		} else {
			$id = MysqlReadField("SELECT `ticket_id` FROM `" . TblPrefix() . "flip_ticket_tickets` WHERE (`id` = '" . addslashes($id) . "');");
			$where = "'$id' = t.ticket_id";
			$orderby = "t.version DESC";
		}
		$r = MysqlReadArea("
		      SELECT t.*, o.name AS `opening`, c.name AS `create`, r.name AS resp
		        FROM (`" . TblPrefix() . "flip_ticket_tickets` t)
		          LEFT JOIN `" . TblPrefix() . "flip_user_subject` o ON (t.opening_user = o.id)  
		          LEFT JOIN `" . TblPrefix() . "flip_user_subject` c ON (t.create_user = c.id)
		          LEFT JOIN `" . TblPrefix() . "flip_user_subject` r ON (t.resp_user = r.id)      
		          WHERE $where
		          GROUP BY t.id
		          ORDER BY $orderby;
		    ", "id");
		return $r;
	}

	function frameDefault($get, $post) {
		global $User, $sortorder;
		ArrayWithKeys($get, array('orderby', 'f_search', 'f_resp', 'f_category', 'f_hidedone'));
		
		$this->Caption = "Tickets";
		$this->ShowContentCell = false;
		
		$t = array ();
		foreach ($this->queryTickets() as $v) {
			$id = $v["ticket_id"];
			if (empty ($t[$id]))
				$t[$id] = $v;
			elseif ($t[$id]["version"] < $v["version"]) $t[$id] = $v;
		}

		if (!empty ($get["orderby"])) {
			$sortorder = $get["orderby"];
			usort($t, "vergleiche");
		}

		if (!empty ($get["f_search"])) {
			$f = addslashes($get["f_search"]);
			$found = MysqlReadCol("SELECT `ticket_id` FROM `" . TblPrefix() . "flip_ticket_event` WHERE (`description` REGEXP \"$f\");", "ticket_id");
		} else
			$found = array ();

		if ($get["f_resp"] == "me") {
			$get["f_resp"] = $User->id;
			$_GET["f_resp"] = $User->id; //f&uuml;r inc.form.php damit im Formular der aktuelle Benutzer ausgew&auml;hlt wird (selected).
		}
		
		foreach ($t as $k => $v) {
			if (!empty ($get["f_resp"]))
				if ($v["resp_user"] != $get["f_resp"])
					unset ($t[$k]);
			if (!empty ($get["f_category"]))
				if ($v["category"] != $get["f_category"])
					unset ($t[$k]);
			if (empty ($get["f_hidedone"])) {
				if ($v["priority"] == 0)
					unset ($t[$k]);
				elseif ($v["status"] > 100) unset ($t[$k]);
			}
			if (!empty ($get["f_search"]))
				if (!preg_match("/$get[f_search]/i", $v["title"]) and !in_array($k, $found))
					unset ($t[$k]);
		}
		return array (
			'f_resp' => (isset($get['f_resp']) ? $get['f_resp'] : 0), 
			"tickets" => $t,
			"frames" => array (
				"editticket" => "edit",
				"verlauf" => "verlauf"
			),
		"owners" => $this->_getTicketOwners());
	}

	function frameEditTicket($get) {
		global $User;
		$User->requireRight($this->EditRight);
		ArrayWithKeys($get, array('id'));
		if (empty ($get["id"])) {
			$this->Caption = "Ticket Erstellen";
			return array (
				"version" => 1,
			"owners" => $this->_getTicketOwners());
		}
		$this->Caption = "Ticket Bearbeiten";
		//aktuelle Version auslesen
		$r = MysqlReadRow("SELECT t.* FROM " . TblPrefix() . "flip_ticket_tickets t, " . TblPrefix() . "flip_ticket_tickets s
		                       WHERE s.id='" .escape_sqlData_without_quotes($get["id"]) . "'
		                         AND t.ticket_id = s.ticket_id
		                       ORDER BY t.version DESC
		                       LIMIT 0,1");
		$r["version"]++;
		$r["owners"] = $this->_getTicketOwners();
		return $r;
	}

	function submitTicket($post) {
		global $User;
		$User->requireRight($this->EditRight);
		$new = empty ($post["id"]);
		if ($new) {
			$desc = $post["event_desc"];
			unset ($post["event_desc"]);
			$post["opening_user"] = $User->id;
			$post["opening_time"] = time();
			$post["ticket_id"] = MysqlReadField("SELECT MAX(`ticket_id`) FROM `" . TblPrefix() . "flip_ticket_tickets`;") + 1;
		}
		$post["create_user"] = $User->id;
		$post["create_time"] = time();
		if (!($id = MysqlWriteByID(TblPrefix() . "flip_ticket_tickets", $post, 0, true)))
			return false;
		if ($new) {
			if (!$this->submitEvent(array (
					"ticket_id" => $post["ticket_id"],
					"description" => $desc
				), false))
				return false;
		} else {
			$lasttitle = MysqlReadField("SELECT title FROM " . TblPrefix() . "flip_ticket_tickets
			                                   WHERE ticket_id='" . $post["ticket_id"] . "'
			                                   ORDER BY version DESC
			                                   LIMIT 1,1", "title");
			$this->_sendMessage("Das Ticket \"$lasttitle\" wurde bearbeitet",
			//Text
			"Das Ticket \"" . $post["title"] . "\" hat jetzt folgenden Status:\n
			Verantwortlicher: " . GetSubjectName($post["resp_user"]) . "
			Status:           " . MysqlReadField("SELECT e.value FROM (" . TblPrefix() . "flip_table_entry e) 
							LEFT JOIN " . TblPrefix() . "flip_table_tables t ON e.table_id=t.id
			                                WHERE t.name = 'ticket_status' AND e.key = '" . $post["status"] . "'", "value") . "
			Priorit&auml;t:        " . MysqlReadField("SELECT e.value FROM (" . TblPrefix() . "flip_table_entry e) 
							LEFT JOIN " . TblPrefix() . "flip_table_tables t ON e.table_id=t.id
			                                WHERE t.name = 'ticket_priority' AND e.key = '" . $post["priority"] . "'", "value") . "
			Kategorie:        " . MysqlReadField("SELECT e.value FROM (" . TblPrefix() . "flip_table_entry e) 
							LEFT JOIN " . TblPrefix() . "flip_table_tables t ON e.table_id=t.id
			                                WHERE t.name = 'ticket_category' AND e.key = '" . $post["category"] . "'", "value") . "
			Deadline:         " . date("d.m.Y H:i", $post["deadline_time"]),
			//Empf&auml;nger
			array (
				$post["opening_user"],
				$post["create_user"],
				$post["resp_user"]
			));
		}
		LogChange("Ein <b>Ticket</b> mit dem Titel <b><a href=\"ticket.php?frame=ticket&amp;id=$id\">$post[title]</a></b> wurde " . (($new) ? "erstellt" : "bearbeitet") . ".");
		$this->NextPage = "ticket.php?frame=ticket&id=" . $id;
		return true;
	}

	function submitEvent($post, $dolog = true) {
		global $User;
		$User->requireRight($this->CommentRight);
		ArrayWithKeys($post, array("id","title","ticket_id","description"));
		$post["create_user"] = $User->id;
		$post["create_time"] = time();
		$link = "<a href=\"ticket.php?frame=ticket&amp;id=$post[id]\">$post[title]</a>";
		$title = $post["title"];
		unset ($post["id"]);
		unset ($post["title"]);
		$r = MysqlWriteByID(TblPrefix() . "flip_ticket_event", $post);
		if ($r) {
			$ticketdata = MysqlReadRow("SELECT * FROM " . TblPrefix() . "flip_ticket_tickets WHERE ticket_id='" . $post["ticket_id"] . "' ORDER BY version DESC LIMIT 1");
			$this->_sendMessage("Dem Ticket \"" . $ticketdata["title"] . "\" wurde ein Ereignis hinzugef&uuml;gt", "Dem Ticket mit dem Titel " .
			$ticketdata["title"] . " wurde von " . GetSubjectName($post["create_user"]) . " folgendes Ereignis hinzugef&uuml;gt:\n\n" . $post["description"] . "\n\nLink: " . ((isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") ? "https://" : "http://") . $_SERVER["HTTP_HOST"] . $_SERVER["SCRIPT_NAME"] . "?frame=ticket&id=" . $ticketdata["id"], array (
				$ticketdata["opening_user"],
				$ticketdata["create_user"],
				$ticketdata["resp_user"]
			));
			if ($dolog)
				LogChange("Dem <b>Ticket</b> mit dem Titel <b>$link</b> wurde ein Ereignis hinzugef&uuml;gt.");
		}
		return $r;
	}

	function _sendMessage($subject, $message, $receivers) {
		global $User;
		include_once ("mod/mod.sendmessage.php");
		$send = array ();
		foreach ($receivers AS $receiver) {
			if (empty ($receiver))
				continue;
			if ($receiver == $User->id)
				continue; //nicht an den Bearbeiter selbst schicken
			if (isset ($send[$receiver]))
				continue; //nicht mehrfach an den selben senden
			if (!SendMessageSendMessage($receiver, new User(SYSTEM_USER_ID), $subject, $message))
				trigger_error_text("Es konnte keine Benachrichtigung an " . GetSubjectName($receiver) . " gesendet werden!", E_USER_WARNING);
			else
				$send[$receiver] = GetSubjectName($receiver);
		}
		if (!empty ($send))
			trigger_error_text("Eine Benachrichtigung wurde an folgende Personen versandt: " .
			implode(", ", $send));
	}

	function frameTicket($get) {
		global $User;
		
		ArrayWithKeys($get, array("id"));
		$tickets = $this->queryTickets($get["id"]);
		$r = isset($tickets[$get["id"]]) ? $tickets[$get["id"]] : null;
		
		if (empty ($r)) {
			trigger_error_text("Das Ticket mit der ID \"$get[id]\" existiert nicht.", E_USER_ERROR);
		}

		$this->Caption = $r["title"];
		$r["events"] = MysqlReadArea(
			"SELECT e.*, u.name AS `user`
		     FROM (`".TblPrefix()."flip_ticket_event` e)
		     LEFT JOIN `" . TblPrefix() . "flip_user_subject` u ON (u.id = e.create_user)
		     WHERE (e.ticket_id = '$r[ticket_id]')"
		);
		
		foreach ($r["events"] as $k => $v) {
			$r["events"][$k]["description"] = nl2br(escapeHtml($v["description"]));
		}

		$r["allow_comment"] = $User->hasRight($this->CommentRight);
		$r["allow_edit"] = $User->hasRight($this->EditRight);
		return $r;
	}

	function frameVerlauf($get) {
		global $User;
		$this->Caption = "Tickets - Verlauf";
		$this->ShowContentCell = false;
		return array (
			"tickets" => $this->queryTickets($get["id"]
		), "frames" => array (), "id" => $get["id"], "allow_edit" => $User->hasRight($this->EditRight));
	}

	function framestats() {
		$this->Caption = "Ticketstatistik";

		// Intervalle in Tagen
		$intervals = array (
			"letzten 24h" => 1,
			"letzte Woche" => 7,
			"letzten Monat" => 30,
			"Gesamt" => 365000
		);

		foreach ($intervals AS $caption => $days) {
			$i = "";
			$j = "";
			$i["caption"] = $caption;
			$i["done"] = MysqlReadField("SELECT COUNT(*) anzahl FROM " . TblPrefix() . "flip_ticket_tickets t, " . TblPrefix() . "flip_table_entry e, " . TblPrefix() . "flip_table_tables d WHERE create_time>'" . (time() - $days * 24 * 60 * 60) . "' AND e.value='realisiert' AND status=e.key AND e.table_id=d.id AND d.name='ticket_status' ORDER BY anzahl DESC", "anzahl");
			$i["edit"] = MysqlReadField("SELECT COUNT(*) anzahl FROM " . TblPrefix() . "flip_ticket_tickets t WHERE version>1 AND create_time>'" . (time() - $days * 24 * 60 * 60) . "' ORDER BY anzahl DESC", "anzahl");
			$i["open"] = MysqlReadField("SELECT COUNT(*) anzahl FROM " . TblPrefix() . "flip_ticket_tickets t, " . TblPrefix() . "flip_table_entry e, " . TblPrefix() . "flip_table_tables d WHERE e.value='eröffnet' AND version=1 AND status=e.key AND e.table_id=d.id AND d.name='ticket_status' AND create_time>'" . (time() - $days * 24 * 60 * 60) . "' ORDER BY anzahl DESC", "anzahl");
			$i["comment"] = MysqlReadField("SELECT COUNT(*) anzahl FROM " . TblPrefix() . "flip_ticket_event t WHERE create_time>'" . (time() - $days * 24 * 60 * 60) . "' ORDER BY anzahl DESC", "anzahl");
			$r["allstats"][] = $i;

			$j["caption"] = $caption;
			list ($top) = MysqlReadArea("SELECT s.name, COUNT(*) anzahl FROM " . TblPrefix() . "flip_ticket_tickets t, " . TblPrefix() . "flip_table_entry e, " . TblPrefix() . "flip_table_tables d, " . TblPrefix() . "flip_user_subject s WHERE create_time>'" . (time() - $days * 24 * 60 * 60) . "' AND create_user=s.id AND e.value='realisiert' AND status=e.key AND e.table_id=d.id AND d.name='ticket_status' GROUP BY create_user ORDER BY anzahl DESC");
			$j["topdone"] = $top["name"] . " (" . $top["anzahl"] . ")";
			list ($top) = MysqlReadArea("SELECT s.name, COUNT(*) anzahl FROM " . TblPrefix() . "flip_ticket_tickets t, " . TblPrefix() . "flip_user_subject s WHERE version>1 AND create_time>'" . (time() - $days * 24 * 60 * 60) . "' AND create_user=s.id GROUP BY create_user ORDER BY anzahl DESC");
			$j["topedit"] = $top["name"] . " (" . $top["anzahl"] . ")";
			list ($top) = MysqlReadArea("SELECT s.name, COUNT(*) anzahl FROM " . TblPrefix() . "flip_ticket_tickets t, " . TblPrefix() . "flip_table_entry e, " . TblPrefix() . "flip_table_tables d, " . TblPrefix() . "flip_user_subject s WHERE e.value='eröffnet' AND version=1 AND create_user=s.id AND status=e.key AND e.table_id=d.id AND d.name='ticket_status' AND create_time>'" . (time() - $days * 24 * 60 * 60) . "' GROUP BY create_user ORDER BY anzahl DESC");
			$j["topopen"] = $top["name"] . " (" . $top["anzahl"] . ")";
			list ($top) = MysqlReadArea("SELECT s.name, COUNT(*) anzahl FROM " . TblPrefix() . "flip_ticket_event t, " . TblPrefix() . "flip_user_subject s WHERE create_time>'" . (time() - $days * 24 * 60 * 60) . "' AND create_user=s.id GROUP BY create_user ORDER BY anzahl DESC");
			$j["topcomment"] = $top["name"] . " (" . $top["anzahl"] . ")";
			$r["userstats"][] = $j;
		}
		return $r;
	}
	
	/**
	 * Displays all requests from the current user.
	 * 
	 * @param $getVars
	 */
	function frameShowMyRequests($getVars) {
		global $User;
		$User->requireRight($this->ViewRight);
		
		$r = array();
		$this->Caption = text_translate('Meine Anfragen');
		
		$sqlUserID = escape_sqlData_without_quotes($User->id);
		$tickets = MysqlReadArea('SELECT *, MAX(`status`) as `current_status` FROM `flip_ticket_tickets` WHERE `create_user` = ' . $sqlUserID . ' GROUP BY `ticket_id`');
		
		if($tickets === false) {
			trigger_error_text(
				text_translate('Fehler beim Lesen der Tickets!'),
				E_USER_ERROR
			);
			
			return array();
		}
		
		$r['tickets'] = $tickets;
		
		return $r;
	}
	
	/**
	 * This frame generates a support request form that users can use 
	 * to get help from orgas.
	 * 
	 * @param $getVars
	 */
	function frameRequestSupport($getVars) {
		global $User;
		$User->requireRight($this->ViewRight);
		
		$r = array();
		$this->Caption = text_translate('Anfrage stellen');
		
		return $r;
	}
	
	/**
	 * Triggered by the submit form for requesting support, this sumbit stores a new request into the db.
	 * 
	 * @param $getVars
	 */
	function submitRequestSupport($getVars) {
		global $User;
		$User->requireRight($this->EditSupportRight);
		
		$newTicket = array(
			'category' => '13',
			'priority' => 5,
			'title' => $getVars['subject'],
			'event_desc' => $getVars['message']
		);
		
		return $this->submitTicket($newTicket);
	}
}

RunPage("TicketPage");
?>
