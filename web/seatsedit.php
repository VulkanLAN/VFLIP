<?php

/**
  * @author Moritz Eysholdt
  * @version $Id: seats.php 1702 2019-01-09 09:01:12Z loom $ edit by naaux u. tominator
  * @copyright (c) The FLIP Project Team
  * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
  * @package pages
  **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once("inc/inc.page.php");
require_once("mod/mod.image.php");
require_once("mod/mod.seats.php");

class SeatsEditPage extends Page {

	var $AdminUserRight = "seats_manage_user";
	var $AdminSeatsRight = "seats_admin";

	function frameBlockTools($get) {
		global $User;
		$User->requireRight($this->AdminSeatsRight);
		$r = SeatGetBlock($get["id"]);
		$this->Caption = "Grafische Tools: $r[caption]";
		
		// Added by VulkanLAN
		$seats = MysqlReadArea("SELECT `center_x`,`center_y`,`angle`,`name`,`id`,`block_id`  FROM `".TblPrefix()."flip_seats_seats` WHERE `block_id`='".addslashes($get["id"])."'");
		$r["seats"] = $seats;
		
		return $r;
	}

	function frameEditBlocks($get) {
		global $User;
		$User->requireRight($this->AdminSeatsRight);
		$this->Caption = "Bl&ouml;cke bearbeiten";
		$r = SeatsGetOverviewHeader();
		$r["blocks"] = SeatGetBlocks();
		return $r;
	}

	function frameEditBlock($get) {
		global $User;
		$this->Caption = "Sitzblock bearbeiten";
		$User->requireRight($this->AdminSeatsRight);
		if (empty ($get["id"]))
			return array (
				"caption" => "neuer Block",
				"imagedir_block" => "tpl/default/images/seats/classic/",
				"imagedir_overview" => "tpl/default/images/seats/simple_8x5/"
			);
		else
			return SeatGetBlock($get["id"], false);
	}

	function submitBlock($post) {
		global $User;
		$User->requireRight($this->AdminSeatsRight);
		//if($post["imagedir_block"] || $post["imagedir_overview"]) RedrawSeats($post["id"]);
		$id = MysqlWriteByID(TblPrefix() . "flip_seats_blocks", $post, $post["id"]);
		if ($id)
			LogChange("Der <b>Sitzplan <a href=\"seats.php?frame=block&amp;blockid=$id\">$post[caption]</a></b> wurde bearbeitet");
		return $id;
	}

	function frameedit($get) {
		global $User;
		$User->requireRight($this->AdminSeatsRight);
		$this->Caption = "Sitzplan bearbeiten";

		$setcols = array ();
		$r["rows"] = array ();
		$seats = MysqlReadArea("SELECT id, center_x, center_y, CONCAT(name,\"<br />(\",center_x,\"/\",center_y,\")\") AS name FROM " . TblPrefix() . "flip_seats_seats WHERE block_id='" . $get["id"] . "'");
		foreach ($seats AS $seat) {
			$r["rows"][$seat["center_y"]]["cols"][$seat["center_x"]] = $seat;
			$setcols[$seat["center_x"]] = $seat["center_x"];
		}
		ksort($r["rows"]);
		asort($setcols);
		foreach ($r["rows"] AS $key => $cols) {
			foreach ($setcols AS $colno)
				if (!isset ($cols["cols"][$colno]))
					$r["rows"][$key]["cols"][$colno] = array ();
			ksort($r["rows"][$key]["cols"]);
		}

		$r["block"] = SeatGetBlock($get["id"], false);
		return $r;
	}

	function actionCalcCords($post) {
		if (is_array($post["ids"]))
			foreach ($post["ids"] as $id)
				SeatsCalcBlockCords($id);
	}

	function actiondeleteall($post) {
		global $User;
		$User->requireRight($this->AdminSeatsRight);
		trigger_error_text("Der Sitzplan des Blocks $post[blockid] wurde gel&ouml;scht!", E_USER_NOTICE);
		LogAction("Der Sitzplan des Blocks $post[blockid] wurde gel&ouml;scht.");
		MysqlWrite("DELETE FROM `" . TblPrefix() . "flip_seats_seats` WHERE block_id = '$post[blockid]';");
		return true;
	}

	function actionDeleteBlock($post) {
		if (is_array($post["ids"]))
			foreach ($post["ids"] as $id)
				SeatsDeleteBlock($id);
	}

	function frameeditseat($get) {
		global $User;
		$User->requireRight($this->AdminSeatsRight);
		$this->Caption = ($get["id"] == "new") ? "Sitzplatz erstellen" : "Sitzplatz bearbeiten";

		//seatdata
		if ($get["id"] == "new") {
			$r = array (
				"angle" => 0
			);
		} else {
			$r = MysqlReadRowByID(TblPrefix() . "flip_seats_seats", $get["id"]);
		}

		//imageinfos
		include_once ("mod/mod.seats.draw.php");
		$seatimage = LoadSeatImage(MysqlReadFieldByID(TblPrefix() . "flip_seats_blocks", "imagedir_block", $get["blockid"]), "default");
		$info = $seatimage->getInfo();
		$r["width0"] = $info["width"];
		$r["height0"] = $info["height"];

		$r["angles"] = array (
			0 => "0°",
			90 => "90°",
			180 => "180°",
			270 => "270°"
		);
		$r['namingmodes'] = array (
			0 => 'Zeilenbeschriftung fest - Spaltenbeschriftung dynamisch',
			1 => 'Zeilenbeschriftung dynamisch - Spaltenbeschriftung fest'
		);
		$r["namerowprefix"] = "Reihe ";
		$r["namerowstart"] = "A";
		$r["namerowsuffix"] = " ";
		$r["namecolprefix"] = "Platz ";
		$r["namecolstart"] = "1";
		$r["namecolsuffix"] = "";

		$r["ipa"] = "10";
		$r["ipb"] = "10";
		$r["xyarr"] = array (
			"xy" => "a . b . c+X . d+Y (vertikal)",
			"yx" => "a . b . c+Y . d+X (horizontal)"
		);

		$r["block_id"] = $get["blockid"];

		return $r;
	}

	function submiteditseat($post) {
		global $User;
		$User->requireRight($this->AdminSeatsRight);
		if ($post["id"] == "new")
			$post["id"] = 0;
		MysqlWriteByID(TblPrefix() . "flip_seats_seats", $post, $post["id"]);
		RedrawSeats($post["block_id"]);
		return true;
	}

	function submitdelseat($post) {
		global $User;
		$User->requireRight($this->AdminSeatsRight);

		$blockid = MysqlReadFieldByID(TblPrefix() . "flip_seats_seats", "block_id", $post["seatid"]);
		if (empty ($blockid))
			return false;
		MysqlDeleteByID(TblPrefix() . "flip_seats_seats", $post["seatid"]);
		RedrawSeats($blockid);
		return true;
	}

	function submitaddrow($post) {
		global $User;
		$User->requireRight($this->AdminSeatsRight);
		if (!is_posDigit($post["ipa"]) || !is_posDigit($post["ipb"]) || !is_posDigit($post["ipc"]) || !is_posDigit($post["ipd"]) || $post["ipd"] < 1 || $post["ipd"] > 254) {
			trigger_error_text("Die IP wurde nicht korrekt angegeben.", E_USER_WARNING);
			return false;
		}
		$rowisnumeric = false;
		$colisnumeric = false;
		$ipc = $post["ipc"];
		$ipd = $post["ipd"];
		//naming
		$namerowprefix = $post["namerowprefix"];
		if (is_posDigit($post["namerowstart"])) {
			$namerow = $post["namerowstart"];
			$rowisnumeric = true;
		} else
			$namerow = ord($post["namerowstart"]);
		$namerowsuffix = $post["namerowsuffix"];
		$namecolprefix = $post["namecolprefix"];
		if (is_posDigit($post["namecolstart"])) {
			$namecol = $post["namecolstart"];
			$colisnumeric = true;
		} else
			$namecol = ord($post["namecolstart"]);
		$namecolsuffix = $post["namecolsuffix"];
		
		//increase Naming according to angle
		// OLD: ($post['angle'] == '90') || ($post['angle'] == '270')
		
		if ($post['namingmode'] > 0) {
			$addx = 0;
			$addy = 1;
		} else {
			$addx = 1;
			$addy = 0;
		}
		
		//add seats
		for ($i = 1; $i <= $post['seatno']; $i++) {
			if ($rowisnumeric)
				$namex = $namerow;
			else
				$namex = chr($namerow);
			if ($colisnumeric)
				$namey = $namecol;
			else
				$namey = chr($namecol);

			MysqlWriteByID(TblPrefix() . "flip_seats_seats", array (
				"block_id" => $post["block_id"],
				"center_x" => $post["center_x"] + ($i -1
			) * $post["spacex"], "center_y" => $post["center_y"] + ($i -1) * $post["spacey"], "angle" => $post["angle"], "name" => "$namerowprefix$namex$namerowsuffix$namecolprefix$namey$namecolsuffix", "ip" => $post["ipa"] . "." . $post["ipb"] . ".$ipc.$ipd",));

			if ($post["xy"] == "yx") {
				$ipc += $addy;
				$ipd += $addx;
			} else {
				$ipc += $addx;
				$ipd += $addy;
			}
			$namerow += $addy;
			$namecol += $addx;
		}

		RedrawSeats($post["block_id"]);
		return true;
	}

	function actionRedraw($post) {
		ArrayWithKeys($post, array('ids'));
		if (is_array($post['ids'])) {
			$count = MysqlReadField('SELECT COUNT(*) FROM `' . TblPrefix() . 'flip_seats_blocks`;');
			if ($count == count($post['ids']))
				RedrawSeats();
			else
				foreach ($post['ids'] as $id) {
					RedrawSeats($id);
					LogAction('Der Sitzplan mit der ID $id wurde neu gezeichnet.');
				}
		}
	}

	function actionDemo($p) {
		$block = (empty ($p["blockid"])) ? 1 : (integer) $p["blockid"];
		LogAction("Eine Sitzplan-Demo f&uuml;r Block $block wird generiert...");

		MysqlWrite("DELETE FROM `" . TblPrefix() . "flip_seats_seats` WHERE block_id='$block';");
		srand(time());
		switch ($p["type"]) {
			case ("ul") :
				break;
			case ("demo") :
				/* for($i = 0; $i < 30; $i++)
				 {
				   $a = $i / 15 * pi();
				   $d = $i / 15 * 180;
				   $this->insert(round(300 + sin($a)*170),round(800 + cos($a)*170),$d);
				 }*/
				for ($i = 7; $i < 34; $i++)
					if (($i != 19) and ($i != 21)) {
						$a = $i / 20 * pi();
						$d = ((40 - $i) / 20 * 180 + 180) % 360;
						$this->insert(round(300 + sin($a) * 190), round(300 - cos($a) * 155), $d, $block);
					}
				for ($i = 0; $i < 12; $i++) {
					$a = sin($i / 7 * pi());
					$b = 90 - (cos($i / 7 * pi()) * 7);
					$this->insert(round(279 - ($a * 12)), round(335 + ($i * 24)), (round($b) + 180) % 360, $block);
				}
				for ($i = 0; $i < 12; $i++) {
					$a = sin($i / 7 * pi());
					$b = 90 + (cos($i / 7 * pi()) * 7);
					$this->insert(round(321 + ($a * 12)), round(335 + ($i * 24)), round($b), $block);
				}

				break;
		}
		RedrawSeats($block);
	}

	function insert($x, $y, $angle, $block = 1) {
		$uid = 0;
		$s = "";
		$res = false;
		$en = round(rand(0, 6));
		if ($en) {
			$uid = 4;
			$res = round(rand(0, 5));
			if ($res) {
				switch (round(rand(0, 4))) {
					case (0) :
						$s = "checked_in";
						break;
					case (1) :
						$s = "online";
						break;
					case (2) :
						$s = "offline";
						break;
					case (3) :
						$s = "checked_out";
						break;
					case (4) :
						$s = "";
						break;
				}
			}
		}
		$r = array (
			"block_id" => $block,
			"center_x" => $x,
			"center_y" => $y,
			"angle" => $angle,
			"name" => "Seat ($x/$y)",
			"ip" => "10.10.$x.$y",
			"enabled" => ($en
		) ? "Y" : "N", "reserved" => ($res) ? "Y" : "N", "user_id" => $uid, "user_status" => $s);
		MysqlWriteByID(TblPrefix() . "flip_seats_seats", $r);
	}
}

RunPage("SeatsEditPage");
?>
