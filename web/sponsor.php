<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: sponsor.php 1702 2019-01-09 09:01:12Z loom $ edit by tominator u. naaux
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");
require_once ("inc/inc.text.php");

class SponsorPage extends Page {
	var $TextList = "sponsor_list";
	var $TextPublicList = "sponsor";
	var $RightInternal = "sponsor_view_internal";
	var $RightEdit = "sponsor_edit";
	var $_status = array (0 => "Nicht bearbeitet", 1 => "Mail abgeschickt", 2 => "Absage", 3 => "Zusage");

	//php 7 public function __construct()
	//php 5 original function SponsorPage()
	function __construct() {
		//php 5:
		//parent :: Page();
		//php7 neu:
		parent::__construct();
	}

	function frameDefault($get, $post) {
		include_once ("inc/inc.sponsor.php");
		$Location = "sponsorlist";
		// Edit VulkanLAN add category
		$categorytableid = MysqlReadField("
		      SELECT `id`,`name` FROM `".TblPrefix()."flip_table_tables`  WHERE (`name` = 'sponsor_category');");
			
		$categoryvalue = MysqlReadArea("
		      SELECT `id`,`table_id`,`key`,`value`,`display`,`display_code` FROM `".TblPrefix()."flip_table_entry`  WHERE (`table_id` = $categorytableid) ORDER BY `id` ASC;", "key");
		// end
		$rows = MysqlReadArea("
		      SELECT `id`,`name`,`homepage`,`description`,`category` FROM `".TblPrefix()."flip_sponsor_sponsors` 
		        WHERE (`public` = '1') 
		        ORDER BY `priority` DESC,`name`;
		    ", "id");
		if (!empty ($rows)) {
			$ids = implode_sqlIn(array_keys($rows));
			$images = MysqlReadArea("
			        SELECT `id`,`image_mtime`,`sponsor_id`,`enable_session_log`,`enable_view_log`,`enable_counter`,`html`,`link`
			          FROM `".TblPrefix()."flip_sponsor_ads` 
			          WHERE ((`show_in_list` = '1') AND (`sponsor_id` IN ($ids)))
			          ORDER BY `priority` DESC;
			      ", "id");
			foreach ($images as $v) {
				$rows[$v["sponsor_id"]]["images"][] = $v;
				SponsorLogAndCount($Location, $v);
			}
		}
		return array ("text" => Loadtext($this->TextPublicList, $this->Caption), "rows" => $rows, "categoryvalue" => $categoryvalue, "location" => $Location,);
	}

	function frameSmallButtons() {
		$Location = "buttonlist";
		$images = CacheGet("sponsor_buttons");
		if (!is_array($images)) {
			$images = MysqlReadArea("SELECT `id`,`image_mtime`,`enable_session_log`,`enable_view_log`,`enable_counter`,`link`,`html` FROM `".TblPrefix()."flip_sponsor_ads` WHERE (`show_as_ad` = '1') AND (`type` = 'button') ORDER BY `priority` DESC;", "id");
			CacheSet($images, array ("flip_sponsor_ads"));
		}
		foreach ($images as $v)
			if ($v["enable_session_log"] or $v["enable_view_log"] or $v["enable_counter"]) {
				include_once ("inc/inc.sponsor.php");
				SponsorLogAndCount($Location, $v);
			}
		return array ("images" => $images, "location" => $Location,);
	}

	function frameList($get) {
		global $User;
		$User->requireRight($this->RightInternal);
		// Edit VulkanLAN add category and responsible
		$sponsors = MysqlReadArea("
		      SELECT s.id, s.name, s.public, s.priority, s.homepage, s.status, s.category, s.responsible, s.amount, s.last_edit_user, UNIX_TIMESTAMP(s.mtime) AS `time`, u.name AS `user`, x.name AS `responsibleuser`
		        FROM (`".TblPrefix()."flip_sponsor_sponsors` s)
		        LEFT JOIN `".TblPrefix()."flip_user_subject` u ON (s.last_edit_user = u.id)
				LEFT JOIN `".TblPrefix()."flip_user_subject` x ON (s.responsible = x.id)
		          ORDER BY public DESC, priority DESC, name ASC;
		    ");
		$banner = MysqlReadCol("SELECT COUNT(`id`) AS `c`,`sponsor_id` FROM `".TblPrefix()."flip_sponsor_ads` WHERE (`type`='banner') GROUP BY `sponsor_id`;", "c", "sponsor_id");
		$button = MysqlReadCol("SELECT COUNT(`id`) AS `c`,`sponsor_id` FROM `".TblPrefix()."flip_sponsor_ads` WHERE (`type`='button') GROUP BY `sponsor_id`;", "c", "sponsor_id");
		foreach ($sponsors as $k => $v) {
			$sponsors[$k]["banner"] = isset($banner[$v["id"]]) ? intval($banner[$v["id"]]) : 0;
			$sponsors[$k]["buttons"] = isset($button[$v["id"]]) ? intval($button[$v["id"]]) : 0;
		}
		return array ("text" => Loadtext($this->TextList, $this->Caption), "rows" => $sponsors, "menu" => "list", "edit" => $User->hasRight($this->RightEdit),);
	}

	function frameViewSponsor($get) {
		global $User;
		$User->requireRight($this->RightInternal);
		$id = addslashes($get["id"]);
		// Edit VulkanLAN add category and responsible
		$r = MysqlReadRow("
		      SELECT s.*, UNIX_TIMESTAMP(s.mtime) AS `time`, u.name AS `user`, x.name AS `responsibleuser`
		        FROM (`".TblPrefix()."flip_sponsor_sponsors` s)
		        LEFT JOIN `".TblPrefix()."flip_user_subject` u ON (s.last_edit_user = u.id)
				LEFT JOIN `".TblPrefix()."flip_user_subject` x ON (s.responsible = x.id)
		        WHERE (s.id = '$id');");
		$this->Caption = "Sponsor -> $r[name]";
		$images = MysqlReadCol("SELECT COUNT(`id`) AS `c`,`type` FROM `".TblPrefix()."flip_sponsor_ads` WHERE (`sponsor_id`='$r[id]') GROUP BY `type`;", "c", "type");
		if(!is_array($images)) {
			$images = array();
		}
		ArrayWithKeys($images, array("banner","button"));
		$r["images"] = array ("banner" => intval($images["banner"]), "button" => intval($images["button"]));
		$r["edit"] = $User->hasRight($this->RightEdit);
		$r["comment"] = nl2br($r["comment"]);
		return $r;
	}

	function frameEditSponsor($get) {
		global $User;
		$User->requireRight($this->RightEdit);
		$this->Caption = "Sponsor -> bearbeiten";
		
		// Edit VulkanLAN - Tominator
		$users = GetSubjects("user");
		$UserSelection = array();
		  foreach($users AS $u=>$user) {
			$UserObject = CreateSubjectInstance($users[$u]["id"], "user");
			if ($UserObject->hasRight($this->RightEdit))
			  $UserSelection += array ($users[$u]["id"]=>$users[$u]["name"]);
		  }
		
		if (empty ($get["id"]))
			$r = array ("name" => "neuer Sponsor", "priority" => 5);
		else
			$r= MysqlReadRowByID(TblPrefix()."flip_sponsor_sponsors", $get["id"]);
		
		$r["responsibleUsers"] = $UserSelection;
		
		return $r;
	}

	function submitSponsor($p) {
		global $User;
		$User->requireRight($this->RightEdit);
		$p["last_edit_user"] = $User->id;
		if (!empty ($p["homepage"]))
			if (!(stristr($p["homepage"], "://")))
				$p["homepage"] = "http://".$p["homepage"];
		$r = MysqlWriteByID(TblPrefix()."flip_sponsor_sponsors", $p, $p["id"]);
		if ($r)
			LogChange("Der <b>Sponsor</b> \"".$p["name"]."\" wurde bearbeitet.");
		return $r;
	}

	function actionDelete($p) {
		global $User;
		$User->requireRight($this->RightEdit);

		foreach ($p["ids"] AS $id) {
			$id = addslashes($id);
			$name = MysqlReadFieldByID(TblPrefix()."flip_sponsor_sponsors", "name", $id);
			$imgs = MysqlReadCol("SELECT `id` FROM `".TblPrefix()."flip_sponsor_ads` WHERE (`sponsor_id` = '$id');", "id");
			$this->actionDeleteImage(array ("ids" => $imgs));
			$r = MysqlWrite("DELETE FROM ".TblPrefix()."flip_sponsor_sponsors WHERE (id = '$id');");
			if ($r)
				LogChange("Der <b>Sponsor</b> \"$name\" wurde gel&ouml;scht.");
		}
	}

	function frameImageList($get) {
		global $User;
		ArrayWithKeys($get, array("type","sponsor_id")); 
		$User->requireRight($this->RightInternal);
		$type = (in_array($get["type"], array ("banner", "button"))) ? $get["type"] : "banner";
		if (!empty ($get["sponsor_id"])) {
			$sponsor = MysqlReadFieldByID(TblPrefix()."flip_sponsor_sponsors", "name", $get["sponsor_id"]);
			$title = " von $sponsor";
			$where = " AND (`sponsor_id` = '".addslashes($get["sponsor_id"])."')";
		} else
			$title = $where = $sponsor = "";
		switch ($type) {
			case ("banner") :
				$this->Caption = "Sponsoren -> Banner $title";
				break;
			case ("button") :
				$this->Caption = "Sponsoren -> Buttons $title";
				break;
		}
		$items = MysqlReadArea("
		      SELECT a.id, a.image_mtime, a.sponsor_id, a.show_as_ad, a.show_in_list, a.priority, a.clicks, a.views, a.sessions, a.link, a.enable_counter, a.html,
		             a.enable_view_log, a.enable_session_log, a.enable_click_log, a.last_counter_reset, s.name AS `sponsor`, s.homepage
		        FROM (`".TblPrefix()."flip_sponsor_ads` a)
		        LEFT JOIN `".TblPrefix()."flip_sponsor_sponsors` s ON (a.sponsor_id = s.id)
		        WHERE (a.type = '$type')$where
		        ORDER by a.priority DESC ,s.priority DESC ,s.name;
		    ");
		foreach ($items as $k => $v)
			$items[$k]["url"] = (empty ($v["link"])) ? $v["homepage"] : $v["link"];
		$get["sponsor_id"] = $get["sponsor_id"];
		return array ("items" => $items, "imgtitle" => ucfirst($type), "type" => $type, "menu" => $type, "sponsors" => MysqlReadCol("SELECT `name`,`id` FROM `".TblPrefix()."flip_sponsor_sponsors`;", "name", "id"), "sid" => $get["sponsor_id"], "sponsor" => $sponsor, "edit" => $User->hasRight($this->RightEdit),);
	}

	function frameEditImage($get) {
		global $User;
		$User->requireRight($this->RightEdit);

		if (!empty ($get["sid"]))
			$get["sponsor_id"] = $get["sid"];
		if (!empty ($get["id"])) {
			$r = MysqlReadRow("
			        SELECT a.id, a.type, a.image_mtime, a.sponsor_id, a.show_in_list, a.show_as_ad, a.priority, a.enable_view_log, 
			               a.enable_session_log, a.enable_click_log, a.link, s.name AS `sponsor`, s.homepage, a.enable_counter, a.html
			          FROM (`".TblPrefix()."flip_sponsor_ads` a)
			          LEFT JOIN `".TblPrefix()."flip_sponsor_sponsors` s ON (a.sponsor_id = s.id)
			          WHERE (a.id = '".addslashes($get["id"])."');
			      ");
			$this->Caption = "Sponsoren -> ".ucfirst($r["type"])." bearbeiten";
		}
		elseif (!empty ($get["sponsor_id"]) and !empty ($get["type"])) {
			if (!in_array($get["type"], array ("banner", "button")))
				trigger_error_text("Der Bildtyp ist ung&uuml;ltig", E_USER_ERROR);
			$sid = addslashes($get["sponsor_id"]);
			$r = MysqlReadRow("SELECT `id` AS `sponsor_id`, `name` AS `sponsor`, `homepage` FROM `".TblPrefix()."flip_sponsor_sponsors` WHERE (`id` = '$sid')");
			$r["type"] = $get["type"];
			$this->Caption = "Sponsoren -> ".ucfirst($r["type"])." erstellen";
		} else
			trigger_error_text("Um ein Werbebild zu bearbeiten/erstellen, muss entweder eine ID oder seine Sponsoren-ID und ein Typ angegeben werden.", E_USER_ERROR);
		return $r;
	}

	function submitImage($dat) {
		global $User;
		$User->requireRight($this->RightEdit);
		ArrayWithKeys($dat, array("id","link","sponsor_id","type"));

		if (empty ($dat["id"]))
			$dat["last_counter_reset"] = time();
		if (!empty ($dat["link"]) and strpos($dat["link"], "://") === false) {
			trigger_error_text("Der Bildlink muss absolut sein (d.h. mit http:// oder https:// oder einem anderen Protokoll beginnen)", E_USER_WARNING);
			return false;
		}
		$dat["image_mtime"] =  date('Y-m-d H:i:s\Z'); // setzt die image_mtime auf die aktuelle zeit.
		$r = MysqlWriteByID(TblPrefix()."flip_sponsor_ads", $dat, $dat["id"]);
		if ($r) {
			if (empty ($dat["sponsor_id"]))
				$dat["sponsor_id"] = MysqlReadFieldByID(TblPrefix()."flip_sponsor_ads", "sponsor_id", $r);
			if (empty ($dat["type"]))
				$dat["type"] = MysqlReadFieldByID(TblPrefix()."flip_sponsor_ads", "type", $r);
			$s = MysqlReadFieldByID(TblPrefix()."flip_sponsor_sponsors", "name", $dat["sponsor_id"]);
			$t = ucfirst($dat["type"]);
			LogChange("F&uuml;r den <b>Sponsor <a href=\"sponsor.php?frame=editsponsor&amp;id=".$dat["sponsor_id"]."\">$s</a></b> wurde ein <b><a href=\"sponsor.php?frame=editimage&amp;id=$r\">$t</a></b> ". ((empty ($dat["id"])) ? "erstellt." : "bearbeitet."));
		}
		return $r;
	}

	function actionDeleteImage($dat) {
		global $User;
		$User->requireRight($this->RightEdit);

		if (is_array($dat["ids"]))
			foreach ($dat["ids"] as $id) {
				$id = addslashes($id);
				$s = MysqlReadRow("
				        SELECT s.name, s.id, a.type FROM `".TblPrefix()."flip_sponsor_sponsors` s, `".TblPrefix()."flip_sponsor_ads` a WHERE (s.id = a.sponsor_id) AND (a.id = '$id');
				      ");
				$t = ucfirst($s["type"]);
				$r = MysqlDeleteByID(TblPrefix()."flip_sponsor_ads", $id);
				if ($r)
					LogChange("Vom <b>Sponsor <a href=\"sponsor.php?frame=editsponsor&amp;id=$s[id]\">$s[name]</a></b> wurde ein <b>$t</b> gel&ouml;scht.");
			}
		return true;
	}

	function actionResetCounter($dat) {
		global $User;
		$User->requireRight($this->RightEdit);

		if (is_array($dat["ids"]))
			foreach ($dat["ids"] as $id) {
				$r = MysqlWrite("UPDATE ".TblPrefix()."flip_sponsor_ads SET `views` = '0',`clicks` = '0',`sessions` = '0', `last_counter_reset` = CURRENT_TIMESTAMP WHERE `".TblPrefix()."flip_sponsor_ads`.`id` = '$id' ");
				if ($r)
					LogChange("Der <b>Banner Counter </b> \"$id\" wurde zurueckgesetzt.");
				}
		return true;
	}

	function frameViewLog($get) {
		global $User;
		$User->requireRight($this->RightEdit);

		$this->Caption = "Sponsoren -> Log";
		$limit = (empty ($get["limit"])) ? "0,100" : ((preg_match("/^\d+\,\d+$/", $get["limit"])) ? $get["limit"] : "0,100");

		$items = MysqlReadArea("
		      SELECT l.*, a.sponsor_id, s.name AS `sponsor`, UNIX_TIMESTAMP(l.mtime) AS `time`, a.type, a.image_mtime AS `admtime`, a.id AS `adid`
		        FROM (`".TblPrefix()."flip_sponsor_log` l)
		          LEFT JOIN `".TblPrefix()."flip_sponsor_ads` a ON (l.ad_id = a.id)
		          LEFT JOIN `".TblPrefix()."flip_sponsor_sponsors` s ON (a.sponsor_id = s.id)
		          ORDER BY l.mtime DESC
		          LIMIT $limit;
		    ");
		return array ("items" => $items, "foundrows" => MysqlReadField("SELECT COUNT(`id`) FROM `".TblPrefix()."flip_sponsor_log`;"),);
	}

	function actionEmptyLog($dat) {
		global $User;
		$User->requireRight($this->RightEdit);

		$r = MysqlWrite("TRUNCATE TABLE `".TblPrefix()."flip_sponsor_log`");
		if ($r)
			LogChange("Der Sponsorenlog wurde geleert.");
		return $r;
	}

	function frameClick($get) {
		include_once ("inc/inc.sponsor.php");
		$id = addslashes($get["id"]);
		$r = MysqlReadRow("
		      SELECT s.homepage, a.link, a.id, a.enable_click_log, a.enable_counter FROM `".TblPrefix()."flip_sponsor_sponsors` s, `".TblPrefix()."flip_sponsor_ads` a 
		        WHERE ((s.id = a.sponsor_id) AND (a.id = '$id'))");
		$link = (empty ($r["link"])) ? $r["homepage"] : $r["link"];
		header("Location: $link");
		SponsorCounterInc(array ("clicks"), $r);
		SponsorDoLog("click", $get["location"], $r);
		$this->ShowFrame = false;
		return true;
	}
}

RunPage("SponsorPage");
?>
