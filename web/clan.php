<?php

/**
 * @author Daniel Raap
 * @version $Id: archiv.php 1702 2019-01-09 09:01:12Z loom $ edit by naaux and tominator
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 * @since 1378 - 24.02.2007
 * @neues Recht eingefuegt - AdminClanRight - im System das Recht clan_admin hinzufuegen u. der Gruppe Usermanager zuweisen.
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");

class ClanPage extends Page {
	//Rechte
	var $joinRight = "clan_join";
	var $leaderRight = "clan_leader";
	var $AdminClanRight = "clan_admin";
	var $ClanPaid = "status_paid_clan";
	//Content
	var $jointext = "clan_join";
	//Nachrichten
	var $joinMessage = "clan_join";
	
	/**
	 * Konstruktor
	 */
	 //php 7 public function __construct()
	//php 5 original function ClanPage()
	function __construct() {
		
		//php 5:
		//$this->Page();
		//php7 neu:		
		parent::__construct();
	}
	
	/**
	 * Liste mit allen Clans
	 */
	function framedefault($get, $post) {
		global $User;
		$this->Caption = text_translate("Clanliste");
		
		if($User->hasRight($this->joinRight)) {
			$create = true;
		} else {
			$create = false;
		}

		if($User->hasRight($this->AdminClanRight)) {
			$clanadmin = true;
		} else {
			$clanadmin = false;
		}
		
		$clans = GetSubjects("clan");
		include_once("mod/mod.url2html.php");
		foreach($clans AS $k=>$clan) {
			$clans[$k]["homepageLink"] = url2html($clan["homepage"]);
			if ($User->HasRight($this->AdminClanRight))
			{
				$clanObject = CreateSubjectInstance($clan["id"], "clan");
				if ($clanObject->HasRight($this->ClanPaid)) $clans[$k]["status_paid_clan"] = 1;
				else $clans[$k]["status_paid_clan"] = 0;
			}
		}

		$ownClans = $User->getParents("clan");
		
		return array("create"=>$create, "myclans"=>$ownClans, "clans"=>$clans, "clanadmin"=>$clanadmin);
	}
	
	function frameviewclan($get) {
		global $User;		
		if(is_numeric($get["id"])) {
			$clanId = $get["id"];
		} else {
			return trigger_error(text_translate("Es wurde kein Clan ausgew&auml;hlt!"),E_USER_ERROR);
		}
		
		$clan = CreateSubjectInstance($clanId, "clan");
		$this->Caption = $clan->name;
		$r["id"] = $clan->id;
		$r["clanadmin"] = $User->HasRight($this->AdminClanRight);
		//Eigenschaften
		$rawProps = $clan->getProperties();
		$cols = GetColumnDeteils("clan");
		//HTML-escapen
		$htmlProps = array("homepage");
		foreach($rawProps AS $key=>$val) {	
			if(!$User->hasRightOver($cols[$key]["required_view_right"], $clan->id)) {
				continue;
			}
			if($key == "homepage") {
				//HTML-Link
				include_once("mod/mod.url2html.php");
				$val = url2html($val);
			}
			if(!in_array($key, $htmlProps)) {
				$val = escapeHtml($val);
			}
			$r["props"][$cols[$key]["sort_index"]] = array_merge(array("value"=>$val), $cols[$key]);
			ksort($r["props"]);
		}

		//Mitglieder
		$memberIDs = array_keys($clan->getChilds());
		$where = "s.id IN (".implode_sqlIn($memberIDs).")";
		$leaders = $clan->getEntitled($this->leaderRight);
		$r["members"] = GetSubjects("user", array(), $where, "name");
		foreach($r["members"] AS $key => $member) {
			$r["members"][$key]["isleader"] = (isset($leaders[$member["id"]])) ? true : false;
		}
		
		//Anfragen
		$asking = MysqlReadCol("SELECT user_id FROM `".TblPrefix()."flip_clan_join` WHERE clan_id='".$clan->id."'", "user_id");
		$memberIDs = array_merge($asking, $memberIDs);
		if(!in_array($User->id, $memberIDs) && $User->hasRight($this->joinRight)) {
			$r["join"] = true;
		} else {
			$r["join"] = false;
		}
		$names = MysqlReadCol("SELECT id, name FROM `".TblPrefix()."flip_user_subject` WHERE id IN (".implode_sqlIn($asking).")", "name", "id");
		$r["asking"] = array();
		foreach($asking AS $uid) {
			$r["asking"][] = array("id"=>$uid, "name"=>$names[$uid]);
		}
		
		if($User->hasRightOver($this->leaderRight, $clan->id)) {
			$r["leader"] = true;
		} else {
			$r["leader"] = false;
		}
		
		//TODO - Hinzufuegen Abfrage ob du CLAN Member bist + Rechte erstellung fuer Frontend
		$UserFromClan = $get["clan_id"];
		$User->hasRight('logged_in');
		if($User->hasRight($this->logged_in)) {
			$r["clanmember"] = true;
		} else {
			$r["clanmember"] = false;
		}
		
		//Paid
		$r["status_paid_clan"] = ($clan->HasRight($this->ClanPaid, $clan->id));

		#if ($clan->HasRight($this->ClanPaid)) $rawProps[$r]["status_paid_clan"] = 1;
		#else $rawProps[$r]["status_paid_clan"] = 0;

		return $r;
	}
	
	function actionconfirm($get) {
		global $User;
		$clan = CreateSubjectInstance($get["clan_id"], "clan");
		if(is_a($clan, "Clan")) {
			$members = $clan->getChilds();
			if(!isset($members[$get["id"]])) {
				$User->requireRightOver($this->leaderRight, $clan->id);
				$member = CreateSubjectInstance($get["id"], "user");
				$clan->addChild($member);
				MysqlExecQuery("DELETE FROM `".TblPrefix()."flip_clan_join` WHERE user_id = '".$member->id."'");
			} else {
				trigger_error_text(text_translate("?1? ist bereits Mitglied.", GetSubjectName($get["id"])), E_USER_WARNING);
			}
		}
		Redirect(EditUrl(array("id"=>$clan->id,"action"=>"","clan_id"=>""), "", false));
	}
	
	function actiondelmember($get) {
		global $User;

		// Mod by Tominator
		// Sitzplan auf Reservierung ueberpruefen
		$UserToRemove = $get["id"];
		$UserFromClan = $get["clan_id"];
		$seats = MysqlReadArea("SELECT `name` FROM ".TblPrefix()."flip_seats_seats WHERE user_id=$UserToRemove AND user_id_clan=$UserFromClan");
		if (count ($seats) > 0) {
			$seatlist = "";
			for($i=0; $i < count($seats); $i++) {
				$seatlist .= $seats[$i]["name"];
				if ($i+1 < count($seats)) $seatlist .= ", ";
			}
			trigger_error(text_translate("Das Clan-Mitglied kann nicht entfernt werden, da es noch Reservierungen im Sitzplan gibt! ($seatlist)"), E_USER_ERROR);			
		}
		
		if($User->id != $get["id"]) {
			$clan = CreateSubjectInstance($get["clan_id"], "clan");
			if(is_a($clan, "Clan")) {
				$User->requireRightOver($this->leaderRight, $clan->id);
				if($clan->remChild($get["id"])) {
					trigger_error_text(GetSubjectName($get["id"]).text_translate(" wurde aus dem Clan entfernt."));
					LogAction("Clan: Aus dem Clan ".$clan->id." wurde von Leader ".$User->id." Mitglied ".$get["id"]." entfernt.");
				}
			}
		} else {
			trigger_error(text_translate("Du kannst dich nicht selbst aus dem Clan nehmen!"), E_USER_WARNING);
		}
		Redirect(EditUrl(array("action"=>""), "", false));
	}

	function actionleaveclan($get) {
		global $User;
		
		// Mod by Tominator
		// Sitzplan auf Reservierung ueberpruefen
		$UserToRemove = $get["id"];
		$UserFromClan = $get["clan_id"];
		$seats = MysqlReadArea("SELECT `name` FROM ".TblPrefix()."flip_seats_seats WHERE user_id=$UserToRemove AND user_id_clan=$UserFromClan");
		if (count ($seats) > 0) {
			$seatlist = "";
			for($i=0; $i < count($seats); $i++) {
				$seatlist .= $seats[$i]["name"];
				if ($i+1 < count($seats)) $seatlist .= ", ";
			}
			trigger_error(text_translate("Das Clan-Mitglied kann nicht entfernt werden, da es noch Reservierungen im Sitzplan gibt! ($seatlist)"), E_USER_ERROR);			
		}
		
		if($User->id != $get["id"]) {
			$clan = CreateSubjectInstance($get["clan_id"], "clan");
			if(is_a($clan, "Clan")) {
				$User->requireRightOver($this->leaderRight, $clan->id);
				if($clan->remChild($get["id"])) {
					trigger_error_text(GetSubjectName($get["id"]).text_translate(" wurde aus dem Clan entfernt."));
					LogAction("Clan: Aus dem Clan ".$clan->id." wurde von Leader ".$User->id." Mitglied ".$get["id"]." entfernt.");
				}
			}
		} else {
			trigger_error(text_translate("Du kannst dich nicht selbst aus dem Clan nehmen!"), E_USER_WARNING);
		}
		Redirect(EditUrl(array("action"=>""), "", false));
	}
	
	
	function actionnominate($post) {
		if(count($post['ids']) > 0)
			$this->_editUsers($post, 'addLeader');
		Redirect(EditUrl(array("action"=>""), "", false));
	}
	
	function _editUsers($post, $action = 'addLeader') {
		if(!in_array($action, array('addLeader','delLeader'))) {
			return null;
		}
		$clan = CreateSubjectInstance($this->FrameVars['id'], "clan");
		if(is_object($clan)) {
			$members = $clan->getChilds('user');
			$added = array();
			foreach(array_intersect(array_keys($members), $post['ids']) AS $uid) {
				switch ($action) {
					case 'addLeader':
						_AddRight($uid, $this->leaderRight, $clan->id);
						$text = ' zum Leader ernannt.';
						break;
					case 'delLeader':
						_RemRight($uid, $this->leaderRight, $clan->id);
						$text = ' als Leader aberkannt.';
						break;
				}
				$added[] = $members[$uid];
			}
			trigger_error_text(join($added, ', ').$text);
		}
	}
	
	function actiondeprive($post) {
		global $User;
		ArrayWithKeys($post, array('ids'));
		if(!is_array($post['ids'])) {
			$post['ids'] = array();
		}
		$myKey = array_search($User->id, $post['ids']);
		if($myKey !== false)
			unset($post['ids'][$myKey]);
		if(count($post['ids']) > 0)
			$this->_editUsers($post, 'delLeader');
		else
			trigger_error_text('Keine anderen Mitglieder ausgew&auml;hlt.',E_USER_WARNING);
		Redirect(EditUrl(array("action"=>""), "", false));
	}
	
	function actiondel($get) {
		global $User;
		$clan = CreateSubjectInstance($get["id"], "clan");
		if(is_a($clan, "Clan")) {
			$User->requireRightOver($this->leaderRight, $clan->id);
			if(DeleteSubject($clan->id)) {
				$msg = "Der Clan ".$clan->name." wurde gel&ouml;scht.";
				LogAction($msg);
				trigger_error($msg);
			}
		}
		Redirect("clan.php");
	}

	function frameeditclan($get) {
		global $User;
		$r = array();
		require_once("user.php");
		$userpage = new UserPage();
		$get["type"] = "clan";
		if(empty($get["id"])) {
			//neu
			$this->Caption = text_translate("Clan erstellen");
			$User->requireRight($this->leaderRight);
			$get["id"] = "create_new_subject";
		} elseif($get["id"] != "create_new_subject") {
			//bearbeiten
			$this->Caption = text_translate("Clan bearbeiten");
			$clan = CreateSubjectInstance($get["id"], "clan");
			$User->requireRightOver($this->leaderRight, $clan);
		}
		return $userpage->frameeditclanproperties($get);
	}
	
	function submitclan($post) {
		global $User;
		$create = false;
		if($post["id"] == "create_new_subject") {
			$User->requireRight($this->joinRight);
			$create = true;
		} else {
			$User->requireRightOver($this->leaderRight, $post["id"]);
		}
		if($post["type"] == "clan") {
			unset($post["type"]);
			if($create) {
				$post["id"] = CreateSubject("clan", $post["name"]);
			}
			$clan = CreateSubjectInstance($post["id"], "clan");
			if(is_a($clan, "Clan")) {
				if($create) {
					LogAction("Der Clan '".$clan->name."' wurde erstellt.");
					$allClansGroup = "clans";
					$allClans = CreateSubjectInstance($allClansGroup, "group");
					$allClans->addChild($clan);
					$clan->addChild($User);
					$User->addRight($this->leaderRight, $clan->id);
				}
				unset($post["id"]);
				$clan->setProperties($post);
				return true;
			}
		}
		return false;
	}
	
	function framejoin($get) {
		global $User;
		$User->requireRight($this->joinRight);
		$r = $get;
		include_once("inc/inc.text.php");
		$r["jointext"] = LoadText($this->jointext, $this->Caption);
		$clan = CreateSubjectInstance($get["id"]);
		$this->Caption .= " '".$clan->name."'";
		return $r;
	}
	
	function submitjoin($post) {
		global $User;
		$User->requireRight($this->joinRight);
		if(isset($post["captcha"]) && $post["yes"]) {
			if(MysqlWriteByID(TblPrefix()."flip_clan_join", array("user_id"=>$User->id, "clan_id"=>$post["id"]))) {
				$clan = CreateSubjectInstance($post["id"], "clan");
				$leaderIDs = array_keys(GetRightOwners($this->leaderRight, $clan->id));
				$confirmLink = ServerURL().EditUrl(array("clan_id"=>$clan->id, "action"=>"confirm", "id"=>$User->id, "frame"=>"viewclan"), "", false);;
				$this->_webMessage($this->joinMessage, $leaderIDs, array("clanname"=>$clan->name, "member"=>$User->name, "link"=>$confirmLink));
				$this->SubmitMessage = text_translate("Deine Anfrage wurde an den Clan geleitet.");
				return true;
			}
		}
		return false;
	}

	function actiondelclanpaid($data) {
		global $User;
		$User->requireRight($this->AdminRightsRight);

		$clan = CreateSubjectInstance($data["id"], "clan");	
		if ($clan->remRight(GetRightID("status_paid_clan"), 0)) {
			trigger_error_text('Status bezahlt konnte nicht entfernt werden.',E_USER_WARNING);
		}
		$log = text_translate("Bezahlt Status des Clans \"" . $clan->name . "\" wurde entfernt");
		LogChange($log);
	}

	function submitSetClanPaid($data) {
		global $User;
		$User->requireRight($this->AdminRightsRight);
		
		$clan = CreateSubjectInstance($data["id"], "clan");	
		if (!(is_numeric ($data["amount"]))) 
			trigger_error_text('Wert muss eine Zahl sein.',E_USER_WARNING);
		else {
			if ($clan->setProperty("clan_seat_limit", $data["amount"])) ;			
		
			$r = $clan->addRight(GetRightID("status_paid_clan"), 0);		
			if ($r) {
				$log = text_translate("Clan \"" . $clan->name . "\" wurde auf bezahlt gesetzt");
				LogChange($log);
			}
			return $r;
		}			
	}
	/**
	 * Sendet eine Nachricht an die angegebenen Empf&auml;nger
	 * 
	 * @access private
	 * @param String $message_name Name der Nachricht in der Datenbank
	 * @param Array $receivers Array mit UserIDs
	 * @param Array $vars Keys sind Variablen in der Nachricht
	 */
	function _webMessage($message_name, $receivers, $vars) {
		global $User;
		include_once("mod/mod.sendmessage.php");
		$m = new ExecMessage();
		$m->messageFromDB($message_name);
		$m->Params = $vars;
		foreach($receivers AS $uid) {
			$m->sendMessage(CreateSubjectInstance($uid));
		} 
	}
}

RunPage("ClanPage");
?>
