<?php
/**
 * @author Matthias Gross
 * @version $Id: inet.php 1702 2019-01-09 09:01:12Z scope $ edit by VulkanLAN
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ('core/core.php');
require_once ('inc/inc.page.php');

/** INet-Modul */
require_once ('mod/mod.inet.php');
require_once ('inc/inc.text.php');

class INetPage extends Page {

	var $INetUseRight	= 'inet_view';
	var $INetAdminRight	= 'inet_admin';
	
	var $INetModule;

	//php 7 public function __construct()
	//php 5 original function INetPage()
	function __construct() {
		global $User;
		//php 5:
		//$this->Page();
		//php7 neu:
		parent::__construct();

		if(strtolower($this->Frame) != 'getips')
			$User->requireRight($this->INetUseRight);
			
		$this->INetModule = new FLIPINetModule();
	}


	function frameDefault($get, $post) {
		global $User;
		
		$info = $this->INetModule->GetUserInfo($User->id);
		
		$r['text']			= LoadText('inet_main', $this->Caption);
		$r['status'] 	  	= $this->INetModule->GetRequestStatus($User->id);
		$r['userid'] 	  	= $User->id;
		$r['admin']  	  	= $User->hasRight($this->INetAdminRight);
		$r['allowmultiple']	= $this->INetModule->_AllowMultipleRequests;
		$r['req_caption'] 	= (($info['status'] == 'expired') && ($r['allowmultiple'])) ? 'Antrag erneut stellen' : 'Antrag stellen';
		
		if($r['status'] == 'waiting')
			$r['queueposition'] = $info['queuepos'];
		elseif($r['status'] == 'granted') {
			$r['timeleft'] = ($info['timeleft'] > 0) ? $this->INetModule->Seconds2Time($info['timeleft']) : '';
		}
				
		return $r;
	}
	
	function actionRequestAccess($g) {
		if(!$this->INetModule->RequestAccess($g['uid']))
			trigger_error_text('Der Antrag konnte nicht gestellt werden!',E_USER_ERROR);
		else
			trigger_error_text('Antrag gestellt!',E_USER_NOTICE);		
	}
	
	function actionWithdrawRequest($g) {
		if(!$this->INetModule->UnRequestAccess($g['uid']))
			trigger_error_text('Der Antrag konnte nicht zur&uuml;ckgenommen werden!',E_USER_ERROR);
		else
			trigger_error_text('Dein Antrag wurde zur&uuml;ckgenommen!',E_USER_NOTICE);
		// n&ouml;tig, da u.U. sonst alter Content gezeigt wird
		$this->NextPage = 'inet.php';
	}
		
	function frameGetIPs($get) {
		// Alle IPs roh und im eingestellten Format ausgeben
		// Und nur an den eingestellten Rechner (Security ;))
		$myip = $_SERVER['REMOTE_ADDR'];
		echo $this->INetModule->GetFormattedUserIPs($myip);
		return true;
	}
	
	/******************************************************************************/
	/*                            Admin area - REQUESTS                           */
	/******************************************************************************/
	
	function frameManageRequests($get) {
		global $User;
		$User->requireRight($this->INetAdminRight);
		$this->Caption = 'Antr&auml;ge verwalten';
		$r['requests'] = MysqlReadArea("SELECT * FROM `".TblPrefix()."flip_inet_requests`;","id");
		$r['states'] = array('waiting' => 'Wartend','granted' => 'Freigeschalten','expired' => 'Abgelaufen','noexpiry' => 'Dauerhaft freigeschalten');
		foreach($r['requests'] as $id => $req) {
			$u = CreateSubjectInstance($req['user_id']);
			$r['requests'][$id]['username'] = $u->name;
			$r['requests'][$id]['online_since'] = ($req['status'] == 'granted') ? date('d.m.Y G:i',$req['online_since']) : '-';
			$r['requests'][$id]['online_time'] = (($r['requests'][$id]['online_since'] == '-') && ($req['status'] == 'noexpiry')) ? 'Unbegrenzt' : $this->INetModule->Seconds2Time($req['online_time']);
			$r['requests'][$id]['online_until'] = ($req['status'] == 'granted') ? date('d.m.Y G:i',$req['online_since'] + $req['online_time']) : '-';
		}
		return !empty($r) ? $r : array(); 
	}
	
	function actionChangeStatus($g) {
		global $User;
		$User->requireRight($this->INetAdminRight);
		if(!empty($g['ids']) && is_array($g['ids'])) {
			$ids = implode_sqlIn($g['ids']);
			$status = escape_sqlData_without_quotes($g['newstatus']);
			$onlinetimes = MysqlReadCol("SELECT `user_id`,`online_time` FROM `".TblPrefix()."flip_inet_requests` WHERE `id` IN (".implode_sqlIn($g['ids']).");",'online_time','user_id');
			switch($status) {
				case 'waiting':
				case 'noexpiry':
					$additional = ", `online_since` = '0'";
					break;
				case 'granted':
					$time = time();
					$additional = ", `online_since` = '".$time."'";
					$params = array('inet_granttime' => date('d.m.Y \u\m G:i \U\h\r',$time),'onlinetimes' => $onlinetimes);
					break;
				default:
					$additional = '';
			}
			if(MysqlWrite("UPDATE `".TblPrefix()."flip_inet_requests` SET `status` = '$status' $additional WHERE `id` IN ($ids);")) {
				$this->INetModule->SendINetNotice(array_keys($onlinetimes),$status,$params);
				trigger_error_text('Der Status wurde auf '.$status.' ge&auml;ndert!',E_USER_NOTICE);
			}
			else
				trigger_error_text('Der Status konnte nicht ge&auml;ndert werden!',E_USER_NOTICE);			
		}
	}
	
	function actionChangePriority($g) {
		global $User;
		$User->requireRight($this->INetAdminRight);
		if(!empty($g['ids']) && is_array($g['ids'])) {
			$ids = implode_sqlIn($g['ids']);
			$priority = escape_sqlData_without_quotes($g['newpriority']);
			if(MysqlWrite("UPDATE `".TblPrefix()."flip_inet_requests` SET `priority` = '$priority' WHERE `id` IN ($ids);"))
				trigger_error_text('Die Priorit&auml;t wurde auf '.$priority.' ge&auml;ndert!',E_USER_NOTICE);
			else
				trigger_error_text('Die Priorit&auml;t konnte nicht ge&auml;ndert werden!',E_USER_NOTICE);	
		}
	}
	
	function actionChangeOnlineTime($g) {
		global $User;
		$User->requireRight($this->INetAdminRight);
		if(!empty($g['ids']) && is_array($g['ids'])) {
			$ids = implode_sqlIn($g['ids']);
			$online_time = escape_sqlData_without_quotes(60 * $g['new_online_time']); // Unsauber wegen typen but who cares...
			if(MysqlWrite("UPDATE `".TblPrefix()."flip_inet_requests` SET `online_time` = '$online_time' WHERE `id` IN ($ids);"))
				trigger_error_text('Die Online-Zeit wurde auf '.$g['new_online_time'].' Minuten ge&auml;ndert!',E_USER_NOTICE);
			else
				trigger_error_text('Die Online-Zeit konnte nicht ge&auml;ndert werden!',E_USER_NOTICE);
		}
	}
	
	function actionDeleteRequest($g) {
		global $User;
		$User->requireRight($this->INetAdminRight);
		if(!empty($g['ids']) && is_array($g['ids'])) {
			$ids = implode_sqlIn($g['ids']);
			if(MysqlWrite("DELETE FROM `".TblPrefix()."flip_inet_requests` WHERE `id` IN ($ids);"))
				trigger_error_text('Die Antr&auml;ge wurden gel&ouml;scht!',E_USER_NOTICE);
			else
				trigger_error_text('Die Antr&auml;ge konnten nicht gel&ouml;scht werden!',E_USER_NOTICE);
		}
	}
	
	/******************************************************************************/
	/*                            Admin area - GROUPS                             */
	/******************************************************************************/
	
	function frameManageGroups($get) {
		global $User;
		$User->requireRight($this->INetAdminRight);
		$this->Caption = 'Priorit&auml;tengruppen verwalten';
		$r['groups'] = MysqlReadArea("SELECT * FROM `".TblPrefix()."flip_inet_groups`;","id");
		foreach($r['groups'] as $id => $grp) {
			$g = CreateSubjectInstance($grp['subject_group_id']);
			$r['groups'][$id]['subject_group_name'] = $g->name;
			$r['groups'][$id]['online_time'] = $this->INetModule->Seconds2Time($grp['online_time']);
		}
		return !empty($r) ? $r : array();
	}
	
	function actionChangeGroupPriority($g) {
		global $User;
		$User->requireRight($this->INetAdminRight);
		if(!empty($g['ids']) && is_array($g['ids'])) {
			$ids = implode_sqlIn($g['ids']);
			$priority = escape_sqlData_without_quotes($g['newpriority']);
			if(MysqlWrite("UPDATE `".TblPrefix()."flip_inet_groups` SET `priority` = '$priority' WHERE `id` IN ($ids);"))
				trigger_error_text('Die Priorit&auml;t wurde auf '.$priority.' ge&auml;ndert!',E_USER_NOTICE);
			else
				trigger_error_text('Die Priorit&auml;t konnte nicht ge&auml;ndert werden!',E_USER_NOTICE);	
		}		
	}
	
	function actionChangeGroupOnlineTime($g) {
		global $User;
		$User->requireRight($this->INetAdminRight);
		if(!empty($g['ids']) && is_array($g['ids'])) {
			$ids = implode_sqlIn($g['ids']);
			$online_time = escape_sqlData_without_quotes(60 * $g['new_online_time']);
			if(MysqlWrite("UPDATE `".TblPrefix()."flip_inet_groups` SET `online_time` = '$online_time' WHERE `id` IN ($ids);"))
				trigger_error_text('Die Online-Zeit wurde auf '.$g['new_online_time'].' Minuten ge&auml;ndert!',E_USER_NOTICE);
			else
				trigger_error_text('Die Online-Zeit konnte nicht ge&auml;ndert werden!',E_USER_NOTICE);
		}		
	}
	
	function actionDeleteGroup($g) {
		global $User;
		$User->requireRight($this->INetAdminRight);
		if(!empty($g['ids']) && is_array($g['ids'])) {
			$ids = implode_sqlIn($g['ids']);
			if(MysqlWrite("DELETE FROM `".TblPrefix()."flip_inet_groups` WHERE `id` IN ($ids);"))
				trigger_error_text('Die Gruppen wurden gel&ouml;scht!',E_USER_NOTICE);
			else
				trigger_error_text('Die Gruppen konnten nicht gel&ouml;scht werden!',E_USER_NOTICE);
		}		
	}
	
	function frameAddGroup($get) {
		global $User;
		$User->requireRight($this->INetAdminRight);
		$this->Caption = 'Gruppe hinzuf&uuml;gen';
		$groups = MysqlReadCol("SELECT `subject_group_id` FROM `".TblPrefix()."flip_inet_groups`;",'subject_group_id','subject_group_id');
		$where = (!empty($groups)) ? "`id` NOT IN(".implode_sqlIn($groups).")" : "";
		foreach(GetSubjects('group',array('id','name'),$where) as $g)
			$r['groups'][$g['id']] = $g['name'];
		return $r;
	}

	function submitAddGroup($get) {
		global $User;
		$User->requireRight($this->INetAdminRight);
		if(empty($get)) return false;
		$get['online_time'] *= 60;
		return MysqlWriteByID(TblPrefix().'flip_inet_groups',$get);
	}
}

RunPage('INetPage');

?>
