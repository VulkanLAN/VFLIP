<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: webmessage.php 1702 2019-01-09 09:01:12Z loom $ edit by naaux
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");

class WebmessagePage extends Page {
	var $User;
	var $MessagingRight = "webmessage_use";
	var $TextContact = "webmessage_contact";
	var $MaxLenConfig = "webmessage_menu_max_len";

	//php 7 public function __construct()
	//php 5 original function WebmessagePage()
	function __construct() {
		global $User;
		//php 5:
		//parent :: Page();
		//php7 neu:
		parent::__construct();
		$u = (empty ($this->FrameVars["id"])) ? $User : $this->FrameVars["id"];
		$this->User = & CreateSubjectInstance($u, "user");
	}

	function frameDefault($get, $post) {
		return $this->getHeader("&uuml;bersicht");
	}

	function getHeader($Caption) {
		global $User;
		$User->requireRightOver($this->MessagingRight, $this->User);
		$u = ($this->User->id == $User->id) ? "" : "(" . $this->User->name . ") ";
		$this->Caption = "WebMessaging {$u}-> $Caption";
		return array (
			"user_id" => $this->User->id,
			"frame" => $this->Frame
		);
	}

	function getMessages($Box, $folder_id = null) {
		if (empty ($folder_id))
			$folder_id = "IS NULL";
		else
			$folder_id = "= '" .escape_sqlData_without_quotes($folder_id) . "'";
		$msgs = MysqlReadArea("
		      SELECT m.*, o.name AS `owner`, s.name AS `sender`, p.name AS `processor`
		        FROM (`" . TblPrefix() . "flip_webmessage_message` m)
		          LEFT JOIN `" . TblPrefix() . "flip_user_subject` o ON (o.id = m.owner_id)
		          LEFT JOIN `" . TblPrefix() . "flip_user_subject` s ON (s.id = m.sender_id)
		          LEFT JOIN `" . TblPrefix() . "flip_user_subject` p ON (p.id = m.processor_id)
		        WHERE ((m.{$Box}_id = '{$this->User->id}') AND (m.{$Box}_deleted = 0) AND (m.folder_id $folder_id))
		        ORDER BY m.date DESC;
		    ", "id");
		if (!empty ($msgs)) {
			$ids = implode_sqlIn(array_keys($msgs));
			$related = MysqlReadArea("SELECT `source_id`, `source_type` FROM `" . TblPrefix() . "flip_webmessage_message` WHERE (`source_id` IN ($ids))");
			foreach ($related as $r)
				$msgs[$r["source_id"]]["is_$r[source_type]"] = true;
		}
		return $msgs;
	}

	function getMessage($ID) {
		global $User;
		$id = addslashes($ID);
		$r = MysqlReadRow("
		      SELECT m.*, o.name AS `owner`, s.name AS `sender`, p.name AS `processor`
		        FROM (`" . TblPrefix() . "flip_webmessage_message` m)
		          LEFT JOIN `" . TblPrefix() . "flip_user_subject` o ON (o.id = m.owner_id)
		          LEFT JOIN `" . TblPrefix() . "flip_user_subject` s ON (s.id = m.sender_id)
		          LEFT JOIN `" . TblPrefix() . "flip_user_subject` p ON (p.id = m.processor_id)
		        WHERE (m.id = '$id');
		    ");

		// nur der sender darf den bearbeiter kennen
		if ($r["sender_id"] != $this->User->id)
			unset ($r["processor_id"]);

		if (!$User->hasRightOver($this->MessagingRight, $r["owner_id"]) and !$User->hasRightOver($this->MessagingRight, $r["sender_id"]))
			trigger_error_text("Du bist nicht berechtigt, diese Nachricht anzusehen.", E_USER_ERROR);
		return $r;
	}

	function getFolders($inbox = true) {
		$r = MysqlReadCol("SELECT id, name
		                       FROM " . TblPrefix() . "flip_webmessage_folder
		                       WHERE user_id='" . $this->User->id . "'
		                       ORDER BY name
		                      ", "name", "id");
		if ($inbox) {
			$r[0] = text_translate("Posteingang");
		}
		return $r;
	}

	function frameOwner($get) {
		$r = $this->getHeader("Posteingang");
		ArrayWithKeys($get, array("folder_id"));
		$r["messages"] = $this->getMessages("owner", $get["folder_id"]);
		$r["allfolders"] = $this->getFolders();
		$r["folders"] = $this->getFolders(false);
		if (!empty ($get["folder_id"])) {
			if(isset($r["allfolders"][$get["folder_id"]])) {
				$this->Caption .= " -> " . $r["allfolders"][$get["folder_id"]];
			} else {
				trigger_error_text(text_translate("Es wurde ein Ordner angegeben, der dir nicht geh&ouml;rt"), E_USER_WARNING);
			}
		}
		return $r;
	}

	function frameSender($get) {
		$r = $this->getHeader("Postausgang");
		$r["messages"] = $this->getMessages("sender");
		return $r;
	}

	function frameeditfolder($get) {
		ArrayWithKeys($get, array("id","folder_id"));
		$this->Caption = text_translate("Ordnerverwaltung");
		$r["folders"] = $this->getFolders(false);
		$r["folder_id"] = (integer) $get["folder_id"];
		$r["user_id"] = $get["id"];
		return $r;
	}

	function submitfolder($post) {
		global $User;
		$User->requireRightOver($this->MessagingRight, $this->User);

		$data = array (
			"name" => $post["foldername"],
			"user_id" => $this->User->id
		);

		if (MysqlWriteByID(TblPrefix() . "flip_webmessage_folder", $data, $post["folder_id"]))
			return true;
		else
			return false;
	}

	function submitdelfolder($post) {
		global $User;
		$User->requireRightOver($this->MessagingRight, $this->User);
		if (!empty ($post["folder_id"])) {
			$msgs = MysqlReadCol("SELECT id FROM " . TblPrefix() . "flip_webmessage_message WHERE folder_id='" .escape_sqlData_without_quotes($post["folder_id"]) . "'", "id");
			$this->actionDelete(array (
				"ids" => $msgs,
				"type" => "owner"
			));
			if (MysqlDeleteByID(TblPrefix() . "flip_webmessage_folder", $post["folder_id"]))
				return true;
		}
		return false;
	}

	function frameSendMessage($get) {
		ArrayWithKeys($get, array("message_id","type"));
		$r = $this->getHeader("neue Nachricht");
		$r["source_id"] = (isset($get["message_id"])) ? $get["message_id"] : "";
		$r["source_type"] = (isset($get["type"])) ? $get["type"] : "";
		if (isset($get["message_id"]) && !empty ($get["message_id"])) {
			$m = $this->getMessage($get["message_id"]);
			$msg = "";
			foreach (explode("\n", $m["text"]) as $l) {
				$l = chop($l);
				$msg .= (empty ($l)) ? "\n" : "> $l\n";
			}
			switch ($get["type"]) {
				case ("resend") :
					$r["subject"] = $m["subject"];
					$r["message"] = $m["text"];
					$r["receiver"] = $m["owner"];
					break;
				case ("oforward") :
				case ("sforward") :
					$f = array ();
					if (preg_match("/^Fw(\d*): /", $m["subject"], $f)) {
						$fw = "Fw" . (($f[1] < 2) ? 2 : $f[1] + 1) . ": ";
						$r["subject"] = preg_replace("/^Fw(\d*): /", $fw, $m["subject"]);
					} else {
						$r["subject"] = "Fw: $m[subject]";
					}
					$r["message"] = $msg;
					break;
				case ("replay") :
					$r["receiver"] = $m["sender"];
					if (preg_match("/^Re(\d*): /", $m["subject"], $f)) {
						$re = "Re" . (($f[1] < 2) ? 2 : $f[1] + 1) . ": ";
						$r["subject"] = preg_replace("/^Re(\d*): /", $re, $m["subject"]);
					} else {
						$r["subject"] = "Re: $m[subject]";
					}
					$r["message"] = $msg;
					break;
			}
		}
		$r['append_signature'] = true;
		return $r;
	}

	function submitMessage($post) {
		global $User;
		// an den Systemuser darf jeder senden.
		if ($post['receiver'] != SYSTEM_USER_ID)
			$User->requireRightOver($this->MessagingRight, $this->User);
			
		ArrayWithKeys($post, array('append_signature','receiver','subject','message','source_id','source_type'));
		
		// Signatur?
		$sig = $post['append_signature'];

		include_once ('mod/mod.sendmessage.php');
		$this->SubmitMessage = 'Die WebMessage wurde gesendet.';
		return SendMessageSendMessage($post['receiver'], $this->User, $post['subject'], $post['message'], $post['source_id'], $post['source_type'], 'custom', $sig);
	}

	function frameViewMessage($get) {
		global $User;
		include_once ("mod/mod.url2html.php");
		$r = $this->getMessage($get["message"]);
		$r += $this->getHeader($r["subject"]);
		//Edit VulkanLAN - Send Message as HTML:
		$sendmessage_format_html_global = ConfigGet("sendmessage_format_html");
			if ($sendmessage_format_html_global != 'Y'){
		$r["text"] = nl2br(url2html(escapeHtml($r["text"])));
				}
			else {
				$r["text"] = (url2html($r["text"]));
				}
		$r["box"] = $get["box"];

		//als gelesen markieren wenn nicht der Absender
		if ($r["status"] == "unread" && ($User->id != $r["sender_id"] || $User->id == $r["owner_id"]))
			MysqlWriteByID(TblPrefix() .
			"flip_webmessage_message", array (
				"status" => "read"
			), $r["id"]);
		if(!isset($r["processor_id"])) $r["processor_id"] = false;
		if ($r["processor_id"] == $r["sender_id"])
			$r["processor_id"] = false;

		$r["related"] = MysqlReadArea("
		      SELECT m.*, o.name AS `owner` FROM (`" . TblPrefix() . "flip_webmessage_message` m)
		        LEFT JOIN `" . TblPrefix() . "flip_user_subject` o ON (o.id = m.owner_id)
		        WHERE (m.source_id = $r[id]) AND (m.sender_id = {$this->User->id});");

		return $r;
	}

	function actionDelete($post) {
		global $User;
		$type = ($post["type"] == "sender") ? "sender" : "owner";
		$other = ($post["type"] == "sender") ? "owner" : "sender";
		if (is_array($post["ids"]))
			foreach ($post["ids"] as $id) {
				$id = addslashes($id);
				$r = MysqlReadRow("SELECT `{$type}_id` AS `user`,`{$other}_deleted` AS `deleted` FROM `" . TblPrefix() . "flip_webmessage_message` WHERE (`id` = '$id');");
				$User->requireRightOver($this->MessagingRight, $r["user"]);

				if ($r["deleted"] == 1)
					$d = MysqlDeleteByID(TblPrefix() . "flip_webmessage_message", $id);
				else
					$d = MysqlWriteByID(TblPrefix() . "flip_webmessage_message", array (
						"{$type}_deleted" => "1"
					), $id);
				if ($d)
					LogAction("Die Webmessage mit der ID $id wurde gel&ouml;scht.");
			}
	}

	function actionSetstatus($post) {
		global $User;
		if (is_array($post["ids"]))
			foreach ($post["ids"] as $id) {
				$User->requireRightOver($this->MessagingRight, MysqlReadFieldByID(TblPrefix() . "flip_webmessage_message", "owner_id", $id));
				MysqlWriteByID(TblPrefix() . "flip_webmessage_message", array (
					"status" => $post["status"]
				), $id);
			}
	}

	function actionMoveToFolder($post) {
		global $User;
		if (empty ($post["folder_id"]))
			$post["folder_id"] = null;
		if (is_array($post["ids"]))
			foreach ($post["ids"] as $id) {
				$User->requireRightOver($this->MessagingRight, MysqlReadFieldByID(TblPrefix() . "flip_webmessage_message", "owner_id", $id));
				MysqlWriteByID(TblPrefix() . "flip_webmessage_message", array (
					"folder_id" => $post["folder_id"]
				), $id);
			}
	}

	function frameContact($get) {
		include_once ("inc/inc.text.php");
		global $User;
		$p = $User->getProperties(array (
			"givenname",
			"familyname"
		));
		$text = (empty ($get["text"])) ? $this->TextContact : $get["text"];
		return array (
			"text" => Loadtext($text,
			$this->Caption
		), "loggedin" => ($User->isLoggedIn()) ? true : false, "name" => "$p[givenname] $p[familyname] aka {$User->name}", "email" => $User->email, "prefix" => (empty ($get["prefix"])) ? "Kontakt" : $get["prefix"],);
	}

	function submitContact($data) {
		global $User;
		if(!empty($data["Sicherheitsabfrage"])) {
			if (!$User->isLoggedin()) {
				$data["message"] .= "\n\n-- \n $data[name]\n$data[email]";
				$data["subject"] = "$data[prefix] ($data[name]): $data[subject]";
			} else {
				$data["subject"] = "$data[prefix]: $data[subject]";
			}
			$data["receiver"] = SYSTEM_USER_ID;
			return $this->submitMessage($data);
		} else {
			trigger_error_text("Die Sicherheitsabfrage fehlt.|Evtl. fehlt im Template das #INPUT (siehe default-Template).", E_USER_WARNING);
		}
	}

	function frameSmallNewMessages($get) {
		global $User;
		$maxlen = ConfigGet($this->MaxLenConfig);
		if (!$User->hasRight($this->MessagingRight))
			return true;
		$items = MysqlReadArea("
		        SELECT `id`,`subject`,`status` FROM `" . TblPrefix() . "flip_webmessage_message` 
		          WHERE ((`owner_id` = '{$User->id}') AND (`status` != 'processed') AND (`owner_deleted` = 0));
		      ");
		if (empty ($items))
			return true;
		if ($maxlen > 0)
			foreach ($items AS $k => $v)
				$items[$k]["subject"] = substr($v["subject"], 0, $maxlen);
		return array (
			"userid" => $User->id,
			"items" => $items
		);
	}
	
	function frameMenueNewMessages($get) {
		global $User;
		$maxlen = ConfigGet($this->MaxLenConfig);
		if (!$User->hasRight($this->MessagingRight))
			return true;
		$items = MysqlReadArea("
		        SELECT `id`,`subject`,`status` FROM `" . TblPrefix() . "flip_webmessage_message` 
		          WHERE ((`owner_id` = '{$User->id}') AND (`status` != 'processed') AND (`owner_deleted` = 0));
		      ");
		if (empty ($items))
			return true;
		if ($maxlen > 0)
			foreach ($items AS $k => $v)
				$items[$k]["subject"] = substr($v["subject"], 0, $maxlen);
		return array (
			"userid" => $User->id,
			"items" => $items
		);
	}
}

RunPage("WebmessagePage");
?>
