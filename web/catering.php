<?php

/**
 * Das Catering wird folgenderma&szlig;en abgewickelt:
 * Kurzfassung:
 * Warenkorb->(evtl. Warteliste)->bestellt->bezahlt->geliefert(->extra: weitergeleitet)
 *
 * Langfassung:
 * 1. (orga) erstellt Kategorien nach denen das Angebot gegliedert werden soll
 * 2. (orga) tr&auml;gt f&uuml;r jede Kategorie Artikel ein
 * 3. (user) w&auml;hlt eine Kategorie
 * 4. (user) w&auml;hlt die Anzahl der Artikel die er bestellen m&ouml;chte,
 *           diese werden im Warenkorb abgelegt
 * 5. (user) schickt den Warenkorb ab, nicht gew&uuml;nschte Artikel werden nicht mitbestellt
 * 6. (user/orga) Artikel werden bezahlt, Orga setzt Status auf bezahlt
 * 7. (orga) setzt Status auf geliefert, wenn die Artikel den User erreicht haben.
 *
 * @author Daniel Raap
 * @version $Id: catering.php 1702 2019-01-09 09:01:12Z loom $ edit by naaux
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");
require_once ("inc/inc.text.php");

class CateringPage extends Page {
	//Rechte
	var $userright = "catering_user";
	var $adminright = "catering_admin";
	var $disabledright = "_infinite_";
	//Texte
	var $welcometext = "catering_title";
	var $edittext = "catering_edit";
	var $verwaltungstext = "catering_verwaltung";
	var $edit_items_text = 'catering_edit_item';

	//Konstruktor
	//php 7 public function __construct()
	//php 5 original function CateringPage()
	function __construct() {
		//php 5:
		//$this->Page();
		//php7 neu:
		parent::__construct();
		$this->valid_status = MysqlColEnum(TblPrefix() . "flip_catering_bestellungen", "status");
	}

	function isValidStatus($status) {
		return in_array($status, $this->valid_status);
	}

	function framedefault($get, $post) {
		global $User;
		if ($User->hasRight($this->userright))
			$r["logged_in"] = "true";
		if ($User->hasRight($this->adminright))
			$r["admin"] = "true";
		$r["text"] = LoadText($this->welcometext, $this->Caption);
		$r["kategorien"] = MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_catering_kategorien` WHERE `sichtbar`='1'");
		$r["count"] = count($r["kategorien"]);
		return $r;
	}

	function framelist($get) {
		global $User;
		$categoryid = escape_sqlData_without_quotes($get["kategorie"]);
		$r = MysqlReadRowByID(TblPrefix() . "flip_catering_kategorien", $categoryid);
		$this->Caption = "Artikel&uuml;bersicht von " . $r["name"];
		//KategorienMen&uuml;
		$r["kategorien"] = MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_catering_kategorien` WHERE aktiv='1' && sichtbar='1'");
		//nicht sichtbare Artikel werden ausgeblendet!
		$r["artikel"] = MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_catering_artikel` WHERE `kategorien_id`='$categoryid' AND `sichtbar`='1'");
		//eigene Bestellungen zu dem Artikel z&auml;hlen
		$myorder = MysqlReadArea("SELECT artikel_id, SUM(anzahl) AS `anzahl` FROM `" . TblPrefix() . "flip_catering_bestellungen` WHERE `subject_id`='" . $User->id . "' AND status!='warenkorb' GROUP BY artikel_id", "artikel_id");
		foreach ($r["artikel"] AS $key => $artikel) {
			$r["artikel"][$key]["myorder"] = (empty ($myorder[$artikel["id"]]["anzahl"])) ? 0 : $myorder[$artikel["id"]]["anzahl"];
		}
		if (!($r["warenkorbcount"] = MysqlReadField("SELECT SUM(anzahl) AS `anzahl` FROM `" . TblPrefix() . "flip_catering_bestellungen` WHERE `subject_id`='" . $User->id . "' AND `status`='warenkorb' GROUP BY subject_id", "anzahl", true)))
			$r["warenkorbcount"] = 0;
		if ($User->hasRight($this->userright))
			$r["logged_in"] = "true";
		return $r;
	}

	function submitbestellung($post) {
		global $User;
		if (empty ($post["anzahl"])) {
			trigger_error_text("Es wurde keine Anzahl eingegeben!", E_USER_WARNING);
			return false;
		}
		$id = escape_sqlData_without_quotes($post["id"]);
		//Infos zu den Artikeln
		$artikel = MysqlReadRow("SELECT a.*, k.`aktiv` AS `kataktiv`, k.`sichtbar` AS `katsichtbar`, k.`name`, k.`warteliste` FROM `" . TblPrefix() . "flip_catering_artikel` a, `" . TblPrefix() . "flip_catering_kategorien` k WHERE (a.`id`='$id') AND k.id=a.kategorien_id", "id");

		//pr&uuml;fen ob Kategorie und Artikel verf&uuml;gbar/aktiv
		if ($artikel["aktiv"] == "1" && $artikel["sichtbar"] == "1" && $artikel["kataktiv"] == "1" && $artikel["katsichtbar"] == "1") {
			//Wurde der Artikel bereits bestellt?
			if ($oldid = MysqlReadField("SELECT id FROM " . TblPrefix() . "flip_catering_bestellungen WHERE subject_id='" . $User->id . "' AND artikel_id='" . $artikel["id"] . "' AND status='warenkorb'", "id", true)) {
				if (MysqlWrite("UPDATE `" . TblPrefix() . "flip_catering_bestellungen` SET anzahl=anzahl+" . escape_sqlData_without_quotes($post["anzahl"]) . " WHERE id='$oldid'"))
					$return = "Es wurden zus&auml;tzlich " . $post["anzahl"] . "x " . $artikel["titel"] . " zum Warenkorb hinzugef&uuml;gt.";
			} else {
				//bestellen
				if (MysqlWriteByID(TblPrefix() . "flip_catering_bestellungen", array (
						"subject_id" => $User->id,
						"artikel_id" => $id,
						"status" => "warenkorb",
						"anzahl" => $post["anzahl"]
					)))
					//        if(MysqlWrite("INSERT INTO `".TblPrefix()."flip_catering_bestellungen` VALUES ('', '".$User->id."', '$id', 'warenkorb', '".escape_sqlData_without_quotes($post["anzahl"])."')"))
					$return = "Es wurden " . $post["anzahl"] . "x " . $artikel["titel"] . " deinem Warenkorb hinzugef&uuml;gt.";
			}
			//Log
			LogAction($User->name . " hat " . $post["anzahl"] . " \"" . $artikel["titel"] . "\" bestellt.");
			trigger_error_text($return);
			#return true;
		}
		//Fehlermeldung
		elseif ($artikel["aktiv"] == "0" || $artikel["sichtbar"] == "0") {
			trigger_error_text("\"" . $artikel["titel"] . "\" ist z.Z. nicht verf&uuml;gbar.");
			return false;
		}
		elseif ($artikel["kataktiv"] == "0" || $artikel["katsichtbar"] == "0") {
			trigger_error_text("Die Kategorie \"" . $artikel["name"] . "\" ist z.Z. nicht verf&uuml;gbar.");
			return false;
		}
	}

	function framekorb() {
		global $User;
		$User->requireRight($this->userright);
		$this->Caption = "Warenkorb von " . htmlentities_single($User->name);
		return array (
		"bestellungen" => MysqlReadArea("SELECT b.`id`, a.`titel`, b.`anzahl`, ROUND(a.`preis`*b.`anzahl`, 2) AS `preis`
		                                                FROM `" . TblPrefix() . "flip_catering_bestellungen` b, `" . TblPrefix() . "flip_catering_artikel` a
		                                                WHERE b.`status`='warenkorb' AND b.`subject_id`='{$User->id}' AND b.`artikel_id`=a.`id`
		                                                "), "sum" => MysqlReadField("SELECT ROUND(SUM(a.`preis`*b.`anzahl`), 2) AS sum FROM `" .
		TblPrefix() . "flip_catering_artikel` a, `" . TblPrefix() . "flip_catering_bestellungen` b WHERE b.`status`='warenkorb' AND b.`subject_id`='{$User->id}' AND b.`artikel_id`=a.`id`", "sum"));
	}

	function actionRemFromWarenkorb($get) {
		global $User;
		$User->requireRight($this->userright);
		$userbestellungen = MysqlReadCol("SELECT id FROM " . TblPrefix() . "flip_catering_bestellungen WHERE subject_id='" . $User->id . "'", "id", "id");
		foreach ($get["ids"] AS $id)
			if (isset ($userbestellungen[$id]))
				MysqlDeleteByID(TblPrefix() .
				"flip_catering_bestellungen", $id);
	}

	function framemybill($get) {
		global $User;
		ArrayWithKeys($get, array("id","status"));
		$r["id"] = $userid = escape_sqlData_without_quotes($get["id"]);
		$r["thisframe"] = "mybill";
		if (empty ($userid))
			$userid = $User->id;
		if (!$User->hasRight($this->adminright))
			$User->requireRightOver($this->userright, $userid);

		if ($userid != $User->id)
			$username = MysqlReadField("SELECT `name`FROM `" . TblPrefix() . "flip_user_subject` WHERE `id`='$userid'", "name");
		else
			$username = $User->name;
		$this->Caption = "Bestell&uuml;bersicht f&uuml;r " . htmlentities_single($username);
		$r["statuses"] = MysqlReadCol("SELECT `status` FROM `" . TblPrefix() . "flip_catering_bestellungen` WHERE `subject_id`='$userid' AND status!='warenkorb' GROUP BY `status`", "status");
		#$r["statuses"] = array("bestellt","bezahlt","geliefert","weitergeleitet");
		
		if (empty ($get["status"]) && is_array($r["statuses"]))
			$status = $r["statuses"][0];
		elseif(!empty($get["status"]))
			$status = $get["status"];
		else
			$status = "warenkorb";
		switch ($status) {
			case "warenkorb" :
				$newstatus = "bestellen";
				$nextright = $this->userright;
				$delright = $this->userright;
				break;
			case "warteliste" :
				$newstatus = "bestellt";
				$nextright = $this->adminright;
				$delright = $this->userright;
				break;
			case "bestellt" :
				$newstatus = "bezahlt";
				$nextright = $this->adminright;
				$delright = $this->userright;
				break;
			case "bezahlt" :
				$newstatus = "geliefert";
				$nextright = $this->adminright;
				$delright = $this->disabledright;
				break;
			default :
				$newstatus = "";
				$nextright = $this->disabledright;
				$delright = $this->disabledright;
		}
		if ($User->hasRight($this->adminright))
			$delright = $this->adminright;
		
		$status = escape_sqlData_without_quotes($status);
			
		$r["status"][] = array (
			"status" => $status,
			"newstatus" => $newstatus,
			"nextright" => $nextright,
			"delright" => $delright,
		"bestellungen" => MysqlReadArea("SELECT b.`id`, a.`titel`, b.`anzahl`, ROUND(a.`preis`*b.`anzahl`,2) AS `preis`, k.`name`
		                                                          FROM `" . TblPrefix() . "flip_catering_bestellungen` b, `" . TblPrefix() . "flip_catering_artikel` a, `" . TblPrefix() . "flip_catering_kategorien` k
		                                                          WHERE b.`status`='$status' AND b.`subject_id`='$userid' AND b.`artikel_id`=a.`id` AND a.`kategorien_id`=k.`id`
		                                                          "), "sum" => MysqlReadField("SELECT ROUND(SUM(a.`preis`*b.`anzahl`),2) AS sum FROM `" .
		TblPrefix() . "flip_catering_artikel` a, `" . TblPrefix() . "flip_catering_bestellungen` b WHERE b.`status`='$status' AND a.`id`=b.`artikel_id` AND b.`subject_id`='$userid'", "sum"));
		$r["sum"] = MysqlReadField("SELECT ROUND(SUM(a.`preis`*b.`anzahl`),2) AS sum FROM `" . TblPrefix() . "flip_catering_artikel` a, `" . TblPrefix() . "flip_catering_bestellungen` b WHERE (b.`status`='warteliste' OR b.`status`='bestellt') AND a.`id`=b.`artikel_id` AND b.`subject_id`='$userid'", "sum");
		(empty ($r["sum"])) ? $r["sum"] = 0 : "";
		if ($User->hasRight($this->adminright))
			$r["admin"] = "true";
		include_once ("mod/mod.seats.php");
		$r["seatname"] = SeatGetSeatByUser($userid);
		return $r;
	}
	
	// enum('warenkorb','warteliste','bestellt','weitergeleitet','bezahlt','geliefert')
	function _setStatus($bestellids = array (), $status) {
		if (!$this->isValidStatus($status)) {
			return false;
		}
		
		$statustext = ($status == 'warteliste') ? 'in die Warteschlange eingereiht' : $status;
		$ids = implode_sqlIn($bestellids);
		
		$names = MysqlReadCol(
			"SELECT a.`titel` " . 
				"FROM `" . TblPrefix() . "flip_catering_artikel` a, " . 
					"`" . TblPrefix() . "flip_catering_bestellungen` b " . 
				"WHERE b.`id` IN ($ids) " . 
				"AND a.`id`=b.`artikel_id` " . 
				"GROUP BY a.`id`", 
			"titel"
		);
		
		$name = implode("\", \"", $names);
		$names_SQL = implode_sqlIn($names);
		
		$nextstatusbestellungen = MysqlReadArea(
			"SELECT c.*, b.id AS oldid " . 
				"FROM `" . TblPrefix() . "flip_catering_bestellungen` b, " . 
					"`" . TblPrefix() . "flip_catering_bestellungen` c " . 
				"WHERE b.`id` IN ($ids) " . 
				"AND c.status='$status' " . 
				"AND b.artikel_id=c.artikel_id " . 
				"AND b.subject_id=c.subject_id", 
			"oldid"
		);
		
		if (!empty ($nextstatusbestellungen)) {
			//Daten der zu &auml;ndernden Bestellungen auslesen
			$bestellungen = MysqlReadArea(
				"SELECT * FROM " . TblPrefix() . "flip_catering_bestellungen " . 
					"WHERE `id` IN ($ids)"
				, "id"
			);
			
			foreach ($bestellids AS $id) {
				if (empty ($nextstatusbestellungen[$id])) {
					//einfach Status &auml;ndern
					$writeOk = MysqlWriteByID(
						TblPrefix() . "flip_catering_bestellungen", 
						array ("status" => $status), 
						$id
					);
					
					if($writeOk) {
						trigger_error_text("\"$name\" wurde(n) $statustext");
					} else {
						trigger_error_text("Fehler beim setzen des Statuses.|($status) f&uuml;r $name", E_USER_WARNING);
					}
				} else {
					// Wenn Artikel bereits mit neuem Status vorhanden sind...
					// ...aktuelle Bestellungen zu vorhandenen addieren
					$updateOK = MysqlWrite(
						"UPDATE " . TblPrefix() . "flip_catering_bestellungen " . 
						"SET anzahl=anzahl+" . $bestellungen[$id]["anzahl"] . 
						" WHERE id='" . $nextstatusbestellungen[$id]["id"] . "'"
					);
					
					if($updateOK) {
						trigger_error_text(
							$bestellungen[$id]["anzahl"] . " \"$name\" wurde(n) zus&auml;tzlich $statustext."
						);
					}
					
					// und alten Status l&ouml;schen
					MysqlDeleteByID(TblPrefix() . "flip_catering_bestellungen", $id);
				}
			}
		} else {
			//Status von allen &auml;ndern
			$updateAllOk = MysqlWrite(
				"UPDATE `" . TblPrefix() . "flip_catering_bestellungen` " . 
				"SET `status`='$status' " . 
				"WHERE `id` IN ($ids)"
			);
			
			if($updateAllOk) {
				trigger_error_text("\"$name\" wurde(n) $statustext.");
			} else {
				return false;
			}
		}
		
		return true;
	}

	function actionstatus_bestellen($post) {
		global $User;
		
		$User->requireRight($this->userright);
		$waitingItemsQueued = $directOrderItemsOrdered = true;

		$warteliste = MysqlReadCol(
			"SELECT b.id FROM `" . TblPrefix() . "flip_catering_bestellungen` b " . 
			"LEFT JOIN `" . TblPrefix() . "flip_catering_artikel` a ON b.artikel_id = a.id " .
			"LEFT JOIN `" . TblPrefix() . "flip_catering_kategorien` k ON a.kategorien_id = k.id " .
			"WHERE b.subject_id='" . $User->id . "' " . 
			"AND b.status='warenkorb' " .
			"AND k.warteliste='1' " .
			"AND a.aktiv='1' " .
			"AND a.sichtbar='1' " .
			"AND k.aktiv='1' " .  
			"AND k.sichtbar='1' "
		);
		
		$nowarteliste = MysqlReadCol(
			"SELECT b.id FROM `" . TblPrefix() . "flip_catering_bestellungen` b " .
			"LEFT JOIN `" . TblPrefix() . "flip_catering_artikel` a ON b.artikel_id = a.id " .
			"LEFT JOIN `" . TblPrefix() . "flip_catering_kategorien` k ON a.kategorien_id = k.id " .
			"WHERE b.subject_id='" . $User->id . "' " .
			"AND b.status='warenkorb' " .
			"AND k.warteliste='0' " .
			"AND a.aktiv='1' " .
			"AND a.sichtbar='1' " .
			"AND k.aktiv='1' " .
			"AND k.sichtbar='1'"
		);
		
		// Set all items that have a production queue to the status 'warteliste'
		if (!empty ($warteliste)) {
			$waitingItemsQueued = $this->_setStatus($warteliste, 'warteliste');
		}
		
		// ... and everything else directly to ordered (-> 'bestellt') 
		if (!empty ($nowarteliste)) {
			$directOrderItemsOrdered = $this->_setStatus($nowarteliste, 'bestellt');
		}
		
		// Finally delete the old items from the shopping cart
		MysqlWrite(
			"DELETE FROM `" . TblPrefix() . "flip_catering_bestellungen` " . 
			"WHERE status='warenkorb' " . 
			"AND subject_id='{$User->id}'"
		);
		
		return $waitingItemsQueued && $directOrderItemsOrdered;
	}

	function actionstatus_bestellt($post) {
		global $User;
		$User->requireRight($this->adminright);
		if (isset ($post["id"]) && empty ($post["ids"]))
			$post["ids"] = array (
				$post["id"]
			);
		return $this->_setStatus($post["ids"], "bestellt");
	}

	function actionstatus_bezahlt($post) {
		global $User;
		$User->requireRight($this->adminright);
		if (isset ($post["id"]) && empty ($post["ids"]))
			$post["ids"] = array (
				$post["id"]
			);
		return $this->_setStatus($post["ids"], "bezahlt");
	}

	function actionstatus_geliefert($post) {
		global $User;
		$User->requireRight($this->adminright);
		if (isset ($post["id"]) && empty ($post["ids"]))
			$post["ids"] = array (
				$post["id"]
			);
		return $this->_setStatus($post["ids"], "geliefert");
	}

	function actiondelbestellung($post) {
		global $User;
		$ids = implode_sqlIn($post["ids"]);
		$name = implode("\", \"", MysqlReadCol("SELECT a.`titel` FROM `" . TblPrefix() . "flip_catering_artikel` a, `" . TblPrefix() . "flip_catering_bestellungen` b WHERE b.`id` IN ($ids) AND a.`id`=b.`artikel_id` GROUP BY a.`id`", "titel"));
		if (MysqlWrite("DELETE FROM `" . TblPrefix() . "flip_catering_bestellungen` WHERE `id` IN ($ids)"))
			trigger_error_text("\"$name\" wurde(n) gel&ouml;scht.");
	}

	function actionartikelgeliefert($post) {
		global $User;
		$User->requireRight($this->adminright);
		$ids = MysqlReadCol("SELECT b.`id` FROM `" . TblPrefix() . "flip_catering_artikel` a, `" . TblPrefix() . "flip_catering_bestellungen` b WHERE a.`id` IN (" . implode_sqlIn($post["ids"]) . ") AND b.`artikel_id`=a.`id`", "id");
		return $this->_setStatus($ids, "geliefert");
	}

	function actionartikelweitergeleitet($post) {
		global $User;
		$User->requireRight($this->adminright);
		$ids = MysqlReadCol("SELECT b.`id` FROM `" . TblPrefix() . "flip_catering_artikel` a, `" . TblPrefix() . "flip_catering_bestellungen` b WHERE a.`id` IN (" . implode_sqlIn($post["ids"]) . ") AND b.`artikel_id`=a.`id`", "id");
		return $this->_setStatus($ids, "weitergeleitet");
	}

	function frameShowGroups($get) {
		global $User;
		$User->requireRight($this->adminright);
		$r["text"] = LoadText($this->edittext, $this->Caption);
		$r["kategorien"] = MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_catering_kategorien`");
		$count = MysqlReadArea("SELECT COUNT(*), kategorien_id FROM `" . TblPrefix() . "flip_catering_artikel` GROUP BY kategorien_id", "kategorien_id");
		foreach ($r["kategorien"] AS $key => $kategorie) {
			$r["kategorien"][$key]["count"] = isset($count[$kategorie["id"]]) ? $count[$kategorie["id"]]["COUNT(*)"] : 0;
		}
		if (empty ($get["groupid"])) {
			$r["action"] = "hinzuf&uuml;gen";
		} else {
			$r["action"] = "bearbeiten";
			$r = array_merge($r, MysqlReadRowByID(TblPrefix() . "flip_catering_kategorien", escape_sqlData_without_quotes($get["groupid"])));
		}
		return $r;
	}

	function submitaddgroup($post) {
		global $User;
		$User->requireRight($this->adminright);
		if ($writeid = MysqlWriteByID(TblPrefix() . "flip_catering_kategorien", $post, $post["id"])) {
			if ($writeid == $post["id"])
				LogChange("Die <b>Catering-Kategorie</b> \"" .
				htmlentities_single($post["name"]) . "\" wurde bearbeitet.");
			else
				LogChange("Die <b>Catering-Kategorie</b> \"" .
				htmlentities_single($post["name"]) . "\" wurde erstellt.");
			return true;
		} else {
			trigger_error_text("Fehler beim Schreiben in die Datenbank", E_USER_WARNING);
			return false;
		}
	}

	function actionkataktiv($post) {
		$this->_changestatus($post["ids"], "kategorien", "aktiv");
		return true;
	}

	function actionkatsichtbar($post) {
		$this->_changestatus($post["ids"], "kategorien", "sichtbar");
		return true;
	}

	function actiondelgroup($post) {
		global $User;
		$User->requireRight($this->adminright);
		$ids = implode_sqlIn($post["ids"]);
		if (count($post["ids"]) > 1)
			$n = "n";
		else
			$n = "";
		$names = MysqlReadCol("SELECT `name` FROM `" . TblPrefix() . "flip_catering_kategorien` WHERE `id` IN ($ids)", "name");
		if (MysqlWrite("DELETE FROM `" . TblPrefix() . "flip_catering_kategorien` WHERE `id` IN ($ids)"))
			LogChange("Die <b>Catering-Kategorie$n</b> \"" .
			implode("\", \"", $names) . "\" wurde$n gel&ouml;scht.");
	}

	function frameEditGroup($get, $post) {
		global $User;
		$User->requireRight($this->adminright);
		
		// Initialize necessary vars and assume the creation of 
		// a new item at first
		$r = array();
		$r['id'] = 0;
		$r['name'] = 'Neue Kategorie';
		$r['aktiv'] = 1;
		$r['sichtbar'] = 1;
		$r['warteliste'] = 0;
		$r['beschreibung'] = '';
		
		$this->Caption = 'Kategorie hinzuf&uuml;gen';
		
		// Load actual values if we are just editing an existing item
		if(isset($get['id']) && ($get['id'] > 0)) {
			$r['id'] = $get['id'];
			$this->Caption = 'Kategorie bearbeiten';
			
			$result = MysqlReadRow(
				'SELECT * FROM `' . TblPrefix() . 'flip_catering_kategorien` '
					. 'WHERE `id` = ' . escape_sqlData_without_quotes($r['id']),
				true					
			);
			
			if($result !== false) {
				$r = $result;
			} else {
				trigger_error('Die Kategorie mit der ID ' . $r['id'] . ' konnte nicht gefunden werden!', E_USER_ERROR);
			}
		}
		
		return $r;
	}
	
	function submitEditGroup($formData) {
		global $User;
		$User->requireRight($this->adminright);
		
		if(!isset($formData['id'])) {
			trigger_error_text('Es wurde keine ID f&uuml;r die neue Kategorie angegeben!');
			return false;
		}
		
		$sqlData = array();
		$sqlData['id'] = isset($formData['id']) ? ($formData['id']) : 0;
		$sqlData['aktiv'] = isset($formData['aktiv']) ? ($formData['aktiv']) : 1;
		$sqlData['sichtbar'] = isset($formData['sichtbar']) ? ($formData['sichtbar']) : 1;
		$sqlData['warteliste'] = isset($formData['warteliste']) ? ($formData['warteliste']) : 0;
		$sqlData['image_id'] = isset($formData['image_id']) ? ($formData['image_id']) : 0;
		$sqlData['bestellzeit'] = isset($formData['bestellzeit']) ? ($formData['bestellzeit']) : '';
		$sqlData['name'] = isset($formData['name']) ? ($formData['name']) : 'Neue Gruppe';
		$sqlData['beschreibung'] = isset($formData['beschreibung']) ? ($formData['beschreibung']) : '';
		
		$writeOK = MysqlWriteByID('flip_catering_kategorien', $sqlData, $sqlData['id']);
		
		if($writeOK == false) {
			trigger_error_text('Das Speichern der Kategorie ('.$name.') schlug fehl!', E_USER_ERROR);
		}
		
		return $writeOK;
	}
	
	function frameShowItems($frameVars) {
		global $User;
		$User->requireRight($this->adminright);
		
		$r['catid'] = isset($frameVars['catid']) ? $frameVars['catid'] : 0;
		$r['category'] = MysqlReadFieldByID(TblPrefix() . 'flip_catering_kategorien', 'name', $r['catid'], true);
		
		if(empty($r['category'])) {
			$r['category'] = 'Unbekannt';
			trigger_error_text(
				'Die Kategorie eines Artikels muss bekannt und g&uuml;ltig sein, um ihn zu bearbeiten!', 
				E_USER_ERROR
			);
		}
		
		$r['text'] = LoadText($this->edit_items_text, $this->Caption);
		$r['artikel'] = MysqlReadArea(
			'SELECT *, ROUND(`preis`,2) AS `preis` ' 
				. 'FROM `' . TblPrefix() . 'flip_catering_artikel` ' 
				. 'WHERE `kategorien_id`=\'' . $r['catid'] . '\' ' 
				. 'ORDER BY `titel`'
		);

		return $r;
	}

	function frameEditItem($frameVars) {
		global $User;
		$User->requireRight($this->adminright);
		
		if(!isset($frameVars['catid']) || empty($frameVars['catid'])) {
			trigger_error_text('Ein Artikel muss einer Kategorie zugeh&ouml;ren!', E_USER_ERROR);
		}		
		
		$r = array();
		
		$this->Caption = 'Artikel hinzuf&uuml;gen';
		
		if(isset($frameVars['id']) && ($frameVars['id'] > 0)) {
			$r['id'] = $frameVars['id'];
			
			$result = MysqlReadRowByID('flip_catering_artikel', $r['id'], true);
			
			if($result !== false) {
				$r = $result;
			} else {
				trigger_error_text(
					'Der Artikel mit der ID ' 
						. $r['id'] 
						. ' konnte nicht gefunden werden!', 
					E_USER_ERROR
				);
			}
		} else {
			$r['id'] = 0;
			$r['titel'] = 'Neuer Artikel';
			$r['beschreibung'] = '';
			$r['preis'] = '0.00';
			
			$r['sichtbar'] = 1;
			$r['aktiv'] = 1;
		}
		
		$r['catid'] = $frameVars['catid'];
		$r['preis'] = sprintf('%.2f', $r['preis']);
		
		return $r;
	}
	
	function submitEditItem($formVars) {
		global $User;
		$User->requireRight($this->adminright);
		
		if(!isset($formVars['id'])) {
			trigger_error_text('Der Artikel konnte nicht gespeichert werden!', E_USER_ERROR);
			return false;
		}
		
		if(!isset($formVars['catid']) || empty($formVars['catid'])) {
			trigger_error_text('Der Artikel muss einer Kategorie zugeh&ouml;ren!', E_USER_ERROR);
			return false;			
		}
		
		$sqldata = array();
		$sqldata['id'] = isset($formVars['id']) ? ($formVars['id']) : 0;
		$sqldata['titel'] = isset($formVars['titel']) ? ($formVars['titel']) : 'Neuer Artikel';
		$sqldata['beschreibung'] = isset($formVars['beschreibung']) ? ($formVars['beschreibung']) : '';
		$sqldata['aktiv'] = isset($formVars['aktiv']) ? ($formVars['aktiv']) : 1;
		$sqldata['sichtbar'] = isset($formVars['sichtbar']) ? ($formVars['sichtbar']) : 1;
		$sqldata['kategorien_id'] = $formVars['catid'];;
		$sqldata['preis'] = isset($formVars['preis']) ? sprintf('%.2f', $formVars['preis']) : '0.00';
		
		$writeOK = MysqlWriteByID(TblPrefix() . 'flip_catering_artikel', $sqldata, $sqldata['id']);
		
		if($writeOK == false) {
			trigger_error_text('Der Artikel konnte nicht gespeichert werden!', E_USER_ERROR);
		}
		
		return $writeOK;
	}

	function actionaktivartikel($post) {
		$this->_changestatus($post["ids"], "artikel", "aktiv");
		return true;
	}

	function actionsichtbarartikel($post) {
		$this->_changestatus($post["ids"], "artikel", "sichtbar");
		return true;
	}

	/**
	 * Setzt Spalten in Cateringtabellen von 0 auf 1 bzw. von 1 auf 0
	 * 
	 * @access private
	 **/
	function _changestatus($ids, $table, $col) {
		global $User;
		$User->requireRight($this->adminright);

		$aktive = MysqlReadArea("SELECT id FROM `" . TblPrefix() . "flip_catering_$table` WHERE `id` IN (" . implode_sqlIn($ids) . ") AND `$col`='1'");
		$inaktive = MysqlReadArea("SELECT id FROM `" . TblPrefix() . "flip_catering_$table` WHERE `id` IN (" . implode_sqlIn($ids) . ") AND `$col`='0'");
		//aktive deaktivieren
		foreach ($aktive AS $item) {
			MysqlWriteByID(TblPrefix() . "flip_catering_$table", array (
				"$col" => "0"
			), $item["id"]);
		}
		//inaktive aktivieren
		foreach ($inaktive AS $item) {
			MysqlWriteByID(TblPrefix() . "flip_catering_$table", array (
				"$col" => "1"
			), $item["id"]);
		}
	}

	function actiondelartikel($post) {
		global $User;
		$User->requireRight($this->adminright);
		$ids = "`id`=" . implode_sql(" OR `id`=", $post["ids"]);
		if (count($post["ids"]) > 1) {
			$d = "Die";
			$n = "n";
		} else {
			$d = "Der";
			$n = "";
		}
		$name = implode("\", \"", MysqlReadCol("SELECT `titel` FROM `" . TblPrefix() . "flip_catering_artikel` WHERE $ids", "titel"));
		if (MysqlWrite("DELETE FROM `" . TblPrefix() . "flip_catering_artikel` WHERE $ids")) {
			LogChange("$d <b>Catering-Artikel</b> \"$name\" wurde$n gel&ouml;scht.");
		}
	}

	function frameOrderManager($get, $post = array ()) {
		global $User;
		$User->requireRight($this->adminright);
		$r = $this->_verwaltung_kopfdaten($get, $post);
		$r['frame'] = 'ordermanager';
		$r["bestellungen"] = MysqlReadArea("SELECT b.`status`, b.`subject_id`, s.`name` AS `nickname`, SUM(b.anzahl) AS `count`, SUM(a.`preis`*b.`anzahl`) AS `preis`
		                                        FROM `" . TblPrefix() . "flip_catering_bestellungen` b, `" . TblPrefix() . "flip_catering_artikel` a, `" . TblPrefix() . "flip_catering_kategorien` g, `" . TblPrefix() . "flip_user_subject` s
		                                        WHERE a.`id`=b.`artikel_id`
		                                        AND a.`kategorien_id`=g.`id`
		                                        AND s.id=b.`subject_id`
		                                        " . $r["groups"] . "
		                                        " . $r["status"] . "
		                                        GROUP BY b.`subject_id`, b.`status`
		                                        ORDER BY s.`name`, b.`status`
		                                        ");
		$r['total'] = 0;

		// Calculate the sum of all orders
		foreach($r['bestellungen'] as $order) {
			$r['total'] += $order['preis'];
		}

		$r['total'] = sprintf('%.2f', $r['total']);
		$r["print"] = Condense($post);
		
		return $r;
	}

	function frameItemManager($get, $post = array ()) {
		ArrayWithKeys($get, array("id"));
		$r = $this->_verwaltung_kopfdaten($get, $post);
		$r["id"] = $get["id"];
		$r['frame'] = 'itemmanager';
		$r["bestellungen"] = MysqlReadArea("SELECT a.`id`, g.`name`, a.`titel`, SUM(b.`anzahl`) AS `anzahl`, ROUND(SUM(a.`preis`*b.`anzahl`),2) AS `preis`
		                                        FROM `" . TblPrefix() . "flip_catering_bestellungen` b, `" . TblPrefix() . "flip_catering_artikel` a, `" . TblPrefix() . "flip_catering_kategorien` g
		                                        WHERE a.`id`=b.`artikel_id`
		                                        AND a.`kategorien_id`=g.`id`
		                                        " . $r["groups"] . "
		                                        " . $r["status"] . "
		                                        GROUP BY a.`id`
		                                        ORDER BY g.`name`, a.`titel`
		                                        ");
		$r["sum"] = MysqlReadField("SELECT ROUND(SUM(a.`preis`*b.`anzahl`),2) AS sum
		                                FROM `" . TblPrefix() . "flip_catering_bestellungen` b, `" . TblPrefix() . "flip_catering_artikel` a, `" . TblPrefix() . "flip_catering_kategorien` g
		                                WHERE a.`id`=b.`artikel_id`
		                                AND a.`kategorien_id`=g.`id`
		                                " . $r["groups"] . "
		                                " . $r["status"] . "
		                                ", "sum");
		//POST => GET
		$r["print"] = Condense($post);
		
		return $r;
	}

	function frameprintlist($get, $post) {
		//GET => POST
		$post = Uncondense($get["status"]);
		$this->ShowMenu = false;
		$this->TplSub = "Artikelliste";
		return array_merge($this->frameartikelverwaltung($get, $post), array (
			"printing" => true
		));
	}

	function frameviewlist($get, $post) {
		$this->ShowMenu = false;
		$post = Uncondense($get["status"]);
		$r = $this->_getStatusArr($post);
		$r["title"] = $this->Caption;
		$r["caption"] = "Cateringliste";
		$groups = "";
		if (!empty ($get["id"]))
			$groups = "AND (g.`id`='" . $get["id"] . "')";
		$r["bestellungen"] = MysqlReadArea("SELECT b.*, g.`name`, a.`titel`, s.`name`, ROUND(a.`preis`*b.`anzahl`, 2) AS `preis`
		                                        FROM (`" . TblPrefix() . "flip_catering_bestellungen` b) 
							INNER JOIN `" . TblPrefix() . "flip_catering_artikel` a ON (b.`artikel_id`=a.`id`) 
							LEFT JOIN `" . TblPrefix() . "flip_catering_kategorien` g ON a.`kategorien_id`=g.`id` 
							INNER JOIN `" . TblPrefix() . "flip_user_subject` s ON b.`subject_id`=s.`id`
		                                        WHERE 1 $groups
		                                        " . $r["status"] . "
		                                        ORDER BY g.`name`, a.`titel`
		                                        ");
		foreach ($r["bestellungen"] AS $b)
			$uids[] = $b["subject_id"];
		array_unique($uids);
		$seats = GetSubjects("user", array (
			"id",
			"seat"
		), "s.id IN(" . implode_sqlIn($uids) . ")");
		foreach ($r["bestellungen"] AS $key => $b)
			$r["bestellungen"][$key]["seat"] = $seats[$b["subject_id"]]["seat"];
		return $r;
	}

	function _verwaltung_kopfdaten($get, $post = array ()) {
		$r["text"] = LoadText($this->verwaltungstext, $this->Caption);
		
		switch (strtolower($get["frame"])) {
			case "itemmanger" :
				$r["this"] = "Artikel";
				$r["thisframe"] = "Artikelverwaltung";
				$r["other"] = "User";
				break;
			case "usermanager" :
			default :
				$r["this"] = "User";
				$r["thisframe"] = "Userverwaltung";
				$r["other"] = "Artikel";
		}
		
		// Kategorien
		$r['kategorien'] = MysqlReadCol('SELECT * FROM `' . TblPrefix() . 'flip_catering_kategorien`', 'name', 'id');
		$r['kategorien'][0] = 'Alle anzeigen';
		
		if (!empty ($get["id"])) {
			$r["groups"] = "AND (g.`id`='" . $get["id"] . "')";
			$r["kategorie"] = MysqlReadFieldByID(TblPrefix() . "flip_catering_kategorien", "name", $get["id"]);
		} else {
			$r["groups"] = "";
			$r["kategorie"] = "Alle";
		}
		
		//Status
		$r = array_merge($this->_getStatusArr($post), $r);
		return $r;
	}

	function _getStatusArr($post) {
		$r = array(
			"status" => "", 
			"statustext" => ""
		);
		
		if (!empty ($post)) {
			$statuslist = array_keys($post);
			$r["statustext"] = implode(", ", $statuslist);
			$r["status"] = "AND b.`status` IN (" . implode_sqlIn($statuslist) . ")";
		} else {
			$r["statustext"] = "shoppingcart, cashdesk, ordered, paid, delivered, completed";
		}
		
		return $r;
	}

	function framedoku() {
		$this->Caption = "Cateringbeschreibung";
		return array ();
	}

	/**
	 * Displays the items left in the stock for each food item. 
	 */
	function frameShowStock() {
		global $User;
		$User->requireRight($this->adminright);
		
		$r = array();
		
		// Init page header etc.
		$this->Caption = 'Stock amounts';
		
		// Get all orders that have been paid for or delivered
		$orderedAmount = MysqlReadCol(
			'SELECT artikel_id, SUM(anzahl) AS total_amount ' . 
				'FROM `' . TblPrefix() . 'flip_catering_bestellungen` ' . 
				'WHERE `status` IN (\'bezahlt\', \'geliefert\') ' .
				'GROUP BY `artikel_id`',
			'total_amount',
			'artikel_id'
		);
		
		// Get the stock data from each article
		$stockAmountsMax = MysqlReadArea(
			'SELECT * FROM `' . TblPrefix() . 'flip_catering_artikel`'
		);
		
		foreach($stockAmountsMax as $currentItem) {
			$item = array();
			$item['name'] = $currentItem['titel'];

			if(isset($orderedAmount[$currentItem['id']])) {
				$item['remaining_amount'] = $currentItem['amount'] - $orderedAmount[$currentItem['id']];
			} else {
				$item['remaining_amount'] = $currentItem['amount'];
			}

			$r['stockamount'][] = $item;
		}
		
		return $r;
	}
	
	function frameCashDesk($get) {
		global $User;
		$User->requireRight($this->adminright);
		$r = array();
		
		$this->Caption = 'Kasse';
		
		// Get item categories
		$categories = MysqlReadCol(
			'SELECT * FROM `' . TblPrefix() . 'flip_catering_kategorien` WHERE `aktiv` = 1 AND `sichtbar` = 1', 
			'name', 
			'id'
		);
		
		// Get the items themselves
		$items = MysqlReadArea(
			'SELECT * FROM `' . TblPrefix() . 'flip_catering_artikel` WHERE `aktiv` = 1 AND `sichtbar` = 1'
		);
		
		$catList = array();
		
		// Build the cashdesk gui
		foreach($items as $item) {
			$item['label'] = $item['titel'] . "\n\n" . sprintf('%.2f', $item['preis']);
			$catList[$item['kategorien_id']]['name'] = $categories[$item['kategorien_id']];
			$catList[$item['kategorien_id']]['items'][] = $item;
		}
		
		if(empty($get['current_user'])) {
			$r['current_user'] = 0;
			$r['current_user_name'] = '';
		} else {
			if(strpos($get['current_user'], '000011') !== false) {
				$get['current_user'] = str_replace('000011', '000013', $get['current_user']);
			}
			
			$current_user_id = escape_sqlData_without_quotes($get['current_user']);
			$u = CreateSubjectInstance($get['current_user']);
			$r['current_user'] = $get['current_user'];
			$r['current_user_name'] = $u->getProperty('name');
			$r['current_user_items'] = MysqlReadArea(
				'SELECT * FROM `' . TblPrefix() . 'flip_catering_bestellungen` b ' . 
				'LEFT JOIN `flip_catering_artikel` a ON b.`artikel_id` = a.`id`' . 
				'WHERE `subject_id` = ' . $current_user_id . ' ' . 
				'AND `status` = \'cashdesk\''
			);
			
			$r['summe'] = 0.0;
			
			foreach($r['current_user_items'] as $k => $item) {
				$r['current_user_items'][$k]['sub_summe'] = $item['anzahl'] * $item['preis'];
				$r['summe'] += $item['anzahl'] * $item['preis'];
			}
			
			$r['summe'] = sprintf('%.2f', $r['summe']);
		} 
		
		$r['category_list'] = $catList;
		
		return $r;
	}
	
	function actionCashdesk_AddItem($g) {
		if(empty($g['user'])) {
			trigger_error('Bitte zuerst einen Benutzer ausw&auml;hlen!', E_USER_WARNING);
			return false;
		}
		
		$userID = escape_sqlData_without_quotes($g['user']);
		$artikelID = escape_sqlData_without_quotes($g['id']);

		// Check if there is already an item in the basket, if so update the record, else add it
		$existingRecord = MysqlReadField(
			'SELECT COUNT(id) AS c FROM `' . TblPrefix() . 'flip_catering_bestellungen` ' . 
				'WHERE subject_id = ' . $userID . ' ' . 
				'AND artikel_id = ' . $artikelID . ' ' .
				'AND `status` = \'cashdesk\'',
			'c'
		);
		
		$query = '';
		
		if($existingRecord == 0) {
			// Add the items to the basket of the user
			$query = 'INSERT INTO `' . TblPrefix() . 'flip_catering_bestellungen` ' . 
					 '(subject_id, artikel_id, status, anzahl) ' . 
					 'VALUES (' . $userID . ', ' . $artikelID . ', \'cashdesk\', 1 )';
		} else {
			$query = 'UPDATE `' . TblPrefix() . 'flip_catering_bestellungen` ' . 
					 'SET anzahl = anzahl + 1 ' . 
					 'WHERE subject_id = ' . $userID . ' ' . 
					 'AND artikel_id = ' . $artikelID;
		}
		
		$writeOK = MysqlWrite($query);
		
		if(!$writeOK) {
			trigger_error('Could not add another article!');
		}
		
		return $writeOK;
	}
	
	function actionCashDesk_Cancel($g) {
		if(empty($g['user'])) {
			trigger_error('Bitte zuerst einen Benutzer ausw&auml;hlen!', E_USER_WARNING);
			return false;
		}
		
		$userID = escape_sqlData_without_quotes($g['user']);
		
		// Delete the elements
		$delete = MysqlWrite(
			'DELETE FROM `' . TblPrefix() . 'flip_catering_bestellungen` ' . 
			'WHERE `status` = \'cashdesk\' ' . 
			'AND `subject_id` = ' . $userID
		);
		
		if($delete) {
			trigger_error_text('Vorgang abgebrochen!', E_USER_NOTICE);
		} else {
			trigger_error_text('Fehler beim L&ouml;schen der aktuellen Artikel!', E_USER_WARNING);
		}
		
		return $delete;
	}
	
	function actionCashDesk_Checkout($g) {
		if(empty($g['user'])) {
			trigger_error('Bitte zuerst einen Benutzer ausw&auml;hlen!', E_USER_WARNING);
			return false;
		}
		
		if(($g['state'] != 'bezahlt') && ($g['state'] != 'geliefert')) {
			trigger_error('Der Status (' . $g['state'] . ') der Kasse ist ung&uuml;ltig!', E_USER_WARNING);
			return false;			
		}
		
		$state = escape_sqlData_without_quotes($g['state']);
		$userID = escape_sqlData_without_quotes($g['user']);
		
		// Delete the elements
		$order = MysqlWrite(
			'UPDATE `' . TblPrefix() . 'flip_catering_bestellungen` ' . 
			'SET `status` = \'' . $state . '\' ' . 
			'WHERE `status` = \'cashdesk\' ' . 
			'AND `subject_id` = ' . $userID
		);
		
		if($order) {
			trigger_error_text('Vielen dank!', E_USER_NOTICE);
		} else {
			trigger_error_text('Fehler beim L&ouml;schen der aktuellen Artikel!', E_USER_WARNING);
		}
		
		return true;
	}
	
}

RunPage("CateringPage");
?>
