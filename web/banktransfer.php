<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: checkininfo.php 1702 2019-01-09 09:01:12Z docfx $ edit by naaux
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");

class BanktransferPage extends Page {
	var $BanktransferText = "banktransfer_main";
	var $BanktransferNotAllowed = "banktransfer_not_allowed";
	var $BanktransferPaid = "banktransfer_paid";

	var $BanktransferViewRight = "banktransfer_view";
	var $StatusConsole = "status_registered_console";

	var $BGImage = "images/banktransfer/bg.jpg";

	var $KontoNrConfig = "banktransfer_account_no";
	var $KontoOwnerName = "banktransfer_account_owner_name";
	var $AccountBankCode = "banktransfer_account_bank_code";
	var $AccountBicSwift = "banktransfer_account_bic_swift";
	var $AccountBankName = "banktransfer_account_bank_name";
	var $Currency = "banktransfer_currency";
	var $SubjectPrefix = "banktransfer_subject_prefix";
	var $MoneyAmount = "banktransfer_money_amount";
	var $MoneyAmount1 = "banktransfer_money_amount1";

	function frameDefault($get, $post) {
		global $User;
		include_once ("mod/mod.image.php");
		include_once ("inc/inc.text.php");
		include_once ("mod/mod.config.php");
		$r = array ();
		$img = new DataImage(getTplFileDir($this->BGImage) . $this->BGImage);
		$r["img"] = $this->getSmallSize($img);

		if ($User->hasRight($this->BanktransferViewRight)) {
			$r["showdata"] = true;
			$r["text"] = LoadText($this->BanktransferText, $this->Caption);
			$r["dat"] = $this->getData();
		} 
		else if ($User->hasRight($this->status_paid)||$User->hasRight($this->status_paid_clan)) {
			$r["showdata"] = false;
			$r["text"] = LoadText($this->BanktransferPaid, $this->Caption);
		}
		else {
			$r["showdata"] = false;
			$r["text"] = LoadText($this->BanktransferNotAllowed, $this->Caption);
		}
		if (ConfigCanEdit())
			$r["configurl"] = "config.php#banktransfer";
		return $r;
	}

	function getSmallSize($img) {
		$i = $img->getInfo();
		$factor = 0.5;
		$i["width"] = round($i["width"] * $factor);
		$i["height"] = round($i["height"] * $factor);
		return $i;
	}

	function getData() {
		global $User;
		$u = $User->getProperties(array (
			"familyname",
			"givenname",
			"name",
			"id",
			"bank_knr",
			"bank_owner"
		));
		;
		if (empty ($u["bank_owner"]))
			$u["bank_owner"] = "$u[givenname] $u[familyname]";

		if($User->hasRight($this->StatusConsole)) {
			return array (
			"account_no"		=> ConfigGet($this->KontoNrConfig),
			"account_name"		=> ConfigGet($this->KontoOwnerName),
			"account_bic_swift" => ConfigGet($this->AccountBicSwift),
			"account_bank_name"	=> ConfigGet($this->AccountBankName),
			"currency"			=> ConfigGet($this->Currency),
			"money"				=> ConfigGet($this->MoneyAmount1),
			"subject1"			=> ConfigGet($this->SubjectPrefix),
			"subject2"			=> "$u[givenname] $u[familyname],  ID$u[id]" ,
			"subject3"			=> "$u[familyname]",
			"subject4"			=> "$u[name]",
			"subject5"			=> "ID$u[id]",
			"subject6"			=> "LAN",
			"subject7"			=> "Ortschaft",
			"subject8"			=> "-",
			"subject9"			=> "$u[givenname]",
			"subject10"			=> "$u[givenname] $u[familyname]" ,
			"sender_name"		=> $u["bank_owner"],
			"sender_acc_no" => $u["bank_knr"],
		);
		} else {
		return array (
			"account_no"		=> ConfigGet($this->KontoNrConfig),
			"account_name"		=> ConfigGet($this->KontoOwnerName),

			"account_bic_swift" => ConfigGet($this->AccountBicSwift),
			"account_bank_name"	=> ConfigGet($this->AccountBankName),
			"currency"			=> ConfigGet($this->Currency),
			"money"				=> ConfigGet($this->MoneyAmount),
			"subject1"			=> ConfigGet($this->SubjectPrefix),
			"subject2"			=> "$u[givenname] $u[familyname],  ID$u[id]" ,
			"subject3"			=> "$u[familyname]",
			"subject4"			=> "$u[name]",
			"subject5"			=> "ID$u[id]",
			"subject6"			=> "LAN",
			"subject7"			=> "Ortschaft",
			"subject8"			=> "-",
			"subject9"			=> "$u[givenname]",
			"subject10"			=> "$u[givenname] $u[familyname]" ,
			"sender_name"		=> $u["bank_owner"],
			"sender_acc_no" => $u["bank_knr"],
			);
		}
		
	}

	function frameImage() {
		include_once ('mod/mod.imageedit.php');
		include_once ('mod/mod.imagefont.php');
		global $User;
		$User->requireRight($this->BanktransferViewRight);

		// bilder laden
		$bg = getTplFileDir($this->BGImage) . $this->BGImage;
		$chars = 'images/banktransfer/handwritten.png';
		$chars = getTplFileDir($chars) . $chars;
		$courier = 'images/banktransfer/courier_new.png';
		$courier = getTplFileDir($courier) . $courier;

		$image = new ResImage($bg);
		$font = new ImageFont($chars, '-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ.,');
		$courier = new ImageFont($courier, '-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');

		// daten laden
		$dat = $this->getData();
		$date = date("j.n.Y");

		// daten schreiben
		$font->writeText($image->Res, 15, 85, $dat["account_name"]);
		$font->writeText($image->Res, 15,130, $dat["account_no"]);
		$font->writeText($image->Res, 15, 175, $dat["account_bic_swift"]);
		
		//$font->writeText($image->Res, 400, 113, $dat["account_bank_name"]);
		//$font->writeText($image->Res, 400, 75, $dat["subject7"]);
		
		$courier->setCenter(true, false);
		//$courier->writeText($image->Res, 306, 166, $dat["currency"]);
		
		$font->writeText($image->Res, 700, 175, $dat["money"],80);
	    $font->writeText($image->Res, 755, 175, $dat["subject8"]);
		//$font->writeText($image->Res, 15, 270, $dat["subject8"]);
	
		$font->writeText($image->Res, 15, 270, $dat["subject1"], 415);
		
		$font->writeText($image->Res, 415, 270, $dat["subject5"], 250);
		$font->writeText($image->Res, 15, 315, $dat["subject10"], 550);
		//$font->writeText($image->Res, 300, 315, $dat["subject6"], 250);
		//$font->writeText($image->Res, 400, 185, $dat["subject3"], 250);
		//$font->writeText($image->Res, 400, 220, $dat["subject9"], 250);
		//$font->writeText($image->Res, 400, 255, $dat["subject4"], 250);
	
		//$font->writeText($image->Res, 400, 150, $dat["subject1"], 250);
		//$font->writeText($image->Res, 400, 185, $dat["subject6"], 250);
		//$font->writeText($image->Res, 400, 260, $dat["subject3"], 250);
		//$font->writeText($image->Res, 400, 300, $dat["subject4"], 250);
		//$font->writeText($image->Res, 400, 335, $dat["subject5"], 250);
		//$font->writeText($image->Res, 400, 220, $dat["subject2"], 240);
	

		$font->writeText($image->Res, 15, 405, $dat["sender_name"], 594);
		//$font->writeText($image->Res, 30, 322, $dat["sender_acc_no"], 220);

		$font->setSizeFactor(0.7);
		$font->setSpacing(-6, 0);
		// $font->writeText($image->Res, 230, 392, $date);

		return $image;
	}

	function frameSmallImage() {
		$i = $this->frameImage();
		$d = $this->getSmallSize($i);
		return $i->createThumbnail($d["width"], $d["height"]);
	}

}

RunPage("BanktransferPage");
?>
