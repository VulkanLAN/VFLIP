<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: barcode.php 1455 2007-08-23 22:47:30Z scope $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ('core/core.php');
require_once ('inc/inc.page.php');

class BarcodePage extends Page {
	function frameDefault($get) {
		// Default: Interlaced 2 of 5 zwecks Abw&auml;rtskompatibilit&auml;t 
		return $this->frameCodeI2of5($get);
	}
	
	function frameCodeI2of5($get) {
		include_once ('ext/barcode.php');
		include_once ('ext/i25object.php');

		if (!isset ($get['style']))
			$get['style'] = BCD_DEFAULT_STYLE | BCS_DRAW_TEXT | BCS_STRETCH_TEXT;
		if (!isset ($get['width']))
			$get['width'] = 160;
		if (!isset ($get['height']))
			$get['height'] = BCD_DEFAULT_HEIGHT;
		if (!isset ($get['xres']))
			$get['xres'] = BCD_DEFAULT_XRES;
		if (!isset ($get['font']))
			$get['font'] = BCD_DEFAULT_FONT;

		$c = sprintf('%08d', intval($get['code']));
		$obj = new I25Object($get['width'], $get['height'], $get['style'], $c);
		$obj->SetFont($get['font']);
		$obj->DrawObject($get['xres']);
		$obj->FlushObject();
		$obj->DestroyObject();
		unset ($obj);
		return true;
	}
	
	function frameCode93($get) { 
		require_once('mod/mod.barcode.code93.php');
		$h =   isset($get['height']) ? $get['height'] : 100;
		$w =   isset($get['width']) ? $get['width'] : 300;
		$txt = isset($get['code']) ? $get['code'] : 'Code93 Barcode';
		$code = new Barcode93($w, $h);
		$code->_BarWidth = isset($get['xres']) ? $get['xres'] : 1;
		$code->drawBarcodeImage($txt);
		return true;
	}
}

RunPage('BarcodePage');
?>