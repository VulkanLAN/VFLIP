<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: seats.php 1054 2005-08-03 09:21:59Z loom $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");
require_once ("mod/mod.image.php");
require_once ("mod/mod.seats.php");
require_once ("mod/mod.seats.draw.php");
require_once ("mod/mod.xmlgen.php");

class SeatsadmPage extends Page {
    //var $ConfigBaseplan = "seats_baseplan";
    var $AdminSeatsRight = "seats_admin";

    function frameDefault($get, $post) {
        global $User, $Session;
        $this->Caption = "Sitzplan bearbeiten (TableDancer)";
        $User->requireRight($this->AdminSeatsRight);

        $max_width = 600;
        $max_height = 1000;
        require_once ("mod/mod.imageedit.php");
        foreach(SeatGetBlocks() AS $block) {
            $image = new ResImage(SeatsGetBackgroundLoader($block["id"]));
            if($image->getWidth() > $max_width)
                $max_width = $image->getWidth(); 
            if($image->getHeight() > $max_height)
                $max_height = $image->getHeight(); 
        }
        
        return array (
            "NAME"  => ConfigGet("session_cookiename"),
            "ID"    => $Session->id,
            "AGENT" => $Session->UserAgent,
            "width" => $max_width,
            "height"=> $max_height
            );
    }

    function _GetXmlIcons($icons) {
        $icons = $icons["small"];
        $types = array
        (
            "tablegreen",
            "tablegrey",
            "chair",
        );

        $r = new XMLDocument("seaticons");
        foreach ($types as $type)
        {
            $tag = new XMLTag($type);
            foreach ($icons[$type] as $angle => $file)
            {
                $tag->addTag("icon", array ("angle" => $angle), $file);
            }
            $r->addTag($tag);
        }
        return $r;
    }
    
    function frameGetSeats() {
        //XML-Elements
        $blockcols = array (
            "id",
            "view_right",
            "caption",
            "description",
            "imagedir_block",
            "link",
            "href",
            "seatstag"
        );
        $seatcols = array (
            "id",
            "enabled",
            "center_x",
            "center_y",
            "angle",
            "user_id",
            "user_status",
            "reserved",
            "name",
            "ip"
        );

        $r = new XMLDocument("blocks");
        $blocks = array ();
        foreach (SeatGetBlocks() AS $blockdata) {
            $seats = GetSeats($blockdata["id"]);
            $seatstag = new XMLTag("seats");
            $seatstag->addTwoDimArrayContent("seat", $seats, $seatcols);
            $blockdata["seatstag"] = $seatstag;
            $blocks[] = $blockdata;
        }
        
        $r->addTwoDimArrayContent("block", $blocks, $blockcols);
        
        return $r;
    }

    function frameGetBGImage($get) {
        include_once ("mod/mod.imageedit.php");
        if (empty ($get["blockid"])) {
            //return new DataImage(ConfigGet($this->ConfigBaseplan));
            return new ResImage(SeatsGetOverviewImageLoader());
        } else {
            return new ResImage(SeatsGetBackgroundLoader($get["blockid"]));
        }
    }

    function frameGetIconsDefault($get) {
        $icons = _GetSeatImages(ConfigGet("seats_images_large"));
        
        return $this->_GetXmlIcons($icons);
    }
    
    function frameGetIcons($get) {
        $block = SeatGetBlock($get["blockid"]);
        $icons = _GetSeatImages($block["imagedir_block"]);
        
        return $this->_GetXmlIcons($icons);
    }

    function parseSeats($SeatsXMLString) {
        $xmldata = simplexml_load_string($SeatsXMLString);
        $result = array ();
        
        foreach ($xmldata->block AS $block) {
            foreach ($block->seats->seat AS $seat) {
                $seat = (array) $seat;
                $seat["block_id"] = (int) $block->id;
                $result[] = $seat;
            }
        }
        
        return $result;
    }

	function frameSetSeats() {
		global $User;
		$User->requireRight($this->AdminSeatsRight);
		if (!empty ($_POST['data']))
			$dat = $_POST['data'];
		else
			$dat = GetRawPostData();
		if (empty ($dat))
			trigger_error_text("Es wurden keine Post-Daten mit &uuml;bergeben.", E_USER_WARNING);

		$dat = $this->parseSeats($dat);
		SeatsUpdateByArray($dat);

		return $this->frameGetSeats();
	}

}

RunPage("SeatsadmPage");
?>
