<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: log.php 642 2004-12-12 12:17:47Z docfx $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");
require_once ("mod/mod.parse.php");

class ExternalPage extends Page {
	var $ModConf = "external_accessabledirs";

	function frameDefault($get, $post) {
		// config laden
		$modules = ConfigGet($this->ModConf);
		if (empty ($modules)) {
			include_once ("mod/mod.config.php");
			$configlink = (ConfigCanEdit()) ? "<a href=\"config.php#external_accessabledirs\">external_accessabledirs</a>" : "external_accessabledirs";
			trigger_error("external.php kann erst verwendet werden, wenn es konfiguriert wurde. Siehe dazu den Konfigurationseintrag: $configlink", E_USER_ERROR);
		}
		$modules = ParseParam($modules, array (), false);

		// parameter auf g&uuml;ltigkeit &uuml;berpr&uuml;fen
		$mod = $get['mod'];
		$file = $get['file'];
		if (empty ($file) or empty ($mod))
			trigger_error_text("Dieser Script ben&ouml;tigt die Parameter 'mod' und 'file'.", E_USER_ERROR);
		if (!preg_match('/^[\d\w\.\-_]+$/', $file))
			trigger_error_text("Der Parameter file enth&auml;lt ung&uuml;ltige Zeichen (erlaubt sind Buchstaben, Ziffern, Punkt, Unterstrich und Bindestrich).", E_USER_ERROR);
		if (empty ($modules[$mod]))
			trigger_error_text("Der Parameter mod ist ung&uuml;ltig. Das Modul wurde nicht gefunden.|mod:$mod, modules:" . implode(",", array_keys($modules)), E_USER_ERROR);

		// &uuml;berpr&uuml;fen, ob die datei existiert.
		$inc = $modules[$mod] . $file;
		if (!is_file($inc))
			trigger_error_text("Eine Datei wurde nicht gefunden.|file:$inc", E_USER_ERROR);

		//Dateinamenteile, welche ohne Men&uuml; ausgegeben werden (v.a. Bilder)
		$directoutputs = array (
			".gif",
			".jpg",
			".bmp",
			".tiff",
			".tif",
			".jpeg",
			".png"
		);
		foreach ($directoutputs AS $partial_text) {
			if (strpos($inc, $partial_text) !== false) {
				$this->ShowMenu = false;
				//TODO: richtigen Content-Type mit header() senden
				include ($inc);
				return true;
			}
		}

		// die datei includen und ihre ausgabe entgegennehmen.
		$cwd = getcwd();
		chdir($modules[$mod]);
		ob_start();
		include ($inc);
		$r = ob_get_contents();
		ob_end_clean();
		chdir($cwd);

		// links in der ausgabe der datei ersetzen
		return array (
			'content' => $this->doReplace($r,
			$modules[$mod],
			$mod
		));
	}

	function doReplace($content, $dir, $mod) {
		$dir = opendir($dir);
		$rep = array ();
		while ($d = readdir($dir))
			if ($d != '.' and $d != '..') {
				$rep[$d] = "external.php?mod=$mod&amp;file=$d";
				$rep["$d?"] = "external.php?mod=$mod&amp;file=$d&amp;";
			}
		return strtr($content, $rep);
	}
}

RunPage("ExternalPage");
?>