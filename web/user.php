<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: user.php 1702 2019-01-09 09:01:12Z loom $ edit by naaux u. Tominator
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");
require_once ("inc/inc.text.php");

class UserPage extends Page {
	var $AdminSubjectsRight	= "user_admin_subjects";
	var $AdminRightsRight	= "user_admin_rights";
	var $AdminHeredity		= "user_admin_heredity";
	var $AdminColumns		= "user_admin_columns";
	var $UserCheckin		= "checkin";
	var $Adduser4TurnierRight = "user_adduser4turnier";
	var $ViewGroupMembers	= "user_view_members";
	var $rightCateringAdmin = 'shop_admin';

	var $SubjectTextPrefix	= "user_subject_";
	var $ColumnsText		= "user_columns";
	var $RightsText			= "user_rights";
	var $RightText			= "user_right";
	var $RegisterText		= "user_register";
	var $DSGVOText			= "dsgvo_datenverarbeitung";
	var $ActivatedText		= "user_activated";
	var $AccountResetPwdText = "user_account_resetpassword";
	var $AccountResendActivation = "user_account_resendactivation";
	var $AccountChangeEmail	= "user_account_changeemail";
	var $PageOrganisationName = "page_organisation_name";

	var $config_lansurferimport = "user_import_lansurfer";

	function frameDefault($get, $post) {
		global $User;
		$User->requireRight($this->AdminSubjectsRight);
		
		$types = array_keys(GetTypeInfo());
		sort($types);
		
		$type = (in_array($get["type"], $types)) ? addslashes($get["type"]) : "user";
		$where = "(`type` = '$type')";
		
		// Wie im Checkin suchen
		if(!empty($get['search'])) {
			$s = addslashes($get['search']);
			if($type == 'user') {
				$ids = MysqlReadCol("SELECT `subject_id` FROM `".TblPrefix()."flip_user_data` WHERE (`val` LIKE '%$s%');", "subject_id", "subject_id");
				$i = (count($ids) > 0) ? " OR (s.id IN (".implode_sqlIn(array_keys($ids))."))" : "";
			}
			$where .= " AND ((s.email LIKE '%$s%') OR (s.name LIKE '%$s%') $i )";
		}
		
		$items = MysqlReadArea(
			"SELECT * FROM `" . TblPrefix() . "flip_user_subject` s " . 
			"WHERE $where " . 
			"ORDER BY upper (`name`) ASC;"
		);
		
		$inf = GetTypeInfo($type);
		$r = array (
			"types"		=> $types,
			"items"		=> $items,
			"text"		=> LoadText($this->SubjectTextPrefix . $type, $this->Caption),
			"itemcount"	=> count($items),
			"type"		=> $type,
			"userightsover" => ($inf["rights_over_for_properties"] == "Y") ? "Ja" : "Nein"
		);
		
		return $r;
	}

	function actionChangeUser($post) {
		global $User;
		$User->requireRight($this->AdminSubjectsRight);
		$old = "{$User->name}[{$User->id}]";

		$User = new User($post["id"]);
		LogAction(text_translate("?1? hat sich mit dem Account von ?2?[?3?] eingeloggt.", array (
			$old,
			$User->name,
			$User->id
		)));
	}

	function actionDeleteSubjects($post) {
		global $User;
		$User->requireRight($this->AdminHeredity);
		foreach ($post["ids"] as $id) {
			$u = CreateSubjectInstance($id);
			DeleteSubject($id);
			$msg = text_translate("Das Subject (?1?) \"?2?\" wurde gel&ouml;scht.", array (
				$u->id,
				$u->name
			));
			LogAction($msg);
			trigger_error_text($msg);
		}
		return true;
	}

	/******************************************************************************/
	/*                Subject-Eigenschaften anzeigen & bearbeiten                 */
	/******************************************************************************/

	function & _getSubject($Ident) {
		global $User;
		if (empty ($Ident) or ($Ident == $User->id))
			return $User;
		$u = CreateSubjectInstance($Ident);
		if ($u->id == $User->id)
			return $User;
		return $u;
	}

	function _subjectHeader($Ident) {
		global $User;
		$aUser = & $this->_getSubject($Ident);
		$r = array (
			"id" => $aUser->id,
			"name" => $aUser->name,
			"type" => $aUser->type,
			"frames" => array (),
			"frame" => $this->Frame,
			"showback" => $User->hasRight($this->AdminSubjectsRight
		));
		//if($User->hasRight($this->AdminSubjectsRight)) $r["frames"]["viewsubject"] = "&Uuml;bersicht";
		if (is_a($aUser, "user") or is_a($aUser, "group"))
			$r["frames"]["viewsubject"] = text_translate("&Uuml;bersicht");
		if (is_a($aUser, "subject"))
			if ($User->hasRight($this->AdminRightsRight))
			$r["frames"]["editproperties"] = text_translate("Eigenschaften");
		if (is_a($aUser, "rightsubject"))
			if ($User->hasRight($this->AdminRightsRight))
				$r["frames"]["editrights"] = text_translate("Rechte");
		if ($User->hasRight($this->AdminHeredity)) {
			if (is_a($aUser, "parentsubject"))
				$r["frames"]["editchilds"] = text_translate("Mitglieder");
			if (is_a($aUser, "childsubject"))
				$r["frames"]["editparents"] = text_translate("Gruppen");
		}
		//$this->Caption = "{$aUser->type} -> {$r[frames][$r[frame]]} [ {$aUser->name} ]";
		$this->Caption = "{$aUser->type} -> {$aUser->name}";
		return array (
			$r,
			$aUser
		);
	}

	function frameViewSubject($get) {
		global $User;
		//$User->requireRight($this->AdminSubjectsRight);

		if (!isset ($get["id"]) && isset ($get["name"])) {
			$get["id"] = MysqlReadField("SELECT id FROM " . TblPrefix() . "flip_user_subject WHERE name='" .escape_sqlData_without_quotes($get["name"]) . "'", "id", true); // WHERE type='user'
		}

		list ($r, $u) = $this->_subjectHeader($get["id"]);

		$usr = $u->getProperties();
		if (!empty ($usr["caption"]))
			$this->Caption = $usr["caption"];

		foreach (GetColumnDeteils("user") as $k => $v) {
			if (!$User->hasRightOver($v["required_view_right"], $u))
				unset ($usr[$k]);
		}

		if (isset ($usr["homepage"]) && !strpos($usr["homepage"], "://"))
			$usr["homepage"] = "http://" . $usr["homepage"];

		if (!empty ($usr["seat"])) {
			include_once ("mod/mod.seats.php");
			$usr["blockid"] = SeatGetBlockByUser($u->id);
		}

		$parentChildList = array (
			"group" => "user",
			"clan" => "user"
		);
		foreach ($parentChildList AS $parentType => $childType) {
			if (is_a($u, $parentType) and $User->hasRightOver($this->ViewGroupMembers, $u)) {
				$p = explode(";", $usr["public_member_columns"]);
				$cols = $r["cols"] = array ();
				$deteils = GetColumnDeteils($childType);
				foreach ($p as $k => $v) {
					$v = trim($v);
					if (!empty ($v)) {
						$cols[] = $v;
						$r["cols"][$v] = $deteils[$v]["caption"];
					}
				}
				array_unshift($cols, "name", "id");
				$ids = array_keys($u->getChilds());
				if (!empty ($ids)) {
					$where = "s.id IN (" . implode_sqlIn($ids) . ")";
					$r["colvals"] = GetSubjects($childType, $cols, $where, "name");

					foreach ($r["colvals"] as $key => $cval) {
						foreach ($cval AS $col => $val) {
							if(isset($deteils[$col])) {
								if (strtolower($deteils[$col]["val_type"]) == "date") {
									//Datum von Timestamp in Datum wandeln
									if (!empty ($val))
										$r["colvals"][$key][$col] = date("d.m.Y", $val);
									elseif ($val == 0) $r["colvals"][$key][$col] = "-";
								}
								elseif (strtolower($deteils[$col]["val_type"]) == "image") {
									//Bilder haben ein Array als Wert
									$r["colvals"][$key][$col] = array (
										"data" => $val
									);
								}
							}
						}
					}
				} else
					$r["colvals"] = array ();
			}
		}
		$r["user_img"] = (!empty ($usr["user_image"])) ? $usr["user_image"] : "";
		$r["user"] = $usr;
		$r["allowchange"] = $User->hasRight($this->AdminSubjectsRight);
		$r["checkin"] = $User->hasRight($this->UserCheckin);
		$r['catering'] = $User->hasRight($this->rightCateringAdmin);
		return $r;
	}

	function framegetdata($get) {
		//valid templatefiledata to supress the warning
		$this->TplFile = "text.tpl";
		$this->TplSub = "text";

		if (empty ($get["sid"]) || empty ($get["cid"])) {
			trigger_error_text(text_translate("Fehlerhafte Daten.|keine Subjekt- und SpaltenID angegeben."), E_USER_ERROR);
			return true;
		}

		$data = MysqlReadRow("SELECT c.caption, b.data, b.mimetype FROM `" . TblPrefix() . "flip_user_column` c LEFT JOIN `" . TblPrefix() . "flip_user_binary` b ON c.id = b.column_id " .
		"WHERE b.subject_id='" .escape_sqlData_without_quotes($get["sid"]) . "' AND c.id='" .escape_sqlData_without_quotes($get["cid"]) . "'");

		if (!$data)
			trigger_error_text(text_translate("Fehlerhafte Daten.|IDs ung&uuml;ltig:") .
			" sid=" . $get["sid"] . ", cid=" . $get["cid"], E_USER_ERROR);

		//output
		$this->_ShowFrame = false;
		$this->NextPage = false;
		header("Content-Type: " . $data["mimetype"]);
		if (!stristr($data["mimetype"], "image")) {
			//download
			$filename = $data["caption"];
			header("Content-Disposition: attachment; filename=$filename");
		}
		echo $data["data"];

		return true;
	}

	/*********************** edit properties ************************/

	function frameEditProperties($get) {
		global $User;
		
		ArrayWithKeys($get, array("id","type"));
		
		if ($get["id"] != "create_new_subject") {
			list ($r, $u) = $this->_subjectHeader($get["id"]);
			$properties = $u->getProperties();
			$c = GetColumnDeteils($type = $u->type);
			
			$inf = GetTypeInfo($type);
			$use_rights_over = ($inf["rights_over_for_properties"] == "Y") ? true : false;
		} else {
			RequireAllowCreateSubject($type = $get["type"]);
			$c = GetColumnDeteils($type);
			
			unset ($c["id"]);
			
			$r = array (
				"id" => "create_new_subject"
			);
			
			$this->Caption = text_translate("Neues Subjekt (?1?)", $type);
			$use_rights_over = false;
		}	
		
		foreach ($c as $k => $v) {
			if($v['val_type'] == 'Dropdown') {
				$c[$k]["vals"] = ConfigGetPossibleValues($v['name']);
			}
			
			$c[$k]["val"] = (isset($properties[$k])) ? $properties[$k] : null;			
			$c[$k]["allowempty"] = ($v["required"] == "Y") ? false : true;
			
			if ($use_rights_over) {
				$c[$k]["enabled"] = $User->hasRightOver($v["required_edit_right"], $u);
				if (!$User->hasRightOver($v["required_view_right"], $u)) {
					unset ($c[$k]);
				}
			} else {
				$c[$k]["enabled"] = $User->hasRight($v["required_edit_right"]);
				if (!$User->hasRight($v["required_view_right"])) {
					unset ($c[$k]);
				}
			}
		}
		
		$r["items"] = $c;
		
		return $r;
	}

	function frameEditClanProperties($get) {
		global $User;

		ArrayWithKeys($get, array("id","type"));

		if ($get["id"] != "create_new_subject") {
			list ($r, $u) = $this->_subjectHeader($get["id"]);
			$properties = $u->getProperties();
			$c = GetColumnDeteils($type = $u->type);

			$inf = GetTypeInfo($type);
			$use_rights_over = ($inf["rights_over_for_properties"] == "Y") ? true : false;
		} else {
			RequireAllowCreateSubject($type = $get["type"]);
			$c = GetColumnDeteils($type);

			unset ($c["id"]);

			$r = array (
				"id" => "create_new_subject"
			);

			$this->Caption = text_translate("Neues Subjekt (?1?)", $type);
			$use_rights_over = false;
		}

		foreach ($c as $k => $v) {
			if($v['val_type'] == 'Dropdown') {
				$c[$k]["vals"] = ConfigGetPossibleValues($v['name']);
			}

			$c[$k]["val"] = (isset($properties[$k])) ? $properties[$k] : null;
			$c[$k]["allowempty"] = ($v["required"] == "Y") ? false : true;

			if ($use_rights_over) {
				$c[$k]["enabled"] = $User->hasRight($v["required_edit_right"], $u);
				if (!$User->hasRightOver($v["required_view_right"], $u)) {
					unset ($c[$k]);
				}
			} else {
				$c[$k]["enabled"] = $User->hasRight($v["required_edit_right"]);
				if (!$User->hasRight($v["required_view_right"])) {
					unset ($c[$k]);
				}
			}
		}

		$r["items"] = $c;

		return $r;
	}

	function submitProperties($data) {
		global $User;
		ArrayWithKeys($data, array("subject_id", "type", "password", "name", "email", "enabled"));
		$createnew = $data["subject_id"] == "create_new_subject";
		if (!$createnew) {
			$u = & $this->_getSubject($data["subject_id"]);
			unset ($data["subject_id"]);
			$inf = GetTypeInfo($u->type);
			$use_rights_over = ($inf["rights_over_for_properties"] == "Y") ? true : false;
		} else {
			RequireAllowCreateSubject($data["type"]);
			//Benutzer m&uuml;ssen ein Passwort angeben!
			if (strtolower($data["type"]) == "user" && $data["password"] == "") {
				trigger_error_text(text_translate("Es muss ein Passwort angegeben werden!"), E_USER_ERROR);
				return false;
			}
			$u = CreateSubject($data["type"], $data["name"], $data["email"]);
			if (!is_object($u)) {
				trigger_error_text(text_translate("Das Subjekt konnte nicht erstellt werden.")."|type=".$data["type"].", name=".$data["name"].", email=".$data["email"], E_USER_WARNING);
				return false;
			}
			unset ($data["name"]);
			unset ($data["type"]);
			unset ($data["email"]);
			unset ($data["subject_id"]);
			$use_rights_over = false;
			if (is_a($u, "user"))
				$u->addParent("registered");
		}
		$ti = GetTypeInfo($u->type);
		ArrayWithKeys($ti, array("use_right_over"));
		$userro = ($ti["use_right_over"] == "Y") ? true : false;
		$c = GetColumnDeteils($u->type);
		foreach ($data as $k => $v) {
			if (empty ($c[$k]))
				unset ($data[$k]);
			elseif ($use_rights_over) {
				if (!$User->hasRightOver($c[$k]["required_edit_right"], $u))
					unset ($data[$k]);
			}
			elseif (!$User->hasRight($c[$k]["required_edit_right"])) unset ($data[$k]);
		}
    
		if (empty ($data["password"]))
			unset ($data["password"]);
		else
      $data["password"] = getPasswordHash($data["password"]);

		// wenn der Useradmin die emailadresse &auml;ndert, wird keine aktivierungsmail versandt, es sei denn, er &auml;ndert seine eigene.
		$dologout = false;
		if (!$User->hasRightOver("edit_internal_profile", $u) or ($u->id == $User->id)) {
			if (isset($data["email"]) && $data["email"] != $u->email) {
				include_once ("mod/mod.user.php");
				if (UserChangeEmail($u, $data["email"]))
					$dologout = true;
				unset ($data["enabled"]);
				unset ($data["email"]);
			}
		}
		$r = $u->setProperties($data);
		if ($createnew)
			$u->afterCreation();
		if (($u->id == $User->id) and $dologout) {
			$User = new User("Anonymous"); // ausloggen
			$this->NextPage = "index.php";
		}
		if ($r) {
			$pser = ($u->id == $User->id) ? text_translate("eigenes Profil bearbeitet.") : text_translate("Profil von ?1?[?2?] bearbeitet.", array (
				$u->name,
				$u->id
			));
			LogAction($pser);
		}
		return $r;
	}

	function actionreactivate($post) {
		$u = new User($post["id"]);
		if ($u->getProperty("random_id") == $post["code"])
			if ($u->setProperty("enabled", "Y"))
				LogAction(text_translate("Account reaktiviert."));

		$url = EditUrl(array (
			"id" => "",
			"random_id" => ""
		));
		Redirect(urldecode($url));
		$this->_ShowFrame = false;
	}

	/************* edit rights **********************/

	function frameEditRights($get) {
		global $User;
		$User->requireRight($this->AdminRightsRight);
		list ($r, $u) = $this->_subjectHeader($get["id"]);
		$r["items"] = $u->getRightsInfo();
		$r["subject_id"] = $u->id;
		return $r;
	}

	function actionDeleteSubjectRight($post) {
		global $User;
		$User->requireRight($this->AdminRightsRight);
		$u = & $this->_getSubject($post["subject_id"]);
		if ($u->remRight($post["id"], $post["controled_id"])) {
			$log = text_translate("Recht ?1? von ?2? &uuml;ber ?3? entfernt.", array (
				GetRightName($post["id"]
			), $u->name, GetSubjectName($post["controled_id"])));
			LogAction($log);
		}
	}

	function submitAddRight($data) {
		global $User;
		$User->requireRight($this->AdminRightsRight);
		$u = & $this->_getSubject($data["id"]);
		$r = $u->addRight($data["right"], $data["subject"]);
		if ($r) {
			$log = text_translate("Recht ?1? an Subjekt ?2? &uuml;ber ?3? vergeben.", array (
				GetRightName($data["right"]
			), $u->name, GetSubjectName($data["subject"])));
			LogAction($log);
		}
		return $r;
	}

	/*************** edit childs **************/

	function frameEditChilds($get) {
		global $User;
		$User->requireRight($this->AdminHeredity);
		list ($r, $u) = $this->_subjectHeader($get["id"]);
		if (!is_a($u, "parentsubject"))
			trigger_error_text(text_translate("Subjekte vom Typ ?1? k&ouml;nnen ihre Mitglieder nicht verwalten. (und sollten auch keine haben)", $u->type), E_USER_ERROR);
		$r["subject_id"] = $u->id;
		$r["childs"] = array ();
		foreach ($u->getChilds() as $id => $name)
			$r["childs"][] = array (
				"id" => $id,
				"name" => $name
			);
		return $r;
	}

	function actionAddChild($get) {
		if(isset($get['subject_id']) && isset($get['child_id'])) {
			if($this->submitChild($get)) {
				trigger_error_text(text_translate('?1? wurde hinzugefuegt.', getSubjectName($get['child_id'])));
			}
		}
		Redirect('user.php?frame=EditChilds&id='. urlencode($get['subject_id']));
	}

	function actionDeleteChilds($post) {
		global $User;
		$User->requireRight($this->AdminHeredity);
		$u = & $this->_getSubject($post["subject_id"]);
		if (!is_a($u, "parentsubject"))
			trigger_error_text(text_translate("Subjekte vom Typ ?1? k&ouml;nnen ihre Mitglieder nicht verwalten. (und sollten auch keine haben)", $u->type), E_USER_ERROR);
		foreach ($post["ids"] as $id)
			if ($u->remChild($id))
				LogAction("Kind " . GetSubjectName($id) . " von {$u->name} entfernt.");
	}

	function actionMoveChilds($post) {
		global $User;
		$User->requireRight($this->AdminHeredity);
		$u = & $this->_getSubject($post["subject_id"]);
		if (!is_a($u, "parentsubject"))
			trigger_error_text(text_translate("Subjekte vom Typ ?1? k&ouml;nnen ihre Mitglieder nicht verwalten. (und sollten auch keine haben)", $u->type), E_USER_ERROR);
		$g = & $this->_getSubject($post["group_id"]);
		if (!is_a($g, "parentsubject"))
			trigger_error_text(text_translate("Subjekte vom Typ ?1? k&ouml;nnen ihre Mitglieder nicht verwalten. (und sollten auch keine haben)", $u->type), E_USER_ERROR);
		foreach ($post["ids"] as $id)
			if ($u->remChild($id))
				if ($g->addChild($id))
					LogAction(text_translate("Kind ?1? von ?2? nach ?3? verschoben.", array (
						GetSubjectName($id
					), $u->name, $g->name)));
	}

	function submitChild($data) {
		global $User;
		$User->requireRight($this->AdminHeredity);
		$u = & $this->_getSubject($data["subject_id"]);
		if (!is_a($u, "parentsubject"))
			trigger_error_text(text_translate("Subjekte vom Typ ?1? k&ouml;nnen ihre Mitglieder nicht verwalten. (und sollten auch keine haben)", $u->type), E_USER_ERROR);
		$r = $u->addChild($data["child_id"]);
		if ($r)
			LogAction(text_translate("Kind ?1? zu ?2? hinzugef&uuml;gt.", array (
				GetSubjectName($data["child_id"]
			), $u->name)));
		return $r;
	}

	/*********************** edit parents ***************************/

	function frameEditParents($get) {
		global $User;
		$User->requireRight($this->AdminHeredity);
		list ($r, $u) = $this->_subjectHeader($get["id"]);
		if (!is_a($u, "childsubject"))
			trigger_error_text(text_translate("Subjekte vom Typ ?1? k&ouml;nnen ihre Mitglieder nicht verwalten. (und sollten auch keine haben)", $u->type), E_USER_ERROR);
		$r["subject_id"] = $u->id;
		$r["parents"] = array ();
		foreach ($u->getParents() as $id => $name)
			$r["parents"][] = array (
				"id" => $id,
				"name" => $name
			);
		return $r;
	}

	function actionDeleteParent($post) {
		global $User;
		$User->requireRight($this->AdminHeredity);
		$u = & $this->_getSubject($post["subject_id"]);
		if (!is_a($u, "childsubject"))
			trigger_error_text(text_translate("Subjekte vom Typ ?1? k&ouml;nnen ihre Gruppen nicht verwalten. (und sollten auch keine haben)", $u->type), E_USER_ERROR);
		if ($u->remParent($post["id"]))
			LogAction(text_translate("Vater ?1? von ?2? entfernt.", array (
				GetSubjectName($post["id"]
			), $u->name)));
	}

	function submitParent($data) {
		global $User;
		$User->requireRight($this->AdminHeredity);
		$u = & $this->_getSubject($data["subject_id"]);
		if (!is_a($u, "childsubject"))
			trigger_error_text(text_translate("Subjekte vom Typ ?1? k&ouml;nnen ihre Gruppen nicht verwalten. (und sollten auch keine haben)", $u->type), E_USER_ERROR);
		$r = $u->addParent($data["parent_id"]);
		if ($r)
			LogAction(text_translate("Vater ?1? zu ?2? hinzu gef&uuml;gt.", array (
				GetSubjectName($data["parent_id"]
			), $u->name)));
		return $r;
	}

	/******************************************************************************/
	/*                     Columns anzeigen und bearbeiten                        */
	/******************************************************************************/

	function frameViewColumns($get) {
		global $User;
		$User->requireRight($this->AdminColumns);
		ArrayWithKeys($get, array("type"));

		$types = array_keys(GetTypeInfo());
		$type = (in_array($get["type"], $types)) ? $get["type"] : "user";
		$cols = GetColumnDeteils($type); //MysqlReadArea("SELECT * FROM `".TblPrefix()."flip_user_column` WHERE (`type` = '$type') ORDER BY `sort_index`;");
		$last = false;
		foreach ($cols as $k => $v) {
			if (!empty ($v["callback_read"])) {
				$cols[$k]["val_type"] = "callback [" . $v["val_type"] . "]";
				$cols[$k]["default_val"] = $v["callback_read"];
			}
			$cols[$k]["last_index"] = $last;
			$last = $v["sort_index"];
		}
		$r = array (
			"types" => $types,
			"type" => $type,
			"items" => $cols,
			"text" => LoadText($this->ColumnsText,
			$this->Caption
		));
		return $r;
	}

	function frameEditColumn($get) {
		global $User;
		$User->requireRight($this->AdminColumns);
		$this->Caption = text_translate("Column bearbeiten");
		if (empty ($get["id"]))
			return array (
				"name" => "new",
				"required_edit_right" => "edit_own_profile",
				"required_view_right" => "view_personal_informations",
				"val_type" => "string"
			);
		return MysqlReadRow("SELECT u.*, c.value AS `default_val` 
		      FROM (`" . TblPrefix() . "flip_user_column` u)
		      LEFT JOIN `" . TblPrefix() . "flip_config` c ON (c.key = u.name)
		      WHERE (u.id = '" . $get["id"] . "');
		    ");

	}

	function submitColumn($data) {
		global $User;
		$User->requireRight($this->AdminColumns);
		if (empty ($data['id'])) {
			$data['sort_index'] = time();
			$data['access'] = (strtolower($data['val_type']) == 'image') ? 'binary' : 'data';
		}
		$data['name'] = strtolower($data['name']);
		$r = MysqlWriteByID(TblPrefix() . 'flip_user_column', $data, $data['id']);
		if ($r)
			LogChange(text_translate('Die <b>ProfilSpalte <a href="user.php?frame=editcolumn&amp;id=?1?">?2?</a></b> wurde ?3?.', array (
				$r,
				$data['name'],
				 (empty ($data['id']
			)) ? text_translate('erstellt') : text_translate('bearbeitet'))));
		return $r;
	}

	function actionSwitchColumns($get) {
		global $User;
		$User->requireRight($this->AdminColumns);

		$id = addslashes($get['id']);
		$t = addslashes($get['this']);
		$l = addslashes($get['last']);
		MysqlWrite("UPDATE `" . TblPrefix() . "flip_user_column` SET `sort_index` = '$t' WHERE (`sort_index` = '$l')");
		MysqlWrite("UPDATE `" . TblPrefix() . "flip_user_column` SET `sort_index` = '$l' WHERE (`id` = '$id')");
	}

	function actiondelcolumn($post) {
		global $User;
		$User->requireRight($this->AdminColumns);

		MysqlDeleteByID(TblPrefix() . "flip_user_column", $post["id"]);
		MysqlWrite("DELETE FROM " . TblPrefix() . "flip_user_data WHERE column_id='" .escape_sqlData_without_quotes($post["id"]) . "'");
	}

	/******************************************************************************/
	/*                        Rechte anzeigen & bearbeiten                        */
	/******************************************************************************/

	function frameViewRights() {
		global $User;
		$User->requireRight($this->AdminRightsRight);

		return array (
		"items" => MysqlReadArea("
		        SELECT r.*, COUNT(s.id) AS `count` 
		          FROM (`" . TblPrefix() . "flip_user_right` r)
		          LEFT JOIN `" . TblPrefix() . "flip_user_rights` s ON (s.right_id = r.id) 
		          GROUP BY r.id 
		          ORDER BY r.right;"), "text" => LoadText($this->RightsText, $this->Caption));
	}

	function frameEditRight($get) {
		global $User;
		$User->requireRight($this->AdminRightsRight);

		$this->Caption = text_translate("Recht Bearbeiten");
		if (empty ($get["id"]))
			return array (
				"right" => "neu"
			);
		return MysqlReadRowByID(TblPrefix() . "flip_user_right", $get["id"]);
	}

	function submitRight($data) {
		global $User;
		$User->requireRight($this->AdminRightsRight);
		$r = MysqlWriteByID(TblPrefix() . "flip_user_right", $data, $data["id"]);
		if ($r)
			LogChange(text_translate("Das <b>Recht <a href=\"user.php?frame=editright&amp;id=?1?\">?2?</a></b> wurde ?3?.", array (
				$r,
				$data["right"],
				 ((empty ($data["id"]
			)) ? text_translate("erstellt") : text_translate("bearbeitet")))));
		return $r;
	}

	function actionDeleteRight($post) {
		global $User;
		$User->requireRight($this->AdminRightsRight);
		$n = GetRightName($post["id"]);
		$r = MysqlDeleteByID(TblPrefix() . "flip_user_right", $post["id"]);
		if ($r)
			LogChange(text_translate("Das <b>Recht ?1?</b> wurde gel&ouml;scht.", $n));
	}

	function frameViewRight($get) {
		global $User;
		$User->requireRight($this->AdminRightsRight);
		$r = MysqlReadRowByID(TblPrefix() . "flip_user_right", $get["id"]);
		$this->Caption = text_translate("Recht ?1?", $r["right"]);
		$r["subjects"] = MysqlReadArea("
		      SELECT r.*, o.name AS `owner`, o.type AS `owner_type`, c.name AS `controled`, c.type AS `controled_type`
		        FROM (`" . TblPrefix() . "flip_user_rights` r)
		          LEFT JOIN `" . TblPrefix() . "flip_user_subject` o ON (r.owner_id = o.id)
		          LEFT JOIN `" . TblPrefix() . "flip_user_subject` c ON (r.controled_id = c.id)
		          WHERE (r.right_id = '$r[id]');
		    ");
		$r["text"] = LoadText($this->RightText, $r["caption"]);
		return $r;
	}

	/*********************************************************************************************/
	/*                                     Checkin                                               */
	/*********************************************************************************************/

	function checkUser($u, $p, & $warnings, & $buttons, & $props, & $barcode, & $signoffbutton) {
		$barcode = false;
		$signoffbutton = false;
		if (!empty ($p["birthday"]))
			$props["Alter"] = floor((time() - $p["birthday"]) / (60 * 60 * 24 * 365.25)) . " (" . date("d.m.y", $p["birthday"]) . ")";
		/*
		* besser w&auml;re, erst mit UserGetStatus den status aus zu lesen, und dann mit diesem string zu arbeiten.
		*/
		if ($u->hasRight("admin"))
			return 0;

		if ($u->hasRight("status_registered")) {
			if ($u->hasRight("status_paid")) {
				if (empty ($p["seat"]))
					$warnings[] = text_translate("Der User hat noch keinen <b class=\"b\">Sitzplatz</b>");
				else {
					$props["Sitzplatz"] = $p["seat"];

					/*   if($p["trash_deposit"] != "Y")    $buttons["settrashdeposit"] = "M&uuml;llpfand kassiert";
					   if($p["coupon"] != "Y")           $buttons["setcoupon"]       = "Essensgutschein ausgeh&auml;ndigt";
					   if($p["is_adult_checked"] != "Y") 
					   {
					     $buttons["setadultchecked"] = "ist &uuml;ber 18";
					     if($p["is_adult"] == "Y") $warnings[] = "Der User sitzt im <b class=\"b\">&uuml;18-Bereich</b>. Er muss dazu vollj&auml;hrig sein. Ist er dies nicht, braucht er einen Sitzplatz im unter-18-Bereich.";
					   }*/
					//$ue18checkin = (($p["is_adult_checked"] == "Y") or ($p["is_adult"] != "Y")) ? true : false;
					//if(($p["coupon"] == "Y") and ($p["trash_deposit"] == "Y") and !$u->hasRight("status_checked_in") and $ue18checkin)
					if (!$u->hasRight("status_checked_in"))
						$buttons["setcheckedin"] = "Checkin!";
				}
				if ($u->hasRight("status_checked_in"))
					$buttons["setcheckedout"] = "Checkout";
				if (!$u->hasRight("status_checked_in") and !$u->hasRight("status_checked_out"))
					$signoffbutton = true;
			} else {
				$warnings[] = text_translate("Der User hat noch nicht <b class=\"b\">bezahlt</b>");
				$buttons["setpaid"] = text_translate("Bezahlt");
			}
		} else {
			$warnings[] = text_translate("Der User ist noch nicht <b class=\"b\">angemeldet</b>!");
			$buttons["setregister"] = text_translate("Anmelden");
		}
	}

	function viewUser($id) {
		include_once ("mod/mod.user.php");
		$u = CreateSubjectInstance($id, "user");
		$p = $u->getProperties(array (
			"id",
			"email",
			"name",
			"givenname",
			"familyname",
			"enabled",
			"is_adult" /*,"is_adult_checked"*/
			,
			"seat",
			"ip",
			"hw_barcode",
			"birthday"
		));
		$warnings = $buttons = array ();
		$props = array (
			"ID" => $p["id"],
			"Status" => UserGetStatusName($u
		), "EMail" => $p["email"], "IP-Adresse" => $p["ip"]);
		$barcode = $signoffbutton = false;
		$this->checkUser($u, $p, $warnings, $buttons, $props, $barcode, $signoffbutton);
		return array (
			"search" => $id,
			"user" => array (
				"nick" => $p["name"],
				"name" => "$p[givenname] $p[familyname]",
				"id" => $p["id"],
				"warnings" => $warnings,
				"buttons" => $buttons,
				"props" => $props,
				"barcode" => $barcode,
				"signoffbutton" => $signoffbutton
			)
		);
	}

	function frameCheckin($get) {
		global $User;
		$User->requireRight($this->UserCheckin);
		$this->Caption = "Checkin";
		if (empty ($get["search"]))
			return array ();
		if (preg_match("/^\d+$/", $get["search"]))
			if (SubjectExists($get["search"], "user"))
				return $this->viewUser($get["search"]);

		$s = addslashes($get["search"]);
		$ids = MysqlReadCol("SELECT `subject_id` FROM `" . TblPrefix() . "flip_user_data` WHERE (`val` LIKE '%$s%');", "subject_id", "subject_id");
		if (count($ids) > 0)
			$i = " OR (s.id IN (" . implode_sqlIn(array_keys($ids)) . "))";
		else
			$i = "";
		$where = "(s.email LIKE '%$s%') OR (s.name LIKE '%$s%')$i";
		$user = GetSubjects("user", array (
			"id",
			"name",
			"email",
			"enabled",
			"givenname",
			"familyname"
		), $where, "name");

		if (count($user) == 1)
			foreach ($user as $u)
				return $this->viewUser($u["id"]);
		return array (
			"search" => $get["search"],
			"users" => $user
		);
	}

	/******************************* checkin-actions ********************************/

	function actionSetAdultChecked($post) {
		global $User;
		$User->requireRight($this->UserCheckin);
		$u = CreateSubjectInstance($post["id"]);
		if ($u->setProperty("is_adult_checked", "Y"))
			LogAction(text_translate("Checkin: User ?1? wurde als erwachsen markiert.", $u->name));
	}

	function actionSetTrashDeposit($post) {
		global $User;
		$User->requireRight($this->UserCheckin);
		$u = CreateSubjectInstance($post["id"]);
		if ($u->setProperty("trash_deposit", "Y"))
			LogAction(text_translate("Checkin: Von ?1? wurde der M&uuml;llpfand kassiert.", $u->name));
	}

	function actionSetCoupon($post) {
		global $User;
		$User->requireRight($this->UserCheckin);
		$u = CreateSubjectInstance($post["id"]);
		if ($u->setProperty("coupon", "Y"))
			LogAction(text_translate("Checkin: An ?1? wurde der Essensgutschein ausgeh&auml;ndigt.", $u->name));
	}

	function actionSetRegister($post) {
		global $User;
		$User->requireRight($this->UserCheckin);
		include_once ("mod/mod.user.php");
		if (UserSetStatus($post["id"], "registered"))
			LogAction(text_translate("Checkin: User ?1? f&uuml;r die Party angemeldet.", $u->name));
	}

	function actionSetPaid($post) {
		global $User;
		$User->requireRight($this->UserCheckin);
		include_once ("mod/mod.user.php");
		if (UserSetStatus($post["id"], "paid"))
			LogAction(text_translate("Checkin: User ?1? auf bezahlt gesetzt.", $u->name));
	}

	function actionSetBarcode($data) {
		global $User;
		$User->requireRight($this->UserCheckin);
		$u = CreateSubjectInstance($data["id"]);
		if ($u->setProperty("hw_barcode", $data["hw_barcode"]))
			LogAction(text_translate("Checkin: User ?1? hat folgenden Hardwarebarcode: ?2?", array (
				$u->name,
				$data["hw_barcode"]
			)));
		$this->actionSetCheckedIn($data);
	}

	function actionSetCheckedIn($post) {
		global $User;
		$User->requireRight($this->UserCheckin);
		include_once ("mod/mod.user.php");
		if (UserSetStatus($post["id"], "checked_in"))
			LogAction(text_translate("Checkin: User ?1? eingecheckt.", $u->name));
	}

	function actionSetCheckedOut($post) {
		global $User;
		$User->requireRight($this->UserCheckin);
		include_once ("mod/mod.user.php");
		if (UserSetStatus($post["id"], "checked_out"))
			LogAction(text_translate("Checkin: User ?1? ausgecheckt.", $u->name));
	}

	function actionSignOff($post) {
		global $User;
		$User->requireRight($this->UserCheckin);
		include_once ("mod/mod.user.php");
		include_once ("mod/mod.seats.php");
		if (UserSetStatus($post["id"], "registered"))
			LogAction(text_translate("Checkin: User ?1? abgemeldet.", $u->name));
		SeatFreeSeatByUser($post["id"]);
	}

	/*********************************************************************************************/
	/*                     Registrieren, Anmelden, Account retten usw.                           */
	/*********************************************************************************************/

	function frameRegister($get) {
		global $User;
		$User->requireRight("logged_out");
		$c = GetColumnDeteils("user");
		$c["password"]["required"] = "Y";
		$ls2flip = array ();
		include_once ("mod/mod.user.php");
		if (function_exists("UserLS2FLIPArray")) //wenn Modul vorhanden ist
			$ls2flip = UserLS2FLIPArray();
		$lansurfer_man_columns = array ();
		foreach ($c as $k => $v) {
			if ($v["required"] != "Y")
				unset ($c[$k]);
			elseif (!in_array($k, $ls2flip)) $lansurfer_man_columns[$k] = $v;
		}
		$tmp = null;
		return array (
			"items" => $c,
			"password" => ConfigGet('lanparty_password_required'),
			"ls_items" => $lansurfer_man_columns,
			"text" => LoadText($this->RegisterText,
			$this->Caption
		), "lanparty" => LoadText("user_party_agreement", $tmp),"dsgvoeinwilligung" => LoadText("dsgvo_einwilligung", $tmp2),"siteKey" => ConfigGet("google_recaptcha_data-sitekey"), "lansurferimport" => ConfigGet($this->config_lansurferimport));
	}

	//edit VulkanLAN add DSGVO
	function frameDSGVO($get) {
		global $User;
		$User->requireRight("logged_in");
		$c = GetColumnDeteils("user");
		$rawProps = $User->getProperties();
		$tmp = null;
		return array (
		  "id" => $User->id,
			"items" => $c,
			"dsgvocheck" => (($rawProps["dsgvo"] == "Y") ? "Y":"N"),
			"text" => LoadText($this->DSGVOText, $this->Caption),
			"dsgvorevoke"  => LoadText("dsgvo_revoke", $tmp3),
			"useraccountrevoke" => LoadText("user_account_revoke", $tmp4),
			"dsgvoinfo" => LoadText("dsgvo_information", $tmp1),
			"dsgvoeinwilligung" => LoadText("dsgvo_einwilligung", $tmp2));
	}


	function submitlansurfer_import($data) {
		require_once ("mod/mod.user.php");
		$ls_data = UserGetLansurfer($data["ls_email"], $data["ls_password"]);
		$data["password"] = $data["ls_password"];
		#Lansurferdaten und eingegeben Daten zusammenpacken
		$data = array_merge($data, $ls_data);
		#Daten f&uuml;r Formular merken, falls Abbruch muss man nicht alles von Hand eintragen
		global $_PostVals;
		$_PostVals = $data;
		unset ($data["ls_email"]);
		unset ($data["ls_password"]);
		#ben&ouml;tigte userdaten auf Vorhandensein pr&uuml;fen
		$columns = GetColumnDeteils("user");
		$columns["password"]["required"] = "Y";
		foreach ($columns AS $column => $columndata) {
			if ($columndata["required"] != "Y")
				continue;
			//abbruch wenn ben&ouml;tigte Daten nicht angegeben wurden
			if (!isset ($ls_data[$column]) && $data[$column] == "") {
				trigger_error_text(text_translate("Die ben&ouml;tigte Eigenschaft '?1?' wurde nicht angegeben!", $columndata["caption"]) . "|property=$key", E_USER_WARNING);
				return false;
			}
		}
		return $this->SubmitRegister($data);
	}

	function SubmitRegister($data) {
		include_once ("mod/mod.user.php");
		if (ConfigGet('lanparty_password_required') == 'N') $nopassword = true;
		if (($data['lanparty_passwordd'] == ConfigGet('lanparty_password')) OR ($nopassword)) {
			if ($siteKey != ''){
			/*********************************************************************************************/
			// include Recaptcha alra edit by naaux
			/*********************************************************************************************/
					require_once('ext/recaptcha/autoload.php'); 
				// Register API keys at https://www.google.com/recaptcha/admin
				$siteKey = ConfigGet("google_recaptcha_data-sitekey");
				$secret = ConfigGet("google_recaptcha_secretkey");
					// reCAPTCHA supported 40+ languages listed here: https://developers.google.com/recaptcha/docs/language
					$lang = "de";

					// The response from reCAPTCHA
					$resp = null;
					// The error code from reCAPTCHA, if any
					$error = null;

					$reCaptcha = new \ReCaptcha\ReCaptcha($secret); 

					// Was there a reCAPTCHA response?
					if ($_POST["g-recaptcha-response"]) {
							$resp = $reCaptcha->verify(
							$_POST["g-recaptcha-response"],$_SERVER["REMOTE_ADDR"]
						); 

					}

				if (!$data["lanparty_participate"]) {
					trigger_error_text(text_translate("Du musst den Datenschutzbestimmungen und den Teilnahmebestimmungen f&uuml;r eine Registrierung zustimmen!"), E_USER_WARNING);
					return false;
				}

					// recaptcha ende 
				/*********************************************************************************************/
			}
			unset($data['lanparty_passwordd']);
			srand((float) microtime() * 1000000);
			//non userdata
			$part = $data["lanparty_participate"];
			unset ($data["lanparty_participate"]);

			$u = CreateSubject("user", $data["name"], $data["email"]);
			if (!is_object($u)) {
				trigger_error_text(text_translate("Es konnte kein Account f&uuml;r dich erstellt werden. M&ouml;glicherweise ist bereits jemand mit deiner Email-Adresse oder deinem Nicknamen registriert."), E_USER_WARNING);
				return false;
			}

			unset ($data["name"]);
			unset ($data["email"]);
			$data["enabled"] = "N";
			$data["registration_time"] = time();
			$data["random_id"] = md5(rand());
			$data["password"] = getPasswordHash($data["password"]);
			$data["dsgvo"] = "Y";
			$data["dsgvo_status"] = "Ich, " . $data["givenname"] . " " . $data["familyname"] . " alias " . $u->name . " bin mit der Datenverarbeitung durch den " . ConfigGet($this->PageOrganisationName) ." einverstanden. Eingewilligt am " . date("d.m.Y \\u\\m H:i \\U\\h\\r");
			$u->setProperties($data);

			if (ConfigGet("sendmessage_disable_email") == "N") {
				include_once ("mod/mod.sendmessage.php");
				if (!SendSysMessage("user_account_activate", $u)) {
					trigger_error_text(text_translate("Die Aktivierungsmail f&uuml;r deinen Account konnte nicht an ?1? gesendet werden. Bitte &uuml;berpr&uuml;fe die Email-Adresse auf ihre Richtigkeit. Der Account wurde nicht erstellt.", $u->email), E_USER_WARNING);
					DeleteSubject($u);
					return false;
				}
				$this->NextPage = "text.php?name=user_registered_successfully";
			} else {
				$this->NextPage = "text.php?name=user_registered_successfully_nom";
			}

			$u->addParent("registered");
			LogAction(text_translate("neuer Account erstellt f&uuml;r ?1?", $u->name));

		if ($part)
			if (UserSetStatus($u, "registered"))
				LogAction(text_translate("und f&uuml;r die Lanparty angemeldet. (?1?)", $u->name));

			return true;
		} else {
			trigger_error_text(text_translate("Du konntest nicht angemeldet werden, das Passwort ist falsch."), E_USER_WARNING);
			return false;	
		}
	}

	function SubmitDSGVO($data) {
		include_once ("mod/mod.user.php");
		$u = CreateSubjectInstance($data["id"], "user");
		$rawProps = $u->getProperties();
		if ($data["dsvgo1"]) {
			$dsgvo = array();
			$dsgvo["dsgvo"] = "Y";
			$dsgvo["dsgvo_status"] = "Ich, " . $rawProps["givenname"] . " " . $rawProps["familyname"] . " alias " . $u->name . " bin mit der Datenverarbeitung durch den Verein VulkanLAN einverstanden. Eingewilligt am " . date("d.m.Y \\u\\m H:i \\U\\h\\r");
			$u->setProperties($dsgvo);
		}
	}

	function frameuser4turnier($get) {
		global $User;
		$User->requireRight($this->Adduser4TurnierRight);
		$this->Caption = text_translate("User erstellen");
		$c = GetColumnDeteils("user");
		$c["password"]["required"] = "Y";
		foreach ($c as $k => $v)
			if ($v["required"] != "Y")
				unset ($c[$k]);
		return array (
			"items" => $c,
			
		);
	}

	function submituser4turnier($data) {
		include_once ("mod/mod.user.php");
		global $User;
		$User->requireRight($this->Adduser4TurnierRight);

		srand((float) microtime() * 1000000);

		$u = CreateSubject("user", $data["name"], $data["email"]);
		if (!is_object($u)) {
			trigger_error_text(text_translate("Es konnte kein Account f&uuml;r dich erstellt werden. M&ouml;glicherweise ist bereits jemand mit deiner Email-Adresse oder deinem Nicknamen registriert."), E_USER_WARNING);
			return false;
		}

		unset ($data["name"]);
		unset ($data["email"]);
		$data["enabled"] = "Y";
		$data["registration_time"] = time();
		$data["random_id"] = md5(rand());
		$data["password"] = getPasswordHash($data["password"]);
		$u->setProperties($data);

		$u->addParent("registered");
		UserSetStatus($u, "checked_in");
		LogAction(text_translate("Turnieruser ?1? erstellt.", $u->name));
		return true;
	}

	function actionActivate($post) {
		$u = CreateSubjectInstance($post["id"], "user");
		if (!is_object($u))
			return trigger_error_text(text_translate("Die UserID ist ung&uuml;ltig.") . "|UserID:$post[id]", E_USER_ERROR);

		$prop = $u->getProperties(array (
			"random_id",
			"enabled"
		));
		if ($prop["enabled"] == "Y")
			return text_translate("Dein Account ist bereits aktiviert.");
		if (empty ($prop["random_id"]))
			return trigger_error_text(text_translate("Dein Account ist nicht auf eine Aktivierung vorbereitet. Bitte fordere erneut die Aktivierungsmail an."), E_USER_ERROR);
		if ($prop["random_id"] == $post["code"]) {
			if ($u->setProperties(array (
					"enabled" => "Y",
					"random_id" => ""
				))) {
				LogAction(text_translate("Der Account von ?1? wurde aktiviert.", $u->name));
				if (empty ($this->FrameVars["frame"]))
					return text_translate("Der Account wurde erfolgreich aktiviert.");
				else
					return trigger_error_text(text_translate("Dein Account wurde aktiviert."));
			} else
				trigger_error_text(text_translate("Fehler beim Schreiben der Eigenschaften."));
		} else
			return trigger_error_text(text_translate("Der angegebene Code ist ung&uuml;ltig.") . "|code:$post[code] random_id: $prop[random_id]", E_USER_ERROR);
	}

	function frameRegistered() {
		return LoadText($this->ActivatedText, $this->Caption);
	}

	/******************************** passwort resetten **********************************/

	function frameResetPwd() {
		return array (
			"text" => LoadText($this->AccountResetPwdText,
			$this->Caption
		));
	}

	function submitReset($p) {
		$u = CreateSubjectInstance($p["email"], "user");
		$u->setRandomID();

		if (ConfigGet("sendmessage_disable_email") == "Y") {
			trigger_error_text(text_translate("Das versenden von Emails wurde deaktiviert. Deshalb kann eine Passwort&Auml;nderung z.Z. nicht durchgef&uuml;hrt werden."), E_USER_WARNING);
			return false;
		} else {
			include_once ("mod/mod.sendmessage.php");
			if (SendSysMessage("user_account_changepassword", $u))
				return text_translate("Eine Email mit einem Link unter dem du dein Password &auml;ndern kannst wurde an dich versandt.");
			trigger_error(text_translate("Die Mail f&uuml;r deinen Account konnte nicht an ?1? gesendet werden. Bitte &uuml;berpr&uuml;fe die Email-Adresse auf ihre Richtigkeit. Du kannst sie <a href=\"user.php?frame=changeemail\">hier &auml;ndern</a>.", htmlentities_single($u->email)), E_USER_WARNING);
			return false;
		}
	}

	function frameSetPwd($g) {
		$this->Caption = text_translate("Passwort setzen");
		$u = CreateSubjectInstance($g["id"], "user");
		$rnd = $u->getProperty("random_id");
		if (empty ($rnd))
			trigger_error_text(text_translate("Der Account wurde nicht auf das setzen des Passwortes vorbereitet, oder das Passwort wurde bereits gesetzt."), E_USER_ERROR);
		if ($g["code"] == $rnd)
			return $g;
		else
			trigger_error_text(text_translate("Der angegebene Code ist ung&uuml;ltig."), E_USER_ERROR);
	}

	function submitSetPwd($p) {
		$u = CreateSubjectInstance($p["id"], "user");
		$rnd = $u->getProperty("random_id");
		if (empty ($rnd))
			trigger_error_text(text_translate("Der Account wurde nicht auf das setzen des Passwortes vorbereitet, oder das Passwort wurde bereits gesetzt."), E_USER_ERROR);
		if ($p["code"] == $rnd) {
			$u->setProperties(array (
				"password" => getPasswordHash($p["password"]
			), "random_id" => ""));
			LogAction(text_translate("?1? hat sein Passwort ge&auml;ndert.", $u->name));
			return text_translate("Das Passwort wurde ge&auml;ndert.");
		} else
			trigger_error_text(text_translate("Der angegebene Code ist ung&uuml;ltig."), E_USER_ERROR);
		return false;
	}

	/******************************* aktivierungsmail erneut senden *****************************/

	function frameResendActivation($get) {
		return array (
			"text" => LoadText($this->AccountResendActivation,
			$this->Caption
		));
	}

	function submitResend($data) {
		$u = CreateSubjectInstance($data["ident"]);

		if ($u->getProperty("enabled") == "Y")
			return text_translate("Dein Account ist nicht deaktiviert, daher ist es nicht n&ouml;tig eine Aktivierungsmail zu senden. Du kannst dich sofort einloggen.");
		$u->setRandomID();

		if (ConfigGet("sendmessage_disable_email") == "Y") {
			trigger_error_text(text_translate("Das versenden von Emails wurde deaktiviert. Deshalb kann keine Aktivierungsmail gesendet werden."), E_USER_WARNING);
			return false;
		} else {
			include_once ("mod/mod.sendmessage.php");
			if (SendSysMessage("user_account_reactivate", $u))
				return text_translate("Die Aktivierungsmail wurde an dich versandt.");
			trigger_error(text_translate("Die Mail f&uuml;r deinen Account konnte nicht an ?1? gesendet werden. Bitte &uuml;berpr&uuml;fe die Email-Adresse auf ihre Richtigkeit. Du kannst sie <a href=\"user.php?frame=changeemail\">hier &auml;ndern</a>.", htmlentities_single($u->email)), E_USER_WARNING);
			return false;
		}
	}

	/******************************** emailadresse &auml;ndern ****************************************/

	function frameChangeEmail($get) {
		return array (
			"text" => LoadText($this->AccountChangeEmail,
			$this->Caption
		));
	}

	function submitChangeEmail($data) {
		$u = UserLogin($data["ident"], $data["pass"], false);
		if (!is_object($u))
			return false;

		if ($u->getProperty("enabled") == "Y")
			return text_translate("Dein Account ist nicht deaktiviert. Wenn du deine Email-Adresse &auml;ndern m&ouml;chtest, tue dies bitte, wenn du eingeloggt bist, in deinem Profil (meine daten -> Profil).");

		include_once ("mod/mod.user.php");
		return UserChangeEmail($u, $data["email"]);
	}

	/*********************************************************************************************/
	/*                                      Login                                                */
	/*********************************************************************************************/

	function frameLogin($get, $post) {
		global $User;
		$this->Caption = "Login";
		return array (
			"isloggedin" => $User->isLoggedIn(), 
			"user" => $User->name, 
			"loginident" => isset ($post["ident"]) ? $post["ident"] : ""
		);
	}
}

RunPage("UserPage");
?>
