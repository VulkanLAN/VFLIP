<?php
/**
 * @author Daniel Herrmann
 * @version $Id$
 * @copyright (C) 2001-2004 The FLIP Project Team
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package tpl
 **/

/****************************** Funktionen ***********************************/

function requiredOptions() {
	return array (
		'Experte',
		'db_host',
		'db_name',
		'db_pass',
		'db_user',
		'root_password'
	);
}

/**
 * Gruppierung der Seiten inkl. &uuml;berschrift und Text
 */
function getConfigGroups() {
	return array (
		'php' => array (
			'name' => 'Einleitung',
			'comment' => 'Dieser Assistent stellt die Grundlagen f&uuml;r ein lauff&auml;higes FLIP bereit. Er erstellt eine Konfigurationsdatei f&uuml;r FLIP und spielt eine Basisdatenbank ein.<br/>'."\n" .
			' Die zu erstellende Datei ist die zentrale Konfigurationsdatei des FLIPs. Sie enth&auml;lt nur grundlegende Optionen um das FLIP lauff&auml;hig zu machen. Alles weitere kann (sobald das FLIP l&auml;uft) &uuml;ber das Webinterface unter admin -> config angepasst werden.',
			'prefix' => 'Expert'
		),
		'db' => array (
			'name' => 'Datenbank',
			'comment' => 'Daten f&uuml;r die Verbindung zur Datenbank',
			'prefix' => array (
				'table',
				'root',
				'db'
			)
		),
		'log' => array (
			'name' => 'Log-Dateien',
			'comment' => '<p>Ein Log kann in mehrere Dateien gleichzeitig geschrieben werden, ihre Namen m&uuml;ssen dann durch getrennt durch ein "|" angegeben werden.</p>
				Beispiel: <code>$CoreConfig["log_action"] = "log1dir/log.action.php|log45dir/log.action.php";</code>

				<p> Standard Dateiberechtigung sollten wie folgt gesetzt sein:<br>
					Generelle Infos: https://wiki.ubuntuusers.de/Rechte/ <br>
					<ul>
					<li>Ordner Berechtigung: 755 (rwxr-xr-x)</li>
					<li>Datei Berechtigung: 644 (rw-r--r--)</li>
					</ul>
				</p>

				<p>Wenn "db" als Dateiname angegeben wird, so schreibt das FLIP die Logeintr&auml;ge in die Datenbank. Von dort
				k&ouml;nnen sie mittels log.php?frame=viewdblog ausgelesen werden. Der Changelog wird standardm&auml;&szlig;ig in eine 
				Datei und in die Datenbank geschrieben.</p>
				
				<p>Es ist nicht schlimm, wenn die Logfiles in einem von au&szlig;en &uuml;ber HTTP erreichbaren Verzeichnis liegen.
				Zumindest so lange sie die Dateiendung .php haben. Sie beginnen mit ein wenig PHP-Code (&lt;?php return 0; ?>), 
				so dass der Webserver niemals ihren Inhalt preisgeben wird.</p>',
			'prefix' => 'log'
		),
		'tmp' => array (
			'name' => 'Sonstiges',
			'comment' => 'Optionen zum Caching und zur Fehlersuche',
			'prefix' => array (
				'cache_path',
				'image_tmp',
				'text_tmp',
				'debug'
			)
		),
		'summary' => array (
			'name' => 'Zusammenfassung',
			'comment' => 'Bitte folgende Daten &uuml;berpr&uuml;fen.',
			'prefix' => ''
		),
		'import' => array (
			'name' => 'Konfiguration',
			'comment' => 'Konfiguration wurde erstellt.',
			'prefix' => 'Es gibt keine Optionen hierf&uuml;r'
		),
		'ende' => array (
			'name' => 'Installation beendet',
			'comment' => 'Das FLIP-Entwicklerteam w&uuml;nscht dir mit dem FLIP viel Spass!<br /><br />
					Standard Admin Zugangsdaten sind:<br /><br />
						Username: test<br /><br />
						Passwort: test<br /><br />
					Bitte den Ordner install oder alternativ die Datei install.php l&ouml;schen und beim Verzeichnis core/ ggf. die CHMOD &Auml;nderung r&uuml;ckg&auml;ngig machen! (Schreibrechte entziehen)<br />
					<a href="../index.php">Los geht\'s</a><br /><br />
					Template Anpassungen sind noch durchzufuehren:<br /><br />
					
					In der Template Datei sind alle Email Adressen anzupassen:<br />
					./tpl/VulkanLAN_bstrap4/user.tpl<br />
					-> info@event.local<br /><br />
					In der Template Datei ist die URL Rewrite Adresse anzupassen:<br />
					./tpl/VulkanLAN_bstrap4/inc.page.tpl<br />
					-> document_base_url: \'https://event.local\',
					<br />
					',
			'prefix' => 'Es gibt keine Optionen hierf&uuml;r'
		),
		
	);
}

function installOptions() {
	return array (
		array (
			'name' => 'Experte',
			'value' => '',
			'type' => 'boolean',
			'comment' => 'Sollen alle Optionen angezeigt werden? Sonst werden nur die wichtigsten abgefragt.'
		),
		array (
			'name' => 'db_Beispieldaten',
			'value' => 'checked',
			'type' => 'boolean',
			'comment' => 'Soll einige Beispieldaten eingespielt werden? (empfohlen)'
		)
	);
}

function isExpert() {
	return isset ($_SESSION['Experte']) ? $_SESSION['Experte'] == 'checked' : false;
}

/**
 * Parst die default-Config und liefert ein Array mit allen Optionen
 * @return Array Schl&uuml;ssel: name, value, type, comment
 */
function defaultConfig() {
	static $defaults = array ();
	if (empty ($defaults)) {
		$defaults = installOptions();
		$filename = '../core/core.config.default.php';
		$logfile_options = array (
			'log_error',
			'log_warning',
			'log_notice',
			'log_action',
			'log_change',
			'log_dbupdate'
		);
		if (!is_readable($filename)) {
			die('Es konnte keine default-config gefunden werden (' . $filename . '). Diese Datei muss vorhanden sein.');
		}
		$lines = file($filename);
		foreach ($lines AS $line) {
			if (preg_match('/^\$CoreConfig\[\s*"?\'?([^\]]+)*\]\s*=\s*"?\'?([^;]+)\s*;\s*(\/\/(.*))?$/', $line, $result)) {
				$lastPattern = '/["\']$/';
				$key = preg_replace($lastPattern, '', trim($result[1]));
				$value = preg_replace($lastPattern, '', trim($result[2]));
				if ($value == 'true' || $value == 'false') {
					$type = 'boolean';
				}
				elseif (in_array($key, $logfile_options)) {
					$type = 'logfile';
					$vals = explode('|', $value);
					$value = array ();
					$logTypes = array_keys(logFileTypes());
					foreach ($logTypes AS $part) {
						foreach ($vals AS $aValue) {
							if (strpos($aValue, $part) !== false) {
								$value[$part] = 1;
								break;
							}
						}
					}
				}
				elseif (substr($value, 0, 4) == 'md5(') {
					$type = 'md5';
					preg_match('/^md5\(\s*["\']?(.*)$/', $value, $realValue);
					$value = '';
					if (isset ($realValue[1])) {
						$realValue = strrev($realValue[1]);
						preg_match('/^\)\s*["\']?(.*)/', $realValue, $realValue);
						$value = isset ($realValue[1]) ? strrev($realValue[1]) : '';
					}
				} else {
					$type = 'string';
				}
				$comment = isset ($result[4]) ? trim($result[4]) : '';
				$defaults[] = array (
					'name' => $key,
					'value' => $value,
					'type' => $type,
					'comment' => $comment
				);
			}
		}
	}
	return $defaults;
}

/**
 * Array mit m&ouml;glichen Typen f&uuml;r Logdateien
 * z.Z. Datei oder Datenbank
 * @return Array Schl&uuml;ssel gibt Teil des Wertes in der Default-Config an, Wert
 * ist Beschreibung
 */
function logFileTypes() {
	return array (
		'log_dir' => 'Datei',
		'db' => 'Datenbank'
	);
}

/**
 * Liefert alle Config-Optionen welche mit einem Prefix aus $prefix beginnen
 * 
 * @param mixed $prefix Anfang der Optionen
 * @return Array
 */
function getConfig($prefix = array ()) {
	if (!is_array($prefix))
		$prefix = array ($prefix);
	$config = defaultConfig();
	$r = array ();
	//nicht grad performant

	foreach ($prefix AS $p) {
		foreach ($config AS $key => $option) {
			if ($p === '' || strpos($option['name'], $p) === 0) {
				$r[] = $config[$key];
			}
		}
	}
	return $r;
}

/**
 * Gibt den Dateinamen vom FLIP-Basedir aus zur flipconfig.php an
 */
function getConfigName() {
	return 'core' . DIRECTORY_SEPARATOR . 'flipconfig.php';
}

function post2session() {
	// GePOSTete Variablen in die Session uebernehmen
	foreach ($_POST as $pn => $pv) {
		if (!get_magic_quotes_gpc()) {
			$pv = addslashes($pv);
		}
		$_SESSION[$pn] = $pv;
	}
}

function allDataChecks() {
	$r = array ();
	$r = array_merge(phpCheck(), $r);
	$r = array_merge(dbCheck(), $r);
	$r = array_merge(tmpCheck(), $r);
	$r = array_merge(logCheck(), $r);
	return $r;
}

function phpCheck() {
	$phpver = substr(phpversion(), 0, 1);
	$r = array ();
	if (($phpver == "7.0") or ($phpver == "7.1"))
		$r['php'] = array (
			'ok' => true,
			'text' => 'Die PHP-Version ist in Ordnung!'
		);
	else
		$r['php'] = array (
			'ok' => false,
			'text' => 'Die PHP-Version ist nicht in Ordnung!<br/>Bitte PHP 7.0 verwenden!'
		);
	$r['config'] = _configCheck();
	$r['config']['ok'] = true; //nur Warnung ausgeben
	return $r;
}

function _configCheck() {
	$configFileName = '..' . DIRECTORY_SEPARATOR . getConfigName();
	$r = false;
	//testen ob vorhandene Datei schreibbar ist
	if (is_file($configFileName) && is_writeable($configFileName)) {
		$r = true;
	} else {
		$r = dirIsWriteable(dirname($configFileName));
	}

	if ($r) {
		$ok = true;
		$text = 'Die Config kann geschrieben werden.';
	} else {
		$ok = false;
		$text = '<span style="color:red;">Die Config kann nicht geschrieben werden!<br/><small>Es wird eine Datei erzeugt, welche manuell eingespielt werden muss.</small></span>';
	}

	return array (
		'ok' => $ok,
		'text' => $text
	);
}

function dbCheck() {
	$r = array ();
	$db_connection = new mysqli($_SESSION["db_host"], $_SESSION["db_user"], $_SESSION["db_pass"], $_SESSION["db_name"]);
	if ($mysqli -> connect_errno)
	{
		printf("Verbindung fehlgeschlagen: %s\n", $mysqli->connect_error);
		exit();
	}
	
	$ok = false;
	if ($db_connection) { // Verbindung OK...
		$mysql_ver = substr(@ mysqli_get_server_info ($db_connection), 0, 1);
		if (($mysql_ver == "5") or ($mysql_ver >= "5")) {
			$db_close = @ mysqli_close ($db_connection);
			$text = "Verbindung zur Datenbank erfolgreich.";
			$ok = true;
		} else {
			$text = "Falsche MYSQL Version";
			$ok = false;
		}
	} else {
		$text = "Die Verbindung ist Fehlgschlagen: " . mysqli_error();
		$ok = false;
	}

	return array (
		'db' => array (
			'ok' => $ok,
			'text' => $text
		)
	);
}

// Test der Verzeichnisrechte vom Log-Verzeichnis, der cfg Datei, usw.
function tmpCheck() {
	$tmpConfig = getConfigGroups();
	$tmpConfig = $tmpConfig['tmp'];
	$dirs = array ();
	$base = '..' . DIRECTORY_SEPARATOR;
	foreach ($tmpConfig['prefix'] AS $option) {
		if (isset ($_SESSION[$option])) {
			$dirs[$option] = $base . $_SESSION[$option];
		}
	}
	$defaultpw = array_shift(getConfig('root_password'));
	$pwok = true;
	$pwtext = '';
	if ($_SESSION['root_password'] == $defaultpw['value']) {
		/* default pw is allowed
		$pwok = false;
		*/
		$pwtext = '<br/><h2 style="color:red;">Du benutzt das default root_password. Dies ist ein Sicherheitsrisiko!</h2>';
	}
	return array (
		'log' => checkDirs($dirs
	), 'pw' => array (
		'ok' => $pwok,
		'text' => $pwtext
	));
}

// Test der Verzeichnisrechte vom Log-Verzeichnis, der cfg Datei, usw.
function logCheck() {
	$base = '..' . DIRECTORY_SEPARATOR;
	$dirs = array (
		'root' => $base,
		'log' => $base . $_SESSION["log_dir"],
		'tmp' => $base . 'tmp',
		'captcha' => $base . 'ext' . DIRECTORY_SEPARATOR . 'captcha' . DIRECTORY_SEPARATOR . '__TEMP__',
		
	);
	return array (
		'log' => checkDirs($dirs
	));
}

function checkDirs($dirs) {
	$ok = true;
	$text = 'Alle ben&ouml;tigten Verzeichnisse sind schreibbar.';
	foreach ($dirs AS $name => $dir) {
		if (!dirIsWriteable($dir)) {
			if ($ok) {
				$ok = false;
				$text = 'In folgenden Verzeichnissen kann nicht geschrieben werden:<br/>';
			}
			$text .= $dir . '<br/>';
		}
	}

	return array (
		'ok' => $ok,
		'text' => $text
	);
}

function summaryCheck() {
	$fails = array ();
	foreach (allDataChecks() AS $check) {
		if (!$check['ok'])
			$fails[] = $check['text'];
	}
	if (count($fails) == 0) {
		$cn = getConfigName();
		$ok = config_write('..' . DIRECTORY_SEPARATOR . $cn);
		if ($ok) {
			$text = 'Konfiguration wurde erstellt.';
		} else {
			$ok = true;
			//TODO
			//FIXME automatische Weiterleitung bei nicht Experten ausschalten
			//HACK
			global $shownOptions;
			$shownOptions++;
			$text = '<div style="color:red;">Die Datei "' . $cn . '" konnte nicht geschrieben werden. Bitte f&uuml;ge manuell den Inhalt <a href="../tmp/flipconfig.txt" target="_blank">dieser Datei</a> ein oder lade sie herunter und speichere sie im Webserververzeichnis als ' . $cn . ' ab.' .
			'<br /><br />Danach ist die Konfiguration in Ordnung und kann in Betrieb genommen werden.</div>';
		}
	} else {
		$ok = false;
		$text = implode("<br/>\n", $fails);
	}
	return array (
		'summary' => array (
			'ok' => $ok,
			'text' => $text
		)
	);
}

function importCheck() {
	// Step 1 - connect to the database and check if the session is valid
	$dbconnection = new mysqli($_SESSION["db_host"], $_SESSION["db_user"], $_SESSION["db_pass"], $_SESSION["db_name"], false);
	
	if ($dbconnection === false) {
		return array (
			'import' => array (
				'ok' => false,
				'text' => 'Fehler: Es konnte keine Datenbankverbindung hergestellt werden!<br/><small>' . mysqli_error() . '</small>'
			)
		);
	}
	
	// Step 1a - Set the correct connection encoding
		if (!mysqli_set_charset($dbconnection, "utf8")) {
			printf("Error loading character set utf8: %s\n", mysqli_error($dbconnection));
			exit();
		} else {
			printf("Current character set: %s\n", mysqli_character_set_name($dbconnection));
		}
	
	// Step 2 - Try selecting the database. If this fails, try creating it.
	$database_selected = mysqli_select_db($dbconnection, $_SESSION["db_name"]);
	if($database_selected == false) {
		$db_created = mysqli_query($dbconnection,'CREATE DATABASE `' . $_SESSION["db_name"] . '` COLLATE utf8_bin');
		
		// Either the database exists and we don't have access to it
		// or it doesn't and we are not allowed to create new databases.
		if($db_created === false) {
			return array (
				'import' => array (
					'ok' => false,
					'text' => 'Fehler: Die Datenbank konnte nicht angelegt werden!<br/><small>' . mysqli_error() . '</small>'
				)
			);			
		}
		// Select it afterwards
		mysqli_select_db($_SESSION["db_name"]);
	}
	
	// Step 3 - Check the existing and selected database and install FLIP's 
	// data model, stopping prematurely if the database is not empty.
	$prefix = $_SESSION["table_prefix"];
	
	if (mysqli_query($dbconnection,"SELECT * FROM `" . mysqli_real_escape_string ($dbconnection,($prefix) . "flip_config` LIMIT 1"))) {		
		return array (
			'import' => array (
				'ok' => false,
				'text' => 'Flip ist bereits mit dem Prefix "' . $prefix . 
						  '" installiert. Bitte erst den Inhalt der Datenbank "' . $_SESSION['db_name'] . 
						  '" mit dem Prefix "' . $prefix . '" l&ouml;schen.<br />'
			)
		);	
	} 
	// Read the file flip.sql and import it
	if (is_file('flip.sql')) {
		$sql_file = file_get_contents('flip.sql'); //lesen
		$sql_file = preg_replace("/(\015\012|\015|\012)/", "\n", $sql_file);
		$sql_file = str_replace("\$prefix\$", $prefix, $sql_file); // prefix einfuegen
		$sql_statements = explode(";\n", $sql_file); // Nach Befehlen teilen
		
		foreach ($sql_statements as $sql_statement) { // Befehle einfuegen
			if (trim($sql_statement) != '') { // Zeilenumbrueche und Leerzeilen am Anfang/Ende entfernen
				if (mysqli_query($dbconnection, $sql_statement) === false) { // in DB Schreiben
					return array (
						'import' => array (
							'ok' => false,
							'text' => 'Fehler (' . mysqli_error() .  ') bei Zeile:<br/>"' . $sql_statement . '"'
						)
					);
				}
			}
		}
		
		// Check success by trying to read the config from the database.
		$db_config_exists = mysqli_query($dbconnection,"SELECT * FROM `" . mysqli_real_escape_string ($dbconnection, ($prefix) . "flip_config` LIMIT 1"));
		
		if($db_config_exists === false) {
			return array (
				'import' => array (
					'ok' => false,
					'text' => 'Fehler beim Eintragen in die Datenbank. ' . 
							  'Bitte &uuml;berpr&uuml;fe noch einmal die Daten und ob die Datenbank "' . $_SESSION['db_name'] . 
							  '" existiert.<br /><br />'
				)
			);
		}
		
		// Step 4 - Import optional data
		if ($_SESSION["db_Beispieldaten"] == "checked") {
			if (is_file('flip-testing.sql')) {
				$sql_file = file_get_contents('flip-testing.sql'); //lesen
				$sql_file = preg_replace("/(\015\012|\015|\012)/", "\n", $sql_file);
				$sql_file = str_replace("\$prefix\$", $prefix, $sql_file); //prefix einfuegen
				$sql_statements = explode(";\n", $sql_file); //Nach Befehlen teilen
				foreach ($sql_statements as $sql_statement) { //Befehle einfuegen
					if (trim($sql_statement) != '') { // Zeilenumbrueche und Leerzeilen am Anfang/Ende entfernen
						mysqli_query($dbconnection, $sql_statement); // in DB Schreiben
					}
				}
				// Erfolg pruefen
				if (!mysqli_query($dbconnection, "SELECT * FROM `" .  mysqli_real_escape_string ($dbconnection, ($prefix) . "flip_config`"))) {
					echo "<input type=\"hidden\" name=\"page\" value=\"1\">";
					echo "<br />Fehler beim Eintragen in die Datenbank. Bitte ueberpruefe noch einmal die Daten und ob die Datenbank \"$_SESSION[db_name]\" existiert.<br /><br />";
					echo "<input class=\"button\" type=\"submit\" value=\"&lt;- Zur&uuml;ck\"></form></div>";
				}
			} else {
				$text = "Die Datei <em>flip-testing.sql</em> ist nicht vorhanden. Daher kann die Datenbank nicht initialisiert werden.<br/>\n" .
				"<ul><li>Entweder die Datei in das Verzeichnis von install.php (" . dirname(__FILE__) . ") kopieren</li>" .
				"<li>oder die Datei manuell in die Datenbank einspielen.<br/>";
				return array (
					'import' => array (
						'ok' => false,
						'text' => $text
					)
				);
			}
		}
	} else {
		$text = "Die Datei <em>flip.sql</em> ist nicht vorhanden. Daher kann die Datenbank nicht initialisiert werden.<br/>\n" .
		"<ul><li>Entweder die Datei in das Verzeichnis von install.php (" . dirname(__FILE__) . ") kopieren</li>" .
		"<li>oder die Datei manuell in die Datenbank einspielen.<br/>" .
		"Bei der base-Version z.B. mit <code>mysql -u " . $_SESSION["db_user"] . (($_SESSION["db_pass"] != "") ? " -p" : "") . " " . $_SESSION["db_name"] . " &lt; flip.sql</code>.<br/>\n" .
		"Bei der \"full\"-Version z.B. mit <code>cd db/; ./exec-sql-file</code> (bei Fehlern die Datei 'tools/sql-file/sql-file.config.php' anpassen).<br/>\n" .
		"</li></ul>";
		
		return array (
			'import' => array (
				'ok' => false,
				'text' => $text
			)
		);
	}
	
	// Remove the current session from existance
	session_destroy();
	
	return array (
		'import' => array (
			'ok' => true,
			'text' => 'Import the Datenbank erfolgreich!'
		)
	);
}

/**
 * Pr&uuml;ft, ob ein Verzeichnis schreibbar bzw. erstellbar ist
 * 
 * @param String $dir Verzeichnisname (von getcwd() aus)
 */
function dirIsWriteable($dir) {
	// Split = regex = nicht gut f&uuml;rs Windows-Ordnertrennzeichen
	// und gibt 'Warning: split() [function.split]: REG_EESCAPE' unter WinDoof
	// Daher is explode besser ;)
	$subdirs = explode(DIRECTORY_SEPARATOR, $dir); 
	$r = false;
	$dirname = '.'; //nur relative Pfade
	foreach ($subdirs AS $aDir) {
		$last = $dirname;
		$dirname .= DIRECTORY_SEPARATOR . $aDir;
		if (!is_dir($dirname)) {
			if (is_writeable($last)) { //parent is writeable
				//Ordner kann erstellt werden
				$r = true;
				break;
			}
		}
	}
	if (!$r) {
		$r = is_writeable($dirname);
	}
	return $r;
}

function skipOptions() {
	$installOptions = installOptions();
	foreach ($installOptions AS $installOption) {
		$skip[] = $installOption['name'];
	}
	return $skip;
}

function config_write($fullFilename) {
	$skip = skipOptions();
	// config muss geschrieben werden
	$input = "<?php \n";
	foreach (getConfigGroups() AS $aPage=>$group) {
		if (in_array($aPage, array('summary','import','ende'))) //TODO: das muss auch besser gehen (evtl. mit Flag 'noConfig' o.&Auml;.)
			continue;
		if (function_exists('htmlspecialchars_decode')) { //ab PHP 5.1.0
			$group['name'] = htmlspecialchars_decode($group['name']);
			$group['comment'] = htmlspecialchars_decode($group['comment']);
		}
		$input .= '/* ' . $group['name'] . "\n " . $group['comment'] . "\n*/\n";
		foreach (getConfig($group['prefix']) as $option) { // Variablen in die Config Schreben
			if (in_array($option['name'], $skip))
				continue;
			$key = $option['name'];
			$val = getSystemValue($option);
			$input .= "\$CoreConfig[\"$key\"] = $val; // " . $option['comment'] . "\n"; //input beschreiben	 
		}
		$input .= "\n";
	}
	$input .= "// Verzeichnisse erstellen, falls nicht vorhanden.\n"
		."if(!empty(\$CoreConfig[\"db_performancelogdir\"])) ForceDir(\$CoreConfig[\"db_performancelogdir\"],E_USER_ERROR);\n"
		."if(!empty(\$CoreConfig[\"cache_path\"])) ForceDir(\$CoreConfig[\"cache_path\"],E_USER_ERROR);\n"
		."ForceDir(\$CoreConfig[\"log_dir\"],E_USER_ERROR);\n"
		."ForceDir(\$CoreConfig[\"image_tmp\"],E_USER_ERROR);\n"
		."ForceDir(\$CoreConfig[\"text_tmp\"],E_USER_ERROR);\n"
		."\n"
		."if(empty(\$CoreConfig[\"root_password\"])) die(\"Das Root-Passwort wurde nicht gesetzt.\");\n"
		."?>";
	
	$tmpDir = '..' . DIRECTORY_SEPARATOR . 'tmp/'; //wichtig: Slash am Ende
	if (dirIsWriteable($tmpDir)) {
		if (!is_dir($tmpDir)) {
			mkdir($tmpDir);
		}
		$fileha = fopen($tmpDir . 'flipconfig.txt', 'w');
		fwrite($fileha, $input);
		fclose($fileha);
	}
	$check = _configCheck();
	if (!$check['ok']) {
		return false;
	}
	if ($fileh = fopen($fullFilename, 'w')) {
		fwrite($fileh, $input);
		fclose($fileh);
		return true;
	} else {
		return false;
	}
}

function getSystemValue($option) {
	$key = $option['name'];
	$val = isset ($_SESSION[$key]) ? $_SESSION[$key] : null;
	switch ($option['type']) {
		case 'boolean' : // "" in false umwandeln
			if ($val)
				$val = 'true';
			else
				$val = 'false';
			break;
		case 'md5' :
			$val = 'md5("' . $val . '")';
			break;
		case 'logfile' :
			$vals = array ();
			foreach (logFileTypes() AS $part => $caption)
				if (isset ($_SESSION[$key .
					'_' . $part])) {
					switch ($part) {
						case 'db' :
							$vals[] = 'db';
							break;
						case 'log_dir' :
							$level = explode('_', $key);
							$level = $level[1];
							$vals[] = '$CoreConfig[log_dir]log.' . $level . '.php';
							break;
					}
				}
			$val = '"' . implode('|', $vals) . '"';
			break;
		case 'string' :
		default :
			$val = '"' . $val . '"';
	}
	return $val;
}

function getHumanValue($option) {
	if (!is_array($option))
		return '';
	$key = $option['name'];
	$val = isset ($_SESSION[$key]) ? $_SESSION[$key] : '';
	switch ($option['type']) {
		case 'boolean' :
			$val = ($val == "checked") ? "Ja" : "Nein";
			break;
		case 'logfile' :
			$vals = array ();
			foreach (logFileTypes() AS $part => $caption) {
				$valKey = $key . '_' . $part;
				if (isset ($_SESSION[$valKey]) && $_SESSION[$valKey] == 'checked') {
					$vals[] = $caption;
					continue;
				}
			}
			$val = implode(', ', $vals);
			break;
		default :
			}
	return $val;
}

function chk2XHtml($chk) {
	return ($chk == 'checked') ? 'checked="checked"' : '';
}

function pageHeader() {
?>
<html>
<head>
<title>VFLIP - Installation</title>
<style type="text/css">
body     { background: White; color: Black; font-family: Courier; font-variant: normal; font-weight: normal; font-style: normal; }
.table   { border: 1px solid Black; align: center;}
.sframe  { padding: 10px 10px 0px 10px; border: 4px double Gray; background: #B8D2B7; }
.button  { border-style : solid; border-width : 1px; border-color : gray; background-color : silver; text-decoration: none;}
.edit    { border-style : solid; border-width : 1px; border-color : gray; padding-left : 3px; padding-right : 3px; padding-top: 1px; padding-bottom: 1px; background-color: White; color: black; }
.check   { border-style : solid; border-width : 1px; border-color : gray; padding: 10px; margin-bottom : 10px; }
.cluster { border: 2px dashed Black; }
.important { color : red; font-weight : bold; }
</style>
</head>
<body>
<br/>
<h1 align="center">VFLIP Installation</h1>
<?php

}

function pageFooter() {
	echo "</body></html>";
}

/***************************** Seitenaufbau **********************************/

//Session starten
$sessionname = 'VFlipInstall';
if(isset($_GET[$sessionname])) {
	ini_set('session.use_only_cookie', 0);
}
session_cache_expire(10); // Session bleibt 10min. erhalten
session_name($sessionname); // Wir geben dem Kind einen Namen ;)
session_start(); // Eine Session zum Speichern der Config starten


post2session();

$config = getConfigGroups();
//name und nummer der Seite
$pages = array_keys($config);
$page = (isset ($_POST['page']) && in_array($_POST['page'], $pages)) ? $_POST['page'] : $pages[0];
$ipage = array_flip($pages);
$ipage = $ipage[$page];
$sid = (!isset($_COOKIE[$sessionname]) && $ipage > 0) ? SID : '';

if(!isset($_COOKIE[$sessionname]) && !isset($_GET[$sessionname]) && $ipage > 0) {
	// keine SessionID
	ini_set('session.use_only_cookies', 0);
	if(ini_get('session.use_only_cookies')) {
		echo '<h1>Fehler</h1>Dein Browser akzeptiert keine Cookies!';
		trigger_error('Die Installation ben&ouml;tigt Cookies!', E_USER_ERROR);
		exit();
	} else {
		// SessionID in URL
		$sid = SID;
	}
}

//     PAGE START    //
pageHeader();

// Ueberschrift
echo '<h3 align="center">Konfiguration - Schritt ' . ($ipage +1) . ' von ' . count($pages) . "</h3>\n";
echo '<table align="center" style="width:600px;"><tr valign="middle"><td class="sframe">' . "\n";

$error = false;
$shownOptions = 0;
if ($page != 'end') {
	//Check
	if (isset ($pages[$ipage -1])) {
		$checkName = $pages[$ipage -1];
	} else {
		$checkName = '';
	}
	$checkFunction = $checkName . 'Check';
	$checks = array ();
	if (function_exists($checkFunction)) {
		$checks = call_user_func($checkFunction);
	}
	if (count($checks) > 0) {
		echo '<div class="check">' . "\n";
		echo '<h3 style="text-align:center;margin-top:5px;">' . $config[$checkName]['name'] . '-Pr&uuml;fung</h3>' . "\n";
		foreach ($checks AS $check) {
			if ($check['ok']) {
				$color = 'green';
			} else {
				$error = true;
				$color = 'red';
			}
			echo '<div align="center" style="color:' . $color . ';">' . $check['text'] . '</div>' . "\n";
		}
		echo '</div>' . "\n";
	}

	//Abschnitts-Titel
	echo '<h2>' . $config[$page]['name'] . '</h2>' . "\n";
	echo '<p>' . $config[$page]['comment'] . '</p>' . "\n";
	echo '<form action="install.php?'.$sid.'" align="right" method="post" name="formular">' . "\n";

	if (!$error) {
		//Datenformular
		if (!is_array($config[$page]['prefix'])) {
			$config[$page]['prefix'] = array (
				$config[$page]['prefix']
			);
		}
		foreach (getConfig($config[$page]['prefix']) AS $option) {
			$key = $option['name'];
			$caption = $key;
			$hidden = '';
			if (in_array($key, requiredOptions())) {
				$caption = '<span class="important">' . $caption . '</span>';
			}
			elseif (!isExpert()) {
				$hidden = 'display:none;';
			}
			echo '<div style="margin-bottom:10px;' . $hidden . '" title="' . $option['comment'] . '">' . $caption . ':';
			$val = (isset ($_SESSION[$key]) ? $_SESSION[$key] : $option['value']);
			switch ($option['type']) {
				case 'logfile' :
					foreach (logFileTypes() AS $type => $caption) {
						$valKey = $key . '_' . $type;
						$checked = isset ($_SESSION[$valKey]) ? $_SESSION[$valKey] : (isset ($option['value'][$type]) ? 'checked' : '');
						echo ' <input type="checkbox" name="' . $valKey . '" value="checked"' . chk2XHtml($checked) . '/> <span style="font-size:0.9em;">' . $caption . '</span>';
					}
					break;
				case 'boolean' :
					echo ' <input type="radio" name="' . $key . '" value="checked"' . chk2XHtml($val) . '/> <span style="font-size:0.9em;">Ja</span>';
					echo ' <input type="radio" name="' . $key . '" value=""' . chk2XHtml($val != 'checked' ? 'checked' : '') . '/> <span style="font-size:0.9em;">Nein</span>';
					break;
				default :
					echo '<br/><input class="edit" name="' . $key . '" type="text" value="' . $val . '" size="40"/>';
			}
			echo '<br/>';
			echo '<small style="display:none;">' . $option['comment'] . '</small></div>' . "\n";
			if ($hidden == '')
				$shownOptions++;
		}
	}
	//Aktion (weiter oder zur&uuml;ck)
	$action = true;
	if (!$error) {
		$buttonText = 'Weiter ->';
		if (isset ($pages[$ipage +1]))
			$nextpage = $pages[$ipage +1];
		else
			$action = false;
	}
	elseif ($ipage > 0) {
		$buttonText = '&lt;- Zur&uuml;ck';
		$nextpage = $pages[$ipage -1];
	} else {
		$action = false;
	}
	echo '<br/><hr size="2">' . "\n";
	echo '<div align="center">' . "\n";
	if ($action) {
		if (!$error && $shownOptions == 0) {
			//mit JavaScript leere Seiten &uuml;berspringen
			echo '<script type="text/javascript">window.setTimeout("document.formular.weiterButton.click()", 1)</script>';
		}
		echo '<input type="hidden" name="page" value="' . $nextpage . '"/>';
		echo '<input class="button" type="submit" value="' . $buttonText . '" name="weiterButton"/>';
	}
	echo "\n" . '</form>' . "\n";
} else {
	// Letzte Seite
	$defaultpw = array_shift(getConfig('root_password'));
	if ($_SESSION['root_password'] == $defaultpw['value']) {
		echo '<div align="center"><h1 style="color:red;">Warnung</h1>';
		echo '<h2 style="color:red;">Du benutzt das default-Passwort. Dies ist ein Sicherheitsrisiko!</h2></div>';
	}

	echo "<div align=\"center\"><b>Bitte die folgenden Daten ueberpruefen:</b></div><br/>";

	echo "<div align=\"center\" class=\"cluster\"></br>";
	$skip = skipOptions();
	foreach (getConfigGroups() AS $group) {
		echo '<div align="center"><b>' . $group['name'] . '</b></div><br/>';
		echo "<table align=\"center\" border=\"0\">";
		foreach (getConfig($group['prefix']) AS $option) {
			if (!in_array($option['name'], requiredOptions()) && !isExpert())
				continue;
			if (in_array($option['name'], $skip))
				continue;
			$val = getHumanValue($option);
			echo '<tr title="' . $option['comment'] . '"><td>' . $option['name'] . ':</td><td style="padding-left: 20px">' . $val . '</td></tr>';
		}
		echo '</table><br/>';
	}
	echo '</div><br/>';
	echo '<form style="float:left;" action="install.php?'.$sid.'" align="right" method="post" name="formular">' . "\n";
	echo '<input class="button" type="submit" value="&lt;- zum Anfang" name="weiterButton"/>';
	echo '<input type="hidden" name="page" value="' . $pages[0] . '"/>';
	echo '</form>';
	echo '<form style="text-align:right;" action="install.php?'.$sid.'" align="right" method="post" name="formular">' . "\n";
	echo '<input class="button" type="submit" value="Konfiguration erstellen ->" name="weiterButton"/>';
	echo '<input type="hidden" name="page" value="create"/>';
	echo '</form>';
}

echo '<br/><small style="color:gray;">MouseOver f&uuml;r Beschreibung der Formularfelder.</small>';
echo '</div></td></tr></table><br/>' . "\n";

pageFooter();
?>
