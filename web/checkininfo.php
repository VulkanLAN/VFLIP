<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: checkininfo.php 1702 2019-01-09 09:01:12Z loom $ edit by naaux
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ('core/core.php');
require_once ('inc/inc.page.php');

class CheckinInfoPage extends Page {
	var $ViewInfoRight = 'checkininfo_view'; // recht &uuml;ber die subjekte, deren checkininfo man einsehen darf.
	var $AdminRights	= "checkin";
	var $CheckininfoText = 'checkininfo_info';
	var $SubnetConfig = 'checkininfo_subnetmask';
	var $DNSConfig = 'checkininfo_dnsserver';
	var $WINSConfig = 'checkininfo_winsserver';
	var $HTTPConfig = 'checkininfo_webserver';
	var $FTPConfig = 'checkininfo_ftpserver';
	var $ShowIPConfig = 'checkininfo_show_ip';
	var $LANPartyFullTitle = 'lanparty_fulltitle';
	var $LANPartyDateStart = 'lanparty_date';
	var $LANPartyDateEnd = 'lanparty_date_end';
	var $LANPartyLocation = 'lanparty_location';

	
	

	function frameDefault($get, $post = array()) {
		include_once ('inc/inc.text.php');
		global $User;
		$u = (empty ($get['id'])) ? $User : CreateSubjectInstance($get['id'], 'user');
		$User->requireRightOver($this->ViewInfoRight, $u);
		$r = $u->getProperties();
		$r['text'] = LoadText($this->CheckininfoText, $this->Caption);
		$r['caption'] = $this->Caption;
		/** add for testing **/
		$r['userimg'] = (!empty ($r["user_image"])) ? $r["user_image"] : "";
		/** end testing **/
		$r['mask'] = ConfigGet($this->SubnetConfig);
		$r['dns'] = ConfigGet($this->DNSConfig);
		$r['wins'] = ConfigGet($this->WINSConfig);
		$r['http'] = ConfigGet($this->HTTPConfig);
		$r['ftp'] = ConfigGet($this->FTPConfig);
		$r['showip'] = (ConfigGet($this->ShowIPConfig) == 'Y') ? true : false;
	
		$r['lantitle'] = ConfigGet($this->LANPartyFullTitle);
		$r['lanstartdate'] = date('d.m.Y',ConfigGet($this->LANPartyDateStart));
		$r['lanenddate'] = date('d.m.Y',ConfigGet($this->LANPartyDateEnd));
		$r['lanlocation'] = ConfigGet($this->LANPartyLocation);
		include_once ('mod/mod.config.php');
		if (ConfigCanEdit())
			$r['configurl'] = 'config.php#checkininfo';
		return $r;
	}

	/** edit by naaux neues Feature - Ausweis/Eintrittskarte ausdrucken im CheckIN - fuer Jeden User */
	function frameDefaultAusweis($get, $post = array()) {
		include_once ('inc/inc.text.php');
		global $User;
		$u = (empty ($get['id'])) ? $User : CreateSubjectInstance($get['id'], 'user');
		$User->requireRight($this->AdminRights, $u);
		$r = $u->getProperties();
		$r['text'] = LoadText($this->CheckininfoText, $this->Caption);
		$r['caption'] = $this->Caption;
		$r['lantitle'] = ConfigGet($this->LANPartyFullTitle);
		$r['lanstartdate'] = date('d-m-Y',ConfigGet($this->LANPartyDateStart));
		$r['lanenddate'] = date('d-m-Y',ConfigGet($this->LANPartyDateEnd));
		$r['showip'] = (ConfigGet($this->ShowIPConfig) == 'Y') ? true : false;
		$r['userimg'] = (!empty ($r["user_image"])) ? $r["user_image"] : "";
		include_once ('mod/mod.config.php');
		if (ConfigCanEdit())
			$r['configurl'] = 'config.php#checkininfo';
		return $r;
	}
	
	function framePrint($get) {
		$this->ShowMenu = false;
		$this->TplSub = 'default';
		$r = $this->frameDefault($get);
		$r['printing'] = true;
		$r['text'] = LoadText($this->CheckininfoText, $this->Caption, false);
		return $r;
	}
        /** edit by naaux neues Feature - Ausweis/Eintrittskarte ausdrucken im CheckIN - fuer Jeden User */
		function frameAusweis($get) {
		$this->ShowMenu = false;
		$this->TplSub = 'ausweis';
		$r = $this->frameDefaultAusweis($get);
		$r['printing'] = true;
		$r['text'] = LoadText($this->CheckininfoText, $this->Caption, false);
		return $r;
	}
}

RunPage('CheckinInfoPage');
?>
