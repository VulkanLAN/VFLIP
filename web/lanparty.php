<?php
/**
 * @author Daniel Raap
 * @version $Id: lanparty.php 1703 2019-02-19 22:11:20Z scope $ edit by naaux, Tominator
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");
require_once ("inc/inc.text.php");

class LanpartyPage extends Page {
	//Module
	var $Usermod = "mod/mod.user.php";
	var $Seatmod = "mod/mod.seats.php";
	var $ConfigMod = "mod/mod.config.php";
	#own
	var $Lanpartymod = "mod/mod.lanparty.php";
	//Config
	var $TitleConfig = "lanparty_fulltitle";
	var $DateConfig = "lanparty_date";
	var $GuestsConfig = "lanparty_maxguests";
	var $AdditionalGroupsConfig = "lanparty_partlist_addgroups";
	var $CheckBankConfig = "banktransfer_last_check";
	var $FreeCalcMethod = 'lanparty_stats_regasnonfree';
	//Rechte
	var $AnonymousRight = "logged_out";
	var $UserRight = "logged_in";
	var $SetPaidRight = "lanparty_set_paid";
	var $ShowPaidRight = "lanparty_show_paidlog";
	var $ViewInternalProfile = "view_internal_profile";
	var $ViewUserProfile = "logged_in";	
	var $leaderRight = "clan_leader";
	//Texte
	var $maintext = "lanparty_guestlist";
	var $registertext = "lanparty_register";	
	var $registertextconsole = "lanparty_registerconsole";
	var $unregistertext = "lanparty_unregister";
	var $unregistertextconsole = "lanparty_unregisterconsole";
	var $agreementtext = "user_party_agreement";
	var $text_checked_in = "user_status_checked_in";
	var $text_checked_out = "user_status_checked_out";
	var $text_logged_out = "user_logged_out";
	var $text_nothing = "user_status_nothing";
	var $text_online = "user_status_online";
	var $text_paid = "user_status_paid";
	var $text_paid_clan = "user_status_paid_clan";
	var $StatusPaidClan = "status_paid_clan";
	var $text_registered = "user_status_registered";
	var $text_registered_console = "user_status_registered_console";
	//Gruppen
	var $allusersgroup = "registered";
	var $statusgroupprefix = "status_";
	var $liststatusgroups = array ("registered", "registered_console", "paid", "paid_clan", "checked_in", "online", "offline", "checked_out");
	//Userdaten
	var $clancolumn = "clan";
	//intern
	var $Title, $Count, $Date;

	//php 7 public function __construct()
	//php 5 original function LanpartyPage()
	function __construct() {
		//php 5:
		//parent :: Page();
		//php7 neu:
		parent::__construct();
		//Config lesen
		$this->Title = ConfigGet($this->TitleConfig);
		$this->Count = ConfigGet($this->GuestsConfig);
		$this->Date = ConfigGet($this->DateConfig);
		$this->Caption = $this->Title;
	}

	function loadCurrentUserStatusText(& $caption) {
		global $User;
		if (!$User->isLoggedIn())
			$status = 'logged_out';
		elseif (!($status = UserGetStatus($User))) $status = 'nothing';
		$statustext = "text_$status";
		return LoadText($this-> $statustext, $caption);
	}

	function getRegBar($paid, $reg, $sold=0) {
		$max = ConfigGet($this->GuestsConfig);
		$regasnonfree = ConfigGet($this->FreeCalcMethod);		
		$free = $max - $sold - (($regasnonfree == 'Y') ? ($reg + $paid) : $paid);
		if ($free < 0)
			$free = 0;

		$r = array ();
		$r['paid'] = array ('count' => $paid, 'width' => round(($paid / $max) * 100));
		$r['sold'] = array ('count' => $sold, 'width' => round(($sold / $max) * 100));
		$r['reg'] = array ('count' => $reg, 'width' => round((min($reg, $max - $paid) / $max) * 100));
		//VulkanLAN aenderung tominator u. naaux 20160920 bug in der Berechnung der Breite
		//$r['free'] = array ('count' => $free, 'width' => round(($free / $max) * 100));
		$r['free'] = array ('count' => $free, 'width' => 100 - (round(($paid / $max) * 100))- (round(($sold / $max) * 100)) - (round((min($reg, $max - $paid) / $max) * 100)));
		$r['max'] = array ('count' => $max, 'width' => 100);
		return $r;
	}

	function getClanSeatsPaid() {
		$clans = GetSubjects("clan");
		$count_clan_paid = 0;
		foreach($clans AS $k=>$clan) {
			$clan_object = CreateSubjectInstance($clans[$k]["id"], "clan");;
			if ($clan_object->hasRight('status_paid_clan'))
			{
				$rawProps = $clan_object->getProperties();
				
				$clan_no_count = "N";
				foreach($rawProps AS $key=>$val) {
					if ($key == "clan_no_seat_count") {
						$clan_no_count = $val;
						break;
					}
				}
				if  ($clan_no_count == "Y") continue;
				
				foreach($rawProps AS $key=>$val) {
					if ($key == "clan_seat_limit") {
						$count_clan_paid += $val;
						break;
					}
				}
			}	
		}
		return $count_clan_paid;
	}

	function getClanSeatsPaidNoCount() {
		$clans = GetSubjects("clan");
		$count_clan_seats_no_count = 0;
		foreach($clans AS $k=>$clan) {
			$clan_object = CreateSubjectInstance($clans[$k]["id"], "clan");;
			if ($clan_object->hasRight('status_paid_clan'))
			{
				$rawProps = $clan_object->getProperties();

				// Clans mit clan no_seat_count (typischerweiser Orga Clans) werden nicht gezaehlt
				foreach($rawProps AS $key=>$val) {
					if ($key == "clan_no_seat_count") {
						if ($val == "Y") {
							$seats = GetSeats();
							foreach($seats as $e) {
								if ($e["user_id_clan"] == $clan_object->id) {
									if ($e["reserved"] == "Y") {
										$count_clan_seats_no_count++;
									}
								}           
							} 
						}
						break;
					}
				}
			}	
		}
		return $count_clan_seats_no_count;
	}

	function frameDefault($get, $post) {
		global $User;
		include_once ($this->Usermod);
		include_once ($this->Seatmod);

		$r['text'] = LoadText($this->maintext, $this->Caption);
		$r['setpaidright'] = $this->SetPaidRight;		
		$r['status'] = $this->loadCurrentUserStatusText($r['statuscaption']);
		$r['check'] = ConfigGet($this->CheckBankConfig);
		$r ['ViewInternalProfile'] =  $User->hasRight('view_internal_profile');
		$r ['ViewUserProfile'] =  $User->hasRight('logged_in');
    
		// f&uuml;r jede statusgruppe den datensatz aus flip_user_subjects auslesen.
		$statusgroups = explode(',', ConfigGet($this->AdditionalGroupsConfig));
		$statusgroups = array_merge(array ('status_registered','status_registered_console', 'status_paid_clan', 'status_paid', 'status_checked_in', 'status_checked_out', 'status_online', 'status_offline'), $statusgroups);
		$statusgroups = implode_sqlIn($statusgroups);
		$statusgroups = MysqlReadArea("SELECT * FROM `".TblPrefix()."flip_user_subject` WHERE `name` IN ($statusgroups)", 'name');
				
		// f&uuml;r jede statusgruppe die mitglieder auslesen und eine array mit allen userids erstellen.
		$groups = $ids = array ();
		foreach ($statusgroups as $group) {
			$g = new ParentSubject($group);
			$groups[$g->name] = $users = $g->getChilds();			
			$ids = array_merge(array_keys($users), $ids);
		}

		// f&uuml;r alle user weitere daten auslesen.
		$ids = implode_sqlIn($ids);
		$seats = array();
		if (!empty ($ids)) {
			// Datenschutz
			if ($User->hasRight('view_internal_profile')) {
			  $userdata = GetSubjects('user', array ('id', 'name', 'enabled', 'clan', 'clan_url', 'age', 'plz', 'givenname', 'familyname'  ), "s.id IN ($ids)");
			}
			else $userdata = GetSubjects('user', array ('id', 'name', 'enabled', 'clan', 'clan_url'), "s.id IN ($ids)");
			$seats = SeatsGetReservedSeats();
		}									
		
		// $userdata, $groups und $seats zusammen in $alluser mergen, dabei alle nicht aktivierten user 
		// aussortieren und die verbliebenen mitglieder z&auml;hlen.
		$groupcount = $alluser = array ();
		foreach ($groups as $status => $aUser) {
			foreach ($aUser as $id => $name) {
				$u = $userdata[$id];
				if ($u['enabled'] != 'Y')
					continue;
				if(isset($seats[$id])) {
					$s = $seats[$id];
					$u['seat'] = $s['name'];
					$u['seat_id'] = $s['id'];
					$u['blockid'] = $s['block_id'];
				} else {
					$u['seat'] = '';
					$u['seat_id'] = '';
					$u['blockid'] = '';
				}				
				if(isset($groupcount[$status])) {
					$groupcount[$status]++;
				} else {
					$groupcount[$status] = 1;
				}
				$u['status'] = $status;
												
				// Edit VulkanLAN - Tominator				
				$UserObject = CreateSubjectInstance($id, "user");		
				
				// Zusaetzliche Infos wenn Rechte vorhanden
				if ($User->hasRight('view_internal_profile')) {		  			  	
				  // Event-Count ermitteln
				  $events = $UserObject->getParents();
				  $eventcount = 0;
				  foreach ($events as $ident => $name) {
  					$pos = strpos($name, "archive_");
	  				if ($pos !== false) $eventcount++;
		  		}
			    $u['eventcount'] = $eventcount;	
			  }
			  			
				// Edit VulkanLAN - Tominator
				// Tabelle zeigt auf Clan Objekt				
				$ownClans = $UserObject->getParents("clan");
        $keys = array_keys($ownClans);
        $clanLink = "";
        $clanName = "";
           
        for($i=0; $i < count($keys); ++$i) {
        	$clanId = $keys[$i];
        	$clan = CreateSubjectInstance($clanId, "clan");
        	//$clanLink = "<b><a href=\"clan.php?frame=viewclan&amp;id=$clan->id\">".escapeHtml($clan->name)."</a></b>";
        	$clanLink = "clan.php?frame=viewclan&id=$clan->id";
        	$clanName = $clan->name;
        }
				
				if ($clanLink != "") {
					$u['clan_name_url'] = $clanLink;
					$u['clan_name'] = $clanName;
				}
				
				if (!empty ($u['clan_url']) and strpos($u['clan_url'], '://') === false)
				  $u['clan_url'] = "http://$u[clan_url]";
					  
				$alluser[] = $u;
			}
		}				

		// user ausfiltern, die nicht der suchanfrage entsprechen.
		if (!empty ($post['searchtext'])) {
			$search = strtolower($post['searchtext']);
			foreach ($alluser as $key => $aUser) {
				$found = false;
				foreach ($aUser as $k => $v)
					if (strpos(strtolower($v), $search) !== false)
						$found = true;
				if (!$found)
					unset ($alluser[$key]);
			}
		}
		
		// edit VulkanLAN - Tominator
		// Doppelte User aus der Liste wieder entfernen
		$statusgroupsRank = array ('status_registered','status_registered_console', 'status_paid_clan', 'status_paid', 'status_checked_in', 'status_checked_out', 'status_online', 'status_offline');		
		foreach ($alluser as $key => $aUser) {
			$found = false;
			$allusertemp = $alluser;
			foreach ($allusertemp as $keytemp => $aUsertemp) {								
				if ($aUser["name"] == $aUsertemp["name"]) {
					$statusRankA = -1;
					$statusRankB = -1;			  				
					
					for ($i=0; $i < count($statusgroupsRank); $i++) {
						if ($aUser["status"] == $statusgroupsRank[$i]) {
							$statusRankA = $i;
							break;
						}
					}  				
					for ($i=0; $i < count($statusgroupsRank); $i++) {
						if ($aUsertemp["status"] == $statusgroupsRank[$i]) {
							$statusRankB = $i;
							break;
						}
					}
					
					if (($statusRankA >= 0) and ($statusRankB >= 0)) {
						if ($statusRankA < $statusRankB) {
							unset ($alluser[$key]);
							$groupcount[$aUser["status"]]--;
							break;
						}
					}
				}
			}
		}		

		uasort($alluser, function($a, $b) {return strnatcasecmp($a['name'],$b['name']);} );

		ArrayWithKeys($groupcount, array("status_registered","status_registered_console","status_paid","status_paid_clan","status_checked_in","status_checked_out","status_online","status_offline"));		
		$paid = $groupcount['status_paid'] + $groupcount['status_paid_clan'] + $groupcount['status_checked_in'] + $groupcount['status_checked_out'] + $groupcount['status_online'] + $groupcount['status_offline'];
		$r['user'] = $alluser;
		$r['count'] = $groupcount;		
		
		$count_clan_paid = $this->getClanSeatsPaid();	
		$count_clan_seats_paid_no_count = $this->getClanSeatsPaidNoCount();
		$paid -= $count_clan_seats_paid_no_count;

		$sold = $count_clan_paid - ($groupcount['status_paid_clan'] - $count_clan_seats_paid_no_count);
		$r['bar'] = $this->getRegBar($paid, $groupcount['status_registered'], $sold);			
		include_once ($this->ConfigMod);
		$r['can_config'] = ConfigCanEdit();
		$r['configlistgroupname'] = $this->AdditionalGroupsConfig;
		return $r;
	}

	function frameSmallStatusBar() {
		include_once ('mod/mod.lanparty.php');

		$count_clan_paid = $this->getClanSeatsPaid();
		$count_clan_seats_paid_no_count = $this->getClanSeatsPaidNoCount();
		$count_seats_clan_paid = LanpartyGetStatusCount('paid_clan'); // Teilnehmer die auf, vom Clan bezahlte, Plaetze sitzen
		$count_registered = LanpartyGetStatusCount('registered') - $count_seats_clan_paid;
		if ($count_registered < 0) { $count_registered = 0;}

		return $this->getRegBar(LanpartyGetStatusCount('allpaid') + $count_seats_clan_paid - $count_clan_seats_paid_no_count, $count_registered, $count_clan_paid - (LanpartyGetStatusCount('paid_clan') - $count_clan_seats_paid_no_count));
	}

	function frameSmallStatusBarText($get) {
		$align = (empty ($get["align"])) ? "left" : $get["align"];
		return array_merge($this->frameSmallStatusBar(), array ("align" => $align));
	}

	function frameuserstatus() {
		global $User;
		include_once ($this->Usermod);
		if (!($status = UserGetStatus($User)))
			$status = "nothing";
		if ($User->hasRight($this->AnonymousRight))
			$status = $this->AnonymousRight;
		$r["status"] = LoadText($this->{"text_".$status}, $r["statuscaption"]);
		return $r;
	}

	function frameRegister() {
		global $User;
		$User->requireRight($this->UserRight);
		//Nach der Party gilt die Anmeldung als geschlossen
		if (time() > $this->Date)
			trigger_error_text(text_translate("Die Anmeldung ist geschlossen"), E_USER_ERROR);

		//Userstatus
		global $User;
		include_once ($this->Usermod);

		$stat = UserGetStatus($User);
		//VulkanLAN Mod - add check CLAN Leader Right

		//$clan = CreateSubjectInstance($clanId, "clan");
		$clanReservation = 0;
		
		$ownClans = $User->getParents("clan");
		$keys = array_keys($ownClans);
		$clans_leadead = array ();
      
		// Clan Leader bekommt seine Clans zur Auswahl
		if (count($keys) > 0) {
			for($i=0; $i < count($keys); ++$i) 
			{
				$clanId = $keys[$i]; 
			  if ($clanId != 0) 
			  {
					$clan = CreateSubjectInstance($clanId, "clan");
					$leaders = $clan->getEntitled($this->leaderRight);            
					$keys_leader = array_keys($leaders);
				
					for($j=0; $j < count($keys_leader); ++$j)
					{                     
					  $leader = CreateSubjectInstance($keys_leader[$j], "user");
				  	if ($keys_leader[$j] == $User->id) 
				  	{ 
						  $clans_leadead += array ($clan->id=>$clan->name);
				  	} 
					}
			  }
			}
		}				
		// Ende
		
		if (empty ($stat)) {
			$r["action"] = text_translate("anmelden");
			$r["text"] = LoadText($this->registertext, $this->Caption);
			$this->Caption .= " ".$this->Title;
			$tmp = null;
			$r["agbtext"] = LoadText($this->agreementtext, $tmp);
			//$r['clans'] = $User->getParents('clan');
			$r['clans'] = $clans_leadead;
			return $r;
		}
		elseif ($stat == "registered") {
			$r["action"] = text_translate("abmelden");
			$r["agbtext"] = LoadText($this->unregistertext, $this->Caption);
			return $r;
		} elseif ($stat == "registered_console") {			
				return trigger_error_text(text_translate("Du bist bereits als Spieler f&uuml;r Konsolen ohne eigenen PC angemeldet."), E_USER_ERROR);
			}
			else {
			  return trigger_error_text(text_translate("Du bist bereits angemeldet und hast bezahlt."), E_USER_ERROR);
		}

	}
	
	function frameRegisterConsole() {
		global $User;
		$User->requireRight($this->UserRight);
		//Nach der Party gilt die Anmeldung als geschlossen
		if (time() > $this->Date)
			trigger_error_text(text_translate("Die Anmeldung ist geschlossen"), E_USER_ERROR);

		//Userstatus
		global $User;
		include_once ($this->Usermod);

		$stat = UserGetStatus($User);		
		
		if (empty ($stat)) {
			$r["action"] = text_translate("anmelden");
			$r["text"] = LoadText($this->registertextconsole, $this->Caption);
			$this->Caption .= " ".$this->Title;
			$tmp = null;
			$r["agbtext"] = LoadText($this->agreementtext, $tmp);
			return $r;
		}
		elseif ($stat == "registered_console") {
			$r["action"] = text_translate("abmelden");
			$r["text"] = LoadText($this->unregistertextconsole, $this->Caption);
			return $r;
		} elseif ($stat == "registered") {			
				return trigger_error_text(text_translate("Du bist bereits als LAN Spieler angemeldet. Wenn du an Konsolenturnieren teilnehmen willst, dann melde dich direkt in der Turnierliste an!"), E_USER_ERROR);
			}
			else {
				return trigger_error_text(text_translate("Du bist bereits angemeldet und hast bezahlt."), E_USER_ERROR);
			}		
	}

	
	function submitRegister($a) {
		global $User;
		$User->requireRight($this->UserRight);
		include_once ($this->Usermod);
		$stat = UserGetStatus($User);
		if (empty ($stat)) {			
			if ($a["lanparty_participate"] == "1") $register_lan = "1";
			                                  else $register_lan = "0";
			if ($a["lanparty_participate_console"] == "1") $register_console = "1";
			                                          else $register_console = "0";
			//if ($a["lanparty_participate"] == "1"){
			if (($register_lan == "1") || ($register_console == "1")) {
				$users = array();
				$users[$User->id] = $User->name;
				//Clans
				foreach($a AS $key=>$value) {
					if(strpos($key, 'clan') === 0) {
						if($value == '1') {
							$clanid = explode('_', $key);
							if(is_posDigit($clanid[1])) {
								$clan = CreateSubjectInstance($clanid[1], 'clan');
								if(is_object($clan)) {
									foreach($clan->getChilds() AS $uid=>$uname) {
										$users[$uid] = $uname;
									}
								}
							}
						}
					}
				}
				include_once ($this->Usermod);
				$r = true;
				foreach($users AS $userID=>$userName) {
					if (!UserGetStatus($userID)) {
						if ($register_lan == "1") {
						  if (UserSetStatus($userID, 'registered'))
							  LogAction(text_translate("$userName wurde f&uuml;r die Lanparty angemeldet."));
						}
            else {
						  if ($register_console == "1") {
						  	if (UserSetStatus($userID, 'registered'))
							    LogAction(text_translate("$userName wurde f&uuml;r die Lanparty angemeldet."));
						    if (UserSetStatus($userID, 'registered_console'))
  							  LogAction(text_translate("$userName wurde f&uuml;r das Konsolenturnier angemeldet."));
								else 
									$r = false;
		  				}		  					
						}
					}
				}
				if ($r) {
					#include_once($this->Lanpartymod);
					#Lanparty_updatelpde();
					$this->SubmitMessage = text_translate("Anmeldung erfolgreich.");
					return true;
				}
				trigger_error_text(text_translate("Anmeldung konnte nicht komplett durchgef&uuml;hrt werden."), E_USER_ERROR);
				return false;
			} else {
				trigger_error_text(text_translate("Du musst die Regeln akzeptieren um dich anzumelden."), E_USER_WARNING);
			}
		}	
		else {
			if ($stat == "registered" && $a["lanparty_participate"] == "1") {				
			  if ($this->unregUser()) {
				  LogAction(text_translate("hat sich von der Lanparty abgemeldet."));
				  $this->SubmitMessage = text_translate("Abmeldung erfolgreich.");
				  return true;
			  } else {
				  trigger_error_text(text_translate("Anmeldung konnte nicht durchgef&uuml;hrt werden."), E_USER_ERROR);
				  return false;
			  }
			}
			else if ($stat == "registered_console") {
				if ($this->unregUser()) {
				  LogAction(text_translate("hat sich vom Konsolenturnier abgemeldet."));
				  $this->SubmitMessage = text_translate("Abmeldung erfolgreich.");
				  return true;
			  } else {
				  trigger_error_text(text_translate("Anmeldung konnte nicht durchgef&uuml;hrt werden."), E_USER_ERROR);
				  return false;
			  }
			}
		}
		return false;
	}

	
	function unregUser() {
		global $User;
		$subjects = $User->getParents();
		foreach ($subjects as $s) {
			if (stristr($s, "status_") != false) {
				$User->remParent($s);
				include_once($this->Seatmod);
		  		SeatFreeSeatByUser($User->id);
				return true;
			}
		}
		return false;
	}

	function actionsetpaid($a) {
		global $User;
		include_once ($this->Usermod);
		$User->requireRight($this->SetPaidRight);
		include_once ("mod/mod.sendmessage.php");
		foreach ($a["ids"] as $uid) {
			UserSetStatus($uid, "paid");
			SendSysMessage("user_status_paid", CreateSubjectInstance($uid));
		}
		#include_once($this->Lanpartymod);
		#Lanparty_updatelpde();
	}

	function actionunsetpaid($a) {
		global $User;
		include_once ($this->Usermod);
		include_once ($this->Seatmod);
		$User->requireRight($this->SetPaidRight);
		foreach ($a["ids"] as $uid) {
			UserSetStatus($uid, "registered");
			SeatUnreserveSeatByUser($uid); //bleibt vorgemerkt
		}
		#include_once($this->Lanpartymod);
		#Lanparty_updatelpde();
	}
	
	function frameShowPaid() {
		global $User;
		$User->requireRight($this->ShowPaidRight);
		$this->Caption = "Paid-Log";
		
		$orgas = array();
		$users = array();
		
		$r["entrys"] = array();
		$r["viewpaidright"] = $this->ShowPaidRight;
		$entrys = MysqlReadArea("SELECT * FROM `".TblPrefix()."flip_setpaid_history` ORDER BY `date` DESC;","date");
		
		foreach ($entrys as $e) {
			if(!in_array($e["orga_id"],$orgas)) {
				$o_subject = CreateSubjectInstance($e["orga_id"]);
				$orgas[$e["orga_id"]] = $o_subject->name;
			}
			if(!in_array($e["user_id"],$users)) {
				$u_subject = CreateSubjectInstance($e["user_id"]);
				$users[$e["user_id"]] = $u_subject->name;
			}
			$e["date"] = date("d.m.y G:i",$e["date"]);
			$e["user_name"] = $users[$e["user_id"]];
			$e["orga_name"] = ($e["orga_id"] == 1) ? "PayPal / Anonym" : $orgas[$e["orga_id"]];
			$r["entrys"][] = $e;
		}
		return $r;
	}

	function actionEmptyLog() {
		global $User;
		$User->requireRight($this->ShowPaidRight);
		return MysqlExecQuery("TRUNCATE TABLE `".TblPrefix()."flip_setpaid_history`;");
	}
	
	function getClanSeatCount ($clan_id) {
		include_once ("mod/mod.user.php");
		global $User;
  	$clan_seat_limit_global = ConfigGet("clan_seat_limit");
		$clan = CreateSubjectInstance($clan_id, "clan");
		$clan_seat_limit = $clan_seat_limit_global;
			
		$rawProps = $clan->getProperties();
		foreach($rawProps AS $key=>$val) {
			if ($key == "clan_seat_limit") {
			  $clan_seat_limit = $val;
			  break;
			}
		}
		return $clan_seat_limit;
	}

	/**
	 * Stellt die Bezahlungen graphisch dar
	 * dazu wird die Bibliothek PHPLOT von phplot.com in ext/phplot verwendet
	 * @param Array $get GET-Paramter
	 */
	function framePaidLogGraph($get) {
		global $User;
		$User->requireRight($this->ShowPaidRight);
		
		require("ext/phplot/phplot.php");
		$graph = new PHPlot($get["x"], $get["y"]);
		//global settings
		$graph->SetDataType("data-data");
		$graph->SetTitle(text_translate("auf bezahlt gesetzte User"));
		$graph->SetPrecisionY(0);
		$graph->SetXGridLabelType("time");
		$graph->SetXTimeFormat("%d.%m.");
		$graph->SetXDataLabelPos("none"); //only tick-labels
		$graph->SetNumXTicks(($get["x"]-50)/50); //50 Pixel per label
		//data (users pers day)
		#$graphdata = MysqlReadArea("SELECT ROUND(`date`/3600/24) AS `day`, `date`, COUNT(user_id) AS usercount FROM ".TblPrefix()."flip_setpaid_history GROUP BY `day`");
		$graphdata = MysqlReadArea("SELECT FROM_UNIXTIME(`date`, '%Y-%m-%d 12:00') AS `day`, UNIX_TIMESTAMP(FROM_UNIXTIME(`date`, '%Y-%m-%d 12:00')) AS `date`, COUNT(user_id) AS usercount FROM ".TblPrefix()."flip_setpaid_history GROUP BY `day` ORDER BY `date` ");
		if(empty($graphdata)) $graphdata = array();
		$sum = $max = $i = 0;
		$sumdata = array(array(0,0,0));
		foreach($graphdata AS $key=>$row) {
			$i++;
			if($row["usercount"] > $max) $max = $row["usercount"];
			$sum += $row["usercount"];
			$sumdata[] = array($row["day"],$row["date"],$sum);
		}
		$graph->SetPrintImage(0);
		//graph of sum
		$graph->SetYTitle(text_translate("Summe User"));
		$graph->SetDataValues($sumdata);
		$graph->SetNewPlotAreaPixels(50,10,$get["x"]-10,$get["y"]-135);
		$graph->DrawGraph();
		//graph of dates
		$graph->SetYTitle(text_translate("User"));
		$graph->SetXTickLabelPos("none");
		$graph->SetDataValues($graphdata);
		$graph->SetPlotAreaPixels(50,$get["y"]-110,$get["x"]-10,$get["y"]-20);
		//recalculate scales
		$graph->SetPlotAreaWorld("","","","");
		//credits
		$graph->SetLegendPixels($get["x"]-135, $get["y"]-20);
		$graph->SetLegend("drawn with phplot.com");
		$graph->DrawGraph();
		//output
		$graph->PrintImage();
		
		$this->ShowFrame = false;
		return 1;
	}

	function framesidebar() {
		$this->ShowMenu = false;
		return $this->frameSmallCounter();
	}

	function frameSmallCounter() {
		include_once ($this->Seatmod);
		$seats = SeatsGetCounters();
		$r["seats_taken"] = $seats["taken"];
		$r["seats_marked"] = $seats["nonfree"] - $seats["taken"];
		$r["seats_free"] = $seats["max"] - $seats["nonfree"];

		foreach ($this->liststatusgroups as $agroup) {
			$group[$agroup] = $this->statusgroupprefix.$agroup;
			$group[$agroup] = CreateSubjectInstance($group[$agroup]);
		}
		$group["accounts"] = CreateSubjectInstance($this->allusersgroup);

		$r["users_max"] = ConfigGet($this->GuestsConfig);
		$r["users_registered"] = $group["registered"]->getChildsCount();
		$r["users_registered_console"] = $group["registered_console"]->getChildsCount();
		$r["users_paid"] = $group["paid"]->getChildsCount();
		$r["users_paid_clan"] = $group["paid_clan"]->getChildsCount();
		$r["users_paid_width"] = round(($r["users_paid"] / $seats["enabled"]) * 100);
		$r["users_checked_in"] = $group["checked_in"]->getChildsCount();
		$r["users_online"] = $group["online"]->getChildsCount();
		$r["users_offline"] = $group["offline"]->getChildsCount();
		$r["users_checked_out"] = $group["checked_out"]->getChildsCount();
		$r["users_all"] = $r["users_registered"] + $r["users_registered_console"] + $r["users_paid"] + $r["users_checked_in"] + $r["users_online"] + $r["users_checked_out"];

		$r["users_on_party"] = $r["users_checked_in"] + $r["users_online"] + $r["users_offline"] + $r["users_checked_out"];
		$r["users_on_party_width"] = round(($r["users_on_party"] / $seats["enabled"]) * 100);
		$r["abendkasse"] = $seats["enabled"] - ($r["users_paid"] + $r["users_on_party"]);
		$r["abendkasse_width"] = round(($r["abendkasse"] / $seats["enabled"]) * 100);

		return $r;
	}

	function frameCountdown() {
		$this->Caption = text_translate("Countdown f&uuml;r ?1?", $this->Title);
		$end = ConfigGet($this->DateConfig);
		$time = $end -time();
		if ($time < 0) {
			$r = array("days"=>null,"hours"=>null,"minutes"=>null,"seconds"=>null);
			$r["msg"] = "Es ist vorbei!";
		} else {
			$r["days"] = $days = floor($time / 3600 / 24);
			$r["hours"] = $hours = floor(($time -24 * 3600 * $days) / 3600);
			$r["minutes"] = $minutes = floor(($time -24 * 3600 * $days -3600 * $hours) / 60);
			$r["seconds"] = $seconds = floor($time -24 * 3600 * $days -3600 * $hours -60 * $minutes);
			$r["msg"] = text_translate("Noch ?1? Tage, ?2? Stunden, ?3? Minuten und ?4? Sekunden", array($days, $hours, $minutes, $seconds));
		}

		return $r;
	}

	function frameCountdown_small() {
		//TODO: mod.time benutzen
		$r = $this->frameCountdown();
		if ($r["days"] > 2)
			$r["msg"] = text_translate("noch ?1? Tage", $r["days"]);
		elseif ($r["days"] * 24 + $r["hours"] > 2) $r["msg"] = text_translate("noch ?1? Stunden", ($r["days"] * 24 + $r["hours"]));
		elseif ($r["hours"] * 60 + $r["minutes"] > 5) $r["msg"] = text_translate("noch ?1? Minuten", ($r["hours"] * 60 + $r["minutes"]));
		else
			$r["msg"] = text_translate("noch ?1? Sekunden", ($r["minutes"] * 60 + $r["seconds"]));
		return $r;
	}

	function frameSmallUserStatus() {
		include_once ("mod/mod.user.php");
		global $User;
		$r = array ();

		if (!$User->isLoggedIn())
			return array_merge($r, array ("acc" => "active"));
		else
			$r["acc"] = "done";

		$status = UserGetStatus($User);
		if (empty ($status))
			return array_merge($r, array ("reg" => "active"));
		else
			$r["reg"] = "done";

		if ($status == "registered")
			return array_merge($r, array ("pay" => "active"));
		elseif ($status == "registered_console")
				return array_merge($r, array ("pay" => "active"));
			else 
			  $r["pay"] = "done";

		$seat = $User->getProperty("seat");
		if (empty ($seat)) {
			return array_merge($r, array ("seat" => "active"));
		} else {
			$r["seat"] = "done";
			$r["seatname"] = $seat;
		}

		return $r;
	}

	function frameKontoCheck() {
		return array ("check" => ConfigGet($this->CheckBankConfig));
	}

}

RunPage("LanpartyPage");
?>
