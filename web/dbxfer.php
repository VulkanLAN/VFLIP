<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: dbxfer.php 1702 2019-01-09 09:01:12Z loom $ edit by naaux
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");
require_once ("mod/mod.dbxfer.php");
require_once ("mod/mod.rpc.php");

class DBXFerPage extends Page {
	var $Right = "dbxfer_dbtransfer";

	//php 7 public function __construct()
	//php 5 original function DBXFerPage()
	function __construct() {
		global $User;
		$User->requireRight($this->Right);
		//php 5:
		//parent :: Page();
		//php7 neu:
		parent::__construct();
	}

	function frameDefault($get, $post) {
		global $User;
		$this->Caption = "Datenbank importieren";
		$dat = DBXFerLoadUserdata();
		$scripts = (empty ($dat["scripts"])) ? array () : $dat["scripts"];
		end($scripts);
		//list ($script,) = key($scripts);
		$script = key($scripts);
		if (empty ($script))
			$script = "http://www.x.de/dbxfer.srv.php";
		return array ("user" => $User->name, "scripts" => $scripts, "script" => $script);
	}

	function _addURLToCache($url) {
		$udat = DBXFerLoadUserdata();
		$a = "";
		preg_match("/^(http:\/\/)?(.*?)(\/dbxfer\.srv\.php)?$/i", $url, $a);
		// um die url ans ende des array einzuf&uuml;gen
		unset ($udat["scripts"][$url]);
		$udat["scripts"][$url] = $a[2];
		DBXFerSaveUserdata($udat);
	}

	function submitLogin($dat) {
		global $Session;
		$this->_addURLToCache($dat["script"]);
		$Session->Data["dbxfer"] = $dat;
		if (isset ($dat["selecttables"])) {
			$this->NextPage = EditURL(array ("frame" => "selecttables"), "", false);
		}
		elseif (isset ($dat["ifchanged"])) {
			$status = $this->_read("tablestatus");
			$tables = $status->getTables();
			$this->actionImportSelected(array ("ids" => $tables), true);
		} else
			print_r($dat);
		$this->SubmitMessage = "";
		return true;
	}

	function _getSession() {
		global $Session;
		return $Session->Data["dbxfer"];
	}

	function _setSession($key, $val) {
		global $Session;
		$Session->Data["dbxfer"][$key] = $val;
	}

	function _getService($frame, $param = array ()) {
		$dat = $this->_getSession();
		$param["password"] = $dat["password"];
		$param["ident"] = $dat["user"];
		$param["frame"] = $frame;
		LogAction("DBXFer: Datenimport mittels: $dat[script]");
		return EditURL($param, $dat["script"], false);
	}

	function _read($frame, $param = array ()) {
		return RPCRead($this->_getService($frame, $param));
	}

	function frameSelectTables($get) {
		$this->Caption = "Tabellen zum Import ausw&auml;hlen";
		$status = $this->_read("tablestatus");
		if (empty ($get["showall"]))
			$status->removeEmpty();
		return array ("tables" => $status->status, "showall" => $get["showall"]);
	}

	function actionImportSelected($dat, $compare = false) {
		$dat["compare"] = $compare;
		$this->_setSession("importdata", $dat);
		$this->NextPage = EditURL(array ("frame" => "doimport"), "", false);
	}

	function actionImportIfChanged($dat) {
		$this->actionImportSelected($dat, true);
	}

	function frameDoImport($get, $post) {
		$this->Caption = "Daten werden &uuml;bertragen...";
		$sess = $this->_getSession();
		$dat = $sess["importdata"];
		$remote = $this->_read("tablelist", array ("names" => $dat["ids"]));
		$this->_setSession("userdata", DBXFerLoadUserdata());
		$this->_setSession("createtable", serialize(new CreateTableWatcher($remote)));
		$dat["tables"] = $remote->getComparison($dat["compare"]);
		$dat["anticache"] = crc32(microtime());
		return $dat;
	}

	function frameImportBlock($get) {
		SetFatalErrorCallback("FatalErrorImage");
		LogCacheActivate();
		$block = $this->_read("blockdata", $get);
		$sess = $this->_getSession();
		$create = unserialize($sess["createtable"]);
		if (!$sess["importdata"]["compare"] or !$create->tableExists($get["tablename"]))
			$create->checkCreateTable($get["tablename"]);
		$block->execute($get["tablename"], $get["firstid"], $get["lastid"]);
		return (is_array(GetErrors())) ? $this->_createImage(255, 0, 0) : $this->_createImage(0, 255, 0);
	}

	function _createImage($r, $g, $b) {
		$im = ImageCreate(8, 8);
		imagefill($im, 2, 2, ImageColorAllocate($im, $r, $g, $b));
		return $im;
	}

	function frameReport($get) {
		$sess = $this->_getSession();
		DBXFerSaveUserdata($sess["userdata"]);
		LogCacheFlush();
		return array ("error" => is_array(GetErrors()));
	}
}

function FatalErrorImage() {
	$im = ImageCreate(8, 8);
	imagefill($im, 2, 2, ImageColorAllocate($im, 255, 0, 0));
	header("Content-Type: image/png");
	imagepng($im);
}

RunPage("DBXFerPage");
?>