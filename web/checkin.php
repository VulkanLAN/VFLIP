<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: user.php 1702 2019-01-09 09:01:12Z loom $ edit by naaux/gregor/fr0nk
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");

class CheckinPage extends Page {
	//Rights
	var $EditCheckin = "checkin_edit";
	var $UserCheckin = "checkin";
	//Data
	var $InputTypes = array ("button" => "Button", "edit" => "Textfeld", "warning" => "Warnung", "text" => "Infotext", "image" => "Userbild");
	var $Statuses = array ("aktiv" => "aktiv", "optional" => "optional", "deaktiviert" => "deaktiviert");

	function checkUser($id) {
		global $User;
		$uid = $id;
		$u = CreateSubjectInstance($uid, "user");
		$warnings = $buttons = $props = array ();

		$list = MysqlReadArea("SELECT * FROM ".TblPrefix()."flip_checkin_list ORDER BY `order`");
		$propdata = GetColumnDeteils("user");

		// Count the distinct steps of the checkin
		$num_steps = 0;
		$current_step = 0;
		$final_step = 0;
		$steps_indices = array();
		
		foreach($list as $record) {
			$steps_indices[] = $record['order'];
			
			if($final_step < $record['order']) {
				$final_step = $record['order'];
			}
		}
		
		$num_steps = count($steps_indices);
		
		foreach ($list AS $item) {
			if ($item["status"] == "deaktiviert")
				continue;

			//Text immer anzeigen
			if ($item["input_type"] == "text") {
				$newProp = array(
					"name" => $propdata[$item["action_name"]]["caption"], 
					"value" => $u->getProperty($item["action_name"]), 
					"type" => $propdata[$item["action_name"]]["val_type"]
				);
				
				$props[] = $newProp;
			}
				
			//Pr&uuml;fung
			$checkmethod = "_check_".strtolower($item["check_type"]);
			$not = ($item["check_not"] == "1") ? true : false;
			
			if (in_array($checkmethod, get_class_methods($this))) {
				if ($not === $this-> {$checkmethod}($u, $item["check_name"], $item["check_value"])) {
					$current_step = $item['order'];
					switch ($item["input_type"]) {
						case "button" :
							$buttons[] = array ("type" => $item["action_type"], "name" => $item["action_name"], "text" => $this-> {
								"_actiontext_".strtolower($item["action_type"])
								}
							($item["action_name"]), "value" => $item["action_value"], "button_color" => (empty ($item["button_color"])) ? "#FF8888" : $item["button_color"], "button_type" => (empty ($item["button_type"])) ? "btn btn-secondary" : $item["button_type"]);
							break;
						case "warning" :
							$warnings[] = $this-> {
								"_checkwarn_".strtolower($item["check_type"])
								}
							($item["check_name"], $not, $item["check_value"]);
							break;
						case "edit":
							$c = GetColumnDeteils("user");
							$buttons[] = array ("input_type"=>"edit", "type" => $item["action_type"], "name" => $item["action_name"], "text" => $this-> {
								"_actiontext_".strtolower($item["action_type"])
								}
							($item["action_name"]), "value" => $item["action_value"], "action_val_type"=>$c[$item["action_name"]]["val_type"], "button_color" => (empty ($item["button_color"])) ? "#FF8888" : $item["button_color"], "button_type" => (empty ($item["button_type"])) ? "btn btn-secondary" : $item["button_type"]);
							break;
						case "image":
							$c = GetColumnDeteils("user");
							$buttons[] = array (
								"input_type"=>"image", 
							 	"type" => $item["action_type"],
								"name" => $item["action_name"],
								"text" => $this-> {"_actiontext_".strtolower($item["action_type"])} (
									$item["action_name"]
								),
								"value" => $item["action_value"],
								"action_val_type"=>$c[$item["action_name"]]["val_type"],
								"button_color" => (empty ($item["button_color"])) ? "#FF8888" : $item["button_color"],
								"button_type" => (empty ($item["button_type"])) ? "btn btn-secondary" : $item["button_type"]
							);
							break;
					}
					if ($item["status"] != "optional")
						break;
				}
			}

		}
		
		$return = array ("search" => $id, 
						 "user" => array ("id" => $u->id, 
						 				  "warnings" => $warnings, 
						 				  "buttons" => $buttons, 
						 				  "props" => $props, 
						 				  "nick" => $u->name, 
						 				  "name" => $u->getProperty("givenname")." ".$u->getProperty("familyname")
						 				  ), 
             "users" => array(),
						 "editright" => $User->hasRight($this->EditCheckin) ? "Y" : "N",
						 "current_step" => $current_step,
						 "final_step" => $final_step);
		
		return $return;
	}

	/******************************* checkin-checks ****************************
	 * Jede Check-Methode hat den Pr&auml;fix _check_<name> und zwei zugeh&ouml;rige Methoden '_checkwarn_<name>' und '_checkvalid_<name>'.
	 * Der _check_-Methode werden die Parameter User, 'Name' und 'Wert'.
	 * Die _checkwarn_-Methode dient dazu einen aussgekr&auml;ftigen Text f&uuml;r die Checkinuser auszugeben, wenn der Check fehl schl&auml;gt. Sie hat die Parameter 'name', 'not' und 'value'.
	 * Die _checkvalid_-Methode soll &uuml;berpr&uuml;fen ob der angegebene Check g&uuml;ltig ist um Checks zu verhindern, welche nicht m&ouml;glich sind. Sie hat 'Name' als Parameter. 
	 */
	
	//UserRight
	function _checkwarn_userright($name, $not, $value) {
		if ($not)
			return "Der User darf das Recht <strong>".GetRightName($name)."</strong> nicht haben!";
		else
			return "Dem User fehlt das Recht <strong>".GetRightName($name)."</strong>!";
	}
	
	function _checkvalid_userright($name)
	{
		return GetRightID($name);
	}
	
	function _check_userright($aUser, $aRight, $notused) {
		$aUser = CreateSubjectInstance($aUser);
		return $aUser->hasRight(GetRightID($aRight));
	}

	//UserProperty
	function _checkwarn_userproperty($name, $not, $value) {
		$c = GetColumns("user");
		$not = (!$not) ? " nicht" : "";
		$value = ($value == '') ? "leer" : "'$value'";
		return "Die Eigenschaft <strong>".$c[$name]."</strong> ist$not $value!";
	}
	
	function _checkvalid_userproperty($name)
	{
		$c = GetColumnDeteils("user");
		if(in_array($name, $c))
			return true;
		if(isset($c[$name]))
			return true;
		trigger_error_text("Die Eigenschaft '$name' existiert nicht!", E_USER_WARNING);
		return false;
	}
	
	function _check_userproperty($aUser, $Prop, $Val) {
		$aUser = CreateSubjectInstance($aUser);
		if ($aUser->getProperty($Prop) == $Val)
			return true;
		else
			return false;
	}
	
	function _check_userimage($aUser, $Prop, $Val = null) {
		$aUser = CreateSubjectInstance($aUser);
		if (!empty($aUser->getProperty($Prop)))
			return true;
		else
			return false;
	}

	//InGroup
	function _checkwarn_ingroup($name, $not, $value)
	{
		if ($not)
			return "Der User darf nicht in der Gruppe <strong>".GetSubjectName($name)."</strong> sein!";
		else
			return "Der User ist nicht in der Gruppe <strong>".GetSubjectName($name)."</strong>!";
	}
	
	function _checkvalid_ingroup($name)
	{
		if(SubjectExists($name, "group"))
			return true;
		trigger_error_text("Die Gruppe '$name' existiert nicht!", E_USER_WARNING);
		return false;
	}
	
	function _check_ingroup($aUser, $Name, $notused)
	{
		$aUser = CreateSubjectInstance($aUser);
		return $aUser->isChildOf($Name);
	}

	// None
	function _checkwarn_none($name, $not, $value) {
		return "Keine";
	}
	
	function _checkvalid_none($notused)
	{
		return true;
	}
	
	function _check_none($aUser, $alsonotused) {
		return true;
	}

	/******************************* checkin-actions ********************************/

	function submitItem($post) {
		global $User;
		$User->requireRight($this->UserCheckin);
		$u = CreateSubjectInstance($post["id"]);
		$this-> {
			"_action_".strtolower($post["action_type"])
			}
		($u, $post["action_name"], $post["action_value"]);
	}

	function _actiontext_setproperty($name) {
		$c = GetColumns("user");
		if (!empty ($c[$name]))
			$name = $c[$name];
		return $name." setzen";
	}
	
	function _actionvalid_setproperty($name)
	{
		return $this->_checkvalid_userproperty($name);
	}
	
	function _action_setproperty($u, $name, $value) {
    if($this->ActionVars['imgdata']) {
      $value = base64_decode($this->ActionVars['imgdata']);
    }
    
		if ($u->setProperty($name, $value))
			LogAction("Checkin: User {$u->name} wurde die Eigenschaft $name auf '$value' gesetzt.");
	}

	function _actiontext_setstatus($name) {
		include_once ("mod/mod.user.php");
		return "Status auf '".UserGetStatusName("Anonymous", $name)."' setzen";
	}
	
	function _actionvalid_setstatus($name)
	{
		include_once ("mod/mod.user.php");
		if(UserValidSetStatus($name))
			return true;
		trigger_error_text("Der Status '$name' kann nicht gesetzt werden!", E_USER_WARNING);
		return false;
	}
	
	function _action_setstatus($u, $status, $notused) {
		include_once ("mod/mod.user.php");
		if (UserSetStatus($u->id, $status))
			LogAction("Checkin: User {$u->name} wurde auf den Status '$status' gesetzt.");
	}

	function _actiontext_addtogroup($name) {
		return "Zur Gruppe '$name' hinzuf&uuml;gen";
	}
	function _actionvalid_addtogroup($name)
	{
		if(SubjectExists($name, "group"))
			return true;
		trigger_error_text("Die Gruppe '$name' existiert nicht.", E_USER_WARNING);
		return false;
	}
	
	function _action_addtogroup($u, $group, $notused) {
		if ($u->addParent($group))
			LogAction("Checkin: User {$u->name} wurde der Gruppe ".GetSubjectName($group)." hinzugef&uuml;gt.");
	}

	function _actiontext_freeseat($name) {
		return "Den Sitzplatz freigeben";
	}
	
	function _actionvalid_freeseat($notused)
	{
		return true;
	}
	
	function _action_freeseat($u, $notused, $alsonotused) {
		include_once ("mod/mod.seats.php");
		if (SeatFreeSeatByUser($u->id))
			LogAction("Checkin: User {$u->name} wurde der Sitzplatz freigegeben.");
	}

	function _actiontext_none($name) {
		return $name;
	}

	function _actionvalid_none($notused) {
		return true;
	}
	
	function _action_none($u, $notused) {
	}

	/******************************* checkin-frames ********************************/

	function framedefault($get, $post) {
		global $User;
		$User->requireRight($this->UserCheckin);
		$this->Caption = "Checkin";
		
		if (empty ($get["search"])) {
			return array ("editright" => $User->hasRight($this->EditCheckin) ? "Y" : "N", "user" => array(), "users" => array());
		}
		
		if (preg_match("/^\d+$/", $get["search"]))
			if (SubjectExists($get["search"], "user"))
				return $this->checkUser($get["search"]);

		$s = addslashes($get["search"]);

		$ids = MysqlReadCol("SELECT `subject_id` FROM `".TblPrefix()."flip_user_data` WHERE (lower(`val`) LIKE lower('%$s%'));", "subject_id", "subject_id");
		if (count($ids) > 0)
			$i = " OR (s.id IN (".implode_sqlIn(array_keys($ids))."))";
		else
			$i = "";

		$where = "(lower(s.email) LIKE lower('%$s%')) OR (lower(s.name) LIKE lower('%$s%'))$i";
		$user = GetSubjects("user", array ("id", "name", "email", "enabled", "givenname", "familyname"), $where, "name");

		if (count($user) == 1) {
			foreach ($user as $u) {
				return $this->checkUser($u["id"]);
			}
		}
				
		return array ("search" => $get["search"], 
					  "users" => $user, 
            "user" => array(),
					  "editright" => $User->hasRight($this->EditCheckin) ? "Y" : "N");
	}

	function frameeditlist() {
		global $User;
		$User->requireRight($this->EditCheckin);

		$this->Caption = text_translate("Pr&uuml;fungen des Checkins bearbeiten");
		foreach (MysqlReadArea("SELECT * FROM ".TblPrefix()."flip_checkin_list ORDER BY `order`") AS $item) {
			$not = ($item["check_not"] == "1") ? true : false;
			$item["check_text"] = $this-> {
				"_checkwarn_".strtolower($item["check_type"])
				}
			($item["check_name"], $not, $item["check_value"]);
			$item["action_text"] = $this-> {
				"_actiontext_".strtolower($item["action_type"])
				}
			($item["action_name"]);
			$item["input_type"] = $this->InputTypes[$item["input_type"]];
			$r["list"][] = $item;
		}

		return empty($r) ? array() : $r;
	}

	function actiondelitems($post) {
		global $User;
		$User->requireRight($this->EditCheckin);
		MysqlExecQuery("DELETE FROM ".TblPrefix()."flip_checkin_list WHERE id IN (".implode_sqlIn($post["ids"]).")");
	}

	function frameedititem($get) {
		global $User;
		$User->requireRight($this->EditCheckin);

		$this->Caption = text_translate("Checkinpunkt bearbeiten");

		if (!empty ($get["id"]))
			$r = MysqlReadRowByID(TblPrefix()."flip_checkin_list", $get["id"]);
		else
			$r = array ();

		if (empty ($r))
			$r["order"] = MysqlReadField("SELECT MAX(`order`)+1 AS `maxi` FROM ".TblPrefix()."flip_checkin_list", "maxi");

		$r["checktypes"] = $this->_Methods("check");
		$r["actiontypes"] = $this->_Methods("action");
		$r["inputtypes"] = $this->InputTypes;
		$r["statuses"] = $this->Statuses;

		return $r;
	}

	function submitcustomize($post) {
		global $User;
		$User->requireRight($this->EditCheckin);
		
		//Checks
		if(!$this->{"_checkvalid_".$post["check_type"]}($post["check_name"]))
			return false;
		if(!$this->{"_actionvalid_".$post["action_type"]}($post["action_name"]))
			return false;
		
		if ($post["order"] < 1)
			$post["order"] = 1;
		return MysqlWriteByID(TblPrefix()."flip_checkin_list", $post, $post["id"]);
	}

	function _Methods($prefix) {
		$a = $r = array ();
		foreach (get_class_methods($this) as $v)
			if (preg_match("/^_".$prefix."_(.*)/i", $v, $a))
				$r[$a[1]] = $a[1];
		return $r;
	}

	function framedoku() {
		return array ();
	}
}

RunPage("CheckinPage");
