<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: seats.php 1703 2019-03-16 22:05:41Z scope $ edit by naaux, datoml, tominator
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");
require_once ("mod/mod.image.php");
require_once ("mod/mod.seats.php");
require_once ("mod/mod.seats.draw.php");

class SeatsPage extends Page {
	var $AdminUserRight = "seats_manage_user";
	var $AdminSeatsRight = "seats_admin";
	var $NoteSeatRight = "seats_note";
	var $ReserveSeatRight = "seats_reserve";

	var $leaderRight = "clan_leader";
	var $AdminClanRight = "clan_admin";
	
	var $TmpPlanConfig = "seats_tmpplan";
	var $MaxNotesConfig = "seats_max_notes";
	var $ShowIPConfig = "seats_show_ip";

	var $MainText = "seats_main";
	var $BlockText = "seats_block";
	var $LegendText = "seats_afterlegend";
	var $StatusPaidClan = "status_paid_clan";
	

	var $User;
	var $LinkPost;
	var $Block;
	
	//php 7 public function __construct()
	//php 5 original function SeatsPage()
	function __construct() {
		global $User;
		//$this->Page();
		//php 5:
		//parent :: Page();
		//php7 neu:		
		parent::__construct();
		if (isset ($this->FrameVars["userid"]) and ($this->FrameVars["userid"] != $User->id)) {
			$User->requireRight($this->AdminUserRight);
			$this->User = new User($this->FrameVars["userid"]);
			$this->LinkPost = "&amp;userid={$this->User->id}";
		} else {
			$this->LinkPost = "";
			$this->User = & $User;
		}

		if (isset ($this->FrameVars["blockid"])) {
			$blockid = (empty ($this->FrameVars["blockid"])) ? 1 : (integer) $this->FrameVars["blockid"];
			$this->Block = SeatGetBlock($blockid);
		}
	}

	function frameDefault($get, $post) {
		$this->TplSub = "blockgraph";
		return $this->frameBlockGraph($get);
	}

	function frameBlockList() {
		include_once ("inc/inc.text.php");
		$r = SeatsGetOverviewHeader();
		$r["text"] = LoadText($this->MainText, $this->Caption);
		$r["blocks"] = SeatGetBlocks();
		return $r;
	}

	function frameBlockGraph($get) {
		ArrayWithKeys($get, array("userid")); 
		include_once ("inc/inc.text.php");
		$img = new ImageLoaderDB(ConfigGet($this->TmpPlanConfig));
		$r = SeatsGetOverviewHeader();
		$r["text"] = LoadText($this->MainText, $this->Caption);
		$r["img"] = $img->loadInfo();
		$r["userid"] = $get["userid"];
		$blocks = SeatGetBlocks();
			
		for($i=0; $i < count($blocks); $i++) {	
			$id = $blocks[$i]["id"]; 

  		$blue = MysqlReadField("SELECT COUNT(*) AS c FROM ".TblPrefix()."flip_seats_seats WHERE (`block_id` = '$id') and (`user_id_clan` IS NOT NULL) and ((`user_id` = 0) or (`user_id` IS NULL));", "c");
  		$red = MysqlReadField("SELECT COUNT(*) AS c FROM ".TblPrefix()."flip_seats_seats WHERE (`block_id` = '$id') and (`reserved` = \"Y\");", "c");
  		$yellow = MysqlReadField("SELECT COUNT(*) AS c FROM ".TblPrefix()."flip_seats_seats WHERE (`block_id` = '$id') and (`reserved` = \"N\") and (`user_id` != 0) ;", "c");
  		$green = MysqlReadField("SELECT COUNT(*) AS c FROM ".TblPrefix()."flip_seats_seats WHERE (`block_id` = '$id') and (`reserved` = \"N\") and ((`user_id` = 0) or (`user_id` IS NULL)) and (`user_id_clan` is NULL) and (`enabled` = \"Y\") ;", "c");
  		$grey = MysqlReadField("SELECT COUNT(*) AS c FROM ".TblPrefix()."flip_seats_seats WHERE (`block_id` = '$id') and (`enabled` = \"N\");", "c");
  		$max = $blue + $red + $yellow + $green + $grey;
 		  
 		  if ($max > 0)	{
  		  $blocks[$i]["blue_width"] = round (100/$max*$blue);
  		  $blocks[$i]["red_width"] = round (100/$max*$red);
  		  $blocks[$i]["yellow_width"] = round (100/$max*$yellow);
  		  $blocks[$i]["grey_width"] = round (100/$max*$grey);
  		  $blocks[$i]["green_width"] = 100 - $blocks[$i]["blue_width"] - $blocks[$i]["red_width"] - $blocks[$i]["yellow_width"] - $blocks[$i]["grey_width"];

  		  $blocks[$i]["blue"] = $blue;
  		  $blocks[$i]["red"] = $red;
  		  $blocks[$i]["yellow"] = $yellow;
  		  $blocks[$i]["green"] = $green;
  		  $blocks[$i]["grey"] = $grey;		  		  	
  		}
  		$blocks[$i]["max"] = $max;	
		}		

		$r["blocks"] = $blocks;
		return $r;
	}
	/** VulkanLAN alle Sitzplaetze freigeben added by naaux - mod by Tominator*/
	function frameFreeSeats() {
		global $User;
		$User->requireRight($this->AdminSeatsRight);
		$seats = GetSeats();
		foreach($seats as $e) {
			$col=GetSeatColor($e);
			if(($col == 'red')||($col=='yellow'))				
				$this->setSeat($e, $User, false, false, true);
		}
		
		// Tominator Mod fuer Freigabe der Clan Zuordnung
		$seats = GetSeats();
		foreach($seats as $e) {
			if ($e["user_id_clan"] != NULL) 
			  $this->setClanSeat($e, false, false, true);             
		} 	
		
		$_GET['frame']="blockgraph";
		return $this->frameBlockGraph($_GET);
		/**return ("Es wurden alle Sitzplaetze freigegeben");*/
	}
	
	function frameOverviewImage() {
				$img = new ImageLoaderDB(ConfigGet($this->TmpPlanConfig));
				return new DataImage($img);
	}

	function frameBlock($p) {
		global $User;
		ArrayWithKeys($p, array("ids")); 
		$User->requireRight($this->Block["view_right"]);
		if (!empty ($this->Block["link"])) {
			Redirect($this->Block["link"]);
			return true;
		}
		$tmpplan = new DataImage(SeatsGetBackgroundTmpLoader($this->Block["id"]));
		$r = $tmpplan->getInfo(false);

		$r["seats"] = array ();
		$r["found"] = "";
		$r["block_id"] = $this->Block["id"];
		foreach (SeatGetBlocks() as $block)
			$r["blocks"][$block["id"]] = $block["caption"];
		$r["admin"] = $User->HasRight($this->AdminSeatsRight);
		$maxnotes = ConfigGet($this->MaxNotesConfig);
		if ($this->User->id == $User->id) {
			include_once ("inc/inc.text.php");
			$tmp = "";
			$r["text"] = LoadText($this->BlockText, $tmp);
			$this->Caption = $this->Block["caption"];
			$r["afterlegend"] = LoadText($this->LegendText, $tmp);
			if ($this->User->hasRight($this->ReserveSeatRight))
				$r["stat"] = "Du kannst dir insgesamt einen Sitzplatz reservieren und $maxnotes Sitzpl&auml;tze vormerken.";
			else
				$r["stat"] = "Sobald du &uuml;berwiesen und dich eingeloggt hast, kannst du dir einen Sitzplatz reservieren.";
		} else {
			$this->Caption = "{$this->User->name}'s Sitzplatz";
			$r["text"] = "";
			if ($this->User->hasRight($this->ReserveSeatRight))
				$r["stat"] = escapeHtml("Du kannst f&uuml;r {$this->User->name} insgesamt einen Sitzplatz reservieren und $maxnotes SitzPl&auml;tze vormerken.");
			else
				$r["stat"] = escapeHtml("Sobald {$this->User->name} &uuml;berwiesen hat, kann er sich einen Sitzplatz reservieren.");
		}
		//list($r["width"],$r["height"]) = getimagesize($img);
		$dosearch = !empty ($p["search"]);
		$dolist = is_array($p["ids"]);
		$r["search"] = ($dosearch) ? $p["search"] : "";

		$userseats = MysqlReadArea("
		      SELECT s.*,u.name AS nickname FROM (".TblPrefix()."flip_seats_seats s)
		      LEFT JOIN ".TblPrefix()."flip_user_subject u ON (s.user_id = u.id)
		      WHERE s.block_id='".$this->Block["id"]."'
		      ORDER BY s.id;
		    ");

		$drawchairs = false;
		$f = array ();
		if ($dosearch)
			$p["search"] = strtolower($p["search"]);
		
		// Schleife VulkanLAN Mod setzt im Seats Array ein "Y" wenn der Sitzplatz hervorgehoben werden soll	
		$search_for = $p["ids"];
		for($l=0; $l < count($userseats); $l++) {
			if ($dosearch or $dolist) {
				for($k=0; $k < count($search_for); $k++) {
					if ($userseats[$l]["user_id"] == $search_for[$k]) {
						$userseats[$l]["drawchairs"] = "Y";
					}
			  }
			}
			else {
  		  if ($userseats[$l]["user_id"] == $this->User->id)
	  			$userseats[$l]["drawchairs"] = "Y";
		  	else
			    $userseats[$l]["drawchairs"] = "N";
			}
	  }		
	  	  	  
	  // Schleife zur Pruefung der Clans auf Existenz
	  $clans = GetSubjects("clan");
	  for($l=0; $l < count($userseats); $l++) {
	  	$clan_id = $userseats[$l]["user_id_clan"];
	  	if ($clan_id > 0) {	  			  			  		
	  		$clan_exist = false;
	  		foreach($clans AS $k=>$clan) {
			    if ($clan_id == $clans[$k]["id"]) $clan_exist = true;
			  }			  
			  if (!$clan_exist) {
			  	//trigger_error_text("Ein Clan (ID: $clan_id) existiert nicht mehr!");
			  	// Aufhebung der Reservierung bei Inkonsistenz
			  	$userseats[$l]["user_id_clan"] = 0;			  	
			  	$this->setSeat($userseats[$l], $User, false, false, true);
			  	$this->setClanSeat($userseats[$l], false, false, true);
			  } 	  
	  	}
	  }	  	  	  	  
	  
	  // Schleife fuer Clan Bilder und Status Paid Abfrage
	  for($l=0; $l < count($userseats); $l++) {
	  	$clan_id = $userseats[$l]["user_id_clan"];
	  	if ($clan_id > 0) {	  		
	  	  $seat_clan = CreateSubjectInstance($clan_id, "clan");
	  	  $rawProps = $seat_clan->getProperties();		  
			  foreach($rawProps AS $key=>$val) {
				  if ($key == "image") {
				    if ($val != null) $userseats[$l]["clanimage"] = $val;				    
				  }
				  if ($key == "clanseatimage") {
				    if ($val != null) {
				    	$userseats[$l]["clanimage"] = $val;
				    	break;
				    }
				  }
				}

				if ($seat_clan->HasRight($this->StatusPaidClan,	$seat_clan->id)) $userseats[$l]["clanpaid"] = "Y";
	  	}
	  }		

		foreach ($userseats AS $i) {
			$clan_id = $i["user_id_clan"];			
			if ($clan_id > 0) $clan = CreateSubjectInstance($clan_id, "clan");	  	  			
			$i["title"] = ($i["enabled"] == "Y") ? (($i["user_id"] == 0) ? "frei" . (($clan_id > 0) ? (" f&uuml;r Clan " . $clan->name) : "") : $i["nickname"] . (($clan_id > 0) ? (" (Clan: " . $clan->name . ")") : "")) : "deaktiviert";
			$i["url"] = "seats.php?frame=seat&amp;id={$i["id"]}{$this->LinkPost}";
			if (!empty ($i["cords"]))
				$i += unserialize($i["cords"]);
			else
				trigger_error_text("Einige Cache-Daten zum generieren der Imagemap Fehlen. Der Sitzplan sollte neu gezeichnet werden.");
			$r["seats"][] = $i;
			if ($i["user_id"] == $this->User->id) {
				$seats_new_template_global = ConfigGet("seats_new_template");
				if ($seats_new_template_global != 'Y'){
				$drawchairs = true;
				}
				else {;}
			}
			if ($dosearch and !empty ($i["nickname"]))
				if (strpos(strtolower($i["nickname"]), $p["search"]) !== false) {
					$f[$i["user_id"]][] = $i;
					continue;
				}
			//Anpassung VulkanLAN: hinzugef�gt: OR $i["reserved"] == "N"
			if ($dolist && $i["reserved"] == "Y")
				if (in_array($i["user_id"], $p["ids"]))
					$f[$i["user_id"]][] = $i;
		}
		
		if ($dosearch or $dolist) {
			$s = array ("result" => array ());
			$ids = array ();
			$count = 0;
			foreach ($f as $k => $v) {
				$n = array ();
				foreach ($v as $i) {
					if($dosearch) {
						//Anpassung VulkanLAN hinzugef�gt: OR $i["reserved"] == "N"
						$x = ($i["reserved"] == "Y" OR $i["reserved"] == "N") ? "<b>$i[name]</b>" : "$i[name]";
						$n[] = "<a href=\"seats.php?frame=seat&amp;id=$i[id]{$this->LinkPost}\">$x</a>";
					} elseif($dolist && $i["reserved"] == "Y" OR $i["reserved"] == "N") {
						$n[] = "<a href=\"seats.php?frame=seat&amp;id=$i[id]{$this->LinkPost}\">".$i["name"]."</a>";
					}
					$count ++;
				}
				$ids[] = $k;
				$s["result"][] = array ("name" => $v[0]["nickname"], "places" => implode(", ", $n));
			}
			$s["count"] = $count;
			if ($s["count"] == 0)
				$s["result"] = "";
			$r["found"] = $s;
			$seats_new_template_global = ConfigGet("seats_new_template");
				if ($seats_new_template_global != 'Y'){
				$drawchairs = true;
				}
				else {;}
		} else
			$ids = array ($this->User->id);
						
		if ($drawchairs and !empty ($ids)) {
			$ids = "ids=".urlencode(base64_encode(gzcompress(implode_sqlIn($ids))));
			$r["img"] = "seats.php?frame=image&amp;blockid=".$this->Block["id"]."&amp;$ids{$this->LinkPost}&amp;anticache=".md5($r["edit_time"]);
		} else 
			$r["img"] = EditURL(array ("anticache" => $r["edit_time"]), $r["link"]); 
			
		$r["legend"] = SeatsGetLegend($this->Block["imagedir_block"]); 
		/* 
		array(
		  array("img1" => GetSeatImage($dir,"tablegreen","small",true),       "desc1" => "Ein freier Sitzplatz.",
		        "img2" => GetSeatImage($dir,"tableyellow","small",true),      "desc2" => "Ein vorgemerkter Sitzplatz."),
		  array("img1" => GetSeatImage($dir,"tablegrey","small",true),        "desc1" => "Ein deaktivierter Sitzplatz.",
		        "img2" => GetSeatImage($dir,"tablered","small",true),         "desc2" => "Ein reservierter Sitzplatz."),
		  array("img1" => GetSeatImage($dir,"tablechecked_in","small",true),  "desc1" => "Ein Sitzplatz mit eingechecktem User.",
		        "img2" => GetSeatImage($dir,"tableonline","small",true),      "desc2" => "Ein Sitzplatz mit User, der online ist."),
		  array("img1" => GetSeatImage($dir,"tableoffline","small",true),     "desc1" => "Ein Sitzplatz mit User, der offline ist.",
		        "img2" => GetSeatImage($dir,"tablechecked_out","small",true), "desc2" => "Ein Sitzplatz mit ausgechecktem User."),
		  array("img1" => GetSeatImage($dir,"chair","small",true),            "desc1" => "Diese Markierung kennzeichnet Sitzpl&auml;tze,<br>die du reserviert oder vorgemerkt hast,<br>bzw. die auf deine Suchanfrage zutreffen.")
		);*/
		return $r;
	}

	function framesearch($get) {
		//es wurden UserIDs angegeben
		$this->Caption = "Suchergebnis";
		if(is_array($get["ids"])) {
			foreach($get["ids"] AS $key=>$val)
			$get[$key] = escape_sqlData($val);
			//Anpassung VulkanLAN, Original:
			$seats = MysqlReadArea("SELECT * FROM ".TblPrefix()."flip_seats_seats WHERE reserved='Y' AND user_id IN (".implode_sqlIn($get["ids"]).")");
			//$seats = MysqlReadArea("SELECT * FROM ".TblPrefix()."flip_seats_seats WHERE reserved='Y' OR reserved='N' AND user_id IN (".implode_sqlIn($get["ids"]).")");
			$nicks = MysqlReadCol("SELECT id, name FROM ".TblPrefix()."flip_user_subject WHERE type='user' AND id IN (".implode_sqlIn($get["ids"]).")", "name", "id");
			$blocks = array();
			foreach($seats AS $aSeat) {
				$aSeat["username"] = $nicks[$aSeat["user_id"]];
				$blocks[$aSeat["block_id"]][] = $aSeat;
			}
			if(count($blocks) == 1) {
			    $keys = array_keys($blocks);
			    Redirect("seats.php?frame=block&blockid=".$keys[0]."&ids[]=".join("&ids[]=", $get["ids"]));
			    return false;
			}
			$blocknames = MysqlReadCol("SELECT id, caption FROM ".TblPrefix()."flip_seats_blocks WHERE id IN (".implode_sqlIn(array_keys($blocks)).")", "caption", "id");
			$blockswithids = $blocks;
			$blocks = array();
			foreach($blockswithids AS $key=>$block) {
				$blocks[$blocknames[$key]] = $block;
			}
		}
		return array("blocks"=>$blocks);
	}
	
	function submitsearch($post) {
		
		//Original: $userIDs = MysqlReadArea('SELECT id FROM `'.TblPrefix().'flip_user_subject` WHERE name LIKE \'%'.escape_sqlData_without_quotes($post['search']).'%\'', 'id');
		// VulkanLAN: Anpassung username gro�/klein Schreibung egal:
		$userIDs = MysqlReadArea('SELECT id FROM `'.TblPrefix().'flip_user_subject` WHERE (lower(`name`) LIKE lower(\'%'.escape_sqlData_without_quotes($post['search']).'%\'));', 'id');
		if(!is_array($userIDs)) {
			$userIDs = array();
		}
		Redirect('seats.php?frame=search&ids[]='. implode('&ids[]=', array_keys($userIDs))  );
	}

	function frameSeat($g) {
		global $User;
		ArrayWithKeys($g, array("id")); 
		if(!is_posDigit($g["id"])) {
			trigger_error_text(text_translate("Es muss eine Sitzplatz-ID angegeben werden!"), E_USER_ERROR);
		}
		include_once ("mod/mod.user.php");
		$pers = ($User->id == $this->User->id) ? "dich" : "<b>{$this->User->name}</b> (<i>".UserGetStatus($this->User)."</i>)";

		$r = GetSeat($g["id"]);
		if (ConfigGet($this->ShowIPConfig) != "Y")
			unset ($r["ip"]);
		$maxnotes = ConfigGet($this->MaxNotesConfig);
		$dir = ConfigGet("seats_images_large");
		$note = array ("img" => GetSeatImage($dir, "tableyellow", "normal", true), "buttonclass" => "btn btn-warning btn-block", "icon"=> "fas fa-hand-paper", "msg" => "Du kannst ihn f&uuml;r $pers vormerken (max. $maxnotes Pl&auml;tze).", "action" => "note", "button" => "Vormerken");		
		$reserve = array ("img" => GetSeatImage($dir, "tablered", "normal", true), "buttonclass" => "btn btn-danger btn-block", "icon"=> "fas fa-stop", "msg" => "Du kannst ihn f&uuml;r $pers reservieren.", "action" => "reserve", "button" => "Reservieren");
		$clannote = array ("img" => GetSeatImage($dir, "tableyellow", "normal", true), "buttonclass" => "btn btn-warning btn-block", "icon"=> "fas fa-hand-paper", "msg" => "Clan-Leader: Du kannst den Platz f&uuml;r dich und deine Clan-Mitglieder vormerken.  ", "action" => "clannote", "button" => "Vormerken");		
		$clanreserve = array ("img" => GetSeatImage($dir, "tablered", "normal", true), "buttonclass" => "btn btn-danger btn-block", "icon"=> "fas fa-stop", "msg" => "Clan-Leader: Du kannst den Platz f&uuml;r dich und deine Clan-Mitglieder reservieren.", "action" => "clanreserve", "button" => "Reservieren");
		$clanfree = array ("img" => GetSeatImage($dir, "tablegreen", "normal", true), "buttonclass" => "btn btn-primary btn-block", "icon"=> "fas fa-stop", "msg" => "Clan-Leader: Du kannst diesen Platz freigeben.", "action" => "clanfree", "button" => "Clan-Mitglied freigeben");
		$decontrol = array ("img" => GetSeatImage($dir, "tablegreen", "normal", true), "buttonclass" => "btn btn-success btn-block", "icon"=> "fas fa-asterisk", "msg" => "Du kannst ihn wieder freigeben.", "action" => "decontrol", "button" => "Freigeben");
		$decontrolfromclan = array ("img" => GetSeatImage($dir, "tablegreen", "normal", true), "buttonclass" => "btn btn-primary btn-block", "icon"=> "fas fa-stop", "msg" => "Du kannst ihn wieder freigeben.", "action" => "decontrol", "button" => "Freigeben");
		$decontrolclan = array ("img" => GetSeatImage($dir, "tablegreen", "normal", true), "buttonclass" => "btn btn-success btn-block", "icon"=> "fas fa-asterisk", "msg" => "Clan Leader: Du kannst die Clan Zuordnung aufheben.", "action" => "DecontrolClan", "button" => "Aufheben");
		$decontrolclanadmin = array ("img" => GetSeatImage($dir, "tablegreen", "normal", true), "buttonclass" => "btn btn-success btn-block", "icon"=> "fas fa-asterisk", "msg" => "Admin: Du kannst die Clan Zuordnung aufheben.", "action" => "DecontrolClan", "button" => "Aufheben");
		$admdecontrol = array ("img" => GetSeatImage($dir, "tablegreen", "normal", true), "buttonclass" => "btn btn-success btn-block", "icon"=> "fas fa-asterisk", "msg" => "Admin: Du kannst ihn wieder freigeben.", "action" => "decontrol", "button" => "Freigeben");
		$admdecontrolclan = array ("img" => GetSeatImage($dir, "tablegreen", "normal", true), "buttonclass" => "btn btn-primary btn-block", "icon"=> "fas fa-stop", "msg" => "Admin: Du kannst ihn wieder freigeben.", "action" => "decontrol", "button" => "Freigeben");
		$enable = array ("img" => GetSeatImage($dir, "tablegreen", "normal", true), "buttonclass" => "btn btn-success btn-block", "icon"=> "fas fa-asterisk", "msg" => "Admin: Du kannst ihn aktivieren.", "action" => "enable", "button" => "Aktivieren");
		$disable = array ("img" => GetSeatImage($dir, "tablegrey", "normal", true), "buttonclass" => "btn btn-block", "buttonstyle"=> "background-color:#aaa", "icon"=> "fas fa-times", "iconstyle"=> "color:black", "msg" => "Admin: Du kannst ihn deaktivieren.<br/> Achtung: eine eventuelle Vormerkung/Reservierung<br/> wird damit aufgehoben.", "action" => "disable", "button" => "Deaktivieren");
				
		// Clan Verwaltung
	  $clanSeatReservation = 0;
	  $clanSeatClearReservation = 0;
	  $reserved_for_clan = NULL;
	  $clan_selection = array();
	  $clan_member_registered = array();
	  $clan_member_paid = array();
	  $clan = NULL;
	  $clanadm = 0;
	  
		// Sitzplatz f�r Clan bereits vorgemerkt?
		if ($r["user_id_clan"] != 0) {
		  $reserved_for_clan = CreateSubjectInstance($r["user_id_clan"], "clan");
		  //print ("Platz f&uuml;r Clan \"" . $reserved_for_clan->name . "\" reserviert");		  
		}
		 
	  // Admin bekommt alle Clans zur Auswahl
	  if ($User->hasRight($this->AdminUserRight)) {
	  	$clans = GetSubjects("clan");
	  	foreach($clans AS $k=>$clan) {
	  		$clan_selection += array ($clans[$k]["id"]=>$clans[$k]["name"]);
	  	}

	  	// Admin kann Clan Reservierung durchf�hren	  	
	  	$clanSeatReservation = 1;
	  	
	  	// Admin kann auch Clan Admin Aktionen durchf�hren (als Admin: angef�hrt)
	    $clanadm = 1;
	    $clannote["msg"] = "Admin: Du kannst diesen Platz f&uuml;r Mitglieder des Clan <b>$reserved_for_clan->name</b> vormerken.";
			$clanreserve["msg"] = "Admin: Du kannst diesen Platz f&uuml;r Mitglieder des Clan <b>$reserved_for_clan->name</b> reservieren.";
	  	
	  	// Admin bekommt auch das Recht JEDE Clan Reservierung zu l�schen
	  	if ($reserved_for_clan != NULL) {
	  	  $clanSeatClearReservation = 1;
	  	  
	  	  // Freigabe-Button aendern (Farbe) fuer Admin Funktion
	  	  $decontrolclan = $decontrolclanadmin;
	  	  $admdecontrol = $admdecontrolclan;
	  	}
	  }
	  
	  // Clan Leader bekommt seine Clans zur Auswahl
	  else
	  { 
  	  $ownClans = $User->getParents("clan");
      $keys = array_keys($ownClans);
      
      // Clan Leader bekommt seine Clans zur Auswahl
      if (count($keys) > 0) {
        for($i=0; $i < count($keys); ++$i) 
        {
        	$clanId = $keys[$i]; 
          if ($clanId != 0) 
          {
            $clan = CreateSubjectInstance($clanId, "clan");
            $leaders = $clan->getEntitled($this->leaderRight);            
            $keys_leader = array_keys($leaders);
            
            for($j=0; $j < count($keys_leader); ++$j)
            {                     
              $leader = CreateSubjectInstance($keys_leader[$j], "user");
              if ($keys_leader[$j] == $User->id) 
              { 
                $clan_selection += array ($clan->id=>$clan->name);
                
                // Recht zur Clan Reservierung
                $clanSeatReservation = 1;
                
                // Clan Leader bekommt auch das Recht seine Clan-Reservierung freizugeben
                if ($reserved_for_clan != NULL)
                {
                	// Nur Rechte f�r SEINE Clans
                	if ($reserved_for_clan->id == $clanId) {
                		$clanSeatClearReservation = 1;
                		$clanadm = 1;
                	}
                }
              } 
            }
          }
        }
      }
      
      // Normaler User kann keine Clan-Reservierungen machen
      else {
        $clanSeatReservation = 0;
      }
    }
    
    // Clan-Auswahlliste setzen
		$r["clanseat"] = $clan_selection;	
    
    // Vormerken und reservieren generell fuer alle Plaetze
    $not = $this->User->hasRight($this->NoteSeatRight);
    $reg = $this->User->hasRight($this->ReserveSeatRight);    
    
    // Clan Mitglied kann Sitzplaetze seines Clans vormerken und reservieren, 
    if ($reserved_for_clan != NULL) {     
    	$clanLink = "<b><a href=\"clan.php?frame=viewclan&amp;id=$reserved_for_clan->id\">".escapeHtml($reserved_for_clan->name)."</a></b>";
    	// Clan Mitglied muss jedoch angemeldet sein
    	if ($not) {
  	    $decontrol = $decontrolfromclan;
	      $user_in_clan = $this->userInClan($this->User, $reserved_for_clan->id); 
      
        // Vormerken und Reservieren f�r Nicht-Clan-Mitglieder etnfaellt
        if (!$user_in_clan) {
        	$not = 0;
        	$reg = 0;
        }
      
        // Clan Mitglied bekommt das Recht Plaetze zu reservieren, wenn der Clan fuer ihn bezahlt hat.
        if (($user_in_clan) && ($reserved_for_clan->hasRight($this->StatusPaidClan))) {        
          $reg = 1;
        }
      }
      // Ohne Anmeldung, darf ein Clan Leader auch keine Clan Plaetze verwalten
      else {
      	$clanSeatReservation = 0;
      	$clanSeatClearReservation = 0;
      }
    }
    
    // Admin Reservierung f�r jemand anderen    
    if ($User->id == $this->User->id)
    {
      // Clan Leader koennen Plaetze f�r andere Mitglieder selbst verwalten
      if (($clanadm) && ($reserved_for_clan != NULL))
      {
      	$memberIDs = array_keys($reserved_for_clan->getChilds());
      	
      	// Zuerst sich selbst - den Clan Leader - hinzuf�gen ...
      	for($i=0; $i < count($memberIDs); ++$i) {
      	  $member = CreateSubjectInstance($memberIDs[$i], "user");
      	  if ($member->hasRight($this->NoteSeatRight))
      	  {
      	  	if ($member->id == $User->id) $clan_member_registered += array ($memberIDs[$i]=>$member->name);
      	    if (($member->hasRight($this->ReserveSeatRight)) || ($reserved_for_clan->hasRight($this->StatusPaidClan)))
      	    {
      	    	if ($member->id == $User->id) $clan_member_paid += array ($memberIDs[$i]=>$member->name);
      	    }
      	  }    	  
				}								
      	
      	// Falls nicht Admin und Clan Leader nicht angemeldet ist => Clan Admin Funktionen sperren
				if ((!$User->hasRight($this->AdminUserRight)) && (count($clan_member_registered)) <= 0) $clanadm = 0;
      	
      	// ... danach �brige Mitglieder hinzuf�gen
      	for($i=0; $i < count($memberIDs); ++$i) {
      	  $member = CreateSubjectInstance($memberIDs[$i], "user");
      	  if ($member->hasRight($this->NoteSeatRight))
      	  {
      	  	if ($member->id != $User->id) $clan_member_registered += array ($memberIDs[$i]=>$member->name);
      	    if (($member->hasRight($this->ReserveSeatRight)) || ($reserved_for_clan->hasRight($this->StatusPaidClan)))
      	    {
      	    	if ($member->id != $User->id) $clan_member_paid += array ($memberIDs[$i]=>$member->name);
      	    }
      	  }
				}
			}
			else ;
    }
    
    // Admin reserviert f�r jemand Bestimmten
    else
    {
			$clanadm = 1;
    	if ($not) $clan_member_registered += array ($this->User->id=>$this->User->name);
			if ($reg) $clan_member_paid += array ($this->User->id=>$this->User->name);    	
			$clannote["msg"] = "Admin: Du kannst diesen Platz f&uuml;r $pers vormerken.";
			$clanreserve["msg"] = "Admin: Du kannst diesen Platz f&uuml;r $pers reservieren.";
    }
    
    if ($clanadm) {
    	$r["clannote"] = $clan_member_registered;
  		$r["clanreserve"] = $clan_member_paid;
    }
						
		$adm = $User->hasRight($this->AdminUserRight); 	
		$own = ($User->id == $r["user_id"]) ? true : false;
		$col = (!empty ($r["user_status"]) and ($r["reserved"] == "Y")) ? GetValidSeatStatus($r["user_status"]) : GetSeatColor($r);

		$a = array ();
		$nick = "<b><a href=\"user.php?frame=viewsubject&amp;id=$r[user_id]\">".escapeHtml($r["nickname"])."</a></b>";		
		
		// Anzahl der Clan Plaetze ermitteln
		if ($reserved_for_clan != NULL) {
    	if (($adm) or ($clanSeatReservation)) {
        $clanseatcount = MysqlReadField("SELECT COUNT(*) AS c FROM ".TblPrefix()."flip_seats_seats WHERE (`user_id_clan` = $reserved_for_clan->id);", "c");
    	}
    }  

    $clanAdminControl = false;
    if ($clanadm) 
      if (($reserved_for_clan != NULL) || ($User->id != $this->User->id)) $clanAdminControl = true;

		$r["stat"] = "unbekannt";
		$allowExchange = false;
		switch ($col) {
			case ("green") :
			  if ($reserved_for_clan == NULL) $r["stat"] = "Dieser Sitzplatz ist noch frei.";
                            		   else $r["stat"] = "Dieser Sitzplatz geh&ouml;rt zum Clan $clanLink.";		  			  			  	
	  		if ($not)
  			  if (!$clanAdminControl) $a[] = $note;
				if ($reg)
				  if (!$clanAdminControl) $a[] = $reserve;
				if ($clanadm)
				{
					if (count($clan_member_registered) > 0) $a[] = $clannote;
					if (count($clan_member_paid) > 0) $a[] = $clanreserve;					
				}	
				if ($adm)
					$a[] = $disable;
				if ($adm)
					$allowExchange = true;
				if ($clanSeatReservation and $clanSeatClearReservation) {
					$a[] = $decontrolclan;
					if ($reserved_for_clan != NULL) $r["stat"] .= " (" .  $clanseatcount . " von " . $this->getClanSeatCount($reserved_for_clan->id) . " Pl&auml;tzen bereits reserviert)";
				}
				break;
			case ("yellow") :
				if ($reserved_for_clan == NULL) $r["stat"] = "Dieser Sitzplatz wurde f&uuml;r $nick <b>vorgemerkt</b>. Das Vormerken dient nur zur Orientierung, der Platz kann trotzdem von jedem reserviert werden.";
				                           else $r["stat"] = "Dieser Sitzplatz wurde f&uuml;r $nick <b>vorgemerkt</b> vom Clan $clanLink. Das Vormerken dient nur zur Orientierung, der Platz kann trotzdem von jedem Clan-Mitglied reserviert werden.";
				if ($reg)
					if (!$clanAdminControl) $a[] = $reserve;
				if ($not and !$own)
					if (!$clanAdminControl) $a[] = $note;
				if ($clanadm)
				{	
					if (count($clan_member_registered) > 0) $a[] = $clannote;
					if (count($clan_member_paid) > 0) $a[] = $clanreserve;
					if (!$adm) $a[] = $clanfree;
				}
				if ($adm and (!$own or $reserved_for_clan))
					$a[] = $admdecontrol;
				if ($own)
					if (!$clanAdminControl) $a[] = $decontrol;			
				if ($adm)
					$a[] = $disable;
				if ($adm)
					$allowExchange = true;
				if ($clanSeatReservation && $clanSeatClearReservation) {
					$a[] = $decontrolclan;
					if ($reserved_for_clan != NULL) $r["stat"] .= " (" .  $clanseatcount . " von " . $this->getClanSeatCount($reserved_for_clan->id) . " Pl&auml;tzen bereits reserviert)";
				}
				break;
			case ("red") :
				if ($reserved_for_clan == NULL) $r["stat"] = "Dieser Sitzplatz wurde von $nick <b>reserviert</b>.";
												else $r["stat"] = "Dieser Sitzplatz wurde von $nick <b>reserviert</b> vom Clan $clanLink.";				
				if ($reg and $own)
					$a[] = $note;
				if ($own)
					if (!$clanAdminControl) $a[] = $decontrol;
				if ($clanadm)
				{	
					if (!$adm) $a[] = $clanfree;
				}
				if ($adm and (!$own or $reserved_for_clan))
					$a[] = $admdecontrol;
				if ($adm)
					$a[] = $disable;
				if ($adm)
					$allowExchange = true;
				if ($clanSeatReservation and $clanSeatClearReservation) {
					$a[] = $decontrolclan;
					if ($reserved_for_clan != NULL) $r["stat"] .= " (" .  $clanseatcount . " von " . $this->getClanSeatCount($reserved_for_clan->id) . " Pl&auml;tzen bereits reserviert)";
				}
				break;
			case ("grey") :
				$r["stat"] = "Dieser Sitzplatz ist <b>deaktiviert</b>. Das bedeutet man kann ihn (noch) nicht reservieren.";
				if ($adm)
					$a[] = $enable;
				break;
			case ("checked_in") :
				if ($reserved_for_clan == NULL) $r["stat"] = "Auf diesem Platz sitzt $nick. Er/Sie hat gerade <b>eingecheckt</b> und war noch nicht online.";
				                           else $r["stat"] = "Auf diesem Platz sitzt $nick vom Clan $clanLink. Er/Sie hat gerade <b>eingecheckt</b> und war noch nicht online.";
				if ($adm)
					$a[] = $admdecontrol;
				if ($adm)
					$a[] = $disable;
				if ($adm)
					$allowExchange = true;
				break;
			case ("online") :
				if ($reserved_for_clan == NULL) $r["stat"] = "Auf diesem Platz sitzt $nick. Er/Sie ist <b>online</b>.";
				                           else $r["stat"] = "Auf diesem Platz sitzt $nick vom Clan $clanLink. Er/Sie ist <b>online</b>.";
				if ($adm)
					$a[] = $admdecontrol;
				if ($adm)
					$a[] = $disable;
				if ($adm)
					$allowExchange = true;
				break;
			case ("offline") :
				if ($reserved_for_clan == NULL) $r["stat"] = "Auf diesem Platz sitzt $nick. Er/Sie ist <b>offline</b>.";
				                           else $r["stat"] = "Auf diesem Platz sitzt $nick vom Clan $clanLink. Er/Sie ist <b>offline</b>.";
				if ($adm)
					$a[] = $admdecontrol;
				if ($adm)
					$a[] = $disable;
				if ($adm)
					$allowExchange = true;
				break;
			case ("checked_out") :
				if ($reserved_for_clan == NULL) $r["stat"] = "Auf diesem Platz sa&szlig; $nick. Er/Sie hat bereits <b>ausgecheckt</b>, d.h. die Party verlassen.";
				                           else $r["stat"] = "Auf diesem Platz sa&szlig; $nick vom Clan $clanLink. Er/Sie hat bereits <b>ausgecheckt</b>, d.h. die Party verlassen.";
				if ($adm)
					$a[] = $admdecontrol;
				if ($adm)
					$a[] = $disable;
				if ($adm)
					$allowExchange = true;
				break;
		}
		$r["actions"] = $a;
		if ($allowExchange)
			$r["exchange"] = $this->getExchangeHistory($r["id"]);	

		$r += GetSeatImage($dir, "table$col", "large");
		$this->Caption = "Sitzplatz ausgew&auml;hlt";
		$r["linkpost"] = $this->LinkPost;
		
		return $r;
	}

	function addToExchangeHistory($SID) {
		global $Session;
		$a = isset($Session->Data["seats-exchange"]) ? $Session->Data["seats-exchange"] : null;
		if (!is_array($a))
			$a = array ();
		array_unshift($a, $SID);
		$a = array_unique($a);
		if (count($a) > 10)
			array_pop($a);
		$Session->Data["seats-exchange"] = $a;
	}

	function getExchangeHistory($current) {
		global $Session;
		$this->addToExchangeHistory($current);
		$sids = isset($Session->Data["seats-exchange"]) ? $Session->Data["seats-exchange"] : null;
		$sids = implode_sqlIn(array_diff($sids, array ($current)));
		if (empty ($sids))
			return array ();
		return MysqlReadCol("SELECT s.id, CONCAT(b.caption,':',s.name,' (',IFNULL(u.name,''),')') AS `name` 
			FROM (".TblPrefix()."flip_seats_seats s) 
			LEFT JOIN ".TblPrefix()."flip_user_subject u ON (u.id=s.user_id) 
			LEFT JOIN ".TblPrefix()."flip_seats_blocks b ON (b.id=s.block_id) 
			WHERE (s.id IN ($sids));", "name", "id");
	}

	function submitSeatExchange($dat) {
		ArrayWithKeys($dat, array("exchange","id"));
		$seat1 = GetSeat($dat["exchange"]);
		$seat2 = GetSeat($dat["id"]);
		$s1 = array ("user_id" => $seat2["user_id"], "reserved" => $seat2["reserved"], "user_status" => $seat2["user_status"], "user_id_clan" => $seat2["user_id_clan"], "enabled" => $seat2["enabled"]);
		$s2 = array ("user_id" => $seat1["user_id"], "reserved" => $seat1["reserved"], "user_status" => $seat1["user_status"], "user_id_clan" => $seat1["user_id_clan"], "enabled" => $seat1["enabled"]);
		if (!MysqlWriteByID(TblPrefix()."flip_seats_seats", $s1, $seat1["id"]))
			return false;
		if (!MysqlWriteByID(TblPrefix()."flip_seats_seats", $s2, $seat2["id"]))
			return false;
		$this->redrawImage($s1 + $seat1);
		$this->redrawImage($s2 + $seat2);
	}
	
	function submitClanSeat($dat) {
		global $User;
		
		if (!$User->hasRight($this->AdminUserRight))
			$this->User->requireRight($this->NoteSeatRight, "Du hast noch nicht bezahlt und kannst noch keinen Sitzplatz vormerken.");
		
		$r = GetSeat ($dat["id"]);
		if ($r["enabled"] == "N")
			trigger_error_text("Dieser Sitzplatz ist deaktiviert und kann nicht vorgemerkt werden.", E_USER_ERROR);
		if (($r["user_id"] != $this->User->id) and ($r["reserved"] == "Y"))
			trigger_error_text("Dieser Sitzplatz wurde bereits von \"".$r["nickname"]."\" reserviert.", E_USER_ERROR);
			
		// Clan-ID einfuegen
		$r["user_id_clan"] = $dat["clanseat"];
		
		if ($this->setClanSeat($r, true, false)) {
			trigger_error_text("Es wurde erfolgreich ein Sitzplatz f&uuml;r den Clan vorgemerkt.");
			LogAction("f&uuml;r ".escapeHtml($this->User->name)."[{$this->User->id}] wurde der Platz $r[name][$r[id]] vorgemerkt");
		} else
			trigger_error_text("Der Sitzplatz konnte nicht f&uuml;r den Clan vorgemerkt werden.", E_USER_ERROR);
	}

	function redrawImage($row) {
		include_once ("mod/mod.imageedit.php");
		$tmpplan = new ResImage(SeatsGetBackgroundTmpLoader($row["block_id"]));
		$over = new ResImage(SeatsGetOverviewImageTmpLoader());
		DrawSeat($row, $tmpplan, $over);
		SaveSeats($tmpplan, $row["block_id"]);
		
		$seats_new_template_global = ConfigGet("seats_new_template");
		if ($seats_new_template_global != 'Y') SeatsSaveOverviewTmp($over);
		//else SeatsSaveOverviewTmp($over);
		return true;
	}
	
		
	function getClanSeatCount ($clan_id) {
		include_once ("mod/mod.user.php");
		global $User;
  	$clan_seat_limit_global = ConfigGet("clan_seat_limit");
		$clan = CreateSubjectInstance($clan_id, "clan");
		$clan_seat_limit = $clan_seat_limit_global;
			
		$rawProps = $clan->getProperties();
		foreach($rawProps AS $key=>$val) {
			if ($key == "clan_seat_limit") {
			  $clan_seat_limit = $val;
			  break;
			}
		}
		return $clan_seat_limit;
	}
	
	function setClanSeat($row, $setclan, $reserve, $enable = true) {
		include_once ("mod/mod.user.php");
		global $User;				
				 
		if ($row["reserved"] == "Y") {
			trigger_error_text("Die Clan Zuordnung von reservierten Plaetzen kann nicht ge&auml;ndert werden.", E_USER_ERROR);								
		}				
		
		// Clan Reservierung setzen bzw. loeschen		
		if ((!($setclan)) and ((!$reserve))) { 
			$reserved_clan_id = null; 	
		}
		else $reserved_clan_id = $row["user_id_clan"];		
				
		$dat = array ("reserved" => "N", "user_id" => 0, "user_status" => "", "enabled" => ($enable) ? "Y" : "N", "user_id_clan" => $reserved_clan_id);

		if ($setclan) {
			$c = MysqlReadField("SELECT COUNT(*) AS c FROM ".TblPrefix()."flip_seats_seats WHERE (`user_id_clan` = $reserved_clan_id);", "c");
			
			$clan = CreateSubjectInstance($reserved_clan_id, "clan");
			$clan_seat_limit = $this->getClanSeatCount($reserved_clan_id);

		  if ($c >= $clan_seat_limit) {
				trigger_error_text("Du hast f&uuml;r deinen Clan \"" . $clan->name . "\" bereits die maximale Anzahl von " . $clan_seat_limit . " Pl&auml;tzen reserviert! Du musst erst wieder welche freigeben, bevor du andere reservieren kannst.", E_USER_ERROR);
			} 
		}
		
		if (MysqlWriteByID(TblPrefix()."flip_seats_seats", $dat, $row["id"]))
			return $this->redrawImage($dat + $row);
		return false;
	}

	function setSeat($row, $User, $setuser, $reserve, $enable = true) {
		include_once ("mod/mod.user.php");
		//global $User;
		
		// Clan Reservierung uebertragen, damit diese nicht verloren geht.
		$reserved_clan_id = $row["user_id_clan"];						
		$dat = array ("reserved" => ($reserve and $enable) ? "Y" : "N", "user_id" => ($setuser and $enable) ? $User->id : 0, "user_status" => ($setuser and $enable and $reserve) ? "" : UserGetStatus($User), "enabled" => ($enable) ? "Y" : "N", "user_id_clan" => $reserved_clan_id);

		if ($setuser) {
			$c = MysqlReadField("SELECT COUNT(*) AS c FROM ".TblPrefix()."flip_seats_seats WHERE ((`user_id` = $dat[user_id]) AND (`reserved` = '$dat[reserved]'));", "c");
			if ($reserve) {
				if ($c > 0)
					trigger_error_text("Du hast bereits einen Sitzplatz f&uuml;r dich reserviert, diesen musst du zuerst wieder freigeben bevor du einen anderen reservieren kannst.", E_USER_ERROR);
			} else {
				$maxnotes = ConfigGet($this->MaxNotesConfig);
				if ($c >= $maxnotes)
					trigger_error_text("Du hast bereits $c Sitzpl&auml;tze von $maxnotes m&ouml;glichen f&uuml;r dich vorgemerkt. Du musst erst wieder welche freigeben, bevor du andere vormerken kannst.", E_USER_ERROR);
			}
		}
		if (MysqlWriteByID(TblPrefix()."flip_seats_seats", $dat, $row["id"]))
		{
			$this->setSeatPostprocessing($row, GetSeat($row["id"]), $User);
			return $this->redrawImage($dat + $row);
		}
		return false;
	}
	
	function setSeatPostprocessing($old, $new, $User) {	
		include_once ("mod/mod.user.php");
		//global $User;
		
		$reserved = (($old["reserved"] == "N") and ($new["reserved"] == "Y"));
		$dereserved = (($old["reserved"] == "Y") and ($new["reserved"] == "N"));
		
		if (($reserved) or ($dereserved)) {					
  		
  		// Check fuer indirekt bezahlte Clan Plaetze
  		$clanId = $old["user_id_clan"];
  		if ($reserved) $userID = $new["user_id"];
  		if ($dereserved) $userID = $old["user_id"];  		
  		if (($old["user_id_clan"] == "0") || ($old["user_id_clan"] == NULL)) $clanId = NULL;
  		
  		if (($clanId != NULL) and ($userID > 0))
  		  $clan = CreateSubjectInstance($clanId, "clan");  		  
  		  $SeatUser = CreateSubjectInstance($userID, "user");  		  
  		  if (($clan != NULL) and ($SeatUser != NULL))
  			  if ($clan->hasRight($this->StatusPaidClan))
  			    if (!$SeatUser->hasRight($this->ReserveSeatRight)) {
  			    
  			      $groups = MysqlReadArea('SELECT * FROM `'.TblPrefix().'flip_user_subject` WHERE `type` LIKE \'group\';');  			      
  			      foreach ($groups as $group) {
  			      	if ($group["name"] == "status_paid_clan") {
  			      		$g = CreateSubjectInstance($group["id"], "group");
  			      		
  			      		if ($reserved) $g->addChild($SeatUser);
  			      		if ($dereserved) $g->remChild($SeatUser);
  			      	}
  			      }
  			    }
    }		
		return true;
	}
	
	function isLeaderOfClanSeat($p) {
	  global $User;
	  
	  $seat = GetSeat($p["id"]);
	  $clanId = $seat["user_id_clan"];
	  if (($clanId != 0) && ($clanId != NULL)) {
	  	$clan = CreateSubjectInstance($clanId, "clan");  	
	  	$leaders = $clan->getEntitled($this->leaderRight); 
	  	$keys_leader = array_keys($leaders);
      for($i=0; $i < count($keys_leader); ++$i) {
      	if ($User->id == $keys_leader[$i]) return true;
      }
	  }  
    return false;
  }
	
	function userInClan($User, $clan) {		
		//global $User;
		$user_in_clan = false;
    $ownClans = $User->getParents("clan");			  
    $keys = array_keys($ownClans);              
    if (count($keys) > 0) {       
      for($i=0; $i < count($keys); ++$i) 
      {
    	  if ($clan == $keys[$i]) {
     	    $user_in_clan = true; 
     	    break;
    	  }
    	}
    }
    return $user_in_clan;
  }

  function submitClanNote($p) {
  	global $User;
  	if (!$User->hasRight($this->AdminUserRight))
  	  if (!$this->isLeaderOfClanSeat($p))
        trigger_error_text("Du bist nicht berechtigt Sitzpl&auml;tze des Clans zu verwalten!", E_USER_ERROR);
  	
  	$userID = $p["clannote"];
  	$SeatUser = CreateSubjectInstance($userID, "user");  
  	$this->actionNoteUser($p, $SeatUser);
  }
  
  function actionNote($p) {
  	global $User;
  	$this->actionNoteUser($p, $User); 
  }

	function actionNoteUser($p, $User) {
		//global $User;
		
		if (!$User->hasRight($this->AdminUserRight))
			$this->User->requireRight($this->NoteSeatRight, "Du hast noch nicht bezahlt und kannst noch keinen Sitzplatz vormerken.");
		$r = GetSeat($p["id"]);
		if ($r["enabled"] == "N")
			trigger_error_text("Dieser Sitzplatz ist deaktiviert und kann nicht vorgemerkt werden.", E_USER_ERROR);
		if (($r["user_id"] != $this->User->id) and ($r["reserved"] == "Y"))
			trigger_error_text("Dieser Sitzplatz wurde bereits von \"".$r["nickname"]."\" reserviert.", E_USER_ERROR);
			
		// User geh�rt nicht zu Clan und ist kein Admin?
		if (!$User->hasRight($this->AdminUserRight)) {
  	  $reserved_clan_id = $r["user_id_clan"];
      if ($reserved_clan_id != 0) {
        $user_in_clan = $this->userInClan($User, $reserved_clan_id);        
        if (!$user_in_clan) 
         	trigger_error_text("Dieser Sitzplatz ist bereits f&uuml;r einen anderen Clan reserviert.", E_USER_ERROR);
      }
    }
    
		if ($this->setSeat($r, $User, true, false)) {
			trigger_error_text("Es wurde erfolgreich ein Sitzplatz f&uuml;r dich vorgemerkt.");
			LogAction("f&uuml;r ".escapeHtml($this->User->name)."[{$this->User->id}] wurde der Platz $r[name][$r[id]] vorgemerkt");
		} else
			trigger_error_text("Der Sitzplatz konnte nicht f&uuml;r dich vorgemerkt werden.", E_USER_ERROR);
	}
	
	function submitClanReserve($p) {
		global $User;
		if (!$User->hasRight($this->AdminUserRight))
		  if (!$this->isLeaderOfClanSeat($p))
        trigger_error_text("Du bist nicht berechtigt Sitzpl&auml;tze des Clans zu verwalten!", E_USER_ERROR);
      
  	$userID = $p["clanreserve"];
  	$SeatUser = CreateSubjectInstance($userID, "user");  
  	$this->actionReserveUser($p, $SeatUser);
  }
  
  function actionReserve($p) {
  	global $User;
  	$this->actionReserveUser($p, $User); 
  }

	function actionReserveUser($p, $User) {
		//global $User;
				
		$r = GetSeat($p["id"]);		
				
		if (!$User->hasRight($this->AdminUserRight)) {			
			if (($r["user_id_clan"] != 0) and ($r["user_id_clan"] != NULL)) {				
			  $reserved_for_clan = CreateSubjectInstance($r["user_id_clan"], "clan");			
		    if ($reserved_for_clan != NULL) {
          if ($this->userInClan($User, $reserved_for_clan->id)) {
        	  // Clan Mitglied bekommt das Recht Plaetze zu reservieren, wenn der Clan fuer ihn bezahlt hat.
        	  if ($reserved_for_clan->hasRight($this->StatusPaidClan))
        	  {
        	  	if ($User->hasRight($this->NoteSeatRight)) {
        	  		; // print "Clan bezahlt" . $reserved_for_clan->name;        	  		
        	  	}
        	  	else trigger_error_text("Dein Clan h&auml;tte zwar f&uuml;r den Sitzplatz bezahlt, du bist jedoch noch nicht angemeldet!", E_USER_ERROR);
        	  }
        	  else $User->requireRight($this->ReserveSeatRight, "Du kannst den Siztplatz nicht reservieren, da weder du noch dein Clan daf&uuml;r bezahlt hat.");
        	}
        	else $User->requireRight($this->ReserveSeatRight, "Du kannst den Siztplatz nicht reservieren, da du nicht Mitglied des Clans \"" . $reserved_for_clan->name. "\" bist.");
    	  }
			  else $User->requireRight($this->ReserveSeatRight, "Du hast noch nicht bezahlt und kannst noch keinen Siztplatz reservieren.");
			}
		}
		
		if ($r["enabled"] == "N")
			trigger_error_text("Dieser Sitzplatz ist deaktiviert und kann nicht reserviert werden.", E_USER_ERROR);
		if ($r["reserved"] == "Y")
			trigger_error_text("Dieser Sitzplatz wurde bereits von \"$r[nickname]\" reserviert.", E_USER_ERROR);
		
		// User geh�rt nicht zu Clan und ist kein Admin?
		if (!$User->hasRight($this->AdminUserRight)) {
  	  $reserved_clan_id = $r["user_id_clan"];
      if ($reserved_clan_id != 0) {
        $user_in_clan = $this->userInClan($User, $reserved_clan_id);       
        if (!$user_in_clan) 
         	trigger_error_text("Dieser Sitzplatz ist bereits f&uuml;r einen anderen Clan reserviert.", E_USER_ERROR);
      }
    }  			

		if ($this->setSeat($r, $User, true, true)) {
			trigger_error_text("Es wurde erfolgreich ein Sitzplatz f&uuml;r dich reserviert.");
			LogAction("f&uuml;r {$this->User->name}[{$this->User->id}] wurde der Platz $r[name][$r[id]] reserviert");
		} else
			trigger_error_text("Der Sitzplatz konnte nicht f&uuml;r dich reserviert werden.", E_USER_ERROR);
	}
	
	function actionClanFree($p) {	  
	  if (!$this->isLeaderOfClanSeat($p))
      trigger_error_text("Du bist nicht berechtigt Sitzpl&auml;tze des Clans zu verwalten!", E_USER_ERROR);
      
	  $this->actionDecontrol($p);
  }

	function actionDecontrol($p) {
		global $User;
		$r = GetSeat($p["id"]);
		if ($r["enabled"] == "N")
			trigger_error_text("Dieser Sitzplatz ist deaktiviert und kann nicht reserviert werden.", E_USER_ERROR);
		if (($r["user_id"] != $this->User->id) and (!$User->hasRight($this->AdminUserRight)) and (!$this->isLeaderOfClanSeat($p)))
			trigger_error_text("Ein Sitzplatz kann nur von demjenigen freigegeben werden, der ihn reserviert/vorgemerkt hat bzw. Administrationsrechte besitzt.", E_USER_ERROR);
		if ($this->setSeat($r, $User, false, false)) {
			trigger_error_text("Der Sitzplatz wurde wieder freigegeben.");
			LogAction("der Platz $r[name][$r[id]] von {$this->User->name}[{$this->User->id}] wurde freigegeben");
			return 1;
		} else
			trigger_error_text("Der Sitzplatz konnte nicht freigegeben werden.", E_USER_ERROR);
	}
	
	function actionDecontrolClan($p) {
		global $User;
		$r = GetSeat($p["id"]);
		$reserved_clan_id = $r["user_id_clan"];
		$clear_reserved_seat = 0;
		if ($r["enabled"] == "N")
			trigger_error_text("Dieser Sitzplatz ist deaktiviert und kann nicht reserviert werden.", E_USER_ERROR);
		if ((!$User->hasRightOver($this->leaderRight, $reserved_clan_id)) and (!$User->hasRight($this->AdminUserRight)))		
			trigger_error_text("Ein Sitzplatz kann nur von demjenigen freigegeben werden, der Clan Leader ist bzw. Administrationsrechte besitzt.", E_USER_ERROR);
		
		if ($r["reserved"] == "Y") {
      if ($User->hasRight($this->AdminUserRight)) {
        $clear_reserved_seat = 1;
      }
			else trigger_error_text("Die Clan Zuordnung kann bei einem reservierten Sitzplatz nicht aufgehoben werden.", E_USER_ERROR);
		}
		
		if ($clear_reserved_seat == 1)
		  if (!$this->actionDecontrol($p)) {
			  trigger_error_text("Der Sitzplatz konnte nicht freigegeben werden, wodurch die Clan Zuordnung ebenfalls nicht aufgehoben werden kann.", E_USER_ERROR);
		  }
		  else $r = GetSeat($p["id"]);
		
		if ($this->setClanSeat($r, false, false)) {
			trigger_error_text("Die Clan Zuordnung des Sitzplatzes wurde aufgehoben.");
			LogAction("die Clan Zuordnung f&uuml;r Platz $r[name][$r[id]] wurde aufgehoben");
		} else
			trigger_error_text("Die Clan Zuordnung des Sitzplatzes konnte nicht aufgehoben werden.", E_USER_ERROR);
	}

	function actionEnable($p) {
		global $User;
		$r = GetSeat($p["id"]);
		$User->requireRight($this->AdminUserRight);
		if ($this->setSeat($r, $User, false, false, true)) {
			trigger_error_text("Der Sitzplatz wurde aktiviert.");
			LogAction("der Platz $r[name][$r[id]] wurde aktiviert");
		} else
			trigger_error_text("Der Sitzplatz konnte nicht aktiviert werden.", E_USER_ERROR);
	}

	function actionDisable($p) {
		global $User;
		$r = GetSeat($p["id"]);
		$User->requireRight($this->AdminUserRight);
		if ($this->setSeat($r, $User, false, false, false)) {
			trigger_error_text("Der Sitzplatz wurde deaktiviert.");			
			$r = GetSeat($p["id"]);
					
			// Clan Zuordnung aufheben wenn besteht
  		if ($r["user_id_clan"] != NULL) 
	  	  if ($this->setClanSeat($r, false, false, false)) {
		  	  trigger_error_text("Die Clan Zuordnung des Sitzplatzes wurde wieder aufgehoben.");
			  }
			  else
			    trigger_error_text("Die Clan Zuordnung des Sitzplatzes konnte nicht aufgehoben werden.", E_USER_ERROR);
			LogAction("der Platz $r[name][$r[id]] wurde deaktiviert");
		} else
			trigger_error_text("Der Sitzplatz konnte nicht deaktiviert werden.", E_USER_ERROR);		
	}

	function frameImage($g) {
		global $User;
		include_once ('mod/mod.imageedit.php');
		$plan = new ResImage(SeatsGetBackgroundTmpLoader($this->Block['id']));

		$ids = array ();
		if (!is_array($g['ids']))
			$g['ids'] = explode(',', gzuncompress(base64_decode($g['ids'])));

		foreach ($g['ids'] as $v)
			$ids[] = intval(str_replace('\'','',$v));
		
		$inf = GetSeatImage($this->Block['imagedir_block'], 'default', 'small', true);
		$th = $inf['height'];
		$block_id = escape_sqlData_without_quotes($this->Block['id']);
		$seats = MysqlReadArea('SELECT `center_x`, `center_y`, `angle` FROM `'.TblPrefix().'flip_seats_seats` WHERE `block_id` = \''.$block_id.'\' AND (user_id IN ('.implode_sqlIn($ids).'));');
		if(!empty($seats) && is_array($seats)) {
	 		foreach ($seats as $s) {
				$img = LoadSeatImage($this->Block['imagedir_block'], 'chair', $s['angle']);
				$h = $img->getHeight();
				$a = $s['angle'] / 180 * pi();
				$dist = (($th + $h) / 2);
				$x = round(sin($a) * $dist) + $s['center_x'];
				$y = round(cos($a) * $dist) + $s['center_y'];
				$plan->drawRotated($img, $x, $y, $s['angle']);

			}
		}
		$plan->printData();
		return true;
	}

}

RunPage("SeatsPage");
?>
