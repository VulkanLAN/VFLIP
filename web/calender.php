<?php

/**
 * 
 * @author Moritz Eysholdt
 * @version $Id: calender.php 1702 2019-01-09 09:01:12Z scope $ edit by naaux
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");

class CalenderPage extends Page {
	var $ViewCalenderRight = "calender_view";
	var $CreateEventsRight = "calender_create_events";
	var $EditTypesRight = "calender_types_edit";
	var $LocalConfig = "calender_locale";

	//php 7 public function __construct()
	//php 5 original function CalenderPage()
	function __construct() {
		global $User;
		$User->requireRight($this->ViewCalenderRight);
		if ($User->hasRight($this->CreateEventsRight))
			$this->canadd = "true";
		else
			$this->canadd = "";
		//php 5:
		//parent :: Page();
		//php7 neu:
		parent::__construct();
		foreach (explode(";", ConfigGet($this->LocalConfig)) as $locale)
			if (@ setlocale(LC_TIME, trim($locale)))
				break;
	}

	function getTimeSpan($Area, $Time = 0) {
		if (empty ($Time))
			$Time = time();
		$t = getdate($Time);
		$from = $to = 0;
		switch ($Area) {
			case ("year") :
				$from = mktime(0, 0, 0, 1, 1, $t["year"]);
				$to = mktime(23, 59, 59, 12, 31, $t["year"]);
				break;
			case ("mon") :
				$from = mktime(0, 0, 0, $t["mon"], 1, $t["year"]);
				$to = mktime(23, 59, 59, $t["mon"], date("t", $Time), $t["year"]);
				break;
			case ("week") :
				$wd = ($t["wday"] == 0) ? 6 : $t["wday"] - 1;
				$d = 60 * 60 * 24;
				$from = mktime(0, 0, 0, $t["mon"], $t["mday"], $t["year"]) - ($wd * $d);
				$to = mktime(23, 59, 59, $t["mon"], $t["mday"], $t["year"]) + ((6 - $wd) * $d);
				break;
			case ("day") :
				$from = mktime(0, 0, 0, $t["mon"], $t["mday"], $t["year"]);
				$to = mktime(23, 59, 59, $t["mon"], $t["mday"], $t["year"]);
				break;

			default :
				trigger_error_text("Ein Zeitspannenname ist ung&uuml;ltig.|Area:$Area", E_USER_ERROR);
		}
		return array (
			$from,
			$to,
			$t
		);
	}

	function loadEventsBetween($from, $to, $cols = array (), $LookupUser = false) {
		global $User;
		if (is_array($cols)) {
			$cols += array (
				"start_time",
				"end_time",
				"caption",
				"id"
			);
			$cols = "e." . implode(",e.", $cols);
		} else
			$cols = "e.*";

		if ($LookupUser) {
			$usercol = ", s.name";
			$userjoin = "LEFT JOIN `" . TblPrefix() . "flip_user_subject` s ON (e.last_change_user_id = s.id)";
		} else
			$usercol = $userjoin = "";

		return MysqlReadArea("
		      SELECT $cols, t.style, t.is_wholeday, t.is_point, t.caption AS `type`$usercol FROM (`" . TblPrefix() . "flip_calender_events` e)
		        LEFT JOIN `" . TblPrefix() . "flip_calender_types` t ON (e.type_id = t.id)
		        $userjoin
		        WHERE (
		          (e.start_time < $to) AND (e.end_time > $from) AND
		          " . $User->sqlHasRight("t.view_right") . " AND
		          " . $User->sqlHasRight("e.view_right") . "
		        )
		        ORDER BY e.start_time;");
	}

	function eventsOverlap($e1, $e2) {
		if (($e1["end_time"] > $e2["start_time"]) and ($e2["end_time"] > $e1["start_time"]))
			return true;
		if ($e1["start_time"] == $e2["start_time"])
			return true;
		return false;
	}

	function getEventsBetween($from, $to, $events) {
		$r = array ();
		foreach ($events as $v)
			if (($v["end_time"] > $from) and ($v["start_time"] < $to)) {
				if ($v["end_time"] > $to) {
					$v["end_time"] = $to;
					$v["openend"] = true;
				}
				if ($v["start_time"] < $from) {
					$v["start_time"] = $from;
					$v["openopening"] = true;
				}
				$r[] = $v;
			}
		return $r;
	}

	function getEventsForDay($month, $day, $year, $events) {
		return $this->getEventsBetween(mktime(0, 0, 0, $month, $day, $year), mktime(23, 59, 59, $month, $day, $year), $events);
	}

	function frameDefault($get, $post) {
		$this->Caption = "Kalender - &Uuml;bersicht";
		return array ();
	}

	function frameEditEvent($get) {
		global $User;
		ArrayWithKeys($get, array("id","type","day"));
		$r = array (
		"time" => time());
		if (empty ($get["id"])) {
			$User->requireRight($this->CreateEventsRight);
			if (empty ($get["type"])) {
				$this->Caption = "Kalender -> Ereignistyp ausw&auml;hlen";
				return array (
				"types" => MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_calender_types` WHERE `name` IS NULL ORDER BY `caption`;"), "selecttype" => true, "allow_edittypes" => $User->hasRight($this->EditTypesRight), "day" => $get["day"], "backlink" => $_SERVER["HTTP_REFERER"]);
			} else {
				$this->Caption = "Kalender -> Ereignis erstellen";
				$r["caption"] = "neues Ereignis";
				$r["edit_right"] = "edit_public_text";
				$r["type_id"] = $get["type"];

				if (empty ($get["day"])) {
					$r["start_time"] = time();
					$r["end_time"] = time() + 60 * 60;
				} else {
					$t = getdate($get["day"]);
					$r["start_time"] = mktime(9, 0, 0, $t["mon"], $t["mday"], $t["year"]);
					$r["end_time"] = mktime(10, 0, 0, $t["mon"], $t["mday"], $t["year"]);
				}
			}
		} else {
			$this->Caption = "Kalender -> Ereignis bearbeiten";
			$r += MysqlReadRowByRight(TblPrefix() . "flip_calender_events", $get["id"], array (
				"view_right",
				"edit_right"
			));
		}
		$r["type"] = $t = MysqlReadRowByID(TblPrefix() . "flip_calender_types", $r["type_id"]);
		$r["text1edit"] = ((($r["time"] < $r["start_time"]) and ($t["text1_editable"] == "before")) or (($r["time"] > $r["start_time"]) and ($t["text1_editable"] == "after")) or ($t["text1_editable"] == "always")) ? true : false;
		$r["text2edit"] = ((($r["time"] < $r["start_time"]) and ($t["text2_editable"] == "before")) or (($r["time"] > $r["start_time"]) and ($t["text2_editable"] == "after")) or ($t["text2_editable"] == "always")) ? true : false;
		$r["start_time_n"] = "start_time";
		return $r;
	}

	function submitEvent($data) {
		global $User;
		ArrayWithKeys($data, array("id"));
		if (empty ($data["id"])) {
			$User->requireRight($this->CreateEventsRight);
			$job = "erstellt";
		} else
			$job = "bearbeitet";
		if (MysqlReadFieldByID(TblPrefix() . "flip_calender_types", "is_point", $data["type_id"])) {
			$data["end_time"] = $data["start_time"];
		}
		elseif ($data["start_time"] > $data["end_time"]) {
			trigger_error_text("Das Ereignis kann nicht zu Ende sein bevor es angefangen hat.", E_USER_WARNING);
			return false;
		}
		$data["last_change_user_id"] = $User->id;
		$data["last_change_time"] = time();
		$r = MysqlWriteByRight(TblPrefix() . "flip_calender_events", $data, array (
			"edit_right",
			"view_right"
		), $data["id"]);
		if ($r)
			LogChange("Im <b>Kalender</b> wurde das <b>Ereignis <a href=\"calender.php?frame=viewevent&amp;id=$r\">$data[caption]</a></b> $job.");
		return $r;
	}

	function frameYearGraph($get) {
		$this->ShowContentCell = false;
		ArrayWithKeys($get, array("time"));
		list ($from, $to, $ts) = $this->getTimeSpan("year", $get["time"]);
		$this->Caption = "Kalender -> Jahres&uuml;bersicht $ts[year]";
		$r = array (
			"next" => $to +10000,
			"previous" => $from -10000,
			"items" => array (),
			"year" => $ts["year"],
			"daynums" => array (),
			"from" => $from,
			"to" => $to,
			"canadd" => $this->canadd
		);
		$events = $this->loadEventsBetween($from, $to);
		// for($d = 1; $d < 32; $d++) $r["daynums"][] = $d;
		for ($m = 1; $m < 13; $m++) {
			$r["months"][$m] = mktime(1, 1, 1, $m, 1, $ts["year"]);
			for ($d = 1; $d < 32; $d++) {
				if (checkdate($m, $d, $ts["year"])) // alle tage im jahr durchgehen
					{
					$e = array ();
					foreach ($this->getEventsForDay($m, $d, $ts["year"], $events) as $v)
						$e[] = array (
							"caption" => date("H:i",
							$v["start_time"]
						) . " - " . date("H:i", $v["end_time"]) . ": $v[caption]", "short" => $v["caption"] { 0 }, "style" => $v["style"], "id" => $v["id"]);
					$t = mktime(1, 1, 1, $m, $d, $ts["year"]);
					$i = array (
						"events" => $e,
						"daytime" => $t,
						"week" => (date("w",
						$t
					) == 1) ? date("W", $t) : "");
				} else
					$i = array ();
				$r["items"][$d][$m] = $i;
			}
		}
		if (($from < time()) and ($to > time())) {
			$t = getdate();
			$r["items"][$t["mday"]][$t["mon"]]["istoday"] = true;
		}
		return $r;
	}

	function frameMonthGraph($get) {
		$this->ShowContentCell = false;
		ArrayWithKeys($get, array("time"));
		if(empty($get['time']))
			$get['time'] = time();
		list ($from, $to, $r) = $this->getTimeSpan("mon", $get["time"]);
		$this->Caption = "Kalender -> Monats&uuml;bersicht " . strftime("%B", $get["time"]) . " $r[year]";
		$r += array (
			"next" => $to +10000,
			"previous" => $from -10000,
			"items" => array (),
			"time" => $get["time"],
			"from" => $from,
			"to" => $to,
			"canadd" => $this->canadd
		);
		$events = $this->loadEventsBetween($from, $to);
		$week = date("W", $from);
		$items = array ();
		$wday = date("w", $from);
		$wday = ($wday == 0) ? 7 : $wday;
		for ($i = 1; $i < $wday; $i++)
			$items[$week]["days"][$i] = array ();
		$wday--;
		$items[$week]["time"] = mktime(1, 1, 1, $r["mon"], 1, $r["year"]);
		for ($d = 1; $d < 32; $d++)
			if (checkdate($r["mon"], $d, $r["year"])) // alle tage im monat durchgehen
				{
				if ($wday > 6) {
					$wday = 1;
					$week++;
					if ($week > 53)
						$week = 1;
					$items[$week]["time"] = mktime(1, 1, 1, $r["mon"], $d, $r["year"]);
				} else
					$wday++;
				$e = array ();
				foreach ($this->getEventsForDay($r["mon"], $d, $r["year"], $events) as $v) {
					$e[] = $v; //date("H:i",$v["start_time"])." - ".date("H:i",$v["end_time"]).": $v[caption]";
				}
				$daytime = mktime(1, 1, 1, $r["mon"], $d, $r["year"]);
				$i = array (
					"events" => $e,
					"day" => $d,
					"time" => $daytime
				);
				$r["days"][$wday] = $daytime;
				$items[$week]["days"][$wday] = $i;
			}
		for ($i = $wday +1; $i < 8; $i++)
			$items[$week]["days"][$i] = array ();
		ksort($r["days"]);
		if (($from < time()) and ($to > time()))
			$items[date("W")]["days"][(date("w") == 0) ? 7 : date("w")]["istoday"] = true;
		$r["items"] = $items;

		return $r;
	}

	function _calcVScala($from, $to) {
		$hour = 60 * 60;
		$scalaitems = array ();
		$time = $from;
		$d = getdate($from);
		$day = mktime(0, 0, 0, $d["mon"], $d["mday"], $d["year"]);
		$scala = $links = array ();
		while ($time < $to) {
			if (($time < $to) and ($time > $from)) {
				$scala[$time] = date("j", $time) . strftime(". %b", $time);
				$link[$time] = true;
			}
			$time += $hour * 8;
			if (($time < $to) and ($time > $from))
				$scala[$time] = "8 Uhr";
			$time += $hour * 4;
			if (($time < $to) and ($time > $from))
				$scala[$time] = "12 Uhr";
			$time += $hour * 3;
			if (($time < $to) and ($time > $from))
				$scala[$time] = "15 Uhr";
			$time += $hour * 5;
			if (($time < $to) and ($time > $from))
				$scala[$time] = "20 Uhr";
			$time += $hour * 4;
		}
		foreach ($scala as $k => $v)
			$scalaitems[] = array (
				"start_range" => $k -3600,
				"end_range" => $k,
				"index" => $v,
				"link" => $link[$k]
			);
		return $scalaitems;
	}

	function calcVGraph($from, $to, $events, $ShowScala = true, $ShowEvents = true, $ShowWholeDayEvents = false) {
		$scalaitems = ($ShowScala) ? $this->_calcVScala($from, $to) : array ();
		$items = array (
			0 => $scalaitems
		);

		// in Ebenen einsortieren
		if ($ShowEvents)
			foreach ($events as $event) {
				$level = 0;
				if ($event["is_wholeday"]) {
					if ($ShowWholeDayEvents) {
						list ($event["start_time"]) = $this->getTimeSpan("day", $event["start_time"]);
						list (, $event["end_time"]) = $this->getTimeSpan("day", $event["end_time"]);
						$event["is_point"] = false;
					} else
						continue;
				}
				if (($event["end_time"] < $from) or ($event["start_time"] > $to))
					continue;
				while (true) {
					$level++;
					if (!is_array($items[$level]))
						$items[$level] = array ();
					foreach ($items[$level] as $i)
						if ($this->eventsOverlap($i, $event))
							continue 2;
					break;
				}
				if ($event["is_point"]) {
					$event["end_range"] = $event["start_time"];
					$event["start_range"] = $event["start_time"] - (60 * 60);
				} else {
					$event["end_range"] = $event["end_time"];
					$event["start_range"] = $event["start_time"];
					if ($event["end_range"] > $to)
						$event = array (
							"openend" => true,
							"end_range" => $to
						) + $event;
					if ($event["start_range"] < $from)
						$event = array (
							"openopening" => true,
							"start_range" => $from
						) + $event;
				}
				$items[$level][] = $event;
			}

		// H&ouml;he berechnen    
		$pageheight = round(($to - $from) / 150);
		$all = $to - $from;
		foreach ($items as $k => $level) {
			$lastend = $from;
			$e = array ();
			$h = 0;
			foreach ($level as $i) {
				if ($lastend != $i["start_range"]) {
					$h += $x = round(($i["start_range"] - $lastend) / $all * $pageheight);
					$e[] = array (
						"height" => $x
					);
				}
				$h += $i["height"] = round(($i["end_range"] - $i["start_range"]) / $all * $pageheight);
				$lastend = $i["end_range"];
				$e[] = $i;
			}
			if ($h < $pageheight)
				$e[] = array (
					"height" => $pageheight - $h
				);
			$items[$k] = $e;
		}

		return array (
			"showscala" => $ShowScala,
			"showevents" => $ShowEvents,
			"scala" => array_shift($items
		), "items" => $items, "pageheight" => $pageheight,);
	}

	function calcDayEvents($from, $to, $events) {
		$r = array (
			"events" => array ()
		);
		foreach ($events as $event) {
			if (!$event["is_wholeday"])
				continue;
			if (($event["end_time"] < $from) or ($event["start_time"] > $to))
				continue;
			$r["events"][] = $event;
		}
		return $r;
	}

	function frameGraph($get) {
		$this->Caption = "Kalender -> grafische &uuml;bersicht";
		ArrayWithKeys($get, array("from","to","time"));
		if (empty ($get["from"]) or empty ($get["to"]))
			list ($from, $to) = $this->getTimeSpan("day", $get["time"]);
		else
			list ($from, $to) = array (
				$get["from"],
				$get["to"]
			);
		if ($from > $to) {
			$x = $from;
			$from = $to;
			$to = $x;
		}
		$events = $this->loadEventsBetween($from, $to);
		return array (
			"vgraph" => $this->calcVGraph($from,
			$to,
			$events,
			true,
			true,
			true
		), "from" => $from, "to" => $to, "next" => $to +60 * 60 * 24, "previous" => $from -60 * 60 * 24, "time" => time());
	}

	function submitRedirgraph($data) {
		$this->SubmitMessage = "";
		$this->NextPage = "$_SERVER[PHP_SELF]?frame=graph&from=$data[from]&to=$data[to]";
		return true;
	}

	function frameDayGraph($get) {
		ArrayWithKeys($get, array("time"));
		list ($from, $to) = $this->getTimeSpan("day", $get["time"]);
		$this->Caption = "Kalender -> Tages&uuml;bersicht zum " . strftime("%A den ", $from) . date("j", $from) . strftime(". %B %Y", $from);
		$events = $this->loadEventsBetween($from, $to);
		return array (
			"vgraph" => $this->calcVGraph($from,
			$to,
			$events,
			true,
			true
		), "dayevents" => $this->calcDayEvents($from, $to, $events), "from" => $from, "to" => $to, "next" => $to +60 * 60 * 24, "previous" => $from -60 * 60 * 24, "time" => time(), "istoday" => (($from < time()) and ($to > time())) ? true : false, "canadd" => $this->canadd);
	}

	function frameWeekGraph($get) {
		ArrayWithKeys($get, array("time"));
		list ($from, $to) = $this->getTimeSpan("week", $get["time"]);
		$this->Caption = "Kalender -> &uuml;bersicht der Kalenderwoche " . date("W, Y", $to);
		$this->ShowContentCell = false;
		$events = $this->loadEventsBetween($from, $to);
		$items = array ();
		$first = true;
		for ($day = $from +12 * 60 * 60; $day < $to; $day += 24 * 60 * 60) {
			list ($f, $t) = $this->getTimeSpan("day", $day);
			if ($first)
				$scala = $this->calcVGraph($f, $t, $events, true, false);
			else
				$first = false;
			$items[] = array (
				"time" => $day,
				"vgraph" => $this->calcVGraph($f,
				$t,
				$events,
				false,
				true
			), "dayevents" => $this->calcDayEvents($f, $t, $events), "istoday" => (($f < time()) and ($t > time())) ? true : false);
		}
		$showdayevents = false;
		foreach ($items as $i)
			if (!empty ($i["dayevents"]["events"]))
				$showdayevents = true;

		return array (
			"items" => $items,
			"scala" => $scala,
			"showdayevents" => $showdayevents,
			"next" => $to +60 * 60,
			"previous" => $from -60 * 60,
			"from" => $from,
			"to" => $to,
			"diffyears" => (date("Y",
			$from
		) == date("Y", $to)) ? false : true, "diffmonth" => (date("n", $from) == date("n", $to)) ? false : true, "canadd" => $this->canadd);
	}

	function frameListEvents($get) {
		include_once ("mod/mod.time.php");
		$this->Caption = "Kalender -> Ereignisliste";
		ArrayWithKeys($get, array("from","to","area","time"));
		if (!empty ($get["from"]) and !empty ($get["to"])) {
			$from = min($get["from"], $get["to"]);
			$to = max($get["from"], $get["to"]);
		} else {
			$area = (empty ($get["area"])) ? "day" : $get["area"];
			$time = (empty ($get["time"])) ? time() : $get["time"];
			list ($from, $to) = $this->getTimeSpan($area, $time);
		}
		$events = $this->loadEventsBetween($from, $to, null, true);
		foreach ($events as $k => $v)
			$events[$k]["span"] = FormatTimeDist($v["end_time"] - $v["start_time"], 2);
		return array (
			"from" => $from,
			"to" => $to,
			"events" => $events
		);
	}

	function actionDeleteEvent($data) {
		if (!is_array($data["ids"]))
			return false;
		global $User;
		$events = MysqlReadArea("SELECT id, caption, edit_right FROM `" . TblPrefix() . "flip_calender_events` WHERE id IN (" . implode_sqlIn($data["ids"]) . ")");
		foreach ($events as $row) {
			$capt = $row["caption"];
			if (!$User->hasRight($row["edit_right"]))
				trigger_error_text("Du bist nicht berechtigt '$capt' zu l&ouml;schen!", E_USER_WARNING);
			else
				if (MysqlDeleteByRight(TblPrefix() .
					"flip_calender_events", $row["id"], "edit_right"))
					LogChange("Im <b>Kalender</b> wurde das Ereignis <b>$capt</b> gel&ouml;scht.");
		}
		return true;
	}
	
	function _getEventParticipants($eventid) {
		return MysqlReadArea('SELECT u.name, u.id FROM (`'.TblPrefix().'flip_user_subject` u)' .
							 'LEFT JOIN `'.TblPrefix().'flip_calender_signups` s ' .
							 'ON (u.id = s.user_id) ' .
							 'WHERE u.type= \'user\' AND s.event_id = \''.escape_sqlData_without_quotes($eventid).'\';','id');
	}

	function frameViewEvent($get) {
		include_once ("mod/mod.time.php");
		global $User;
		ArrayWithKeys($get, array("id"));
		$id = addslashes($get["id"]);
		$r = MysqlReadRow("
		      SELECT e.*, t.style, t.text2_caption, t.text1_caption, t.caption AS `type`, s.name, t.is_wholeday, t.is_point, e.signup_right, t.can_signup 
			FROM (`" . TblPrefix() . "flip_calender_events` e)
		        LEFT JOIN `" . TblPrefix() . "flip_calender_types` t ON (e.type_id = t.id)
		        LEFT JOIN `" . TblPrefix() . "flip_user_subject` s ON (e.last_change_user_id = s.id)
		        WHERE (
		          (e.id = '$id') AND
		          " . $User->sqlHasRight("t.view_right") . " AND
		          " . $User->sqlHasRight("e.view_right") . "
		        );");
		if (!$r["is_point"])
			$r["span"] = FormatTimeDist($r["end_time"] - $r["start_time"], 10);
		$r["referer"] = $_SERVER["HTTP_REFERER"];
		$r["allow_edit"] = $User->hasRight($r["edit_right"]);
		$r['user_can_signup'] = ($User->hasRight($r['signup_right']) && $r['can_signup']);
		if($r['can_signup']) {
			$p = $this->_getEventParticipants($r['id']);
			$r['event_participants'] = array_chunk($p, 10);
			$r['user_is_signedup'] = array_key_exists($User->id,$p);
		}
		$this->Caption = "Kalender -> $r[caption]";
		return $r;
	}
	
	function actionSignup4Event($g) {
		global $User;
		if(empty($g['event_id']))
			return trigger_error_text('Die Event-ID ist f&uuml;r eine Anmeldung erforderlich!',E_USER_ERROR);
		$eid = escape_sqlData_without_quotes($g['event_id']);
		$event_right = MysqlReadField('SELECT * FROM `'.TblPrefix().'flip_calender_events` ' .
									  'WHERE `id` = \''.$eid.'\';','signup_right');
		if(is_empty($event_right))
			return trigger_error_text('Es gibt keinen Event mit der ID \''.$eid.'\'!',E_USER_ERROR);
		if(!$User->hasRight($event_right))
			return trigger_error_text('Dir fehlt das Recht um dich am Event anmelden zu k&ouml;nnen!',E_USER_ERROR);
		$data = array('event_id' => $g['event_id'], 'user_id' => $User->id);
		// Unique Keys der Tabelle: event_id und user_id
		// -> Doppelte Datens&auml;tze nicht m&ouml;glich und 1 MySQL-Abfrage weniger :)
		if(MysqlWriteByID(TblPrefix() . 'flip_calender_signups', $data)) {
			trigger_error_text('Du bist angemeldet worden!',E_USER_NOTICE);
			return true;
		} else {
			trigger_error_text('Die Anmeldung zum Event war nicht erfolgreich!',E_USER_ERROR);
			return false;
		}
	}

	function actionSignoffFromEvent($g) {
		global $User;
		if(empty($g['event_id']))
			return trigger_error_text('Die Event-ID ist f&uuml;r eine Abmeldung erforderlich!',E_USER_ERROR);
		$eid = escape_sqlData_without_quotes($g['event_id']);
		if(MysqlDeleteRow('DELETE FROM `'.TblPrefix().'flip_calender_signups` ' .
						  'WHERE `event_id` =\''.$eid.'\' AND `user_id` = \''.$User->id.'\';')) {
			trigger_error_text('Du wurdest abgemeldet!',E_USER_NOTICE);
			return true;
		} else {
			trigger_error_text('Du konntest nicht abgemeldet werden!',E_USER_ERROR);
			return false;			
		}
	}

	/*******************************************************************************************/
	/*                         Typen anzeigen und bearbeiten                                   */
	/*******************************************************************************************/

	function frameListTypes($get) {
		$this->Caption = "Kalender -> Ereignistypen";
		return array (
		"items" => MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_calender_types` ORDER BY `caption`"), "editright" => $this->EditTypesRight, "time" => time());
	}

	function frameEditType($get) {
		global $User;
		$this->Caption = "Kalender -> Typ bearbeiten";
		$User->requireRight($this->EditTypesRight);
		ArrayWithKeys($get, array("id"));
		$r = array (
			"edittypes" => array (
				"always" => "Immer",
				"never" => "Nie (ausgeblendet)",
				"before" => "Vor dem Ereignis",
				"after" => "Nach dem Ereignis"
			)
		);
		if (empty ($get["id"]))
			return $r + array (
				"caption" => "Neuer Type",
				"editable" => 1
			);
		$r += MysqlReadRowByRight(TblPrefix() . "flip_calender_types", $get["id"], array (
			"view_right",
			"edit_right"
		));
		$r["editable"] = empty ($r["name"]);
		return $r;
	}

	function submitType($dat) {
		global $User;
		ArrayWithKeys($dat, array("id"));
		$User->requireRight($this->EditTypesRight);
		$r = MysqlWriteByRight(TblPrefix() . "flip_calender_types", $dat, array (
			"view_right",
			"edit_right"
		), $dat["id"]);
		if ($r) {
			$c = htmlspecialchars(MysqlReadFieldByID(TblPrefix() . "flip_calender_types", "caption", $r));
			$e = (empty ($dat["id"])) ? "erstellt" : "bearbeitet";
			LogChange("Im <b>Kalender</b> wurde der Typ <a href=\"calender.php?frame=edittype&amp;id=$r\"><b>$c</b></a> $e.");
		}
		return $r;
	}

	function actionDeleteType($post) {
		global $User;
		$User->requireRight($this->EditTypesRight);
		ArrayWithKeys($post, array("id"));
		$c = htmlspecialchars(MysqlReadFieldByID(TblPrefix() . "flip_calender_types", "caption", $post["id"]));
		$r = MysqlDeleteByRight(TblPrefix() . "flip_calender_types", $post["id"], array (
			"view_right",
			"edit_right"
		));
		if ($r)
			LogChange("Im <b>Kalender</b> wurde der Typ <b>$c</b> gel&ouml;scht.");
		return $r;
	}
}

RunPage("CalenderPage");
?>
