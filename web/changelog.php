<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: log.php 642 2004-12-12 12:17:47Z docfx $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");

class ChangeLogPage extends Page {
	var $SourceXML = "changelog_sourcexml";

	#<logentry revision="2">
	#<author>root</author>
	#<date>2003-12-29T14:12:00.000000Z</date>
	#<msg>initial checkin
	#</msg>
	#</logentry>

	function parseChangeLog($ChangeLogXMLString) {
		// TODO: diesen Code in PHP5 mit SimpleXML implementieren.

		$items = $a = $result = array ();
		preg_match_all("/<logentry\s+revision=\"?(\d+)\"?>(.*?)<\/logentry>/is", $ChangeLogXMLString, $items, PREG_SET_ORDER);
		foreach ($items as $i) {
			$r["revision"] = $i[1];
			preg_match("/<author>(.*)<\/author>/is", $i[2], $a);
			if(!isset($a[1])) {
				$a[1] = null;
			}
			$r["author"] = $a[1];
			preg_match("/<date>([\d\-]*)t([\d:]*).*<\/date>/is", $i[2], $a);
			$r["date"] = strtotime("$a[1] $a[2]");
			preg_match("/<msg>(.*)<\/msg>/is", $i[2], $a);
			$r["msg"] = nl2br(trim(utf8_decode($a[1])));
			$result[] = $r;
		}
		return $result;
	}

	function frameDefault($get, $post) {
		include_once ("mod/mod.httpclient.php");
		$this->Caption = "FLIP-ChangeLog";
		$xml = HTTPRequestByGet(ConfigGet($this->SourceXML));
		if (empty ($xml))
			trigger_error_text("Die XML-Datei welche den Changelog enthält konnte nicht geladen werden.|file:$xml", E_USER_ERROR);
		$xml = utf8_encode($xml);
		return array (
			"items" => $this->parseChangeLog($xml
		), "dbversion" => REQUIRED_DB_VERSION);
	}

}

RunPage("ChangeLogPage");
?>