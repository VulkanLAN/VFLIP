<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: checkininfo.php 1702 2019-01-09 09:01:12Z docfx $ edit by VulkanLAN
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");

class TablePage extends Page {
	var $TableRight = "table_edit";
	
	//php 7 public function __construct()
	//php 5 original function TablePage()
	function __construct() {
		//php 5:
		//parent :: Page();
		//php7 neu:
		parent::__construct();
		global $User;
		$User->requireRight($this->TableRight);
	}

	function frameDefault($get, $post) {
		$this->Caption = "Table";
		return array (
		"items" => MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_table_tables`;"));
	}

	function frameEditTable($get) {
		$this->Caption = "Table -> Bearbeiten";
		if (empty ($get["id"]))
			return array (
				"name" => "new_table"
			);
		else
			return MysqlReadRowByRight(TblPrefix() . "flip_table_tables", $get["id"], "edit_right");
	}

	function submitTable($data) {
		$r = MysqlWriteByRight(TblPrefix() . "flip_table_tables", $data, "edit_right", $data["id"]);
		if ($r)
			LogChange("Die <b>Tabelle <a href=\"table.php?frame=viewtable&amp;id=$r\">{$data[name]}</a></b> wurde " . ((empty ($data["id"])) ? "erstellt." : "bearbeitet."));
		return $r;
	}

	function actionDeleteTable($data) {
		if (is_array($data["ids"]))
			foreach ($data["ids"] as $id) {
				$n = MysqlReadFieldByID(TblPrefix() . "flip_table_tables", "name", $id);
				MysqlWrite("DELETE FROM `" . TblPrefix() . "flip_table_entry` WHERE (`table_id` = '" . addslashes($id) . "');");
				$r = MysqlDeleteByRight(TblPrefix() . "flip_table_tables", $id, "edit_right");
				if ($r)
					LogChange("Die <b>Tabelle $n</b> wurde gel&ouml;scht.");
			}
	}

	function frameViewTable($get) {
		global $User;
		ArrayWithKeys($get, array("id","name")); 
		if (is_posDigit($get["id"]))
			$id = $get["id"];
		elseif (!empty ($get["name"])) $id = MysqlReadField("SELECT `id` FROM `" . TblPrefix() . "flip_table_tables` WHERE (`name` = '" . addslashes($get["name"]) . "')");
		else
			trigger_error_text("Die Tabelle kann nicht angezeigt werden, da weder ein g&uuml;ltiger Name noch eine g&uuml;ltige ID &uuml;bergeben wurde.", E_USER_ERROR);
		$this->Caption = "Table -> " . MysqlReadField("SELECT `name` FROM `" . TblPrefix() . "flip_table_tables` WHERE (`id` = '" . addslashes($id) . "')");
		;
		return array (
		"items" => MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_table_entry` WHERE (`table_id` = '$id');"), "table_id" => $id, "allowedit" => ($User->hasRight(MysqlReadFieldByID(TblPrefix() . "flip_table_tables", "edit_right", $id))) ? true : false);
	}

	function frameEditItem($get) {
		global $User;
		$this->Caption = "Table -> Eintrag bearbeiten";
		if (empty ($get["id"]))
			return array (
				"key" => "new_key"
			);
		$r = MysqlReadRowByID(TblPrefix() . "flip_table_entry", $get["id"]);
		$User->requireRight(MysqlReadFieldByID(TblPrefix() . "flip_table_tables", "edit_right", $r["table_id"]));
		if ($r["value"] == $r["display"])
			unset ($r["display"]);
		return $r;
	}

	function submitItem($data) {
		global $User;
		$User->requireRight(MysqlReadFieldByID(TblPrefix() . "flip_table_tables", "edit_right", $data["table_id"]));
		if (empty ($data["display"]))
			$data["display"] = $data["value"];
		$data["display_code"] = trim(TemplateCompile($data["display"], "Display"));
		if (empty ($data["display_code"])) {
			trigger_error_text("Der Templatecode konnte nicht Compiliert werden.", E_USER_WARNING);
			return false;
		}
		$data["code_exec"] = ($data["display"] == $data["display_code"]) ? 0 : 1;
		$r = MysqlWriteByID(TblPrefix() . "flip_table_entry", $data, $data["id"]);
		if ($r)
			LogChange("Der <b>Tabelleneintrag <a href=\"table.php?frame=edititem&amp;id=$r\">{$data[value]}</a></b> wurde " . ((empty ($data["id"])) ? "erstellt." : "bearbeitet."));
		return $r;
	}

	function actionDeleteItem($get) {
		global $User;
		if (is_array($get["ids"]))
			foreach ($get["ids"] as $id) {
				$old = MysqlReadRowByID(TblPrefix() . "flip_table_entry", $id);
				if ($User->hasRight(MysqlReadFieldByID(TblPrefix() . "flip_table_tables", "edit_right", $old["table_id"]))) {
					$r = MysqlDeleteByID(TblPrefix() . "flip_table_entry", $id);
					if ($r)
						LogChange("Der <b>Tabelleneintrag $old[value]</b> wurde gel&ouml;scht.");
				} else
					trigger_error_text("Der Tabelleneintrag $old[value] wurde nicht gel&ouml;scht, da nicht gen&uuml;gend Rechte vorhanden waren.", E_USER_WARNING);
			}
		return true;
	}
}

RunPage("TablePage");
?>