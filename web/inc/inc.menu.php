<?php
/**
 * @author Moritz Eysholdt
 * @version $Id: inc.page.menu.php 1702 2019-01-09 09:01:12Z docfx $ edit by VulkanLAN
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package inc
 **/

/** FLIP-Kern */
require_once ("core/core.php");

function MenuLoad($Dir = 0, $Level = null) {
	global $Session, $User;
	$MenuCacheSize = 8;

	if ($Session->Data["menuuser"] != $User->id) // wenn der user gewechselt hat, das gesamte menu neu laden
		{
		$Session->Data["menuuser"] = $User->id;
		unset ($Session->Data["menucache"]);
	}

	$cachename = "menu_items_$Dir#$Level";
	$menu = null;
	if(isset($Session->Data["menucache"]) && isset($Session->Data["menucache"][$cachename])) {
		$menu = $Session->Data["menucache"][$cachename];
		unset ($Session->Data["menucache"][$cachename]); //aus dem cache loeschen, um es spaeter ans ende des caches zu setzen.
	} 

	if (empty ($menu)) {
		if (empty ($Dir))
			$level = $parent = 0;
		else {
			$dir = explode("-", $Dir);
			$level = count($dir);
			$parent = addslashes($dir[$level -1]);
		}
		if (!is_null($Level))
			$level = $Level;

		$dyn = (empty ($parent) and !empty ($level)) ? "" : "((`parent_item_id` = '$parent') AND (`level_index` = 0)) OR ";

		$blocks = MysqlReadArea("
						  SELECT `id`,`caption`, `description`, `callback_items`, 
				            IF(LENGTH(`image_title`) > 0, CONCAT('".TblPrefix()."flip_menu_blocks:image_title:',`id`,':',`mtime`), 0) AS `image_title`,
				            IF(LENGTH(`image_bg`) > 0, CONCAT('".TblPrefix()."flip_menu_blocks:image_bg:',`id`,':',`mtime`), 0) AS `image_bg`
				            FROM `".TblPrefix()."flip_menu_blocks` 
						    WHERE (`enabled`='1' AND
						      ($dyn((`parent_item_id` = 0) AND (`level_index` = '$level'))
					          ) AND ".$User->sqlHasRight("view_right")."
						    ) ORDER BY `order`;
						", "id");
		if (!empty ($blocks)) {
			$ids = implode_sqlIn(array_keys($blocks));
			// VulkanLAN edit by naaux for bootstrap funktion
			/** add font_image als DB Feld fuer Menue Icons_ Modul - font-awesome - wird benoetigt*/
			$items = MysqlReadArea("
									  SELECT `id`,`caption`,`link`,`description`, `use_new_wnd`, `block_id`, `frameurl`, `text`, `font_image`,
						                IF(LENGTH(`image`) > 0, CONCAT('".TblPrefix()."flip_menu_links:image:',`id`,':',`mtime`), 0) AS `image`
						                FROM `".TblPrefix()."flip_menu_links` 
									    WHERE (
									      (`enabled` = 1 AND `block_id` IN ($ids)) AND
									      ".$User->sqlHasRight("view_right")."
									    ) ORDER BY `order`;
									");
			foreach ($items as $i) {
				$id = $i["id"];
				$b = $i["block_id"];
				$i["dir"] = (empty ($Dir)) ? $id : "$Dir-$id";
				if (!$i["use_new_wnd"])
					$i["link"] = $i["link"]. ((strpos($i["link"], "?") == 0) ? "?" : "&amp;")."menudir=$i[dir]";
				$blocks[$b]["items"][$id] = $i;
			}
		} else
			$blocks = array ();
		foreach ($blocks as $blockkey => $block)
			if (empty ($block["items"]) and empty ($block["frameurl"]))
				unset ($blocks[$blockkey]);
		$menu = $blocks; //ArrayCleanup($blocks);
	}

	$Session->Data["menucache"][$cachename] = $menu;
	if (count($Session->Data["menucache"]) > $MenuCacheSize)
		array_shift($Session->Data["menucache"]);

	$CurrentDir = isset($_GET["menudir"]) ? $_GET["menudir"] : "";
	if (empty ($CurrentDir))
		$CurrentDir = isset($Session->Data["menudir"]) ? $Session->Data["menudir"] : "";
	else
		$Session->Data["menudir"] = $CurrentDir;

	$cd = explode("-", $CurrentDir);
	$cdir = 0;
	$active = "";
	foreach ($menu as $blockkey => $block) {
		if (is_array($block["items"]))
			foreach ($block["items"] as $itemkey => $item) {
				if (!empty ($item["frameurl"])) {
					$item["frame"] = PageLoadFrame($item["frameurl"]);

					// frames mit leeren R&uuml;ckgabewerten werden ausgeblendet.
					if (empty ($item["frame"])) {
						unset ($menu[$blockkey]["items"][$itemkey]);
						continue;
					}
					unset ($item["frameurl"]);
					unset ($item["link"]);
				}
				elseif (in_array($itemkey, $cd)) {
					$active = $item["dir"];
					$item["active"] = 1;
					if ($CurrentDir == $active)
						$cdir = $item["currentdir"] = 1;
				} else
					$item["active"] = 0;
				$menu[$blockkey]["items"][$itemkey] = $item;
			}
		// Bl&ouml;cke ohne items werden ausgeblendet.
		if (empty ($menu[$blockkey]["items"]))
			unset ($menu[$blockkey]);
	}
	return array ("menu" => $menu, "activedir" => $active, "currentdir" => $cdir, "selecteddir" => $CurrentDir);
}

function LoadMenu($Dir = 0, $Level = null) {
	global $User;
  global $Session;
	$MenuCacheSize = 8;

	$menu = null;

	if (empty ($menu)) {
		if (empty ($Dir))
			$level = $parent = 0;
		else {
			$dir = explode("-", $Dir);
			$level = count($dir);
			$parent = addslashes($dir[$level -1]);
		}
		if (!is_null($Level))
			$level = $Level;

		$dyn = (empty ($parent) and !empty ($level)) ? "" : "((`parent_item_id` = '$parent') AND (`level_index` = 0)) OR ";

		$blocks = MysqlReadArea("
						  SELECT `id`,`caption`, `description`, `callback_items`, 
				            IF(LENGTH(`image_title`) > 0, CONCAT('".TblPrefix()."flip_menu_blocks:image_title:',`id`,':',`mtime`), 0) AS `image_title`,
				            IF(LENGTH(`image_bg`) > 0, CONCAT('".TblPrefix()."flip_menu_blocks:image_bg:',`id`,':',`mtime`), 0) AS `image_bg`
				            FROM `".TblPrefix()."flip_menu_blocks` 
						    WHERE (`enabled`='1' AND
						      ($dyn((`parent_item_id` = 0) AND (`level_index` = '$level'))
					          ) AND ".$User->sqlHasRight("view_right")."
						    ) ORDER BY `order`;
						", "id");
		if (!empty ($blocks)) {
			$ids = implode_sqlIn(array_keys($blocks));
			// VulkanLAN edit by naaux for bootstrap funktion
			/** add font_image als DB Feld fuer Menue Icons_ Modul - font-awesome - wird benoetigt*/
			$items = MysqlReadArea("
									  SELECT `id`,`caption`,`link`,`description`, `use_new_wnd`, `block_id`, `frameurl`, `text`, `font_image`,
						                IF(LENGTH(`image`) > 0, CONCAT('".TblPrefix()."flip_menu_links:image:',`id`,':',`mtime`), 0) AS `image`
						                FROM `".TblPrefix()."flip_menu_links` 
									    WHERE (
									      (`enabled` = 1 AND `block_id` IN ($ids)) AND
									      ".$User->sqlHasRight("view_right")."
									    ) ORDER BY `order`;
									");
			foreach ($items as $i) {
				$id = $i["id"];
				$b = $i["block_id"];
				$i["dir"] = (empty ($Dir)) ? $id : "$Dir-$id";
				if (!$i["use_new_wnd"])
					$i["link"] = $i["link"]. ((strpos($i["link"], "?") == 0) ? "?" : "&amp;")."menudir=$i[dir]";
				$test =  MenuLoad($id, 1);
				$i["sub"] = $test;
				// dd($test);
				$blocks[$b]["items"][$id] = $i;
				
			}
		} else
			$blocks = array ();
		foreach ($blocks as $blockkey => $block)
			if (empty ($block["items"]) and empty ($block["frameurl"]))
				unset ($blocks[$blockkey]);
		$menu = $blocks;
	}

	$Session->Data["menucache"][$cachename] = $menu;
	if (count($Session->Data["menucache"]) > $MenuCacheSize)
		array_shift($Session->Data["menucache"]);

	$CurrentDir = isset($_GET["menudir"]) ? $_GET["menudir"] : "";
	if (empty ($CurrentDir))
		$CurrentDir = isset($Session->Data["menudir"]) ? $Session->Data["menudir"] : "";
	else
		$Session->Data["menudir"] = $CurrentDir;

	$cd = explode("-", $CurrentDir);
	$cdir = 0;
	$active = "";

	

	return array ("menu" => $menu);
}

function dd($i) {
	echo "<pre>" . var_dump($i) . "</pre>";
	exit;
}

function MenuFlushCache() {
	global $Session, $User;
	// menu-user aus 0 setzen, damit wird der menucache beim n&auml;chsten Laden verworfen.
	$Session->Data["menuuser"] = 0;
}
?>