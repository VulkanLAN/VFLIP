<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: inc.text.php 1447 2007-08-10 11:09:21Z loom $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package inc
 **/

/** FLIP-Kern */
require_once ("core/core.php");

function _parseText($text) {
	$a = explode("{#IMAGE", $text);
	$r = array_shift($a);
	if (count($a) > 0) {
		include_once ("mod/mod.parse.php");
		include_once ("inc/inc.image.php");
		foreach ($a as $v) {
			list ($param, $text) = explode("}", $v, 2);
			$param = ParseParam($param, array (
				"name"
			), false);
			if (is_array($param)) {
				$i = LoadImage($param["name"], $param);
				$r .= $i->get() . $text;
			} else {
				$i = LoadImage("", array (), $param);
				$r .= $i->get() . $text;
			}
		}
	}
	return $r;
}

/**
 * Loads a text from the database.
 * 
 * @param String $Name The name of the text
 * @param String $Caption A variable into which the caption will be written
 * @param Boolean $showEdit If true (the default) the edit text will be shown, else it'll be supressed
 */
function LoadText($Name, & $Caption, $showEdit = true) {
	include_once ("mod/mod.template.php");
	global $User;
	if (empty ($Name))
		trigger_error_text(text_translate("F&uuml;r den angeforderten Text wurde kein Name angegeben!"), E_USER_ERROR);
	$n = addslashes($Name);
	$typ = (is_posDigit($n)) ? "id" : "name";
	$t = MysqlReadRow("
	    SELECT t.caption, t.text, t.id, t.edit_right, t.view_right, t.edit_time,
	          a.name AS `author`, g.view_right AS `group_view_right`, g.edit_right AS group_edit_right
		  FROM (`" . TblPrefix() . "flip_content_text` t)
		  LEFT JOIN `" . TblPrefix() . "flip_user_subject` a ON (t.edit_user_id = a.id)
		  LEFT JOIN `" . TblPrefix() . "flip_content_groups` g ON (t.group_id = g.id)
		  WHERE (t.$typ = '$n');	  
	  ", true);
	if (!is_array($t)) {
		$Caption = text_translate("Text nicht gefunden!");
		$t = array (
			"name" => $Name,
			"found" => false,
			"editable" => $User->hasRight("admin")
		);
		
		$text = text_translate("Der Text mit dem Namen oder der ID \"?1?\" existiert nicht.", $Name);
		trigger_error_text($text, E_USER_WARNING);
	} else {
		$User->requireRight($t["view_right"]);
		$User->requireRight($t["group_view_right"]);

		$Caption = $t["caption"];
		
		// Convert the special chars &auml;&ouml;&uuml;&szlig; into HTML code
		$t["text"] = _parseText(convertSpecialChars($t["text"]));

		// Translate the text
		$t["text"] = $t["text"];
		
		$t["editable"] = ($User->hasRight($t["edit_right"]) && $User->hasRight($t["group_edit_right"]));
		$t["date"] = date("y-m-d H:i", $t["edit_time"]);
		$t["found"] = true;
	}
	
	// Disable the edit line if it's explicitly requested
	$t['editable'] = $t['editable'] && $showEdit;
	
	return new Template("inc.text.tpl", $t);
}
?>
