<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: inc.form.php 1702 2019-01-09 09:01:12Z VulkanLAN $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package inputs
 **/

/** Die Datei nur einmal includen */
if (defined("MOD.FORM.PHP"))
	return 0;
define("MOD.FORM.PHP", 1);

/** FLIP-Kern */
require_once ("core/core.php");

function _InitForms() {
	global $_PostVals;
	$_PostVals = array ();
	foreach ($_GET as $k => $v)
		$_PostVals[$k] = $v;
	foreach ($_POST as $k => $v)
		$_PostVals[$k] = $v;
}

function InputExists($InputName) {
	return class_exists("_input_$InputName");
}

function _parseInputType($Type, & $Param) {
	$type = explode("|", $Type, 2);
	if (count($type) == 2)
		$Param = $type[1];
	return $type[0];
}

class Form {
	var $_Vals;
	var $_Post;
	var $_Meta;
	var $_KeepVals;

	//php 7 public function __construct()
	//php 5 original function Form()
	function __construct($aValues = array (), $aKeepVals = true) {
		$this->_Vals = $aValues;
		$this->_Post = array ();
		$this->_Meta = array ();
		$this->_KeepVals = $aKeepVals;
	}

	function _getClass($Type) {
		$class = "_Input_$Type";
		if (!class_exists($class)) {
			trigger_error("Ein Input-Typ ist ung&uuml;ltig.|Typ:\"$Type\"", E_USER_WARNING);
			return false;
		}
		return new $class ();
	}

	/*
	* $Param = array(
	*   array(
	*     "name" => 
	*     "type" =>
	*     "val" =>
	*     "param" => 
	*     "caption" => 
	*     "allowempty" => 
	*     "enabled" => 
	*   ),
	*   ... 
	* );
	*/
	function getEdits($Param) {
		$r = array ();
		foreach ($Param as $k => $p) {
			if (!is_array($p))
				$p = array (
					"type" => $p
				);
			if (!isset ($p["name"]))
				$p["name"] = $k;
			if (!isset ($p["type"]))
				$p["type"] = "string";
			if (!isset ($p["allowempty"]))
				$p["allowempty"] = true;
			if (!isset ($p["enabled"]))
				$p["enabled"] = true;

			$r[$k] = $this->getEdit($p["name"], $p["type"], $p["val"], $p["param"], $p["caption"], $p["allowempty"], $p["enabled"]);
		}
		return $r;
	}

	/*
	 * gibt eine instanz einer template-klasse zur&uuml;ck
	 * 
	 */
	function getEdit($Name, $Type = "string", $Val = NULL, $Param = NULL, $Caption = NULL, $AllowEmpty = true, $Enabled = true) {
		include_once ("mod/mod.template.php");
		global $_PostVals;
		$class = $this->_getClass(_parseInputType($Type, $Param));
		if (!is_object($class))
			return false;

		if ($Val !== NULL)
			$v = $Val;
		elseif (isset ($_PostVals[$Name]) and $class->allowKeepVals($this->_KeepVals)) $v = $_PostVals[$Name];
		elseif (isset ($this->_Vals["name"])) $v = $this->_Vals["name"];
		else
			$v = "";

		$v = $class->getEdit($Name, $v, $Enabled, $Param, $AllowEmpty);

		if (strpos($Name, "[]") !== false)
			$Name = substr($Name, 0, -2);
		if ($Enabled)
			$this->_Meta[$Name] = array (
				"t" => $Type,
				"c" => $Caption,
				"a" => $AllowEmpty,
				"p" => $Param
			);
		return new Template("inc.form.tpl", $v, $class->SubName);
	}

	function allowValue($Name, $Caption = "", $AllowEmpty = true, $Param = array ()) {
		$this->_Meta[$Name] = array (
			"t" => "hidden",
			"c" => $Caption,
			"a" => $AllowEmpty,
			"p" => $Param
		);
	}

	function getVal($Name, & $Fail) {
		if (!is_array($this->_Meta[$Name]))
			return $Fail = true;

		$m = $this->_Meta[$Name];
		$n = (empty ($m["c"])) ? $Name : $m["c"];
		$class = $this->_getClass(_parseInputType($m["t"], $m["p"]));
		$err = "";
		$r = $class->getVal($Name, $m["p"], $err);
		if (empty ($err)) {
			if ($m["a"])
				return $r;
			elseif (!$class->isEmpty($r)) return $r;
			else
				trigger_error("Das Feld \"$n\" muss auch ausgef&uuml;llt werden.", E_USER_WARNING);
		} else
			trigger_error("Der eingegebene Wert f&uuml;r \"$n\" ist ung&uuml;ltig. $err", E_USER_WARNING);
		return $Fail = true;
	}

	function getVals($Metadata) {
		$this->setMetadata($Metadata);
		$r = array ();
		foreach (array_keys($this->_Meta) as $k) {
			$fail = false;
			$v = $this->getVal($k, $fail);
			if ($fail)
				return NULL;
			if ($v !== NULL)
				$r[$k] = $v;
		}
		return $r;
	}

	// metatata enth&auml;lt name, type, caption, allowempty aller inputs die enabled sind.
	function getMetadata() {
		return Condense($this->_Meta);
	}

	function setMetadata($Metadata) {
		$this->_Meta = Uncondense($Metadata);
	}

	function getInputNames() {
		return array_keys($this->_Meta);
	}
}

class _Input {
	var $SubName;

	//php 7 public function __construct()
	//php 5 original function _Input()
	function __construct() {
		$this->SubName = strtolower(get_class($this));
	}

	function getEdit($Name, $Val, & $Enabled, $Param) {
		// gibt ein array zur&uuml;ck mit welches ans template weitergereicht wird.
		return array (
			"name" => $Name,
			"val" => $Val,
			"enabled" => (($Enabled
		) ? "" : " disabled=\"\""));
	}

	function getVal($Name, $Param, & $err) {
		// gibt den wert zur&uuml;ck und setzt bei fehlern $err non-empty
		global $_PostVals;
		return $_PostVals[$Name];
	}

	function isEmpty($Val) {
		return empty ($Val);
	}

	function allowKeepVals($keep) {
		return $keep;
	}
}

class _Input_String extends _Input {}

class _Input_LongString extends _Input {}

class _Input_Radio extends _Input {
	var $keep;

	function allowKeepVals($aKeep) {
		$this->keep = $aKeep;
		return false;
	}

	function getEdit($Name, $Val, & $Enabled, $Param) {
		global $_PostVals;
		$p = parent :: getEdit($Name, $Val, $Enabled, $Param);
		$p["sel"] = (($_PostVals[$Name] == $Val) and $this->keep) ? " checked=\"checked\"" : "";
		return $p;
	}
}

class _Input_InputTypes extends _Input_Dropdown {
	function getEdit($Name, $Val, & $Enabled, $Param) {
		$param = $a = "";
		$Val = _parseInputType($Val, $param);
		$p = array ();
		foreach (get_declared_classes() as $v)
			if (preg_match("/_Input_(.*)/i", $v, $a))
				$i[$a[1]] = array (
					"key" => $a[1],
					"caption" => $a[1],
					"sel" => ((strtolower($a[1]
				) == strtolower($Val)) ? "selected=\"selected\"" : ""));
		ksort($i);
		$en = (($Enabled) ? "" : " disabled=\"\"");
		return array (
			"name" => $Name,
			"items" => $i,
			"enabled" => $en, 
			"param" => $Param
		);
	}

	function getVal($Name, $Param, & $err) {
		global $_PostVals;
		$t = $_PostVals["{$Name}_type"];
		$p = $_PostVals["{$Name}_param"];
		$p = (empty ($p)) ? "" : "|$p";
		if (class_exists("_Input_$t"))
			return "$t$p";
		$err = text_translate("\"?1?\" is kein g&uuml;ltiger InputTyp", $t);
		return NULL;
	}
}

class _Input_YesNo extends _Input {
	function getEdit($Name, $Val, & $Enabled, $Param) {
		if ($Val == "Y") {
			$y = " checked=\"checked\"";
			$n = "";
		} else {
			$y = "";
			$n = " checked=\"checked\"";
		}
		return array (
			"name" => $Name,
			"sely" => $y,
			"seln" => $n,
			"enabled" => (($Enabled
		) ? "" : " disabled=\"\""));
	}

	function getVal($Name, $Param, & $err) {
		global $_PostVals;
		return ($_PostVals[$Name] == "Y") ? "Y" : "N";
	}
}

class _Input_Checkbox extends _Input {
	function getEdit($Name, $Val, & $Enabled, $Param) {
		$val = (empty ($Val)) ? "" : " checked=\"checked\"";
		if (empty ($Param))
			$Param = 1;
		return array (
			"name" => $Name,
			"sel" => $val,
			"enabled" => (($Enabled
		) ? "" : " disabled=\"disabled\""), "param" => $Param);
	}

	function getVal($Name, $Param, & $err) {
		global $_PostVals;
		return (empty ($_PostVals[$Name])) ? 0 : $_PostVals[$Name];
	}
}

class _Input_Checkbox_OnClick extends _Input {
	function getEdit($Name, $Val, & $Enabled, $Param) {
		$val = (empty ($Val)) ? "" : " checked=\"checked\"";
		if (empty ($Param))
			$Param = 1;
		return array (
			"name" => $Name,
			"sel" => $val,
			"enabled" => (($Enabled
		) ? "" : " disabled=\"disabled\""), "param" => $Param);
	}

	function getVal($Name, $Param, & $err) {
		global $_PostVals;
		return (empty ($_PostVals[$Name])) ? 0 : $_PostVals[$Name];
	}
}
class _Input_Integer extends _Input {
	function getVal($Name, $Param, & $err) {
		global $_PostVals;
		$v = $_PostVals[$Name];
		if (preg_match("/^\-?\d*$/", $v))
			return $v;
		$err = text_translate("\"?1?\" ist keine g&uuml;ltige Zahl (integer)", $v);
		return NULL;
	}
}

class _Input_Decimal extends _Input {
	function getVal($Name, $Param, & $err) {
		global $_PostVals;
		$v = $_PostVals[$Name];
		
		if (preg_match("/^\-?[\d]*[,.]?[\d]*$/", $v)) {
			return $v;
		}
		
		$err = text_translate("\"?1?\" ist keine g&uuml;ltige Zahl (float)", $v);
				
		return NULL;
	}
}

class _Input_IP extends _Input {
	function getVal($Name, $Param, & $err) {
		global $_PostVals;
		$v = $_PostVals[$Name];
		if ($v == "") {
			return $v;
		} else {
			if (preg_match("/^(25[0-5]|2[0-4][0-9]|[01]?[0-9]{1,2})\.(25[0-5]|2[0-4][0-9]|[01]?[0-9]{1,2})\.(25[0-5]|2[0-4][0-9]|[01]?[0-9]{1,2})\.(25[0-5]|2[0-4][0-9]|[01]?[0-9]{1,2})$/", $v))
				return $v;
			$err = text_translate("\"?1?\" ist keine g&uuml;ltige Zahl (integer)", $v);
			return NULL;
		}
	}
}

class _Input_Dropdown extends _Input {
	function getEdit($Name, $Val, & $Enabled, $Param) {
		$i = array ();
		if (is_array($Param))
			foreach ($Param as $k => $c)
				$i[] = array (
					"key" => $k,
					"caption" => $c,
					"sel" => (($k == $Val) ? "selected=\"selected\"" : "")
				);
		else
			$i = array (
				"" => ""
			);
		return array (
			"name" => $Name,
			"items" => $i,
			"enabled" => (($Enabled
		) ? "" : " disabled=\"\""));
	}

	function getVal($Name, $Param, & $err) {
		global $_PostVals;
		if (isset ($Param[$_PostVals[$Name]]))
			return $_PostVals[$Name];
		if (empty ($_PostVals[$Name]))
			return "";
		$err = text_translate("\"?1?\" ist keine g&uuml;ltige Auswahl.", $_PostVals[$Name]);
		return NULL;
	}
}

class _Input_Multisel extends _Input {
	function getEdit($Name, $Val, & $Enabled, $Param) {
		$i = array ();
		$Val = explode(",", $Val);
		if (is_array($Param))
			foreach ($Param as $k => $c) {
				$sel = (in_array($k, $Val)) ? " checked=\"checked\"" : "";
				$i[] = array (
					"key" => $k,
					"caption" => $c,
					"sel" => $sel
				);
			} else
			$i = array (
				"" => ""
			);
		return array (
			"name" => $Name,
			"items" => $i,
			"enabled" => (($Enabled
		) ? "" : " disabled=\"\""));
	}

	function getVal($Name, $Param, & $err) {
		global $_PostVals;
		$r = array ();
		foreach ($Param as $k => $v)
			if (isset ($_PostVals["{$Name}_$k"]))
				$r[] = $k;
		return implode(",", $r);
	}
}

class _Input_Text extends _Input {}

class _Input_Document extends _Input {}

class _Input_DocumentWrap extends _Input {}

class _Input_Phone extends _Input {
	function getVal($Name, $Param, & $err) {
		global $_PostVals;
		$v = trim($_PostVals[$Name]);
		if (preg_match("/^\+?[\d ]*$/", $v))
			return $v;
		$err = text_translate("\"?1?\" ist keine g&uuml;ltige Telefonnummer. 
		      Es d&uuml;rfen nur die Ziffern 0-9, das Leerzeichen \" \" und am Anfang das Pluszeichen \"+\" enthalten sein.", $v);
		return NULL;
	}
}

class _Input_Captcha extends _Input {
	var $temp_path = "ext/captcha/__TEMP__";

	function getEdit($Name, $Val, & $Enabled, $Param) {
		global $Session;
		require_once ("ext/captcha/class.captcha.php");
		$captcha = new Captcha($Session->id, $this->temp_path);
		$r = parent :: getEdit($Name, $Val, $Enabled, $Param);
		$len = (is_posDigit($Param)) ? $Param : 4;
		if ($len < 1) {
			$len = 1;
		}
		if ($r["url"] = $captcha->get_pic($len))
			return $r;
		else
			trigger_error_text(text_translate("Captcha-Bild konnte nicht erstellt werden!"), E_USER_ERROR);
	}

	function getVal($Name, $Param, & $err) {
		global $Session;
		require_once ("ext/captcha/class.captcha.php");
		$captcha = new Captcha($Session->id, $this->temp_path);
		global $_PostVals;
		$v = trim($_PostVals[$Name]);
		if (!$captcha->verify($v)) {
			$err = text_translate("Es wurde nicht der Text aus dem Bild eingegeben!");
			return false;
		}
		return $v;
	}
}

class _Input_PasswordEdit extends _Input {
	function getEdit($Name, $Val, & $Enabled, $Param) {
		return array (
			"name" => $Name,
			"enabled" => (($Enabled
		) ? "" : " disabled=\"\""));
	}

	function getVal($Name, $Param, & $err) {
		global $_PostVals;
		if ($_PostVals[$Name . "1"] == $_PostVals[$Name . "2"])
			return $_PostVals[$Name . "1"];
		$err = text_translate("Die Wiederholung des Passwords entspricht nicht dem Original.");
		return false;
	}
}

class _Input_PasswordQuery extends _Input {
	function getEdit($Name, $Val, & $Enabled, $Param) {
		return array (
			"name" => $Name,
			"enabled" => (($Enabled
		) ? "" : " disabled=\"\""));
	}
}

class _Input_Hidden extends _Input {}

class _Input_Rights extends _Input_Dropdown {
	function getEdit($Name, $Val, & $Enabled, $Param, $AllowEmpty = false) {
		global $User;
		$this->SubName = "_input_dropdown";
		if (empty ($Val))
			$Val = 0;
		// wenn der user das recht "edit_page_rights" nicht hat, darf er die rechte nicht bearbeiten.    
		if ($Enabled)
			if ($User->hasRight("edit_page_rights") || $User->hasRight("user_admin_rights"))
				$Enabled = true;
			else
				$Enabled = false;
		// wenn der user das recht "user_admin_rights" nicht hat, darf er nur die rechte verwalten die er selbst besitzt
		$rights = ($User->hasRight("user_admin_rights")) ? MysqlReadArea("SELECT `id`,`right` FROM `" . TblPrefix() . "flip_user_right` ORDER BY `right`;", "id") : $User->getRights();
		$r = ($AllowEmpty) ? array (
			0 => "<none>"
		) : array ();
		foreach ($rights as $id => $v) {
			$r[$id] = $v["right"];
			if ($Val === $v["right"])
				$Val = $id;
		}
		//VulkanLAN $Param infinite Abfrage erneuert. Original:
		//if (!$Param["infinite"])
		//	unset ($r[-1]);
		if (!((is_array($Param) && isset($Param['infinite'])) or $Param === "infinite")) {
			unset ($r[-1]);
		}
		if (!isset ($r[$Val]))
			$r[$Val] = "<unknown>";
		return parent :: getEdit($Name, $Val, $Enabled, $r);
	}

	function getVal($Name, $Param, & $err) {
		global $_PostVals, $User;
		$admin = $User->hasRight("user_admin_rights");
		$edit = $User->hasRight("edit_page_rights");
		if (!$admin and !$edit) {
			$err = text_translate("Du bist nicht berechtigt, Rechte zu setzen.");
			return NULL;
		}
		switch ($_PostVals[$Name]) {
			case (-1) :
				//VulkanLAN $Param infinite Abfrage erneuert. Original:
				//if ($Param["infinite"])
				if ((is_array($Param) && isset($Param['infinite'])) or $Param === "infinite")
					return -1;
				$err = text_translate("Das Recht darf nicht \"<infinite>\" sein.");
				return NULL;
			case (0) :
				return 0;
			default :
				if ($edit)
					if ($User->hasRight($_PostVals[$Name]))
						return $_PostVals[$Name];
				if ($admin)
					if (GetRightName($_PostVals[$Name]))
						return $_PostVals[$Name];
		}
		$err = text_translate("Das Recht ist ung&uuml;ltig");
		return NULL;
	}
}

class StringCheck extends _Input {
	function getEdit($Name, $Val, & $Enabled, $Param) {
		$this->SubName = "_input_string";
		return parent :: getEdit($Name, $Val, $Enabled, $Param);
	}

	function getVal($Name, $Param, & $err) {
		global $_PostVals;
		if ($err = $this->invalid($_PostVals[$Name])) {
			return false;
		} else {
			return $_PostVals[$Name];
		}
	}
}

class _Input_Name extends StringCheck {
	function invalid($string) {
		if (preg_match("/^[\d\w]*$/i", $string)) {
			return false;
		}
		return text_translate("Ein Wert vom Typ Name darf nur Zeichen (A-Z), Ziffern (0-9) und den Unterstrich (_) enthalten.");
	}
}

class _Input_Domain extends StringCheck {
	function invalid($string) {
		if (preg_match("/^[a-z0-9\-]*$/i", $string)) {
			return false;
		}
		return text_translate("Ein Wert vom Typ Domain darf nur Zeichen (A-Z), Ziffern (0-9) und den Bindestrich (-) enthalten.");
	}
}

class _Input_Url extends StringCheck {
	function invalid($string) {
		include_once ("mod/mod.url2html.php");
		if (IsValidUrl($string)) {
			return false;
		}
		return text_translate("Ein Wert vom Typ URL muss ein Protokoll und eine Domain (mit g&uuml;ltiger TLD) bzw IP enthalten.");
	}
}

function _GetUploadInfo($Name, & $err) {
	if (!is_array($_FILES[$Name]))
		return false;
	switch ($_FILES[$Name]["error"]) //TODO: zusammengesetzte Errors (z.B. 4+2=6 (Bin&auml;raddition))
		{
		case (UPLOAD_ERR_OK) :
			return $_FILES[$Name];
		case (UPLOAD_ERR_INI_SIZE) :
			$err = "Die hochgeladene Datei &uuml;berschreitet die in der PHP-Config festgelegte maximale Dateigr&ouml;&szlig;e (" . ini_get("upload_max_filesize") . ").";
			return false;
		case (UPLOAD_ERR_FORM_SIZE) :
			return $_FILES[$Name];
		case (UPLOAD_ERR_PARTIAL) :
			$err = "Die Datei wurde nicht vollst&auml;ndig hochgeladen.";
			return false;
		case (UPLOAD_ERR_NO_FILE) :
			return "";
		default :
			return $_FILES[$Name];
	}
}

function GetByte($IniVal) {
	$val = trim($IniVal);
	$last = strtolower($val {
		strlen($val) - 1 });
	switch ($last) {
		case 'g' :
			$val *= 1024;
		case 'm' :
			$val *= 1024;
		case 'k' :
			$val *= 1024;
	}
	return $val;
}

function GetMaxUploadSize($UseDB, & $Reason) {
	$a["max. Postgr&ouml;&szlig;e"] = GetByte(ini_get("post_max_size"));
	$a["max. Dateiuploadgr&ouml;&szlig;e"] = GetByte(ini_get("upload_max_filesize"));
	if ($UseDB)
		$a["max. Datenbankfeldgr&ouml;&szlig;e"] = MysqlGetMaxAllowedPacket();
	$r = 0xFFFFFFFF;
	foreach ($a as $k => $v)
		if ($r > $v) {
			$Reason = $k;
			$r = $v;
		}
	return $r;
}

class _Input_File extends _Input {
	function getEdit($Name, $Val, & $Enabled, $Param) {
		$r = parent :: getEdit($Name, $Val, $Enabled, $Param);
		;
		$UseDB = ($Param == "db") ? true : false;
		$r["maxupload"] = GetMaxUploadSize($UseDB, $r["maxreason"]);
		return $r;
	}

	function getVal($Name, $Param, & $err) {
		return _GetUploadInfo($Name, $err);
	}
}

class _Input_Image extends _Input {
	function getEdit($Name, $Val, & $Enabled, $Param) {
		$r = parent :: getEdit($Name, $Val, $Enabled, $Param);
		$r["maxupload"] = GetMaxUploadSize(true, $r["maxreason"]);
		$url = ini_get("allow_url_fopen");
		$r["allowurl"] = (preg_match("/^(1|true|on)$/i", $url)) ? true : false;
		$r["image"] = $Val;
		return $r;
	}

	function getVal($Name, $Param, & $err) {
		global $_PostVals, $CoreConfig;
		if (!isset ($_PostVals["{$Name}_sel"]))
			return null;
		switch ($_PostVals["{$Name}_sel"]) {
			case ("url") :
				include_once ("mod/mod.image.php");
				$tmp = tempnam(realpath($CoreConfig["image_tmp"]), "download");
				$f = $_PostVals["{$Name}_url"];
				$r = copy($f, $tmp);
				if (!$r or !is_file($tmp))
					$err = "Die Datei konnte von \"$f\" nicht herunter geladen werden.";
				$dat = false;
				if (empty ($err))
					if (IsValidImage($tmp))
						$dat = file_get_contents($tmp);
				if (is_file($tmp));
				unlink($tmp);
				if (empty ($err) and !empty ($dat))
					return $dat;
				$err = "Das Bild ist besch&auml;digt oder hat ein ung&uuml;ltiges Format";
				return false;
			case ("file") :
				include_once ("mod/mod.image.php");
				$u = _GetUploadInfo("{$Name}_file", $err);
				if (!is_array($u))
					return NULL;

				if (IsValidImage($u["tmp_name"]))
					return file_get_contents($u["tmp_name"]);
				$err = "Das Bild ist besch&auml;digt oder hat ein ung&uuml;ltiges Format";
				return false;
			case ("del") :
				return "";
			default :
				return NULL;
		}
	}
}

class _Input_TableDropdown extends _Input_Dropdown {
	function getEdit($Name, $Val, & $Enabled, $Param) {
		include_once ("inc/inc.table.php");
		$r = TableGetTableValues($Param);
		$t = parent :: getEdit($Name, $Val, $Enabled, $r);
		$t["canedit"] = TableCanEdit($Param);
		$t["table"] = $Param;
		return $t;
	}

	function getVal($Name, $Param, & $err) {
		include_once ("inc/inc.table.php");
		return parent :: getVal($Name, TableGetTableValues($Param), $err);
	}
}

class _Input_TableMultisel extends _Input_Multisel {
	function getEdit($Name, $Val, & $Enabled, $Param) {
		include_once ("inc/inc.table.php");
		$r = TableGetTableValues($Param);
		$t = parent :: getEdit($Name, $Val, $Enabled, $r);
		$t["canedit"] = TableCanEdit($Param);
		$t["table"] = $Param;
		return $t;
	}

	function getVal($Name, $Param, & $err) {
		include_once ("inc/inc.table.php");
		global $_PostVals, $User;
		return parent :: getVal($Name, TableGetTableValues($Param), $err);
	}
}

class _Input_UserFromGroup extends _Input_Dropdown {
	function getEdit($Name, $Val, & $Enabled, $Param, $AllowEmpty = false) {
		$this->SubName = "_input_dropdown";
		$g = new Group($Param);
		$r = $g->getChilds();
		if ($AllowEmpty)
			$r[0] = "<nobody>";
		uasort($r, 'strnatcasecmp');
		return parent :: getEdit($Name, $Val, $Enabled, $r);
	}

	function getVal($Name, $Param, & $err) {
		global $_PostVals, $User;
		if (empty ($_PostVals[$Name]))
			return "0";
		$u = new User($_PostVals[$Name]);
		if ($u->isChildOf($Param))
			return $_PostVals[$Name];
		$err = "Die User-ID \"{$_PostVals[$Name]}\" ist ung&uuml;ltig.";
		return false;
	}
}

class _Input_Sex extends _Input_Dropdown {
	function getEdit($Name, $Val, & $Enabled, $Param, $AllowEmpty = true) {
		$this->SubName = "_input_dropdown";
		if ($AllowEmpty)
		$r[0] = "<unbekannt>";
		$r["n"] = "none";
		$r["m"] = "m&auml;nnlich";
		$r["f"] = "weiblich";
		return parent :: getEdit($Name, $Val, $Enabled, $r);
	}

	function getVal($Name, $Param, & $err) {
		global $_PostVals, $User;
		$r = $_PostVals[$Name];
		if (in_array($r, array (
				"m",
				"f",
				"n",
				"0"
			)))
			return $r;
		$err = "Das Geschlecht ist ung&uuml;ltig.";
		return false;
	}
}

class _Input_MessageType extends _Input_Dropdown {
	function getEdit($Name, $Val, & $Enabled, $Param) {
		include_once ("mod/mod.sendmessage.php");
		$this->SubName = "_input_dropdown";
		$r = GetMessageTypes(false);
		return parent :: getEdit($Name, $Val, $Enabled, $r);
	}

	function getVal($Name, $Param, & $err) {
		global $_PostVals, $User;
		include_once ("mod/mod.sendmessage.php");
		$r = GetMessageTypes(false);
		if (isset ($r[$_PostVals[$Name]]))
			return $_PostVals[$Name];
		$err = "Der Nachrichten-Typ \"{$_PostVals[$Name]}\" ist ung&uuml;ltig.";
		return false;
	}
}

class _Input_Date extends _Input {
	function getEdit($Name, $Val, & $Enabled, $Param) {
		if (empty ($Val))
			$Val = "";
		elseif (preg_match("/^\d+$/", $Val)) $Val = date("d.m.y H:i", $Val);
		return array (
			"name" => $Name,
			"val" => $Val,
			"enabled" => (($Enabled
		) ? "" : " disabled=\"\""));
	}

	function _getDate($str, & $r) {
		$a = array ();
		if (preg_match("/^(\d{1,2})\.(\d{1,2})\.(\d{1,4})$/", $str, $a))
			list (, $r["day"], $r["month"], $r["year"]) = $a;
		elseif (preg_match("/^(\d{2,4})\.(\d{1,2})\.(\d{1,2})$/", $str, $a)) list (, $r["year"], $r["month"], $r["day"]) = $a; //1975.02.24
		elseif (preg_match("/^(\d{1,4})\-(\d{1,2})\-(\d{1,2})$/", $str, $a)) list (, $r["year"], $r["month"], $r["day"]) = $a;
		else
			return false;

		if ($r["year"] <= 70)
			$r["year"] += 2000;
		elseif (($r["year"] > 70) and ($r["year"] < 100)) $r["year"] += 1900;

		if (checkdate($r["month"], $r["day"], $r["year"]))
			return true;
		list ($r["day"], $r["month"], $r["year"]) = array (
			0,
			0,
			0
		);
		return false;
	}

	function _getTime($str, & $r) {
		$a = array ();
		if (preg_match("/^(\d{1,2}):(\d{1,2})$/", $str, $a))
			list (, $r["hour"], $r["minute"]) = $a;
		elseif (preg_match("/^(\d{1,2})(\d{2})$/", $str, $a)) list (, $r["hour"], $r["minute"]) = $a;
		elseif (preg_match("/^(\d{1,2})h$/", $str, $a)) list ($r["hour"], $r["minute"]) = array (
			$a[1],
			0
		);
		else
			list ($r["hour"], $r["minute"]) = array (
				0,
				0
			);
	}

	function getVal($Name, $Param, & $err) {
		global $_PostVals;
		$t = $_PostVals[$Name];
		if (empty ($t))
			return 0;
		if (preg_match("/^\d+$/", $t))
			return $t; // es ist ein unix-timestamp
		$t = explode(" ", $t);
		$ok = false;
		$r = array ();
		foreach ($t as $date) {
			if ($this->_getDate($date, $r))
				$ok = true;
			$this->_getTime($date, $r);
		}
		if ($ok) {
			if (($r["year"] < 1970) and (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')) {
				$x = @ mktime($r["hour"], $r["minute"], 0, $r["month"], $r["day"], $r["year"] + 56);
				$x -= mktime(0, 0, 0, 1, 1, 1970 + 56);
			} else
				$x = @ mktime($r["hour"], $r["minute"], 0, $r["month"], $r["day"], $r["year"]);
			return $x;
		}
		$err = "Die Angegebene Zeit ist ung&uuml;ltig. g&uuml;ltige Zeiten sind z.B. \"24.12.2004 18:00\" oder \"2004-12-24 1800\"";
		return false;
	}
}

class _Input_EMail extends _Input {
	function getVal($Name, $Param, & $err) {
		global $_PostVals;
		$v = $_PostVals[$Name];
		if (empty ($v))
			return "";
		if (IsValidEmail($v))
			return $v;
		$err = "\"$v\" ist keine g&uuml;ltige Email-Adresse";
		return NULL;
	}
}

class _Input_Subjects extends _Input_Dropdown {
	function getEdit($Name, $Val, & $Enabled, $Param, $AllowEmpty = false) {
		$this->SubName = "_input_dropdown";
		$w = empty ($Param) ? "" : " WHERE (`type`= '" . addslashes($Param) . "')";
		$r = MysqlReadCol("SELECT CONCAT('[',`type`,'] ',`name`) as `name`,`id` FROM `" . TblPrefix() . "flip_user_subject`$w ORDER BY `name`", "name", "id");
		if ($AllowEmpty)
			$r[0] = "<nobody>";
		//    uasort($r,'strnatcasecmp');
		return parent :: getEdit($Name, $Val, $Enabled, $r);
	}

	function getVal($Name, $Param, & $err) {
		global $_PostVals, $User;
		if (empty ($_PostVals[$Name]))
			return "0";
		if (SubjectExists($_PostVals[$Name]))
			return $_PostVals[$Name];
		$err = "Die User-ID \"{$_PostVals[$Name]}\" ist ung&uuml;ltig.";
		return false;
	}
}

class _Input_Content extends _Input_Dropdown {
	function getEdit($Name, $Val, & $Enabled, $Param, $AllowEmpty = false) {
		$this->SubName = "_input_dropdown";
		$t = ($Param == "images") ? "flip_content_image" : "flip_content_text";
		$r = MysqlReadCol("SELECT `name`,`id` FROM `" . TblPrefix() . "$t` ORDER BY `name`", "name", "id");
		if ($AllowEmpty)
			$r[0] = "<leer>";
		//    uasort($r,'strnatcasecmp');
		return parent :: getEdit($Name, $Val, $Enabled, $r);
	}

	function getVal($Name, $Param, & $err) {
		global $_PostVals, $User;
		if (empty ($_PostVals[$Name]))
			return "0";
		$t = ($Param == "images") ? "flip_content_image" : TblPrefix() . "flip_content_text";
		if (MysqlReadFieldByID($t, "name", $_PostVals[$Name], true))
			return $_PostVals[$Name];
		$err = "Die Content-ID \"{$_PostVals[$Name]}\" ist ung&uuml;ltig.";
		return false;
	}
}

class _Input_Country extends _Input_Dropdown {
	function getEdit($Name, $Val, & $Enabled, $Param, $AllowEmpty = false) {
		$this->SubName = "_input_dropdown";
		
		$r = array();
		$r = MysqlReadCol('SELECT * FROM `' . TblPrefix() . 'flip_i18n_countrycodes`', 'printable_name', 'name');
		
		return parent :: getEdit($Name, $Val, $Enabled, $r);
	}

	function getVal($Name, $Param, & $err) {
		global $_PostVals, $User;
		
		if (empty ($_PostVals[$Name])) {
			return 'Germany';
		}
		
		$sqlName = escape_sqlData($_PostVals[$Name]);
		$country = MysqlReadRow('SELECT * FROM `' . TblPrefix() . 'flip_i18n_countrycodes` WHERE `name` = ' . $sqlName, true);
		
		if($country === false) {
			trigger_error_text(text_translate('Das Land ?1? gibt es nicht!', $sqlName), E_USER_WARNING);
			return false;
		}
		
		return $_PostVals[$Name];
	}	
}

class _Input_Template extends _Input_Dropdown {
	function getEdit($Name, $Val, & $Enabled, $Param, $AllowEmpty = false) {
		$this->SubName = "_input_dropdown";
		$dir = ConfigGet("template_dir");
		$h = opendir($dir);
		$r = array ();
		while ($d = readdir($h)) {
			if (is_file($dir . $d . "/inc.page.tpl")) {
				$r[$d] = $d;
			}
		}
		closedir($h);
		if ($AllowEmpty)
			$r[0] = "<voreinstellung>";
		uasort($r, 'strnatcasecmp');
		return parent :: getEdit($Name, $Val, $Enabled, $r);
	}

	function getVal($Name, $Param, & $err) {
		global $_PostVals, $User;
		if (empty ($_PostVals[$Name]))
			return ConfigGet("template_customname", NULL, "", false);
		$dir = ConfigGet("template_dir");
		if (is_dir($dir . $_PostVals[$Name]))
			return $_PostVals[$Name];
		$err = "Es existiert kein Template mit dem Namen \"{$_PostVals[$Name]}\"";
		return false;
	}
}

class _Input_DropdownEdit extends _Input {
	function getEdit($Name, $Val, & $Enabled, $Param) {
		$i = array (
			array (
				"key" => "",
				"caption" => text_translate("---Anderes--->"
			),
			"sel" => ((isset ($Param[$Val]
		)) ? "" : "selected")));
		if (is_array($Param))
			foreach ($Param as $k => $c)
				$i[] = array (
					"key" => $k,
					"caption" => $c,
					"sel" => (($k == $Val
				) ? "selected" : ""));
		else
			$i = array (
				"" => ""
			);
		return array (
			"name" => $Name,
			"items" => $i,
			"enabled" => (($Enabled
		) ? "" : " disabled=\"\""), "val" => $Val);
	}

	function getVal($Name, $Param, & $err) {
		global $_PostVals;
		if (empty ($_PostVals["drop_$Name"]))
			return $_PostVals["edit_$Name"];
		else
			return $_PostVals["drop_$Name"];
	}
}

_InitForms();
?>