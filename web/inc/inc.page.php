<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: inc.page.php 1702 2019-01-09 09:01:12Z loom $
 * @copyright ? 2001-2007 The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package inc
 **/

/** Die Datei nur einmal includen */
if (defined("INC.PAGE.PHP"))
	return 0;
define("INC.PAGE.PHP", 1);

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("mod/mod.template.php");

//$PageExists = false;

function RunPage($PageName) {
	global $PageExists;
	// stranger workaround f&uuml;r php, welches innerhalb eines error-hooks globale variablen zu vergessen scheint.
	// problem identifiziert als PHP-Bug #29562 :S
	if (function_exists("MainPageExists"))
		return $PageExists = true;
	function MainPageExists() {
		return 0;
	}

	$PageExists = false;
	if (!class_exists($PageName))
		trigger_error_text(text_translate("Die Page mit dem Namen \"?1?\" existiert nicht.", $PageName), E_USER_ERROR);
	$AtomPage = new $PageName ();
	$PageExists = true;
	$AtomPage->run();

}

class Page {
	var $TplSub;
	var $TplFile;
	var $Frame = "";
	var $FrameVars = array ();
	var $Action = "";
	var $ActionVars = array ();
	var $Submit = "";
	var $SubmitVars = array ();
	var $Confirmation = "";
	var $ConfirmationUri = "";
	var $_ShowFrame = true;
	var $Caption = "";
	var $ShowContentCell = true;
	var $ContentType = "content"; // [content|error|notice|hidden]
	var $ShowMenu = true;
	var $NextPage = "";
	var $SubmitMessage = "";
	var $RefreshTimer = 0;

	//php 7 public function __construct()
	//php 5 original function Page()
	function __construct() {
		global $PageExists;
		if (!$PageExists) {
			if (!empty ($_GET["frame"]))
				$this->Frame = escapeHtml($_GET["frame"]);
			else
				$this->Frame = "default";
			$this->FrameVars = $_GET;

			if (isset ($_POST["confirmation"])) {
				$confMsg = base64_decode($_POST["confirmation"]);
				$this->Confirmation = escapeHtml($confMsg);
				$this->ConfirmationUri = isset ($_POST["confirmation_uri"]) ? htmlspecialchars($_POST["confirmation_uri"]) : "";
			}
			
			elseif (isset ($_GET["confirmation"])) {
				// GET's do not use Base64 (yet)
				$confMsg = ($_GET["confirmation"]);
				$this->Confirmation = escapeHtml($confMsg);
				$this->ConfirmationUri = isset ($_GET["confirmation_uri"]) ? htmlspecialchars($_GET["confirmation_uri"]) : "";
			}

			if (isset ($_POST["submit"]))
				$this->pareseSubmit($_POST["submit"]);

			if (isset ($_POST["action"])) {
				$this->Action = escapeHtml($_POST["action"]);
				$this->ActionVars = $_POST;
			}
			elseif (isset ($_GET["action"])) {
				$this->Action = escapeHtml($_GET["action"]);
				$this->ActionVars = $this->FrameVars;
			} else
				$this->ActionVars = $_POST;
				
			// Refresh timer settings
			if(isset($_GET['refreshevery']) && is_numeric($_GET['refreshevery'])) {
				$this->RefreshTimer = $_GET['refreshevery'];
			} else if(isset($_POST['refreshevery']) && is_numeric($_POST['refreshevery'])) {
				$this->RefreshTimer = $_POST['refreshevery'];
			}
		}
	}

	function pareseSubmit($data) {
		list ($s, $m) = explode("@", stripslashes($data));
		$m = strtr($m, " ", "+"); //einige Clients senden die Daten URL-dekodiert (z.B. JMeter Proxy), einige nicht (z.B. Firefox 2.0)
		if (empty ($m))
			return trigger_error_text(text_translate("Die Submit-Daten sind besch&auml;digt oder nicht vorhanden."), E_USER_WARNING);

		include_once ("inc/inc.form.php");
		$f = new Form();
		if ($v = $f->getVals($m)) {
			$this->Submit = escapeHtml($s);
			$this->SubmitVars = $v;
		}
	}

	function _printTemplate($tpl) {
		global $Errors, $Session, $User, $MysqlPerformanceQueryCount;

		$pmsg = ConfigGet("page_permanent_message");
		if (!empty ($pmsg))
			trigger_error($pmsg);

		static $DoCompress = false;
		
		if (!$DoCompress && !headers_sent()) { // wenn nicht bereits komprimiert wird, config lesen und evtl. komprimieren
			$DoCompress = ConfigGet("page_compress_output");
			if ($DoCompress == "Y") {
				ob_start("ob_gzhandler");
			}
		}

		$cfg = array ();
		$cfg['metarefresh'] = ($this->RefreshTimer == 0) ? '' : (
				'<meta http-equiv="refresh" content="' 
					. $this->RefreshTimer 
					. '; URL=' . $_SERVER['REQUEST_URI'] . '">'
		);
		$cfg["title"] = ConfigGet("page_title");
		$cfg["errorsinline"] = ConfigGet("page_errorsinline");
		$cfg["copyright"] = "Code: VFLIP " . FLIP_VERSION_LONG . " Copyright &copy; 2001-" . date('Y') .
			"<a href=\"https://gitlab.com/VulkanLAN/VFLIP/\" target=\"_blank\" class=\"copyright\">The VFLIP Project Team</a>";
		$cfg['flipversion'] = FLIP_VERSION_LONG;
		$cfg["caption"] = $this->Caption;
		$cfg["frame"] = $this->Frame;
		$cfg["action"] = $this->Action;
		$cfg["page"] = $tpl;
		$cfg["xmltag"] = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
		$cfg["isloggedin"] = $User->isLoggedIn();
		//edit VulkanLAN dsgvo
		$rawProps = $User->getProperties();
		$cfg["dsgvo"] = ( array_key_exists("dsgvo", $rawProps) && $rawProps["dsgvo"] == "Y") ? "Y":"N";
		$cfg["dsgvocheck"] = (($User->isLoggedIn()) and ($rawProps["dsgvo"] != "Y")) ? "Y":"N";
		
		$cfg["user"] = $User->name;
		$cfg["userimg"] = ($rawProps["user_image"]);
		//    $cfg["showcontentcell"] = $this->ShowContentCell;
		$cfg["contenttype"] = ($this->ShowContentCell) ? $this->ContentType : "hidden";
		$cfg["unread_messages"] = $User->getProperty("unread_messages");
		$cfg["loginident"] = isset ($_POST["ident"]) ? $_POST["ident"] : "";

		if (isset ($this->FrameVars["nomenu"]) && $this->FrameVars["nomenu"]) {
			//Content mit HTML-<body>-Rahmen und &uuml;berschrift
			$t = new Template("inc.page.tpl", $cfg, "head");
			$t->out();
			echo "<h1>" . $this->Caption . "</h1>\n";
			$tpl->out();
			$t = new Template("inc.page.tpl", $cfg, "foot");
			$t->out();
			return true;
		}
		elseif (!$this->ShowMenu) {
			//nur Content
			$tpl->out();
			return true;
		}
		
		$rep = '';
		$t = new Template("inc.page.tpl", $cfg, "page");
		if ($cfg["errorsinline"] != "N") {
			$start_time = MicroSeconds();
			$s = $t->get();
			$stop_time = MicroSeconds();
			
			$creation_time = $stop_time - $start_time;
			
			if(is_numeric($creation_time)) {
				$creation_time = $creation_time * 1000;
				$creation_time = round($creation_time, 0);
			} else {
				$creation_time = 'N/A';
			}
			
			if (count($Errors[0]) > 0) {
				$err = $notice = array ();
				foreach ($Errors[0] as $e) {
					if ($e->Type == "notice")
						$notice[] = $e->getMessage(false);
					else
						$err[] = $e->getMessage($User->hasRight("view_debug_messages"));
				}
				$e = new Template("inc.page.tpl", array (
					"errors" => $err,
					"notices" => $notice
				), "errorcell");
				$rep = $e->get();

			} else {
				$rep = "<span style=\"display:none;\">FreeLanpartyInterIntranetSystem v" . 
					   FLIP_VERSION_LONG . 
					   "</span>";
			}
			
			echo strtr(
				$s, 
				array(
					"<!-- ERRORCELL -->" => $rep,
					"<!-- {creation_time} -->" => $creation_time,
					"<!-- {query_count} -->" => $MysqlPerformanceQueryCount
				)
			);
		} else {
			$t->out();
			if (count($Errors[0]) > 0) {
				echo "<pre>\n";
				print_r($Errors[0]);
				echo "</pre>\n";
			}
		}
		
		if ($DoCompress)
			ob_end_flush();
		//DisplayErrors();
	}

	function _printResource($res) {
		switch ($t = get_resource_type($res)) {
			case ("gd") :
				header("Content-type: image/png");
				imagepng($res);
				break;
			default :
				trigger_error_text(text_translate("Der Resourcentyp \"?1?\" ist unbekannt und kann deswegen nicht ausgegeben werden.", $t), E_USER_ERROR);
		}
		//
	}

	function run() {
		if (!empty ($this->Confirmation))
			if ($this->execConfirmation($this->Confirmation))
				return false;

		if (!empty ($this->Action)) {
			$a = "action{$this->Action}";
			if (method_exists($this, $a)) {
				$r = $this-> $a ($this->ActionVars);
				if (is_string($r))
					$this->message(text_translate("Nachricht"), $r);
				if (!empty ($this->NextPage)) {
					Redirect($this->NextPage);
					return false;
				}
			} else
				trigger_error_text(text_translate("Die Aktion \"?1?\" existiert nicht und wurde deshalb nicht ausgef&uuml;hrt.", $this->Action), E_USER_WARNING);
		}

		if (!empty ($this->Submit))
			if ($this->execSubmit() === 1)
				return false;

		if (!$this->_ShowFrame)
			return false;

		$tpl = $this->loadFrame(basename($_SERVER["PHP_SELF"], ".php") . ".tpl", $this->Frame, $this->FrameVars, $this->ActionVars);
		if (!$this->_ShowFrame)
			return false;

		if (is_a($tpl, "template"))
			$this->_printTemplate($tpl);
		elseif (is_a($tpl, "XMLDocument")) $tpl->out();
		elseif (is_a($tpl, "RPCData")) $tpl->out();
		elseif (is_a($tpl, "_AbstractImage")) $tpl->printData();
		elseif (is_resource($tpl)) $this->_printResource($tpl);
	}

	function loadFrame($TplFile, $Frame, $Param1 = array (), $Param2 = array (), $ErrorType = E_USER_ERROR) {
		$this->TplSub = $this->Frame = $Frame;
		$this->TplFile = $TplFile;

		$f = "frame" . $this->Frame;
		if (!method_exists($this, $f))
			return trigger_error_text(text_translate("Der Frame \"?1?\" existiert nicht und kann deshalb nicht angezeigt werden.", $this->Frame) . "|" . text_translate("Klasse") . ": " . get_class($this) . " TPLFile: {$this->TplFile}", $ErrorType);

		$v = $this-> $f ($Param1, $Param2);
		if (!$this->_ShowFrame)
			return false;
		
		if (is_array($v)) {
			$v = array_merge(array('http_referer'=>GetHttpReferer(false)), $v);
			return new Template($this->TplFile, $v, $this->TplSub);
		} elseif (is_object($v) or is_resource($v)) return $v;
		elseif ($v === true) return;
		elseif (empty ($v)) trigger_error_text(text_translate("Der Frame \"?1?\" hat kein Ergebnis zur&uuml;ckgegeben und kann deswegen nicht angezeigt werden.", $this->Frame), $ErrorType);
		else
			return new Template("inc.page.content.tpl", array (
				"text" => strval($v
			)), "text");
	}

	function execConfirmation($msg) {
		unset ($this->ActionVars["confirmation"]);
		unset ($this->ActionVars["confirmation_uri"]);
		$this->ContentType = "notice";
		$this->Caption = text_translate("Achtung!");
		$r = array (
			"get" => ($this->ConfirmationUri
		) ? $this->ConfirmationUri : $_SERVER['HTTP_REFERER'], "msg" => $msg, "posts" => $this->ActionVars);
		$this->_printTemplate(new Template("inc.page.content.tpl", $r, "confirmation"));
		return true;
	}

	function execSubmit() {
		$s = "submit{$this->Submit}";
		$this->NextPage = $this->ActionVars["backlink"];
		$this->SubmitMessage = text_translate("Die Daten wurden gespeichert.");
		if (!method_exists($this, $s))
			return trigger_error_text(text_translate("Die Submit-Funktion \"?1?\" existiert nicht und wurde deshalb nicht ausgef&uuml;hrt.", $this->Submit), E_USER_WARNING);
		$r = $this-> $s ($this->SubmitVars);
		if (!$r)
			return false;
		elseif (is_string($r) and !is_numeric($r)) $this->message(text_translate("Nachricht"), escapeHtml($r), $this->NextPage);
		if (!empty ($this->SubmitMessage))
			trigger_error_text($this->SubmitMessage);
		if (empty ($this->NextPage))
			return false;
		return Redirect($this->NextPage);
	}

	function message($aCaption, $aMsg, $aBackURL = "") {
		$this->_ShowFrame = false;
		$this->Caption = $aCaption;
		$this->_printTemplate(new Template("inc.page.content.tpl", array (
			"text" => $aMsg,
			"backurl" => $aBackURL
		), "msg"));
	}

	function frameDefault($GetVars, $PostVars) {
		trigger_error_text(text_translate("Diese Seite kann nicht ohne den Parameter \"frame\" aufgerufen werden."), E_USER_ERROR);
	}

	function saveAs($fileName, $contentType = "plain/text") {
		header("Content-Type: $contentType");
		header("Content-Disposition: attachment; filename=$fileName");
	}
}

function FatalMessage($msg) {
	global $User;

	$p = new Page();
	$p->ContentType = "error";
	$p->Caption = text_translate("Oops...");	
	$err = $msg->getMessage($User->hasRight("view_debug_messages"));
	$ref = (isset ($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : (isset ($_SERVER['SCRIPT_NAME'])) ? $_SERVER['SCRIPT_NAME'] : "index.php";
	$t = new Template("inc.page.content.tpl", array (
		"text" => $err,
		"backurl" => $ref
	), "msg");
	$p->_printTemplate($t);
}

function _ErrorTpl($Msg) {
	trigger_error_text($Msg, E_USER_WARNING);
	return new Template("inc.page.content.tpl", array (
		"text" => "Fehler",
		"backurl" => $_SERVER['HTTP_REFERER']
	), "msg");
}

function PageLoadFrame($FrameURL) {
	list ($file, $query) = explode("?", $FrameURL, 2);
	$file = basename($file);
	$a = array ();
	if (!preg_match("/^([\w\d]+)\.php$/", $file, $a))
		return _ErrorTpl(text_translate("Eine Paket-Datei hat einen ung&uuml;ltigen Dateinamen.") . "|" . text_translate("Datei") . ": $file FrameURL: $FrameURL");

	if (!is_file($file))
		return _ErrorTpl(text_translate("Eine Paket-Datei existiert nicht.") . "|" . text_translate("Datei") . ": $file FrameURL: $FrameURL");
		
	# bug in php 4.22: SCRIPT_NAME ist nicht in get_includet_files() :S
	if (basename($_SERVER["SCRIPT_NAME"]) != basename($file))
		include_once ($file);
		
	$class = "$a[1]Page";
	if (!class_exists($class))
		return _ErrorTpl(text_translate("Eine Paket-Klasse existiert nicht.") . "|" . text_translate("Klasse") . ": $class FrameURL: $FrameURL");
	$p = new $class ();
	$param = array ();
	parse_str($query, $param);
	$frame = (empty ($param["frame"])) ? "default" : $param["frame"];
	//old_Try();
	$tpl = $p->loadFrame("$a[1].tpl", $frame, $param, array (), E_USER_WARNING);
	//old_Except(false);
	if (is_a($tpl, "template"))
		return $tpl;
	if (is_bool($tpl) or empty ($tpl))
		return false;
	return _ErrorTpl(text_translate("Der R&uuml;ckgabewert eines Frames ist ung&uuml;ltig.") . "|FrameURL: $FrameURL");
}

SetFatalErrorCallback("FatalMessage");
?>
