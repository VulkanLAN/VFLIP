<?php

/**
 * inc.image.php
 * @author Moritz Eysholdt
 * @version $Id: inc.image.php 1447 2007-08-10 11:09:21Z loom $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package inc
 **/

/** FLIP-Kern */
require_once ("core/core.php");
/** Bildverarbeitung */
require_once ("mod/mod.image.php");

function LoadImage($Name, $Param = array (), $Error = "") {
	global $User;
	if (empty ($Error)) {
		$i = new DataImage($Name);
		$dat = $i->getInfo(false);
	} else
		$dat = array (
			"error" => $Error,
			"name" => $Name
		);

	if (empty ($dat["link"])) {
		$dat["editable"] = ($User->hasRight("admin") and preg_match("/^[0-9a-z_]+$/i", $dat["name"])) ? true : false;
		trigger_error_text("Ein Bild konnte nicht geladen werden: $dat[error]|name: $dat[name]", E_USER_WARNING);
	} else {
		$User->requireRight($dat["view_right"]);

		if (isset ($Param["width"]))
			unset ($Param["width"]);
		if (isset ($Param["height"]))
			unset ($Param["height"]);
		if (isset ($Param["name"]))
			unset ($Param["name"]);
		if (isset ($Param["alt"])) {
			$dat["caption"] = $Param["alt"];
			unset ($Param["alt"]);
		}
		$dat["param"] = $Param;
	}
	return new Template("inc.image.tpl", $dat);
}
?>