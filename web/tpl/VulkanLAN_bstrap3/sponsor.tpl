{! $Id: sponsor.tpl 1388 2007-03-27 10:46:25Z loom $ }

{#SUB default}
<div class="panel panel-default">
	<div class="panel-heading">	
		<div class="row">
		{#TPL $text}
		</div>
	</div>
	<div class="panel-body">
	<div class="row">
	<div class="col-xs-1"></div>
	<div class="col-xs-8">
		{#FOREACH $rows}
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="row"><h3>{%name}</h3></div>
			</div>
			<div class="panel-body">
				{#IF is_array($images)}
					{#FOREACH $images $i}
						<div class="row">
						<div align="center">  
						{#IF !empty($i.link)}<a href="sponsor.php?frame=click&amp;id={$i.id}&amp;location={$location}" target="_blank">
						{#END}
						{#IF empty($i.html)}
						{#DBIMAGE flip_sponsor_ads image $i.id $i.image_mtime border=0 height=250 sizeonlyforswf=1}
						{#ELSE}{$i.html}{#END}
						{#IF !empty($i.link)}</a>{#END}
						</div>
						</div>
					{#END}
				{#END}
				<div class="row">
				<div class="col-xs-12">
					<p>{$description}</p>
				</div>
				</div>
			</div>
		</div>
		{#END}
	</div>
	<div class="col-xs-3"></div>
	</div>
	</div>
</div>
{#END}

{#SUB smallbuttons}
{#IF empty($images)}
  <p>keine <a href="sponsor.php?frame=imagelist&amp;type=button">Buttons</a></p>
{#ELSE}
  <table>
    {#FOREACH $images $i}
      <tr><td>{#IF !empty($i.link)}<a href="sponsor.php?frame=click&amp;id={$i.id}&amp;location={$location}" target="_blank">{#END}
	  {#IF empty($i.html)}{#DBIMAGE flip_sponsor_ads image $i.id $i.image_mtime border=0 width=88 height=31 sizeonlyforswf=1}
	  {#ELSE}{$i.html}{#END}{#IF !empty($i.link)}</a>{#END}</td></tr>
    {#END}
  </table>
{#END}
{#END}

{#SUB header}
<h3>
  <a{#WHEN "$menu=='list'" " class=\"important\""} href="sponsor.php?frame=list">Sponsoren</a> &middot;
  <a{#WHEN "$menu=='banner'" " class=\"important\""} href="sponsor.php?frame=imagelist&amp;type=banner">Banner</a> &middot;
  <a{#WHEN "$menu=='button'" " class=\"important\""} href="sponsor.php?frame=imagelist&amp;type=button">Buttons</a> &middot;
  <a{#WHEN "$menu=='log'" " class=\"important\""} href="sponsor.php?frame=viewlog">Log</a>
</h3>
{#END}

{#SUB list}
  {#VAR sub=header}
  {#TPL $text}
  <br />
  <br />
  {#TABLE $rows}
    {#IF $edit}<a href="sponsor.php?frame=editsponsor">add</a>{#END}
    {#COL Pub $public}<b>{#IF $public}<span style="color:green">Ja</span>{#ELSE}<span style="color:gray">Nein</span>{#END}</b>
    {#COL Prio $priority align="center"}
    {#COL Name %name}<a href="sponsor.php?frame=viewsponsor&amp;id={$id}"><b>{%name}</b></a>
    {#COL Homepage $homepage}<a href="{$homepage}">{$homepage}</a>
    {#COL Status $status}{#TABLEDISPLAY sponsor_status $status}
    {#COL "Banner<br />Buttons" align="center"}
      <b>
        <a href="sponsor.php?frame=imagelist&amp;type=banner&amp;sponsor_id={$id}">{$banner}</a>
        <a href="sponsor.php?frame=imagelist&amp;type=button&amp;sponsor_id={$id}">{$buttons}</a>
      </b>
    {#COL Bearbeitet}{%user}<br />{#DATE $time}
    {#FRAME edit editsponsor condition=$edit}
    {#OPTION L&ouml;schen delete condition=$edit confirmation="Soll der Sponsor/die Sponsoren wirklich gel&ouml;scht werden? Die zugeh&ouml;rigen Banner und Buttons werden mit gel&ouml;scht!"}
  {#END}
{#END}

{#SUB viewsponsor}
  <a href="sponsor.php?frame=list"><h3>&Uuml;bersicht</h3></a>
  <table width="80%">
    <tr>
      <td>
        <table>   
          <tr><td colspan="2" align="center"><b>&ouml;ffentliche Daten</b></td></tr>
          <tr><td align="right">Name:</td><td>{$name}</td></tr>
          <tr><td align="right">Homepage:</td><td>{$homepage}</td></tr>
          <tr><td align="right">Banner / Buttons:</td><td><a href="sponsor.php?frame=imagelist&amp;type=banner&amp;sponsor_id={$id}">{$images.banner}</a> / <a href="sponsor.php?frame=imagelist&amp;type=button&amp;sponsor_id={$id}">{$images.button}</a></td></tr>
          {#IF !empty($description)}
          <tr><td align="center" colspan="2">{$description}</td></tr>
          {#END}
        </table>
      </td>
      <td>
        <table>   
          <tr><td colspan="2" align="center"><b>interne Daten</b></td></tr>
          <tr><td align="right">Status:</td><td>{#WHEN $public "&ouml;ffentlich, "}{#TABLEDISPLAY sponsor_status $status}, Priorit&auml;t: {#TABLEDISPLAY sponsor_priority $priority}</td></tr>
          <tr><td align="right">Email:</td><td>{$email}</td></tr>
          <tr><td align="right">Ansprechpartner:</td><td>{$contact}</td></tr>
          <tr><td align="right">Anschrift:</td><td>{$street}{#WHEN $city ", $city"}</td></tr>
          <tr><td align="right">Telefon:</td><td>{$phone}</td></tr>
          <tr><td colspan="2" align="right"><i>zuletzt {#IF $edit}<a href="sponsor.php?frame=editsponsor&amp;id={$id}">bearbeitet</a>{#ELSE}bearbeitet{#END} von {%user} am {#DATE $time}</i></td></tr>
        </table>     
      </td>
    </tr>     
    {#IF !empty($comment)}
    <tr><td colspan="2">&nbsp;</td></tr>
    <tr><td colspan="2" align="center"><b>interne Bemerkung</b></td></tr>
    <tr><td colspan="2" align="center"><p align="left">{$comment}</p></td></td></tr>
    {#END}
  </table>
{#END}

{#SUB editsponsor}
{#FORM sponsor}
  {#INPUT id hidden $id}
    <table>   
     <tr><td colspan="2" align="center"><b>Status</b></td></tr>
     <tr><td align="right">ist &ouml;ffentlich:</td><td>{#INPUT public checkbox $public} (f&uuml;hrt diesen Sponsor in der &ouml;ffentlich zug&auml;nglichen Sponsorenliste auf)</td></tr>
     <tr><td align="right">Priorit&auml;t:</td><td>{#INPUT priority tabledropdown $priority param=sponsor_priority}</td></tr>
     <tr><td align="right">Status:</td><td>{#INPUT status tabledropdown $status param=sponsor_status}</td></tr>
     <tr><td colspan="2" align="center">&nbsp;</td></tr>
     
     <tr><td colspan="2" align="center"><b>&ouml;ffentliche Daten</b></td></tr>
     <tr><td align="right">Name:</td><td>{#INPUT name string $name allowempty=0}</td></tr>
     <tr><td align="right">Homepage:</td><td>{#INPUT homepage string $homepage}</td></tr>
     <tr><td align="right">Beschreibung:</td><td>{#INPUT description text $description}</td></tr>
     <tr><td align="right">Banner:</td><td><a href="sponsor.php?frame=imagelist&amp;type=banner&amp;sponsor_id={$id}">bearbeiten</a></td></tr>
     <tr><td align="right">Buttons:</td><td><a href="sponsor.php?frame=imagelist&amp;type=button&amp;sponsor_id={$id}">bearbeiten</a></td></tr>
     <tr><td colspan="2" align="center">&nbsp;</td></tr>
     
     <tr><td colspan="2" align="center"><b>interne Daten</b></td></tr>
     <tr><td align="right">Email:</td><td>{#INPUT email email $email}</td></tr>   
     <tr><td align="right">Ansprechpartner:</td><td>{#INPUT contact string $contact}</td></tr>
     <tr><td align="right">Stra&szlig;e:</td><td>{#INPUT street string $street}</td></tr>
     <tr><td align="right">Plz/Ort:</td><td>{#INPUT city string $city}</td></tr>
     <tr><td align="right">Telefon:</td><td>{#INPUT phone phone $phone}</td></tr>
     <tr><td align="right">Bemerkung:</td><td>{#INPUT comment text $comment}</td></tr>
     <tr><td colspan="2" align="center">&nbsp;</td></tr>
     <tr><td colspan="2" align="center">{#SUBMIT Speichern}</td></tr>
     <tr><td colspan="2" align="center">&nbsp;</td></tr>
     <tr><td colspan="2" align="center">{#BACKLINK Zur&uuml;ck}</td></tr>
    </table>
  {#END}
{#END}

{#SUB imagelist}
	{#VAR sub=header}
	{#TABLE $items}
	  {#COL Werbung* $show_as_ad}<b>{#IF $show_as_ad}<span style="color:green">Ja</span>{#ELSE}<span style="color:gray">Nein</span>{#END}</b>
	  {#COL Liste* $show_in_list}<b>{#IF $show_in_list}<span style="color:green">Ja</span>{#ELSE}<span style="color:gray">Nein</span>{#END}</b>  
	  {#COL Prio $priority align=center}
	  {#COL $imgtitle}
		{#IF !empty($html)}{$html}
		{#ELSEIF $type=='banner'}{#DBIMAGE flip_sponsor_ads image $id $image_mtime border=0 width=468 height=60 sizeonlyforswf=1}
		{#ELSE}{#DBIMAGE flip_sponsor_ads image $id $image_mtime border=0 width=88 height=31 sizeonlyforswf=1}{#END}
	  {#COL Sponsor %sponsor}<a href="sponsor.php?frame=viewsponsor&amp;id={$sponsor_id}"><b>{%sponsor}</b></a><br /><a href="{%url}" target="_blank">{%url}</a>
	  {#COL "Counter*<br />letzter Reset" $views align="center"}
		{#IF $enable_counter}
		  <b>
			<span style="color:gray">{$views}</span>
			<span style="color:orange">{$sessions}</span>
			<span style="color:green">{$clicks}</span>
		  </b><br />
		  {#DATE $last_counter_reset}
		{#END}
	  {#COL Logs*}
		  {#IF $enable_view_log}<span style="color:gray">v</span>{#END}
		  {#IF $enable_session_log}<span style="color:orange">s</span>{#END}
		  {#IF $enable_click_log}<span style="color:green">c</span>{#END}
	  {#FRAME edit editimage condition=$edit}
	  {#OPTION L&ouml;schen deleteimage condition=$edit}
	  {#OPTION "reset Counter" resetcounter condition=$edit}
	{#END}
	<br />
	{#IF $edit}
	{#FORM action=sponsor.php method=get keepvals=0}
	  <table class="table" cellpadding="2" cellspacing="1"><tr>
		<td class="tdcont" style="padding: 4px;white-space:nowrap;">
		  {#IF empty($sponsors) and empty($sid)}
			Bilder k&ouml;nnen nur f&uuml;r Sponsoren hinzugef&uuml;gt werden.
		  {#ELSE}
			{#INPUT frame hidden editimage}
			{#INPUT type hidden $type}
			{$imgtitle} f&uuml;r
			{#IF empty($sid)}
			  {#INPUT sponsor_id dropdown $sid param=$sponsors}
			{#ELSE}
			  {#INPUT sid hidden $sid}<b>{%sponsor}</b>
			{#END}
			</td><td class="tdaction">
			{#SUBMIT hinzuf&uuml;gen}
		  {#END}
		</td>
	  </tr></table>
	{#END}
	{#END}
	<br />
	<p align="left" style="width:700px;">
	  <b>*Counter:</b><br />
	  <span style="color:gray">Views</span>: Die Anzahl der insgesamten Einblendungen des Bildes.<br />
	  <span style="color:orange">Sessions</span>: Die Anzahl der verschiedenen Sessions und damit in etwa der verschiedenen User, die das Bild betrachtet hat.<br />
	  <span style="color:green">Clicks</span>: Die Anzahl der Clicks auf das Bild.<br /><br />
	  <b>*Werbung:</b> Wird das Bild in der Bannerrotation/Buttonliste angezeigt?<br />
	  <b>*Liste:</b> Wird das Bild in der Sponsorenliste angezeigt?<br />
	</p>
{#END}

{#SUB editimage}
{#FORM image}
  {#INPUT id hidden $id}
  {#INPUT type hidden $type}
  {#INPUT sponsor_id hidden $sponsor_id}
  <table>   
   <tr><td align="right">Sponsor:</td><td><b>{%sponsor}</b></td></tr>
   <tr><td align="right">Werbung:</td><td>{#INPUT show_as_ad checkbox $show_as_ad} <i>(Anzeigen in Bannerrotation/Buttonliste)</i></td></tr>
   <tr><td align="right">Sponsorenliste:</td><td>{#INPUT show_in_list checkbox $show_in_list} <i>(Anzeigen in der Sponsorenliste)</i></td></tr>
   <tr><td align="right">Counter:</td><td>{#INPUT enable_counter checkbox $enable_counter}</td></tr>
   <tr><td align="right"><span style="color:gray">ViewLog</span>:</td><td>{#INPUT enable_view_log checkbox $enable_view_log}</td></tr>
   <tr><td align="right"><span style="color:orange">SessionLog</span>:</td><td>{#INPUT enable_session_log checkbox $enable_session_log}</td></tr>
   <tr><td align="right"><span style="color:green">ClickLog</span>:</td><td>{#INPUT enable_click_log checkbox $enable_click_log}</td></tr>
   <tr><td align="right">Priorit&auml;t:</td><td>{#INPUT priority tabledropdown $priority param=sponsor_priority}</td></tr>
   <tr><td align="right">Bildlink:</td><td>{#INPUT link longstring $link}</td></tr>
   <tr><td align="right">Bild*:</td><td>{#INPUT image image}</td></tr>
   <tr><td align="right">HTML*:</td><td>{#INPUT html text $html}</td></tr>
   <tr><td colspan="2" align="center">{#DBIMAGE flip_sponsor_ads image $id $image_mtime border=0}</td></tr>
   <tr><td colspan="2" align="center">&nbsp;</td></tr>
   <tr><td colspan="2" align="center">{#SUBMIT Speichern}</td></tr>
   <tr><td colspan="2" align="center">&nbsp;</td></tr>
   <tr><td colspan="2" align="center">{#BACKLINK Zur&uuml;ck}</td></tr>
   <tr><td colspan="2" align="center">&nbsp;</td></tr>
   <tr><td colspan="2" align="center">
    <b>*</b> Es kann entweder ein Bild/SWF oder HTML-Code als &Uuml;berbringer der Werbebotschaft angegeben werden.<br /> 
    Wenn HTML-Code angegeben ist, wird das Bild nicht angezeigt.
   </td></tr>
   </table>
  {#END}
{#END}

{#SUB viewlog}
{#VAR sub=header}
  {#ACTION Leeren emptylog confirmation="Soll der Sponsorenlog wirklich geleert werden? Er ist unabh&auml;ngig von den Countern."}
  {#TABLE $items maxrows=100 foundrows=$foundrows}
    {#COL Zeit<br/>Vorgang}{#DATE $time}<br />{$reason}
    {#COL Bild}{#DBIMAGE flip_sponsor_ads image $adid $admtime border=0}
    {#COL Typ<br/>Sponsor}{$type} {$location}<br /><b><a href="sponsor.php?frame=viewsponsor&amp;id={$sponsor_id}">{%sponsor}</a></b>
    {#COL IP}{$ip}
  {#END}
{#END}