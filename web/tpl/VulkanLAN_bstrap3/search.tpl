{! $Id$ }

{#SUB default}
<div class="panel-group">
	<!-- Suchformular -->
	<div class="panel panel-default">
		<div class="panel-heading">	
			<div class="row">
				<div class="col-xs-12">
				</div>
			</div>
		</div>
		<div class="panel-body">
			{#FORM method=get action=search.php?frame=result}
			<div class="row">
				<div class="col-xs-6">
					
						{#IF isset($nextframelink)}
						{#INPUT nextframelink hidden $nextframelink}
						{#END}
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
						
						{#INPUT searchstring string $searchstring "type=\"text\" class=\"form-control\" placeholder=\"Suchtext\""}
						<div class="input-group-btn">
						<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
						</div>
					</div>
				</div>
				<div class="col-xs-5">
				Modus:
				{#INPUT mode dropdown $mode param=$modes}
				</div>
				<div class="col-xs-1"></div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="checkbox">
						<label class="checkbox-inline">{#INPUT titleonly checkbox $titleonly} Nur Titel durchsuchen</label>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					Module:
				</div>
				<div class="col-xs-12">
					<div class="checkbox">
						{#FOREACH $modules}
						<label class="checkbox-inline">{#INPUT usedmodules[$name] checkbox $checked}{$name}</label>
						{#END}
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
				{#SUBMIT Suchen}
				</div>
			</div>
			{#END}
		</div>
	</div>
		<!-- Ergebnisse -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="row">
				<div class="col-xs-12">
					<h3>Suchergebnisse</h3>
				</div>
				<div class="col-xs-12">
					<u>{$count} Eintr&auml;ge in {$time} Sekunden gefunden</u>
				</div>
			</div>
		</div>
		<div class="panel-body">
			<div class="row">
				{#IF $usenextlink}
				<div class="col-xs-12">
				<h3 style="color: #008800;">Klicke auf den Titel (Usernamen) um fortzufahren!</h3>
				</div>
				{#END}
				<div class="col-xs-12">
				{#TABLE $results maxrows=50 class="table table-condensed table-hover table-striped"}
				  {#COL Titel %title}<a href="{$link}"{#WHEN $link_new " target=_blank"}>{%title}</a>
				  {#COL Text $text}
				  {#COL Modul $mod}
				{#END}
				</div>
			</div>
		</div>
	</div>
</div>
{#END}