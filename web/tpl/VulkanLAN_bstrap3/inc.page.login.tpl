{#SUB login}

{#IF $isloggedin}

<!-- neuer login in der Hauptmenue leiste - edit naaux -->
<form class="navbar-form navbar-right" method="post" id="frm" action="" style="margin-right: 10px">

{#IF $unread_messages}

  <span class="label label-warning"" style="font-size: 120%;">
    <a href="webmessage.php?frame=owner">
      <span class="fa fa-envelope-o"></span>
    </a>
  </span>

  {#END}

  <span class="label label-default" style="font-size: 120%;">
     <strong>{%user}</strong>
  </span>
  <input type="hidden" name="ident" value="Anonymous" />
  <input type="hidden" name="password" value="x" />
  <button type="submit" class="btn btn-primary" value="logout"> Logout 
    <span class="fa fa-sign-out"></span>
  </button>
</form>

{#ELSE}

<!-- neuer logout in der Hauptmenue leiste - edit naaux -->
<form class="navbar-form navbar-right" method="post" id="frm" action="" style="margin-right: 10px">
  <div class="form-group">
	<div class="input-group">
		<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
		<input type="text"
		  class="form-control"
		  name="ident"
		  title="Deine UserID, dein Nickname oder deine Email-Adresse"
		  placeholder="UserID, Nickname oder Email-Adresse"
		  onFocus="if(this.value=='Nickname casesensitive') this.value='';" {#WHEN "!empty($loginident) and ($loginident != \"Anonymous\")" "value=\"%loginident\" "}/>
	</div>
	<div class="input-group">
		<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
		<input type="password"
			id="password"
		  class="form-control"
		  name="password"
		  title="Passwort"
		  placeholder="Password"
		  onclick="this.value='';" />
	</div>
  </div>
  <button type="submit" class="btn btn-success">Sign In &nbsp;
    <i class="fa fa-sign-in"></i>   
  </button>
  <!--<a href="text.php?name=user_account_trouble">
    <font color="#aaff00">
      <strong>
        <i class="fa fa-magic"></i>
        ?
        <i class="fa fa-user-md"></i>
      </strong>
    </font>
  </a>-->
</form>

<script type="text/javascript">
  var loginform = document.getElementById('frm');
  {#IF empty($loginident) or ($loginident == "Anonymous")}
    loginform.ident.value='Nickname casesensitive';	
    loginform.password.value='Password';
  {#ELSE}
    loginform.password.focus();
  {#END}
</script>

{#END}   

{#END}