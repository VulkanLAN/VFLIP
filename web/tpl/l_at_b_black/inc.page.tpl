{#SUB page}<html>
<head>
  <link rel="stylesheet" type="text/css" href="{#STATICFILE inc.page.css}" />
  <link rel="stylesheet" type="text/css" href="{#STATICFILE inc.content.css}" />
  <link rel="stylesheet" type="text/css" href="{#STATICFILE inc.form.css}" />
  <link rel="shortcut icon" href="{#STATICFILE favicon.ico}" />
  <title>{%title} - {%caption}</title>
  <style>
  <!--
  ul { list-style-image: url({#STATICFILE images/dot2.png}); }
  -->
  </style>
</head>
<body>
<table cellpadding="0" cellspacing="0" width="100%">
  <tr>
    <td style="width:32px; height:84px;"></td>
    <td class="toplogo" rowspan="2" style="background-image:url({#STATICFILE images/logo_latb.png});"></td>
    <td style="height:84px;" align="center" valign="center">
      {#VAR sub=banner file=inc.sponsor.tpl}
    </td>
    <td style="height:84px;" align="right" valign="center">
      {#VAR sub=login file=inc.page.login.tpl}
    </td>
    <td style="width:32px; height:84px;"></td>
  </tr>
  <tr>
    <td class="bartl" style="background-image:url({#STATICFILE images/upper_left.png});"></td>
    <td class="bartm" style="background-image:url({#STATICFILE images/upper.png});" nowrap="1" colspan="2">
      {#LOAD MenuLoad() $level0 inc/inc.menu.php}
      {#FOREACH $level0.menu}{#FOREACH $items}
        <a class="{#WHEN $active menulinksel menulink}" href="{$link}" title="{%description}"{#WHEN $use_new_wnd " target=\"_blank\""}>{%caption}</a>
      {#MID} | {#END}
      {#END}
    </td>
    <td class="bartr" style="background-image:url({#STATICFILE images/upper_right.png});"></td>
  </tr>
</table>

  <table cellpadding="0" cellspacing="0" width="100%">
    <tr>
      {!------------------------- left menu ---------------------------}
      {#LOAD MenuLoad($level0.activedir) $level1 inc/inc.menu.php}
      {#IF !empty($level1.menu)}
      <td valign="top" style="padding-right:8px; padding-top:8px; padding-bottom:8px;" width="100">
        <table cellpadding="0" cellspacing="0" width="100%">
          {#FOREACH $level1.menu}
          <tr>
            <td class="menuleft">
              <div style="background-image:url({#STATICFILE images/menu_title.png});" class="menutitle">{%caption}</div>
              <div style="padding-top:4px; padding-bottom:4px;">
                {#FOREACH $items}
                  {#IF is_object($frame)}{#TPL $frame}
                  {#ELSEIF !empty($text)}{$text}
                  {#ELSE}{#DBIMAGE $image}<a class="{#WHEN $active menulinksel menulink}" href="{$link}" title="{%description}"{#WHEN $use_new_wnd " target=\"_blank\""}>{%caption}</a><br />
                  {#END}
                {#END}
              </div>
            </td>
          </tr>
          {#MID}
          <tr><td style="height:8px;"></td></tr>
          {#END}
        </table>
      </td>
      {#END}
      {!---------------------- end left menu --------------------------}
      <td align="center">
<!-- ERRORCELL -->
        <table cellpadding="0" cellspacing="0" width="100%" style="margin-top: 8px;">
          <tr>
            <td class="{$contenttype}cell" align="center" valign="top">
              {#WHEN !empty($caption) <h1>%caption</h1>}
{#TPL $page}
            </td>
          </tr>
        </table>
      </td>
      <td style="width:32px;">
    </tr>
  </table>
<table cellpadding="0" cellspacing="0" width="100%" style="margin-top:8px;">
  <tr>
    <td class="barbl" style="background-image:url({#STATICFILE images/lower_left.png});"></td>
    <td class="barbm" style="background-image:url({#STATICFILE images/lower.png});" colspan="2">{$copyright}</td>
    <td class="barbr" style="background-image:url({#STATICFILE images/lower_right.png});"></td>
  </tr>
</table>
</body>
</html>
{#END}

{#SUB errorcell}
<table cellpadding="0" cellspacing="0" width="100%" style="margin-top: 8px;">
{#IF count($errors) > 0}
<tr><td class="errorcell">
  <h1 align="center">Fehler</h1>
  <ul>
  {#FOREACH $errors $err}
    <li>{$err}</li>
  {#END}
  </ul>
</td></tr>
<tr><td style="height:8px;"></td></tr>
{#END}
{#IF count($notices) > 0}
<tr><td class="noticecell">
  <h1 align="center">Nachricht</h1>
  <ul>
  {#FOREACH $notices $msg}
    <li>{$msg}</li>
  {#END}
  </ul>
</td></tr>
<tr><td style="height:8px;"></td></tr>
{#END}
</table>
{#END}