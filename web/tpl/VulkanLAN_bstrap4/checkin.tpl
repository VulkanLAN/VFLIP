{! $Id$ }

{#SUB default}
	<div class="card ">
		<div class="card-header">
			<div class="row">
				<div class="col-12 col-sm-2 col-md-2">
					<div style="margin-bottom: 0px; margin-top: 15px;">
					<a href="user.php?frame=editproperties&amp;id=create_new_subject&amp;type=user&amp;enabled=Y" class="btn btn-info btn-sm" role="button"><i class="fas fa-user-plus"></i>&nbsp;
					Neuen User erstellen
					</a></div>
				</div>
				<div class="col-sm-2 col-md-1"></div>
				<div class="col-12 col-sm-8 col-md-9">
					{#IF $editright}
					<div class="alert alert-info" role="alert" style="margin-bottom: 0px; margin-top: 0px;">
						<div class="row">
							<div class="col-12">
							<div class="btn-group">
							<a href="checkin.php?frame=editlist" class="btn btn-secondary btn-sm" role="button"><i class="far fa-object-group"></i>&nbsp; Pr&uuml;fungen bearbeiten</a>
							<a href="config.php?category=checkininfo" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-cogs"></i>&nbsp; Systemeinstellungen bearbeiten</a>
							<a href="checkin.php?frame=doku" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-book"></i>&nbsp; Dokumentation</a>
							</div>
							</div>
						</div>
					</div>
					{#END}
				</div>
			</div>
		</div>
	</div>
	<div class="card ">
		<div class="card-body">
			<div class="row">
				<div class="col-12 col-sm-10 col-md-6">
				 <form name="search user" action="checkin.php" method="get">
				  <div class="input-group">
					<span class="input-group-btn"><button class="btn btn-secondary btn-md" type="submit"><i class="fas fa-user"></i></button></span>
					<input name="search" type="text" value="{%search}" class="form-control" placeholder="Irgend ein im Profil enthaltener Wert">
					<div class="input-group-btn">
						<button class="btn btn-secondary btn-md" type="submit">
						<i class="fas fa-search"></i>
						</button>
					</div>
				  </div>
				</form>
				</div>
				<div class="col-sm-2 col-md-6"></div>
			</div>
		</div>
	</div>
	<div class="card ">
		<div class="card-body">
			<div class="row">
				<div class="col-12">
				Suchergebnisse:
				{#IF count($users) > 0}
				  {#TABLE $users class="table table-sm table-hover table-striped"}
					{#COL En $enabled}
					{#COL Nick $name}<b><a href="checkin.php?search={$id}">{%name}</a></b>
					{#COL Name $givenname}{%givenname} {%familyname}
					{#COL Email %email}
				  {#END}
				{#END}
				</div>
			</div>
			<div class="row">
				<div class="col-12">
				{#IF count($user) > 0}{#WITH $user}
					<div class="card ">
						<div class="card-header">
							<div class="row">
								<div class="col-12">
								<p><h3 class="text-success">{%nick} ({%name}) &nbsp;
								{#IF !empty($current_step)}
									<span class="label label-success">
									Schritt {$current_step} von {$final_step}
									</span>
									</span></h3></p>
								</div>
								<div class="col-12">
									<a href="user.php?frame=viewsubject&amp;id={$id}" class="btn btn-secondary btn-md" role="button"><i class="far fa-user"></i>&nbsp;&Uuml;bersicht</a>
									<a href="user.php?frame=editproperties&amp;id={$id}" class="btn btn-secondary btn-md" role="button"><i class="far fa-edit"></i>&nbsp; Profil</a>
									<a href="seats.php?userid={$id}" class="btn btn-secondary btn-md" role="button"><i class="fas fa-map-marker-alt"></i>&nbsp;Sitzplatz</a>
									<a href="checkininfo.php?frame=ausweis&id={%id}" target="_blank" class="btn btn-secondary btn-md" role="button"><i class="far fa-id-card"></i>&nbsp;Ausweis</a>
									<a href="sendmessage.php?frame=sendcheckin&id=8&userid={%id}" target="_blank" class="btn btn-secondary btn-md" role="button"><i class="far fa-envelope"></i>&nbsp;Bezahlbestätigung senden</a>
								</div>
								{#END}
							</div>
						</div>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-12 col-lg-12 col-xl-5">
							{#FOREACH $warnings $w}<span style="color:#E60000"><h3>{$w}</h3></span><br />{#END}
							<table class="table table-hover table-striped">
							{#FOREACH $props}
								<tr>
									<td align="right">{%name}:</td>
									<td style="font-weight: bold;">
									{#IF $type == "date"}
										{#DATE $value}
									{#ELSE}
										{#IF $type=="Image"}{#DBIMAGE $value process=createthumbnail(200,200)}
										{#ELSE}{%value}
										{#END}
									{#END}
									</td>
								</tr>
							{#END}
							</table>
							</div>
							<div class="col-12 col-lg-12 col-xl-7">
								<!-- Checkin Checks-->
								{#FOREACH $buttons}
									{#IF $input_type!="image"}
										{#FORM item action="checkin.php?search=$id" keepvals=0}
											{#INPUT action_type hidden $type}
											{#INPUT action_name hidden $name}
											{#IF $input_type == "edit"}
												<b>{$text}:</b>
												{#INPUT action_value $action_val_type $value}
											{#ELSE}
												{#INPUT action_value hidden $value}
											{#END}
											{!der Templatecompiler kann nicht die Werte der Variablen vorraussehen und somit nicht wissen ob ein Uploadfeld vorhanden ist oder nicht ($val_type)}
											{#IF false}{#INPUT justtomaketheformanuploadform image enabled=0}{#END}
											{#INPUT id hidden $id}
											<input type="submit" class="{$button_type}" role="button" value="{$text}" />
										{#END}<br />
									{#END}
								{#END}
								<!-- End Checkin Checks-->
								<!-- Checkin WebCAM Photo Upload-->
								{#FOREACH $buttons}
									{#IF $input_type=="image"}
										{#INPUT action_type hidden $type}
										{#INPUT action_name hidden $name}
										{#INPUT imgdata hidden $imgdata}
										<div class="row">
											<div class="col-lg-12">
											<h5>Neues Userbild mit Kamera aufnehmen</h5>
											<div class="row">
												<div class="col-lg-12 alert-secondary p-2">
												<button type="button" id="start-camera" class="btn btn-primary">Kamera starten</button>
												<button type="button" id="click-photo" class="btn btn-warning">Foto schießen</button>
												<div class="float-right">
													<button type="submit" class="btn btn-success">Foto als Userbild speichern</button>
												</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-6 text-center">
												<div><label>Vorschau</label></div>
												<video id="preview-video" class="m-auto bg-dark" style="width: 320px; height:240px;"></video>
												</div>
												<div class="col-lg-6 text-center">
												<div><label>Foto</label></div>
												<canvas id="photo-canvas" class="m-auto bg-dark" style="width: 320px; height:240px;"></canvas>
												</div>
											</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-12">
											<b>Oder {$text}:</b>
											{#INPUT action_value $action_val_type $value}
											</div>
										</div>
									{#END}
									{!der Templatecompiler kann nicht die Werte der Variablen vorraussehen und somit nicht wissen ob ein Uploadfeld vorhanden ist oder nicht ($val_type)}
									{#IF false}{#INPUT justtomaketheformanuploadform image enabled=0}{#END}
									{#INPUT id hidden $id}
									<input type="submit" class="{$button_type}" role="button" value="{$text}" />
								{#END}
								<!-- END Checkin WebCAM Photo Upload-->
								<!-- Checkin Photo Maual Upload-->
								{#FOREACH $buttons}
									{#IF $input_type=="image"}
										{#FORM item action="checkin.php?search=$id" keepvals=0}
											{#IF $input_type=="image"}
												<b>{$text}:</b>
												{#INPUT action_value $action_val_type $value}
											{#END}
											{!der Templatecompiler kann nicht die Werte der Variablen vorraussehen und somit nicht wissen ob ein Uploadfeld vorhanden ist oder nicht ($val_type)}
											{#IF false}{#INPUT justtomaketheformanuploadform image enabled=0}{#END}
											{#INPUT id hidden $id}
											<input type="submit" class="{$button_type}" role="button" value="{$text}" />
										{#END}
									{#END}
								{#END}
								<!-- End Checkin Photo Maual Upload-->
							</div>
						</div>
					</div>
					{#END}
				</div>
			</div>
    <div class="row">
      <div class="col-12">
        {#IF (count($users) == 0) and (count($user) == 0)}
          <p>
          Keine User gefunden :-(
          </p>
        {#END}
        <script>
          {#IF $user.barcode}
          document.b.hw_barcode.focus();
          {#ELSE}
          document.f.search.focus();
          document.f.search.select();
          {#END}
        </script>
      </div>
    </div>
  </div>
</div>

{#END}

{#SUB editlist}
<div class="card ">
	<div class="card-header">
		<div class="row">
			<div class="col-12">
			<a href="checkin.php" class="btn btn-secondary btn-sm" role="button"><i class="far fa-check-square"></i>&nbsp;zum Checkin</a>
			<a href="checkin.php?frame=doku" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-book"></i>&nbsp;Dokumentation</a>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12">
				{#TABLE $list class="table table-hover table-striped"}
				<a href="checkin.php?frame=edititem" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-plus"></i>&nbsp;{§hinzuf&uuml;gen}</a>
				  {#COL "Schritt #" $order align="center"}{#WHEN "$input_type == \"text\"" ">="}{$order}
				  {#COL Status $status}
				  {#COL Pr&uuml;fung $check_name}{$check_text}
				  {#COL Anzeige $input_type}
				  {#COL Aktion $action_name}{$action_text}
				  {#OPTION entfernen delitems}
				  {#FRAME edit edititem}
				{#END}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB edititem}
<div class="card ">
	<div class="card-header">
		<div class="row">
			<div class="col-12">
			<a href="checkin.php?frame=editlist" class="btn btn-secondary btn-sm" role="button"><i class="far fa-object-group"></i>&nbsp;{§zur&uuml;ck zur Liste}</a>
			<a href="checkin.php?frame=doku" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-book"></i>&nbsp;Dokumentation</a>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12">
				{#FORM customize}
					{#INPUT id hidden $id}
					Status: {#INPUT status dropdown $status param=$statuses}<br />
					<h3>{§Pr&uuml;fung}</h3>
					<table>
					<tr><td>{§Typ}:</td><td>{#INPUT check_type dropdown $check_type $checktypes}</td></tr>
					<tr><td>{§Name}:</td><td>{#INPUT check_name string $check_name}</td></tr>
					<tr><td>{§Wert}:</td><td>{#INPUT check_not checkbox $check_not} {§nicht}{#INPUT check_value string $check_value}</td></tr>
					</table>
					<h3>{§Eingabe/Anzeige}</h3>
					<table>
					<tr><td>{§Typ}:</td><td>{#INPUT input_type dropdown $input_type $inputtypes}</td></tr>
					<tr><td>{§Buttonfarbe}:</td><td>{#INPUT button_color string $button_color}</td></tr>
					<tr><td>{§Buttontype}:</td><td>{#INPUT button_type string $button_type}</td></tr>
					</table>
					<h3>{§Aktion}</h3>
					<table>
					<tr><td>{§Typ}:</td><td>{#INPUT action_type dropdown $action_type $actiontypes}</td></tr>
					<tr><td>{§Name}:</td><td>{#INPUT action_name string $action_name}</td></tr>
					<tr><td>{§Wert}:</td><td>{#INPUT action_value string $action_value}</td></tr>
					</table>
					<br />
					{§Reihenfolge}: {#INPUT order integer $order}<br />
					{#SUBMIT speichern}
				{#END}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB doku}
<div class="card ">
	<div class="card-header">
		<div class="row">
			<div class="col-12">
			<a href="checkin.php" class="btn btn-secondary btn-sm" role="button"><i class="far fa-check-square"></i>&nbsp;zum Checkin</a>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12">
			<h2>Checkindokumentation</h2>
			Stand: 2005-09-17<br />
			</div>
			<div class="col-12">
				<div align="left">
				<h2>Aufbau</h2>
				<p>Der Checkin besteht aus einer Pr&uuml;fung, Darstellung und Aktion. Wenn die Pr&uuml;fung fehlschl&auml;gt, wird dies
				dargestellt. Die Pr&uuml;fung gibt an, wie der Zustand des Users sein soll, z.B. das er ein bestimmtes Recht hat.
				Ist dies nicht der Fall, wird eine Aktion dargestellt. Die Darstellung kann ein Hinweis sein oder ein Button der direkt eine Aktion ausf&uuml;hrt. Ausserdem
				k&ouml;nnen Informationen zum Benutzer angezeigt werden.<br />
				Es wird eine Liste mit allen Pr&uuml;fungen und Aktionen angelegt. Diese wird nacheinander abgearbeitet. Schl&auml;gt eine
				Pr&uuml;fung fehl, wird die Abarbeitung abgebrochen. Die Darstellung wird angezeigt, damit das "Problem" behoben werden kann.
				Ist dies ein Button kann direkt die Aktion durchgef&uuml;hrt werden. Z.B. bei der Auswahl eines Sitzplatzes ist dies nicht so
				einfach m&ouml;glich und es wird ein Hinweis angezeigt, dass ein Sitzplatz ausgew&auml;hlt werden muss.<br />
				Die Aktionen k&ouml;nnen das setzen einer Eigenschaft, das Zuweisen eines Rechtes oder das Verschieben in eine Gruppe sein.
				</p>
				<h2>Konfiguration</h2>
				<p>F&uuml;r jeden Eintrag der abgearbeitet wird, werden folgende Daten festgelegt:<br />
				<dt>Status</dt>
				<dd>Gibt an ob der Eintrag <em>aktiv</em>, <em>optional</em> oder <em>inaktiv</em> ist.</dd>
				<dt>Pr&uuml;fung</dt>
				<dd>Die Pr&uuml;fung die durchgef&uuml;hrt wird. Der <em>Typ</em> bestimmt wie <em>Name</em> und <em>Wert</em> genutzt werden.
				Oft ist Wert nicht notwendig, z.B. wenn eine Gruppenzugeh&ouml;rigkeit gepr&uuml;ft wird, gibt <em>Name</em> die Gruppe an.</dd>
				<dt>Eingabe/Anzeige</dt>
				<dd>Gibt an was angezeigt wird, wenn die Pr&uuml;fung fehl schl&auml;gt.Eine Ausnahme ist <em>Infotext</em>: Er wird immer angezeigt (unabh&auml;ngig von der Pr&uuml;fung).<br />
				Die Anzeige bezieht sich auf die durchzuf&uuml;hrende Aktion:
				Soll ein Button etwas setzen, ein Wert eingegeben werden oder ein Hinweis angezeigt werden?</dd>
				<dt>Aktion</dt>
				<dd>Die Aktion die durchgef&uuml;hrt wird. Der <em>Typ</em> bestimmt wie <em>Name</em> und <em>Wert</em> genutzt werden.
				Oft ist Wert nicht notwendig, z.B. wenn der Status gesetzt wird, gibt <em>Name</em> den Status an.</dd>
				<dl><dt>Reihenfolge</dt>
				<dd>Die Reihenfolge in der die Eintr&auml;ge abgearbeitet werden.</dd>
				</dl>
				</p>
				</div>
			</div>
		</div>
	</div>
</div>
{#END}
