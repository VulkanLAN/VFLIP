
{#SUB _input_string}
  <input type="text" class="form-control" name="{%name}" value="{%val}" size="32"{$enabled} />
{#END}

{#SUB _input_longstring}
  <input style="width:100%;" type="text" class="form-control" name="{%name}" value="{%val}" size="32"{$enabled} />
{#END}

{#SUB _input_yesno}
<!-- Umstellung VulkanLAN notwendig button Check funktioniert nicht -->


<div class="card panel-default">
	<div >
		<input type="radio" class="checkbox" name="{%name}" value="Y"{$sely}{$enabled} />{§Ja}
	</div>
	<div >
		<input type="radio" class="checkbox" name="{%name}" value="N"{$seln}{$enabled} />{§Nein}
	</div>
</div>


<!--
<div class="card panel-default">
	<div class="form-check">
		<label {#WHEN "!empty($sely)" "class=\"btn btn-default active\""}>
			<input type="radio" name="checkbox" id="{%name}"  name="{%name}" value="Y" {#WHEN "!empty($sely)" "checked=\"\""} {$sely} {$enabled}>
			{§Ja}
		</label>
	</div>
	<div class="radio">
		<label {#WHEN "!empty($seln)" "class=\"btn btn-default active\""}>
			<input type="radio" name="checkbox" id="{%name}" name="{%name}" value="N" {#WHEN "!empty($seln)" "checked=\"\""} {$seln} {$enabled}>
			{§Nein}
		</label>
	</div>
</div> -->
{#END}

{#SUB _input_radio}
  <input type="radio" class="checkbox" name="{%name}" value="{%val}"{$sel}{$enabled} />
{#END}

{#SUB _input_checkbox}
  <input type="checkbox" class="checkbox" name="{%name}" value="{%param}"{$sel}{$enabled} />
{#END}

{#SUB _input_checkbox_onclick}
  <input id="{%name}" type="checkbox" class="checkbox" name="{%name}" value="{%param}"{$sel}{$enabled} onclick="onCheckBoxClick(this)" />
{#END}

{#SUB _input_integer}
  <input type="text" class="form-control" name="{%name}" value="{%val}" size="12"{$enabled} />
{#END}

{#SUB _input_decimal}
  <input type="text" class="form-control" name="{%name}" value="{%val}" size="12"{$enabled} />
{#END}

{#SUB _input_ip}
  <input type="text" class="form-control" name="{%name}" value="{%val}" size="12"{$enabled} />
{#END}

{#SUB _input_dropdown}
  <select name="{%name}"{$enabled}>
    {#FOREACH $items}<option value="{%key}" {$sel}>{%caption}</option>{#END}
  </select>
{#END}

{#SUB _input_inputtypes}
  <select name="{%name}_type"{$enabled}>
    {#FOREACH $items}<option value="{%key}" {$sel}>{%caption}</option>{#END}
  </select>
  {§Parameter}:
  <input type="text" class="form-control" name="{%name}_param" value="{%param}" size="32"{$enabled} />
{#END}

{#SUB _input_multisel}
  {#FOREACH $items}
    <input type="checkbox" class="checkbox" name="{%name}_{%key}" value="1"{$sel}{$enabled} />{%caption}
  {#MID}
    <br />
  {#END}
{#END}

{#SUB _input_tabledropdown}
  <select name="{%name}"{$enabled}>
    {#FOREACH $items}<option value="{%key}" {$sel}>{%caption}</option>{#END}
  </select>{#IF $canedit}&nbsp;&nbsp;&nbsp;<i><a href="table.php?frame=viewtable&amp;name={$table}">({§edit})</a></i>{#END}
{#END}

{#SUB _input_tablemultisel}
  {#FOREACH $items}
    <input type="checkbox" class="checkbox" name="{%name}_{%key}" value="1"{$sel}{$enabled} />{%caption}
  {#MID}
    <br />
  {#END}
  {#IF $canedit}<br/><i><a href="table.php?frame=viewtable&amp;name={$table}">({§edit})</a></i>{#END}
{#END}

{#SUB _input_text}
<!-- new editor TinyMCE edit by VulkanLAN -->
	<textarea rows="10" cols="48" name="{%name}" class="form-control"{$enabled}>{%val}</textarea>
{#END}

{#SUB _input_document}
<!-- new editor TinyMCE edit by VulkanLAN -->
<!-- <form method="post">  </form>  -->
	<textarea style="width:100%;" cols="90" rows="30" name="{%name}" class="form-control"{$enabled}>{%val}</textarea>

{#END}

{#SUB _input_documentwrap}
<!-- new editor TinyMCE edit by VulkanLAN -->
  <textarea style="width:100%;" cols="90" rows="30" wrap="virtual" name="{%name}" class="form-control"{$enabled}>{%val}</textarea>

{#END}

{#SUB _input_phone}
  <input type="text" class="form-control" name="{%name}" value="{%val}" size="15"{$enabled} />
{#END}

{#SUB _input_captcha}
	<div class="form-group col-sm-12">
		<label for="Bildtext" class="col-sm-2 control-label">{§Bild}:</label>
		<div class="col-sm-10">
		<img src="ext/captcha/captcha_image.php?img={$url}" alt="{§Bild mit Text fehlt!}"/>
		</div>
	</div>
	<div class="form-group col-sm-12">
		<label for="Bildtext" class="col-sm-2 control-label">{§Bildtext}:</label>
		<div class="col-sm-5">
		<input type="text" class="form-control" placeholder="{%name}" name="{%name}" value="{%val}"{$enabled} />
		</div><div class="col-sm-5"></div>
	</div>
{#END}

{#SUB _input_passwordedit}
  <div class="form-group col-sm-12">
  <div>
  Passwort:
  <input type="password" class="form-control" name="{%name}2" value="" size="24"{$enabled} />
  </div>
  <div>
  Passwort {§Wiederholung}:
  <input type="password" class="form-control" name="{%name}1" value="" size="24"{$enabled} />
  </div>
  </div>
{#END}

{#SUB _input_passwordquery}
  <input type="password" class="form-control" name="{%name}" value="" size="24"{$enabled} />
{#END}

{#SUB _input_hidden}
  <input type="hidden" name="{%name}" value="{%val}" />
{#END}

{#SUB _input_file}
<!-- muss noch angepasst werden -->
	<form class="form-inline">
	  <div class="form-group">
		<div class="input-group">
		  <div class="input-group-addon"><i class="fas fa-cloud-upload-alt"></i></div>
		  <input type="file" name="{%name}" class="btn btn-default"{$enabled} />
		  <div class="input-group-addon" align="left">
			<div class="col-12 col-sm-12 col-md-12">
			<small>{§maximale Dateigr&ouml;&szlig;e}: {#BYTE $maxupload format=mb prec=1}MB</small></div>
			<div class="col-12 col-sm-12 col-md-12">
		    <small>({$maxreason})</small></div>
			</div>
		</div>
	  </div>
	</form>

{#END}

{#SUB _input_fileinline}
<!-- file input ohne form -->
	  <div class="form-group">
		<div class="input-group">
		  <div class="input-group-addon"><i class="fas fa-cloud-upload-alt"></i></div>
		  <input type="file" name="{%name}" class="btn btn-default" {$enabled} />
		  <div class="input-group-addon" align="left">
			<div class="col-12 col-sm-12 col-md-12">
			<small>{§maximale Dateigr&ouml;&szlig;e}: {#BYTE $maxupload format=mb prec=1}MB</small></div>
			<div class="col-12 col-sm-12 col-md-12">
		    <small>({$maxreason})</small></div>
			</div>
		</div>
	  </div>

{#END}

{#SUB _input_image}
<div class="card panel-default">
	<table class="table">
	<colgroup><col class="col-2"><col class="col-10"></colgroup>
		<thead> <tr> <th>Auswahl</th> <th>Beschreibung</th> </tr> </thead>
		<tbody>
	  {#IF $allowurl}
	  <tr>
		<th>
		<div class="form-check">
			<input type="radio" class="checkbox form-check-input" name="{%name}_sel" id="{%name}_sel_url" value="url"{$enabled} />
			</div>
		</th>
		<td >
			<div class="input-group mb-2">
			<div class="input-group-prepend">
			  <div class="input-group-text">@</div>
			</div>	<input type="text" class="edit form-control" name="{%name}_url" id="{%name}_url" value="" onchange="document.getElementById('{%name}_sel_url').checked=true; document.getElementById('{%name}_file').value=''" {$enabled} placeholder="URL" />
			</div>
		</td>
	  </tr>
	  {#END}
	  <tr>
		<th><div class="form-check"><input type="radio" class="checkbox form-check-input" name="{%name}_sel" id="{%name}_sel_file" value="file"{$enabled} /></div></th>
		<td>
			<div class="input-group form-row align-items-center">
			  <label class="form-check-label" for="autoSizingCheck2">{§Datei}:&nbsp; </label>
			  <input type="file" name="{%name}_file" id="{%name}_file" class="edit btn btn-outline-info" onchange="document.getElementById('{%name}_sel_file').checked=true; document.getElementById('{%name}_url').value=''" {$enabled} />
			  <p class="help-block"> ({§maximale Dateigr&ouml;&szlig;e}: {#BYTE $maxupload format=mb prec=1}MB, <span style="color:grey;">{$maxreason}</span>)</p>
			</div>
		</td>
	  </tr>
	  <tr>
		<th>
		<div class="form-check">
			<input type="radio" class="checkbox form-check-input" name="{%name}_sel" value="del"{$enabled} />
			<label class="form-check-label" for="radio"> {§altes Bild l&ouml;schen} </label>
		</div>
		</th>
		<td>{#DBIMAGE $image process=createThumbnail(160,64)}</td>
	  </tr>
	 </tbody>
	</table>
</div>
{#END}

{#SUB _input_date}
  <input type="text" class="form-control" name="{%name}" value="{%val}" size="17"{$enabled} /> (d.m.y H:i)
{#END}

{#SUB _input_email}
  <input type="text" class="form-control" name="{%name}" value="{%val}" {$enabled} />
{#END}

{#SUB _input_dropdownedit}
  <select name="drop_{$name}"{$enabled} onchange="{$name}_change()">
    {#FOREACH $items}<option value="{%key}" {$sel}>{%caption}</option>{#END}
  </select>
  <input type="text" class="form-control" name="edit_{$name}" value="{%val}" size="32"{$enabled} />
  <script>
    function {$name}_change()
    {
      var i = -1;
      while(document.forms[++i])
        if(document.forms[i].edit_{$name}) with(document.forms[i])
        {
          edit_{$name}.disabled = (drop_{$name}.value == '') ? false : true;
          if(drop_{$name}.value != '') edit_{$name}.value = drop_{$name}.value;
        }
    }
  {$name}_change()
  </script>
  <noscript></noscript>
{#END}
