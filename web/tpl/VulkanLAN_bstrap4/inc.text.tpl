{#IF $found}
  <div class="textcontent">
    {$text}
  </div>
  {#IF $editable}
    <div class="textfooter">last edited by {%author} on {$date}. <a href="content.php?frame=edittext&amp;id={$id}" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-edit"></i>&nbsp;edit Text</a></div>
  {#END}
{#ELSE}
  <p>
    {§Fehler}: Der Text mit dem Namen "{%name}" wurde nicht gefunden.
    {#IF $editable} Du kannst ihn <b><a href="content.php?frame=edittext&amp;name={%name}" class="btn btn-secondary btn-sm" role="button">hier erstellen.</a></b>{#END}
  </p>
{#END}