{! $Id: lanparty.tpl 1415 20015-01-29 13:56:00Z loom edit by naaux $ }

{#SUB default}
<div class="card ">
	<div class="card-header">
		<div class="row">
			<div class="col-sm-12">
				<h3>Status</h3>
			</div>
		</div>
		{#IF $can_config}
		<div class="row">
			<div class="alert alert-info" role="alert" style="margin-bottom: 0px;">
				<a href="config.php#{$configlistgroupname}" class="btn btn-secondary" role="button">
				<i class="fas fa-users"></i>&nbsp;{§zus&auml;tzliche Gruppen in der Teilnehmerliste bearbeiten}</a>
			</div>
		</div>
		{#END}
	</div>
	<div class="card-body">
		<div class="row"><div class="col-sm-12">
				{#IF !empty($check)}
				 {§Das Konto wurde zu letzt &uuml;berpr&uuml;ft am}: {#DATE $check}
				{#END}
		</div></div>
	</div>
	<div class="card border-warning mb-3">
		<div class="card-header ">
			<div class="row">
				<div class="col-sm-3" style="border-bottom:1px solid #ddd;"><b>{§Dein Status}:</b></div>
				<div class="col-sm-9" style="border-bottom:1px solid #ddd;"><b>{$statuscaption}</b></div>
				<div class="col-sm-3"><b>{§n&auml;chster Schritt}:</b></div>
				<div class="col-sm-9">{#TPL $status}</div>
			</div>
		</div>
	</div>
	
	{#VAR $bar statusbar textclass=lanparty_statusbar_normal}
	
</div>
<div class="card ">
	<div class="card-header">
	{#TPL $text}
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-sm-12">
			 {#FORM class="form-horizontal"}
				 <div class="input-group">
					<span class="input-group-addon"><i class="fas fa-search"></i>&nbsp;{§Suche}:</span>
					 {#INPUT searchtext string $searchtext }
					</div>
			
			 {#END}
			 </div>
		</div>
		<div>&nbsp;</div>
		<div class="row">
			<div class="col table-responsive">
			{#IF $ViewInternalProfile}
				{#TABLE $user maxrows=200 class="table table-striped table-bordered table-hover no-footer" role="grid"}
					{#COL Name %name} {#IF $ViewInternalProfile}<a href="user.php?frame=viewsubject&amp;id={$id}">{%name}</a> {#ELSE}{%name}{#END}
					{#COL Vorname %givenname}{%givenname}
					{#COL Nachname %familyname}{%familyname}
					{#COL Clan %clan_name}{#IF empty($clan_name_url)}{%clan_name}{#ELSE}<a href="{%clan_name_url}">{%clan_name}</a>{#END}
					{#COL Status $status}{#TABLEDISPLAY lanparty_status $status}
					{#COL Sitzplatz $seat}{#IF !empty($seat_id)} <a href="seats.php?frame=block&amp;blockid={$blockid}&amp;ids[]={$id}">{%seat}</a>{#END}
					{#COL Events %eventcount}{%eventcount}
					{#OPTION "auf bezahlt setzen" setpaid right=$setpaidright}
					{#OPTION "auf angemeldet setzen<br/>und reservierten Sitzplatz vormerken" unsetpaid right=$setpaidright}
				{#END}
			{#ELSE}
			 {#TABLE $user maxrows=100 class="table table-striped table-bordered table-hover no-footer" role="grid"}
				{#COL Name %name} {#IF $ViewUserProfile}<a href="user.php?frame=viewsubject&amp;id={$id}">{%name}</a> {#ELSE}{%name}{#END}
				{#COL Clan %clan_name}{#IF empty($clan_name_url)}{%clan_name}{#ELSE}<a href="{%clan_name_url}">{%clan_name}</a>{#END}
				{#COL Status $status}{#TABLEDISPLAY lanparty_status $status}
				{#COL Sitzplatz $seat}{#IF !empty($seat_id)} <a href="seats.php?frame=block&amp;blockid={$blockid}&amp;ids[]={$id}">{%seat}</a>{#END}
			  {#END}
			 {#END}
			  </div>
		</div>
	</div>
</div>
{#END}

{#SUB statusbar}
<!-- Menue smallstatusbar as Frame-->
<div class="card border-success mb-3">
  <div class="card-header text-success">
	<h3 class="card-title">Teilnehmer</h3>
  </div>
	<div class="card-body">
	 <p class="card-text">{§maximal} {$max.count}</p>
			<div class="progress" style="margin-bottom: 2px; height: 35px;font-size: 1em;">
			  <div class="progress-bar bg-info" style="min-width: 1em; width: {$paid.width}%;">
				{$paid.count} <span class="sr-only">{§bezahlt}</span>
			  </div>
			  <div class="progress-bar progress-bar-striped bg-info" style="min-width: 1em; width: {$sold.width}%;">
				{$sold.count} <span class="sr-only">{§verkauft}</span>
			  </div>
			  <div class="progress-bar bg-warning progress-bar-striped" style="width: {$reg.width}%;">
			   {#IF !$hidereg}{$reg.count} {§angemeldet}{#END}  <span class="sr-only">{$reg.count} {§angemeldet}</span>
			  </div>
			  <div class="progress-bar bg-success" style="width: {$free.width}%;">
				{$free.count}<span class="sr-only">{$max.count} {§maximal}</span>
			  </div>
			</div>
			<div style="font-size:9px;">
				<div style="float: left;">
					{§Bezahlungen}
				</div>
				<div style="float: right;">
					{§freie Pl&auml;tze}
				</div>
			</div>
	</div>
</div>
{#END}

{#SUB smallstatusbar}
  {#VAR sub=statusbar hidereg=1 textclass=lanparty_statusbar_small}
{#END}

{#SUB smallstatusbartext}
<div align="{$align}">
<ul class="list-group">
  <li class="list-group-item"><span class="badge">{$max.count}</span>{§maximal}</li>
  <li class="list-group-item list-group-item-info"><span class="badge">{$paid.count}</span>{§bezahlt}</li>
  <li class="list-group-item list-group-item-info-striped"><span class="badge">{$sold.count}</span>{§verkauft}</li>
  <li class="list-group-item list-group-item-warning"><span class="badge">{$reg.count}</span>{§angemeldet}</li>
  <li class="list-group-item list-group-item-success"><span class="badge">{$free.count}</span>{§freie Pl&auml;tze}</li>
</ul>
</div>
{#END}

{#SUB userstatus}
<div class="card ">
	<div class="card-header">
		<small>{§Status}:<b>{$statuscaption}</b></small>
	</div>
	<div class="card-body">
		<small>{§n&auml;chster Schritt}:{#TPL $status}</small>
	</div>
</div>
{#END}

{#SUB register}
<div class="card ">
	<div class="card-header">
		{#TPL $text}
	</div>
	<div class="card-body">
		{#FORM register}
		<div class="row">
			<div class="col">
				<div class="card ">
					<div class="container-fluid my-2">
					{#TPL $agbtext}
					</div>
					<div class="container-fluid my-1">
						<div class="mx-auto" style="width: 400px;">
							<div class="checkbox">
							<label style="font-size: 1.2em">
								{#INPUT lanparty_participate checkbox}
								<span class="cr"><i class="cr-icon fa fa-check"></i></span> Einverstanden
							</label>
							</div>
						</div>
						<div class="mx-auto" style="width: 400px;">
						{#IF count($clans) > 0}
								<div class="row my-3">
									zus&auml;tzlich all deine Clan-Member anmelden
								</div>
								{#FOREACH $clans $name $id}
								<div class="row">
									<div class="checkbox">
										<label style="font-size: 1.1em">
											{#INPUT clan_$id checkbox_onclick }
											<span class="cr"><i class="cr-icon fa fa-check"></i></span>{%name}
										</label>
									</div>
								</div>
								{#END}
									<div id="submit1" style="display:none">
									<!-- Button trigger modal -->
									<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#CLANModalCenter">
										anmelden (Alt+S)+
									</button>
									<!-- Button trigger modal -->

									<!-- Modal -->
									<div class="modal fade" id="CLANModalCenter" tabindex="-1" role="dialog" aria-labelledby="CLANModalCenterTitle" aria-hidden="true">
										<div class="modal-dialog modal-dialog-centered" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title" id="CLANModalLongTitle">Alle CLAN Member Anmelden?</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<div class="modal-body">
													Bist du dir Sicher, das du alle CLAN Member zum Event anmelden möchtest?
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-danger" data-dismiss="modal">Nein. Close</button>
													{#SUBMIT $action}
												</div>
											</div>
										</div>
									</div>
									<!-- Modal Ende-->
								</div>
								<div id="submit2" style="display:block">
								{#SUBMIT $action}
								</div>
								<script>
								function onCheckBoxClick(caller) {
									var currentId = $(caller).attr('id');

									if (typeof window.submitState === 'undefined') {
										window.submitState = [currentId]
									} else {
										var index = window.submitState.indexOf(currentId);
										if (index >= 0) {
											window.submitState.splice(index, 1);
										} else {
											window.submitState.push(currentId)
										}
									}

									if (window.submitState.length === 0) {
										$('#submit1').hide()
										$('#submit2').show()
									} else {
										$('#submit1').show()
										$('#submit2').hide()
									}
								}
								</script>
						{#ELSE}
								<div id="submit3" style="display:block">
								{#SUBMIT $action}
								</div>
						{#END}
						</div>
					</div>	
				</div>
			</div>
		</div>
		{#END}
</div>
{#END}

{#SUB registerconsole}
<div class="card ">
	<div class="card-header">
		{#TPL $text}
	</div>

	{#FORM register}
		<div class="row my-2">
			<div class="col">
				<div class="card ">
					<div class="container-fluid my-2">
					{#TPL $agbtext}
					</div>	
					<div class="container-fluid my-1">
						<div class="mx-auto" style="width: 300px;">
							<div class="checkbox ">
							<label style="font-size: 1.2em">
								{#INPUT lanparty_participate_console checkbox}
								<span class="cr"><i class="cr-icon fa fa-check"></i></span> Einverstanden
							</label>
							</div>
							{#SUBMIT $action}
						</div>
					</div>	
				</div>
			</div>
		</div>
	{#END}
</div>
{#END}

{#SUB showpaid}
<div class="card ">
  <div class="card-header">
	<a href="lanparty.php?frame=paidloggraph&amp;x=1000&amp;y=500"><img src="lanparty.php?frame=paidloggraph&amp;x=600&amp;y=300" alt="Graph"/></a>
  </div>
	<div class="card-body">
		<div class="row">
			<div class="col-sm-12">
			  {#TABLE $entrys class="table table-striped table-bordered table-hover dataTable no-footer" role="grid" maxrows=100}
				{#COL Zeitpunkt %date}
				{#COL Username %user_name}
				{#COL Orga %orga_name}
			  {#END}
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12" align='left';>
				{#Action "Die Liste leeren" emptylog confirmation="Soll der Log wirklich geleert werden?"}
			</div>
		</div>	
	</div>	
</div>
{#END}

{#SUB sidebar}
<div class="card ">
  <div class="card-header">
	<h3 class="card-title">{§Teilnehmer}&nbsp;<span class="badge">{$users_max}</span></h3>
  </div>
  <div class="card-header">
	<a href="lanparty.php?menudir=27-59" target="_blank">{§Anmeldungen}</a>
  </div>
	<div class="card-body">
		<div class="row">
			<div class="col-3 col-sm-2 col-md-1">{§insgesamt}</div>
			<div class="col-9 col-sm-10 col-md-11">
				<div class="progress">
					<div class="progress-bar" role="progressbar" aria-valuenow="{$users_all}" aria-valuemin="0" aria-valuemax="{$users_max}" style="width: {$users_all}%;">{$users_all}</div>
				</div>
			</div>
			<div class="col-3 col-sm-2 col-md-1">{§angemeldet}</div>
			<div class="col-9 col-sm-10 col-md-11">
				<div class="progress">
					<div class="progress-bar bg-warning" role="progressbar" aria-valuenow="{$users_registered}" aria-valuemin="0" aria-valuemax="{$users_max}" style="width: {$users_registered}%;">{$users_registered}</div>
				</div>
			</div>
			<div class="col-3 col-sm-2 col-md-1">{§bezahlt}</div>
			<div class="col-9 col-sm-10 col-md-11">
				<div class="progress">
					<div class="progress-bar bg-info" role="progressbar" aria-valuenow="{$users_paid}" aria-valuemin="0" aria-valuemax="{$users_max}" style="width: {$users_paid}%;">{$users_paid}</div>
				</div>
			</div>
			<div class="col-3 col-sm-2 col-md-1">{§eingechecked}</div>
			<div class="col-9 col-sm-10 col-md-11">
				<div class="progress">
					<div class="progress-bar bg-info" role="progressbar" aria-valuenow="{$users_checked_in}" aria-valuemin="0" aria-valuemax="{$users_max}" style="width: {$users_checked_in}%;">{$users_checked_in}</div>
				</div>
			</div>
			<div class="col-3 col-sm-2 col-md-1">{§online}</div>
			<div class="col-9 col-sm-10 col-md-11">
				<div class="progress">
					<div class="progress-bar bg-info" role="progressbar" aria-valuenow="{$users_online}" aria-valuemin="0" aria-valuemax="{$users_max}" style="width: {$users_online}%;">{$users_online}</div>
				</div>
			</div>
			<div class="col-3 col-sm-2 col-md-1">{§offline}</div>
			<div class="col-9 col-sm-10 col-md-11">
				<div class="progress">
					<div class="progress-bar bg-info" role="progressbar" aria-valuenow="{$users_offline}" aria-valuemin="0" aria-valuemax="{$users_max}" style="width: {$users_offline}%;">{$users_offline}</div>
				</div>
			</div>
			<div class="col-3 col-sm-2 col-md-1">{§ausgechecked}</div>
			<div class="col-9 col-sm-10 col-md-11">
				<div class="progress">
					<div class="progress-bar bg-info" role="progressbar" aria-valuenow="{$users_checked_out}" aria-valuemin="0" aria-valuemax="{$users_max}" style="width: {$users_checked_out}%;">{$users_checked_out}</div>
				</div>
			</div>
		</div>
	</div>
  <div class="card-header">
	<a href="seats.php?menudir=27-61" target="_blank">{§Sitzplan}</a>
  </div>
	<div class="card-body">
		<div class="row">
			<div class="col-3 col-sm-2 col-md-1">{§reserviert}</div>
			<div class="col-9 col-sm-10 col-md-11">
				<div class="progress">
					<div class="progress-bar bg-danger" role="progressbar" aria-valuenow="{$seats_taken}" aria-valuemin="0" aria-valuemax="{$users_max}" style="width: {$seats_taken}%;">{$seats_taken}</div>
				</div>
			</div>
			<div class="col-3 col-sm-2 col-md-1">{§vorgemerkt}</div>
			<div class="col-9 col-sm-10 col-md-11">
				<div class="progress">
					<div class="progress-bar bg-warning" role="progressbar" aria-valuenow="{$seats_marked}" aria-valuemin="0" aria-valuemax="{$users_max}" style="width: {$seats_marked}%;">{$seats_marked}</div>
				</div>
			</div>
			<div class="col-3 col-sm-2 col-md-1">{§frei}</div>
			<div class="col-9 col-sm-10 col-md-11">
				<div class="progress">
					<div class="progress-bar bg-success" role="progressbar" aria-valuenow="{$seats_free}" aria-valuemin="0" aria-valuemax="{$users_max}" style="width: {$seats_free}%;">{$seats_free}</div>
				</div>
			</div>
		</div>
	</div>
  <div class="card-header">
	{§So, und jetzt kommen die wichtigen Daten}
  </div>
	<div class="card-body">
		{! textclass auf nicht bewirkt die nichtbenutzung der kleinen }
		{! Schriftart, welche fuers menu benutzt wird. }
		{#VAR sub=smallcounter textclass="nicht_" showdesc=1}
	</div>
</div>
{#END}

{#SUB smallcounter}
<div class="card border-success mb-3">
  <div class="card-header bg-success text-white">
	<h3 class="card-title">Status Counter</h3>
  </div>
	<div class="card-body">
			<div class="card ">
				<div style="float: left;">
					{§Bezahlt}
				</div>
				<div style="float: right;">
					{$users_paid}
				</div>
			</div>
			<div class="card ">
				<div style="float: left;">
					{§Besucher}
				</div>
				<div style="float: right;">
					{$users_on_party}
				</div>
			</div>
			<div class="card ">
				<div style="float: left;">
					{§Abendkasse}
				</div>
				<div style="float: right;">
					{$abendkasse}
				</div>
			</div>
			<div class="progress">
			  <div class="progress-bar bg-info" style="min-width: 2em; width:{$users_paid_width}%;">
			   {$users_paid} <span class="sr-only">{#IF $showdesc }{§Bezahlt}{#END}</span>
			  </div>
			  <div class="progress-bar bg-warning progress-bar-striped" style="min-width: 2em; width:{$users_on_party_width}%;">
				{$users_on_party} <span class="sr-only"> {#IF $showdesc }{§Besucher}{#END}</span>
			  </div>
			  <div class="progress-bar bg-danger progress-bar-striped" style="min-width: 2em; width:{$abendkasse_width}%;">
				{$abendkasse}<span class="sr-only">{#IF $showdesc }{§Abendkasse}{#END}</span>
			  </div>
			</div>
	</div>
</div>

{#END}

{#SUB countdown}
	{! es stehen auch zur Verf&uuml;gung: $days, $hours, $minutes, $seconds }
	<!-- Beispiel {%days} -->
	<!-- Alles {%msg} -->	
	<div class="card border-info mb-3" style="max-width: 18rem;">
		<div class="card-body">
		<h5 class="card-title text-center"><i class="far fa-clock fa-9x"></i></h5>
		<p class="card-text text-center">Noch {%days} Tage u. {%hours} Stunden </p>
		</div>
	</div>

{#END}

{#SUB countdown_small}
	<div id="countdown" class="card">
		<div class="card-header ">
		<p> <br> &nbsp; &nbsp; {%msg} &nbsp; &nbsp;  <br>
		</p>
		</div>
	</div>
{#END}

{#SUB smalluserstatus}
	{#IF $seat == "done"}
		<div class="card border-success mb-3">
		<div class="card-header bg-success text-white">
	{#ELSE}
		<div class="card border-warning mb-3">
		<div class="card-header bg-warning">
	{#END}
	<h3 class="card-title"><i class="fas fa-tasks fa-1x"></i>Checklist</h3>
	</div>
	<div class="list-group">
		<ul class="list-group">
		  {#IF $acc == "active"}
			<li class="list-group-item list-group-item-warning">1. <a href="user.php?frame=register" class="alert-link">{§Registrieren} <i class="fas fa-fw fa-plus"></i> </a></li>
		  {#ELSEIF $acc == "done"}
			<nobr><li class="list-group-item list-group-item-success">1. {§Page Account} <i class="fas fa-fw fa-check"></i></li></nobr>
		  {#END}
		  
		  {#IF $reg == "active"}
			<li class="list-group-item list-group-item-warning">2. <a href="lanparty.php?frame=register">{§Anmeldung} <i class="fas fa-fw fa-sign-in-alt"></i></a></li>
		  {#ELSEIF $reg == "done"}
			<nobr><li class="list-group-item list-group-item-success">2. {§Anmeldung} <i class="fas fa-fw fa-check"></i></li></nobr>
		  {#ELSE}
			<li class="list-group-item">2. {§Anmeldung}</li>
		  {#END}
		  
		  {#IF $pay == "active"}
			<li class="list-group-item list-group-item-warning">3. <a href="banktransfer.php">{§&Uuml;berweisung} <i class="far fa-fw fa-money-bill-alt"></i></a></li>
		  {#ELSEIF $pay == "done"}
			<nobr><li class="list-group-item list-group-item-success">3. {§&Uuml;berweisung} <i class="fas fa-fw fa-check"></i></li></nobr>
		  {#ELSE}
			<li class="list-group-item">3. {§&Uuml;berweisung}</li>
		  {#END}

		  {#IF $seat == "active"}
			<li class="list-group-item list-group-item-warning">4. <a href="seats.php">{§Sitzplatz} <i class="fas fa-fw fa-map-map-marker-alt"></i></a></li>
		  {#ELSEIF $seat == "done"}
			<li class="list-group-item list-group-item-success">4. {§Sitzplatz}:<br />
			<nobr><a href="seats.php">{$seatname}</a><i class="fas fa-fw fa-check"></i></li></nobr>
		  {#ELSE}
			<li class="list-group-item">4. {§Sitzplatz}</li>
		  {#END}
		  </ul>
	</div>
</div>
{#END}

{#SUB kontocheck}
	{#IF !empty($check)}
		<div class="card ">
		  <div class="card-header">{§Letzter Kontocheck}:</div>
		  <div class="card-body">
			{#DATE $check}
		  </div>
		</div>
	{#END} 
{#END}
