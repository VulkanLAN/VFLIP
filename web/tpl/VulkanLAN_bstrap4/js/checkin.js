// use webcam to capture user image
let camera_button = document.querySelector("#start-camera");
let video = document.querySelector("#preview-video");
let click_button = document.querySelector("#click-photo");
let canvas = document.querySelector("#photo-canvas");

let img_input = document.querySelector("input[name=imgdata]");

camera_button.addEventListener('click', async function() {
  let stream = await navigator.mediaDevices.getUserMedia({ video: true, audio: false });
	video.srcObject = stream;
  video.play();
});

click_button.addEventListener('click', function() {
  
  const ctx = canvas.getContext('2d');
  canvas.width = video.videoWidth;
  canvas.height = video.videoHeight;
  ctx.drawImage(video, 0,0);
  
  /*
  //canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
  canvas.width = video.videoWidth;
  canvas.height = video.videoHeight;
  //canvas.getContext('2d').drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
  canvas.getContext('2d').drawImage(video, 0, 0);
   */
  let image_data_url = canvas.toDataURL('image/jpeg');
  //canvas.style = "max-width: 100%; height: 100%";
  // data url of the image
  img_input.value = image_data_url.split(';base64,')[1];
  console.log(image_data_url);
});