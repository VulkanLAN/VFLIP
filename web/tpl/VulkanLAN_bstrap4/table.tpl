{#SUB default}
<div class="card ">
	<div class="card-header">
		<a href="table.php?frame=edittable" class="btn btn-success ml-1" role="button" data-toggle="tooltip" title="Table"><i class="far fa-plus-square"></i>&nbsp;add Table</a>
		</div>
		<div class="card-body table">
	{#TABLE $items class="table"}
	  {#COL Name %name}<a href="table.php?frame=viewtable&amp;id={$id}">{%name}</a>
	  {#COL Schreibrecht}{#RIGHT $edit_right edit}
	  {#COL Beschreibung %description}
	  {#FRAME edit edittable right=$edit_right}
	  {#OPTION L&ouml;schen deletetable}
	{#END}
</div>
{#END}

{#SUB edittable}
<div class="card ">
		{#FORM table}
		<div class="card-header">
			{#BACKLINK Zur&uuml;ck}&nbsp; {#SUBMIT Speichern}
		</div>
		<div class="card-body table">
		<table class="table table-sm">
		  <tr><td align="right">Name:</td><td>{#INPUT name name $name allowempty=0}</td></tr>
		  <tr><td align="right">Schreibrecht:</td><td>{#INPUT edit_right rights $edit_right}</td></tr>
		  <tr><td align="right">Beschreibung:</td><td>{#INPUT description longstring $description}</td></tr>
		  <tr><td colspan="2" align="center">{#SUBMIT Speichern}</td></tr>
		</table>
		{#INPUT id hidden $id}
		</div>
		{#END}
</div>
{#END}

{#SUB viewtable}
<div class="card ">
	<div class="card-header">
		<a href="table.php" class="btn btn-secondary ml-1" role="button" data-toggle="tooltip" title="back"><i class="fas fa-arrow-left"></i>&nbsp;&Uuml;bersicht</a>
		{#IF $allowedit}<a href="table.php?frame=edititem&amp;table_id={$table_id}" class="btn btn-success ml-1" role="button" data-toggle="tooltip" title="Add"><i class="far fa-plus-square"></i>&nbsp;add</a>{#END}
	</div>
	<div class="card-body table">
	{#TABLE $items class="table table-sm"}
	  {#IF $allowedit}<a href="table.php?frame=edititem&amp;table_id={$table_id}" class="btn btn-success ml-1" role="button" data-toggle="tooltip" title="Add"><i class="far fa-plus-square"></i>&nbsp;add</a>{#END}
	  {#COL Key $key}
	  {#COL Value %value}
	  {#COL Display %display}
	  {#FRAME edit edititem condition=$allowedit}
	  {#OPTION L&ouml;schen deleteitem condition=$allowedit}
	{#END}
	</div>
	<div class="card-body">
		<p style="width:60%">
		  &Uuml;ber den <i>Key</i> wird ein Datensatz intern angesprochen, die <i>Value</i> sieht der 
		  User in Dropdownboxen und den Wert von <i>Display</i> wann immer ein einzelner Wert angezeigt wird.
		  <i>Display</i> darf sowohl HTML als auch Templatecode enthalten.
		</p>
	</div>
</div>
{#END}

{#SUB edititem}
<div class="card ">
	{#FORM item}
		<div class="card-header">
		{#BACKLINK Zur&uuml;ck} {#SUBMIT Speichern}
		</div>
		<div class="card-body table">
		<table class="table table-sm">
		  <tr><td align="right">Key:</td><td>{#INPUT key name $key}</td></tr>
		  <tr><td align="right">Value:</td><td>{#INPUT value longstring $value allowempty=0}</td></tr>
		  <tr><td colspan="2">Display: <i>(darf HTML und Templatecode enthalten)</i></td></tr>
		  <tr><td colspan="2">{#INPUT display text $display}</td></tr>
		  <tr><td colspan="2">
			Wenn das Feld <i>Display</i> leer gelassen wird,<br />
			wird automatisch der Wert des Feldes <i>Value</i> verwendet.
		  </td></tr>
		  <tr><td colspan="2" align="center">{#SUBMIT Speichern}</td></tr>
		</table>
		{#INPUT id hidden $id}
		{#INPUT table_id hidden $table_id}
		</div>
		{#END}
</div>
{#END}