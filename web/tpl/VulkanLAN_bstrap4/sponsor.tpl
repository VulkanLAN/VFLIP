{! $Id: sponsor.tpl 2500 2018-05-08 10:46:25Z loom $ edit by naaux}

{#SUB default}
<div class="card ">
	<div class="card-header">	
		<div class="row">
		{#FOREACH $categoryvalue}
		<a href="#{$value}"class="btn btn-secondary ml-1" role="button"><i class="far fa-thumbs-up"></i>&nbsp;{$value}</a>
		{#END}
		<a href="#sponsor"class="btn btn-secondary ml-1" role="button"><i class="far fa-thumbs-up"></i>&nbsp;Unterst&uuml;tzung</a>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col">
			{#TPL $text}
			</div>
		</div>
	</div>
	<!-- Partner -->
	{#FOREACH $categoryvalue}
	{#IF $key == "partner"}
	<div class="card my-3">
	<div class="card-header">
	<h2 id="{$value}"> {$display_code} </h2>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col col-md-10">
			<ul class="list-unstyled">
				{#FOREACH $rows}
					{#IF $category == "partner" }
						<li class="media my-3">
							{#IF is_array($images)}
								{#FOREACH $images $i}
									<div align="center">  
									{#IF !empty($i.link)}<a href="sponsor.php?frame=click&amp;id={$i.id}&amp;location={$location}" target="_blank">{#END}
										{#IF empty($i.html)}
											<div class="d-none d-lg-block d-xl-block">{#DBIMAGE flip_sponsor_ads image $i.id $i.image_mtime border=0 width=350 height=100% sizeonlyforswf=0 class="align-self-start mr-3"}</div>
											<div class="d-none d-sm-block d-lg-none">{#DBIMAGE flip_sponsor_ads image $i.id $i.image_mtime border=0 width=150 height=100% sizeonlyforswf=0 class="align-self-start mr-3"}</div>
											<div class="d-block d-sm-none">{#DBIMAGE flip_sponsor_ads image $i.id $i.image_mtime border=0 width=100 height=100% sizeonlyforswf=0 class="align-self-start mr-3"}</div>
										{#ELSE}{$i.html}
										{#END}
									{#IF !empty($i.link)}</a>{#END}
									</div>
								{#END}
							{#END}
							<div class="media-body">
							  <h5 class="mt-0 mb-1">{%name}</h5>
							 {$description}
							</div>
						</li>
					 {#END}
				{#END}
				</ul>
				</div>
				<div class="col-md-1 col-lg-2"></div>
			</div>
		</div>
	</div>
	{#END}
	{#END}
	<!-- .rowend -->
	<!-- Eventpartner -->
	{#FOREACH $categoryvalue}
		{#IF $key == "eventpartner"}
		<div class="card my-2">
			<div class="card-header">
			<h2 id="{$value}"> {$display_code} </h2>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col col-md-10">
					<ul class="list-unstyled">
						{#FOREACH $rows}
						{#IF $category == "eventpartner" }
						  <li class="media my-3">
						  {#IF is_array($images)}
									{#FOREACH $images $i}
										<div align="center">  
										{#IF !empty($i.link)}<a href="sponsor.php?frame=click&amp;id={$i.id}&amp;location={$location}" target="_blank">{#END}
											{#IF empty($i.html)}
											<div class="d-none d-lg-block d-xl-block">{#DBIMAGE flip_sponsor_ads image $i.id $i.image_mtime border=0 width=250 height=100% sizeonlyforswf=0 class="align-self-start mr-3"}</div>
											<div class="d-none d-sm-block d-lg-none">{#DBIMAGE flip_sponsor_ads image $i.id $i.image_mtime border=0 width=150 height=100% sizeonlyforswf=0 class="align-self-start mr-3"}</div>
											<div class="d-block d-sm-none">{#DBIMAGE flip_sponsor_ads image $i.id $i.image_mtime border=0 width=100 height=100% sizeonlyforswf=0 class="align-self-start mr-3"}</div>
											{#ELSE}{$i.html}
											{#END}
										{#IF !empty($i.link)}</a>{#END}
										</div>
									{#END}
								{#END}
							<div class="media-body">
							  <h5 class="mt-0 mb-1">{%name}</h5>
							 {$description}
							</div>
						  </li>
						{#END}
						{#END}
					</ul>
					</div>
					<div class="col-md-1 col-lg-2"></div>
				</div>
			</div>
		</div>
		{#END}
	{#END}
	<!-- .rowend -->
	<!-- Hardwarepartner -->
	{#FOREACH $categoryvalue}
		{#IF $key == "hardwarepartner"}
		<div class="card my-2">
			<div class="card-header">
			<h2 id="{$value}"> {$display_code} </h2>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col col-md-10">
					<ul class="list-unstyled">
						{#FOREACH $rows}
						{#IF $category == "hardwarepartner" }
						  <li class="media my-3">
						  {#IF is_array($images)}
									{#FOREACH $images $i}
										<div align="center">  
										{#IF !empty($i.link)}<a href="sponsor.php?frame=click&amp;id={$i.id}&amp;location={$location}" target="_blank">{#END}
											{#IF empty($i.html)}
											<div class="d-none d-lg-block d-xl-block">{#DBIMAGE flip_sponsor_ads image $i.id $i.image_mtime border=0 width=250 height=100% sizeonlyforswf=0 class="align-self-start mr-3"}</div>
											<div class="d-none d-sm-block d-lg-none">{#DBIMAGE flip_sponsor_ads image $i.id $i.image_mtime border=0 width=150 height=100% sizeonlyforswf=0 class="align-self-start mr-3"}</div>
											<div class="d-block d-sm-none">{#DBIMAGE flip_sponsor_ads image $i.id $i.image_mtime border=0 width=100 height=100% sizeonlyforswf=0 class="align-self-start mr-3"}</div>
											{#ELSE}{$i.html}
											{#END}
										{#IF !empty($i.link)}</a>{#END}
										</div>
									{#END}
								{#END}
							<div class="media-body">
							  <h5 class="mt-0 mb-1">{%name}</h5>
							 {$description}
							</div>
						  </li>
						{#END}
						{#END}
					</ul>
					</div>
					<div class="col-md-1 col-lg-2"></div>
				</div>
			</div>
		</div>
		{#END}
	{#END}
	<!-- .rowend -->
	<!-- Regionalsponsor -->
	{#FOREACH $categoryvalue}
		{#IF $key == "regionalesponsoren"} 
		<div class="card my-2">
			<div class="card-header">
			<h2 id="{$value}"> {$display_code} </h2>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col col-md-10">
					<ul class="list-unstyled">
						{#FOREACH $rows}
						{#IF $category == "regionalesponsoren" }
						  <li class="media my-3">
						  {#IF is_array($images)}
									{#FOREACH $images $i}
										<div align="center">  
										{#IF !empty($i.link)}<a href="sponsor.php?frame=click&amp;id={$i.id}&amp;location={$location}" target="_blank">{#END}
											{#IF empty($i.html)}
											<div class="d-none d-lg-block d-xl-block">{#DBIMAGE flip_sponsor_ads image $i.id $i.image_mtime border=0 width=250 height=100% sizeonlyforswf=0 class="align-self-start mr-3"}</div>
											<div class="d-none d-sm-block d-lg-none">{#DBIMAGE flip_sponsor_ads image $i.id $i.image_mtime border=0 width=150 height=100% sizeonlyforswf=0 class="align-self-start mr-3"}</div>
											<div class="d-block d-sm-none">{#DBIMAGE flip_sponsor_ads image $i.id $i.image_mtime border=0 width=100 height=100% sizeonlyforswf=0 class="align-self-start mr-3"}</div>
											{#ELSE}{$i.html}
											{#END}
										{#IF !empty($i.link)}</a>{#END}
										</div>
									{#END}
								{#END}
							<div class="media-body">
							  <h5 class="mt-0 mb-1">{%name}</h5>
							 {$description}
							</div>
						  </li>
						{#END}
						{#END}
					</ul>
					</div>
					<div class="col-md-1 col-lg-2"></div>
				</div>
			</div>
		</div>
		{#END}
	{#END}
	<!-- .rowend -->
	<!-- Sponsore -->

	<div class="card my-2">
		<div class="card-header">
		<h2 id="sponsor"> Unterst&uuml;tzung </h2>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col col-md-10">
					<ul class="list-unstyled">
					{#FOREACH $rows}
					{#IF $category == "0" }
						  <li class="media my-3">
						  {#IF is_array($images)}
									{#FOREACH $images $i}
										<div align="center">  
										{#IF !empty($i.link)}<a href="sponsor.php?frame=click&amp;id={$i.id}&amp;location={$location}" target="_blank">{#END}
											{#IF empty($i.html)}
											<div class="d-none d-lg-block d-xl-block">{#DBIMAGE flip_sponsor_ads image $i.id $i.image_mtime border=0 width=250 height=100% sizeonlyforswf=0 class="align-self-start mr-3"}</div>
											<div class="d-none d-sm-block d-lg-none">{#DBIMAGE flip_sponsor_ads image $i.id $i.image_mtime border=0 width=150 height=100% sizeonlyforswf=0 class="align-self-start mr-3"}</div>
											<div class="d-block d-sm-none">{#DBIMAGE flip_sponsor_ads image $i.id $i.image_mtime border=0 width=100 height=100% sizeonlyforswf=0 class="align-self-start mr-3"}</div>
											{#ELSE}{$i.html}
											{#END}
										{#IF !empty($i.link)}</a>{#END}
										</div>
									{#END}
								{#END}
							<div class="media-body">
							  <h5 class="mt-0 mb-1">{%name}</h5>
							 {$description}
							</div>
						  </li>
						{#END}
					{#END}
					</ul>
				</div>
				<div class="col-md-1 col-lg-2"></div>
			</div>
		</div>
	</div>
	<!-- .rowend -->
</div>
{#END}

{#SUB smallbuttons}
	{#IF empty($images)}
	  <p>keine <a href="sponsor.php?frame=imagelist&amp;type=button">Buttons</a></p>
	{#ELSE}
	  <table class="table">
		{#FOREACH $images $i}
		  <tr><td>{#IF !empty($i.link)}<a href="sponsor.php?frame=click&amp;id={$i.id}&amp;location={$location}" target="_blank">{#END}
		  {#IF empty($i.html)}{#DBIMAGE flip_sponsor_ads image $i.id $i.image_mtime border=0 width=88 height=31 sizeonlyforswf=1}
		  {#ELSE}{$i.html}{#END}{#IF !empty($i.link)}</a>{#END}</td></tr>
		{#END}
	  </table>
	{#END}
{#END}

{#SUB header}
	
	  <a{#WHEN "$menu=='list'" " class=\"btn btn-md btn-primary\""} class="btn btn-md btn-secondary" role="button" href="sponsor.php?frame=list">Sponsoren</a> 
	  <a{#WHEN "$menu=='banner'" " class=\"btn btn-md btn-primary\""} class="btn btn-md btn-secondary" role="button" href="sponsor.php?frame=imagelist&amp;type=banner">Banner</a> 
	  <a{#WHEN "$menu=='button'" " class=\"btn btn-md btn-primary\""} class="btn btn-md btn-secondary" role="button" href="sponsor.php?frame=imagelist&amp;type=button">Buttons</a> 
	  <a{#WHEN "$menu=='log'" " class=\"btn btn-md btn-primary\""} class="btn btn-md btn-secondary" role="button" href="sponsor.php?frame=viewlog">Log</a>
	
{#END}

{#SUB list}
<div class="card">
		<script>
		$(document).ready(function(){
		  $("#sponsorInput").on("keyup", function() {
			var value = $(this).val().toLowerCase();
			$("#sponsorTable tr").filter(function() {
			  $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			});
		  });
		});
		</script>
	<div class="card-header">
	  {#VAR sub=header}
	  {#IF $edit}<a href="sponsor.php?frame=editsponsor" class="btn btn-md btn-success"><i class="fas fa-plus"></i>&nbsp;Neuer Sponsor</a>{#END}
	  </div>
	<div class="card-body">
		<p class="card-text">
		{#TPL $text}
		</p>
		<div class="row">
			<div class="col col-md-6">
			<form>
			<div class="form-group">
				<label for="search">Suche</label>
				<input class="form-control" id="sponsorInput" type="text" placeholder="Search..">
				<small id="searchHelp" class="form-text text-muted">Sponsoren durchsuchen...</small>
			</div>
			</form>
			
			</div>
		</div>
		 <div class="row">
		 <div class="col">
		  {#TABLE $rows id="sponsorTable" class="table table-sm table-striped"}
			
			{#COL Pub $public}<b>{#IF $public}<span style="color:green">Ja</span>{#ELSE}<span style="color:gray">Nein</span>{#END}</b>
			{#COL Prio $priority align="center"}
			{#COL Name %name}<a href="sponsor.php?frame=viewsponsor&amp;id={$id}"><b>{%name}</b></a>
			{#COL Homepage $homepage}<a href="{$homepage}">{$homepage}</a>
			{#COL Status $status}{#TABLEDISPLAY sponsor_status $status}
			{#COL Kategorie $category}{#TABLEDISPLAY sponsor_category $category}
			{#COL Zust&auml;ndig $responsibleuser}{%responsibleuser}
			{#COL Summe $amount}{%amount}
			{#COL "Banner<br />Buttons" align="center"}
			  <div class="btn-group" role="group" aria-label="BannerButton">
				<a href="sponsor.php?frame=imagelist&amp;type=banner&amp;sponsor_id={$id}" class="btn btn-outline-dark">{$banner}</a>
				<a href="sponsor.php?frame=imagelist&amp;type=button&amp;sponsor_id={$id}" class="btn btn-outline-dark">{$buttons}</a>
			  </div>
			{#COL Bearbeitet}{%user}<br />{#DATE $time}
			{#FRAME edit editsponsor condition=$edit}
			{#OPTION L&ouml;schen delete condition=$edit confirmation="Soll der Sponsor/die Sponsoren wirklich gel&ouml;scht werden? Die zugeh&ouml;rigen Banner und Buttons werden mit gel&ouml;scht!"}
		  {#END}
		</div>
		</div>
	</div>
</div>
{#END}

{#SUB viewsponsor}
<div class="card">
	<div class="card-header">	
		<a href="sponsor.php?frame=list" class="btn btn-md btn-secondary">&Uuml;bersicht</a>
	</div>
  <div class="card-body">
	<div class="row">
		<div class="col col-lg-6">
			<table class="table table-striped">   
			  <tr><td colspan="2" align="center"><b>&ouml;ffentliche Daten</b></td></tr>
			  <tr><td align="right">Name:</td><td>{$name}</td></tr>
			  <tr><td align="right">Homepage:</td><td>{$homepage}</td></tr>
			  <tr><td align="right">Kategorie:</td><td>{$category}</td></tr>
			  <tr><td align="right">Banner / Buttons:</td><td>
			  <div class="btn-group" role="group" aria-label="BannerButton">
			  <a href="sponsor.php?frame=imagelist&amp;type=banner&amp;sponsor_id={$id}" class="btn btn-outline-dark">{$images.banner}</a>
			  <a href="sponsor.php?frame=imagelist&amp;type=button&amp;sponsor_id={$id}" class="btn btn-outline-dark">{$images.button}</a>
			  </div>
			  </td></tr>
			  {#IF !empty($description)}
			  <tr><td align="center" colspan="2">{$description}</td></tr>
			  {#END}
			</table>
		</div>
		<div class="col col-lg-6">
        <table class="table table-striped">   
          <tr><td colspan="2" align="center"><b>interne Daten</b></td></tr>
          <tr><td align="right">Status:</td><td>{#WHEN $public "&ouml;ffentlich, "}{#TABLEDISPLAY sponsor_status $status}, Priorit&auml;t: {#TABLEDISPLAY sponsor_priority $priority}</td></tr>
		  <tr><td align="right">Zust&auml;ndig:</td><td>{$responsibleuser}</td></tr>
          <tr><td align="right">Email:</td><td>{$email}</td></tr>
          <tr><td align="right">Ansprechpartner:</td><td>{$contact}</td></tr>
          <tr><td align="right">Anschrift:</td><td>{$street}{#WHEN $city ", $city"}</td></tr>
          <tr><td align="right">Telefon:</td><td>{$phone}</td></tr>
		   <tr><td align="right">Gesponserte Summe:</td><td>{$amount}</td></tr>
          <tr><td colspan="2" align="right"><i>zuletzt {#IF $edit}<a href="sponsor.php?frame=editsponsor&amp;id={$id}">bearbeitet</a>{#ELSE}bearbeitet{#END} von {%user} am {#DATE $time}</i></td></tr>
        </table>
		</div>
	</div>
	<div class="row">
			<div class="col">    
		{#IF !empty($comment)}
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr><td colspan="2" align="center"><b>interne Bemerkung</b></td></tr>
		<tr><td colspan="2" align="center"><p align="left">{$comment}</p></td></td></tr>
		{#END}
		</div>
	</div>
  </div>
{#END}

{#SUB editsponsor}
<div class="col col-lg-10">
<div class="card">
		{#FORM sponsor}
			{#INPUT id hidden $id}
			<div class="card-header">	
			 {#BACKLINK Zur&uuml;ck} {#SUBMIT Speichern}
			</div>
		  <div class="card-body table">
			<table class="table">   
			 <tr><td colspan="2" align="center"><b>Status</b></td></tr>
			 <tr><td align="right">ist &ouml;ffentlich:</td><td>{#INPUT public checkbox $public} (f&uuml;hrt diesen Sponsor in der &ouml;ffentlich zug&auml;nglichen Sponsorenliste auf)</td></tr>
			 <tr><td align="right">Priorit&auml;t:</td><td>{#INPUT priority tabledropdown $priority param=sponsor_priority}</td></tr>
			 <tr><td align="right">Status:</td><td>{#INPUT status tabledropdown $status param=sponsor_status}</td></tr>
			 <tr><td align="right">Kategorie:</td><td>{#INPUT category tabledropdown $category param=sponsor_category}</td></tr>
			 <tr><td align="right">Zust&auml;ndig:</td><td>{#INPUT responsible dropdown $responsible param=$responsibleUsers}</td></tr>
			 <tr><td colspan="2" align="center">&nbsp;</td></tr>
			 
			 <tr><td colspan="2" align="center"><b>&ouml;ffentliche Daten</b></td></tr>
			 <tr><td align="right">Name:</td><td>{#INPUT name string $name allowempty=0}</td></tr>
			 <tr><td align="right">Homepage:</td><td>{#INPUT homepage string $homepage}</td></tr>
			 <tr><td align="right">Beschreibung:</td><td>{#INPUT description text $description}</td></tr>
			 <tr><td align="right">Banner:</td><td><a target="_blank" href="sponsor.php?frame=imagelist&amp;type=banner&amp;sponsor_id={$id}">bearbeiten</a></td></tr>
			 <tr><td align="right">Buttons:</td><td><a target="_blank" href="sponsor.php?frame=imagelist&amp;type=button&amp;sponsor_id={$id}">bearbeiten</a></td></tr>
			 <tr><td colspan="2" align="center">&nbsp;</td></tr>
			 
			 <tr><td colspan="2" align="center"><b>interne Daten</b></td></tr>
			 <tr><td align="right">Email:</td><td>{#INPUT email email $email}</td></tr>   
			 <tr><td align="right">Ansprechpartner:</td><td>{#INPUT contact string $contact}</td></tr>
			 <tr><td align="right">Stra&szlig;e:</td><td>{#INPUT street string $street}</td></tr>
			 <tr><td align="right">Plz/Ort:</td><td>{#INPUT city string $city}</td></tr>
			 <tr><td align="right">Telefon:</td><td>{#INPUT phone phone $phone}</td></tr>
			 <tr><td align="right">Gesponserte Summe:</td><td>{#INPUT amount decimal $amount}</td></tr>
			 <tr><td align="right">Bemerkung:</td><td>{#INPUT comment document $comment}</td></tr>
			 <tr><td colspan="2" align="center">{#SUBMIT Speichern}</td></tr>
			</table>
			</div>
		  {#END}
		  


</div>
</div>
{#END}

{#SUB imagelist}
<div class="card ">
	<div class="card-body">
	{#VAR sub=header}
	</div>
	<div class="card-body">
	{#TABLE $items class="table table-striped"}
	  {#COL Werbung* $show_as_ad}<b>{#IF $show_as_ad}<span style="color:green">Ja</span>{#ELSE}<span style="color:gray">Nein</span>{#END}</b>
	  {#COL Liste* $show_in_list}<b>{#IF $show_in_list}<span style="color:green">Ja</span>{#ELSE}<span style="color:gray">Nein</span>{#END}</b>  
	  {#COL Prio $priority align=center}
	  {#COL $imgtitle}
		{#IF !empty($html)}{$html}
		{#ELSEIF $type=='banner'}<a href="sponsor.php?frame=imagelist&amp;type=banner&amp;sponsor_id={$sponsor_id}">{#DBIMAGE flip_sponsor_ads image $id $image_mtime border=0 width=200 height=100% sizeonlyforswf=0}</a>
		{#ELSE}{#DBIMAGE flip_sponsor_ads image $id $image_mtime border=0 width=80 height=100% sizeonlyforswf=0}{#END}
	  {#COL Sponsor %sponsor}<a href="sponsor.php?frame=viewsponsor&amp;id={$sponsor_id}"><b>{%sponsor}</b></a><br /><a href="{%url}" target="_blank">{%url}</a>
	  {#COL "Counter*<br />letzter Reset" $views align="center"}
		{#IF $enable_counter}
		  <b>
			<span style="color:gray">{$views}</span>
			<span style="color:orange">{$sessions}</span>
			<span style="color:green">{$clicks}</span>
		  </b><br />
		  {#DATE $last_counter_reset}
		{#END}
	  {#COL Logs*}
		  {#IF $enable_view_log}<span style="color:gray">v</span>{#END}
		  {#IF $enable_session_log}<span style="color:orange">s</span>{#END}
		  {#IF $enable_click_log}<span style="color:green">c</span>{#END}
	  {#FRAME edit editimage condition=$edit}
	  {#OPTION L&ouml;schen deleteimage condition=$edit}
	  {#OPTION "reset Counter" resetcounter condition=$edit}
	{#END}
	<br />
	{#IF $edit}
	{#FORM action=sponsor.php method=get keepvals=0}
	  <table class="table" cellpadding="2" cellspacing="1"><tr>
		<td class="tdcont" style="padding: 4px;white-space:nowrap;">
		  {#IF empty($sponsors) and empty($sid)}
			Bilder k&ouml;nnen nur f&uuml;r Sponsoren hinzugef&uuml;gt werden.
		  {#ELSE}
			{#INPUT frame hidden editimage}
			{#INPUT type hidden $type}
			{$imgtitle} f&uuml;r
			{#IF empty($sid)}
			  {#INPUT sponsor_id dropdown $sid param=$sponsors}
			{#ELSE}
			  {#INPUT sid hidden $sid}<b>{%sponsor}</b>
			{#END}
			</td><td class="tdaction">
			{#SUBMIT hinzuf&uuml;gen}
		  {#END}
		</td>
	  </tr></table>
	{#END}
	{#END}
	<br />
	<p align="left" style="width:700px;">
	  <b>*Counter:</b><br />
	  <span style="color:gray">Views</span>: Die Anzahl der insgesamten Einblendungen des Bildes.<br />
	  <span style="color:orange">Sessions</span>: Die Anzahl der verschiedenen Sessions und damit in etwa der verschiedenen User, die das Bild betrachtet hat.<br />
	  <span style="color:green">Clicks</span>: Die Anzahl der Clicks auf das Bild.<br /><br />
	  <b>*Werbung:</b> Wird das Bild in der Bannerrotation/Buttonliste angezeigt?<br />
	  <b>*Liste:</b> Wird das Bild in der Sponsorenliste angezeigt?<br />
	</p>
	</div>
</div>
{#END}

{#SUB editimage}
<div class="card">
	{#FORM image}
	<div class="card-header">	
		<a href="sponsor.php?frame=list" class="btn btn-md btn-secondary">&Uuml;bersicht</a>
		{#BACKLINK Zur&uuml;ck}
		{#SUBMIT Speichern}
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col table">
			  {#INPUT id hidden $id}
			  {#INPUT type hidden $type}
			  {#INPUT sponsor_id hidden $sponsor_id}
			  <table class="table">   
			   <tr><td align="right">Sponsor:</td><td><b>{%sponsor}</b></td></tr>
			   <tr><td align="right">Werbung:</td><td>{#INPUT show_as_ad checkbox $show_as_ad} <i>(Anzeigen in Bannerrotation/Buttonliste)</i></td></tr>
			   <tr><td align="right">Sponsorenliste:</td><td>{#INPUT show_in_list checkbox $show_in_list} <i>(Anzeigen in der Sponsorenliste)</i></td></tr>
			   <tr><td align="right">Counter:</td><td>{#INPUT enable_counter checkbox $enable_counter}</td></tr>
			   <tr><td align="right"><span style="color:gray">ViewLog</span>:</td><td>{#INPUT enable_view_log checkbox $enable_view_log}</td></tr>
			   <tr><td align="right"><span style="color:orange">SessionLog</span>:</td><td>{#INPUT enable_session_log checkbox $enable_session_log}</td></tr>
			   <tr><td align="right"><span style="color:green">ClickLog</span>:</td><td>{#INPUT enable_click_log checkbox $enable_click_log}</td></tr>
			   <tr><td align="right">Priorit&auml;t:</td><td>{#INPUT priority tabledropdown $priority param=sponsor_priority}</td></tr>
			   <tr><td align="right">Bildlink:</td><td>{#INPUT link longstring $link}</td></tr>
			   <tr><td align="right">Bild*:</td><td>{#INPUT image image}</td></tr>
			   <tr><td align="right">HTML*:</td><td>{#INPUT html text $html}</td></tr>
			   <tr><td colspan="2" align="center">{#DBIMAGE flip_sponsor_ads image $id $image_mtime border=0}</td></tr>
			   <tr><td colspan="2" align="center">&nbsp;</td></tr>
			   <tr><td colspan="2" align="center">{#SUBMIT Speichern}</td></tr>
			   <tr><td colspan="2" align="center">
				<b>*</b> Es kann entweder ein Bild/SWF oder HTML-Code als &Uuml;berbringer der Werbebotschaft angegeben werden.<br /> 
				Wenn HTML-Code angegeben ist, wird das Bild nicht angezeigt.
			   </td></tr>
			   </table>
			  </div>
		</div>
	</div>
	{#END}
</div>
{#END}

{#SUB viewlog}
<div class="card">
	<div class="card-body">
	{#VAR sub=header}
	</div>
	<div class="card-body">
	  {#ACTION Leeren emptylog confirmation="Soll der Sponsorenlog wirklich geleert werden? Er ist unabh&auml;ngig von den Countern."}
	  {#TABLE $items maxrows=100 foundrows=$foundrows}
		{#COL Zeit<br/>Vorgang}{#DATE $time}<br />{$reason}
		{#COL Bild}{#DBIMAGE flip_sponsor_ads image $adid $admtime border=0}
		{#COL Typ<br/>Sponsor}{$type} {$location}<br /><b><a href="sponsor.php?frame=viewsponsor&amp;id={$sponsor_id}">{%sponsor}</a></b>
		{#COL IP}{$ip}
	  {#END}
	  </div>
 </div>
{#END}