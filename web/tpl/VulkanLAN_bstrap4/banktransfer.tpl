{#SUB default}
  <table width="80%">
    <tr>
      <td colspan="2">{#TPL $text}<br /><br /></td>
    </tr>
    {#IF $showdata}      
      <tr><td align="left">
        <table height="{$img.height}">
          <tr><td>Empf&auml;ngername:</td><td>{$dat.account_name}</td></tr>
          <tr><td>KontoNr.:</td><td>{$dat.account_no}</td></tr>
          <tr><td>Bank:</td><td>{$dat.account_bank_name}</td></tr>
          <tr><td>BLZ.</td><td>{$dat.account_bank_code}</td></tr>
          <tr><td>Betrag:</td><td>{$dat.money} {$dat.currency}</td></tr>
          <tr><td>Betreff 1:</td><td>{$dat.subject1}</td></tr>
          <tr><td>Betreff 2:</td><td>{$dat.subject2}</td></tr>
        </table>
      </td>
      <td><a href="banktransfer.php?frame=image"><img src="banktransfer.php?frame=smallimage" width="{$img.width}" height="{$img.height}" alt="&Uuml;berweisungsformular" border="0" /></a></td></tr>
    {#END}
  </table>
  {#IF !empty($configurl)}<div class="textfooter" align="right">Admin:<br/><a href="{$configurl}">{§vorgegebene Daten bearbeiten}</a></div>{#END}
{#END}