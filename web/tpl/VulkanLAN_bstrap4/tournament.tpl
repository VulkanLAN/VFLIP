{!
  $Id: tournament.tpl 1500 2015-02-12 13:40:59Z loom $ edit by naaux
}

{#SUB defaultlink}
<!-- Turnier Menue -->
		<a href="{$file}?frame=default"{#WHEN "empty($curframe) || $curframe == 'default'" " class='btn btn-md btn-primary'"} class="btn btn-md btn-secondary" role="button"><i class="fas fa-list "></i>&nbsp;Turnierliste</a>
		{#IF $logged_in=="true"}<a href="{$file}?frame=myinfo"{#WHEN "$curframe == 'myinfo'" " class='btn btn-md btn-primary'"} class="btn btn-md btn-secondary" role="button"><i class="fas fa-trophy"></i>&nbsp;Meine Turnierverwaltung</a>{#END}
{#END}

{#SUB tournamenthead}
<!-- erweitertes Turnier Menue, wird mit inc.tournament.tpl ergaenst -->
		<div class="btn-group" role="group">
		{#VAR sub=defaultlink}
		{#VAR sub=menu file=inc.tournament.tpl}
		</div>
{#END}

{#SUB AdminLink}
<!-- Turnier Admin Menue -->
	<div class="alert alert-info" role="alert" style="margin-bottom: 0px;">
		{#IF !empty($adminlinks)}
			<div><b>Admin Turnierverwaltung:</b></div>
			<div class="btn-group" aria-label="button group" role="group">
			 {#FOREACH $adminlinks}
				<a href="turnieradm.php?frame={$frame}&amp;id={$tournamentid}" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-cogs"></i>&nbsp;{$linktext}</a>
			 {#END}
			</div>
		{#END}
		{#IF !empty($tournamentfooter)}
			<div><b>Weitere Einstellungen:</b></div>
			<div class="btn-group" aria-label="button group" role="group">
			 {#FOREACH $tournamentfooter}
				<a href="{$url}" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-cogs"></i>&nbsp;{$linktext}</a>
			 {#END}
			</div>
		{#END}
	</div>
{#END}

{#SUB Coins}
<!-- Turnier Coins -->
	<div class="card ">
		<div class="card-header">
			<h3 class="card-title">
			{#IF isset($coins)}Dein aktuelles Guthaben betr&auml;gt{#IF !is_array($coins)} <strong>{$coins} {$currency}</strong>.<br />{#ELSE}:
			</h3>
		</div>
		<div class="card-body" align="left">
			<table class="table">
				{#FOREACH $coins}
				<tr>
				  <td>{#WHEN empty($currency) "Unbekannt" $currency}</td><td><strong>{$coins}</strong></td>
				</tr>
				{#END}
				</table>
				{#END}
				{#END}
		</div>
	</div>
{#END}

{#SUB default}
	<!-- Turnier Startseite -->
<div class="card ">
		<div class="card-header">
			<div class="row">
				<div class="col-9 col-sm-10 col-md-10"><!--Turnier default menue-->
					{#VAR sub=defaultlink}
					{#IF $logged_in=="true"}{#IF $coins > 0}<a href="tournament.php?frame=coinoverview" class="btn btn-secondary btn-sm" role="button"><i class="fab fa-bitcoin"></i>&nbsp;Turnier Kosten&uuml;bersicht</a>{#END}{#END}
				</div>
				<div class="col-3 col-sm-2 col-md-2"><!--Turniertypenauswahl -->
					{#IF count($turniertypen) > 1}
						<nav class="navbar navbar-right" style="margin-bottom: 0px;">
						  <ul class="nav nav-pills">
							<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fas fa-list-ul"></i>&nbsp;Turniere<span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									{#FOREACH $turniertypen}
									<li><a href="#{$caption}">{$caption}</a></li>
									{#END}
								</ul>
							</li>
						  </ul>
						</nav>
					{#END}
				</div>
			</div>
			{#IF !empty($adminlinks)}
			<div class="row"><!--Turnier admin menue-->
				{#VAR sub=AdminLink}
			</div>
			{#END}
		</div>
		<div class="card-body" align="center"><!--Turnierliste als frame-->
			{#VAR sub=_tournament_list}
		</div>
</div>
{#END}

{#SUB _tournament_list}
<!-- Turnier Liste -->
	{#FOREACH $turniertypen}
		<h1><a name="{$caption}"></a>{#IF $image}{#DBIMAGE $image alt=$caption}{#ELSE}{$caption}{#END}</h1>
		{#TABLE $turniere class="table table-hover table-striped"}
		  {#COL Spiel $game height=32 style="padding:1px 6px"}{#DBIMAGE flip_tournament_tournaments icon_small $id $mtime align=middle hspace=4px}{$gamelink}
		  {#COL Typ $teamsize align=center}{$teamsize}on{$teamsize}
		  {#COL Teams $count align=right}{$count}/{$maximum}
		  {#COL Status $status}
		  {#COL Start $start}{$timestr}
		  {#COL Kosten $coins align=right style="padding:1px 6px"}{$coins}&nbsp;{$currency}
		{#END}
	{#END}
{#END}

{#SUB myinfo}
<!-- Turnier Meine Info -->
<div class="card ">
		<div class="card-header">
			{#VAR sub=defaultlink}
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-6">
					<div class="card ">
						<div class="card-header">
							<h3 class="card-title">Meine Teams</h3>
						</div>
						<div class="card-body">
							{#TABLE $teams class="table table-sm table-hover table-striped"}
							  {#COL Name $teamname}
							  {#COL Mitglieder $count align=center}
							  {#COL Turniere $tcount align=center}{#IF $tcount == 1}{$tournament}{#ELSE}{$tcount} {§Turniere}{#END}
							{#END}
						</div>
					</div>
				</div>
				<div class="col-6">
				{#VAR sub=Coins}
				</div>
				<div class="col-12">
					<div class="card ">
						<div class="card-header">
							<h3 class="card-title">Meine Matches</h3>
							</div>
						<div class="card-body">
							{#TABLE $matches class="table table-sm table-hover table-striped"}
							  {#COL Turnier $tournament}
							  {#COL Runde $level}<a href="{$file}?frame=tree&amp;id={$tournament_id}#{$level}">{$level}</a>
							  {#COL Gegner $vs}
							  {#COL Status $status align=center}
							  {#COL Zeit $endtime}{#IF $endtime>0}{#IF $status=="offen"}<strong>{#IF $endtime>time() && $endtime < time()+30*60}<span class="important">{#DATE $endtime}</span>{#ELSE}{#DATE $endtime}{#END}</strong>{#ELSE}{#DATE $endtime}{#END}{#ELSE}{§unbegrenzt}{#END}
							  {#COL Server %server}
							{#END}
						</div>
					</div>
				</div>
				<div class="col-12">
					<div class="card ">
						<div class="card-header">
							<h3 class="card-title">Meine Turniere</h3>
						</div>
						<div class="card-body">
						{#FOREACH $turniere $turniertyp $caption}
							<div class="card ">
								<div class="card-header">
									<h3 class="card-title">{#WHEN empty($caption) "Sonstige" %caption}</h3>
								</div>
								<div class="card-body">
									{#TABLE $turniertyp class="table table-bordered table-sm table-hover table-striped"}
									  {#COL Platz $rank align=center}{#IF !empty($rank)}{$rank}.{#END}
									  {#COL Spiel $game}<a href="tournament.php?frame=overview&amp;id={$id}">{$game}</a>
									  {#COL Typ $teamsize align=center}{$teamsize}on{$teamsize}
									  {#COL Teams $count align=center}{$count}/{$maximum}
									  {#COL Status $status}
									  {#COL Start $start}{$timestr}
									  {#COL Kosten $coins align=center}{$coins} {$currency}
									{#END}
								</div>
							</div>
						{#END}
						</div>
					</div>
				</div>
			</div>
		</div>
</div>
{#END}

{#SUB coinoverview}
<!-- Turnier Kostenuebersicht -->
<div class="card ">
	<div class="card-header">
		{#VAR sub=defaultlink}
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12">
				{#VAR sub=Coins}
			</div>
			<div class="col-12">
				{#FOREACH $groups key=$currency}
				<div class="card ">
					<div class="card-header">
						<h3 class="card-title">{#WHEN empty($currency) "Unbekannt" $currency}</h3>
					</div>
					<div class="card-body">
						{#TABLE $turniere class="table table-sm table-hover table-striped"}
						  {#COL Turnier $name}<a href="tournament.php?frame=overview&amp;id={$id}">{%name}</a>
						  {#COL Kosten $coins}{$coins} {$currency}
						  {#ACTION + addcoins right=$adminright}
						  {#ACTION - remcoins right=$adminright}
						{#END}
					</div>
				</div>
				{#END}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB screenshots}
<!-- Turnier screenshot frame -->
	  <div align="center">
		{#IF empty($screens)}
			Keine Screenshots vorhanden.
		{#ELSE}
		  {#FOREACH $screens var=$name}
			<img src="image.php?name={$name}" alt="{%name}"/><br />
			<br />
		  {#END}
		{#END}
	  </div>
	  <a href="tournament.php?frame=tree&id={$tournamentid}">{§zum Turnierbaum}</a>
{#END}

{#SUB Overview}
<!-- Turnier Uebersicht -->
<div class="card ">
		<div class="card-header">
			<div class="row">
				{#VAR sub=tournamenthead}
			</div>
			{#IF !empty($adminlinks)}
			<div class="row">
					{#VAR sub=AdminLink}
			</div>
			{#END}
		</div>

		<div class="card-body">
			<div class="row" align="center">
				<div class="col-sm-12">{#DBIMAGE flip_tournament_tournaments icon $id $mtime width="95%" height="100%" style="max-width: 960px;"}</div>
				<div class="col-sm-12"><h3>Info</h3></div>
			</div>
			<div class="row" align="left">
				<div class="col-2 col-sm-2 col-md-2 col-lg-2"><b>Orga:</b></div>
				<div class="col-4 col-sm-3 col-md-2 col-lg-3">{#FOREACH $orgas $organame $orgaid}<a href="user.php?frame=viewsubject&amp;id={$orgaid}">{%organame}</a>{#MID}, {#END}</div>
				<div class="col-sm-2 col-md-4 col-lg-2"></div>
				<div class="col-2 col-sm-2 col-md-2 col-lg-2"><b>Kosten:</b></div>
				<div class="col-4 col-sm-3 col-md-2 col-lg-3">{$coins} {$currency}</div>
			</div>
			<div class="row" align="left">
				<div class="col-2 col-sm-2 col-md-2 col-lg-2"><b>Beginn:</b></div>
				<div class="col-4 col-sm-4 col-md-4 col-lg-3">{$begin}</div>
				<div class="		 col-sm-1 col-md-2 col-lg-2"></div>
				<div class="col-2 col-sm-2 col-md-2 col-lg-2"><b>Teams:</b></div>
				<div class="col-4 col-sm-3 col-md-2 col-lg-3">{$numteams}</div>
			</div>
			<div class="row" align="left">
				<div class="col-2 col-sm-2 col-md-2 col-lg-2"><b>Status:</b></div>
				<div class="col-4 col-sm-3 col-md-2 col-lg-3">{$status}</div>
				<div class="col-sm-2 col-md-4 col-lg-2"></div>
				<div class="col-2 col-sm-2 col-md-2 col-lg-2"><b>Spieler:</b></div>
				<div class="col-4 col-sm-3 col-md-2 col-lg-3">{$numplayers}</div>
			</div>
			<div class="row" align="left">
				<div class="col-2 col-sm-2 col-md-2 col-lg-2"><b>Modus:</b></div>
				<div class="col-4 col-sm-3 col-md-2 col-lg-3">{$mode}</div>
				<div class="col-sm-1 col-md-4 col-lg-2"></div>
				<div class="col-3 col-sm-3 col-md-2 col-lg-2"><b>Rundendauer:</b></div>
				<div class="col-3 col-sm-3 col-md-2 col-lg-3">{$round}</div>
			</div>
		</div>
		<div class="card-body">
			<div class="row" align="left">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
					<div class="card ">
						<div class="card-header">Beschreibung</div>
						<div class="card-body">{$TDescription}</div>
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
					<div class="card ">
						<div class="card-header">Teilnehmer</div>
						<div class="card-body">
						<!-- edit VulkanLAN Check User Status Paid -->

								{#IF is_numeric($anmeldeid)}
									{#IF $right_status_paid_true}
										<a href="{$file}?frame=getTeam&amp;id={$anmeldeid}" class="btn btn-md btn-secondary" role="button"><i class="fas fa-sign-in"></i>&nbsp;Anmelden</a><br />
									{#ELSE}
										Um am Turnier Teilnehmen zu können mu&szlig;t du bezahlt haben.
									{#END}
								{#ELSE}
								  {%anmeldeid}
                  {#IF $teamsize <= 1 and $angemeldet}
                    <br /><a href="{$file}?frame=overview&amp;id={$tournamentid}&amp;action=abmelden" class="btn btn-md btn-danger" role="button"><i class="fas fa-sign-in"></i>&nbsp;Abmelden</a>
                  {#END}
                  <br />

								{#END}

						</div><!-- fronktpl -->
						<div class="table-responsive-xl" style="overflow: hidden;">
						<table class="table table-bordered table-sm dataTable table-hover table-striped" role="grid">
							<thead>
							  <tr>
								<th>Team</th>
								{#IF $teamsize > 1}
									{#IF $anmeldestatus}
									<th>Aktion</th>
									{#END}
								{#END}
								<th>Nickname</th>
								<th>Ingame Nickname</th>
							  </tr>
							 </thead>
							 <tbody>
								{#FOREACH $teams index=$i}
								  <tr class="{#IF ($i % 2 == 1)}row2{#ELSE}row1{#END}">
									<td>{#IF !empty($adminlinks)}
											<a href="user.php?frame=editchilds&amp;id={$id}" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-wheelchair"></i>&nbsp; {$team}</a>
											{#ELSE}
											<b>{$team}</b>
										{#END}</td>
									{#IF $teamsize > 1}
									{#IF $anmeldestatus}<td><i>{$action}</i></td>{#END}
									{#END}
									<td>
									  {#FOREACH $players index=$i}<a href="user.php?frame=viewsubject&amp;id={$userid}">{$nick}</a>{#MID}, {#END}
									</td>
									 <td>
										<!-- alra Anpassung - ingame nickname Anzeige Turnier Teilnehmerliste -->
									  {#FOREACH $players index=$i}<a href="user.php?frame=viewsubject&amp;id={$userid}">{$ingame_nick_name}</a>{#MID}, {#END}
									</td>
								  </tr>
								{#END}
							</tbody>
						</table>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>
{#END}

{#SUB Results}
<!-- Turnier Platzierung -->
<div class="card ">
	<div class="card-header">
			{#VAR sub=tournamenthead}
	</div>
	<div class="card-body" align="center">
		<div class="card ">
			<div class="card-header">
				<h3 class="card-title">Platzierung</h3>
			</div>
			<div class="card-body" align="center">
				<div class="row" align="center">
					<div class="col-1 col-sm-1 col-md-1 col-lg-1"></div>
					<div class="col-3 col-sm-3 col-md-3 col-lg-3"></div>
					<div class="col-3 col-sm-3 col-md-3 col-lg-3">{#IF empty($isFirst)}{$first}{#ELSE}<strong>{$first}</strong>{#END}</div>
					<div class="col-3 col-sm-3 col-md-3 col-lg-3"></div>
					<div class="col-1 col-sm-1 col-md-1 col-lg-1"></div>
				</div>
				<div class="row" align="center">
					<div class="col-1 col-sm-1 col-md-1 col-lg-1"></div>
					<div class="col-3 col-sm-3 col-md-3 col-lg-3">{#IF empty($isSecond)}{#IF empty($second)}&nbsp;{#ELSE}{$second}{#END}{#ELSE}<strong>{$second}</strong>{#END}</div>
					<div class="col-3 col-sm-3 col-md-3 col-lg-3" style="background-color: #ccc;"><strong>1</strong></div>
					<div class="col-3 col-sm-3 col-md-3 col-lg-3">{#IF empty($isThird)}{#IF empty($third)}&nbsp;{#ELSE}{$third}{#END}{#ELSE}<strong>{$third}</strong>{#END}</div>
					<div class="col-1 col-sm-1 col-md-1 col-lg-1"></div>
				</div>
				<div class="row" align="center">
					<div class="col-1 col-sm-1 col-md-1 col-lg-1"></div>
					<div class="col-3 col-sm-3 col-md-3 col-lg-3" style="background-color: #ccc;"><strong>2</strong></div>
					<div class="col-3 col-sm-3 col-md-3 col-lg-3" style="background-color: #ccc;">&nbsp;</div>
					<div class="col-3 col-sm-3 col-md-3 col-lg-3" style="background-color: #ccc;"><strong>3</strong></div>
					<div class="col-1 col-sm-1 col-md-1 col-lg-1"></div>
				</div>
				<div class="row" align="left">
					{#FOREACH $rankings}
					<div class="col-2 col-sm-2 col-md-2 col-lg-2" align="right">{$rank}.</div>
					<div class="col-3 col-sm-3 col-md-3 col-lg-3">{#IF empty($isInTeam)}{$name}{#ELSE}<strong>{$name}</strong>{#END}</div>
					<div class="col-7 col-sm-7 col-md-7 col-lg-7">&nbsp;</div>
					{#END}
				</div>
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB getTeam}
<!-- Turnier Team zum Turnier anmelden u. zusammenstellen -->
<div class="card">
	<div class="card-header">
		<div class="row">
			<div class="col-6" align="left"><h3 class="card-title" style="padding-top: 10px;">Turnier - Teamanmeldung</h3>
			</div>
			<div class="col-6" align="right"><a href="{$file}?frame=overview&amp;id={$tournamentid}" class="btn btn-md btn-secondary" role="button"><i class="fas fa-arrow-left"></i>&nbsp;zur&uuml;ck</a>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="card ">
			<div class="card-header">
				<h3 class="card-title">{#IF isset($coins)}Dieses Turnier kostet <span class="important">{$tcoins} {$currency}</span>.</h3>
			</div>
			{#VAR sub=Coins}
			{#END}
			<div class="card-body">
				<div class="row">
					<div class="col-10">
					{#IF !$oldsingleplayer}
						<strong>Neues</strong> Turnierteam:
						{#FORM addTeam action=$action}
							{#IF $newsingleplayer}
								<h4>{%nickname}</h4>
							{#END}
						 {#INPUT teamname $nametype $nickname allowempty=0}<br />
							{#IF !empty($ligaid)}
								{$liga}-ID ("none"=>noch keine ID):<br />
								{#INPUT ligaid string $ligaid}<br />
							{#END}
						  {#SUBMIT Anmelden}
						{#END}
					{#END}
					</div>
					<div class="col-10">
					{#IF !$newsingleplayer}
						{#FOREACH $oldteam}
							<strong>Bestehendes</strong> Turnierteam:
							{#FORM fixsize action="$file?frame=$frame&amp;id=$tournamentid"}
								{#IF $oldsingleplayer}
								  {#FOREACH $teams $teamname $teamid}
								  <h4>{%teamname}</h4>
								  {#END}
								{#END}
							  {#INPUT teamid $idtype $teamid param=$teams allowempty=0}<br />
							  {#IF !empty($newligaid)}
								{$liga}-ID ("none"=>noch keine ID):<br />
								{#INPUT ligaid string $ligaid}<br />
							  {#END}
							  {#SUBMIT Anmelden}
							{#END}
						{#END}
					{#END}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB editTeam}
<!-- Turnier Teamaendern -->
<div class="card">
	<div class="card-header">
		<div class="row">
			<div class="col-6" align="left"><h3 class="card-title" style="padding-top: 10px;">Turnier - Teamverwaltung</h3>
			</div>
			<div class="col-6" align="right"><a href="{$file}?frame=overview&amp;id={$id}" class="btn btn-md btn-secondary" role="button"><i class="fas fa-arrow-left"></i>&nbsp;zur&uuml;ck</a>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-4 col-sm-4 col-md-3 col-lg-3">Weitere Seiten:</div>
			<div class="col-6 col-sm-6 col-md-7 col-lg-7">
				{#FOREACH $links}
				<a href="{$file}?frame={$frame}&amp;id={$id}&amp;newteamid={$newteamid}" class="btn btn-md btn-secondary" role="button"><i class="fas fa-user"></i>&nbsp;{$text}</a>
				<p></p>
				{#END}
			</div>
			{#IF !$singleplayer}
			<div class="col-12"><hr></div>
			<div class="col-4 col-sm-4 col-md-3 col-lg-3">Team umbenennen:</div>
			<div class="col-6 col-sm-6 col-md-7 col-lg-7">
				{#FORM}
					{#INPUT action hidden renTeam}
					{#INPUT teamname string $teamname}
					{#SUBMIT &auml;ndern shortcut=e}
				{#END}
			</div>
			{#END}
			{#IF !empty($liga)}
			<div class="col-12"><hr></div>
			<div class="col-4 col-sm-4 col-md-3 col-lg-3">{$liga}-ID &auml;ndern:</div>
			<div class="col-6 col-sm-6 col-md-7 col-lg-7">
				{#FORM}
					{#INPUT action hidden changeliga}
					{$liga}-ID ("none"=>noch keine ID):<br />
					{#INPUT ligaid string $ligaid}<br />
					{#SUBMIT &auml;ndern shortcut=l}
				{#END}
			</div>
			{#END}
			<div class="col-12"><hr></div>
			<div class="col-4 col-sm-4 col-md-3 col-lg-3">Turnier verlassen:</div>
			<div class="col-6 col-sm-6 col-md-7 col-lg-7">
				{#FORM action="$file?frame=overview&amp;id=$tournamentid"}
					{#INPUT action hidden remTeam}
					{#INPUT id hidden $id}
					{#INPUT newteamid hidden $newteamid}
					{#SUBMIT "Team aus dem Turnier nehmen" shortcut=r}
				{#END}
			</div>
			{#IF $deleteable=="true"}
			<div class="col-12"><hr></div>
			<div class="col-4 col-sm-4 col-md-3 col-lg-3">Team aufl&ouml;sen:</div>
			<div class="col-6 col-sm-6 col-md-7 col-lg-7">
				{#FORM action="$file?frame=overview&amp;id=$tournamentid"}
					{#INPUT action hidden delteam}
					{#INPUT id hidden $id}
					{#INPUT newteamid hidden $newteamid}
					<i class="far fa-trash-alt"></i>&nbsp;<input type="submit" value="Team l&ouml;schen (aus dem System)" class="btn btn-md btn-danger" role="button">
				{#END}
			</div>
			{#END}
		</div>
	</div>
</div>
{#END}

{#SUB Playerlist}
<!-- Turnier Spielerliste -->
	{#TABLE $candidates maxrows=50 class="table table-hover table-striped table-bordered datatable"}
	  {#COL Nickname %nick}
	  {#OPTION %submitval $action}
	{#END}
{#END}

{#SUB Playerlist_bak}
<!-- Turnier Spielerliste_bak -->
	<form method="post" action="{$file}?frame={$frame}&amp;id={$tournamentid}">
	<select class="edit" name="playerid[]"{$select}>
	  {#FOREACH $candidates $nick $id}
	  <option value="{$id}">{%nick}</option>
	  {#END}
	</select>
	<br />
	<input type="hidden" name="action" value="{$action}">
	<input class="button" type="submit" value="{$submitval}">
	</form>
{#END}

{#SUB AddPlayer}
<!-- Turnier Teamspieler hinzufuegen -->
<!-- ab hier fortsetzen-->
<div class="card">
	<div class="card-header">
		<div class="row">
			<div class="col-4 col-sm-4 col-md-3 col-lg-3" align="left"><h3 class="card-title" style="padding-top: 10px;">Teamspieler hinzuf&uuml;gen</h3>
			</div>
			<div class="col-8 col-sm-8 col-md-9 col-lg-9" align="right">
				<a href="{$file}?frame=editTeam&amp;id={$tournamentid}&amp;newteamid={$newteamid}" class="btn btn-sm btn-secondary" role="button"><i class="fas fa-arrow-left"></i>&nbsp;zur&uuml;ck zur Teamverwaltung</a>
				<a href="{$file}?frame=overview&amp;id={$tournamentid}" class="btn btn-sm btn-secondary" role="button"><i class="fas fa-arrow-up"></i>&nbsp;zur Turnierseite</a>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12">Es k&ouml;nnen noch <strong>{$free}</strong> Spieler in diesem Team teilnehmen.</div>
			<div class="col-12"><hr></div>
			{#IF !empty($invited)}
				<div class="col-4 col-sm-4 col-md-3 col-lg-3">Eingeladen wurden:</div>
				<div class="col-6 col-sm-6 col-md-7 col-lg-7">
					{#FOREACH $invited index=$i}
						{#IF $i>1}, {#END}{%nickname}{#IF ($i % 3 == 0)}<br />{#END}
					{#END}
				</div>
				<div class="col-12"><hr></div>
			{#END}
			{#IF !empty($join)}
				<div class="col-4 col-sm-4 col-md-3 col-lg-3">Anfragen von:</div>
				<div class="col-6 col-sm-6 col-md-7 col-lg-7">
				{#TABLE $join class="table"}
					{#COL Nickname %nickname}
					{#OPTION hinzuf&uuml;gen addPlayer}
					{#OPTION l&ouml;schen reminvite}
				{#END}
				</div>
				<div class="col-12"><hr></div>
			{#END}
		</div>
		<div class="card ">
			<div class="card-header">Spieler aus dem Team einladen:</div>
			<div class="card-body">
				<div class="row">
					{#FOREACH $searchform}
					<div class="col-4 col-sm-4 col-md-3 col-lg-3">&nbsp; Teamspieler suchen</div>
					<div class="col-5 col-sm-5 col-md-6 col-lg-6">
							{#FORM action="$file?frame=saveTeam&amp;id=$tournamentid&amp;newteamid=$newteamid&amp;teamid=$teamid"}
								{#INPUT search string $search}
								<i class="fas fa-search"></i>{#SUBMIT suchen}
							{#END}

					</div>
					{#END}
					<div class="col-2 col-sm-2 col-md-2 col-lg-2">
						{#FOREACH $teamlinks}
							<a href="{$file}?frame=saveTeam&amp;id={$tournamentid}&amp;newteamid={$newteamid}&amp;teamid={$teamid}"class="btn btn-md btn-secondary" role="button"><i class="fas fa-users"></i>&nbsp;{%teamname}</a><br />
						{#END}
					</div>
					<div class="col-12"><hr></div>
					<div class="col-12">
					{#VAR sub=Playerlist}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB RemPlayer}
<!-- Turnier Teamspieler entfernen -->
<div class="card">
	<div class="card-header">
		<div class="row">
			<div class="col-4 col-sm-4 col-md-3 col-lg-3" align="left"><h3 class="card-title" style="padding-top: 10px;">Teamspieler entfernen</h3>
			</div>
			<div class="col-8 col-sm-8 col-md-9 col-lg-9" align="right">
				<!--muss noch angepasst werden -->
				<!--passender return/forward Button, einheitlich dem TurnierMenue -->
				<!--<a href="{$file}?frame=editTeam&amp;id={$tournamentid}&amp;newteamid={$newteamid}" class="btn btn-sm btn-secondary" role="button">
				<i class="fas fa-arrow-left"></i>&nbsp;zur&uuml;ck zur Teamverwaltung</a>
				-->
				<a href="{$file}?frame=overview&amp;id={$tournamentid}" class="btn btn-sm btn-secondary" role="button"><i class="fas fa-arrow-up"></i>&nbsp;zur Turnierseite</a>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12">
			{#VAR sub=Playerlist}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB PointForm}
<!-- Turnier Ergebnisse eintragen -->
<div class="card">
	{#WITH $pointinput}
	<div class="card-header">
		<div class="row">
		{#IF !empty($servername)}<h3>Server: {%servername} ({$ip})</h3>{#END}
		</div>
		<div class="row">
	</div>
		{#FORM addpoints action="$file?frame=tree&amp;id=$tournamentid"}
		<div class="card-header">
			<div class="row">
				<div>
					<a href="{$file}?frame=tree&amp;id={$tournament_id}" class="btn btn-sm btn-secondary" role="button"><i class="fas fa-arrow-left"></i>&nbsp;zur&uuml;ck</a>
					<a href="{$file}?frame=overview&amp;id={$tournamentid}" class="btn btn-sm btn-secondary" role="button"><i class="fas fa-arrow-up"></i>&nbsp;zur Turnierseite</a>
				</div>
			</div>
		</div>
		<div class="card-body">
		<div class="row">
			<div class="col-8">
			<div class="row"><h3>Turnierergebnis eintragen</h3></div>
			{#IF $type == 'dm'}
				<div class="row">
					<div class="col-4 col-sm-4 col-md-3 col-lg-2">{§Gewinner}:</div>
					<div class="col-8 col-sm-8 col-md-9 col-lg-10">{#INPUT points1 dropdown $points1 param=$teams}</div>
					<div class="col-12"><hr></div>
				{#INPUT points2 hidden 1}
				</div>
			{#ELSE}
			<div class="row">
				<table rules="none" border="0" cellspacing="0" cellpadding="3">
				  <tr>
					<td width="45%" align="center">{#IF $isadmin=="true"}{#INPUT team1 dropdown $team1id param=$combatants}{#ELSE}{$team1}<br/>{#IF $ready1}{§bereit seit} {#DATE $ready1}{#ELSE}{§nicht bereit}{#END}{#END}</td><td width="10%" align="center" rowspan="2">vs.</td><td width="45%" align="center">{#IF $isadmin=="true"}{#INPUT team2 dropdown $team2id param=$combatants}{#ELSE}{$team2}<br/>{#IF $ready2}{§bereit seit} {#DATE $ready2}{#ELSE}{§nicht bereit}{#END}{#END}</td>
				  </tr>
				  <tr>
					<td align="center">{#INPUT points1 integer $points1}</td><td align="center">{#INPUT points2 integer $points2}</td>
				  </tr>
				</table>
			</div>
			{#END}{!if $type != dm}
			{#IF $cancomment=="true"}
				<div class="row">
				<form>
					<div class="form-group row">
						<label for="inputKommentar" class="col-sm-6 col-form-label">Kommentar:</label>
					<div class="col-sm-10">
							{#INPUT comment string $comment}
					</div>
					</div>
				</form>
				</div>
			{#END}
				<div class="row">
				<div class="col-12">letzter Bearbeiter: {%editor}</div>
				<div class="col-12"><hr></div>
				</div>
			{#IF $enabled1 || $enabled2}
				<div class="row">
					<div class="col-12">
					<h2>Screenshot Upload</h2>
					({%types})<br />
					<table rules="none" border="0" cellspacing="0" cellpadding="3">
					<tr><td>Screenshot1: {#INPUT screen1 file $screen1 enabled=$enabled1}{$screen1}</td></tr>
					<tr><td>Screenshot2: {#INPUT screen2 file $screen2 enabled=$enabled2}{$screen2}</td></tr>
					</table>
					</div>
					<div class="col-12"><hr></div>
				</div>
			{#END}
			<div class="row">
			{#INPUT matchid hidden $matchid allowempty=0}
			{#SUBMIT Eintragen}
			</div>
		</div><div class="col-4"></div></div></div>
		{#END}

		{#FOREACH $admin}
		<div class="alert alert-info" role="alert" style="margin-bottom: 0px;">
		Admin Menue:
				{#FORM confirmation="$confirm"}{!removed for icon uplod: action="$file?frame=$frame&amp;id=$tournament_id&amp;matchid=$id"}
				{#INPUT  confirmation_uri hidden $confirm_uri}
				{#INPUT action hidden $action}
				{#INPUT matchid hidden $matchid}
				{#SUBMIT $text}
				{#END}
		</div>
		{#END}

	{#END}
</div>
{#END}



{#SUB edittime}
<!-- Turnier Zeit aendern -->
<div class="card">
	<div class="card-header">
		<div class="row">
			<div class="col-4 col-sm-4 col-md-3 col-lg-3" align="left"><h3 class="card-title" style="padding-top: 10px;">Turnierzeit &auml;ndern</h3>
			</div>
			<div class="col-8 col-sm-8 col-md-9 col-lg-9" align="right">
				<a href="{$file}?frame=tree&amp;id={$tournamentid}" class="btn btn-sm btn-secondary" role="button"><i class="fas fa-arrow-left"></i>&nbsp;zur&uuml;ck</a>
				<a href="{$file}?frame=overview&amp;id={$tournamentid}" class="btn btn-sm btn-secondary" role="button"><i class="fas fa-arrow-up"></i>&nbsp;zur Turnierseite</a>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
		{#FORM action="$file?frame=tree&amp;id=$tournamentid"}
			{#INPUT action hidden settime}
			{#INPUT matchid hidden $matchid}
			<div class="col-4 col-sm-4 col-md-3 col-lg-2">Zeit:</div>
			<div class="col-8 col-sm-8 col-md-9 col-lg-10">{#INPUT endtime date $endtime}</div>
			<div class="col-4 col-sm-4 col-md-3 col-lg-2" align="right">{#INPUT nextround checkbox $nextround} </div>
			<div class="col-8 col-sm-8 col-md-9 col-lg-10">nachfolgende Runden entsprechend &auml;ndern (Winner- und Loserbracket!)</div>
			<div class="col-4 col-sm-4 col-md-3 col-lg-2">{#SUBMIT setzen}</div>
		{#END}
		</div>
	</div>
</div>
{#END}

{#SUB tree}
<!-- Turnierbaum -->
<!-- siehe tounament.php (frametree)
	"treetypefile" = "inc.tree.default.tpl";
	"treetypestyles" = "inc.tree.default.styles.tpl";
 -->
<div class="card ">
	<div class="card-header">
		<div class="row">
			{#VAR sub=tournamenthead}
		</div>
		{#IF !empty($adminlinks)}
		<div class="row">
				{#VAR sub=AdminLink}
		</div>
		{#END}
	</div>
	<div class="card-body">
			{#IF !$showgames}
			<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12"><h3>Bisher hat das Turnier noch nicht begonnen.</h3></div>
			</div>
			{#ELSE}
				{#IF $toolate=="true"}
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12">
						<ul class="list-group">
							<li class="list-group-item list-group-item-danger">
								<i class="fas fa-exclamation-triangle"></i> Die Spielzeit f&uuml;r diese Runde ist bereits abgelaufen: <br>
																		Ergebnismeldung bzw. R&uuml;cksprache halten beim zust&auml;ndigen Turnierorga.
							</li>
						</ul>
					</div>
				</div>
				{#END}














				{#IF true}<!-- Tournament Tree Winnerbracket -->
				<div class="row">
					<div class="col-sm-12 col-md-8 col-lg-6">
					{#FORM jumpto}<!-- Suche-->
						<div class="form-group">
						<div class="form-group mx-sm-6 mb-4">
							 <label for="teamname">Teamname:</label>
							{#INPUT teamname string $teamname}
						</div>
						{#SUBMIT finden}
						</div>
					{#END}</div>
					<div class="col-md-4 col-lg-6"></div>
					<!-- Suche ausgabe-->
					<div class="col-12 col-sm-12 col-md-12 col-lg-12">{#FOREACH $teamids $id $name index=$i}<a href="#{$id}">{%name}</a>{#MID}, {#END}</div>
					<!--Jump to -->
					<div class="col-12 col-sm-12 col-md-12 col-lg-12">
						<a href="#winner" class="btn btn-md btn-secondary" role="button"><i class="fas fa-arrow-right "></i>&nbsp;Winnerbracket</a>
						{#IF !empty($lcoldef)}<a href="#loser" class="btn btn-md btn-secondary" role="button"><i class="fas fa-arrow-right"></i>&nbsp;Loserbracket</a>{#END}
					</div>
					<div class="col-12 col-sm-12 col-md-12 col-lg-12">
						{#WHEN "!empty($groups)" "<h3>"}<!-- muss noch angepasst werden. nur fuer Gruppen Turnier? wtf... -->
							{#FOREACH $groups}<a href="#{$title}">{$title}</a>{#MID}&middot;{#END}
						{#WHEN "!empty($groups)" "</h3>"}
					</div>
				</div>










				<div style="overflow-x:scroll;">
							{#WITH $coldef}<!-- Ueberschrift incl. jump to -->
								<div>
									<h2><a name="winner"></a>Winnerbracket</h2>
								</div>
							{#END}
							<?php
								$columnCounter = 1;
								$rowCounter = 0;
								$rowCountMax = 0;
								$teamContainerWidth = 260;
								$teamContainerHeight = 120;
							?>
							{#FOREACH $col}
								<?php $columnCounter++; ?>
								<?php $rowCounter = 0; ?>
								{#FOREACH $row}
									<?php $rowCounter++; ?>
								{#END}
								<?php
									if ($rowCounter > $rowCountMax){
										$rowCountMax = $rowCounter;
									}
								?>
							{#END}
							<?php
								$tournamentTotalWidth = $teamContainerWidth * $columnCounter;
								$tournamentTotalHeigth = $teamContainerHeight * $rowCountMax;
							?>
							<div style="width: <?php echo $tournamentTotalWidth; ?>px;height: <?php echo $tournamentTotalHeigth; ?>px;">
							  {#FOREACH $col index=$currentcol}
								<?php
									$teamCount=0;
								?>
								{#FOREACH $row}
								<?php
									$teamCount++;
								?>
								{#END}
								<?php
									$roundHeightTotal = $teamCount * $teamContainerHeight;

									$offsetCalc = (($tournamentTotalHeigth - ($teamCount * $teamContainerHeight)) / 2);

									if ($offsetCalc >= 0){
										$offsetHeight = $offsetCalc;
									}
								?>
								<div style="float:left;margin-top:<?php echo $offsetHeight; ?>px;">
									{#VAR sub=_atree}
								</div>
							  {#END}
							</div>
						</div>
				{#END} <!-- ende Tournament Tree winnerbracket -->



















				{#IF true}<!-- Tournament Tree Looserbracket -->
				<div style="overflow-x:scroll;">
					{#IF !empty($lcoldef)}
							<div align="center" valign="middle">
								{#WITH $lcoldef}<!-- Ueberschrift incl. jump to -->
									<div>
										<h2><a name="loser"></a>Loserbracket</h2>
									</div>
								{#END}

								<?php
									$columnCounter = 1;
									$rowCounter = 0;
									$rowCountMax = 0;
									$teamContainerWidth = 260;
									$teamContainerHeight = 120;
								?>
								{#FOREACH $lcol}
									<?php $columnCounter++; ?>
									<?php $rowCounter = 0; ?>
									{#FOREACH $row}
										<?php $rowCounter++; ?>
									{#END}
									<?php
										if ($rowCounter > $rowCountMax){
											$rowCountMax = $rowCounter;
										}
									?>
								{#END}
								<?php
									$tournamentTotalWidth = $teamContainerWidth * $columnCounter;
									$tournamentTotalHeigth = $teamContainerHeight * $rowCountMax;
								?>

								<div style="width: <?php echo $tournamentTotalWidth; ?>px;height: <?php echo $tournamentTotalHeigth; ?>px;">
								  {#FOREACH $lcol index=$currentcol}<!-- je Runde 1x frametree - inc.tree.default.style -->
									<?php
										$teamCount=0;
									?>
									{#FOREACH $row}
									<?php
										$teamCount++;
									?>
									{#END}
									<?php
										$roundHeightTotal = $teamCount * $teamContainerHeight;

										$offsetCalc = (($tournamentTotalHeigth - ($teamCount * $teamContainerHeight)) / 2);

										if ($offsetCalc >= 0){
											$offsetHeight = $offsetCalc;
										}
									?>
									<div style="float:left;margin-top:<?php echo $offsetHeight; ?>px;">
										{#VAR sub=_atree}
									</div>
								  {#END}
								</div>
							</div>
						{#END}{!if !empty(lcoldef)}
				</div>







				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12">
						{#FOREACH $groups}<!-- Tournament Tree Groupgames -->
							  <h2><a name="{$title}">{$title}</a></h2>
							  <h3>Tabelle</h3>
							  {#TABLE $ergebnis class="table"}
								{#COL "Team" $name}
								{#COL "Spiele" $matchcount}
								{#COL "+" $won}
								{#COL "=" $equal}
								{#COL "-" $lost}
								{#COL "Punkte" $points}
								{#COL "Ergebnis" $pluspunkte}
									<div style="height:1em;position:relative;">
										<div style="text-align:right;width:2em;position:absolute;display:inline;">{$pluspunkte}</div>
										<div style="left:2em;position:absolute;display:inline;">: {$minuspunkte}</div>
									</div>
							  {#END}
							  <br />
							  <h3>Paarungen</h3>
							  <table class="table" cellpadding="0" cellspacing="1">
							  {#FOREACH $rounds index=$j}
								  <tr>
									<td class="tdedit">{$j}. Runde</td>
									{#FOREACH $matches}
									<td class="tdcont">
									  <table width="100%" cellspacing="0" cellpadding="0" border="0">
									  <tr>
										<td class="tdcont" width="40%"><span class="{$color1}">{#IF !empty($team1) && empty($points1) && empty($points2)}{#LOAD $ready1 $ready}{#LOAD $readylink1 $readylink}{#VAR sub=_ready file=$treetypefile}&nbsp;{#END}{$team1html}</span></td>
										<td class="tdcont" width="20%" align="center">{$score}<br /><small class="comment">{$comment}</small></td>
										<td class="tdcont" width="40%" align="right"><span class="{$color2}">{$team2html}{#IF !empty($team2) && empty($points1) && empty($points2)}{#LOAD $ready2 $ready}{#LOAD $readylink2 $readylink}&nbsp;{#VAR sub=_ready file=$treetypefile}{#END}</span></td>
									  </tr>
									  </table>
									</td>
									{#END}
								</tr>
							  {#END}
							  </table>
						{#END}{!foreach groups}
						<!-- Vorbereitung fuer Punkte System
						/**#FOREACH $points}
						//	 <h2><a name="$title}">$title}</a></h2>
						//  <h3>Tabelle</h3>
						//  #TABLE $ergebnis class="table"}
						//		#COL "Teilnehmer" $name}
						//			#FOREACH $rounds index=$j}
						//				<tr>
						//					<td class="tdedit">$j}. Runde</td>
						//				#FOREACH $matches}
						//				<td class="tdcont">
						//					#COL "Punkte" $points}
						//				</td>
						//				</tr>
						//		#COL "Ergebnis" $pluspunkte}<div style="height:1em;position:relative;">
						//			<div style="text-align:right;width:2em;position:absolute;display:inline;">{$pluspunkte}</div>
						//			<div style="left:2em;position:absolute;display:inline;">: {$minuspunkte}</div></div>
						//	  #END}
						//	  <br />
						//#END}**/ --> <!-- ende Tournament Tree Pointsgame -->
					</div>
				</div>
				{#END}{!group} <!-- ende Tournament Tree Groupegames -->
			{#END}{!if empty} <!-- close else -->
	</div>
</div>
{#END}

{#SUB showdescription}
<!-- Turnierbeschreibung -->
	<div class="col-12 col-sm-12 col-md-12 col-lg-12">
		{$description}
	</div>
{#END}

{! subs below this line should not be used standalone }

{#SUB _atree}
	<!-- siehe tounament.php (frametree)
		"treetypefile" = "inc.tree.default.tpl";
		"treetypestyles" = "inc.tree.default.styles.tpl";
	 -->
	{#VAR sub=atree file=$treetypefile}
{#END}
