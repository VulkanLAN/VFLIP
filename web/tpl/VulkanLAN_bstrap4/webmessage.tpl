{! $Id: webmessage.tpl 1415 20015-01-29 13:56:00Z edit by naaux $ }

{#SUB header}
<div class="card ">
	<div class="btn-group" role="group" aria-label="Basic">
	  <a href="webmessage.php?frame=owner&amp;id={$user_id}"{#WHEN "$frame == 'owner'" " class=\"btn btn-md btn-primary\""} class="btn btn-md btn-secondary" role="button"><i class="fas fa-inbox"></i>&nbsp;Posteingang</a> 
	  <a href="webmessage.php?frame=sender&amp;id={$user_id}"{#WHEN "$frame == 'sender'" " class=\"btn btn-md btn-primary\""} class="btn btn-md btn-secondary" role="button"><i class="fas fa-upload"></i>&nbsp;Postausgang</a> 
	  <a href="webmessage.php?frame=sendmessage&amp;id={$user_id}"{#WHEN "$frame == 'sendmessage'" " class=\"btn btn-md btn-primary\""} class="btn btn-md btn-success" role="button"><i class="fas fa-pencil-alt"></i>&nbsp;neue Nachricht</a>
  </div>
</div>
 {#END}

{#SUB default}
{#VAR sub=header}
{#END}

{#SUB owner}
{#VAR sub=header}
<div class="card ">
	<div class="row">
		<div class="visible-md visible-lg col-md-2 col-lg-2" style="padding-right: 5px;">
			<div class="card ">
				<div class="card-header"><i class="far fa-folder-open fa-1x"></i>&nbsp;{§Ordner}</div>
					<div class="list-group">
						<a href="webmessage.php?frame=owner{#IF !empty($user_id)}&amp;id={$user_id}{#END}" class="list-group-item"><i class="far fa-folder fa-1x"></i>&nbsp; {§Posteingang}</a>
						{#FOREACH $folders $afoldername $afolderid}
						<a href="webmessage.php?frame=owner&amp;folder_id={$afolderid}{#IF !empty($user_id)}&amp;id={$user_id}{#END}" class="list-group-item"><i class="far fa-folder fa-1x"></i>&nbsp; {%afoldername}</a>
						{#END}
						<p align="center"><span style="vertical-align:middle;">
						<a href="webmessage.php?frame=editfolder&amp;id={$user_id}" class="btn btn-sm btn-secondary" role="button">{§Ordner}{§verwalten}</a>
						</span></p>
					</div>
			</div>
		</div>
		<div class=".col-12 col-sm-12 col-md-10 col-lg-10" style="padding-left: 5px;">
			<!-- Default panel contents -->
			<!-- Posteingang -->
			<div class="card border-warning">
				<div class="card-header bg-warning">Nachrichten</div>
				<div class="card-body">
				<!-- Table -->
				{#TABLE $messages width="100%" maxrows=20 class="table"}
				  {#COL Sender $sender width="120" align="center"}<a href="user.php?frame=viewsubject&amp;id={$sender_id}">{%sender}</a>
				  {#COL Gesendet $date width="100" align="center"}{#DATE $date}
				  {#COL Betreff %subject}
					<a {#WHEN "$status == 'unread'" "class=\"important\" "}{#WHEN "$status != 'processed'" "style=\"font-weight:bold;\" "}href="webmessage.php?frame=viewmessage&amp;id={$user_id}&amp;message={$id}&amp;box=owner">{%subject}</a>
				  {#COL "" title="Antworten" width="20" align="center"}
					<b><a {#WHEN $is_replay "class=\"important\" "}href="webmessage.php?frame=sendmessage&amp;id={$user_id}&amp;type=replay&amp;message_id={$id}"><i class="fas fa-reply fa-2x"></i></a></b>
				  {#COL "" title="Weiterleiten" width="20" align="center"}
					<b><a {#WHEN $is_oforward "class=\"important\" "}href="webmessage.php?frame=sendmessage&amp;id={$user_id}&amp;type=oforward&amp;message_id={$id}"><i class="fas fa-share fa-2x"></i></a></b>  
				 {#OPTION L&ouml;schen delete}
				 {#INPUT type hidden owner}
				  {#OPTION Als setstatus}
				  {#INPUT status dropdown param="array(read=gelesen unread=ungelesen processed=bearbeitet)"} &nbsp;markieren<br />
				  {#OPTION Nach movetofolder}
				  {#INPUT folder_id dropdown param=$allfolders} &nbsp;verschieben
				 {#END}
				</div>
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB sender}
{#VAR sub=header}
<div class="card border-primary">
		<!-- Postausgang -->
		<div class="card-header">Nachrichten</div>
		<div class="card-body">
			<!-- Table -->
			{#TABLE $messages width="100%" maxrows=20 class="table"}
				  {#COL Empf&auml;nger %owner width="120" align="center"}<a href="user.php?frame=viewsubject&amp;id={$owner_id}">{%owner}</a>
				  {#COL Gesendet $date width="100" align="center"}{#DATE $date}
				  {#COL Betreff %subject}    
					<b><a href="webmessage.php?frame=viewmessage&amp;id={$user_id}&amp;message={$id}&amp;box=sender">{%subject}</a></b>
					{#IF !empty($processor_id) and ($sender_id != $processor_id)}<span style="color:gray">von</span> <a href="user.php?frame=viewsubject&amp;id={$processor_id}">{%processor}</a>{#END}
				  {#COL "" title="Noch einmal senden" width="16" align="center"}
					<b><a {#WHEN $is_resend "class=\"important\" "}href="webmessage.php?frame=sendmessage&amp;id={$user_id}&amp;type=resend&amp;message_id={$id}"><i class="fas fa-redo fa-2x"></i></a></b>
				  {#COL "" title="Weiterleiten" width="16" align="center"}
					<b><a {#WHEN $is_sforward "class=\"important\" "}href="webmessage.php?frame=sendmessage&amp;id={$user_id}&amp;type=sforward&amp;message_id={$id}"><i class="fas fa-share fa-2x"></i></a></b>
				  {#OPTION L&ouml;schen delete}{#INPUT type hidden sender} 
			{#END}
		</div>
</div>
{#END}

{#SUB editfolder}
<div class="card border-info col-10">
	<div class="card-header">
		<div class="row">
				<div class="col-6" align="left"><i class="far fa-folder-open fa-1x"></i>&nbsp; Ordner {§hinzuf&uuml;gen}/{§bearbeiten}</div>
				<div class="col-6" align="right">
				  <a href="webmessage.php?frame=owner{#IF !empty($user_id)}&amp;id={$user_id}{#END}"class="btn btn-secondary"><i class="far arrow-alt-circle-left fa-1x"></i>{§zur&uuml;ck}</a>
				</div>
			</div>
	</div>
	{#FORM folder}
	{#INPUT folder_id hidden $folder_id}
	<div class="card-body">
		<form>
			   <div class="form-group">
				<label for="Folder" class="col-2 col-form-label">Name:</label>
				<div class="col-10">
					{#INPUT foldername string}
				</div>
			  </div>
		</form>
		<div class="col-12" align="left">
		{#SUBMIT anlegen a}
		{#END}
		</div>
	</div>
</div>
	{#IF !empty($folders)}
	{#FORM delfolder}
<div class="card bg-danger">
	<div class="card-header">
		<div class="row">
				<div class="col-12" align="left"><i class="far fa-folder-open fa-1x"></i>&nbsp; {§Ordner}{§l&ouml;schen}</div>
			</div>
	</div>
	<div class="card-body">
		<form>
			   <div class="form-group">
				<label for="Folder" class="col-3 col-form-label">Ordner ausw&auml;hlen</label>
				<div class="col-9">
					{#INPUT folder_id dropdown $folder_id param=$folders}
				</div>
			</div>
		</form>
		<div class="col-12" align="left">
		{#SUBMIT l&ouml;schen l}
		{#END}
		</div>
	</div>
	{#END}	
</div>
{#END}

{#SUB sendmessage}
{#VAR sub=header}
{#FORM message}
<div class="card border-dark">
		<div class="card-header">
			<div class="row">
				<div class="col-6" align="left">Nachrichten</div>
				<div class="col-6" align="right">
				  <button type="submit" class="btn btn-success">Senden</button>
				 {#BACKLINK Zur&uuml;ck}
				</div>
			</div>
		</div>
		<div class="card-body">
		
			<form>
			   <div class="form-group row">
				<label for="inputEmail3" class="col-sm-2 col-form-label">Empf&auml;nger:</label>
				<div class="col-sm-10">
					{#INPUT receiver string $receiver}  <i>(UserID, Nickname oder Email-Addresse)</i>
				</div>
			  </div>

			   <div class="form-group row">
				<label for="inputSubject3" class="col-sm-2 col-form-label">Betreff:</label>
				<div class="col-sm-10">
					{#INPUT subject longstring $subject allowempty=0}
				</div>
			  </div>

			    <div class="form-group row">
				<div class="col-sm-offset-2 col-sm-10">
				  <div class="form-check">
					<label>
					  {#INPUT append_signature checkbox $append_signature} Signatur anf&uuml;gen
					</label>
				  </div>
				</div>
			  </div>
			</form>

		</div>
		<div class="card-body">
			{#INPUT message documentwrap $message allowempty=0}
		</div>
</div>
{#INPUT source_id hidden $source_id}
{#INPUT source_type hidden $source_type}
{#END}
{#END}

{#SUB viewmessage}
{#VAR sub=header}
<!-- Nachricht -->
<div class="card border-primary">
		<div class="card-header">
			<div class="row">
				<div class="col-2" align="left">Nachrichten</div>
				<div class="col-10" align="right">
				  {#IF $box == "owner"}
					  <a href="webmessage.php?frame=sendmessage&amp;id={$user_id}&amp;type=replay&amp;message_id={$id}" class="btn btn-secondary" role="button"><i class="fas fa-reply"></i>Antworten</a>
					  <a href="webmessage.php?frame=sendmessage&amp;id={$user_id}&amp;type=oforward&amp;message_id={$id}"  class="btn btn-secondary" role="button"><i class="fas fa-share"></i>Weiterleiten</a>
					 {#ACTION L&ouml;schen delete "webmessage.php?frame=owner&id=$user_id" params="array(ids[]=$id type=owner)" formattrs="array(style=display:inline;)" buttonclass="btn btn-danger btn-sm"}
					  {#IF $status != "processed"}
						{#ACTION auf_bearbeitet setstatus params="array(ids[]=$id status=processed)" formattrs="array(style=display:inline;)" buttonclass="btn btn-success btn-sm"}
					  {#ELSE}
						{#ACTION auf_unbearbeitet setstatus params="array(ids[]=$id status=read)" formattrs="array(style=display:inline;)" buttonclass="btn btn-warning btn-sm"}
					  {#END}
					{#ELSEIF $box == "sender"}
					  <a href="webmessage.php?frame=sendmessage&amp;id={$user_id}&amp;type=resend&amp;message_id={$id}" class="btn btn-secondary" role="button"><i class="fas fa-redo"></i>Nochmals senden</a>
					  <a href="webmessage.php?frame=sendmessage&amp;id={$user_id}&amp;type=sforward&amp;message_id={$id}" class="btn btn-secondary" role="button"><i class="fas fa-share"></i>Weiterleiten</a>
					 {#ACTION L&ouml;schen delete "webmessage.php?frame=sender" params="array(ids[]=$id type=sender)" formattrs="array(style=display:inline;)" buttonclass="btn btn-danger btn-sm"}
					{#END}
				</div>
			</div>
		</div>
		<div class="card-body">
			<div class="row">
				 <div class="col-2" align="left">Status:</div>
				 <div class="col-10">
									{#IF $status == "processed"}
										bearbeitet
									{#ELSEIF $status == "unread"}
										ungelesen
									{#ELSEIF $status == "read"}
										gelesen
									{#ELSE}
										unbekannt
									{#END}
								
						</div>
			</div>
			<div class="row">
				<div class="col-2" align="left">Absender:</div>
					<div class="col-10">
						<a href="user.php?frame=viewsubject&amp;id={$sender_id}">{%sender}</a>
							{#IF $processor_id}(<span style="color:gray">bearbeitet von</span> 
						<a href="user.php?frame=viewsubject&amp;id={$processor_id}">{%processor}</a>){#END}
					</div>
			</div>	
			<div class="row">
				<div class="col-2" align="left">Empf&auml;nger:</div>
					<div class="col-10">
						 <a href="user.php?frame=viewsubject&amp;id={$owner_id}">{%owner}</a>
					</div>
			</div>
			<div class="row">
				<div class="col-2" align="left">Betreff</div>
					<div class="col-10">
						{%subject}
					</div>
			</div>
		</div>
		<div class="card-footer">
			<div class="row">
				<div class="col-12">
					<p>{$text}</p>
				</div>
			</div>
		</div>
		<div class="card ">
		{#IF !empty($related)}
			<div class="card-header">Zugeh&ouml;rige Nachrichten:</div>
			 <ul class="list-group">{#FOREACH $related}
			  <li class="list-group-item">
				{#IF $source_type == "replay"}Antwort{#ELSEIF $source_type == "resend"}Nochmals gesendet{#ELSE}Weitergeleitet{#END}
				an <a href="user.php?frame=viewsubject&amp;id={$owner_id}">{%owner}</a>:
				<b><a href="webmessage.php?frame=viewmessage&amp;message={$id}&amp;box=sender&amp;id={$user_id}">{%subject}</a></b>
			  </li>  
			{#END}</ul>
		  {#END}
		</div>
</div>
{#END}

{#SUB contact}
{#FORM contact}
<div class="card border-primary">
		<div class="card-body">
		{#TPL $text}
		</div>
		{#IF !$loggedin}
		<div class="alert alert-warning">
		<b>Wichtig:</b> Wenn du auf dieser Seite bereits einen 
		Account hast, dann logge dicht mit diesem ein, bevor du die Nachricht schreibst.
		</div>
		{#END}
		<div class="card-header">
			<div class="row">
				<div class="col-12" align="left">Nachricht</div>
			</div>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-2" align="left">Name:</div>
					<div class="col-10">
						{#IF $loggedin}{%name}{#ELSE}{#INPUT name string allowempty=0}{#END}
					</div>
			</div>
			<div class="row">
				<div class="col-2" align="left">EMail:</div>
					<div class="col-10">
						{#IF $loggedin}{%email}{#ELSE}{#INPUT email email allowempty=0}{#END}
					</div>
			</div>	
			<div class="row">
				<div class="col-2" align="left">Betreff:</div>
					<div class="col-10">
						{#INPUT subject longstring $test}
					</div>
			</div>
		</div>
		<div class="card-body">
			 {#INPUT message document allowempty=0}
		</div>
		<div class="card-footer">
			<div class="row">
				{#INPUT Sicherheitsabfrage captcha}
				<form>
				<div class="form-group col-sm-12">
					<label for="Bildtext" class="col-sm-2 col-form-label"></label>
					<div class="col-sm-10">
						{#SUBMIT Absenden}
					</div>
				</div>
				</form>
			</div>
		</div>
		
</div>
{#INPUT prefix hidden $prefix}
{#END}
{#END}

{#SUB smallnewmessages}
<!-- in ein Div verpackt - edit naaux -->
 <?php 
   $counter = 0;
 ?>
 {#FOREACH $items}<?php $counter++; ?>{#END}
<div class="card panel-yellow">
  <div class="card-header">
     <div class="row">
        <div class="col-3">
            <i class="far fa-comments fa-5x"></i>
        </div>
          <div class="col-9 text-right">
            <div class="huge"><?php echo $counter; ?></div>
                 <div>New Messages!</div>
           </div>
     </div>
    </div>
    
     <div class="card-footer">
        <span class="pull-left">
		{#FOREACH $items}
			<nobr>
		  {#IF $status=='unread'}
			<span class="text-warning"><i class="far fa-envelope"></i></span>
			<a class="important" href="webmessage.php?frame=viewmessage&amp;id={$userid}&amp;message={$id}&amp;box=owner">{%subject} &nbsp;<i class="far arrow-alt-circle-right"></i></a><br />
		  {#ELSE}
			<span class="text-success"><i class="far fa-envelope-open"></i></span>
			<a href="webmessage.php?frame=viewmessage&amp;id={$userid}&amp;message={$id}&amp;box=owner">{%subject} &nbsp;<i class="far arrow-alt-circle-right"></i></a><br /> 
		  {#END}
		  </nobr>
		{#END}</span>
          <span class="pull-right"></span>
        <div class="clearfix"></div>
     </div>
</div>
{#END}

{#SUB menuenewmessages}
<!-- in ein Div verpackt - edit naaux -->
	 <?php 
	   $counter = 0;
	 ?>
	{#FOREACH $items}<?php $counter++; ?>{#END}
 
             <li class="nav-item dropdown no-arrow mx-1" style="margin-top: .85rem;">
              <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<span class="text-warning"><i class="far fa-envelope"></i></span>
                <!-- Counter - Messages -->
                <span class="badge badge-danger badge-counter"> <?php echo $counter; ?> </span>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">

                <h6 class="dropdown-header">
                  Message Center
                </h6>
				{#FOREACH $items limit=8}
				  {#IF $status=='unread'} 
					<a class="dropdown-item d-flex align-items-center" href="webmessage.php?frame=viewmessage&amp;id={$userid}&amp;message={$id}&amp;box=owner">
					  <div class="mr-3">
						<div>
						  <i class="far fa-envelope text-warning"></i>
						</div>
					  </div>
					  <div>
						<div class="small text-gray-500">{%subject}</div>
					  </div>
					</a>
					{#ELSE}
					<a class="dropdown-item d-flex align-items-center" href="webmessage.php?frame=viewmessage&amp;id={$userid}&amp;message={$id}&amp;box=owner">
					  <div class="mr-3">
						<div>
						  <i class="far fa-envelope text-primary"></i>
						</div>
					  </div>
					  <div>
						<div class="small text-gray-500">{%subject}</div>
					  </div>
					</a>
				  {#END}
				{#END}
                <a class="dropdown-item text-center small text-gray-500" href="webmessage.php?frame=owner">Read More Messages</a>
              </div>
            </li>
{#END}