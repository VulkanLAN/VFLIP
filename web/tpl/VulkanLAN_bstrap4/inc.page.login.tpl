{#SUB login}

{#IF $isloggedin}

<!--VulkanLAN neuer logout in der Hauptmenue leiste - edit naaux -->

<ul class="navbar-nav ml-auto">
            <!-- Nav Item - Messages -->
			{#IF $unread_messages}
				{#LOAD PageLoadFrame("'webmessage.php?frame=menuenewmessages'") $temp1}
					{#TPL $temp1}			
			{#ELSE}
			<li class="nav-item dropdown no-arrow mx-1" style="margin-top: .85rem;">
              <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="far fa-envelope fa-fw"></i>
                <!-- Counter - Messages -->
                <span class="badge badge-counter">0</span>
              </a>
			 </li>
			{#END}

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">{%user}</span>
				{#DBIMAGE $userimg process=createthumbnail(60,60) class="rounded-circle" style="height: 60px;"}
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
			  	{#LOAD MenuLoad() $level0 inc/inc.menu.php}
					{#FOREACH $level0.menu}
						{#FOREACH $items}
						{#IF $id==9}
							{#LOAD LoadMenu(%id,1) $sub inc/inc.menu.php}
							{#FOREACH $sub.menu}
								{#FOREACH $items}
									{#IF is_object($frame)}<div class="dropdown-item">{#TPL $frame}</div>
									{#ELSEIF !empty($text)}<div class="dropdown-item">{$text}</div>
									{#ELSE}<div class="dropdown-item">{#DBIMAGE $image}</div>
									
									<a class="dropdown-item {#WHEN $active }" aria-haspopup="true" aria-expanded="false" href="{$link}" title="{%description}"{#WHEN "isset($use_new_wnd) && $use_new_wnd" " target=\"_blank\""}>
									<i class="{$font_image}"></i>&nbsp;{%caption}</a>
									{#END}
							{#END}
							{#END}
						{#END}
						
					{#END}
				{#END}

				<div class="dropdown-divider"></div>
				
				<div class="dropdown-item">
				<form method="post" id="frm2">
					<input type="hidden" name="ident" value="Anonymous" />
					<input type="hidden" name="password" value="x" />
					<button type="submit" class="btn btn-primary btn-md" value="logout">Logout &nbsp; <i class="fas fa-sign-out-alt"></i>
					</button>
				</form>
				</div>

              </div>
            </li>
          </ul>

{#ELSE}

<!--VulkanLAN neuer login in der Hauptmenue leiste - edit naaux -->

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<span class="mr-2 d-none d-xl-inline d-sm-none d-md-none text-gray-600 small">Wilkommen, hast du bereits einen Account?</span>
				<span class="mr-2 d-none d-lg-inline d-sm-none d-md-none d-xl-none text-gray-600 small">Wilkommen!</span>
				<span class="rounded-circle border" style="display: inline-block; border-radius: 3em;  box-shadow: 0px 0px 2px #888; padding: 0.5em 0.8em;">
				<i class="fa fa-user"></i>
				</span>
				</a>
				<!-- Dropdown - User Information -->
				<div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
				<a class="dropdown-item" href="user.php?frame=register">
					<i class="fas fa-user-plus fa-sm fa-fw mr-2 text-gray-400"></i>
					Registrieren
				</a>
				<a class="dropdown-item" href="text.php?name=user_account_trouble">
				<i class="fas fa-user-md fa-sm fa-fw mr-2 text-gray-400"></i>
					Account Wiederherstellung
				</a>
				<a class="dropdown-item" href="user.php?frame=resetpwd"> 
					<i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
					Passwort vergessen?
				</a>
				<div class="dropdown-divider"></div>
				
				<form method="post" id="frm2">
					<div class="form-group">
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text"> <i class="fa fa-user"></i> </span>
						</div>
						<input type="text" class="form-control" name="ident" title="Der Login ist mit deine UserID, Nickname oder Email-Adresse moeglich."
							placeholder="UserID, Nickname, Email"
							onFocus="if(this.value=='Nickname casesensitive') this.value='';" {#WHEN "!empty($loginident) and ($loginident != \"Anonymous\")" "value=\"%loginident\" "}/>
					</div> <!-- input-group.// -->
					</div> <!-- form-group// -->
					<div class="form-group">
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text"> <i class="fa fa-lock"></i> </span>
						</div>
						<input type="password" id="password" class="form-control" name="password" title="Passwort" placeholder="Password" onclick="this.value='';" />
					</div> <!-- input-group.// -->
					</div> <!-- form-group// -->
					<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block"> <i class="fas fa-sign-in-alt fa-sm fa-fw mr-2 text-gray-400"></i> Login  </button>
					</div> <!-- form-group// -->
				</form>
					<script>
					var loginform = document.getElementById('frm2');
					{#IF empty($loginident) or ($loginident == "Anonymous")}
					loginform.password.value='Password';
					{#ELSE}
					loginform.password.focus();
					{#END}
					</script>
              </div>
            </li>
          </ul>

{#END}   

{#END}