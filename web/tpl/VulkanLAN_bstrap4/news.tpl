{! $Id: news.tpl 1702 2019-04-16 06:12:40Z loom $ edit by naaux}

{#SUB default}
<div class="card ">
	<div class="card-header">
		<div class="row">
			<div class="col" style="text-align: right;">
				<div class="btn-group btn-md">
				{#IF $usecat}
					{#FORM method=get}
						<div>
							{§Kategorie}:
							{#INPUT category tabledropdown $category param=news_category}
							{#SUBMIT Anzeigen 0}
						</div>
					{#END}
				{#END}
					<a href="news.php?frame=archiv" class="btn btn-outline-dark" role="button">{§Archiv}</a>
				{#IF !empty($admin)}
					<a href="news.php?frame=edit" class="btn btn-success" role="button"><i class="fas fa-plus"></i>&nbsp;{%admin}</a>
				{#END}
				</div>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col">
				{#FOREACH $news}
				<div class="row">
				<div class="col">
					<p class="card-title">
						<strong>{#IF $intern}{§Intern}: {#END} <h2>{%title}</h2></strong>
					</p>
					<p class="card-subtitle mb-2 text-muted" style="text-align: right;">
						Date: {#DATE $date}
						{#IF $usecat} | Kategorie: {#TABLEDISPLAY news_category $category}{#END}
						| Author: {%author}
						{#IF ($id > 0)}
							{#IF ($edit=="true" || $spellchecker=="true")}<br />
									{§Artikel}:
										<a href="news.php?frame=edit&amp;id={$id}" class="btn button btn-warning btn-sm" role="button"><i class="far fa-edit"></i></a>
										<a href="news.php?frame=del&amp;id={$id}"  class="btn button btn-danger btn-sm" role="button"><i class="far fa-trash-alt"></i></a>
							{#END}
						{#END}
					</p>
					<p class="card-text">
						{$text}
					</p >
				</div>
				</div>
				<div class="row">
					<div class="col">
						{#IF ($id > 0)}
							<div>
							<hr>
							<a href="news.php?frame=comments&amp;id={$id}" class="btn button btn-secondary btn-sm">{§Kommentare} ({$commentcount})</a>
							</div>
						{#END}
					</div>
				</div>
				{#END}
			</div>
		</div>
	</div>
	<div class="card-footer text-muted">
			{#IF isset($less)}<a href="news.php?more={$less}" class="btn button btn-outline-dark btn-md" role="button">{§vorherige}</a>{#END}
			{#IF isset($less) && !empty($more)} - {#END}
			{#IF !empty($more)}<a href="news.php?more={$more}" class="btn button btn-outline-dark btn-md" role="button">{§weitere}</a>{#END}
			<br />
	</div>
</div>
{#END}

{#SUB del}
<div class="card ">
	<div class="card-header">
		{§ text="Soll der Newseintrag \"?1?\" wirklich gel&ouml;scht werden?" param=%title}
	</div>
	<div class="card-body">
		<div class="row">
			{#FORM del action="news.php"}
				{#INPUT id hidden $id}
				{#SUBMIT ja}<br />
				<br />
				{#BACKLINK NEIN/zur&uuml;ck}
			{#END}
		</div>
	</div>
</div>
{#END}

{#SUB edit}
	{#FORM news}
	<div class="card ">
		<div class="card-header">
			{#BACKLINK zur&uuml;ck}
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col">
					<table cellspacing="0" cellpadding="2" border="0" class="table">
						{#INPUT view_right hidden $view_right}
						{#IF $newsposter=="true"}<tr><td>{§Public}:</td><td>{#INPUT public checkbox $public}</td></tr>
						<tr><td>{§Datum}:</td><td>{#INPUT date date $date} {§leer} = {§jetzt}</td></tr>{#END}
						<tr><td>{§Immer oben}:</td><td>{#INPUT on_top checkbox $on_top}</td></tr>
						<tr><td>{§Autor}:</td><td>{#INPUT author string $author} {§leer} = {§Benutzer}</td></tr>
						<tr><td>{§Kategorie}:</td><td>{#INPUT category tabledropdown $category param=news_category}</td></tr>
						<tr><td>{§Titel}:</td><td>{#INPUT title string $title}</td></tr>
					</table>
					{#INPUT text document $text}<br />
					{#INPUT id hidden $id}
					{#SUBMIT Eintragen}<br />
				</div>
			</div>
		</div>
	</div>
	{#END}
{#END}

{#SUB comments}
<div class="card ">
	<div class="card-header">	
		<a href="news.php" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-arrow-left"></i>&nbsp;{§zur&uuml;ck zu den Neuigkeiten}</a>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col">
				<p class="card-title">
					<h2>{%title}</h2>
				</p>
				<p class="card-subtitle mb-2 text-muted" style=" text-align: right;">
					Date: {#DATE $date}
					{#IF $usecat} | Kategorie: {#TABLEDISPLAY news_category $category}{#END}
					| Author: {%author}
				</p>
				<p class="card-text">
					{$text}
				</p >
			</div>
		</div>
		<div class="row">
			<div class="col">
				<h2>{§Kommentare}:</h2>
				{#TABLE $comments class="table"}
					{#IF $cancomment=="true"}
						<a href="news.php?frame=addcomment&amp;id={$id}" class="btn btn-success btn-sm" role="button"><i class="fas fa-plus"></i>&nbsp;{§Kommentar abgeben}</a>
					{#END}
						{#COL Autor %username widht="220" valign="top" }
							<p style="font-size:0.8em;"> <a href="user.php?frame=viewsubject&amp;id={$user_id}">{%username}</a></p>
							<p style="font-size:0.8em;"> {#DATE $timestamp}</p>
						{#COL Kommentar} <p class="card-text"> {$text} </p >
						{#OPTION l&ouml;schen delcomment right="$editright"}
				{#END} 
			</div>
		</div>
	</div>
	<div class="card-footer text-muted">
	<br /><a href="news.php" class="btn btn-secondary btn-md" role="button"><i class="fas fa-arrow-left"></i>&nbsp;{§zur&uuml;ck zu den Neuigkeiten}</a>
	</div>
</div>
{#END}

{#SUB addcomment}
<div class="card ">
	<div class="card-header">
		<a href="news.php?frame=comments&amp;id={$id}" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-arrow-left"></i>&nbsp;{§zur&uuml;ck}</a>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col">
				{#FORM comment action="news.php?frame=comments&amp;id=$id"}
					{§Kommentar}:<br />
						{#INPUT text string $text}<br />
						{#INPUT id hidden $id}
					{#SUBMIT hinzuf&uuml;gen}
				{#END}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB archiv}
	{#TPL $text}
	{#IF $usecat}
		<div align="right">
		{#FORM method=get}
			{#INPUT frame hidden archiv}
			Kategorie:
			{#INPUT category tabledropdown $category param=news_category}
			{#SUBMIT Anzeigen 0}
		{#END}
	</div>
	{#END}
	{#TABLE $news maxrows=25 class="table"}
		{#COL Datum $date}{#DATE $date}
		{#COL Titel %title}<a href="news.php?newsid={$id}">{#IF $on_top}<span class="important">{#END}{#IF $intern}{§Intern}: {#END}{%title}{#IF $on_top}</span>{#END}</a>
		{#COL Autor %author}
		{#COL Kategorie $category}{#TABLEDISPLAY news_category $category}
	{#END}
{#END}

{#SUB smallnews}
	<style type="text/css">
	.news_small_li
	{
		list-style-position:inside;
		list-style-type:circle;
		position:inherit;
		margin: 2px 0px 2px 0px;
		float:none;
		text-align: left;
	}
	.news_small_ul {
		margin: 0px;
	}

	</style>
	<ul class="news_small_ul">
	{#FOREACH $items}
		<li class="news_small_li"><a href="news.php?newsid={$id}" title="{#DATE $date}: {%author}">{#IF $intern}i: {#END}{%title}</a></li>
	{#END}
	</ul>
{#END}