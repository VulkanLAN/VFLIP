{!
  $Id: turnieradm.tpl 1469 2007-09-27 13:22:27Z loom $
}

{#SUB liga}
	{#FORM liga}
		{#INPUT liga dropdown param=$ligalist}
		{#SUBMIT ausw&auml;hlen}
	{#END}
	<br/>
	<br/>
	{§Falls euer Spiel nicht in der Liste auf der nachfolgenden Seite ist, hilft evtl folgendes}:
	{#FOREACH $ligen}
		{#IF $xml}
		{#ACTION "$liga-Spiele aus XML-Datei lesen" updategames params="array(liga=$liga)" confirmation="Sollen alle vorhandenen $liga-Spiele gel&ouml;scht und neue aus der XML-Datei gelesen werden?"}<br/>
		{#END}
	{#END}
{#END}

{#SUB add}
<div class="card ">
		<div class="card-header">
				<div class="card-header">
					<strong>{$done}</strong>
					<a href="tournament.php?frame={$frame}" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-arrow-left"></i>&nbsp;{§zur&uuml;ck zum Turnier}</a><br />
				</div>
		</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12">
			{#FORM $formname action=$formaction}
				<table border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<th>Spiel:</th><td>{#INPUT game dropdown $game param=$games}{#WHEN $gameEditLink $gameEditLink}</td>
				  </tr>
				  {#IF !$notAskTeamsize}
				  <tr>
					<th>Teamgr&ouml;sse:</th><td>{#INPUT teamsize integer $teamsize allowempty=0} z.B. 5on5</td>
				  </tr>
				  {#END}
				  <tr>
					<th>Typ:</th><td>{#INPUT type dropdown $type param=$types}</td>
				  </tr>
				  <tr>
					<th>Max Teams:</th><td>{#INPUT maximum integer $maximum allowempty=0}</td>
				  </tr>
				  <tr>
					<th>Loserbracket:</th><td>{#INPUT looser yesno $looser}</td>
				  </tr>
				  <tr>
					<th>Gruppen:<br />(nur Typ=Gruppen)</th><td>{#INPUT groups integer $groups}</td>
				  </tr>
				  <tr>
					<td colspan="2"><hr /></td>
				  </tr>
				  <tr>
					<th>Rundendauer:</th><td>{#INPUT roundtime integer $roundtime} Minuten (0 = unbegrenzt)</td>
				  </tr>
				  <tr>
					<th>Beginn:</th><td>{#INPUT start date $start}</td>
				  </tr>
				  <tr>
					<th>Beschreibung/<br />Regeln:<br />(HTML {#INPUT html checkbox $html})</th><td>{#INPUT description text $description}</td>
				  </tr>
				  <tr>
					<td colspan="2"><hr /></td>
				  </tr>
				  <tr>
					<th>Kosten ({$currency}):</th><td>{#INPUT coins integer $coins}</td>
				  </tr>
				  <tr>
					<th>Kostengruppe:</th><td>{#INPUT coin_id dropdown $coin_id $coinlist} <a href="turnieradm.php?frame=editgroups">bearbeiten</a></td>
				  </tr>
				  <tr>
					<th>Baumdarstellung:</th><td>{#INPUT treetype dropdown $treetype param=$treetypes}</td>
				  </tr>
				  <tr>
					<th>Turniergruppe:</th><td>{#INPUT group_id dropdown $group_id $grouplist} <a href="turnieradm.php?frame=editgroups">bearbeiten</a></td>
				  </tr>
				  <tr>
					<td colspan="2"><hr /></td>
				  </tr>
				  <tr>
					<th>Frei f&uuml;r u18:</th><td>{#INPUT u18 yesno $u18} (anhand <a href="config.php#{$u18configname}">{#WHEN "$u18config=='seat'" "des Sitzplatzes" "der Eigenschaft 'vollj&auml;hrig'"}</a>)</td>
				  </tr>
				  <tr>
					<th>Screenshot Upload:</th><td>{#INPUT screenshots yesno $screenshots}</td>
				  </tr>
				  <tr>
					<td colspan="2"><hr /></td>
				  </tr>
				  <tr>
					<th>Icon:<br />{#DBIMAGE flip_tournament_tournaments icon_small $id $mtime}</th><td>{#INPUT icon image}</td>
				  </tr>
				  <tr>
					<td colspan="2"><hr /></td>
				  </tr>
				  <tr>
					<th>Status:</th><td>{#INPUT status dropdown $status param=$statuses}</td>
				  </tr>
				  <tr>
					<td colspan="2"><hr /></td>
				  </tr>
				  <tr>
					<td colspan="2" align="center">{#INPUT id hidden $id}{#SUBMIT $submit}</td>
				  </tr>
				</table>
			{#INPUT liga hidden $liga}
			{#IF $id>0}<input type="hidden" name="confirmation" value="&Auml;nderungen speichern? Wenn der Status auf *Anmeldung* gesetzt wird gehen *alle* Matches (der Turnierbaum) verloren!\n Wenn der Status von 'l&auml;ft' auf *Gruppenspiele* gesetzt wird gehen alle *KO-*Matches (der Turnierbaum) verloren!">{#END}
			{#END}
				</div>
			</div>
		</div>
	</div>
{#END}

{#SUB editgroups}
	<div class="card ">
		<div class="card-header">
				<div class="card-header">
				<a href="tournament.php" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-arrow-left"></i>&nbsp;{§zur&uuml;ck zur Turnierliste}</a>
				</div>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-12">
				<h2>{§Turniergruppen}</h2>
				{#TABLE $groups}
				  <a href="turnieradm.php?frame=editgroup">{§hinzuf&uuml;gen}</a>
				  {#COL # $order}
				  {#COL Name %name}
				  {#COL Bild}{#DBIMAGE $image process=createThumbnail(160,64)}
				  {#COL}<a href="turnieradm.php?frame=editgroup&amp;gid={$id}">edit</a>
				  {#ACTION del deltournamentgroup condition="count($groups)>1"}
				  {#ACTION /\ gup condition=$move}
				{#END}
				<h2>{§Kostengruppen}</h2>
					{#TABLE $coins}
					  <a href="turnieradm.php?frame=editcoin">{§hinzuf&uuml;gen}</a>
					  {#COL W&auml;hrung $currency}
					  {#COL verf&uuml;gbar $maxcoins}
					  {#COL}<a href="turnieradm.php?frame=editcoin&amp;cid={$id}">edit</a>
					  {#ACTION del delcoingroup condition="count($coins)>1"}
					{#END}
				</div>
			</div>
		</div>
	</div>
{#END}

{#SUB editgroup}
	<div class="card ">
		<div class="card-header">
				<div class="card-header">
					<a href="turnieradm.php?frame=editgroups" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-arrow-left"></i>&nbsp;{§zur&uuml;ck zur Gruppenliste}</a><br />
				</div>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-12">
				{#WITH $group}
					{#FORM group}
						{#INPUT gid hidden $id}
						{#INPUT order hidden $order}
							<table cellpadding="4" cellspacing="0" border="0">
							<tr>
							  <th valign="top">{§Name}:</th><td>{#INPUT name string $name}</td>
							</tr>
							<tr>
							  <th valign="top">{§Bild} ({§optional}):</th><td>{#INPUT image image $image}</td>
							</tr>
							</table>
						{#SUBMIT}
					{#END}
				{#END}
				</div>
			</div>
		</div>
	</div>
{#END}

{#SUB editcoin}
	<div class="card ">
		<div class="card-header">
				<div class="card-header">
					<a href="turnieradm.php?frame=editgroups" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-arrow-left"></i>&nbsp;{§zur&uuml;ck zur Gruppenliste}</a><br />
				</div>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-12">
				{#WITH $coin}
					{#FORM coin}
						{#INPUT id hidden $id}
							<table cellpadding="4" cellspacing="0" border="0">
							<tr>
							  <th valign="top">{§W&auml;hrung}:</th><td>{#INPUT currency string $currency}</td>
							</tr>
							<tr>
							  <th valign="top">{§verf&uuml;gbar}:</th><td>{#INPUT maxcoins integer $maxcoins}</td>
							</tr>
							<tr>
							  <th></th><td></td>
							</tr>
							</table>
						{#SUBMIT}
					{#END}
				{#END}
				</div>
			</div>
		</div>
	</div>
{#END}

{#SUB editorgas}
<div class="card ">
		<div class="card-header">
				<div class="card-header">
					<h3 class="card-title">Turnier Orga Verwaltung</h3>
				</div>
				<div class="alert alert-info" role="alert" style="margin-bottom: 0px;">
					<a href="tournament.php?frame=overview&amp;id={$id}" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-arrow-left"></i>zur&uuml;ck</a><br />
				</div>
		</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12">
				<div class="card-header">
					<h3 class="card-title">Hinzuf&uuml;gen</h3>
				</div>
				<div class="card-body">
					{#FORM addorga}
					<div class="row">
						<div class="col-1 col-sm-1 col-md-1 col-lg-1" align="right">{#INPUT type radio "orga"}</div>
						<div class="col-3 col-sm-3 col-md-2 col-lg-1">Orgas:</div>
						<div class="col-8 col-sm-8 col-md-9 col-lg-10">{#INPUT subject userfromgroup $subject param=orga allowempty=0}</div>
						<div class="col-1 col-sm-1 col-md-1 col-lg-1" align="right">{#INPUT type radio "user"}</div>
						<div class="col-3 col-sm-3 col-md-2 col-lg-1">User:</div>
						<div class="col-4 col-sm-4 col-md-4 col-lg-5">{#INPUT userident string}</div>
						<div class="col-4 col-sm-4 col-md-5 col-lg-5"></div>
					</div>
					<div class="row">
						<div class="col-4 col-sm-4 col-md-3 col-lg-2">{#SUBMIT hinzuf&uuml;gen}</div>
					</div>
					{#END}
				</div>
			</div>
			<div class="col-12">
				<div class="card-header">
					<h3 class="card-title">Entfernen</h3>
				</div>
				<div class="card-body">
				{#TABLE $orgas class="table"}
				  {#COL Name %name}
				  {#OPTION entfernen delorga}
				{#END}
				</div>
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB rem}
	{#FOREACH $tournament}
		<form method="post">
		  <input type="hidden" name="id" value="{$tournamentid}">
		  <input type="hidden" name="action" value="del">
		  <input class="button" type="submit" value="'{$TournamentString}' entfernen">
		</form>
		<br />
	{#END}
	<br />
	<a href="tournament.php" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-arrow-left"></i>&nbsp;zur&uuml;ck</a>
{#END}

{#SUB screenshots}
	<div class="card ">
		<div class="card-header">
			<div class="row">
				<div class="col">
					<a href="tournament.php?frame=tree&amp;id={$id}" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-arrow-left"></i>&nbsp;Zur&uuml;ck</a>&nbsp;
					<a href="content.php?frame=editimage&amp;id=0&amp;group_id={$group.id}" class="btn btn-success ml-1" role="button" data-toggle="tooltip" title="Add"><i class="far fa-plus-square"></i>&nbsp;add</a>
				</div>
			</div>
		</div>
		<div class="card-body">
		<div class="row">
			<div class="col-12">
			<h2>Screenshots</h2>
			{#TABLE $images}
			  {$imagecount} Bild{#WHEN "$imagecount!=1" er} ({#BYTE $imagesize kb 1} kByte)
			  <a href="content.php?frame=editimage&amp;id=0&amp;group_id={$group.id}" class="btn btn-success ml-1" role="button" data-toggle="tooltip" title="Add"><i class="far fa-plus-square"></i>&nbsp;add</a>
			  {#COL Vorschau style="padding:0px;"}<a href="image.php?name={%name}"><img border="0" src="image.php?frame=thumbnail&amp;name={$name}" alt="{$name}" width="{$tn_width}" height="{$tn_height}" /></a>
			  {#COL "Name (Gr&ouml;&szlig;e)<br />Beschreibung" $name}<strong><a href="image.php?name={%name}">{%name}</a></strong> ({#BYTE $size kb 1} kB)<br />{%description}
			  {#COL Titel %caption}
			  {#COL Leserecht<br/>Schreibrecht}{#RIGHT $view_right view}<br />{#RIGHT $edit_right edit}
			  {#COL Autor<br/>Datum}{%author}<br /><nobr>{#DATE $edit_time}</nobr>
			  {#FRAME edit link="content.php?frame=editimage&amp;id=$id" right=$edit_right}
			  {#OPTION L&ouml;schen deleteimage}
			{#END}
			</div>
		</div>
		</div>
	</div>
{#END}

{#SUB server}
	<div class="card ">
		<div class="card-header">
			<div class="row">
				<div class="col">
					<a href="tournament.php" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-arrow-left"></i>&nbsp;zur Turnierliste</a>&nbsp;
					<a href="turnieradm.php?frame=server" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-arrow-left"></i>&nbsp;zur Serverliste</a>&nbsp;
					<a href="" class="btn btn-secondary btn-sm active" role="button" aria-pressed="true"><i class="fas fa-server"></i>&nbsp;Verwaltung</a>&nbsp;
					<a href="turnieradm.php?frame=serverplan" class="btn btn-secondary btn-sm" role="button"><i class="far fa-calendar-alt"></i>&nbsp;Einsatzplan</a>&nbsp;
					<br />
					Hier k&ouml;nnen Server erstellt, bearbeitet und gel&ouml;scht werden.<br />
					{#FOREACH $games}<a href="turnieradm.php?frame=server&amp;game={$long}" class="btn btn-secondary btn-sm" role="button"><i class="far fa-keyboard"></i>&nbsp;{%long}</a>&nbsp;{#END}
				</div>
			</div>
		</div>
		<div class="card-body">
			<div class="row">
			<div class="col-12">
				<h4>{%game}</h4>
				<a href="turnieradm.php?frame=editserver&amp;game={$short}&amp;liga={$liga}" class="btn btn-success ml-1" role="button" data-toggle="tooltip" title="Add"><i class="far fa-plus-square"></i>&nbsp;hinzuf&uuml;gen</a>
				</div>
				<div class="col-12">
				{#TABLE $server class="table"}
					<a href="turnieradm.php?frame=editserver&amp;game={$short}&amp;liga={$liga}" class="btn btn-success ml-1" role="button" data-toggle="tooltip" title="Add"><i class="far fa-plus-square"></i>&nbsp;hinzuf&uuml;gen</a>
				  {#COL Game $game}
				  {#COL Name %name}<a href="turnieradm.php?frame=viewserver&amp;serverid={$id}">{%name}</a>
				  {#COL IP %ip}
				  {#COL Matches $count align=right}
				  {#FRAME bearbeiten link="turnieradm.php?frame=editserver&amp;serverid=$id"}
				  {#OPTION l&ouml;schen delserver}
				{#END}
			</div>
			</div>
		</div>
	</div>
{#END}

{#SUB serverliga}
	{#FORM bla method=get}
		{#INPUT frame hidden editserver}
		Liga: {#INPUT liga dropdown $liga param=$ligen}<br/>
			{#FOREACH $hiddens $value $key}
			  {#INPUT $key hidden $value}
			{#END}
		{#SUBMIT}
	{#END}
{#END}

{#SUB viewserver}
	<div class="card ">
		<div class="card-header">
			<div class="row">
				<div class="col-12">
					<a href="turnieradm.php?frame=server" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-arrow-left"></i>&nbsp;zur Serververwaltung</a><br />
				</div>
				<div class="col-12">
					<h3>Turnierauswahl</h3>
					{#FORM tosetserver}
						{#INPUT serverid hidden $serverid}
						{#INPUT id dropdown $id $tournaments}{#SUBMIT "Match zuweisen"}
					{#END}
				</div>
			</div>
		</div>
		<div class="card-body">
			<div class="row">
			<div class="col-12">
				<h3>Eins&auml;tze</h3>
				{#TABLE $matches class="table"}
				  {#COL "Zeit bis" $endtime}{#DATE $endtime}
				  {#COL Turnier $tournament}
				  {#COL Runde $round}<a href="tournament.php?frame=tree&amp;id={$tournamentid}#{$round}">{$round}</a>
				  {#COL Team1 $team1}
				  {#COL : $vs align=center}
				  {#COL Team2 $team2}
				{#END}
			</div>
		</div>
		</div>
	</div>
{#END}

{#SUB editserver}
	<div class="card ">
		<div class="card-header">
			<div class="row">
				<div class="col-12">
				<a href="turnieradm.php?frame=server" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-arrow-left"></i>&nbsp;zur&uuml;ck</a><br />
				</div>
			</div>
		</div>
		<div class="card-body">
		<div class="row">
			<div class="col-12">
			{#FORM server}
				<h3>Serverdaten</h3>
				<table>
				<tr>
				  <th>Liga</th><td>{%liga}</td>
				</tr>
				<tr>
				  <th>Spiel</th><td>{#INPUT game dropdown $game param=$games}</td>
				</tr>
				<tr>
				  <th>Name</th><td>{#INPUT name string $name}</td>
				</tr>
				<tr>
				  <th>IP</th><td>{#INPUT ip string $ip}</td>
				</tr>
				<tr>
				  <th>&nbsp;</th><td>{#SUBMIT speichern}</td>
				</tr>
				</table>
				{#INPUT serverid hidden $serverid}
			{#END}
			</div>
		</div>
		</div>
	</div>

{#END}

{#SUB serverplan}
	<div class="card ">
		<div class="card-header">
			<div class="row">
				<div class="col-12">
					<a href="tournament.php" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-arrow-left"></i>&nbsp;zur Turnierliste</a>&nbsp;
					<a href="turnieradm.php?frame=server" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-server"></i>&nbsp;Verwaltung</a>&nbsp;
					<a href="" class="btn btn-secondary btn-sm active" role="button" aria-pressed="true"><i class="far fa-calendar-alt"></i>&nbsp;Einsatzplan</a>&nbsp;
				</div>
			</div>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-12">
				<h3>Belegt:</h3>
				{#TABLE $matches}
				  {#COL "Zeit bis" $endtime}{#DATE $endtime}
				  {#COL Server $server}<a href="turnieradm.php?frame=viewserver&amp;serverid={$serverid}">{$server}</a>
				  {#COL IP $ip}
				  {#COL Turnier $tournament}
				  {#COL Runde $round}<a href="tournament.php?frame=tree&amp;id={$tournamentid}#{$round}">{$round}</a>
				  {#COL Team1 $team1}
				  {#COL : $vs align=center}
				  {#COL Team2 $team2}
				{#END}
				</div>
				<div class="col-12">
				<h3>Frei:</h3>
				{#TABLE $servers}
				  {#COL Spiel $game}
				  {#COL Server %name}<a href="turnieradm.php?frame=viewserver&amp;serverid={$id}">{$name}</a>
				  {#COL IP $ip}
				{#END}
				</div>
			</div>
		</div>
	</div>
{#END}

{#SUB setserver}
	{#FORM setserver}
			{#BACKLINK zur&uuml;ck}
		{#IF !empty($servers)}
			<h3>Server:</h3>
		{#END}
		{#INPUT serverid $serverinputtype $serverid $servers}<br />
		<br />
		{#IF !empty($matches)}
			<h3>Match:</h3>
		{#END}
		{#INPUT matchid $matchinputtype $matchid $matches}<br />
		<br />
		{#SUBMIT zuweisen}
	{#END}
{#END}

{#SUB overlap}
  <div class="card ">
    <div class="card-header">
      <div class="row">
        <div class="col">
          	<a href="tournament.php?frame=overview&id={$id}" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-arrow-left"></i>&nbsp;Zum Turnier</a>&nbsp;
						{#IF ($inactive)}
          		<a href="turnieradm.php?frame=overlap&id={$id}" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-layer-group"></i>&nbsp;Alle aktiven &Uuml;berschneidungen zeigen</a>&nbsp;
						{#END}
						{#IF !($inactive)}
							{#IF !($singletournament)}
								<a href="turnieradm.php?frame=overlap&id={$id}&inactive=1" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-user-alt-slash"></i>&nbsp;Ausgeschiedene Spieler einblenden</a>&nbsp;
							{#END}
							{#IF ($singletournament)}
								<a href="turnieradm.php?frame=overlap&id={$id}" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-layer-group"></i>&nbsp;Alle aktiven &Uuml;berschneidungen zeigen</a>&nbsp;
							{#END}
						{#END}
						{#IF !($singletournament)}
							<a href="turnieradm.php?frame=overlap&id={$id}&singletournament=1" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-street-view"></i>&nbsp;Spieler bei nur diesem Turnier zeigen</a>&nbsp;
						{#END}          
          <br />
        </div>
      </div>
    </div>
		<br>
		{#IF !($singletournament)}
			<h3>Anzahl an &Uumlberschneidungen bei "{$tournamentname}"</h3>
			<div class="card-body">
				<div class="row">
					<div class="col-12">
						<table class="table table-sm">
							<tr>
								<th>Turnier</th>
								<th>Ein-/Ausblenden</th>
								<th>Aktive Spieler</th>
								<th>Ausgeschieden</th>								
							</tr>
							{#FOREACH $overlaps}
							<tr style="background-color: #{$bgcolor}">
							  <td><a href="tournament.php?frame=overview&amp;id={$tid}">{$game}</a></td>
								{#IF !($filter_action)}									
									<td><a class="btn btn-info btn-sm" href="turnieradm.php?frame=overlap&id={$id}&inactive={$inactive}&ids[]={$filter_list}" role="button">Eingeblendet</a></td>
								{#END}
								{#IF ($filter_action)}
									<td><a  class="btn btn-outline-info btn-sm" href="turnieradm.php?frame=overlap&id={$id}&inactive={$inactive}&ids[]={$filter_list}" role="button">Ausgeblendet</a></td>
								{#END}
								<td>{$in}</td>
								<td>{$out}</td>								
							</tr>
							{#END}
						</table>
					</div>
				</div>
			</div>

			<h3>Spieler mit Turnierkollisionen bei "{$tournamentname}"</h3>
			<div class="card-body">
				<div class="row">
					<div class="col-12">
						<table class="table table-sm">
							<tr style="background-color: #a0a0a0">
								<th>User</th>
								<th>Clan</th>
								<th>Team</th>
								<th>Sitzplatz</th>
								<th>Turnier</th>              
							</tr>
							{#FOREACH $users}
							<tr style="background-color: #{$bgcolor}">
								<td><a href="user.php?frame=viewsubject&id={$uid}">{$username}</a></td>
								<td><a href="clan.php?frame=viewclan&id={$clan_id}">{$clan_name}</a></td>
								<td><a href="user.php?frame=editchilds&id={$teamid}">{$teamname}</a></td>
								<td><a href="seats.php?frame=block&blockid={$block_id}&ids[]={$uid}">{$seat_name}</a></td>
								<td style="background-color: #{$bgcolor_tournament}"><a href="tournament.php?frame=overview&amp;id={$tid}">{$game}</a></td>
							</tr>
							{#END}
						</table>
					</div>
				</div>
			</div>
		{#END}
		{#IF ($singletournament)}
			<h3>Spieler ohne Kollision bei "{$tournamentname}"</h3>
			<div class="card-body">
				<div class="row">
					<div class="col-12">
						<table class="table table-sm">
							<tr style="background-color: #c0c0c0">
								<th>User</th>
								<th>Clan</th>
								<th>Team</th>
								<th>Sitzplatz</th>
							</tr>
							{#FOREACH $userssolo}
							<tr style="background-color: #{$bgcolor}">
								<td><a href="user.php?frame=viewsubject&id={$uid}">{$username}</a></td>
								<td><a href="clan.php?frame=viewclan&id={$clan_id}">{$clan_name}</a></td>
								<td><a href="user.php?frame=editchilds&id={$teamid}">{$teamname}</a></td>
								<td><a href="seats.php?frame=block&blockid={$block_id}&ids[]={$uid}">{$seat_name}</a></td>       
							</tr>
							{#END}
						</table>
					</div>
				</div>
			</div>
		{#END}
  </div>
{#END}


{#SUB doku}
	<h1 align="center" name="title">Turniersystem Dokumentation f&uuml;r Organisatoren</h1>
	<div align="left"> {! because you don't know which framework is around }
	<div align="center"><strong>Version 1.6, 2005-04-13</strong></div>

	<h2><a name="Inhalt"></a>Inhalt</h2>
	<ol>
	<li><a href="#Einleitung">Einleitung</a>
	  <ol>
	  <li>Allgemeines</li>
	  <li>Rechte</li>
	  </ol>
	</li>
	<li><a href="#Turniereerstellen">Turniere erstellen</a></li>
	<li><a href="#Anmeldung">Anmeldung</a></li>
	<li><a href="#Turnierbaum">Der Turnierbaum</a></li>
	<li><a href="#Seeding">Seeding</a></li>
	<li><a href="#Serververwaltung">Serververwaltung</a></li>
	<li><a href="#Ergebnisseeintragen">Ergebnisse eintragen</a>
	  <ol>
	  <li>Wer darf eintragen?</li>
	  <li>Rundenzeit &auml;ndern</li>
	  <li>Ergebnisse &auml;ndern</li>
	  </ol>
	</li>
	<li><a href="#BeendeteTurniere">Beendete Turniere</a>
	  <ol>
	  <li>Allgemeines</li>
	  <li>Export</li>
	  </ol>
	</li>
	</ol>

	<ol>
	<h2><li><a name="Einleitung"></a>Einleitung</h2>
	<ol>
	<h3><li>Allgemeines</h3>
	<p>Das Turniersystem dient der Durchf&uuml;hrung von Turnieren (wer h&auml;tte das jetzt nicht gedacht?).
	Dazu werden die einzelnen Turniere in einer Liste zusammengefasst und f&uuml;r jedes Turnier gibt es eine &Uuml;bersichts-, Baumansichts- und Platzierungsseite.
	Um ein Turnier durchzuf&uuml;hren m&uuml;ssen folgende Schritte durchgef&uuml;hrt werden:
	<ol>
	  <li><a href="#Turniereerstellen">Turnier anlegen</a></li>
	  <li><a href="#Anmeldung">Teams melden sich an</a></li>
	  <li>Turnier wird gestartet</li>
	  <li><a href="#Ergebnisseeintragen">Spiele werden ausgetragen und Ergebnisse eingetragen</a></li>
	  <li>Turnier wird mit dem letzten Ergebnis beendet</li>
	</ol>
	Diese Punkte werden in den n&auml;chsten Abschnitten detailliert beschrieben.
	</p></li>
	<h3><li>Rechte</h3>
	<p>Die Verwaltung ist in zwei Rechtegruppen unterteilt: Admin und Orga.
	Ein Admin darf alles, ein Orga darf nur das Turnier bearbeiten, f&uuml;r welches er eingetragen wurde.
	Im Folgenden wird deshalb immer Orga verwendet, wenn dessen Rechte ausreichen, ohne explizit den Admin zu nennen.
	<br />
	Nur ein Admin hat das Recht Turniere zu erstellen und wieder zu l&ouml;schen, sowie Turnierorgas festzulegen.
	Ausserdem darf nur ein Admin einen Turnierexport erstellen und die Paarungen im Turnierbaum &auml;ndern (Seeding).
	</p></li>
	</ol>
	<a href="#Inhalt">&uarr; Inhalt</a>
	</li>

	<h2><a name="Turniereerstellen"></a><li>Turniere erstellen (Admin)</h2>
	<p>Um ein neues Turnier anzulegen muss auf der Turnierliste unten rechts auf den Link "Turnier hinzuf&uuml;gen" geklickt werden.
	Anschlie&szlig;end m&uuml;ssen alle Parameter des Turnieres angegeben werden. Diese sind:<br />
	<dl>
	<dt>Spiel</dt>
	<dd>Dies ist der Titel des Turnieres und gibt das Spiel an, welches gespielt wird.
	<br />
	Aus der Liste muss das entsprechende Spiel ausw&auml;hlt werden. Neue Spiele k&ouml;nnen von der Turnierliste aus unter dem Link "Spielnamen &auml;ndern" eingetragen werden. Die in der Liste angegebenen K&uuml;rzel werden in der Datenbank gespeichert und in volle Spieltitel umgesetzt.
	</dd>
	<dt>Teamgr&ouml;sse</dt>
	  <dd>Legt die Anzahl der Spieler je Team fest. Auch die Teamgr&ouml;sse von 1 wird hier als Team bezeichnet (1on1).</dd>
	<dt>Max Teams</dt>
	  <dd>Legt die maximale Anzahl teilnehmender Teams fest. F&uuml;r KO-Spiele werden entsprechend Freilose hinzugef&uuml;gt um die n&auml;chst h&ouml;here zweier Potenz zu erreichen (z.B. 16, 32, 64).</dd>
	<dt>Typ</dt>
	  <dd>Der Typ gibt an, nach welchem Muster die Spiele ausgef&uuml;hrt und gewertet werden.
	  <br />
	  Beim Typ "Gruppen" spielen die Teams in <em>Gruppen</em> (siehe unten) jeder gegen jeden. Dabei bringt ein Sieg 3 Punkte, ein Unentschieden 1 Punkt (Spiele gegen Freilose werden auch gewertet). Der/die Teams mit den meisten Punkten in der Gruppe kommen in die KO-Runde.
	  <br />
	  Beim Typ "KO" spielen je zwei Teams gegeneinander und der Gewinner spielt anschliessend gegen einen anderen Gewinner. Dies wird solange durchgef&uuml;hrt bis letztlich ein Gewinner &uuml;brig bleibt. Hierbei muss jedesmal ein Gewinner ermittelt werden, ein Unentschieden kann es nicht geben.
	  Entsprechend der Reihenfolge, in der die Teams verlieren werden die Platzierungen ermittelt. Dadurch gibt es einen 1. und 2. Platz, zwei 3., vier 5. usw.. Siehe auch <em>Loserbracket</em>.
	  </dd>
	<dt>Loserbracket</dt>
	  <dd>Gibt an ob nach Single- ("Nein") oder Double-Elimination ("Ja") gespielt wird.
	  <br />
	  Wenn KO-Spiele ausgetragen werden, kommt nur der Gewinner weiter und jeder Verlierer "fliegt raus", spielt also in dem Turnier nicht mehr mit (<em>Singel Elimination</em>).
	  Es kann aber zus&auml;tzlich noch einen zweiten Turnierbaum geben, in welchem die Verlierer nochmals gegeneinander Spielen. Dadurch fliegt man erst nach zweimaligem verlieren aus dem Turnier (<em>Double Elimination</em>).
	  Das Team, welches das Loserbracket (den zweiten Baum) gewinnt, spielt anschlie&szlig;end gegen den Gewinner des Winnerbracket (erster Baum). Dabei wird oft die Regel angewand, dass das Team aus dem Winnerbracket zweimal verlieren muss, um dem <em>Double Elimination</em> gerecht zu werden.
	  </dd>
	<dt>Liga</dt>
	  <dd>Gibt an ob das Turnier nach Ligaregeln gespielt wird. Entsprechend wird bei der Anmeldung eine "LigaID" angegeben. Dies ist eine vorhandene NGL/WWCL Player/Clan-ID oder ein Platzhalter. Diese Option ist recht heikel, da sie nachtr&auml;glich nur mit Aufwand aktiviert werden kann, da jedem Team manuell die LigaID zugewiesen werden muss.</dd>
	<dt>Gruppen</dt>
	  <dd>Legt die Anzahl der Gruppen fest. Dies ist nur notwendig wenn der Typ "Gruppen" ausgew&auml;hlt wurde.
	  <br />
	  Mit dieser Option wird festgelegt, wieviele Teams (ungef&auml;hr) im anschlie&szlig;enden KO-Baum teilnehmen.
	  Durch eine h&ouml;here Anzahl an Gruppen wird die Anzahl der Teams je Gruppe geringer und der KO-Baum gr&ouml;sser.
	  Wenn nur eine Gruppe festgelegt wird, kann anhand der Punkte die Platzierung der Teams ermittelt werden, da nur ein Team als Gewinner im KO-Baum erscheint.
	  </dd>
	<dt>Baumdarstellung</dt>
	  <dd>Gibt an wie der Baum dargestellt wird. Es gibt diverse Templates hierf&uuml; im FLIP, welche f&uuml;r bestimmte Turnierr&ouml;&szlig;en geeignet sind. So ist z.B. die Art "schmal" f&uuml;r sehr gro&szlig;e Turnierb&auml;e mit 128 und mehr Teams geeignet, w&auml;hrend die "default"-Darstellung f&uuml;r kleine Turnierb&auml;me gut ist, welche eine Seite im Browser nicht komplett f&uuml;llen.
	  </dd>
	<dt>frei f&uuml;r <18</dt>
	  <dd>Im Profil des Teilnehmers muss "vollj&auml;hrig" (is_adult) auf "Ja" stehen um sich zum Turnier anzumelden.</dd>
	<dt>Screenshot Upload</dt>
	  <dd>Legt fest ob beim Eintragen von Ergebnissen, der Upload von Screenshots m&ouml;glich sein soll. Die Bilder werden im FLIP gespeichert, d.h. in der Datenbank und sind unter Admin->Content->Screenshots wiederzufinden.
	  <br />
	  Diese Option ist zur Zeit auf 2MB beschr&auml;nkt!</dd>
	<dt>Rundendauer</dt>
	  <dd>Gibt die Dauer einer Runde an. Nach dieser Zeit k&ouml;nnen die Teams keine Ergebnisse mehr eintragen. Das weitere Vorgehen liegt dann beim Orga.</dd>
	<dt>Beginn</dt>
	  <dd>Gibt den Beginn des Turnieres an. Das Turnier wird <strong>nicht</strong> automatisch gestartet! Es dient lediglich als Information (die Zeiten der Spiele basieren allerdings auf der Startzeit). Die Startzeit wird vom Turniersystem &uuml;berschrieben, wenn das Turnier gestartet wird, also die Paarungen erstellt werden.</dd>
	<dt>Kosten ({$currency})</dt>
	  <dd>Gibt an wie viele {$currency} dem Spieler bei der Teilnahme von seinem Guthaben abgezogen werden. Dazu muss in der Config "tournaments_coins" > 0 sein, sonst ist eine Teilnahme nicht m&ouml;glich.</dd>
	<dt>Beschreibung/Regeln</dt>
	  <dd>Hier kann ein beschreibender Text und/oder das Regelwerk bekannt gegeben werden. Dieser Text wird auf der &Uuml;bersichtsseite des Turnieres angezeigt.</dd>
	<dt>Status</dt>
	  <dd>Die Auswahlm&ouml;glichkeiten haben folgende Bedeutungen:
	  <dl>
	  <dt>geschlossen</dt>
		<dd>Es sind keine Aktionen m&ouml;glich (anmelden, Ergebnisse eintragen, usw.).</dd>
	  <dt>Anmeldung</dt>
		<dd>Teams k&ouml;nnen sich f&uuml;r das Turnier anmelden und die angemeldeten Teams k&ouml;nnen bearbeitet werden.
		<br />
		<span class="important">Achtung:</span> Wenn dieser Status gesetzt wird, werden alle Paarungen und Platzierungen des Turnieres gel&ouml;scht!
		</dd>
	  <dt>Aufw&auml;rmphase</dt>
		<dd>Nur die Orgas k&ouml;nnen die Spiele sehen. Die Spiele werden erstellt wenn der Status zuvor Anmeldung war.<br />
		Dies ist n&uuml;tzlich um z.B. &Auml;nderungen vorzunehmen (Seeding) bevor die Paarung &ouml;ffentlich gemacht werden. Dieser Status kann nur manuell &uuml;ber den Punkt "bearbeiten" auf der Turnier&uuml;bersichtsseite ausgew&auml;hlt werden und es muss hier&uuml;ber auch der Status auf KO-Spiele bzw. Gruppenspiele gesetzt werden!
		</dd>
	  <dt>Gruppenspiele</dt>
		<dd>Der <em>Typ</em> ist "Gruppen" und Spiele werden ausgetragen, d.h. Ergebnisse k&ouml;nnen eingetragen werden.
		<br />
		Dieser Status wird automatisch vom Turniersystem gesetzt, wenn "Turnier starten" ausgew&auml;hlt wird. Umgekehrt wird das Turnier gestartet, wenn der Status von "Anmeldung" auf "Gruppenspiele" gesetzt wird. (Der <em>Typ</em> muss nat&uuml;rlich "Gruppen" sein!)
		</dd>
	  <dt>KO-Spiele</dt>
		<dd>Es werden Spiele nach dem KO-System ausgetragen (<em>Typ</em> = "KO"). Die Ergebnisse der Spiele k&ouml;nnen von den entsprechenden Teams eingetragen werden.
		<br />
		Dieser Status wird automatisch vom Turniersystem gesetzt, wenn "Turnier starten" ausgew&auml;hlt wird. Umgekehrt wird das Turnier gestartet, wenn der Status von "Anmeldung" auf "KO-Spiele" gesetzt wird. (Der <em>Typ</em> muss nat&uuml;rlich "KO" sein!)
		</dd>
	  </dl>
	</dd>
	</dl>
	<br />
	Anschlie&szlig;end auf den Button "hinzuf&uuml;gen" klicken. Das Turnier wurde erstellt und man ist auf der "Turnier bearbeiten"-Seite. Hier k&ouml;nnen alle Optionen nochmals ge&auml;ndert werden. &Uuml;ber den Link "zur&uuml;ck zum Turnier" gelangt man auf die &Uuml;bersichtsseite des Turnieres.
	</p>
	<p>Um jemanden die Administration des Turnieres zu erm&ouml;glichen, muss unten auf der Tunier&uuml;bersichtsseite der Admin-Link "Orgaverwaltung" angeklickt werden. Anschliessend muss ein oder mehrere Orgas ausgew&auml;hlt werden (aus der Gruppe Orga, ansonsten muss &uuml;ber das Rechtemanagemant dem User das Recht "tournament_orga" &uuml;ber das "Turniersubjekt<em>TurnierID</em>" gegeben werden!).
	</p>
	<a href="#Inhalt">&uarr; Inhalt</a>
	</li>

	<h2><a name="Anmeldung"></a><li>Anmeldung (User/Orga)</h2>
	<p>An einem Turnier k&ouml;nnen nur Teams teilnehmen. Man kann entweder ein neues Team gr&uuml;nden oder ein bestehendes ausw&auml;hlen.
	F&uuml;r ersteres braucht ein Spieler nur auf den Link "Anmeldung" auf der Turnier&uuml;bersichtsseite zu klicken (wenn er das Recht Config->tournament_playright hat). Anschliessend muss ein Teamname eingegeben werden (ausser <em>Teamgr&ouml;sse</em> = 1 (1on1)), dann wird automatisch der Nickname verwendet). Falls das Turnier ein Ligaturnier ist wird noch eine LigaID abgefragt.
	<br />
	Um in einem Team mitzuspielen kann ein Spieler bei dem entsprechenden Team auf der Turnier&uuml;bersichtsseite auf den Link "Anfrage" klicken. Dadurch wird f&uuml;r das Team unter "Spieler hinzuf&uuml;gen"&sup1; die Anfrage wiederum als Link angezeigt. Ein Teammitglied muss der Anfrage nur durch Klick hierauf zustimmen.
	<br />
	Dies funktioniert auch umgekehrt, indem zu erst ein Teammitglied &uuml;ber "Spieler hinzuf&uuml;gen"&sup1; eine Einladung verschickt, welche auf der Turnier&uuml;bersichtsseite angenommen wird.
	</p>
	<p>Nur w&auml;hrend der Anmeldung k&ouml;nnen die Teams bearbeitet werden. Neben dem Hinzuf&uuml;gen und Entfernen von Spielern ist es m&ouml;glich:
	<ul>
	  <li>das Team umzubenennen,</li>
	  <li>die LigaID zu &auml;ndern,</li>
	  <li>das Team aus dem Turnier zu nehmen</li>
	  <li>und das Team zu l&ouml;schen (sofern es nicht in anderen Turnieren ist).</li>
	</ul>
	Diese M&ouml;glichkeiten hat jedes Teammitglied f&uuml;r sein eigenes Team und die Turnierorgas f&uuml;r jedes Team.<br />
	Wird ein Team aus dem Turnier genommen bzw. gel&ouml;scht und bereits Paarungen/Platzierungen existieren, dann wird das Team durch ein Freilos ersetzt!
	</p>
	<p>&sup1; <small>auf der Turnier&uuml;bersichtsseite auf den Teamnamen klicken um das Team zu bearbeiten. Wenn das Team noch nicht voll ist, findet man hier den Link "Spieler hinzuf&uuml;gen". Turnierorgas d&uuml;rfen alle Teams bearbeiten.</small>
	</p>
	<a href="#Inhalt">&uarr; Inhalt</a>
	</li>

	<h2><a name="Turnierbaum"></a><li>Der Turnierbaum (User/Orga)</h2>
	<p>Der Turnierbaum zeigt alle Spiele an. Die KO-Spiele werden als Baum hierarchisch nach Winner- und Loserbracket getrennt. Dabei wird die erste Runde des Loserbracket gekreuzt. D.h. der Verlierer des obersten Winnermatches der ersten Runde kommt nach unten in das Loserbracket. Der oberste Verlierer der zweiten Runde kommt nach oben in das Loserbracket und spielt gegen den untersten Verlierer der ersten Runde. Dadurch wird vermieden, dass ein gro&szlig;teil der Teams zweimal gegeneinander spielt.<br />
	Die Gruppenspiele werden nach Gruppen getrennt und nach Runden sortiert.
	</p>
	<a href="#Inhalt">&uarr; Inhalt</a>
	</li>

	<h2><a name="Seeding"></a><li>Seeding (Orga)</h2>
	<p>Das Seeding (platzieren der Teams) dient haupts&auml;chlich dazu, die Favoriten in der 1. Runde auseinander zu setzten, damit diese nicht sofort rausfliegen.<br />
	Im Turniersystem wird unter Seeding ganz allgemein das Ver&auml;ndern der Teams im Turnierbaum verstanden, allerdings kann nur in der 1. Runde, wenn kein Ergebnis eingetragen ist, das Seeding durchgef&uuml;hrt werden. Die Paarungen werden auf der "Ergebnis &auml;ndern"-Seite festgelegt. Man gelangt also &uuml;ber den Link "vs." zum eintragen bzw. editieren des Ergebnisses auf diese Seite.<br />
	Um eine Paarung zu &auml;ndern stehen nur Teams zur Verf&uuml;gung, welche in der selben oder folgenden Runde noch nicht eingetragen sind und noch keine Platzierung haben. Dadurch wird verhindert, dass Teams doppelt im Baum auftauchen und die Integrit&auml;t gew&auml;hrleistet ist. Mann muss zum wechseln von Teams also immer erst ein Freilos einsetzen um das Team aus der Runde zu nehmen und anschliessend mit einem anderen Team tauschen. Um w&auml;hrend des Turnieres ein Seeding durchzuf&uuml;hren, m&uuml;ssen erst alle Spiele bis zur 1. Runde zur&uuml;ckgesetzt werden.</p>
	<a href="#Inhalt">&uarr; Inhalt</a>
	</li>

	<h2><a name="Serververwaltung"></a><li>Serververwaltung (Orga)</h2>
	<p>Jedem Match kann ein Server zugewiesen werden. Dadurch soll die Koordination mit den Spielern erleichtert werden, da diese im Turnierbaum zu ihrem Match sehen k&ouml;nnen, wo sie hin m&uuml;ssen. Die Zuweisung erfolgt momentan von einem Orga, welcher aus einer Liste aller Server einen ausw&auml;hlt bzw. einem Server ein Match zuweist.
	</p>
	<p>Die Serververwaltung wird von der Turnierliste aus &uuml;ber den Link "Turnierserver verwalten" erreicht. Hier hat man dann eine Liste aller eingetragenen Gameserver.<br />
	Um einen Server hinzuzuf&uuml;gen einfach auf den Link "hinzuf&uuml;gen" klicken. Anschliessend wird das Spiel ausgew&auml;hlt, ein Name und die Adresse angegeben.
	</p>
	<p>Um einem Match ein Server zuzuweisen, kann man auf der Server&uuml;bersicht einen Server anklicken und anschliessend ein Turnier ausw&auml;hlen. Umgekehrt kann man auch zu jedem Match auf den "Svr"-Link klicken und einen Server ausw&auml;hlen.<br />
	Die Zuweisung eines Servers zu einem Match ist unter dem Eintragelink ("vs.") ersichtlich (auch f&uuml;r die beteiligten Teams). Die Nutzung aller Server kann unter dem Punkt "Einsatzplan" eingesehen werden. Hier werden alle zugewiesenen Matches aufgelistet.
	</p>
	<a href="#Inhalt">&uarr; Inhalt</a>
	</li>

	<h2><a name="Ergebnisseeintragen"></a><li>Ergebnisse eintragen (User/Orga)</h2>
	<ol>
	<h3><li>Wer darf eintragen?</h3>
	<p>Grunds&auml;tzlich d&uuml;rfen die Teams ihr jeweiliges Ergebnis eintragen. Dies kann durch die Configoption (im Standardmen&uuml;: admin->Config) "tournament_losersubmit" eingeschr&auml;nkt werden, indem nur der Verlierer das Ergebnis eintragen darf. Als weitere Einschr&auml;nkung z&auml;hlt die Zeit (<em>Rundendauer</em>) welche f&uuml;r jedes Spiel gesetzt wird. Wenn diese &uuml;berschritten wird, k&ouml;nnen nur noch Turnierorgas Ergebnisse eintragen. Oder es kann in der Config die Option "tournament_defaultwinbytimeout" auf 1 gesetzt werden, wodurch automatisch ein Team zuf&auml;llig als Gewinner eingetragen wird wenn die Zeit abgelaufen ist.<br />
	Wenn in der Config die Option "tournament_comment_orgaonly" auf 1 steht d&uuml;rfen nur Turnierorgas Kommentare zu Spielen schreiben, sonst auch das Team welches eintr&auml;gt.
	</p></li>
	<h3><li>Rundenzeit &auml;ndern</h3>
	<p>Um die Endzeit einer Runde zu &auml;ndern, einfach auf eine Endzeit eines noch offenen Spiels klicken und im Formular die entsprechende Zeit angeben. Alle nachfolgenden Runden die noch offen sind erhalten automatisch die neue Zeit als Endzeit. Dadurch k&ouml;nnen z.B. Turnierpausen einfach realisiert werden, wenn bereits ein Spiel in der Runde nach der Pause existiert.
	</p></li>
	<h3><li>Ergebnisse &auml;ndern</h3>
	<p>Ergebnisse der zu letzt gespielten Paarungen k&ouml;nnen von den Orgas direkt ge&auml;ndert werden. Ausserdem kann ein Spiel auch zur&uuml;ckgesetzt werden, d.h. der Punktestand wird gel&ouml;scht und das Spiel ist somit wieder offen. Hierdurch kann anschlie&szlig;end ein vorheriges Ergebnis bearbeitet oder wiederum zur&uuml;ckgesetzt werden. Auf diese Weise l&auml;sst sich der gesamte Turnierbaum bearbeiten.<br />
	Falls der Gewinner durch eine Ergebnis&auml;nderung wechseln sollte, wird dies f&uuml;r nachfolgende Paarungen entsprechend ge&auml;ndert.
	</p></li>
	</ol>
	<a href="#Inhalt">&uarr; Inhalt</a>
	</li>

	<h2><a name="BeendeteTurniere"></a><li>Beendete Turniere (Orga/Admin)</h2>
	<ol>
	<h3><li>Allgemeines</h3>
	<p>Nachdem das lezte Ergebnis eingetragen wurde wird der Status des Turnieres automatisch auf "beendet" gesetzt. Das Ranking ist vollst&auml;ndig unter dem Punkt "Platzierungen" einzusehen. Wenn das Turnier beendet wurde k&ouml;nnen Ergebnisse nachtr&auml;glich nur ge&auml;ndert werden, wenn der Status wieder auf "KO-Spiele" bzw. "Gruppenspiele" zur&uuml;ckgesetzt wird.
	</p></li>
	<h3><li>Export</h3>
	<p>F&uuml;r diverse Ligen und als HTML kann ein Export erstellt werden. Der HTML-Export gibt eine Liste aller Turniere und der Platzierungen aus. Zur Zeit werden die LAN-Ligen <em>WWCL</em> und <em>NGL</em> unterst&uuml;tzt. Um solch einen Export zu erstellen, muss die Exportseite &uuml;ber den Link "Export" auf der Turnierlistenseite angeklickt werden. Anschlie&szlig;end werden die ben&ouml;tigten Daten abgefragt und die gew&uuml;nschten Turniere k&ouml;nnen aus einer Liste ausgew&auml;hlt werden. Die generierte Datei wird dann als Download angeboten.
	</p></li>
	</ol>
	<a href="#Inhalt">&uarr; Inhalt</a>
	</li>
	</ol>
	</div>
{#END}

{#SUB export}
	<a href="tournament.php">zur&uuml;ck zur Turnierliste</a><br />
	<h2>Typ</h2>
	<h3>{#FOREACH $exporttypes $type}<a href="turnieradm.php?frame=export&amp;type={$type}">{$type}</a>{#MID} &middot; {#END}</h3>
	{#VAR sub=_export_$exporttype}
{#END}

{#SUB _export_HTML}
	<h2>HTML</h2>
	{#FORM action="turnieradm.php?frame=resultexport"}
		Liste der ersten {#INPUT firstn dropdown param=$firstn} als {#INPUT outputtype dropdown param=$outputs} {#SUBMIT erstellen}
	{#END}
{#END}

{#SUB _export_WWCL}
	<h2>WWCL</h2>
	{#FORM WWCLexport}
		{#INPUT type hidden "WWCL"}
		<table border="0" cellspacing="0" cellpadding="3">
		<tr>
		  <td colspan="2" align="center"><h3>Partydaten</h3></td>
		</tr>
		<tr>
		  <td>WWCL-PID</td><td>{#INPUT party_id integer}</td>
		</tr>
		<tr>
		  <td>WWCL-PVDID</td><td>{#INPUT party_vdid integer}</td>
		</tr>
		<tr>
		  <td>Ort</td><td>{#INPUT party_ort string}</td>
		</tr>
		<tr>
		  <td colspan="2" align="center"><h3>beendete Turniere</h3></td>
		</tr>
		{#FOREACH $turniere}
		<tr>
		  <td colspan="2">{#INPUT tournaments[] checkbox 1 param=$tournamentid} {$tournamenttitle}</td>
		</tr>
		{#END}
		</table>
		{#SUBMIT export}
	{#END}
{#END}

{#SUB _export_NGL}
	<h2>NGL</h2>
	{#FORM NGLexport}
		{#INPUT type hidden "NGL"}
		<table border="0" cellspacing="0" cellpadding="3">
		<tr>
		  <td colspan="2" align="center"><h3>Partydaten</h3></td>
		</tr>
		<tr>
		  <td>NGL-EventID</td><td>{#INPUT party_id integer}</td>
		</tr>
		<tr>
		  <td>NGL-Contact (email)</td><td>{#INPUT party_mail email}</td>
		</tr>
		<tr>
		  <td>Datum</td><td>{#INPUT party_datum date $party_datum}</td>
		</tr>
		<tr>
		  <td colspan="2" align="center"><h3>beendete Turniere</h3></td>
		</tr>
		{#FOREACH $turniere}
		<tr>
		  <td colspan="2">{#INPUT tournaments[] checkbox 1 param=$tournamentid} {$tournamenttitle}</td>
		</tr>
		{#END}
		</table>
		{#SUBMIT export}
	{#END}
{#END}

{#SUB _export_CSV}
	{#FORM CSVexport}
		{#INPUT nothing hidden ""}
		{#SUBMIT "alle Turniere als .csv-Datei exportieren" "t"}
	{#END}
	<br/>
	{#FORM CSVranking}
		{#INPUT nothing hidden ""}
		{#SUBMIT "alle Rankings als .csv-Datei exportieren" "r"}
	{#END}
{#END}

{#SUB _export_keine}
	Bitte eine Exportmethode w&auml;hlen!
{#END}
