{#SUB default}
	{#TPL $text}
	<br />
	{#FOREACH $groups}
	<div id="groupcontainer" style="padding-right: 10px; width: 50%; height: 20%">
		<div style="float: left">
			{#IF !empty($imageid)}
				<img src="image.php?name={$imageid}" alt="Bild{$image_id}" />
			{#ELSE}
				&nbsp;
			{#END}			
		</div>
		<div style="text-align: center; margin: auto; padding-top: 5%">
			<h3><a href="shop.php?frame=showgroup&id={$id}">{%name}</a></h3>
			<b>{%description}</b>
		</div>
	</div>
	<hr style="clear: both; border: 0px" />
	{#ELSE}
		<ul>
			<li>
				<h3>Keine Kategorien vorhanden!</h3>
				<strong>Momentan k&ouml;nnen wir dir nichts anbieten, sorry!</strong>
			</li>
		</ul>
	{#END}
{#END}

{#SUB showgroup}
	{#FOREACH $items}
		{#IF $enabled}
			<div style="color: #000000; border-color: #000000;">
		{#ELSE}
			<div style="color: #808080; border-color: #808080;">
		{#END}
				<div style="float: left; padding-right: 5px; padding-bottom: 2px">
					{#IF $enabled}
						<img style="font-decoration: none; border: 0px" width="64" height="64" alt="In den Einkaufswagen" src="{#STATICFILE images/shop/generic_item.png}" />
					{#ELSE}
						<img style="font-decoration: none; border: 0px" width="64" height="64" alt="In den Einkaufswagen" src="{#STATICFILE images/shop/generic_item_grey.png}" />
					{#END}
				</div>
				<div style="float: left; text-align: left">
					<h3>{%name}</h3>
					<b>{#IF $enabled}{%description}{#ELSE}Dieser Artikel ist momentan leider nicht verf&uuml;gbar, sorry!{#END}</b>
				</div>
				<div style="height: 40px; float: right; padding-right: 32px; margin-top: 24px">
					<div style="float: left; padding-top: 21px">
						{#IF $stockenabled}
							<b>&euro; {%price} pro St&uuml;ck (<i>{$remaining_amount} von {$stockamount} &uuml;brig</i>) </b>
						{#ELSE}
							<b>&euro; {%price} pro St&uuml;ck</b>
						{#END}
					</div>
					&nbsp;&nbsp;
					<div style="float: right">
						{#IF $enabled}
							{#HTMLACTION cartadditem params="array(itemid=$id)" formattrs="array(style=\"width: 32px\")"}
								<img style="font-decoration: none; border: 0px" alt="In den Einkaufswagen" src="{#STATICFILE images/shop/cart_add.png}" />
							{#END}
						{#ELSE}
							<img style="font-decoration: none; border: 0px" alt="In den Einkaufswagen" src="{#STATICFILE images/shop/cart_grey.png}" />
						{#END}
					</div>
				</div>
				{#WHEN $enabled "<hr align=\"left\" style=\"border: 1px solid #000000; clear: both;\"/>" "<hr align=\"left\" style=\"border: 1px solid #808080; clear: both;\"/>"}
			</div>		
	{#ELSE}
		<h3>Keine Artikel vorhanden!</h3>
		<strong>Hier wird dir unser Angebot aufgelistet, sobald es Artikel in dieser Gruppe gibt!</strong>
		<br />
	{#END}
	<br />
	<a href="shop.php">Zur&uuml;ck</a>
{#END}

{#SUB showshoppingcart}
	{#TPL $text}
	<br />
	{#TABLE $contents}
		<div style="text-align: right; font-weight: bold">
			Gesamtsumme: &euro; {#NUMBER $total_sum 2}<br />
			<a href="shop.php?frame=showmybalance"><i>Dein Guthaben: &euro; {#NUMBER $balance 2}</i></a>
		</div>
		{#COL Name $name}{%name}
		{#COL Beschreibung $description}{%description}
		{#COL Produktionswarteschlange $queued align=center}{#WHEN $queued "Ja" "Nein"}
		{#COL Menge $amount align=center}{%amount}
		{#COL St&uuml;ckpreis $price align=center}&euro; {#NUMBER $price 2}
		{#COL St&uuml;cksumme $item_sum align=right}&euro; {#NUMBER $item_sum 2}
		{#OPTION "Artikel l&ouml;schen" cartdeleteitems}
		{#OPTION "Anzahl auf" cartchangeamount}&nbsp;{#INPUT newamount integer}&nbsp;&auml;ndern
	{#END}
	<br />
	<div style="width: 250px">
		<div id="paybuttoncontainer" style="float: left; margin: auto">
		{#IF $balance > 0.0}
		{#HTMLACTION cartCheckOut confirmation="Die Artikel im Warenkorb werden mit deinem Guthaben verrechnet - einverstanden?" buttonattrs="array(style=\"background-color: #00DD00\")"}
			<div>
				<img alt="Checkout" src="{#STATICFILE images/shop/checkout.png}" />
				<div style="margin: auto;">Zur Kasse gehen</div>
			</div>
		{#END}
		{#ELSE}
		{#HTMLACTION cartCheckOut confirmation="Die Artikel im Warenkorb werden mit deinem Guthaben verrechnet - einverstanden?" buttonattrs="array(style=\"background-color: #00DD00\" disabled=\"disabled\")"}
			<div>
				<img alt="Checkout" src="{#STATICFILE images/shop/checkout.png}" />
				<div style="margin: auto;">Zur Kasse gehen</div>
			</div>
		{#END}
		{#END}
		</div>
		<div id="orderbuttoncontainer" style="float: right; margin: auto">
		{#HTMLACTION cartOrder confirmation="Die Artikel im Warenkorb werden nur bestellt - du musst sie nacher noch bezahlen, OK?" buttonattrs="array(style=\"background-color: #DDDD00\")"}
			<div>
				<img alt="Checkout" src="{#STATICFILE images/shop/checkout.png}" />
				<div style="margin: auto;">Nur bestellen</div>
			</div>
		{#END}
		</div>
		<hr style="border: 0px; clear: both" />
	</div>
{#END}

{#SUB menushoppingcart}
		<div style="float: left; padding-left: 5px">
			<a href="shop.php?frame=showshoppingcart">
				<img style="font-decoration: none; border: 0px" width="32" height="32" alt="Einkaufswagen enth&auml;lt Artikel" src="{#STATICFILE images/shop/cart.png}" />
			</a>
		</div>
		<div style="text-align: center; float: left; valign: center; width: 98px; height: 32px; padding-top: 5px">
			<a href="shop.php?frame=showshoppingcart">
				{#IF $num_items > 0}
					{$num_items} Artikel f&uuml;r<br />
					&euro;&nbsp;{#NUMBER $total_sum 2}
				{#ELSE}
					Der Warenkorb<br/>ist leer!
				{#END}
			</a>
		</div>
		<hr style="border: 0px; clear: both;" />
		<div style="padding-top: 5px"><i>Dein Guthaben: &euro; {#NUMBER $balance 2}</i></div>
{#END}

{#SUB showmybalance}
	{#TPL $text}
	<br />
	<h3>
		Dein Guthaben betr&auml;gt: &euro;
		{#IF $balance == 0.0}
			<span style="color: #DD0000;">{#NUMBER $balance 2}</span>
		{#ELSE}
			<span style="color: #008800;">{#NUMBER $balance 2}</span>
		{#END}
	</h3>
	<br />
	<u>Deine abgeschlossenen Bestellungen</u>
	<br /><br />
	{#TABLE $complete}
		{#COL Datum $ordertime}{#DATE $ordertime}
		{#COL Artikel $items align=left}
			<table>
				{#FOREACH $items}
					<tr>
						<td>{%amount} x</td>
						<td>{%name}</td>
						<td>&aacute; &euro; {#NUMBER $price 2}</td>
						<td>=&nbsp; &euro; {#NUMBER $price_sum 2}</td>
					</tr>
				{#END}
			</table>
		{#COL Menge $num_items align=center}{%num_items}
		{#COL Gesamtsumme $total align=center}<b>&euro; {#NUMBER $total 2}</b>
	{#END}
	<br />
	<u>Deine offenen Bestellungen</u>
	<br /><br />
	{#TABLE $incomplete}
		{#COL Datum $ordertime}{#DATE $ordertime}
		{#COL Artikel $items align=left}
			<table>
				{#FOREACH $items}
					<tr>
						<td>{%amount} x</td>
						<td>{%name}</td>
						<td>&aacute; &euro; {#NUMBER $price 2}</td>
						<td>=&nbsp;&euro; {#NUMBER $price_sum 2}</td>
					</tr>
				{#END}
			</table>
		{#COL Menge $num_items align=center}{%num_items}
		{#COL Status $status align=center}{#TABLEDISPLAY shop_status_names $status}
		{#COL Gesamtsumme $total align=center}<b>&euro; {#NUMBER $total 2}</b>
		{#COL Bezahlt $paid align=center}{#IF $paid}<b><span style="color: #00AA00">Ja</span></b>{#ELSE}<b><span style="color: #FF0000">Nein</span></b>{#END}
	{#END}
{#END}

{#SUB adminOverview}
	{#TPL $text}
	<br />
	<div style="width: 40%; text-align: left">
		<div style="float: left">
			<div id="editlinks">
				<h3 align="left"><u>Angebot</u></h3>
				<ul style="padding-left: 20px">
					<li><a href="shop.php?frame=adminitemgroups">Artikelgruppen bearbeiten</a></li>
					<li><a href="shop.php?frame=adminitems">Artikel bearbeiten</a></li>
				</ul>
			</div>
			<br />
			<div id="managelinks">
				<h3 align="left"><u>Bestellungen</u></h3>
				<ul style="padding-left: 20px">
					<li><a href="shop.php?frame=adminusers">Nach User anzeigen</a></li>
					<li><a href="shop.php?frame=adminTransactionsByItem">Nach Artikel anzeigen</a></li>
				</ul>
			</div>
		</div>
		
		<div style="float: right">
			<div id="editlinks">
				<h3 align="left"><u>Kasse</u></h3>
				<ul style="padding-left: 20px">
					<li><a href="shop.php?frame=admincashregisterbuttons">Buttons bearbeiten</a></li>
					<li><a href="shop.php?frame=cashregister">Kasse anzeigen</a></li>
				</ul>
			</div>
			<br />
			<div id="editlinks">
				<h3 align="left"><u>Sonstige Tools</u></h3>
				<ul style="padding-left: 20px">
					<li><a href="shop.php?frame=adminproductionqueue">Produktionswarteschlange verwalten</a></li>
					<li><a href="shop.php?frame=adminbalanceaccounts">Guthabenkonten verwalten</a></li>
					<li><a href="config.php?category=shop">Einstellungen des Moduls</a></li>
				</ul>
			</div>
		</div>
		<hr style="border: 0px; clear: both" />
	</div>	
{#END}

{#SUB adminItemGroups}
	{#TPL $text}
	<br />
	{#TABLE $groups}
		<a href="shop.php?frame=adminedititemgroup&amp;id=new">Neue Gruppe hinzuf&uuml;gen</a>
		{#COL Name $name}<a href="shop.php?frame=adminedititemgroup&amp;id={$id}">{%name}</a>
		{#COL Beschreibung $description}{%description}
		{#COL Aktiviert $enabled align="center"}{#WHEN $enabled "Ja" "Nein"}
		{#COL Bild $imageid}
			{#IF $imageid}
				<img alt="{%name}" src="image.php?frame=thumbnail&amp;name={$imageid}" />
			{#ELSE}
				<div style="text-align: center">-</div>
			{#END}
		{#OPTION "L&ouml;schen" deleteItemGroup}
	{#END}
	<br />
	<a href="shop.php?frame=adminoverview">Zur&uuml;ck</a>
{#END}

{#SUB adminEditItemGroup}
	{#FORM adminEditItemGroup}
		{#INPUT id hidden $id}
		<table>
			<tr>
				<td>Name:</td>
				<td>{#INPUT name string $name}</td>
			</tr>
			
			<tr>
				<td>Beschreibung:</td>
				<td>{#INPUT description text $description}</td>
			</tr>			
			
			<tr>
				<td>Aktiv:</td>
				<td>{#INPUT enabled checkbox $enabled}</td>
			</tr>			
			
			<tr>
				<td>Bild:</td>
				<td>{#INPUT imageid content $imageid param=images}</td>
			</tr>

			<tr>
				<td colspan="2" align="center">&nbsp;</td>
			</tr>
			
			<tr>
				<td colspan="2" align="center">{#SUBMIT "Speichern"}</td>
			</tr>
			
			<tr>
				<td colspan="2" align="center">&nbsp;</td>
			</tr>
			
			<tr>
				<td colspan="2" align="center">{#BACKLINK Zur&uuml;ck}</td>
			</tr>
		</table>
	{#END}
{#END}

{#SUB adminItems}
	{#TPL $text}	
	<br />
	{#TABLE $items}
		<a href="shop.php?frame=adminedititem&amp;id=new">Neuen Artikel hinzuf&uuml;gen</a>
		{#COL Name $name}<a href="shop.php?frame=adminedititem&amp;id={%id}">{%name}</a>
		{#COL Gruppe $groupid}{%groupname}
		{#COL Beschreibung $description}{%description}
		{#COL "Preis pro Einheit" $price align=center}&euro;&nbsp;{#NUMBER $price 2}
		{#COL Aktiv $enabled align=center}{#IF $enabled}Ja{#ELSE}Nein{#END}
		{#COL Lager&uuml;berwachung $stockenabled align=center}{#IF $stockenabled}An{#ELSE}Aus{#END}
		{#COL Lagerkapazit&auml;t $stockamount align=center}{#IF $stockenabled}{%stockamount} Einheiten{#ELSE}<center><span style="font-size: 16px">&infin;</span></span></center>{#END}
		{#COl "Muss warten" $queued align=center}{#IF $queued}Ja{#ELSE}Nein{#END}
		{#OPTION "L&ouml;schen" deleteItems}
	{#END}
	<br />
	<a href="shop.php?frame=adminoverview">Zur&uuml;ck</a>
{#END}

{#SUB adminEditItem}
	{#FORM adminEditItem action="shop.php?frame=adminitems"}
		{#INPUT id hidden $id}
		<table>
			<tr>
				<td>Name:</td>
				<td>{#INPUT name string $name}</td>
			</tr>
			
			<tr>
				<td>Artikelgruppe:</td>
				<td>{#INPUT groupid dropdown $groupid param=$groups}</td>
			</tr>
			
			<tr>
				<td>Beschreibung:</td>
				<td>{#INPUT description text $description}</td>
			</tr>
			
			<tr>
				<td>Preis pro Einheit:</td>
				<td>{#INPUT price decimal $price}&euro;</td>
			</tr>
			
			<tr>
				<td>Aktiv:</td>
				<td>{#INPUT enabled checkbox $enabled}</td>
			</tr>
			
			<tr>
				<td>Lager&uuml;berwachung:</td>
				<td>{#INPUT stockenabled checkbox $stockenabled}</td>
			</tr>
			
			<tr>
				<td>Lagerkapazit&auml;t:</td>
				<td>{#INPUT stockamount integer $stockamount}</td>
			</tr>
			
			<tr>
				<td>Produktionswarteschlange:</td>
				<td>{#INPUT queued checkbox $queued}</td>
			</tr>
			
			<tr>
				<td>Bild:</td>
				<td>{#INPUT imageid content $imageid param=images}</td>
			</tr>
			
			<tr>
				<td colspan="2" align="center">&nbsp;</td>
			</tr>
			
			<tr>
				<td colspan="2" align="center">{#SUBMIT "Speichern"}</td>
			</tr>
			
			<tr>
				<td colspan="2" align="center">&nbsp;</td>
			</tr>
			
			<tr>
				<td colspan="2" align="center">
					{#BACKLINK Zur&uuml;ck}
				</td>
			</tr>
		</table>
	{#END}
{#END}

{#SUB adminUsers}
	{#IF !isset($username)}
		{#TPL $text}
		<br />
		{#TABLE $users}
			{#COL Username $name}<a href="shop.php?frame=adminusers&amp;userid={%userid}">{%name}</a>
			{#COL Guthaben $balance}&euro;&nbsp;<a href="shop.php?frame=adminbalanceaccount&amp;userid={%userid}">{%balance}</a>
			{#COL "&Sigma; Bestellungen" $num_orders align=center}{%num_orders}
		{#END}
		<br />
		<a href="shop.php?frame=adminoverview">Zur&uuml;ck</a>
	{#ELSE}
		{#TPL $text}
		<h3><u>Bestellungen von "{$username}"</u></h3>
		{#TABLE $transactions}
			{#COL Datum $ordertime}{#DATE $ordertime}
			{#COL Artikel $items align=left}
				<a href="shop.php?frame=admintransaction&id={$id}">
					<table>
						{#FOREACH $items}
							<tr>
								<td style="text-decoration: underline">{%amount} x</td>
								<td style="text-decoration: underline">{%name}</td>
								<td style="text-decoration: underline">&aacute; &euro; {#NUMBER $price 2}</td>
								<td style="text-decoration: underline">=&nbsp;&euro; {#NUMBER $price_sum 2}</td>
							</tr>
						{#END}
					</table>
				</a>
			{#COL Menge $num_items align=center}{%num_items}
			{#COL Status $status align=center}{#TABLEDISPLAY shop_status_names $status}
			{#COL Gesamtsumme $total align=center}<b>&euro; {#NUMBER $total 2}</b>
			{#COL Bezahlt $paid align=center}{#IF $paid}<b><span style="color: #00AA00">Ja</span></b>{#ELSE}<b><span style="color: #FF0000">Nein</span></b>{#END}
			{#OPTION "Status setzen auf" adminChangeTransactionState}&nbsp;{#INPUT state dropdown param=$changestates}
			{#OPTION "Bestellung l&ouml;schen - <span style=\"color: #00AA00;\">Betrag gutschreiben</span>" adminDeleteTransactionWithRefund}
			{#OPTION "Bestellung l&ouml;schen - <span style=\"color: #AA0000;\">Betrag nicht gutschreiben</span>" adminDeleteTransaction}
		{#END}
		<br />
		<a href="shop.php?frame=adminusers">Zur&uuml;ck</a>
	{#END}
{#END}

{#SUB adminTransaction}
	<h3>Transaktionsdaten</h3>
	<div style="width: 20%; margin: auto;">
		<div id="rightcol" style="float: right; text-align: left; padding-left: 5px;">
			<div>{$id}</div>
			<div>{#DATE $ordertime}</div>
			<div>{#TABLEDISPLAY shop_status_names $status}</div>
		</div>
		<div id="leftcol" style="float: right; font-weight: bold; text-align: right;">
			<div>ID:</div>
			<div>Datum:</div>
			<div>Status:</div>
		</div>

		<hr style="border: 0px; clear: both" />
	</div>
	<h3>Artikel</h3>
	{#TABLE $items}
		{#COL Artikelnummer $itemid align=center}{%itemid}
		{#COL Name $name}{%name}
		{#COL St&uuml;ckpreis $price align=center}&euro;&nbsp;{#NUMBER $price 2}
	{#END}
	<br />
	<a href="shop.php?frame=adminusers&userid={$userid}">Zur&uuml;ck</a>
{#END}

{#SUB adminTransactionsByItem}
	{#TPL $text}
	<br />
	{#TABLE $items}
		{#COL Artikelnummer $itemid align=center}{%itemid}
		{#COL Name $name}{%name}
		{#COL St&uuml;ckpreis $price}&euro;&nbsp;{#NUMBER $price 2}
		{#COL Gruppe $groupname}{%groupname}
		{#COL "&sum; Bestellungen" $numorders align=center}{%numorders}
		{#COL "%" $percentage align=center}{#NUMBER $percentage 2}
	{#END}
	<br />
	<h3>Einnahmen aller Bestellungen: {#NUMBER $sales 2} &euro;</h3>
	<br />
	<a href="shop.php?frame=adminoverview">Zur&uuml;ck</a>
{#END}

{#SUB adminBalanceAccounts}
	{#TPL $text}
	<br />
	{#TABLE $accounts}
		<a href="shop.php?frame=adminbalanceaccount">Neuen User hinzuf&uuml;gen</a>
		{#COL Benutzername $username}<a href="shop.php?frame=adminbalanceaccount&userid={$userid}">{%username}</a>
		{#COL Guthaben $balance}{#NUMBER $balance 2}&nbsp;&euro;
	{#END}
	<br />
	<a href="shop.php?frame=adminoverview">Zur&uuml;ck</a>
{#END}

{#SUB adminBalanceAccount}
	{#IF empty($userid)}
		<h3>Schritt 1: User suchen</h3>
		{#FORM method=get action="search.php?frame=result"}
		{#INPUT nextframelink hidden $nextframelink}
		{#INPUT usedmodules[user] hidden "1"}
		{#INPUT titleonly hidden "1"}
		{#INPUT mode hidden "default"}
		<table border="0" cellspacing="1" cellpadding="1" class="table">
			<tr>
			  <th class="tdedit" style="text-align:right;">Suchtext:</th>
			  <td class="tdcont">{#INPUT searchstring string $searchstring}</td>
			</tr>
		</table>
		<br />
		{#SUBMIT Suchen}
		{#END}
	{#ELSE}
		<h3>Der User hat {#NUMBER $balance 2} &euro; auf seinem Konto!</h3>
		<br />
		<div>
			<div style="padding: 20px; border: 2px solid #00DD00; width: 50%">
				{#FORM changeUserBalance action="shop.php?frame=adminbalanceaccount&amp;userid=$userid"}
					{#INPUT userid hidden $userid}
					<table border="0" cellspacing="1" cellpadding="1" class="table">
						<tr>
						  <th class="tdedit" style="text-align:right;">Neues Guthaben:</th>
						  <td class="tdcont">{#INPUT newbalance decimal $balance}</td>
						</tr>
						<tr>
							<td colspan="2" align="center">{#SUBMIT Speichern}</td>
						</tr>
					</table>
				{#END}
			</div>
			<br />
			<div style="padding: 20px; border: 2px solid #FF0000; width: 50%">
				{#ACTION "Guthaben aufl&ouml;sen" clearUserBalance uri="shop.php?frame=adminbalanceaccounts" params="array(userid=$userid)" buttonattrs="array(style=\"background-color: #FF0000\")"}
			</div>
		</div>
	{#END}
	<br />
	<a href="shop.php?frame=adminbalanceaccounts">Zur&uuml;ck</a>
{#END}

{#SUB adminProductionQueue}
	<h4 style="border-bottom: 2px solid black; text-align: left; font-size: 18px; padding-top: 0px">
		<div style="float: left; padding-bottom: 2px">
			{#FORM setTimer method="get"}
				{#INPUT frame hidden "adminproductionqueue"}
				Aktualisiere alle {#INPUT refreshevery dropdown $refreshevery param=$times} Sekunden
				{#SUBMIT Speichern}
			{#END}
		</div>
		<div style="float: left; width: 5px">&nbsp;</div>
		<div style="float: left; padding-bottom: 2px">
			{#FORM resetTimer method="get"}
				&nbsp;
				{#INPUT frame hidden "adminproductionqueue"}
				{#SUBMIT Stop}
			{#END}			
		</div>
		<hr style="border: 0px; clear: both; height: 0px;" />
	</h4>
	
	{#FOREACH $waiting}
		{#HTMLACTION setTransactionReady params="array(id=$id)" confirmation="Das Produkt ist fertig und kann vom User abgeholt werden - OK?" buttonattrs="array(style=\"width: 70%\")"}
		<div>
			<h4 style="border-bottom: 2px solid black; text-align: left; font-size: 18px; padding-top: 0px">
				Bestellung #{$id} vom {#DATE $ordertime}
			</h4>
			<div style="text-align: left;">
				<table>
				{#FOREACH $items}
					<tr>
						<td style="font-size: 18px">{$amount} x</td>
						<td style="font-size: 18px">{$name}</td>
						<td style="width: 9px">&nbsp;</td>
						<td style="font-size: 18px;">&aacute;</td>
						<td style="width: 9px">&nbsp;</td>
						<td style="font-size: 18px">&euro;&nbsp;{$price}</td>
					</tr>
				{#END}
				</table>
			</div>
		</div>
		{#END}
		<br />
	{#ELSE}
		<h3>Keine Bestellungen vorhanden!</h3>
	{#END}
{#END}