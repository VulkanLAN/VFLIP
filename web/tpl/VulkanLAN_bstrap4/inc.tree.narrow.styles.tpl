
{#SUB treestyles}
<style type="text/css">
.treehead {background-color:#E3E3E3;}
.open {border:1px solid black;background-color:#FFFFAD/*#ddbb40*/;padding:1px;margin:0px;border-spacing:0px;}
.notset {border:1px solid black;background-color:#E3E3E3;padding:1px;margin:0px;border-spacing:0px;}
.winner {border:1px solid black;background-color:#ADFFAD/*#88dd88*/;padding:1px;border-spacing:0px;}
.loser {border:1px solid black;background-color:#FFADAD/*#dd9999*/;padding:1px;border-spacing:0px;}
{! height von treecell wird in {$file} (H&ouml;he der Zelle) verwendet um die Linien korrekt darzustellen! }
.treecell {height:23px;margin:0px;border-spacing:0px;}
</style>
{#END}

{#SUB _atree_winnerlineborderstyle}
2px solid green
{#END}

{#SUB _atree_loserlineborderstyle}
2px dotted gray
{#END}
