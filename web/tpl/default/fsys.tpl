{#SUB default}
  <style type="text/css">
  <!--
   .requested   { background-color: yellow; }
   .accepted    { background-color: green; }
   .denied      { background-color: red; }
   .requestable { background-color: silver; }
  //-->
  </style>
  <center>
    Mit dem Feature-System kannst du bestimmte optionale Features anfragen.<br />
    Ein Feature-Account wird dabei automatisch erstellt.<br />
    Der Anfragestatus des Accounts kann '<font color="#CBCB00">Angefragt</font>', 
    '<font color="green">Akzeptiert</font>' 
    oder '<font color="red">Abgelehnt</font>' sein.<br />
    {#IF $admin}Mit einem Klick auf den Featurenamen kann diese bearbeitet werden.{#END}<br />
  </center>
  <br />
  {#TABLE $accounts}
    {#COL Name %name}{#IF $admin}<a href="fsys.php?frame=editfeature&id={%id}">{%name}</a>{#END}
    {#COL Beschreibung %description}
    {#COL Verf&uuml;gbar %free}
    {#COL Status "<b><center>&nbsp;$status.name&nbsp;</center></b>" class=$status.class}
    {#OPTION "Anfragen" requestfeature}
    {#OPTION "Anfrage zur&uuml;cknehmen" unrequestfeature}
  {#END}
  <br />
  {#IF $admin}{#VAR sub=adminsection}{#END}
{#END}

{#SUB filterlinks}
  <center>
  <div style="text-decoration: none; font-size: 12px; border: 1px solid grey; padding: 5px; width: 34%">
    <u><i>Filter:</i></u>&nbsp;&nbsp;
    <a href="fsys.php?frame=accountsadmin{$ftype_f}">Alle</a>&nbsp;
    <a href="fsys.php?frame=accountsadmin{$ftype_f}&showonly=requested">Angefragte</a>&nbsp;
    <a href="fsys.php?frame=accountsadmin{$ftype_f}&showonly=accepted">Akzeptierte</a>&nbsp;
    <a href="fsys.php?frame=accountsadmin{$ftype_f}&showonly=denied">Abgelehnte</a>
    <br />
    <div style="padding-top: 4px;">
    {#FORM method=get}
      {#INPUT frame hidden accountsadmin}
      {#INPUT ftype dropdown param=$filtertypes}
      {#IF !empty($showonly)}
        {#INPUT showonly hidden $showonly}
      {#END}
      {#SUBMIT Anzeigen shortcut=""}
    {#END}
    </div>
  </div>
  </center>
{#END}

{#SUB accountsadmin}
  <style type="text/css">
  <!--
   .requested   { background-color: yellow; }
   .accepted    { background-color: green; }
   .denied      { background-color: red; }
  //-->
  </style>
  <center>
    Hier sind die Feature-Anfragen der User aufgelistet.<br />
    Du die Feature-Anfragen der User annehmen oder ablehnen.<br />
    Es besteht auch die M&ouml;glichkeit einen Account zu l&ouml;schen.<br />
    <br />
    {#VAR sub=filterlinks}
  </center>
  <br />
  {#TABLE $accounts maxrows=30}
    {#COL User %owner}
    {#COL Accounttyp %name}
    {#COL Status "<b><center>&nbsp;$status.name&nbsp;</center></b>" class=$status.class}
    {#OPTION "Anfrage akzeptieren" acceptrequest}
    {#OPTION "Anfrage ablehnen" denyrequest}
    {#OPTION "Anfrage l&ouml;schen" deleterequest}
  {#END}
  <br />
  <a href="fsys.php">zur&uuml;ck zur &Uuml;bersicht</a>
{#END}

{#SUB editfeature}
  <center>
    {#FORM editfeature}
      <table>
        {#INPUT id hidden $id}
        <tr>
          <td>Name:</td>
          <td>{#INPUT name string $name}</td>
        </tr>
        <tr>
          <td>Beschreibung:</td>
          <td>{#INPUT description string $description}</td>
        </tr>
        <tr>
          <td>H&ouml;chstanzahl:</td>
          <td>{#INPUT maxcount decimal $maxcount}</td>
        </tr>
        <tr>
          <td>Message bei Freischaltung:</td>
          <td>{#INPUT msg_accepted dropdown $msg_accepted param=$msgs}</td>
        </tr>
        <tr>
          <td>Messge bei Ablehnung:</td>
          <td>{#INPUT msg_denied dropdown $msg_denied param=$msgs}</td>
        </tr>
        <tr>
          <td>Messge bei Anfrage:</td>
          <td>{#INPUT msg_requested dropdown $msg_requested param=$msgs}</td>
        </tr>
        <tr align="center">
          <td colspan="2"><br />{#SUBMIT Speichern}</td>
        </tr>
      </table>
    {#END}
    <br />
    {#ACTION l&ouml;schen deletetype params=array("id=$id") shortcut="d" confirmation="Soll der Featuretyp wirklich gel&ouml;scht werden?"}
    <br />
    <br />
    {#FORM}{#BACKLINK Zur&uuml;ck}{#END}
  </center>
{#END}

{#SUB adminsection}
  <div align="right">
    <div class="textfooter" style="text-align:left;border:1px solid gray;padding:4px;width:14%;">
      Admin:<br />
      <a class="textfooter" href="fsys.php?frame=accountsadmin">Accountsadmin</a><br />
      <a class="textfooter" href="fsys.php?frame=editfeature&id=new">Feature hinzuf&uuml;gen</a>
    </div>
  </div>
{#END}
