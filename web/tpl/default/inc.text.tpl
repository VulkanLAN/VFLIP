{#IF $found}
  <table cellspacing="0" cellpadding="0" border="0"><tr><td>
    {$text}
  </td></tr></table>
  <br />
  {#IF $editable}
    <div class="textfooter">last <a href="content.php?frame=edittext&amp;id={$id}">edited</a> by {%author} on {$date}</div>
  {#END}
{#ELSE}
  <p>
    {§Fehler}: Der Text mit dem Namen "{%name}" wurde nicht gefunden.
    {#IF $editable} Du kannst ihn <b><a href="content.php?frame=edittext&amp;name={%name}">hier erstellen.</a></b>{#END}
  </p>
{#END}