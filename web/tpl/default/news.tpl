{! $Id: news.tpl 1465 2007-09-13 12:18:40Z loom $ }

{#SUB default}
{#IF !empty($admin)}
<a href="news.php?frame=edit">{%admin}</a>
{#END}
{#IF $usecat}
  {#FORM method=get}
  <div align="right">
    {§Kategorie}:
    {#INPUT category tabledropdown $category param=news_category}
    {#SUBMIT Anzeigen 0}
  </div>
  {#END}
{#END}
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  {#FOREACH $news}
  <tr>
    <td><b>{#DATE $date}{#IF $usecat} - {#TABLEDISPLAY news_category $category}{#END}</b></td><td align="right">{%author}</td>
  </tr>
  <tr>
    <td colspan="2"><hr /></td>
  </tr>
  <tr>
    <td align="justify" colspan="2" style="padding-bottom:40px;"><strong>{#IF $intern}{§Intern}: {#END}{%title}</strong><br /><br />{$text}<br />
      {#IF ($id > 0)}<br /><span class="textfooter"><a href="news.php?frame=comments&amp;id={$id}">{§Kommentare} ({$commentcount})</a>
      {#IF ($edit=="true" || $spellchecker=="true")}<br />{§Admin}: <a href="news.php?frame=edit&amp;id={$id}">{§bearbeiten}</a> - <a href="news.php?frame=del&amp;id={$id}">{§l&ouml;schen}</a>{#END}</span>{#END}</td>
  </tr>
  {#END}
</table>
{#IF isset($less)}<a href="news.php?more={$less}">{§vorherige}</a>{#END}
{#IF isset($less) && !empty($more)} - {#END}
{#IF !empty($more)}<a href="news.php?more={$more}">{§weitere}</a>{#END}
<br />
<a href="news.php?frame=archiv">{§Archiv}</a>
{#END}

{#SUB del}
{§ text="Soll der Newseintrag \"?1?\" wirklich gel&ouml;scht werden?" param=%title}<br />
<br />
{#FORM del action="news.php"}
  {#INPUT id hidden $id}
  {#SUBMIT ja}<br />
  <br />
  {#BACKLINK NEIN/zur&uuml;ck}
{#END}
{#END}

{#SUB edit}
{#FORM news}
{#BACKLINK zur&uuml;ck}<br />
<br />
<table cellspacing="0" cellpadding="2" border="0">
{#INPUT view_right hidden $view_right}
{#IF $newsposter=="true"}<tr><td>{§Public}:</td><td>{#INPUT public checkbox $public}</td></tr>
<tr><td>{§Datum}:</td><td>{#INPUT date date $date} {§leer} = {§jetzt}</td></tr>{#END}
<tr><td>{§Immer oben}:</td><td>{#INPUT on_top checkbox $on_top}</td></tr>
<tr><td>{§Autor}:</td><td>{#INPUT author string $author} {§leer} = {§Benutzer}</td></tr>
<tr><td>{§Kategorie}:</td><td>{#INPUT category tabledropdown $category param=news_category}</td></tr>
<tr><td>{§Titel}:</td><td>{#INPUT title string $title}</td></tr>
</table>
{#INPUT text document $text}<br />
{#INPUT id hidden $id}
{#SUBMIT Eintragen}<br />
{#END}
{#END}

{#SUB comments}
<a href="news.php">{§zur&uuml;ck zu den Neuigkeiten}</a><br />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><b>{#DATE $date}{#IF $usecat} - {#TABLEDISPLAY news_category $category}{#END}</b></td><td align="right">{%author}</td>
  </tr>
  <tr>
    <td colspan="2"><hr></td>
  </tr>
  <tr>
    <td align="justify" colspan="2"><strong>{%title}</strong><br /><br />{$text}</td>
  </tr>
</table>
<br />
<h2>{§Kommentare}:</h2>
{#IF (count($comments)>4 && $cancomment=="true")}<a href="news.php?frame=addcomment&amp;id={$id}">{§Kommentar abgeben}</a><br />{#END}
{#TABLE $comments class="wide_table"}
{#IF $cancomment=="true"}<a href="news.php?frame=addcomment&amp;id={$id}">{§Kommentar abgeben}</a>{#END}
  {#COL Autor %username widht="130" valign="top"}<strong><a href="user.php?frame=viewsubject&amp;id={$user_id}">{%username}</a></strong><br />{#DATE $timestamp}
  {#COL Kommentar}{$text}
  {#OPTION l&ouml;schen delcomment right="$editright"}
{#END}
<br /><a href="news.php">{§zur&uuml;ck zu den Neuigkeiten}</a>
{#END}

{#SUB addcomment}
<a href="news.php?frame=comments&amp;id={$id}">{§zur&uuml;ck}</a><br />
<br />
{#FORM comment action="news.php?frame=comments&amp;id=$id"}
{§Kommentar}:<br />
{#INPUT text text $text}<br />
{#INPUT id hidden $id}
{#SUBMIT hinzuf&uuml;gen}
{#END}
{#END}

{#SUB archiv}
{#TPL $text}
{#IF $usecat}
  <div align="right">
  {#FORM method=get}
    {#INPUT frame hidden archiv}
    Kategorie:
    {#INPUT category tabledropdown $category param=news_category}
    {#SUBMIT Anzeigen 0}
  {#END}
</div>
{#END}
{#TABLE $news maxrows=25}
  {#COL Datum $date}{#DATE $date}
  {#COL Titel %title}<a href="news.php?newsid={$id}">{#IF $on_top}<span class="important">{#END}{#IF $intern}{§Intern}: {#END}{%title}{#IF $on_top}</span>{#END}</a>
  {#COL Autor %author}
  {#COL Kategorie $category}{#TABLEDISPLAY news_category $category}
{#END}
{#END}

{#SUB smallnews}
<style type="text/css">
.news_small_li
{
  list-style-position:inside;
  list-style-type:circle;
  position:inherit;
  margin: 2px 0px 2px 0px;
  float:none;
  text-align: left;
}
.news_small_ul {
  margin: 0px;
}

</style>
<ul class="news_small_ul">
{#FOREACH $items}
  <li class="news_small_li"><a href="news.php?newsid={$id}" title="{#DATE $date}: {%author}">{#IF $intern}i: {#END}{%title}</a></li>
{#END}
</ul>
{#END}
