{#SUB default}
<div class="wide_topdiv">
  <a href="ticket.php?frame=stats">Statistik</a><br />
  <br />
</div>
  <table class="wide_table" cellspacing="1" cellpadding="1">
{#FORM method=get keepparams=1}
    <tr>
    <td colspan="5" class="tdedit"><b>Filter</b></td></tr>
    <tr>
      <td colspan="2" class="tdcont">Suche: {#INPUT f_search string $f_search}</td>
    </tr>
    <tr>
      <td class="tdcont">Zugewiesen: {#INPUT f_resp dropdown $f_resp param=$owners}</td>
      <td class="tdcont">Kategorie: {#INPUT f_category tabledropdown $f_category param=ticket_category}</td>
    </tr>
    <tr>
      <td class="tdcont">{#INPUT f_hidedone checkbox $f_hidedone} abgeschlossene und unwichtige einblenden.</td>
      <td class="tdaction" align="center">{#SUBMIT Anzeigen}</td>
    </tr>
{#END}
  </table>
<div class="wide_div">&nbsp;</div>
{#TABLE $tickets frames=$frames class="wide_table" maxrows=100}
  <a href="ticket.php?frame=editticket">add</a>
  {#COL Name $title}<a href="ticket.php?frame=ticket&amp;id={$id}">{%title}</a>
  {#COL Ge&auml;ndert $create_time}<nobr>{#DATE $create_time}</nobr>
  {#COL Vers %version}
  {#COL Zugewiesen %resp}
  {#COL Kategorie $category}{#TABLEDISPLAY ticket_category $category}
  {#COL Priorit&auml;t %priority}
  {#COL Status $status}{#TABLEDISPLAY ticket_status $status}
  {#COL Deadline $deadline_time}{#DATE $deadline_time}
{#END}
{#END}

{#SUB editticket}
{#FORM ticket}
<table width="100%">
  <tr><td align="right">Version:</td><td>{$version}</td></tr>
  <tr><td align="right">Titel:</td><td>{#INPUT title string $title allowempty=0}</td></tr>
  <tr><td align="right">Kategorie:</td><td>{#INPUT category tabledropdown $category param=ticket_category}</td></tr>
  <tr>
  {#IF !empty($id)}
  <td align="right">Status:</td><td>{#INPUT status tabledropdown $status param=ticket_status}</td>  
  {#ELSE}
  <td align="right">Status:</td><td>Er&ouml;ffnet</td>
  {#END}
  </tr>  
  <tr><td align="right">Zugewiesen an:</td><td>{#INPUT resp_user dropdown $resp_user param=$owners}</td></tr>
  <tr><td align="right">Priorit&auml;t:</td><td>{#INPUT priority tabledropdown $priority param=ticket_priority}</td></tr>
  <tr><td align="right">Zu erledigen bis:</td><td>{#INPUT deadline_time date $deadline_time}</td></tr>
  {#IF empty($id)}
  <tr><td align="right">Beschreibung:</td><td>{#INPUT event_desc text}</td></tr>
  {#END}  
  <tr><td colspan="2" align="center"><br /><br />{#SUBMIT Speichern}</td></tr>
  <tr><td colspan="2" align="center"><br /><br />{#BACKLINK Zur&uuml;ck}</td></tr>
</table>
{#INPUT id hidden $id}
{#INPUT version hidden $version}
{#INPUT opening_user hidden $opening_user}
{#INPUT opening_time hidden $opening_time}
{#INPUT ticket_id hidden $ticket_id}
{#END}
{#END}

{#SUB ticket}
<h3>
  <a href="ticket.php">&Uuml;bersicht</a>
  &middot;
  {#IF $allow_edit}
    <a href="ticket.php?frame=editticket&amp;id={$id}">bearbeiten</a>
    &middot;
  {#END}
  <a href="ticket.php?frame=verlauf&amp;id={$id}">verlauf</a>
</h3>
<table width="100%">
  <tr>
    <td align="right"><b>Verantwortlicher:</b></td><td class="important">{%resp}</td>
    <td align="right"><b>Version:</b></td><td class="important"><b>{%version}</b></td>
  </tr><tr>
    <td align="right"><b>Bearbeitet:</b></td><td>{%create} am <span class="important">{#DATE $create_time}</span></td>
    <td align="right"><b>Status:</b></td><td>{#TABLEDISPLAY ticket_status $status}</td>
  </tr><tr>
    <td align="right"><b>Erstellt:</b></td><td>{%opening} am {#DATE $opening_time}</td>
  <td align="right"><b>Priorit&auml;t:</b></td><td>{#TABLEDISPLAY ticket_priority $priority}</td> 
  </tr><tr>
    <td align="right"><b>Zu erledigen bis:</b></td><td>{#DATE $deadline_time}</td>
    <td align="right"><b>Kategorie:</b></td><td>{#TABLEDISPLAY ticket_category $category}</td>
  </tr>
</table>
<br />
<table class="table" cellspacing="1">
  <tr><td colspan="2" class="tdedit">Aufgabenbeschreibung und Entwicklung:</td></tr>
  {#FOREACH $events}
  <tr>
    <td class="tdedit" valign="top"><b>{%user}</b><br /><nobr>{#DATE $create_time}</nobr></td>
  <td class="tdcont">{$description}</td>
  </tr>  
  {#END}
</table>
{#IF $allow_comment}
  <br />
  <br />
  <table>
    <tr>
    {#FORM event action="ticket.php?frame=ticket&amp;id=$id"}
      <td>
      Entwicklungsstand hinzuf&uuml;gen:<br />
      {#INPUT description text}
    </td>
    {#INPUT ticket_id hidden $ticket_id}
    {#INPUT id hidden $id}
    {#INPUT title hidden %title}
    <tr><td align="center">{#SUBMIT Hinzuf&uuml;gen}</td></tr>
    <tr><td align="center"><br /><br />{#BACKLINK Zur&uuml;ck}</td></tr>
    {#END}
    </tr>
  </table> 
{#ELSE}
  <i>nicht gen&uuml;gend Rechte, um dieses Ticket zu kommentieren.</i>
{#END}
{#END}

{#SUB verlauf}
<h3>
  <a href="ticket.php">&Uuml;bersicht</a>
  &middot;
  {#IF $allow_edit}
    <a href="ticket.php?frame=editticket&amp;id={$id}">bearbeiten</a>
    &middot;
  {#END}
  <a href="ticket.php?frame=ticket&amp;id={$id}">detail</a>
</h3>
{#TABLE $tickets frames=$frames width="90%"}
  <a href="ticket.php?frame=editticket&amp;id={$id}">edit</a>
  {#COL "Name" %title}
  {#COL "Ge&auml;ndert"}<nobr>{#DATE $create_time}</nobr>
  {#COL "Vers" %version}
  {#COL "Zugewiesen" %resp}
  {#COL Kategorie $category}{#TABLEDISPLAY ticket_category $category}
  {#COL "Priorit&auml;t" $priority}
  {#COL Status $status}{#TABLEDISPLAY ticket_status $status}
  {#COL "Deadline"}{#DATE $deadline_time}
{#END}
{#END}

{#SUB stats}
<table cellpadding="6" cellspacing="0" border="0" width="100%">
<tr><td align="center" valign="top">
<h2>Gesamt</h2>
{#TABLE $allstats}
  {#COL "Zeitraum" $caption}
  {#COL "Realisiert" $done align=right}
  {#COL "Bearbeitet" $edit align=right}
  {#COL "Er&ouml;ffnet" $open align=right}
  {#COL "Kommentiert" $comment align=right}
{#END}
</td><td align="center" valign="top">
<h2>Top Leute</h2>
{#TABLE $userstats}
  {#COL "Zeitraum" $caption}
  {#COL "Realisiert" $topdone align=right}
  {#COL "Bearbeitet" $topedit align=right}
  {#COL "Er&ouml;ffnet" $topopen align=right}
  {#COL "Kommentiert" $topcomment align=right}
{#END}
</td></tr>
</table>
{#END}

{#SUB showMyRequests}
	{#TABLE $tickets}
		{#COL Betreff $title}<a href="ticket.php?frame=ticket&amp;id={%id}">{%title}</a>
		{#COL Zeit $create_time}{#DATE $create_time}
		{#COL Status $status}{#TABLEDISPLAY ticket_status $current_status}
	{#END}
	<br />
	<a href="ticket.php?frame=requestsupport">{§Neue Anfrage stellen}</a>
{#END}

{#SUB requestSupport}
	{#FORM requestSupport}
		<table>
			<tr>
				<td>{§Betreff:}</td>
				<td>{#INPUT subject string $subject}</td>
			</tr>
			<tr>
				<td>{§Dein Anliegen:}</td>
				<td>{#INPUT message text $message}</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: center">{#SUBMIT Absenden}</td>
			</tr>
		</table>
	{#END}
{#END}
