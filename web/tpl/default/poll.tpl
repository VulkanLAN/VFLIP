{! $Id: poll.tpl 1388 2007-03-27 10:46:25Z loom $ }

{#SUB default}
{#TPL $text}
{#TPL $loggedout}
<br />
{#IF empty($polls)}Es sind noch keine Umfragen vorhanden.<br />{#ELSE}
{#TABLE $polls}
  {#COL "Poll Archiv:" $titel}{#IF $can_vote=="true"}<a href="poll.php?frame=vote&amp;votefor={$id}">{$titel}</a>{#ELSE}{$titel}{#END}
  {#COL Ende $date}
  {#COL &nbsp;}<a href="poll.php?frame=poll&amp;id={$id}">Ergebnis</a>{#IF ($admin=="true" && $votes<1)} <a href="poll.php?frame=add&amp;id={$id}">add</a>{#END}
  {#COL Stimmen $votes align=center}
  {#FRAME bearbeiten edit right=$editright}
{#END}
{#END}
{#IF $admin=="true"}
<a href="poll.php?frame=add">Poll hinzuf&uuml;gen</a><br />
<a href="poll.php?frame=del">Poll entfernen</a>
{#END}
{#END}

{#SUB poll}
<h2 align="center"><a name="#poll{$id}"></a>{$titel}</h2>
{$text}
<br /><br />
<table cellspacing="0" border="0">
{#FOREACH $results}
  <tr>
    <td>{$txt}</td>
    <td><img src="{#STATICFILE images/poll/bar.png}" height="20" width="{$breite}" align="absmiddle" border="0">&nbsp;{$percent}</td>
  </tr>
{#END}
</table>
<br />
Es wurden <b>{$stimmen}</b> Stimmen abgegeben.<br />
{$votetext}{#IF $logged_in=="true"} <a href="poll.php?frame=vote&amp;votefor={$id}">VOTE</a>{#END}<br />
<br />
<a href="poll.php">zur&uuml;ck zum Umfragearchiv</a>
{#END}

{#SUB vote}
<h3 align="center">{$title}</h3>
{$text}
{#FORM action="poll.php?frame=poll&amp;id=$id"}
{#INPUT action hidden vote}
<table cellspacing="0" border="0">
{#FOREACH $results}
  <tr>
    <td>{#INPUT vote radio $vote}</td>
    <td>{$txt}</td>
  </tr>
{#END}
  <tr>
    <td align="center" colspan="2">{#SUBMIT "vote 4 it!" v}</td>
  </tr>
</table>
{#END}
{$votetext}<br />
<br />
{#END}

{#SUB edit}
<a href="poll.php">{§zur&uuml;ck}</a><br />
<br />
{#FORM edit action="poll.php?frame=default"}
{#INPUT id hidden $id}
<table border="0" cellspacing="0" cellpadding="0">
<tr>
  <td>Titel</td>
  <td>{#INPUT titel string $titel}</td>
</tr>
<tr>
  <td>Beschreibung</td>
  <td>{#INPUT text text $text}</td>
</tr>
<tr>
  <td>Leserecht</td>
  <td>{#INPUT view_right rights $view_right} ({§Anonymous darf nicht voten!})</td>
</tr>
</table><br />
{#SUBMIT speichern}<br />
{#END}
<h3>{§Auswahlm&ouml;glichkeiten}</h3>
<ul>
{#FOREACH $votes}
  <li>{#FORM editvote action="$uri"}{#INPUT id hidden $id}{#INPUT text string $text} {#SUBMIT &auml;ndern shortcut=""}{#END}</li>
{#END}
</ul>
{#END}

{#SUB add}
{#FORM add action="poll.php?frame=default"}
{#INPUT id hidden $id}
<table border="0" cellspacing="0" cellpadding="0">
<tr>
  <td>Titel</td>
  <td>{#INPUT titel string $titel}</td>
</tr>
<tr>
  <td>Beschreibung</td>
  <td>{#INPUT text text $text}</td>
</tr>
<tr>
  <td>Leserecht</td>
  <td>{#INPUT view_right rights $view_right} ({§Anonymous darf nicht voten!})</td>
</tr>
{#FOREACH $votes}
<tr>
  <td>{$no}. Wahl</td>
  <td>{#INPUT $voteno string}</td>
</tr>
{#END}
<tr>
  <td colspan="2" align="center">{#submit hinzuf&uuml;gen}</td>
</tr>
</table>
{#END}
{#END}

{#SUB del}
{#FORM del action="poll.php?frame=default"}
<table border="0" cellspacing="0" cellpadding="0">
{#FOREACH $votes}
<tr>
  <td>{#INPUT poll_id radio $poll_id} {$title}</td>
</tr>
{#END}
<tr>
  <td colspan="2" align="center">{#SUBMIT entfernen}</td>
</tr>
</table>
{#END}

{#SUB menuvote}
	{#IF empty($warning) && !empty($results)}
		{!Formular zum voten}
		<div align="center">{$titel}</div>
		{#FORM action="poll.php?frame=poll&amp;id=$id"}
		{#INPUT action hidden vote}
		<table cellspacing="0" border="0" width="98%">
		{#FOREACH $results}
			<tr>
				<td style="white-space:nowrap;"><small>{#INPUT vote radio $vote}{$txt}</small></td>
			</tr>
		{#END}
		<tr>
			<td align="center" width="80%" colspan="0">{#SUBMIT Vote}</td>
		</tr>
		</table>
		{#END}
	{#ELSE}
		{#IF empty($warning) && empty($results)}
			Keine Abstimmungen gefunden!
		{#END}
		{#IF $warning=="results"}
			{!Ergebnistabelle}
			<div align="center">{$titel}</div>
			<table cellspacing="0" border="0" width="98%">
			{#FOREACH $results}
				<tr>
				  <td><small>{$txt}</small></td>
				  <td align="right"><small>{$percent}%</small></td>
				</tr>
			{#END}
			</table>
		{#ELSE}
				<div align="center"><font size="-2">{$warning}</font></div>
		{#END}
		{#END}
	{#END}
{#END}