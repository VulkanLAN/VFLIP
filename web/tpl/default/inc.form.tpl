
{#SUB _input_string}
  <input type="text" class="edit" name="{%name}" value="{%val}" size="32"{$enabled} />
{#END}

{#SUB _input_longstring}
  <input style="width:100%;" type="text" class="edit" name="{%name}" value="{%val}" size="32"{$enabled} />
{#END}

{#SUB _input_yesno}
  <input type="radio" class="checkbox" name="{%name}" value="Y"{$sely}{$enabled} />{§Ja}&nbsp;&nbsp;&nbsp;
  <input type="radio" class="checkbox" name="{%name}" value="N"{$seln}{$enabled} />{§Nein}
{#END}

{#SUB _input_radio}
  <input type="radio" class="checkbox" name="{%name}" value="{%val}"{$sel}{$enabled} />
{#END}

{#SUB _input_checkbox}
  <input type="checkbox" class="checkbox" name="{%name}" value="{%param}"{$sel}{$enabled} />
{#END}

{#SUB _input_integer}
  <input type="text" class="edit" name="{%name}" value="{%val}" size="12"{$enabled} />
{#END}

{#SUB _input_decimal}
  <input type="text" class="edit" name="{%name}" value="{%val}" size="12"{$enabled} />
{#END}

{#SUB _input_ip}
  <input type="text" class="edit" name="{%name}" value="{%val}" size="12"{$enabled} />
{#END}

{#SUB _input_dropdown}
  <select name="{%name}"{$enabled}>
    {#FOREACH $items}<option value="{%key}" {$sel}>{%caption}</option>{#END} 
  </select>
{#END}

{#SUB _input_inputtypes}
  <select name="{%name}_type"{$enabled}>
    {#FOREACH $items}<option value="{%key}" {$sel}>{%caption}</option>{#END} 
  </select>
  {§Parameter}:
  <input type="text" class="edit" name="{%name}_param" value="{%param}" size="32"{$enabled} />
{#END}

{#SUB _input_multisel}
  {#FOREACH $items}
    <input type="checkbox" class="checkbox" name="{%name}_{%key}" value="1"{$sel}{$enabled} />{%caption}
  {#MID}
    <br />
  {#END}
{#END}

{#SUB _input_tabledropdown}
  <select name="{%name}"{$enabled}>
    {#FOREACH $items}<option value="{%key}" {$sel}>{%caption}</option>{#END} 
  </select>{#IF $canedit}&nbsp;&nbsp;&nbsp;<i><a href="table.php?frame=viewtable&amp;name={$table}">({§edit})</a></i>{#END}
{#END}

{#SUB _input_tablemultisel}
  {#FOREACH $items}
    <input type="checkbox" class="checkbox" name="{%name}_{%key}" value="1"{$sel}{$enabled} />{%caption}
  {#MID}
    <br />
  {#END}
  {#IF $canedit}<br/><i><a href="table.php?frame=viewtable&amp;name={$table}">({§edit})</a></i>{#END}
{#END}

{#SUB _input_text}
  <textarea rows="10" cols="48" name="{%name}" class="edit"{$enabled}>{%val}</textarea>
{#END}

{#SUB _input_document}
  <textarea style="width:100%;" cols="90" rows="30" name="{%name}" class="edit"{$enabled}>{%val}</textarea>
{#END}

{#SUB _input_documentwrap}
  <textarea style="width:100%;" cols="90" rows="30" wrap="virtual" name="{%name}" class="edit"{$enabled}>{%val}</textarea>
{#END}

{#SUB _input_phone}
  <input type="text" class="edit" name="{%name}" value="{%val}" size="15"{$enabled} />
{#END}

{#SUB _input_captcha}
  <img src="ext/captcha/captcha_image.php?img={$url}" alt="{§Bild mit Text fehlt!}"/><br/>{§Bildtext}: <input type="text" class="edit" name="{%name}" value="{%val}" size="10"{$enabled} />
{#END}

{#SUB _input_passwordedit}
  <input type="password" class="edit" name="{%name}2" value="" size="24"{$enabled} />
  &nbsp;&nbsp;&nbsp;{§Wiederholung}:
  <input type="password" class="edit" name="{%name}1" value="" size="24"{$enabled} />
{#END}

{#SUB _input_passwordquery}
  <input type="password" class="edit" name="{%name}" value="" size="24"{$enabled} />
{#END}

{#SUB _input_hidden}
  <input type="hidden" name="{%name}" value="{%val}" />
{#END}

{#SUB _input_file}  
  <table cellpadding="0" cellpadding="1" style="display:inline;">
    <tr><td rowspan="2"><input type="file" name="{%name}" class="edit" size="48"{$enabled} /></td>
    <td>&nbsp;&nbsp;{§maximale Dateigr&ouml;&szlig;e}: {#BYTE $maxupload format=mb prec=3}MB</td></tr>
    <tr><td style="color:grey;">&nbsp;&nbsp;({$maxreason})</td></tr>
  </table>
{#END}

{#SUB _input_image}
<table>
  {#IF $allowurl}
  <tr>
    <td><input type="radio" class="checkbox" name="{%name}_sel" value="url"{$enabled} />URL:</td>
    <td colspan="3"><input type="text" class="edit" name="{%name}_url" value="" size="69"{$enabled} /></td>
  </tr>
  {#END}
  <tr>
    <td><input type="radio" class="checkbox" name="{%name}_sel" value="file"{$enabled} />{§Datei}:</td>
    <td colspan="3"><input type="file" name="{%name}_file" class="edit" size="40"{$enabled} /></td>
    <td rowspan="2" style="padding-left:16px;">{#DBIMAGE $image process=createThumbnail(160,64)}</td>
  </tr>
  <tr>
    <td align="left" colspan="2"><input type="radio" class="checkbox" name="{%name}_sel" value="del"{$enabled} />{§altes Bild l&ouml;schen}</td>
    <td  colspan="2" align="right" valign="top">({§maximale Dateigr&ouml;&szlig;e}: {#BYTE $maxupload format=mb prec=3}MB, <span style="color:grey;">{$maxreason}</span>)</td>
  </tr>
</table>
{#END}

{#SUB _input_date}
  <input type="text" class="edit" name="{%name}" value="{%val}" size="17"{$enabled} /> (d.m.y H:i)
{#END}

{#SUB _input_email}
  <input type="text" class="edit" name="{%name}" value="{%val}" size="32"{$enabled} />
{#END}

{#SUB _input_dropdownedit}
  <select name="drop_{$name}"{$enabled} onchange="{$name}_change()">
    {#FOREACH $items}<option value="{%key}" {$sel}>{%caption}</option>{#END} 
  </select>
  <input type="text" class="edit" name="edit_{$name}" value="{%val}" size="32"{$enabled} />
  <script>
    function {$name}_change()
    {
      var i = -1;
      while(document.forms[++i])
        if(document.forms[i].edit_{$name}) with(document.forms[i])
        {
          edit_{$name}.disabled = (drop_{$name}.value == '') ? false : true;
          if(drop_{$name}.value != '') edit_{$name}.value = drop_{$name}.value;        
        }
    }
  {$name}_change()
  </script>
  <noscript></noscript>
{#END}
