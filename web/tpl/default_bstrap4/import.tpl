{! $Id$ }

{#SUB default}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
			<center>
				{#TRANSLATE "Bitte das zu importierende System auswählen!"}
			</center>
			<br />
			<a href="import.php?frame=clansphereimport">Accounts von Clansphere</a>
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB clansphereImport}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
			<h3>Bitte die Quelldatenbank w&auml;hlen!</h3>
			<br />
			{#FORM importClansphere url="import.php?frame=clansphereshowusers"}
				<table>
					<tr>
						<td>Clansphere DB Server:</td>
						<td>{#INPUT dbserver string}</td>
					</tr>
					
					<tr>
						<td>Clansphere DB User:</td>
						<td>{#INPUT dbuser string}</td>
					</tr>
					
					<tr>
						<td>Clansphere DB Password:</td>
						<td>{#INPUT dbpass string}</td>
					</tr>
					
					<tr>
						<td>Clansphere DB Name:</td>
						<td>{#INPUT dbname string}</td>
					</tr>
					
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
					
					<tr>
						<td colspan="2"><center>{#SUBMIT Weiter}</center></td>
					</tr>
				</table>
			{#END}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB lansurfer}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
			{#FORM action="import.php?frame=lscheck"}
			<table>
			<tr>
			<td>XML-Datei:</td>
			<td>{#INPUT xmldatei file $xmldatei}</td>
			</tr>
			<tr>
			<td>User:</td>
			<td>{#INPUT users yesno $users}</td>
			</tr>
			<tr>
			<td>Sitzplan:</td>
			<td>{#INPUT seats yesno $seats}</td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td>{#SUBMIT "Datei pr&uuml;fen"}</td>
			</tr>
			</table>
			{#END}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB lscheck}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
			{#FORM lansurfer}
			{#IF !empty($blocks)}
			<table>
			<tr>
			<th>Block</th>
			<th>Koordinaten*</th>
			</tr>
			{#FOREACH $blocks index=$i}
			<tr>
			<td>{$name}</td>
			<td>{#INPUT coord$i string}</td>
			</tr>
			{#END}
			</table>
			{#END}
			{#INPUT seats hidden $seats}
			{#INPUT users hidden $users}
			{#SUBMIT importieren}
			{#END}
			{#IF !empty($blocks)}
			<br />
			* Beginnend bei 0/0 f&uuml;r jeden Block eine X/Y-Koordinate angeben:<br />
			<table class="table">
			<tr>
			<td>0/0</td><td>1/0</td>
			</tr>
			<tr>
			<td>0/1</td><td>1/1</td>
			</tr>
			</table>
			{#END}
			</div>
		</div>
	</div>
</div>
{#END}
