{! $Id$ }

{#SUB default}
<div class="card ">

		<script>
		$(document).ready(function(){
		  $("#clanInput").on("keyup", function() {
			var value = $(this).val().toLowerCase();
			$("#clanTable tr").filter(function() {
			  $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			});
		  });
		});
		</script>

		<div class="card-header">	
			<div class="row">
				{#IF $create}&nbsp;<a href="clan.php?frame=editclan" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-crosshairs"></i>&nbsp;{§neuen Clan erstellen}</a>&nbsp;{#END}
			</div>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col col-md-6">
					<form>
					<div class="form-group">
						<label for="search">Suche</label>
						<input class="form-control" id="clanInput" type="text" placeholder="Search..">
						<small id="searchHelp" class="form-text text-muted">Clans durchsuchen...</small>
					</div>
					</form>
				</div>
			</div>
			<div class="row" align="left">
			<div class="table" style="overflow-x:auto;">
				{#IF $clanadmin}
					{#TABLE $clans id="clanTable" class="table table-sm table-hover table-bordered table-striped" role="grid" data-sort-name="name" data-pagination="true" data-sort-order="desc"}
					{#COL Name $name}<a href="clan.php?frame=viewclan&amp;id={$id}">{%name}</a>
					{#COL Website %homepage}{$homepageLink}
					{#COL Bild $image}<img src="{$image}" height="80" alt=""/>
					{#COL Beschreibung $description}<div style="overflow: hidden; height: 70px; width: 100%;">{%description}</div>
					{#COL Sitzpl&auml;tze $clan_seat_limit}{$clan_seat_limit}
					{#COL ClanPaid $status_paid_clan} {#IF $status_paid_clan == 1}<i class="fas fa-check-circle text-success fa-1x"></i>{#ELSE}<i class="fas fa-times text-secondary fa-1x"></i>{#END} 
					{#END}
				{#ELSE}
					{#TABLE $clans id="clanTable" class="table table-sm table-hover table-bordered table-striped" role="grid" data-sort-name="name" data-pagination="true" data-sort-order="desc"}
					{#COL Name $name}<a href="clan.php?frame=viewclan&amp;id={$id}">{%name}</a>
					{#COL Website %homepage}{$homepageLink}
					{#COL Bild $image}<img src="{$image}" height="80" alt=""/>
					{#COL Beschreibung $description}<div style="overflow: hidden; height: 70px; width: 100%;">{%description}</div>
					{#COL Sitzpl&auml;tze $clan_seat_limit}{$clan_seat_limit}
					{#END}
				{#END}
			</div>
		</div>
</div>
{#END}

{#SUB viewclan}
	<div class="card ">
		<div class="card-header">	
			<div class="row">
				<div class="col-6 col-sm-10 col-md-9">
				<a href="clan.php" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-reply"></i>&nbsp;{§zur&uuml;ck zur Liste}</a>&nbsp;
				{#IF $join}<a href="clan.php?frame=join&amp;id={$id}" class="btn btn-success btn-sm" role="button"><i class="far fa-question-circle"></i>&nbsp;Clan beitreten</a>&nbsp;{#END}
				{#IF $leader}&nbsp;<a href="clan.php?frame=editclan&amp;id={$id}" class="btn btn-secondary btn-sm" role="button"><i class="far fa-edit"></i>&nbsp; Clan {§Daten bearbeiten}</a>{#END}
				</div>
				<div class="col-6 col-sm-1 col-md-2">
				{#IF $leader}{#ACTION "Clan l&ouml;schen" del condition=$leader params="array(id=$id)" confirmation="Soll der Clan wirklich gel&ouml;scht werden?" buttonattrs="array(style=\"background-color:#c9302c\")" buttonclass="btn btn-danger btn-sm"}{#END}
				</div>
				<div class="col-sm-1 col-md-1"></div>
			</div>
			{#IF $clanadmin}
			<div class="alert alert-info" role="alert" style="margin-bottom: 0px; margin-top: 5px;">
				<div class="row">
					<div class="col-12">
					<div class="btn-group" role="group" aria-label="Basic example">
					<a href="user.php?frame=editchilds&amp;id={$id}" class="btn btn-secondary btn-sm" role="button"><i class="far fa-address-card"></i>&nbsp;Clan Mitglieder Verwalten</a>&nbsp;
					<a href="user.php?frame=editrights&amp;id={$id}" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-copyright"></i>&nbsp;Clan Rechte Verwalten</a>&nbsp;
					</div>
					
					{#IF $status_paid_clan}
					<button type="button" class="btn btn-sm btn-warning" data-dismiss="modal">
						{#ACTION RemoveStatusClanPaid delclanpaid params="array(id=$id)" buttonclass="btn btn-warning btn-sm"}</button>
					{#ELSE}
					<!-- Button trigger Clan Paid -->
					<button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#ClanPaidModalCenter">
					<i class="fas fa-copyright"></i>&nbsp;Clan auf Bezahlt setzen
					</button>

					<!-- Modal -->
					<div class="modal fade" id="ClanPaidModalCenter" tabindex="-1" role="dialog" aria-labelledby="ClanPaidModalCenterTitle" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="ClanPaidModalLongTitle">Clan auf Bezahlt setzen</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<p>Hiermit koennen alle Clan Mitglieder die bezahlten Clan Plaetze reservieren.
								Die Anzahl der Clan Mitglieder kann hoehersein als die bezahlten Plaetze.</p>
								{#FORM setclanpaid}
								{#INPUT id hidden $id}
								<div class="form-group">
									<label for="clanseatlimit">Anzahl der bezahlten Plaetze</label>
									{#INPUT amount decimal $amount}
								</div>
								<p class="text-center">{#SUBMIT Speichern buttonclass="btn btn-success btn-sm"}</p>
								{#END}
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
					</div>

					{#END}
					</div>
				</div>
			</div>
			{#END}
		</div>
	</div>
	{#LOAD $id $clan_id}
	<div class="card ">
		<div class="card-body">
			<div class="row">
				<div class="col col-md-6">
					<div class="col-12">
					<!-- Claninfos -->
					<h2>CLAN {§Infos}</h2>
					</div>
					<div class="col-12">
						<table class="table table-sm table-hover table-striped">
						{#FOREACH $props}
							{#IF $value != ""}
							<tr><th valign="top">{$caption}:</th><td>
								{#IF $caption == "Clan-Icon"}{#DBIMAGE $value process=createthumbnail(64,64)}
								{#ELSEIF $val_type == "Image"}{#DBIMAGE $value process=createthumbnail(150,150)}
								{#ELSE}{#IF $val_type == "Text" || substr($val_type, 0, 8) == "Document"}<pre>{$value}</pre>
								{#ELSE}{$value}
								{#END}
								{#END}</td></tr>
							{#END}
						{#END}
						
						</table>
					</div>
				</div>
				<div class="col col-md-6">
					{#IF $leader}
					<!-- Anfragen -->
					<div class="col-12">
					<h2>{§Anfragen}</h2>
					</div>
					<div class="col-12">
					{#TABLE $asking class="table table-sm table-hover table-striped"}
						{#COL Name %name}<a href="user.php?frame=viewsubject&amp;id={$id}">{%name}</a>
						{#ACTION hinzuf&uuml;gen confirm params="array(clan_id=$clan_id)"}
					{#END}
					</div>
					{#END}
				</div>
			</div>
		</div>
	</div>
	<div class="card ">		
		<div class="card-body">
				<!-- Clanmitglieder -->
				<div class="col-12">
				<h2>{§Mitglieder}</h2>
				</div>
				<div class="col col-md-6">
				{#LOAD $id $clan_id}
				{#TABLE $members class="table table-sm table-hover table-striped"}
					{#COL Name %name}<a href="user.php?frame=viewsubject&amp;id={$id}">{#IF $isleader}<i class="fas fa-user-circle"></i>&nbsp;{%name}&nbsp;(Leader){#ELSE}{%name}{#END}</a>
					{#ACTION entfernen delmember condition=$leader params="array(clan_id=$clan_id)"}
					{#OPTION "Leader ernennen" nominate condition=$leader}
					{#OPTION "Leader aberkennen" deprive condition=$leader}
				{#END}
				</div> 
				<div class="col col-md-6"></div>
		</div>
	</div>
{#END}

{#SUB editclan}
<div class="card ">
	<div class="card-header">	
		<div class="row">
			<div class="col-12">
				<a href="clan.php{#IF !(empty($id) || $id == "create_new_subject")}?frame=viewclan&amp;id={$id}{#END}"" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-reply"></i>&nbsp;{§zur&uuml;ck}</a>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12">
				{#FORM clan}
				{#INPUT id hidden $id}
				{#INPUT type hidden clan}
				<table>
				{#FOREACH $items}
					<tr><th>{#WHEN "$required==\"Y\"" "<span class=\"important\">$caption</span>" $caption} <br> {$hint}</th>
					<td>{#INPUT $name $val_type $val allowempty=$allowempty caption=$caption enabled=$enabled} </td></tr>
				{#END}
				{!der Templatecompiler kann nicht die Werte der Variablen vorraussehen und somit nicht wissen ob ein Uploadfeld vorhanden ist oder nicht ($val_type)}
				{#IF false}{#INPUT justtomaketheformanuploadform file enabled=0}{#END}
				<tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
				<td></td>
				<td>{#SUBMIT}</td></tr>
				</table>
				{#END}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB join}
<div class="card ">
	<div class="card-header">	
		<div class="row">
			<div class="col-12">
			<a href="clan.php?frame=viewclan&amp;id={$id}" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-reply"></i>&nbsp;{§zur&uuml;ck}</a>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12">
				{#TPL $jointext}
			</div>
			<div class="col-6 col-sm-6 col-md-6">
					{#FORM join}
					<table class="table table-bordered">
						<thead>
						<tr>
							<th>Checkbox</th>
							<th>Info</th>
						</tr>
						</thead>
						<tbody>
						<tr class="success">
							<td align="center">{#INPUT yes checkbox $yes}</td>
							<td><i class="far fa-handshake"></i>&nbsp;{§Ja, ich will.}</td>
						</tr>
						<tr>
							<td colspan="2">
							{#INPUT id hidden $id}
							{#INPUT captcha captcha $captcha}
							</td>
						</tr>
						<tr>
							<td colspan="2">
							{#SUBMIT anfragen a}
							</td>
						</tr>
					 </table>
					{#END}
			</div>
			<div class="col-6 col-sm-6 col-md-6"></div>
		</div>
	</div>
</div>
{#END}
