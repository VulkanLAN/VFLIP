{#SUB default}
<div class="card ">
	<div class="card-header">	
		<div class="row">
			<div class="col-12">
			  {#IF $admin}{#VAR sub=adminsection}{#END}
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12">
      {#TPL $text}
      </div>
			<div class="col-12">
        <div style="border: 2px solid grey; width: 60%">
            <br />
            <b>Dein Antrags-Status</b><br />
            <br />
            {#TABLEDISPLAY inet_status_messages $status inet_timeleft=$timeleft inet_queueposition=$queueposition}
            <br />
            <br />
            {#ACTION $req_caption requestaccess params="array(uid=$userid)" condition="(($status == 'unrequested') || (($status == 'expired') && $allowmultiple))"}
            {#ACTION "Antrag zur&uuml;cknehmen" withdrawrequest params="array(uid=$userid)" condition="$status == 'waiting'" confirmation="Willst du deinen Antrag wirklich zur&uuml;cknehmen?"}
            <br />
        </div>
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB adminsection}
  <div align="right">
    <div class="textfooter" style="text-align:left;border:1px solid gray;padding:4px;width:20%;">
      Admin:<br />
      <a class="textfooter" href="inet.php?frame=managerequests">Antr&auml;ge verwalten</a><br />
      <a class="textfooter" href="inet.php?frame=managegroups">Priorit&auml;tengruppen verwalten</a>
    </div>
  </div>
{#END}

{#SUB managerequests}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
        Hier k&ouml;nnen alle Internet-Antr&auml;ge bearbeitet werden.<br />
        Die &Auml;nderungen werden sofort in die Datenbank &uuml;bernommen, allerdings
        kann es eine Weile dauern, bis sie wirksam werden, da die freizuschaltenden
        Adressen normalerweise in einem bestimmten Intervall vom Gateway abgeholt werden!
      </div>
			<div class="col-12">
          {#TABLE $requests maxrows=50}
            {#COL Priorit&auml;t $priority}
            {#COL User $username}<a href="user.php?frame=viewsubject&amp;id={$user_id}">{$username}</a>
            {#COL Status $status}<center>{#TABLEDISPLAY inet_status_names $status}</center>
            {#COL "Online seit" $online_since}<center>{$online_since}</center>
            {#COL "Online bis" $online_until}<center>{$online_until}</center>
            {#COL "Onlinezeit" $online_time}<center>{$online_time}</center>
            {#OPTION "Status auf" changestatus}&nbsp;{#INPUT newstatus dropdown $status $states}&nbsp;setzen
            {#OPTION "Priorit&auml;t auf" changepriority}&nbsp;{#INPUT newpriority integer}&nbsp;setzen
            {#OPTION "Onlinezeit auf" changeonlinetime}&nbsp;{#INPUT new_online_time integer}&nbsp;Minuten setzen
          {#OPTION "L&ouml;schen" deleterequest}
          {#END}
          <br />
          <a href="inet.php">zur&uuml;ck</a>
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB managegroups}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
        Dies sind die m&ouml;glichen INet-Gruppen, anhand denen die User automatisch eine<br />
        bestimmte Prio sowie eine bestimmte Online-Zeit bekommen k&ouml;nnen. 
        Sie werden zugewiesen, wenn ein User einen Antrag stellt. Nachtr&auml;gliche &Auml;nderungen
        k&ouml;nnen jedoch nur &uuml;ber die Antrags-Verwaltung gemacht werden!
      </div>
			<div class="col-12">
          {#TABLE $groups maxrows=50}
            <a href="inet.php?frame=addgroup">add</a>
            {#COL Priorit&auml;t $priority}
            {#COL Subjekt-Gruppe $subject_group_name}<center>{$subject_group_name}</center>
            {#COL Online-Zeit $online_time}<center>{$online_time}</center>
            {#OPTION "Priorit&auml;t auf" changegrouppriority}&nbsp;{#INPUT newpriority integer}&nbsp;setzen
            {#OPTION "Onlinezeit auf" changegrouponlinetime}&nbsp;{#INPUT new_online_time integer}&nbsp;Minuten setzen
          {#OPTION "L&ouml;schen" deletegroup}
          {#END}
          <br />
          <a href="inet.php">zur&uuml;ck</a>
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB addgroup}
  <br />
  {#FORM addgroup}
  	<table>
  	  <tr>
  	    <td>Gruppe:</td>
  	    <td>{#INPUT subject_group_id dropdown param=$groups}</td>
  	  </tr>
  	  <tr>
  	    <td>Priorit&auml;t:</td>
  	    <td>{#INPUT priority integer $priority allowempty=0}</td>
  	  </tr>
  	  <tr>
  	    <td>Online-Zeit:</td>
  	    <td>{#INPUT online_time integer $online_time allowempty=0}&nbsp;Minuten</td>
  	  </tr>
  	  <tr align="center">
  	    <td colspan="2"><br />{#SUBMIT Hinzuf&uuml;gen}</td>
  	  </tr>
  	  <tr align="center">
  	    <td colspan="2"><br />{#BACKLINK Zur&uuml;ck}</td>
  	  </tr>
  	</table>
  {#END}
{#END}

