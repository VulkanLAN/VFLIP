﻿{#SUB default}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
				<div class="errorcell" style="color: red; font-weight: bold">
					<del>
					TableDancer befindet sich zur Zeit in der Beta-Phase!
					Bitte nicht im Produktiveinsatz verwenden.<br/>
					Es wird Java 5.0 (1.5.0) ben&ouml;tigt!
					</del>
					TableDancer ist veraltet und kann in der Aktuellen VFLIP Version nicht mehr verwendet werden.
				</div>
				<br />
				<object codetype="application/java-vm" 
						classid="java:tabledancer/TableDancer.class" 
						archive="ext/tabledancer.jar" 
						width="100%"
						height="{$height}">
					<param name="flipid" value="{%ID}"/>
					<param name="useragent" value="{%AGENT}"/>
					<param name="cookiename" value="{%NAME}"/>
				</object>
				<br />
			</div>
		</div>
	</div>
</div>
{#END}
