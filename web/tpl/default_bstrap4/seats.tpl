{! $Id: seats.tpl 2500 018-04-10 23:06:25Z loom $ edit by naaux u. tominator}

{#SUB blockgraph}
<div class="card ">
	<div class="card-header">
		{#VAR sub=header file=inc.seats.tpl}
		{#IF $showadmin}
		<div class="alert alert-info" role="alert" style="margin-bottom: 0px;">
			<div class="row">
				<div class="col">
					<p><b>Admin Sitzplatzverwaltung:</b></p>
					<div class="btn-group" role="group" aria-label="Basic example">
						&nbsp;<a href="seatsedit.php?frame=editblock&amp;id={$id}" {#WHEN "$frame=='editblock'" " class='btn btn-md btn-primary'"} class="btn btn-secondary btn-sm" role="button"><i class="fas fa-edit"></i>&nbsp; {§Block erstellen}</a>
						&nbsp;<a href="seatsedit.php?frame=editblocks"{#WHEN "$frame=='editblocks'" " class='btn btn-md btn-primary'"} class="btn btn-sm btn-secondary" role="button"><i class="fas fa-edit"></i>&nbsp;Bl&ouml;cke Bearbeiten</a> 
						&nbsp;<a href="seats.php?frame=FreeSeats"{#WHEN "$frame=='FreeSeats'" " class='btn btn-md btn-primary'"} class="btn btn-sm btn-secondary" role="button"><i class="fas fa-recycle"></i>&nbsp;alle Sitzplätze Freigeben</a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col col-lg-6">
					{#VAR sub=searchform}
					</div>
				<div class="col-lg-6"></div>
			</div>
		</div>
		{#END}
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12">
			{#TPL $text}
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-12">
				{#FOREACH $blocks}
					{#IF !empty ($cords)}
					<a href=" {%href}{#WHEN "!empty($userid)" "&amp;userid=%userid"}" title="{$caption}" alt="{$caption}" /" >
					<div style="text-align:center; position:absolute; {$cords}" id="{$name}">
					<div>{$caption} - Sitzpl&aumlze</div>
						<div class="progress">
							<div class="progress-bar progress-bar-striped" role="progressbar" style="width: {$blue_width}%" aria-valuenow="{$blue}" aria-valuemin="0" aria-valuemax="{$max}"> {$blue}<span class="sr-only">Clan-Vorgemerkt</span> </div>
							<div class="progress-bar progress-bar-striped bg-warning" role="progressbar" style="width:{$yellow_width}%" aria-valuenow="{$yellow}" aria-valuemin="0" aria-valuemax="{$max}">{$yellow}<span class="sr-only">Vorgemerkt</span></div>
							<div class="progress-bar progress-bar-striped bg-danger" role="progressbar" style="width:{$red_width}%" aria-valuenow="{$red}" aria-valuemin="0" aria-valuemax="{$max}">{$red}<span class="sr-only">Reserviert</span></div>
							<div class="progress-bar bg-success" role="progressbar" style="width:{$green_width}%" aria-valuenow="{$green}" aria-valuemin="0" aria-valuemax="{$max}">{$green}<span class="sr-only">Frei</span></div>
							<div class="progress-bar progress-bar-grey" role="progressbar" style="width:{$grey_width}%" aria-valuenow="{$grey}" aria-valuemin="0" aria-valuemax="{$max}">{$grey}<span class="sr-only">Deaktiviert</span></div>
						</div>
					</div>
					</a>
					{#END}
				{#END}
				<div class="clear"></div>
				<img src="seats.php?frame=overviewimage" width="{$img.width}" height="{$img.height}" border="0" alt="" usemap="#seats" />
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB FreeSeats}
<div class="card ">
	<div class="card-header">
		{#VAR sub=header file=inc.seats.tpl}
		{#IF $showadmin}
		<div class="alert alert-info" role="alert" style="margin-bottom: 0px;">
			<div class="row">
				<div class="col-8 col-sm-8 col-md-8">
						<div><b>Admin Sitzplatzverwaltung:</b></div>
							&nbsp;<a href="seatsedit.php?frame=editblock&amp;id={$id}" {#WHEN "$frame=='editblock'" " class='btn btn-md btn-primary'"} class="btn btn-secondary btn-sm" role="button"><i class="fas fa-edit"></i>&nbsp; {§Block erstellen}</a>
							&nbsp;<a href="seatsedit.php?frame=editblocks"{#WHEN "$frame=='editblocks'" " class='btn btn-md btn-primary'"} class="btn btn-sm btn-secondary" role="button"><i class="fas fa-edit"></i>&nbsp;Bl&ouml;cke Bearbeiten</a> 
							&nbsp;<a href="seats.php?frame=FreeSeats"{#WHEN "$frame=='FreeSeats'" " class='btn btn-md btn-primary'"} class="btn btn-sm btn-secondary" role="button"><i class="fas fa-recycle"></i>&nbsp;alle Sitzplätze Freigeben</a>
				</div>
			</div>
			<div class="row">
				<div class="col-6 col-sm-6 col-md-6">
					{#VAR sub=searchform}
					</div>
				<div class="col-6 col-sm-6 col-md-6"></div>
			</div>
		</div>
		{#END}
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12">
			{#TPL $text}
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-12">
				<map name="seats" id="seats">
				{#FOREACH $blocks}
					<area {$cords} href="{%href}{#WHEN "!empty($userid)" "&amp;userid=%userid"}" title="{$caption}" alt="{$caption}" /">
				{#END}
				</map>
				<p>
					<img src="seats.php?frame=overviewimage" width="{$img.width}" height="{$img.height}" border="0" alt="" usemap="#seats" />
				</p>
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB blocklist}
<div class="card ">
	<div class="card-header">
		{#VAR sub=header file=inc.seats.tpl}
		{#IF $showadmin}
		<div class="alert alert-info" role="alert" style="margin-bottom: 0px;">
			<div class="row">
				<div class="col-8 col-sm-8 col-md-8">
						<div><b>Admin Sitzplatzverwaltung:</b></div>
							&nbsp;<a href="seatsedit.php?frame=editblock&amp;id={$id}" {#WHEN "$frame=='editblock'" " class='btn btn-md btn-primary'"} class="btn btn-secondary btn-sm" role="button"><i class="fas fa-edit"></i>&nbsp; {§Block erstellen}</a>
							&nbsp;<a href="seatsedit.php?frame=editblocks"{#WHEN "$frame=='editblocks'" " class='btn btn-md btn-primary'"} class="btn btn-sm btn-secondary" role="button"><i class="fas fa-edit"></i>&nbsp;Bl&ouml;cke Bearbeiten</a> 
							&nbsp;<a href="seats.php?frame=FreeSeats"{#WHEN "$frame=='FreeSeats'" " class='btn btn-md btn-primary'"} class="btn btn-sm btn-secondary" role="button"><i class="fas fa-recycle"></i>&nbsp;alle Sitzplätze Freigeben</a>
				</div>
			</div>
			<div class="row">
				<div class="col-6 col-sm-6 col-md-6">
					{#VAR sub=searchform}
					</div>
				<div class="col-6 col-sm-6 col-md-6"></div>
			</div>
		</div>
		{#END}
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12">
			{#TPL $text}
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-12">
				{#TABLE $blocks class="table table-bordered table-striped table-hover table-sm"}
						{#COL Bild style="padding:0px"}<a href="{$href}">{#DBIMAGE $background_tmp process="createThumbnail(200,128)" border=0}</a>
						{#COL Sitzplan}<a href="{$href}">{$caption}</a>
						{#COL Beschreibung}{$description}
						
				{#END}

			</div>
		</div>
	</div>
</div>
{#END}

{#SUB block}
<div class="card ">
	<div class="card-header">
		<div class="row">
			<div class="col">
				<a href="seats.php?frame=blockgraph" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-map-marker-alt"></i>&nbsp; &Uuml;bersicht</a>
			</div>
		</div>
		{#IF $admin}<!-- edit by vulkanlan -->
		<div class="row">
			<div class="alert alert-info" role="alert" style="margin-bottom: 0px;">
				<div class="row">
					<p><b>Admin Sitzplatzverwaltung:</b></p>
					<form class="form-inline">
						<div class="form-row align-items-center">
							<div class="col-auto">
							{#FORM action="seats.php" method="get"}
								{#INPUT frame hidden block}
									<select name="blockid" onchange="submit()">
									{#FOREACH $blocks $blockname $blockid}
									<option value="{$blockid}"{#WHEN "$blockid==$block_id" " selected=\"1\""}>{$blockname}</option>
									{#END}
									</select>
									<noscript>{#SUBMIT anzeigen shortcut=0}</noscript>
							{#END}
							</div>
							<div class="col-auto">
								<div class="btn-group" role="group" aria-label="admincontrol">
											&nbsp;<a href="seatsedit.php?frame=editblock&amp;id={$id}" {#WHEN "$frame=='editblock'" " class='btn btn-md btn-primary'"} class="btn btn-secondary btn-sm" role="button"><i class="fas fa-edit"></i>&nbsp; {§Block erstellen}</a>
											&nbsp;<a href="seatsedit.php?frame=editblock&amp;id={$block_id}" {#WHEN "$frame=='editblock'" " class='btn btn-md btn-primary'"} class="btn btn-secondary btn-sm" role="button"><i class="fas fa-edit"></i>&nbsp; Block Daten editieren</a>
											&nbsp;<a href="seatsedit.php?frame=blocktools&amp;id={$block_id}" {#WHEN "$frame=='blocktools'" " class='btn btn-md btn-primary'"} class="btn btn-secondary btn-sm" role="button"><i class="fas fa-edit"></i>&nbsp; Block Grafisch editieren</a>
											&nbsp;<a href="seatsedit.php?frame=editblocks"{#WHEN "$frame=='editblocks'" " class='btn btn-md btn-primary'"} class="btn btn-sm btn-secondary" role="button"><i class="fas fa-edit"></i>&nbsp;Bl&ouml;cke Bearbeiten</a> 
											&nbsp;<a href="seats.php?frame=FreeSeats"{#WHEN "$frame=='FreeSeats'" " class='btn btn-md btn-primary'"} class="btn btn-sm btn-secondary" role="button"><i class="fas fa-recycle"></i>&nbsp;alle Sitzplätze Freigeben</a>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="row">
					<div class="col-6 col-sm-6 col-md-6">
						{#VAR sub=searchform}
						</div>
					<div class="col-6 col-sm-6 col-md-6"></div>
				</div>
			</div>
		</div>
		{#END}
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12">
			{#TPL $text}
			</div>
		</div>
		<div class="row">
			<div class="col-12">
			<b>{$stat}</b>
			</div>
		</div>
		<div class="row">
			{#IF !empty($found)}
			<div class="col-6 success">
			  <table cellspacing="0" cellpadding="1" class="table table-sm table-hover table-striped">
				{#FOREACH $found.result}
				<tr>
				  <td style="padding-right:8px;"><b>{$name}</b></td><td>{$places}</td>
				</tr>
				{#END}  
			  </table>
			</div>
			<div class="col-6"></div>
			{#END}
			<div class="card-body">
				<div class="row" align="left">
					<div class="col-12">
						{#FOREACH $seats}
							{#IF $shape == "rectangle"}
								<div 
								{#IF $drawchairs == 'Y'}
								 style="text-align:center; 
									position:absolute; top: {$top}px; left: {$left}px; width: 40px; height: 40px;
									-ms-transform: rotate(-{$angle}deg); /* IE 9 */
									-webkit-transform: rotate(-{$angle}deg); /* Safari */
									transform: rotate(-{$angle}deg); /* Standard syntax */"
									id="{$name}">
								{#ELSE}
								 style="position:absolute; top: {$top}px; left: {$left}px; width: 40px; height: 40px;
									-ms-transform: rotate(-{$angle}deg); /* IE 9 */
									-webkit-transform: rotate(-{$angle}deg); /* Safari */
									transform: rotate(-{$angle}deg); /* Standard syntax */" 
									id="{$name}">
								{#END}
								<a href="{$url}" alt="{$name}" data-toggle="tooltip"  data-html="true" title="<b>{$name}</b> - <em>{$title}</em>">
									{#VAR sub=seatstatus}
								</a></div>
							{#ELSEIF $shape != "rectangle"}
								<div class="card bg-danger">
									<div class="card-header">Fehler - Es gibt einen Sitzplatz der einen anderen Winkel als (0,90,180,270 verwendet. Bitte den "Sitzplaz - Winkel" von "polygon" (freier Winkel) auf "rectangle" umstellen!</div>
								</div>
								<?php break; ?>
							{#END}
						{#END}
						<div class="clear"></div>
						<img src="{$img}" width="{$width}" height="{$height}" border="0" alt="">
					</div>
				</div>
				<div class="row" align="left">
					<div class="col-12 col-sm-12 col-lg-8 table-responsive-sm">
					<h3>Legende:</h3>
					
					<table class="table table-bordered table-striped table-sm">
					
						<thead> <tr> <th>Sitzplatz</th> <th>Beschreibung</th> <th>Sitzplatz</th> <th>Beschreibung</th> </tr> </thead> 
						<tbody> 
						<tr>
							<th scope="row"> <button type="button" class="btn btn-success "><i class="fas fa-asterisk"></i></button> </th> 
							<td>Ein freier Sitzplatz.</td>
							<th scope="row"> <button type="button" class="btn " style="background-color:#aaa"><i class="fas fa-times" style="color:black"></i></button> </th> 
							<td>Ein deaktivierter Sitzplatz.</td>
						</tr>
						<tr>
							<th scope="row"> <button type="button" class="btn " style="background-color:#fff"><i class="far fa-user-circle"></i></button> </th> 
							<td>Diese Markierung kennzeichnet Sitzplätze, die du reserviert oder vorgemerkt hast, bzw. die auf deine Suchanfrage zutreffen. </td>
							<th scope="row"> <button type="button" class="btn btn-primary "><i class="fas fa-stop"></i></button> </th> 
							<td>Ein vorgemerkter Clan - Sitzplatz. Nur fuer Clan Mitglieder.</td>
						</tr>
						
						<tr>
							<th scope="row"> <button type="button" class="btn btn-warning "><i class="fas fa-hand-paper"></i></button> </th> 
							<td>Ein vorgemerkter Sitzplatz.</td>
							<th scope="row"> <button type="button" class="btn btn-warning "><i class="fas fa-hand-paper" style="color:blue"></i></button> </th> 
							<td>Ein vorgemerkter Clan - Sitzplatz, der von einem CLAN-Mitglied vorgemerkt ist.</td>
						</tr>
						<tr>
							<th scope="row"> <button type="button" class="btn btn-danger "><i class="fas fa-stop"></i></button> </th> 
							<td>Ein reservierter Sitzplatz.</td>
							<th scope="row"> <button type="button" class="btn btn-danger "><i class="fas fa-stop" style="color:blue"></i></button> </th> 
							<td>Ein reservierter Sitzplatz, der von einem CLAN-Mitglied reserviert ist.</td>
						</tr>
						<tr>
							<th scope="row"> <button type="button" class="btn btn-purple "><i class="fas fa-stop"></i></button> </th> 
							<td>Ein reservierter CLAN Sitzplatz.</td>
							<th scope="row"> <button type="button" class="btn btn-purple "><i class="fas fa-stop" style="color:black"></i></button> </th> 
							<td>Ein reservierter Clan - Sitzplatz, der von keinem CLAN-Mitglied reserviert ist.</td>
						</tr>
						<!--
						<tr>
							<th scope="row"> <button type="button" class="btn btn-danger btn-block btn-sm"><i class="fas fa-stop" style="color:green"></i></button> </th> 
							<td>Ein Sitzplatz mit eingechecktem User.</td>
							<th scope="row"> <button type="button" class="btn btn-primary btn-block btn-sm"><i class="fas fa-stop" style="color:green"></i></button> </th> 
							<td>Ein Sitzplatz mit eingechecktem Clan - User.</td>
						</tr>
						<tr>
							<th scope="row"> <button type="button" class="btn btn-danger btn-block btn-sm"><i class="fas fa-stop" style="color:lime"></i></button> </th> 
							<td>Ein Sitzplatz mit User, der online ist.</td>
							<th scope="row"> <button type="button" class="btn btn-primary btn-block btn-sm"><i class="fas fa-stop" style="color:lime"></i></button> </th> 
							<td>Ein Sitzplatz mit Clan-User, der online ist.</td>
						</tr>
						<tr>
							<th scope="row"> <button type="button" class="btn btn-danger btn-block btn-sm"><i class="fas fa-stop" style="color:gray"></i></button> </th> 
							<td>Ein Sitzplatz mit User, der offline ist. </td>
							<th scope="row"> <button type="button" class="btn btn-primary btn-block btn-sm"><i class="fas fa-stop" style="color:gray"></i></button> </th> 
							<td>Ein Sitzplatz mit Clan-User, der offline ist.</td>
						</tr>
						<tr>
							<th scope="row"> <button type="button" class="btn btn-danger btn-block btn-sm"><i class="fas fa-stop" style="color:blue"></i></button> </th> 
							<td>Ein Sitzplatz mit ausgechecktem User.</td>
							<th scope="row"> <button type="button" class="btn btn-primary btn-block btn-sm"><i class="fas fa-stop" style="color:blue"></i></button> </th> 
							<td>Ein Sitzplatz mit ausgechecktem Clan-User.</td>
						</tr> -->
						</tbody>
					</table>

					</div><div class="col col-lg-2"></div>
				</div>
			</div>
			{#TPL $afterlegend}
		</div>
	</div>
</div>
{#END}

{#SUB seatstatus}
		
		{#IF $enabled != "Y"}
			<button type="button" class="btn btn-block btn-sm seatbutton" style="background-color:#aaa"><i class="fas fa-times" style="color:black;"></i></button>
		{#ELSE}
			{#IF $user_id_clan == null}
				{#IF $user_id == 0}
					<button type="button" class="btn btn-success btn-block btn-sm seatbutton"><i class="fas fa-asterisk"></i></button>
				{#ELSEIF $reserved == 'N'}
					<button type="button" class="btn btn-warning btn-block btn-sm seatbutton"><i class="fas fa-hand-paper"></i></button>
					{#IF $drawchairs == 'Y'}<i class="far fa-user-circle"></i>{#END}
				{#ELSE}
					<button type="button" class="btn btn-danger btn-block btn-sm seatbutton"><i class="fas fa-stop"></i></button>
					{#IF $drawchairs == 'Y'}<i class="far fa-user-circle"></i>{#END}
				{#END}
			{#ELSE}
				{#IF $user_id == 0}
					{#IF $clanpaid == 'Y'}
						{#IF !empty($clanimage)}
							<button type="button" class="btn btn-purple seatbutton" style="padding: 0; margin: 0;">
								{#VAR sub=seatclanimagerotate}
							</button>
						{#ELSE}
							<button type="button" class="btn btn-purple btn-block btn-sm seatbutton"><i class="fas fa-stop"></i></button>
						{#END}
					{#ELSE}
						{#IF !empty($clanimage)}
							<button type="button" class="btn btn-primary seatbutton" style="padding: 0; margin: 0;">
								{#VAR sub=seatclanimagerotate}
							</button>
						{#ELSE}
							<button type="button" class="btn btn-primary btn-block btn-sm seatbutton"><i class="fas fa-stop"></i></button>
						{#END}
					{#END}
				{#ELSEIF $reserved == "N"}
					{#IF !empty($clanimage)}
					<div style=" position: relative; text-align: center;">
					<button type="button" class="btn btn-warning seatbutton" style="padding: 0; margin: 0;">
						{#VAR sub=seatclanimagerotate}
					 </button>
					 </div>
					{#ELSE}
					<button type="button" class="btn btn-warning btn-block btn-sm seatbutton"><i class="fas fa-hand-paper" style="color:blue"></i></button>
					{#END}
					{#IF $drawchairs == 'Y'}<i class="far fa-user-circle"></i>{#END}
				{#ELSE}
					{#IF !empty($clanimage)}
						<div style=" position: relative; text-align: center;">
						<button type="button" class="btn btn-danger seatbutton" style="padding: 0; margin: 0;">
							{#VAR sub=seatclanimagerotate}
						 </button>
						 </div>
					{#ELSE}
					<button type="button" class="btn btn-danger btn-block btn-sm seatbutton"><i class="fas fa-stop" style="color:blue"></i></button>
					{#END}
					{#IF $drawchairs == 'Y'}<i class="far fa-user-circle"></i>{#END}
				{#END}
			{#END}
		{#END}
{#END}

{#SUB seatclanimagerotate}

	{#IF $angle == 90}
			<div style="-ms-transform: rotate(90deg); /* IE 9 */
			-webkit-transform: rotate(90deg); /* Safari */
			transform: rotate(90deg); /* Standard syntax */">{#DBIMAGE $clanimage process=createthumbnail(32,32)} </div>
		{#ELSEIF $angle == 180} 
			<div style="-ms-transform: rotate(180deg); /* IE 9 */
			-webkit-transform: rotate(180deg); /* Safari */
			transform: rotate(180deg); /* Standard syntax */">{#DBIMAGE $clanimage process=createthumbnail(32,32)} </div>
		{#ELSEIF $angle == 270} 
			<div style="-ms-transform: rotate(270deg); /* IE 9 */
			-webkit-transform: rotate(270deg); /* Safari */
			transform: rotate(270deg); /* Standard syntax */">{#DBIMAGE $clanimage process=createthumbnail(32,32)} </div>
		{#ELSE}{#DBIMAGE $clanimage process=createthumbnail(32,32)}
	{#END}

{#END}

{#SUB seat}
	<div class="card ">
		<div class="card-header">	
			<div class="row">
				<div class="col-12">
					 <a href="seats.php?frame=block&amp;blockid={$block_id}{$linkpost}" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-map-marker-alt"></i>&nbsp;Zur&uuml;ck zum Sitzplan</a>
				</div>
			</div>
		</div>
		<div class="card-body">
		<div class="row">
		<div class="col-12">
			
					<!-- Sitzplatzstatusänderungen -->
					
						<table class="table table-bordered table-striped">
							<thead> <tr> <th>Status</th> <th>Info</th> </tr> </thead> 
							<tbody>
								<tr>
									<th scope="row">Sitzplatz:</th> 
									<td>{$name}</td>
								</tr>
								{#IF !empty($ip)}
								<tr>
									<th scope="row"> IP-Adresse: </th> 
									<td>{$ip}</td>
								</tr>
								{#END}
								<tr>
									<th scope="row">
									<div style="text-align:center; width: 40px; height: 40px;">
									{#VAR sub=seatstatus}
									</div>
									</th> 
									<td>{$stat}</td>
								</tr>
								
							</tbody>
						</table>
					
			</div>
			{#IF !empty($actions)}
			<div class="col-12">
				
						<!-- Sitzplatzstatusänderungen -->
						
						<table class="table table-bordered table-striped">
						
						<thead> <tr> <th>Status</th> <th >Info</th> <th>ToDo</th> </tr> </thead> 
						<tbody>
							{#FOREACH $actions}
							<tr>
								<td><!-- <img src="{$img.imgname}" border="0" alt="" width="{$img.width}" height="{$img.height}"> -->
								<div class="d-flex" style="text-align:center; width: 40px; height: 40px;">
									{#IF !empty($clanimage)}
									<div style=" position: relative; text-align: center;">
									<button type="button" class="{$buttonclass} seatbutton align-content-center p-2 w-100" style="{$buttonstyle}" style="padding: 0; margin: 0;">
									 {#DBIMAGE $clanimage process=createthumbnail(32,32)}
									<!-- <div style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);" ><i class="fas fa-stop" style="color:blue"></i></div> -->
									 </button>
									 </div>
									{#ELSE}
									<button type="button" class="{$buttonclass} seatbutton" style="{$buttonstyle}"><i class="{$icon}" style="{$iconstyle};"></i></button>
									{#END}
								</div>
								</td>
								<td >{$msg}
								{#IF $action == "clannote"}
								  {#FORM ClanNote class="form-inline"}
								    {#INPUT clannote dropdown param=$clannote}{#INPUT id hidden $id}
								    </td>
								    <td>
								      <div class="col col-sm-12 col-md-11">
								        {#SUBMIT "$button" shortcut=0}
								      </div>
								    </td>
								  {#END}
								{#ELSEIF $action == "clanreserve"}
								  {#FORM ClanReserve class="form-inline"}
								    {#INPUT clanreserve dropdown param=$clanreserve}{#INPUT id hidden $id}
								    </td>
								    <td>
								      <div class="col col-sm-12 col-md-11">
								        {#SUBMIT "$button" shortcut=0}
								      </div>
								    </td>
								  {#END}
								{#ELSE}
								</td>
								<td>
									<form method="post" class="form-check">
											<input type="hidden" name="action" value="{$action}"/>
											<input type="hidden" name="id" value="{$id}"/>
											<input type="submit" class="button btn btn-outline-dark" role="button" value="{$button}"/>
									</form>
								</td>
								{#END}
							</tr>
							{#END}
						</tbody>
						</table>
						
				
				</div>
				{#IF empty($user_id_clan) and !empty($clanseat)}
				<!-- Sitzplatz CLAN Statusaenderungen -->
				<div class="col col-sm-12 col-md-11">
				<div class="card">
					<div class="card-body">
						{#FORM ClanSeat class="form-inline"}
						<div class="form-row align-items-center">
							<div class="col-auto">Admin: Clan Reservierung:</div>
							<div class="col-auto">{#INPUT clanseat dropdown param=$clanseat}{#INPUT id hidden $id}</div>
							<div class="col-auto">{#SUBMIT Clan-Reservieren shortcut=0}</div>
						</div>
						{#END}
					</div>
				</div>
				</div>	<div class="col-md-1 col-lg-1"></div>
				{#END}
				{#IF !empty($exchange)}
				<!-- Sitzplatz Tauschen -->
				<div class="col col-md-11 col-lg-11">
					<div class="card ">
						<div class="card-body">
								{#FORM seatexchange class="form-inline"}
								<div class="form-row align-items-center">
								<div class="col-auto">Admin: Sitzplatz tauschen mit:</div>
								<div class="col-auto">{#INPUT exchange dropdown param=$exchange}{#INPUT id hidden $id}</div>
								<div class="col-auto">{#SUBMIT tauschen shortcut=0}</div>
								</div>
								{#END}
						</div>
					</div>
				</div> <div class="col-md-1 col-lg-1"></div>
				{#END}
			{#END}
		</div>
		</div>
	</div>
{#END}

{#SUB searchform}
		{#FORM search}
		<div>
			Spielername: {#INPUT search string $search} {#SUBMIT suchen}</div>
		{#END}
{#END}

{#SUB search}
	<div class="card ">
		<div class="card-header">	
			<div class="row">
				<div class="col-12">
					<a href="seats.php" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-map-marker-alt"></i>&nbsp; zur Sitzplan&uuml;bersicht</a>
				</div>
			</div>
			<div class="row">
				  {#IF $admin} <!-- edit by vulkanlan -->
				<div class="col-6 col-sm-6 col-md-6">
				 {#VAR sub=searchform}</div>
				 <div class="col-6 col-sm-6 col-md-6"></div>
				 {#END}
			</div>
		</div>
		<div class="card-body">
		<div class="row">
		{#IF empty($blocks)}
				<div class="col-12">
				<p>Es wurden keine Sitzpl&auml;tze gefunden.</p>
				</div>
			{#ELSE}
				<div class="col-12">
				{#FOREACH $blocks $ablock $block_caption}
				<h3>{$block_caption}</h3>
					{#TABLE $ablock}
						{#COL User %username}
						{#COL Sitzplatz $name}<a href="seats.php?frame=block&amp;blockid={$block_id}&amp;ids[]={$user_id}">{$name}</a>
					{#END}
				{#END}
			</div>
		{#END}
		</div>
		</div>
	</div>
{#END}
