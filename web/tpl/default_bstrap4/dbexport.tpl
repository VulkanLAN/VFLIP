{#SUB default}
<div class="card ">
	<div class="card-header">	
		<div class="row">
			<div class="col-12">
      Hier kannst du die gesamte Datenbank als SQL-Dump exportieren
      <a href="dbexport.php?frame=export" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-file-export"></i>&nbsp;Alles exportieren</a>
      <a href="dbexport.php?frame=selecttables" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-table"></i>&nbsp;Tabellen ausw&auml;hlen</a>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12">
      <p align="justify">
        Um die Datenbank zu exportieren wird das Recht dbexport_create_dump be&ouml;tigt.
        Da du diesen Text liest, hast du es bereits ;) Der User dbexport besitzt 
        nur dieses Recht und kann helfen, das Datenbankbackup zu automatisieren.
        Wenn er aktiviert und mit einem Passwort versehen ist, l&auml;sst sich der Dump
        &uuml;ber die URL 
        <pre>
          dbexport.php?frame=export&ident=dbexport&password=blablablupp
        </pre>
        herunterladen. Unter Linux ist folgender Script dabei hilfreich: Er f&uuml;gt die 
        Zeit in den Dateinamen mit ein und kann gut als regelm&auml;&szlig;iger Cronjob 
        ausgef&uuml;hrt werden.
      </p>
      <pre>
        #!/bin/bash
        ####################### Config ############################

        # Die URL, unter der das FLIP laeuft
        FLIP="http://localhost/flip"

        # Das Verzeichnis, unter dem die Backups gespeichert werden.
        TARGET="./"

        # Das Passwort des Users dbexport im FLIP.
        PASSWORD="123"

        # Soll der Dump nach dem herunterladen bzip2-komprimiert werden?
        #BZIP2=1

        ###########################################################

        FILE="${TARGET}flip_sql_dump-`date +%Y%m%d%k%M%S`.sql"
        URL="${FLIP}/dbexport.php?frame=export&ident=dbexport&password=${PASSWORD}"
        wget ${URL} -O${FILE}
        if [ $BZIP2 ] 
        then 
          bzip2 -z ${FILE}
        fi
        </pre>
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB selecttables}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
      {#TABLE $tables class="table table-bordered"}
        {#COL Tabelle $Name}    
        {#COL "Gr&ouml;&szlig;e"}{#BYTE $Data_length format=kb prec=1} kB  
        {#OPTION "ausgew&auml;hlte Tabellen exportieren" exportSelected}
        {#OPTION "nicht ausgew&auml;hlte Tabellen exportieren" exportNotSelected}
      {#END}
			</div>
		</div>
	</div>
</div>
{#END}