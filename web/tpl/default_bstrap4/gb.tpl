{! $Id: gb.tpl 1285 2007-02-16 19:10:53Z tazz $ }

{#SUB default}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
        <a href="gb.php?frame=add">{§G&auml;stebucheintrag schreiben...}</a><br/>
        {#IF $edit == "true"}<a href="gb.php?frame=icons">{§Icons bearbeiten}</a><br/>{#END}

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          {#FOREACH $entries}
          <tr>
            <td><b>{#DATE $date}</b></td><td align="right">{%author}</td>
          </tr>
          <tr>
            <td colspan="2"><hr /></td>
          </tr>
          <tr>
            <td align="justify" colspan="2" style="padding-bottom:40px;"><strong>{%title}</strong><br /><br />{$text}<br />
              {#IF $edit && !isset($noentry)}<br /><span class="textfooter">
              <br />Admin: <a href="gb.php?frame=del&amp;id={$id}">{§l&ouml;schen}</a>
              <a href="gb.php?frame=edit&amp;id={$id}">edit</a>
              </span>{#END}</td>
          </tr>
          {#END}
        </table>
        {#IF isset($less)}<a href="gb.php?more={$less}">vorherige</a>{#END}
        {#IF isset($less) && !empty($more)} - {#END}
        {#IF !empty($more)}<a href="gb.php?more={$more}">weitere</a>{#END}
        <br />
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB del}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
        {§Soll der gbeintrag} "{%title}" {§wirklich gel&ouml;scht werden}?<br />
        <br />
        {#FORM del action="gb.php"}
          {#INPUT id hidden $id}
          {#SUBMIT ja}<br />
          <br />
          {#BACKLINK NEIN/zur&uuml;ck}
        {#END}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB add}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
        {#FORM entry id=gaestbuchformular}
        {#BACKLINK zur&uuml;ck}<br />
        <br />
        <script type="text/javascript">
        function add(zeichen)
        {
            var form = document.getElementById('gaestbuchformular');
            //Formular sollte nur ein Textarea beinhalten
            var area = form.getElementsByTagName('textarea')[0];
            if (document.selection) {
                area.focus();
                sel = document.selection.createRange();
                sel.text = zeichen;
            }
            else if (area.selectionStart || area.selectionStart == '0') {
                area.value = area.value.substring(0, area.selectionStart) + zeichen + area.value.substring(area.selectionEnd, area.value.length);
            }
            // General
            else {
                area.value += zeichen;
            }
            area.focus();
        }
        </script>

        <table cellspacing="0" cellpadding="2" border="0">
        <tr><td>{§Autor}:</td><td>{#INPUT author string $author} {§leer} = {§Benutzer}</td></tr>
        <tr><td>{§Titel}:</td><td>{#INPUT title string $title param="array(id=text)"}<br/><br/>
        </td></tr>
        </table>
        Zum Smilie einf&uuml;gen, einfach anklicken:
        {#FOREACH $codes $path $icontext}
        <a href="javascript:add('{$icontext}');"><img src="{#STATICFILE $path}" alt="{$icontext} border="0"></a>
        {#END}
        <br/><br/>
        {#INPUT text document $text}<br />
        {#INPUT id hidden $id}
        {#INPUT Sicherheitsabfrage captcha}
        {#SUBMIT Eintragen}<br />
        {#END}
        {#END}

        {#SUB icons}
        <a href="gb.php">{§zum G&auml;stebuch}</a><br/>
        {#TABLE $icons}
          <a href="gb.php?frame=editicon">{§neu}</a>
          {#COL Text %text}
          {#COL Icon $icon}<img src="{#STATICFILE $icon}" alt="{$icon}" />
          {#FRAME edit editicon}
        {#END}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB editicon}
    {#FORM icon}
    {#BACKLINK zur&uuml;ck}<br/>
    {#INPUT text String $text} => {#INPUT icon dropdown $icon param=$icons}<br/>
    {#INPUT id hidden $id}
    {#SUBMIT}
    {#END}
{#END}

{#SUB edit}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
        {#FORM edit}
          {#INPUT id hidden $id}
          <table>
            <tr>
              <td>Titel:</td>
              <td>{#INPUT title string $title}</td>
            </tr>
            <tr>
              <td>Author:</td>
              <td>{#INPUT author string $author}</td>
            </tr>
            <tr>
              <td>Datum:</td>
              <td>{#INPUT date date $date}</td>
            </tr>     
            <tr>
              <td>Eintrag:</td>
              <td>{#INPUT text text $text}</td>
            </tr>
            <tr align="center">
              <td colspan="2"><br />{#SUBMIT Speichern}</td>
            </tr>
            <tr align="center">
              <td colspan="2"><br />{#BACKLINK zur&uuml;ck}</td>
            </tr>
          </table>
        {#END}
			</div>
		</div>
	</div>
</div>
{#END}