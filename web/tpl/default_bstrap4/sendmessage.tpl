{! $Id: sendmessage.tpl 1330 2006-11-26 01:20:57Z scope $ }

{#SUB default}
<div class="card ">
	<div class="card-header">
		<div class="row">
			<div class="col">
		{#TPL $text}
		<hr>
		</div>
		</div>
		<div class="row my-2">
		<div class="col">
		<a class="btn btn-secondary btn-md " role="button" href="sendmessage.php">Rundbriefe anzeigen</a> 
		<a class="btn btn-secondary btn-md " role="button" href="sendmessage.php?type=sys">Systemnachrichten anzeigen</a>
		<a class="btn btn-secondary btn-md " role="button" href="sendmessage.php?frame=editmessage&amp;type={$type}">Neue Nachricht</a>
		<a class="btn btn-secondary btn-md " role="button" href="sendmessage.php?frame=send">Nachricht senden</a>
		</div>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col">
			{#TABLE $mails actions=$action}
			  {#COL Name $name}<a href="sendmessage.php?frame=viewmessage&amp;id={$id}&amp;user={$user}">{$name}</a>
			  {#COL Betreff %subject}
			  {#COL Beschreibung %description}
			  {#FRAME edit editmessage}
			  {#FRAME send send}
			  {#OPTION L&ouml;schen deletemessage condition="$type!='sys'"}
			{#END}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB editmessage}
{#FORM message}
<table width="100%">
  <tr><td>{#TPL $text}</td></tr>
  <tr><td>G&uuml;ltige Variablen: {%vars}<br /><br /></td></tr>
  <tr><td>Name: {#INPUT name string $name}</td></tr>
  <tr><td><table cellpadding="0" cellspacing="0" width="100%"><tr><td width="85">Beschreibung:</td><td>{#INPUT description longstring $description}</td></tr></table></td></tr>
  <tr><td><table cellpadding="0" cellspacing="0" width="100%"><tr><td width="43">Betreff:</td><td>{#INPUT subject longstring $subject allowempty=0}</td></tr></table></td></tr>
  <tr><td>Nachricht:<br />{#INPUT message documentwrap $message allowempty=0}</td></tr>
  <tr><td align="center"><br /><br />{#SUBMIT Speichern}</td></tr>
  <tr><td align="center"><br /><br />{#BACKLINK Zur&uuml;ck}</td></tr>
</table>
{#INPUT id hidden $id}
{#INPUT type hidden $type}
{#END}
{#END}

{#SUB viewmessage}
<div  class="card">
	<div class="card-header">
		<{#TPL $text}
	</div>
	<div class="card-body">
	<form class="form-inline">
		 <div class="form-row align-items-center">
			<div class="col-auto">
			{#FORM method=get action=sendmessage.php}

			  {#INPUT frame hidden viewmessage}
			  {#INPUT id hidden $id}
			  {#INPUT user userfromgroup $user param=registered}
			  {#SUBMIT Wechseln}
		   {#END}
			</div>
			<div class="col-auto">
			<a class="btn btn-secondary" role="button" href="sendmessage.php?type={$type}">&Uuml;bersicht</a>
			<a class="btn btn-secondary" role="button" href="sendmessage.php?frame=editmessage&amp;id={$id}">Bearbeiten</a>
			</div>
		</div>
	</form>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12">
				<b>Betreff: </b>"{%subject}"
			</div>
			<div class="col-12">
				<p><b>Nachricht:</b></p> {$message}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB send}
	{#FORM action="sendmessage.php?frame=sending"}
	<table width="100%">
	  <tr><td>{#TPL $text}</td></tr>
	  <tr><td>G&uuml;ltige Variablen: {%vars}<br /><br /></td></tr>
	  <tr><td>
		<table cellpadding="2" cellspacing="0" width="100%">
		  <tr><td width="80">Empf&auml;nger:</td><td>{#INPUT receiver subjects param=$to allowempty=0} <a href="{#EDITURL to=$changeto}">list {$changeto}s</a></td></tr>
		  <tr><td width="80">NachrichtenTyp:</td><td>{#INPUT messagetype dropdown email param=$messagetypes}</td></tr>
		  <tr><td width="80">Betreff:</td><td>{#INPUT subject longstring $subject allowempty=0}</td></tr>
		  <tr><td colspan="2">{#INPUT forceenabled checkbox 1} Die Nachrichten nur an aktivierte Accounts in Gruppen senden.</td></tr>
		  <tr><td colspan="2">{#INPUT sendhtml checkbox 1} Die Nachrichten als HTML-Mail versenden.</td></tr>
		</table>
	  </td></tr>
	  <tr><td>Nachricht:<br />{#INPUT message documentwrap $message allowempty=0}</td></tr>
	  <tr><td align="center"><br /><br />{#SUBMIT Senden}</td></tr>
	  <tr><td align="center"><br /><br />{#BACKLINK Zur&uuml;ck}</td></tr>
	</table>
	{#INPUT id hidden $id}
	{#INPUT type hidden $type}
	{#END}
{#END}

{#SUB sendcheckin}
	{#FORM action="sendmessage.php?frame=sending"}
	<table width="100%">
	  <tr><td>{#TPL $text}</td></tr>
	  <tr><td>G&uuml;ltige Variablen: {%vars}<br /><br /></td></tr>
	  <tr><td>
		<table cellpadding="2" cellspacing="0" width="100%">
		  <tr><td width="80">Empf&auml;nger:</td><td>{#INPUT receiver dropdown param=$userid}</td></tr>
		  <tr><td width="80">NachrichtenTyp:</td><td>{#INPUT messagetype dropdown email param=$messagetypes}</td></tr>
		  <tr><td width="80">Betreff:</td><td>{#INPUT subject longstring $subject allowempty=0}</td></tr>
		  <tr><td colspan="2">{#INPUT forceenabled checkbox 1} Die Nachrichten nur an aktivierte Accounts in Gruppen senden.</td></tr>
		  <tr><td colspan="2">{#INPUT sendhtml checkbox 1} Die Nachrichten als HTML-Mail versenden.</td></tr>
		</table>
	  </td></tr>
	  <tr><td>Nachricht:<br />{#INPUT message documentwrap $message allowempty=0}</td></tr>
	  <tr><td align="center"><br /><br />{#SUBMIT Senden}</td></tr>
	  <tr><td align="center"><br /><br />{#BACKLINK Zur&uuml;ck}</td></tr>
	</table>
	{#INPUT id hidden $id}
	{#INPUT type hidden $type}
	{#END}
{#END}

{#SUB sending}
<b>{%subject}</b> wird versendet:
<br />
<br />
{#TABLE $users}
  {#COL User}{%names}
  {#COL status style="background-image:url(sendmessage.php?frame=sendto&amp;$ids&amp;anticache=$time);"}
{#END}
<p>
  <span class="important">Achtung:</span><br />
  Erst wenn alle Statusfelder ein farbiges Bild geladen haben wurden alle Nachrichten gesendet und du kannst auf "Weiter" klicken!
</p>
<p>
  {#FORM action="sendmessage.php?frame=result"}{#SUBMIT Weiter}{#END}
</p>
{#END}

{#SUB result}
<p>
{#IF $errors}
  Es wurde versucht die Nachrichten zu versenden, allerdings sind dabei Fehler aufgetreten. 
  Bitte entnimm selbst den Fehlermeldungen an welchen Stellen etwas Schiefgelaufen ist.
{#ELSE}
  Alle Nachrichten wurden erfolgreich versandt.
{#END}
</p>
<p>
  <a class="btn btn-secondary btn-md " role="button" href="sendmessage.php"><i class="fas fa-arrow-left"></i> &nbsp; Zur&uuml;ck</a>
</p>
{#END}
