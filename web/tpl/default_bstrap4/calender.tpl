{#SUB default}
  <ul>
    <li>
      <b>Dieses Jahr</b>
      <a href="calender.php?frame=yeargraph">grafisch</a> / 
      <a href="calender.php?frame=listevents&amp;area=year">als Liste</a>
    </li>
    <li>
      <b>Dieser Monat</b>
      <a href="calender.php?frame=monthgraph">grafisch</a> / 
      <a href="calender.php?frame=listevents&amp;area=mon">als Liste</a>
    </li>
    <li>
      <b>Diese Woche</b>
      <a href="calender.php?frame=weekgraph">grafisch</a> / 
      <a href="calender.php?frame=listevents&amp;area=week">als Liste</a>
    </li>
    <li>
      <b>Heute</b>
      <a href="calender.php?frame=daygraph">grafisch</a> / 
      <a href="calender.php?frame=listevents&amp;area=day">als Liste</a>
    </li>
  </ul>
{#END}

{#SUB styles}
  <style type="text/css">
  .calender_empty { background-color: #F8F8F8; }
  .calender_events { background-color: #CCCCCC; }
  .calender_wday:link,.calender_wday:visited,.calender_wday:active { color: #999999; text-decoration:none; }
  .calender_wday_today:link,.calender_wday_today:visited,.calender_wday_today:active { color: red; text-decoration:none; font-weight:bold;}
  .calender_title { font-size:16px; font-weight:bold; }
  .calender_yearweek:link,.calender_yearweek:visited,.calender_yearweek:active { text-decoration:none; color: #336699; font-size: 8px; vertical-align: top;}
  .calender_yearweek_today:link,.calender_yearweek_today:visited,.calender_yearweek_today:active { text-decoration:none; color: red; font-size: 8px; vertical-align: top; font-weight:bold;}
  .calender_yearevent:link,.calender_yearevent:visited,.calender_yearevent:active { text-decoration:none; border-style:solid; border-width:1px;}
  .calender_vgraphscala { border-width:0px; border-style: solid; border-bottom-width: 2px; border-color: #999999; }
  .calender_vgraphweekscala { width:50px; background-color: #F8F8F8; border-width:px; }
  .calender_vgrapharea { background-color: #EEEEEE; border-style: solid; border-width: 1px; border-color: #999999; padding: 2px; }
  .calender_vgraphpoint { border-style: solid; border-width: 0px; border-bottom-width: 2px; border-color: #999999; padding: 2px; vertical-align:bottom;}
  .calender_vgraphday { background-color: #EEEEEE; border-style: solid; border-width: 1px; border-color: #999999; padding: 2px; line-height: 24px; }
  .calender_vgraph { border-style: solid; border-width: 2px; border-color: #999999; width:80%; }
  .calender_vgraphweek { width:100%; background-color: #F8F8F8; border-width:px; }
  .calender_yearaddevent:link,.calender_yearaddevent:visited,.calender_yearaddevent:active { color:green; text-decoration:none; vertical-align:top; font-size: 8px;}
  .calender_addevent:link,.calender_addevent:visited,.calender_addevent:active { color:green; text-decoration:none; vertical-align:top;}
  .calender_today,.calender_today:link,.calender_today:visited,.calender_today:active { color:red; font-weight:bold; }
  </style>
{#END}

{#SUB editevent}
  {#VAR sub=styles}
  {#IF $selecttype}
    <p>
      Bitte w&auml;hle f&uuml;r das neue Ereignis einen Typ aus.<br />
      Er kann nach dem erstellen des Ereignisses nicht mehr ge&auml;ndert werden.
      {#IF $allow_edittypes}
        <br /><br />
        <i>(Typen <a href="calender.php?frame=listtypes">bearbeiten</a>)</i>
      {#END}
    </p>
    <table class="table table-bordered"  align="center"><tr><td>
      {#FOREACH $types}
        <ul>
          <li class="calender_vgrapharea" style="{%style}">
            <a href="calender.php?frame=editevent&amp;type={$id}&amp;day={%day}&amp;backlink={#URL $backlink}"><b>{%caption}</b></a><br />
            {$description}
          </li>
        </ul>
      {#END}
    </td></tr><tr><td align="center">
      <a href="{%backlink}">Zur&uuml;ck</a>
    </td></tr></table>  
  {#ELSE}
    {#FORM event}
    <table class="table table-bordered" >
      <tr>
        <td>Titel:</td><td>{#INPUT caption string $caption allowempty=0}</td>
      </tr>
      <tr>
        <td>Typ:</td><td>{%type.caption}</td>
      </tr>
      {#IF $type.location_editable}
      <tr>
        <td>Ort:</td><td>{#INPUT location string $location}</td>
      </tr>
      {#END}
      <tr>
        {#IF $type.is_point}
          <td>{#WHEN $type.is_wholeday Tag Zeitpunkt}:</td><td>{#INPUT $start_time_n date $start_time allowempty=0}</td>
        {#ELSE}      
          <td>Von:</td><td>{#INPUT $start_time_n date $start_time allowempty=0}</td>
        </tr>
        <tr>
          <td>Bis:</td><td>{#INPUT end_time date $end_time allowempty=0}</td>
        {#END}
      </tr>
      <tr>
        <td>Leserecht</td><td>{#INPUT view_right rights $view_right}</td>
    </tr>
      <tr>
        <td>Schreibrecht</td><td>{#INPUT edit_right rights $edit_right}</td>
      </tr>
      {#IF $type.can_signup}
      <tr>
        <td>Teilnahmerecht</td><td>{#INPUT signup_right rights $signup_right}</td>
      </tr>
      {#END}
      {#IF $type.text1_editable != "never"}
      <tr>
        <td colspan="2">
          {#IF $text1edit}
            {%type.text1_caption}:<br /> {#INPUT text1 text $text1}
          {#ELSE}
            Der Text "{%type.text1_caption}" kann {#WHEN "$type.text1_editable='before'" "nur vor" "erst nach"} dem Ereignis bearbeitet werden.
          {#END}
        </td>
      </tr>    
      {#END}
      {#IF $type.text2_editable != "never"}
      <tr>
        <td colspan="2">
          {#IF $text2edit}
            {%type.text2_caption}:<br /> {#INPUT text2 text $text2}
          {#ELSE}
            Der Text "{%type.text2_caption}" kann {#WHEN "$type.text2_editable='before'" "nur vor" "erst nach"} dem Ereignis bearbeitet werden.
          {#END}
        </td>
      </tr>
      {#END}
      <tr><td>&nbsp;</td></tr>
      <tr>
        <td colspan="2" align="center">{#SUBMIT Speichern}</td>
      </tr>
      <tr><td>&nbsp;</td></tr>
      <tr>
        <td colspan="2" align="center">{#BACKLINK Zur&uuml;ck}</td>
      </tr>
    </table>  
    {#INPUT id hidden $id}
    {#INPUT type_id hidden $type_id}
    {#END}
  {#END}
{#END}

{#SUB yeargraph}
  {#VAR sub=styles}
  <table class="table table-bordered wide_table" cellspacing="1">
    <tr>
      <th colspan="13" class="tdedit">
        <table class="table table-bordered"  width="100%">
          <tr>
            <td class="calender_title" align="left">
              <a href="calender.php?frame=yeargraph&amp;time={$previous}">&lt;&lt;</a>
            </td>
            <td class="calender_title" align="center">
              {$year}
            </td>
            <td align="center" class="calender_title">
              <a href="calender.php?frame=listevents&amp;from={$from}&amp;to={$to}">Liste</a>
            </td>
            <td class="calender_title" align="right">
              <a href="calender.php?frame=yeargraph&amp;time={$next}">&gt;&gt;</a>
            </td>
          </tr>
        </table>
      </th>
    </tr>
    <tr>
      <th class="tdedit"></th>
    {#FOREACH $months $m}
      <th class="tdedit"><a href="calender.php?frame=monthgraph&amp;time={$m}">{#STRFTIME $m §b}</a></th>
    {#END}
    </tr>
    {#FOREACH $items $d $day}  
    <tr>
      <td class="tdcont" align="right"><b>{$day}</b></td>
      {#FOREACH $d}
        {#IF $daytime}
          <td class="{#IF $events}calender_events{#ELSE}tdcont{#END}"{#IF !empty($events)} title="{#FOREACH $events}{$caption}{#MID} {#END}"{#END}>
            <a class="calender_wday{#WHEN $istoday _today}" href="calender.php?frame=daygraph&amp;time={$daytime}">{#STRFTIME $daytime §a}</a>
            {#IF !empty($week)}<a class="calender_yearweek{#WHEN $istoday _today}" href="calender.php?frame=weekgraph&amp;time={$daytime}">{$week}</a>{#END}
            {#FOREACH $events} <a class="calender_yearevent" style="{%style}" href="calender.php?frame=viewevent&amp;id={$id}">&nbsp;&nbsp;</a>{#END}
            {#IF $canadd=="true"}<a class="calender_yearaddevent" href="calender.php?frame=editevent&amp;day={$daytime}">+</a>{#END}
          </td>
        {#ELSE}
          <td class="calender_empty"></td>
        {#END}
      {#END}
    </tr>
    {#END}
  </table>
{#END}

{#SUB monthgraph}
  {#VAR sub=styles}
  <table class="wide_table" cellspacing="1">
    <tr>
      <th colspan="8" class="tdedit">
        <table class="table table-bordered"  width="100%">
          <tr>
            <th class="calender_title" align="left">
              <a href="calender.php?frame=monthgraph&amp;time={$previous}">&lt;&lt;</a>
            </td>
            <th class="calender_title" align="center">
              {#STRFTIME $time §B} <a href="calender.php?frame=yeargraph&amp;time={$time}">{$year}</a>
            </td>
            <td align="center" class="calender_title">
              <a href="calender.php?frame=graph&amp;from={$from}&amp;to={$to}">Graf</a> &middot;
              <a href="calender.php?frame=listevents&amp;area=mon&amp;time={$time}">Liste</a>
            </td>
            <th class="calender_title" align="right">
              <a href="calender.php?frame=monthgraph&amp;time={$next}">&gt;&gt;</a>
            </td>
          </tr>      
        </table>    
      </th>
    </tr>
    <tr>
      <th width="20" class="tdedit">#</th>
    {#FOREACH $days $d}
      <th class="tdedit">{#STRFTIME $d §A}</td>
    {#END}
    </tr>
    {#FOREACH $items key=$week}
    <tr>
      <td width="20" class="calender_empty" align="center"><a href="calender.php?frame=weekgraph&amp;time={$time}">{$week}</a></td>
      {#FOREACH $days}
      <td class="{#IF !$day}calender_empty{#ELSE}tdcont{#END}" height="50" valign="top">
        {#IF $day}
          <a{#WHEN $istoday " class=\"calender_today\""} href="calender.php?frame=daygraph&amp;time={$time}">{$day}</a>
          {#IF $canadd=="true"}<a class="calender_addevent" href="calender.php?frame=editevent&amp;day={$time}">+</a>{#END}
          {#IF !empty($events)}
            <table  class="table table-bordered" cellpadding="0" cellspacing="0" style="margin-top:3px;">
            {#FOREACH $events}
              <tr><td class="calender_vgrapharea" style="{%style}">              
                {#IF !$is_wholeday}
                  {#IF $is_point}
                    {#DATE $start_time H:i}:
                  {#ELSE}
                    {#IF $openopening}&lt;{#ELSE}{#DATE $start_time H:i}{#END} - 
                    {#IF $openend}&gt;{#ELSE}{#DATE $end_time H:i}{#END}:
                  {#END}
                {#END}
                <a href="calender.php?frame=viewevent&amp;id={$id}">{%caption}</a>
              </td></tr>
            {#END}
            </table>
          {#END}
        {#END}
      </td>
      {#END}
    </tr>
    {#END}
  </table>
{#END}

{#SUB vgraph}
  <table class="table table-bordered {$class}" cellpadding="0" cellspacing="0">
    <tr>
    {#IF $showscala}
      <td width="50">
        <table cellspacing="0" cellpadding="3" height="{$pageheight}" width="50">
        {#FOREACH $scala}
          <tr height="{$height}">
            <td class="calender_vgraphscala" valign="bottom" align="center">
              {#IF $link}
                <a href="calender.php?frame=daygraph&amp;time={$end_range}">{%index}</a>
              {#ELSE}{%index}{#END}
            </td>
          </tr>
        {#END}
        </table>
      </td>
    {#END}
    {#IF $showevents}
      {#FOREACH $items $level}
        <td>
          <table class="table table-bordered" cellspacing="0" cellpadding="3" height="{$pageheight}" width="100%">
          {#FOREACH $level}
            <tr height="{$height}">
            {#IF !empty($caption)}
              <td class="calender_vgraph{#WHEN $is_point point area}" valign="top" style="white-space:nowrap;{%style}">
                {#IF $is_point}
                  {#DATE $start_time H:i}
                {#ELSE}
                  {#IF $openopening}&lt;{#ELSE}{#DATE $start_time H:i}{#END} -
                  {#IF $openend}&gt;{#ELSE}{#DATE $end_time H:i}{#END}
                {#END}
                <a href="calender.php?frame=viewevent&amp;id={$id}">{%caption}</a>
              </td>
            {#ELSE}
              <td></td>
            {#END}          
            </tr>      
          {#END}
          </table>
        </td>
      {#END}
      {#IF empty($items)}<td height="{$pageheight}"></td>{#END}
    {#END}
    </tr>
  </table>
{#END}

{#SUB dayevents}
  {#FOREACH $events}
    <a class="calender_vgraphday" style="{%style}" title="{%type}" href="calender.php?frame=viewevent&amp;id={$id}">{%caption}</a>
  {#END}
{#END}

{#SUB graph}
  {#VAR sub=styles}
  {! #VAR sub=vgraph}
  {#FORM redirgraph}
  Vom {#INPUT from date $from} bis zum {#INPUT to date $to}: {#SUBMIT anzeigen}
{#END}
  <p>
    Von <b>{#STRFTIME $from §A}</b>, dem <b>
    <a href="calender.php?frame=daygraph&amp;time={$from}">{#DATE $from j}</a>.
    <a href="calender.php?frame=monthgraph&amp;time={$from}">{#STRFTIME $from §B}</a>
    <a href="calender.php?frame=yeargraph&amp;time={$from}">{#DATE $from Y}</a></b> 
    bis <b>{#STRFTIME $to §A}</b>, dem <b>
    <a href="calender.php?frame=daygraph&amp;time={$to}">{#DATE $to j}</a>.
    <a href="calender.php?frame=monthgraph&amp;time={$to}">{#STRFTIME $to §B}</a>
    <a href="calender.php?frame=yeargraph&amp;time={$to}">{#DATE $to Y}</a></b> 
  </p>
  {#VAR $vgraph vgraph class="calender_vgraph"}
{#END}

{#SUB daygraph}
  {#VAR sub=styles}
  <table class="table table-bordered" width="80%" cellspacing="0" cellpadding="6">
    <tr>
      <td align="left" class="calender_title">
        <a href="calender.php?frame=daygraph&amp;time={$previous}">&lt;&lt;</a>
      </td>
      <td align="left" class="calender_title">
        <span{#WHEN $istoday " class=\"calender_today\""}>{#STRFTIME $from §A}, der {#DATE $from j}.</span>
        <a{#WHEN $istoday " class=\"calender_today\""} href="calender.php?frame=monthgraph&amp;time={$from}">{#STRFTIME $from §B}</a>
        <a{#WHEN $istoday " class=\"calender_today\""} href="calender.php?frame=yeargraph&amp;time={$from}">{#DATE $from Y}</a>
      </td>
      <td align="center" class="calender_title">
        {#IF $canadd=="true"}<a href="calender.php?frame=editevent&amp;day={$from}">neu</a> &middot;{#END}
        <a href="calender.php?frame=graph&amp;from={$from}&amp;to={$to}">Graf</a> &middot;
        <a href="calender.php?frame=listevents&amp;from={$from}&amp;to={$to}">Liste</a> &middot;
        <a href="calender.php?frame=daygraph&amp;time={$time}">Heute</a> &middot;
        <a href="calender.php?frame=weekgraph&amp;time={$from}">Woche</a>
      </td>
      <td align="right" class="calender_title">
        <a href="calender.php?frame=daygraph&amp;time={$next}">&gt;&gt;</a>
      </td>
    </tr>
    <tr>
      <td colspan="4" style="border-color:#999999; border-top-width:2px; border-left-width:2px; border-right-width:2px; border-style:solid; border-bottom-width:0px;">
        <b>{#VAR $dayevents dayevents}</b>
      </td>
    </tr>
  </table>
  {#VAR $vgraph vgraph class="calender_vgraph"}
{#END}

{#SUB weekgraph}
  {#VAR sub=styles}
  <table cellspacing="1" class="table table-bordered wide_table">
    <tr>
      <th class="tdedit" colspan="8">
        <table class="table table-bordered" width="100%">
          <tr>
            <td align="left" class="calender_title">
              <a href="calender.php?frame=weekgraph&amp;time={$previous}">&lt;&lt;</a>
            </td>
            <td align="center" class="calender_title">
              {#IF $diffyears}
                <a href="calender.php?frame=monthgraph&amp;time={$from}">{#STRFTIME $from "§B"}</a>
                <a href="calender.php?frame=yeargraph&amp;time={$from}">{#STRFTIME $from "§Y"}</a> und
                <a href="calender.php?frame=monthgraph&amp;time={$to}">{#STRFTIME $to "§B"}</a>
                <a href="calender.php?frame=yeargraph&amp;time={$to}">{#STRFTIME $to "§Y"}</a>
              {#ELSEIF $diffmonth}
                <a href="calender.php?frame=monthgraph&amp;time={$from}">{#STRFTIME $from "§B"}</a> und
                <a href="calender.php?frame=monthgraph&amp;time={$to}">{#STRFTIME $to "§B"}</a>
                <a href="calender.php?frame=yeargraph&amp;time={$to}">{#STRFTIME $to "§Y"}</a>
              {#ELSE}
                <a href="calender.php?frame=monthgraph&amp;time={$to}">{#STRFTIME $to "§B"}</a>
                <a href="calender.php?frame=yeargraph&amp;time={$to}">{#STRFTIME $to "§Y"}</a>
              {#END}
            </td>
            <td align="center" class="calender_title">
              <a href="calender.php?frame=graph&amp;from={$from}&amp;to={$to}">Graf</a> &middot;
              <a href="calender.php?frame=listevents&amp;from={$from}&amp;to={$to}">Liste</a>
            </td>
            <td align="right" class="calender_title">
              <a href="calender.php?frame=weekgraph&amp;time={$next}">&gt;&gt;</a>
            </td>          
          </tr>
        </table>
      </th>
    </tr>
    <tr>
      <th class="tdedit" width="1"></th>
      {#FOREACH $items}
        <th class="tdedit">
          {#IF $istoday}<span class="calender_today">{#STRFTIME $time §A}</span>{#ELSE}{#STRFTIME $time §A}{#END}
          {#IF $canadd=="true"}<a class="calender_addevent" href="calender.php?frame=editevent&amp;day={$time}">+</a>{#END}<br />
          <a{#WHEN $istoday " class=\"calender_today\""} href="calender.php?frame=daygraph&amp;time={$time}">{#DATE $time j}. {#STRFTIME $time "§b §Y"}</a>
        </th>
      {#END}
    </tr>
    {#IF $showdayevents}
    <tr>
      <td class="tdcont" width="1">&nbsp;</td>
      {#FOREACH $items}
        <td class="tdcont" style="padding:4px;">{#VAR $dayevents dayevents}</td>
      {#END}
    </tr>  
    {#END}
    <tr>
      <td width="1" style="padding:0px;" valign="top">{#VAR $scala vgraph class="calender_vgraphweekscala"}</td>
      {#FOREACH $items}
        <td style="padding:0px;" valign="top">{#VAR $vgraph vgraph class="calender_vgraphweek"}</td>
      {#END}
    </tr>
  </table>
{#END}

{#SUB listevents}
  {#VAR sub=styles}
  <table class="table table-bordered">
    <tr>
      <td class="calender_title" align="left">
        Vom <a href="calender.php?frame=daygraph&amp;time={$from}">{#DATE $from j}.</a>
        <a href="calender.php?frame=monthgraph&amp;time={$from}">{#STRFTIME $from §b}</a>
        <a href="calender.php?frame=yeargraph&amp;time={$from}">{#STRFTIME $from §Y}</a>
        {#DATE $from H:i}
      </td>
      <td class="calender_title" align="right">
        bis zum <a href="calender.php?frame=daygraph&amp;time={$to}">{#DATE $to j}.</a>
        <a href="calender.php?frame=monthgraph&amp;time={$to}">{#STRFTIME $to §b}</a>
        <a href="calender.php?frame=yeargraph&amp;time={$to}">{#STRFTIME $to §Y}</a>
        {#DATE $to H:i}
      </td>
    </tr>
  </table>
  {#TABLE $events}
    {#COL Titel %caption style=$style class=calender_vgrapharea}<a href="calender.php?frame=viewevent&amp;id={$id}"><b>{%caption}</b></a>
    {#COL Zeit $start_time}
      {#IF $is_point}
        <a href="calender.php?frame=daygraph&amp;time={$start_time}">{#DATE $start_time j}.</a>
        <a href="calender.php?frame=monthgraph&amp;time={$start_time}">{#STRFTIME $start_time §b}</a>
        <a href="calender.php?frame=yeargraph&amp;time={$start_time}">{#STRFTIME $start_time §Y}</a>
        {#IF !$is_wholeday}- {#DATE $start_time H:i} Uhr{#END}
      {#ELSE}
        {!---- termin an einem tag ----}
        {#IF date("Yz",$start_time) == date("Yz",$end_time)}
          <a href="calender.php?frame=daygraph&amp;time={$start_time}">{#DATE $start_time j}.</a>
          <a href="calender.php?frame=monthgraph&amp;time={$start_time}">{#STRFTIME $start_time §b}</a>
          <a href="calender.php?frame=yeargraph&amp;time={$start_time}">{#STRFTIME $start_time §Y}</a>
          {#IF !$is_wholeday}- {#DATE $start_time H:i} - {#DATE $end_time H:i} Uhr{#END}
        {!---- termin in einem monat ----}
        {#ELSEIF date("Ym",$start_time) == date("Ym",$end_time)}
          <a href="calender.php?frame=daygraph&amp;time={$start_time}">{#DATE $start_time j}.</a> - 
          <a href="calender.php?frame=daygraph&amp;time={$end_time}">{#DATE $end_time j}.</a>
          <a href="calender.php?frame=monthgraph&amp;time={$start_time}">{#STRFTIME $start_time §b}</a>
          <a href="calender.php?frame=yeargraph&amp;time={$start_time}">{#STRFTIME $start_time §Y}</a>
      {!---- termin in einem jahr ----}
        {#ELSEIF date("Y",$start_time) == date("Y",$end_time)}
          <a href="calender.php?frame=daygraph&amp;time={$start_time}">{#DATE $start_time j}.</a>
          <a href="calender.php?frame=monthgraph&amp;time={$start_time}">{#STRFTIME $start_time §b}</a> -
          <a href="calender.php?frame=daygraph&amp;time={$end_time}">{#DATE $end_time j}.</a>
          <a href="calender.php?frame=monthgraph&amp;time={$end_time}">{#STRFTIME $end_time §b}</a>
          <a href="calender.php?frame=yeargraph&amp;time={$start_time}">{#STRFTIME $start_time §Y}</a>
        {!---- ansonsten ----}
        {#ELSE}
          <a href="calender.php?frame=daygraph&amp;time={$start_time}">{#DATE $start_time j}.</a>
          <a href="calender.php?frame=monthgraph&amp;time={$start_time}">{#STRFTIME $start_time §b}</a>
          <a href="calender.php?frame=yeargraph&amp;time={$start_time}">{#STRFTIME $start_time §Y}</a> -
          <a href="calender.php?frame=daygraph&amp;time={$end_time}">{#DATE $end_time j}.</a>
          <a href="calender.php?frame=monthgraph&amp;time={$end_time}">{#STRFTIME $end_time §b}</a>
          <a href="calender.php?frame=yeargraph&amp;time={$end_time}">{#STRFTIME $end_time §Y}</a>
        {#END}
      {#END}    
    {#COL Ort %location}
    {#COL Typ %type}
    {#COL Leserecht}{#RIGHT $view_right view}
    {#COL Schreibrecht}{#RIGHT $edit_right edit}
    {#FRAME edit editevent right=$edit_right}
    {#OPTION L&ouml;schen deleteevent}
  {#END}
{#END}

{#SUB viewevent}
  {#VAR sub=styles}
  <table cellspacing="1" class="table table-bordered">
    <tr>
      <th align="center" colspan="2" class="calender_vgrapharea" style="{%style}">
      {%type}: {%caption}
      </th>
    </tr>
    {#IF $is_point}
    <tr>
      <td class="tdedit" style="text-align:right;">{#WHEN $is_wholeday Tag Zeitpunkt}:</td>
      <td class="tdcont">
        <a href="calender.php?frame=daygraph&amp;time={$start_time}">{#DATE $start_time j}.</a>
        <a href="calender.php?frame=monthgraph&amp;time={$start_time}">{#STRFTIME $start_time §B}</a>
        <a href="calender.php?frame=yeargraph&amp;time={$start_time}">{#STRFTIME $start_time §Y}</a>
        {#IF !$is_wholeday}, {#DATE $start_time H:i} Uhr{#END}
      </td>
    </tr>
    {#ELSE}
      {!---- termin an einem tag ----}
      {#IF date("Yz",$start_time) == date("Yz",$end_time)}
      <tr>
        <td class="tdedit" style="text-align:right;">{#WHEN $is_wholeday Tag Zeitspanne}:</td>
        <td class="tdcont">
          <a href="calender.php?frame=daygraph&amp;time={$start_time}">{#DATE $start_time j}.</a>
          <a href="calender.php?frame=monthgraph&amp;time={$start_time}">{#STRFTIME $start_time §B}</a>
          <a href="calender.php?frame=yeargraph&amp;time={$start_time}">{#STRFTIME $start_time §Y}</a>
          {#IF !$is_wholeday}, {#DATE $start_time H:i} bis {#DATE $end_time H:i} Uhr{#END}
        </td>
      </tr>
      {#ELSE}
      <tr>
        <td class="tdedit" style="text-align:right;">Von:</td>
        <td class="tdcont">
          <a href="calender.php?frame=daygraph&amp;time={$start_time}">{#DATE $start_time j}.</a>
          <a href="calender.php?frame=monthgraph&amp;time={$start_time}">{#STRFTIME $start_time §B}</a>
          <a href="calender.php?frame=yeargraph&amp;time={$start_time}">{#STRFTIME $start_time §Y}</a>
          {#IF !$is_wholeday}, {#DATE $start_time H:i} Uhr {#END}
        </td>
      </tr>
      <tr>  
        <td class="tdedit" style="text-align:right;">Bis:</td>
        <td class="tdcont">
          <a href="calender.php?frame=daygraph&amp;time={$end_time}">{#DATE $end_time j}.</a>
          <a href="calender.php?frame=monthgraph&amp;time={$end_time}">{#STRFTIME $end_time §B}</a>
          <a href="calender.php?frame=yeargraph&amp;time={$end_time}">{#STRFTIME $end_time §Y}</a>
          {#IF !$is_wholeday}, {#DATE $end_time H:i} Uhr{#END}
        </td>
      </tr>
      {#END}
    {#END}
    {#IF !$is_point}
    <tr>
      <td class="tdedit" style="text-align:right;">Dauer:</td>
      <td class="tdcont">{%span}</td>
    </tr>
    {#END}
    {#IF !empty($location)}
    <tr>
      <td class="tdedit" style="text-align:right;">Ort:</td>
      <td class="tdcont">{%location}</td>
    </tr>
    {#END}
    {#IF $user_can_signup}
      {#IF $user_is_signedup}
      <tr>
        <td class="tdedit" style="text-align:right;">Vom Event abmelden:</td>
        <td class="tdcont">{#ACTION Abmelden signofffromevent params=array("event_id=$id")}</td>
      </tr>
      {#ELSE}
      <tr>
        <td class="tdedit" style="text-align:right;">Zum Event anmelden:</td>
        <td class="tdcont">{#ACTION Anmelden signup4event params=array("event_id=$id")}</td>
      </tr>
      {#END}
    {#END}
    {#IF !empty($text1)}
    <tr>
      <td class="tdedit" align="center" colspan="2"><b>{%text1_caption}</b></td></td>
    </tr>
    <tr>
      <td class="tdcont" colspan="2" style="padding:6px;">{$text1}</td>
    </tr>
    {#END}
    {#IF !empty($text2)}
    <tr>
      <td class="tdedit" align="center" colspan="2"><b>{%text2_caption}</b></td>
    </tr>
    <tr>
      <td class="tdcont" colspan="2" style="padding:6px;">{$text2}</td>
    </tr>
    {#END}
    {#IF $allow_edit}
    <tr>
      <td class="tdcont" align="center" colspan="2" style="color:#AAAAAA; font-size:10px;"><i>
        zuletzt <a href="calender.php?frame=editevent&amp;id={$id}">bearbeitet</a> von {%name} am {#DATE $last_change_time}
        &middot; <a href="calender.php?action=deleteevent&amp;ids[]={$id}&amp;confirmation={#URL Soll das Event wirklich gel&ouml;scht werden?}&amp;confirmation_uri={#URL $referer}">L&ouml;schen</a>
      </i></td>
    </tr>
    {#END}
  </table>
  {#IF $user_can_signup}
    <br />
    <h3 align="center">Teilnehmerliste</h4>
    {#IF empty($event_participants)}
      <center><b>Bis jetzt hat sich noch niemand angemeldet!</b></center>
    {#ELSE}
    {#FOREACH $event_participants $p}
      {#FOREACH $p}{$name}{#MID},&nbsp;{#END}
    {#END}
    {#END}
  {#END}
{#END}

{!----------------------------- Typen anzeigen und bearbeiten -------------------------}

{#SUB listtypes}
  {#VAR sub=styles}
  <table class="table table-bordered"><tr><td class="calender_title">
    Heute: <a href="calender.php?frame=daygraph">{#DATE $time j}.</a>
    <a href="calender.php?frame=monthgraph">{#STRFTIME $time §b}</a>
    <a href="calender.php?frame=yeargraph">{#STRFTIME $time §Y}</a>
  </td></tr></table>
  {#TABLE $items}
    <a href="calender.php?frame=edittype">add</a>
    {#COL Titel $caption style=$style class=calender_vgrapharea}{%caption}
    {#COL Genauigkeit<br/>Typ}{#WHEN $is_wholeday Tag Sekunde}<br />{#WHEN $is_point Zeitpunkt Zeitspanne}
    {#COL Leserecht<br/>Schreibrecht}{#RIGHT $view_right view}<br />{#RIGHT $edit_right edit}
    {#COL Texte}
      {#IF $text1_editable != 'never'}{%text1_caption} {#WHEN "$text1_editable != 'always'" "(<i>$text1_editable</i>)"}<br />{#END}
      {#IF $text2_editable != 'never'}{%text2_caption} {#WHEN "$text2_editable != 'always'" "(<i>$text2_editable</i>)"}{#END}
    {#COL Ort $location_editable}{#WHEN $location_editable Ja Nein}
    {#COL Anmeldung}{#WHEN $can_signup Ja Nein}
    {#FRAME edit edittype right=$editright}
    {#ACTION del deletetype condition="empty($name)" right=$editright confirmation="Soll der Typ wirklich gel&ouml;scht werden?"}
  {#END}
  <p>
    Vordefinierte Typen k&ouml;nnen nicht gel&ouml;scht werden. Auch lassen sich nur ihr Style und ihre Rechte bearbeiten.
  </p>
{#END}

{#SUB edittype}
  {#FORM type}
  <table width="70%" class="table table-bordered">
    <tr><td align="right">Titel:</td><td>{#INPUT caption string $caption enabled=$editable}</td></tr>
    <tr><td align="right">ist Ganztags:</td><td>{#INPUT is_wholeday checkbox $is_wholeday enabled=$editable}</td></tr>
    <tr><td align="right">ist Zeitpunkt:</td><td>{#INPUT is_point checkbox $is_point enabled=$editable}</td></tr>
    <tr><td align="right">Anmeldung m&ouml;glich:</td><td>{#INPUT can_signup checkbox $can_signup enabled=$editable}</td></tr>
    <tr><td align="right">Leserecht:</td><td>{#INPUT view_right rights $view_right}</td></tr>
    <tr><td align="right">Schreibrecht:</td><td>{#INPUT edit_right rights $edit_right}</td></tr>
    <tr><td align="right">Text1 Titel:</td><td>{#INPUT text1_caption string $text1_caption enabled=$editable}</td></tr>
    <tr><td align="right">Text2 Titel:</td><td>{#INPUT text2_caption string $text2_caption enabled=$editable}</td></tr>
    <tr><td align="right">Text1 bearbeitbar:</td><td>{#INPUT text1_editable dropdown $text1_editable enabled=$editable param=$edittypes}</td></tr>
    <tr><td align="right">Text2 bearbeitbar:</td><td>{#INPUT text2_editable dropdown $text2_editable enabled=$editable param=$edittypes}</td></tr>
    <tr><td align="right">Ort bearbeitbar:</td><td>{#INPUT location_editable checkbox $location_editable enabled=$editable}</td></tr>
    <tr><td align="right">Style:</td><td>{#INPUT style longstring $style}</td></tr>
    <tr><td align="right">Beschreibung:</td><td>{#INPUT description text $description enabled=$editable}</td></tr>
    <tr><td colspan="2">&nbsp;</td></tr>
    <tr><td colspan="2" align="center">{#SUBMIT Speichern}</td></tr>
    <tr><td colspan="2">&nbsp;</td></tr>
    <tr><td colspan="2" align="center">{#BACKLINK Zur&uuml;ck}</td></tr>
  </table>
  {#INPUT id hidden $id}
  {#END}
{#END}