{#SUB default}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
			<p>Pfad: {$path}</p>
			<div style="border: 1px solid grey;">
				{#FOREACH array=$cats}
					<div style="border: 1px solid {#IF $parent_id == $parent}#DEDEDE{#ELSE}#ECECEC{#END}; margin: 0; height: 68px; background-color: {#IF $parent_id == $parent}#DEDEDE{#ELSE}#ECECEC{#END}; text-align: left;">
						<div style="float: left; margin: 1px 10px 0 {#IF $parent_id == $parent}5{#ELSE}40{#END}px;"><img src="{#IF $albums != 0}{#STATICFILE images/gallery/gallery_folder_alb.png}{#ELSE}{#STATICFILE images/gallery/gallery_folder.png}{#END}" width="64px" alt="Ordner" /></div>
						<p style="padding: 0; margin: 5px 0 4px 10px;"><b>
							{#IF $subcats == 0 && $albums == 0}
								{$name}
							{#ELSEIF isset($albid)}
								<a href="?frame=pics&amp;albumid={$albid}">{$name}</a>
							{#ELSE}
								<a href="{#IF $subcats != 0}?parentid={$id}{#ELSE}?frame=albums&amp;catid={$id}{#END}">
									{$name}
								</a>
							{#END}</b>
						</p>
						<p style="font-size: 90%; padding: 0; margin: 0 0 5px 10px;">{$description}</p>
					</div>
				{#ELSE}
					<p style="margin-left: 20px;">Es sind noch keine Kategorien vorhanden!</p>
				{#END}
			</div>
			<p>Insgesamt gibt es {$countpics} Bilder verteilt auf {$countalbums} Alben in {$countcats} Kategorien.</p>
			{#IF !is_null($pparent)}<p align="center"><a href="?parentid={$pparent}">Zur&uuml;ck</a></p>{#END}
			{#IF $admin}<p align="center"><a href="?frame=admin">Verwaltung</a></p>{#END}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB albums}
	<div style="position: relative; border: 1px solid gray; background-color: #ECECEC;">
	{#FOREACH array=$albums var=$row}
		{#FOREACH array=$row var=$album}
			{#WITH $album}
				<!-- <div style="text-align: center; position: relative; float: left; margin: 8px; width: {$bwidth}px; height: {$bheight}px; background-color: lightgray; border: 1px solid gray;">
					<p style="font-size: 12px;"><b>Album:</b> {$name}</p>
					<a href="?frame=Pics&amp;albumid={$id}" style="border: none;">
						<img src="{#IF is_null($titlepic)}{#STATICFILE images/gallery/no_picture.jpg}{#ELSE}{$cthumbpath}/{$id}/{$titlepic}{#END}" alt="{$name}" border="0" style="max-width: {$thumbmaxwidth}px; max-height: {$thumbmaxheight}px; margin: 5px;"/>
					</a>
				</div> -->
				<div style="position: relative; width: 33%; float: left; text-align: center;" width="33%">
					<table border="0" cellspacing="2px">
						<tr>
							<td colspan="2" style="padding-left: 7px;">
								<b>Album: {$name}</b>
							</td>
						</tr>
						<tr>
							<td style="padding-left: 2px;">
								<a href="?frame=Pics&amp;albumid={$id}" style="border: none;">
									<img src="{#IF is_null($titlepic)}{#STATICFILE images/gallery/no_picture.jpg}{#ELSE}{$cthumbpath}/{$id}/{$titlepic}{#END}" alt="{$name}" border="0" style="max-width: {$thumbmaxwidth}px; max-height: {$thumbmaxheight}px; margin: 5px;"/>
								</a>
							</td>
							<td style="padding-left: 2px;">
								<p>{$piccount} Bilder</p>
								<p style="font-size: 10px;">
									Letzte Aktualisierung:<br />
									{$mtime}
								</p>
							</td>
						</tr>
					</table>
				</div>
			{#END}
		{#END}
		<div style="clear: both;"></div>
	{#END}
	</div>
	<p align="center">
		<a href="?parentid={$cat.parent_id}">Zurück</a>
	</p>
{#END}

{#SUB pics}
	<p style="font-size: 12px; margin: 0;">Insgesamt {$piccount} Bilder</p>
	{#FOREACH array=$rows var=$row}
		{#FOREACH array=$row var=$pic}
			{#WITH $pic}
				<div style="text-align: center; position: relative; float: left; margin: 8px; width: {$bwidth}px; height: {$bheight}px; background-color: lightgray; border: 1px solid gray;">
					<a href="?frame=showPic&amp;picid={$id}" style="border: none;">
						<img src="{$cthumbpath}/{$albumid}/{$filename}" alt="{$name}" border="0" style="max-width: {$thumbmaxwidth}px; max-height: {$thumbmaxheight}px; margin: 5px;"/>
					</a>
					<div style="font-size: 10px; position: absolute; width: 100%; left: 0; bottom: 5px;">
						<b>{$name}</b><br />
						{$clicks} Zugriffe
					</div>
				</div>
			{#END}
		{#END}
		<div style="clear: both;"></div>
	{#END}
	<p align="center">{#IF isset($less)}<a href="?frame=Pics&amp;albumid={$albumid}&amp;page={$less}">Vorherige</a> - {#END}Seite {$page} von {$pages}{#IF isset($next)} - <a href="?frame=Pics&amp;albumid={$albumid}&amp;page={$next}">N&auml;chste</a>{#END}</p>
	<p align="center"><a href="?frame=albums&amp;catid={$pcatid}">Zur&uuml;ck</a></p>
{#END}

{#SUB showpic}
		<table align="center" cellspacing="20px;" cellpadding="0">
			<tr>
				<td>
					{#HTMLACTION right="gallery_admin" action="Rotate" params="array(rotate=270 picid=$id)" style="border:0; height:33px; width: 33px; background-color: white; background-image:url(tpl/default/images/gallery/gallery_rotate_left.png);"}
					{#END}
				</td>
				{#IF !$begin}
				<td>
					<a href="gallery.php?frame=showpic&amp;picid={$prev}" title="vorherigeDateianzeigen"> 
						<img src="tpl/default/images/prev.gif" border="0" align="middle" alt="vorheriges Bild anzeigen" /> 
					</a>
				</td>
				{#END} 
				{#IF !$end}
				<td>
					<a href="gallery.php?frame=showpic&amp;picid={$next}" title="naechsteDateianzeigen"> 
						<img src="tpl/default/images/next.gif" border="0" align="middle" alt="naechstes Bild anzeigen" /> 
					</a>
				</td>
				{#END}
				<td>
					{#HTMLACTION right="gallery_admin" action="Rotate" params="array(rotate=90 picid=$id)" style="border:0; height:33px; width: 33px; background-color: white; background-image:url(tpl/default/images/gallery/gallery_rotate_right.png);"}
					{#END}
				</td>
			</tr>
		</table>
		<br />
		<div align="center">
			<img src="{%cpicpath}/{$album_id}/{$filename}" border="2" style="max-height:{%picmaxheight}px; max-width:{%picmaxwidth}px;" alt="{$filename}">
		</div>
		{#IF $hits == "Y"}
			<div align="center" style="margin: 20px;">Das Bild wurde <b>{$clicks}x</b> angesehen.</div>
		{#END}
		<p align="center"><a href="{$backlink}">Zur&uuml;ck</a>
{#END}

<!-- ************* admin frames ************* -->

{#SUB admin}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
			<h2 style="margin-bottom: 15px;" align="center">Kategorien verwalten</h2>
			<div style="margin: 0 auto; width: 75%;">
				<p>Pfad: {$path}</p>
				{#TABLE items=$cats width="100%"}<a href="?frame=ModCat&amp;pid={$pid}">Unterkategorie hinzufügen</a>
					{#COL caption="Name" val=%name} {#IF !%albums > 0}<a href="gallery.php?frame=admin&pid={%id}">{%name}</a>{#ELSE}{%name}{#END}
					{#COL caption="Beschreibung" val=%description}
					{#FRAME caption="%albums Alben - verwalten" frame="ManageAlbums" condition="$subcats == 0"}
					{#ACTION /\ movecat params="array(id=$id)" condition="!$first" align="center"}
					{#FRAME caption="edit" frame="ModCat" align="center"}
					{#ACTION del delcat params="array(id=$id)" condition="%recdeep == 0 && !%albums > 0" confirmation="Soll die Kategorie wirklich gel&ouml;scht werden?" align="center"}
				{#END}
				<p><b>Hinweis:</b> Eine Kategorie kann entweder Unterkategorien oder Alben enthalten. Beides gleichzeitig ist nicht möglich.</p>
				<p>{#IF $ppid >= 0}<a href="?frame=admin&amp;pid={$ppid}">Zur&uuml;ck</a>{#ELSE}<a href="?">Zur Galerie</a>{#END}</p>
			</div>
			</div>
		</div>
	</div>
</div>
{#END}

<!-- ************* categorie frames ************* -->

{#SUB ModCat}
	{#FORM cat}
		{#INPUT parentid hidden $pcatid}
		{#INPUT catid hidden $catid}
		<table cellspacing="10px">
			<tr>
				<td>Name:</td>
				<td>{#INPUT name string $name}</td>
			</tr>
			<tr>
				<td>Beschreibung:</td>
				<td>{#INPUT desc string $desc}</td>
			</tr>
			<tr>
				<td>Oberkategorie:</td>
				<td>{$pcatid} - {$pcatname}</td>
			</tr>
			<tr>
				<td colspan="2">
					{#SUBMIT Speichern}
				</td>
			</tr>
			<tr>
				<td>{#BACKLINK Zur&uuml;ck}</td>	
			</tr>
		</table>
	{#END}
{#END}

{#SUB RemCat} 
	{#IF $page=="1"}
		<form name="delcat" action="gallery.php?frame=RemCat" method="post">
			<p align="center">Hier kannst Du eine Kategorie l&ouml;schen. Bitte w&auml;hle die Kategorie aus:<br />
				<br />
				<input type="hidden" name="page" value="2"> 
				<select name="cat">
					{#FOREACH $cats}
						<option value="{%id}">{%name}</option>
					{#END}
				</select> 
				<br />
				<br />
				<input type="submit" value="L&ouml;schen">
			</p>
		</form>
	{#END} 
	{#IF $page=="2"} 
		Erfolgreich gel&ouml;scht ! 
	{#END} 
{#END}

<!-- ************* picture frames ************* -->

{#SUB AddPics}
	Bitte kopiere nun die Bilder in den folgenden Ordner: <br /> <font color="blue">/{%picpath}/{$albumid}/</font>
	<br /> <br /> Die Thumbnails (H&ouml;he und Breite max 100px, selber
	Name wie Originalbild) in den Ordner: <br />
	<font color="blue">/{%thumbpath}/{$albumid}/</font> <br /> <br />
	Hinweis: Die Thumbnails werden automatisch erstellt, falls sie nicht vorhanden sind.<br />
	<form name="addpics" action="gallery.php?frame=AddPicsStep2&amp;albumid={$albumid}" method="post">
		<input type="submit" value="Weiter">
	</form>
	<br /><br />
	<p align="center"><a href="?frame=ManageAlbums&amp;id={$catid}">Zur&uuml;ck</a></p>	
{#END}

{#SUB AddPicsStep2}
	{#TABLE $files}
		{#COL "Dateiname" $id} {$id}
		{#COL "Thumbnail" $id} <img src="{$thumbpath}/{$albumid}/{$id}" alt="N/A" />
		{#INPUT albumid hidden $albumid}
		{#OPTION "Hinzufügen" addpics}
	{#END}
	<p align="center"><a href="?frame=ManageAlbums&amp;id={$catid}">Zur&uuml;ck</a></p>	
{#END}

{#SUB rempics} 
	{#TABLE $bilder}
		{#COL "ID" $id} {$id} 
		{#COL "Name" $id} {$name}
		{#COL "Thumbnail" $id} <img src="{%thumbpath}/{$albumid}/{%filename}" alt="{%filename}" />
		{#ACTION /\ moveup params="$id" condition="$id != $firstid"}
		{#ACTION right="gallery_admin" action="Rotate" params="array(rotate=270 picid=$id)" buttonattrs="array(style=background-color:transparent;border:0;width:33px;height:33px;background-image:url(tpl/default/images/gallery/gallery_rotate_left.png);)"}
		{#ACTION right="gallery_admin" action="Rotate" params="array(rotate=90 picid=$id)" buttonattrs="array(style=background-color:transparent;border:0;width:33px;height:33px;background-image:url(tpl/default/images/gallery/gallery_rotate_right.png);)"}
		{#COL "Edit" $id} <a href="gallery.php?frame=editpic&picid={%id}">edit</a>
		{#OPTION "L&ouml;schen" rempics}
		{#INPUT albumid hidden $albumid}
	{#END} 
	<br />
	<br />
	<p align="center"><a href="gallery.php?frame=ManageAlbums&amp;id={$catid}">Zur&uuml;ck</a></p>
{#END}

{#SUB editpic}
	{#WITH $pic}
		{#FORM pic}
			<p align="center">
				Name: {#INPUT picname string $name}<br />
				{#INPUT picid hidden $id}<br />
				{#SUBMIT Speichern}<br />
				{#BACKLINK Zur&uuml;ck} 
			</p>
		{#END}
	{#END} 
{#END}


<!-- ************* album frames ************* -->

{#SUB ManageAlbums}
	{#TABLE $alben} <a href="gallery.php?frame=AddAlbum&amp;catid={$catid}">Album hinzufügen</a>
		{#COL "Name" $id} {$name}
		{#COL "Titelbild" $id} {$titlepic}
		{#ACTION /\ moveupalbum params="$id" condition="$id != $firstid"}
		{#COL "Bilder hinzufügen" $id} <a href="gallery.php?frame=addPics&amp;albumid={%id}">Bilder hinzufügen</a>
		{#COL "Bilder verwalten" $id} <a href="gallery.php?frame=RemPics&albumid={%id}">Bilder verwalten</a>
		{#COL "Edit" $id} <a href="gallery.php?frame=ModAlbum&albumid={%id}">edit</a>
		{#OPTION "L&ouml;schen" remalbums}
	{#END}
	<br />
	<br />
	<p align="center"><a href="gallery.php?frame=admin&pid={$pid}">Zur&uuml;ck</a></p>
{#END}

{#SUB AddAlbum}
	{#FORM album}
		{#INPUT sent hidden "true"}
		<table cellspacing="20">
			<tr>
				<td>Name:</td>
				<td>{#INPUT albumname string}</td>
			</tr>
			<tr>
				<td>Kategorie:</td>
				<td>{#INPUT catid hidden $catid} {$catname}</td>
			</tr>
			<tr>
				<td colspan="2">
					{#SUBMIT Speichern}
				</td>
			</tr>
			<tr>
				<td colspan="2">
					{#BACKLINK Zur&uuml;ck} 
				</td>
			</tr>
		</table>
	{#END}
{#END}

{#SUB ModAlbum}
	<script language="JavaScript" type="text/JavaScript">
		var Pic = new Array();
		Pic[0] = "{#STATICFILE images/gallery/no_picture.jpg}";
		{#FOREACH array=$pics var=$value key=$key}
			Pic[{$key}] = "{$cthumbpath}/{$id}/{$value}";
		{#END}

		function ChangeThumb(index) {
			document.images.preview.src = Pic[index];
			document.forms[0].titlepic.value = index;
		}
	</script>
	{#FORM ModAlbum}
		{#INPUT id hidden $id}
		{#INPUT titlepic hidden $titlepicid}
		<table cellspacing="20px">
			<tr>
				<td>Name:</td>
				<td>{#INPUT albumname string $name}</td>
			</tr>
			<tr>
				<td>Kategorie:</td>
				<td>{#INPUT name="newcat" type="dropdown" val=$id param=$cats}</td>
			</tr>
			<tr>
				<td>Titelbild:</td>
				<td>
					<select name="titlepicsel" onChange="ChangeThumb(this.options[this.selectedIndex].value);" onKeyUp="ChangeThumb(this.options[this.selectedIndex].value);">
						<option value="0">Kein Titelbild</option>
						{#FOREACH array=$pics var=$value key=$key}
							<option value="{$key}"{#IF $key == $titlepicid} selected="selected"{#END}>{$value}</option>
						{#END}
					</select>
				</td>
			</tr>
			<tr>
				<td>Vorschau:</td>
				<td><img src="{#IF $titlepicid != 0}media/thumbnails/{$id}/{$picpath}{#ELSE}{#STATICFILE images/gallery/no_picture.jpg}{#END}" name="preview" alt="Vorschaubild" width="100px" /></td>
			</tr>
			<tr>
				<td colspan="2">
					{#SUBMIT Speichern}
				</td>
			</tr>
			<tr>
				<td colspan="2">
					{#BACKLINK Zur&uuml;ck} 
				</td>
			</tr>
		</table>
	{#END}
{#END}