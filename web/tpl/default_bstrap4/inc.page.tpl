{#SUB head}<<?php echo"?"?>xml version="1.0" encoding="ISO-8859-1"?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<!-- 
		<meta http-equiv="Content-Security-Policy" 	script-src https://*.example.com/; 
													style-src https://*.example.com/; 
													manifest-src https://*.example.com/;
													connect-src https://*.example.com/ ">
		-->
		<link rel="icon" href="{#STATICFILE images/favicon.ico}">

		<!-- Bootstrap core CSS -->
		<link href="{#STATICFILE dist/css/bootstrap.min.css}" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="{#STATICFILE css/custom.css}" rel="stylesheet">
		<link href="{#STATICFILE css/bootstrap_custom.css}" rel="stylesheet">
		
		<!-- Bootstrap core JavaScript ================================================== -->
			<!-- Placed at the end of the document so the pages load faster -->
			<script src="{#STATICFILE jquery_bracket/jquery-3.3.1.min.js}" type="text/javascript"></script>
			<script src="{#STATICFILE js/popper.min.js}" type="text/javascript"></script> 
			<script src="{#STATICFILE dist/js/bootstrap.min.js}"></script>

			<!-- jquery bracket tournament plugin -->
			<script src="{#STATICFILE jquery_bracket/jquery.bracket.min.js}" type="text/javascript"></script>
			
			<!-- Font Awesome Module -->
			<link href="{#STATICFILE font-awesome/css/all.min.css}" rel="stylesheet">
			<link href="{#STATICFILE css/jquery.bracket.min.css}" rel="stylesheet"  type="text/css" />
			
		
		<!-- Custom javascript for Bootstrap -->
		<script type="text/javascript">
		$(document).ready(function(){
			$('a[data-toggle=tooltip]').tooltip();
		});
		</script>
		<!-- ende -->
		
		<!--[if IE]>
		<link rel="stylesheet" type="text/css" href="{#STATICFILE css/all-ie-only.css}" />
		<![endif]-->
		
		<!-- VulkanLAN include tinymce edit by naaux -->
		<script type="text/javascript" src="./ext/tinymce/tinymce.min.js"></script>
		<script type="text/javascript">
			tinymce.init({
			selector: 'textarea',
			theme: 'silver',
			forced_root_block : false,
			force_p_newlines : false,
			remove_linebreaks : false,
			force_br_newlines : false,
			verify_html : false,
			cleanup: false,
			cleanup_on_startup: false,
			relative_urls : false,
			remove_script_host : false,
			document_base_url: 'https://event.local',
			browser_spellcheck : true,
			theme_advanced_resizing: true,
			theme_advanced_resize_horizontal: false,
			auto_resize: false,
			trim_span_elements: false,
			remove_trailing_nbsp : false,
			fontsize_formats: "0.5em 0.8em 0.9em 1em 1.2em 1.5em 1.8em 2em",
			
			height: 300,
			plugins: [
				 "advlist autolink link image lists charmap print preview hr anchor pagebreak",
				 "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
				 "save table contextmenu directionality emoticons template paste textcolor"
					],
			toolbar: "insertfile undo redo | styleselect | fontsizeselect | fontselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons", 
			style_formats: [
				{title: 'Bold text', inline: 'b'},
				{title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
				{title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
				{title: 'Example 1', inline: 'span', classes: 'example1'},
				{title: 'Example 2', inline: 'span', classes: 'example2'},
				{title: 'Table styles'},
				{title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
				]
			}); 
		</script>
		<title>{%title} - {%caption}</title>
	</head>
{#END}

//no menue option
{#SUB foot}
</body>
</html>
{#END}
//end no menue option

{#SUB page}
<!-- no menue option-->
{#VAR sub=head}
<!-- end no menue option-->
<body role="document">
	
<nav class="navbar navbar-expand-lg navbar-dark text-light bg-dark fixed-top" role="navigation">
	<div class="container-fluid">
	<div class="row offset-lg-1">
		{#LOAD MenuLoad() $level0 inc/inc.menu.php}
			{#FOREACH $level0.menu}
				{#FOREACH $items}
					{#IF $id==8}
						{#LOAD LoadMenu(%id,1) $sub inc/inc.menu.php}
							{#FOREACH $sub.menu}
								{#FOREACH $items}
									{#IF $id==93}
									<!--Get the link from the mainpage and set it on the logo to get back to the start page-->
									<!--id 8 -> menu lanparty-->
									<!--id 93 -> submenu lan-info-->
									<a class="navbar-brand d-none d-sm-block" href="{$link}" rel="home">
										<img alt="VulkanLAN" src="{#STATICFILE images/logo.png}"> 
									</a>
									<a class="navbar-brand d-block d-sm-none" href="{$link}" rel="home">
										<img alt="VulkanLAN" src="{#STATICFILE images/logo.png}" style="width: 200px;"> 
									</a>
								{#END}
							{#END}
						{#END}
					{#END}
				{#END}
			{#END}
			</div>
			<button type="button" class="navbar-toggler navbar-toggler-right" data-toggle="collapse" data-target="#navbarContent" aria-expanded="false" aria-controls="navbarContent" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			
			<div class="collapse navbar-collapse" id="navbarContent">
			<ul class="navbar-nav mr-auto">
				{#FOREACH $level0.menu}
				{#FOREACH $items}
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="{$link}" role="button" title="{%description}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  id="navbarDropdown">
						<i class="{$font_image}"></i>
						{%caption}
						<!--<b class="caret"></b> -->
					</a>
				
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					{#LOAD LoadMenu(%id,1) $sub inc/inc.menu.php}
					{#FOREACH $sub.menu}
					{#FOREACH $items}
							{#IF is_object($frame)}<div class="dropdown-item">{#TPL $frame}</div>
							{#ELSEIF !empty($text)}<div class="dropdown-item">{$text}</div>
							{#ELSE}<div class="dropdown-item">{#DBIMAGE $image}</div>
							
							<a class="dropdown-item {#WHEN $active }" aria-haspopup="true" aria-expanded="false" href="{$link}" title="{%description}"{#WHEN "isset($use_new_wnd) && $use_new_wnd" " target=\"_blank\""}>
							<i class="{$font_image}"></i>&nbsp;{%caption}</a>
							{#END}
					{#END}
					{#END}
					</div>
				</li>
				{#END}
				{#END}
			</ul>
			{#VAR sub=login file=inc.page.login.tpl}
			</div>
			
			
	</div>
</nav>

{#IF $dsgvocheck == "Y"}
	<div class="container-fluid my-1">
		<div class="row">
			<div class="col col-md-11 col-lg-10 offset-md-1 offset-lg-1">
			{#LOAD PageLoadFrame("'user.php?frame=dsgvo'") $dsgvo1}
			{#TPL $dsgvo1}
			</div>
		</div>
	</div>
	{#ELSE}
	<!--Get the link from the mainpage and set carouseIndicators to the start page-->
	<!--id 8-93 -> menu lanparty-->
		{#IF $level0.selecteddir=="8-93"}
			{#VAR sub=carouseIndicators}
		{#END}
	<!-- end carouseIndicators -->
	<div class="container-fluid my-1">
			<button data-toggle="collapse" data-target="#infostatus" class="btn btn-secondary float-right d-none d-sm-block" aria-expanded="true"><i class="fas fa-angle-down"></i></button>
			<div id="infostatus" class="collapse <?php $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; if (strpos($url,'text.php?name=menu_lanparty') !== false) { echo 'show'; }  ?> ">
				<div class="row">
					<div class="col col-md-11 col-lg-10 offset-md-1 offset-lg-1">
						<div class="row">
							<div class="col-sm d-none d-sm-block">
							{#LOAD PageLoadFrame("'lanparty.php?frame=smalluserstatus'") $temp2}
							{#TPL $temp2}
							</div>
							<div class="col-sm d-none d-sm-block">
							{#LOAD PageLoadFrame("'lanparty.php?frame=smallstatusbar'") $temp}
							{#TPL $temp}
							</div>
							<div class="col-sm d-none d-lg-block">
							{#LOAD PageLoadFrame("'lanparty.php?frame=countdown'") $temp3}
							{#TPL $temp3}
							</div>
							<div class="col-sm d-none d-lg-block">
							{#LOAD PageLoadFrame("'webmessage.php?frame=smallnewmessages'") $temp1}
							{#TPL $temp1}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col col-md-11 col-lg-10 offset-md-1 offset-lg-1">
					<div class="card "><!-- ERRORCELL --></div>
						{#WHEN "$caption != \"\"" <h1>%caption</h1> ""}
						{#TPL $page}
				</div>
			</div>
	</div>
<!-- no menue option-->
{#VAR sub=foot}
<!-- end no menue option-->
{#END}


<!-- Sponoren Anzeige - Position - Vorschlaege-->

<div class="container-fluid" id="footer">
	<div class="row">
		
		<div class="list-group col col-md-9 col-lg-10 offset-md-1 offset-lg-1 text-center">	
			<ul class="list-group">
				<li class="list-group-item list-group-item-info"> Sponsor</li>
				<li class="list-group-item">{#VAR sub=banner file=inc.sponsor.tpl}</li>
			</ul>
		</div>
		<div class="col-md-auto"></div>
	</div>
</div>

<!--Ende Sponoren Anzeige-->


	<!-- Footer -->
	<footer class="py-5 bg-dark">
		<div class="container">
		<p class="m-0 text-center text-white">{$copyright} | Design by <a href="https://www.vulkanlan.at">naaux</a>.</p>
		</div>
	</footer>
	<!-- /.footer -->

	</body>
</html>
{#END}
<!-- Error Ausgabe - fuer Troubleshooting -->
{#SUB errorcell}
	{#IF count($errors) > 0}
		<h2>{§Fehler}</h2>
		<p class="errorcell">
		{#FOREACH $errors $err}
		- {$err}<br/>
		{#END}
		</p>
	{#END}
	{#IF count($notices) > 0}
		<h2>{§Nachricht}</h2>
		<p class="noticecell">
		{#FOREACH $notices $msg}
		- {$msg}<br/>
		{#END}
		</p>
	{#END}
{#END}

{#SUB pagecontent}
	<div class="container-fluid my-1">
			<div class="row">
				<div class="col col-md-11 col-lg-10 offset-md-1 offset-lg-1">
					<div class="card "><!-- ERRORCELL --></div>
						{#WHEN "$caption != \"\"" <h1>%caption</h1> ""}
						{#TPL $page}
				</div>
			</div>
	</div>
{#END}


{#SUB dsgvocheckold}
	{#IF $dsgvo == "N"}
		{#IF $isloggedin=="1"}
		<div class="container-fluid my-1">
			<div class="row">
				<div class="col col-md-11 col-lg-10 offset-md-1 offset-lg-1">
				{#LOAD PageLoadFrame("'user.php?frame=dsgvo'") $dsgvo1}
				{#TPL $dsgvo1}
				</div>
			</div>
		</div>
		{#ELSE}
			{#VAR sub=pagecontent}
		{#END}
	{#ELSE} 
		{#VAR sub=pagecontent}
	{#END}
{#END}

{#SUB carouseIndicators}
	<header>
		  <div id="carouseIndicators" class="carousel slide" data-ride="carousel" style="margin-top: -15px;">
			<ol class="carousel-indicators">
			  <li data-target="#carouseIndicators" data-slide-to="0" class="active"></li>
			  <li data-target="#carouseIndicators" data-slide-to="1"></li>
			  <li data-target="#carouseIndicators" data-slide-to="2"></li>
			  <li data-target="#carouseIndicators" data-slide-to="3"></li>
			  <li data-target="#carouseIndicators" data-slide-to="4"></li>
			  <li data-target="#carouseIndicators" data-slide-to="5"></li>
			</ol>
			<div class="carousel-inner" role="listbox">
			  <!-- Slide One - Set the background image for this slide in the line below -->
			  <div class="carousel-item active">
				 <img class="d-block w-100" src="{#STATICFILE images/header_foto_1.jpg}" alt="until0">
				<div class="carousel-caption d-none d-sm-block">
				   <span style="font-size: 8vw; text-shadow:5px 5px 10px black;">Cooler Spruch 1!</span>
				</div>
			  </div>
			  <!-- Slide Two - Set the background image for this slide in the line below -->
			  <div class="carousel-item">
				<img class="d-block w-100" src="{#STATICFILE images/header_foto_2.jpg}" alt="until1">
				<div class="carousel-caption d-none d-sm-block">
				  
				   <span style="font-size: 8vw; text-shadow:5px 5px 10px black;">Cooler Spruch 2!</span>
				</div>
			  </div>
			  <!-- Slide Three - Set the background image for this slide in the line below -->
			  <div class="carousel-item">
				<img class="d-block w-100" src="{#STATICFILE images/header_foto_3.jpg}"  alt="until2">
				<div class="carousel-caption d-none d-sm-block">
				 
				 <span style="font-size: 8vw; text-shadow:5px 5px 10px black;">Cooler Spruch 3!</span>
				</div>
			  </div>
				<!-- Slide four - Set the background image for this slide in the line below -->
				<div class="carousel-item">
					<img class="d-block w-100" src="{#STATICFILE images/header_foto_4.jpg}"  alt="until3">
					<div class="carousel-caption d-none d-sm-block">
					 
					 <span style="font-size: 8vw; text-shadow:5px 5px 10px black;">Cooler Spruch 4!</span>
					</div>
				</div>
				<!-- Slide five - Set the background image for this slide in the line below -->
				<div class="carousel-item">
					<img class="d-block w-100" src="{#STATICFILE images/header_foto_5.jpg}"  alt="until4">
					<div class="carousel-caption d-none d-sm-block">
					 
					 <span style="font-size: 8vw; text-shadow:5px 5px 10px black;">Cooler Spruch 5!</span>
					</div>
				</div>
				<!-- Slide six - Set the background image for this slide in the line below -->
				<div class="carousel-item">
					<img class="d-block w-100" src="{#STATICFILE images/header_foto_7.jpg}"  alt="until5">
					<div class="carousel-caption d-none d-sm-block">
					 
					 <span style="font-size: 8vw; text-shadow:5px 5px 10px black;">Cooler Spruch ;) 7!</span>
					</div>
				</div>
			</div>
			<a class="carousel-control-prev" href="#carouseIndicators" role="button" data-slide="prev">
			 <span class="carousel-control-prev-icon" aria-hidden="true"></span>
			  <span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#carouseIndicators" role="button" data-slide="next">
			  <span class="carousel-control-next-icon" aria-hidden="true"></span>
			  <span class="sr-only">Next</span>
			</a>
		  </div>
	  </header>
{#END}