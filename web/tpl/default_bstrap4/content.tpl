{#SUB default}
<div class="card">
	<div class="card-header">
		<a href="content.php" class="btn btn-md btn-secondary ml-1"><i class="fas fa-arrow-left"></i>&nbsp;Zur&uuml;ck</a>
		<a href="content.php?frame=editgroup&amp;id=0" class="btn btn-success ml-1" role="button"><i class="fas fa-plus"></i>&nbsp;add</a>
	</div>
	<div class="card-body">
		<div class="table">
		  {#TABLE $items class="table"}
			{$groupcount} Gruppen ( {#BYTE $size mb 3} MByte ) </b><a href="content.php?frame=editgroup&amp;id=0" class="btn btn-success ml-1" role="button"><i class="fas fa-plus"></i>&nbsp;add</a></b>
			{#COL Name $name}<b><a href="content.php?frame=viewgroup&amp;id={$id}">{%name}</a></b>
			{#COL Gr&ouml;&szlig;e $size align=right}{#BYTE $size kb 1}
			{#COL Beschreibung %description}
			{#COL Leserecht<br/>Schreibrecht}{#RIGHT $view_right view}<br />{#RIGHT $edit_right edit}
			{#COL Texte<br/>Bilder align=center}{$texts} | {$images}
			{#FRAME edit editgroup right=$edit_right}
			{#OPTION L&ouml;schen deletegroup}
		  {#END}
		</div>
	</div>
</div>
{#END}

{#SUB editgroup}
<div class="card">
	{#FORM group}
		<div class="card-header">
			{#BACKLINK Zur&uuml;ck}
			{#SUBMIT Speichern}
		</div>
		<div class="card-body">
			<div class="table">
				<table class="table">
				  <tr><td align="right">Name:</td><td>{#INPUT name name $name allowempty=0}</td></tr>
				  <tr><td align="right">Beschreibung:</td><td>{#INPUT description longstring $description}</td></tr>
				  <tr><td align="right">Leserecht:</td><td>{#INPUT view_right rights $view_right}</td></tr>
				  <tr><td align="right">Schreibrecht:</td><td>{#INPUT edit_right rights $edit_right}</td></tr>
				  <tr><td colspan="2" align="center"><br /><br />{#SUBMIT Speichern}</td></tr>
				  <tr><td colspan="2" align="center"><br /><br />{#BACKLINK Zur&uuml;ck}</td></tr>
				</table>
				{#INPUT id hidden $id}
			</div>
		</div>
	{#END}
</div>
{#END}

{#SUB viewgroup}
<div class="card">
	<div class="card-header">
		<div class="row">
		{%group.description}
		<a href="content.php?frame=editgroup&amp;id={$group.id}" class="btn btn-secondary btn-sm ml-1" role="button">bearbeiten</a>
		</div>
		<div class="row">
		<a href="content.php" class="btn btn-md btn-secondary ml-1"><i class="fas fa-arrow-left"></i>&nbsp;Zur&uuml;ck</a>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12 table">
				<h3>Texte <a href="content.php?frame=edittext&amp;id=0&amp;group_id={$group.id}" class="btn btn-success btn-sm ml-1" role="button"><i class="fas fa-plus"></i>&nbsp;add</a></h3>
				{#TABLE $texts class="table"}
				  {$textcount} Text{#WHEN "$textcount!=1" e} ( {#BYTE $textsize kb 1} kByte ) 
				  <a href="content.php?frame=edittext&amp;id=0&amp;group_id={$group.id}" class="btn btn-success ml-1" role="button"><i class="fas fa-plus"></i>&nbsp;add</a>
				  {#COL "Name (Gr&ouml;&szlig;e)<br />Beschreibung" $name}<b><a href="text.php?name={%name}">{%name}</a></b> ({$size})<br />{%description}
				  {#COL Titel %caption}
				  {#COL Leserecht<br/>Schreibrecht}{#RIGHT $view_right view}<br />{#RIGHT $edit_right edit}
				  {#COL Autor<br/>Datum}{%author}<br /><nobr>{#DATE $edit_time}</nobr>
				  {#FRAME edit edittext right=$edit_right}
				  {#OPTION L&ouml;schen deletetext right=admin}
				  {#OPTION "Verschieben nach" movetext}{#INPUT group dropdown param=$groups}
				{#END}
			</div>
			<div class="col-12 table">
				<h3>Bilder  <a href="content.php?frame=editimage&amp;id=0&amp;group_id={$group.id}" class="btn btn-success btn-sm ml-1" role="button"><i class="fas fa-plus"></i>&nbsp;add</a></h3>
				{#TABLE $images class="table"}
				  {$imagecount} Bild{#WHEN "$imagecount!=1" er} ( {#BYTE $imagesize kb 1} kByte )
				  &nbsp;&nbsp;&middot;&nbsp;&nbsp;
				  <a href="content.php?frame=editimage&amp;id=0&amp;group_id={$group.id}" class="btn btn-success ml-1" role="button"><i class="fas fa-plus"></i>&nbsp;add</a>
				  {#COL Vorschau style="padding:0px;"}<a href="image.php?name={%name}"><img border="0" src="image.php?frame=thumbnail&amp;name={$name}" alt="{$name}" width="{$tn_width}" height="{$tn_height}" /></a>
				  {#COL "Name (Gr&ouml;&szlig;e)<br />Beschreibung" $name}<b><a href="image.php?name={%name}">{%name}</a></b> ({#BYTE $size kb 1}kB)<br />{%description}
				  {#COL Titel %caption}
				  {#COL Leserecht<br/>Schreibrecht}{#RIGHT $view_right view}<br />{#RIGHT $edit_right edit}
				  {#COL Autor<br/>Datum}{%author}<br /><nobr>{#DATE $edit_time}</nobr>
				  {#FRAME edit editimage right=$edit_right}
				  {#OPTION L&ouml;schen deleteimage}
				  {#OPTION "Verschieben nach" moveimage}{#INPUT group dropdown param=$groups}
				{#END}
			</div>
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB edittext}
<div class="card">
	{#FORM text}
		<div class="card-header">
			{#BACKLINK Zur&uuml;ck}
			{#SUBMIT Speichern}
		</div>
		<div class="card-body">
			<div class="table">
				<table class="table">
				  <tr><td>    
					<table width="100%" cellpadding="0" cellspacing="0"><tr>
					  <td>Name: {#INPUT name name $name allowempty=0}</td>
					  <td>Titel:{#INPUT caption string $caption}</td>
					</tr></table>
				  </td></tr>
				  <tr><td>
					<table width="100%" cellpadding="0" cellspacing="0"><tr>
					  <td width="83">Beschreibung:</td>
					  <td>{#INPUT description longstring $description}</td>
					</tr></table>
				  </td></tr>
				  <tr><td>
					<table width="100%" cellpadding="0" cellspacing="0"><tr>
					  <td>Gruppe: {#INPUT group_id dropdown $group_id $groups}</td>
					  <td>Leserecht: {#INPUT view_right rights $view_right}</td>
					  <td>Schreibrecht: {#INPUT edit_right rights $edit_right}</td>
					</tr></table>
				  </td></tr>
				  <tr>
					<td>{#INPUT text document $text}</td>
				  </tr>
				  <tr><td align="center"><br /><br />{#SUBMIT Speichern}</td></tr>
				  <tr><td>
					Innerhalb eines Textes sollten Bilder mittels {#LBRACE}#IMAGE ...{#RBRACE} eingebunden werden.
					Das hat den Vorteil, dass automatisch die Gr&ouml;&szlig;e des Bildes aus der Datenbank ausgelesen und
					in den IMG-Tag eingef&uuml;gt wird. Das gleiche gilt f&uuml;r das ALT-Attribut.<br />
					G&uuml;ltige Aufrufe sind:
					<pre>{#LBRACE}#IMAGE bildname{#RBRACE}
				{#LBRACE}#IMAGE name=bildname{#RBRACE}
				{#LBRACE}#IMAGE bildname alt="dies ist ein alternativ-text"{#RBRACE}
				{#LBRACE}#IMAGE bildname alt=soso;) border="10"{#RBRACE}</pre>
					Folgende Parameter sind erlaubt:<br />
					name: ohne den geht's nicht.<br />
					width, height: werden von den Werten aus der Datenbank &uuml;berschrieben.<br />
					alt: &uuml;berschreibt den Wert aus der Datenbank.<br />
					ansonsten ist alles erlaubt, was ein g&uuml;ltiges Attribut des IMG-Tags ist.
				  </td></tr>
				</table>
				{#INPUT id hidden $id}
			</div>
		</div>
	{#END}
</div>
{#END}

{#SUB editimage}
<div class="card">
	{#FORM image}
		<div class="card-header">
			{#BACKLINK Zur&uuml;ck}
			{#SUBMIT Speichern}
		</div>
		<div class="card-body">
			<div class="table">
				<table class="table">
				  <tr>
					<td align="right">Name:</td>
					<td>{#INPUT name name $name allowempty=0}&nbsp;&nbsp;&nbsp;&nbsp;Alt-Attribut:{#INPUT caption string $caption}</td>
					<td></td>
				  </tr>
				  <tr>
					<td align="right">Beschreibung:</td>
					<td>{#INPUT description longstring $description}</td>
				  </tr>
				  <tr>
					<td align="right">Leserecht:</td>
					<td>{#INPUT view_right rights $view_right}&nbsp;&nbsp;&nbsp;&nbsp;Schreibrecht: {#INPUT edit_right rights $edit_right}</td>
				  </tr>
				  <tr>
					<td align="right">Bild:</td>
					<td>{#INPUT image file param=db} ({$types})</td>
				  </tr>
				  <tr>
					<td align="right">Gruppe:</td>
					<td>{#INPUT group_id dropdown $group_id $groups}</td>
				  </tr>
				  <tr><td colspan="2" align="center"><br />{#SUBMIT Speichern}</td></tr>
				  {#IF !empty($mime)}
				  <tr><td colspan="2" align="center"><br /><br /><img alt="{%caption}" src="image.php?name={$name}"></td></tr>
				  {#END}
				</table>
				{#INPUT id hidden $id}
			</div>
		</div>
	{#END}
</div>
{#END}
