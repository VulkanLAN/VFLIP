{! $Id: server.tpl 1285 2006-08-01 19:10:53Z loom $ }

{#SUB default}
<div class="card ">
	<div class="card-header">	
		<div class="row">
			<div class="col-12">
        <h2>Liste der Gastserver</h2>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12">
        {#TPL $text}
      </div>
      <div class="col-12">
        Unter <a href="server.php?frame=add&amp;menudir=36-70">meine daten&rarr;mein(e) Server</a> kannst du deine(n) Server hinzuf&uuml;gen.<br />
        <br />
        {#TABLE $serverlist}
          {#COL "<b>Name</b><br />Besitzer" %name}<b>{%name}</b><br />{%owner}
          {#COL "OS<br />Hardware" %os}{#TABLEDISPLAY server_oses $os}<br/>{%hardware}
          {#COL "IP<br />DNS" $ip}{$ip}<br/>{$server_dns}
          {#COL "Dienste<br />Spiele" $services}{#TABLEDISPLAY server_services $services}<br/>{#TABLEDISPLAY server_games $games}    
          {#COL Beschreibung %description}
          {#COL}{#IF $canedit}<a href="user.php?frame=editproperties&id={$id}">edit</a>{#END}
        {#END}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB add}
<div class="card ">
	<div class="card-header">	
		<div class="row">
			<div class="col-12">
        <h2>&Uuml;bersicht</h2>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12">
      {#TPL $text}
      </div>
      <div class="col-12">
      {#TABLE $serverlist}
        <a href="user.php?frame=editproperties&id=create_new_subject&type=server">hinzuf&uuml;gen</a>
        {#COL "<b>Server</b>"}
          <b>{%name}</b> {#WHEN "$atparty=='Y'" "<span style=\"color:green;\">dabei</span>" "<span style=\"color:red;\">nicht dabei</span>"}<br />
          {%hardware}<br/>
          {#TABLEDISPLAY server_oses $os}<br />
          {#TABLEDISPLAY server_services $services}<br/>
          {#TABLEDISPLAY server_games $games}
        {#COL Konfiguration}
          IP: {#WHEN empty($ip) "nicht vergeben" $ip}<br />
          {#IF !empty($server_dns)}DNS-Name: {$server_dns}<br/>{#END}
          {#IF !empty($server_dns_request)}Wunsch-DNS: {$server_dns_request}<br/>{#END}
          {#IF !empty($server_gateway)}Gateway: {$server_gateway}<br/>{#END}
          {#IF !empty($server_subnetmask)}Subnetmask: {$server_subnetmask}<br/>{#END}
          {#IF !empty($server_dns_ip)}DNS-Server: {$server_dns_ip}<br/>{#END}
          {#IF !empty($server_wins_ip)}WINS-Server: {$server_wins_ip}<br/>{#END}
        {#COL Beschreibung %description valign="top"}
        {#COL}<a href="user.php?frame=editproperties&id={$id}">edit</a>
        {#OPTION "bei n&auml;chstem Event <span style=\"color:green;\">dabei</span>" atparty}
        {#OPTION "bei n&auml;chstem Event <span style=\"color:red;\">nicht dabei</span>" notatparty}
        {#OPTION l&ouml;schen del}
      {#END}
			</div>
		</div>
	</div>
</div>
{#END}
