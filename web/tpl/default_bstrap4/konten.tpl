{! $Id: konten.tpl 1285 2006-08-01 19:10:53Z loom $ }

{#SUB default}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
        {#FOREACH $list index=$i}
        {#IF $i>1}- {#END}<a href="konten.php?kid={$id}">{$title}</a> 
        {#END}
        <table border="0" cellspacing="0" cellpadding="3">
          <tr class="hdr">
            <th>Datum</th>
            <th>Beschreibung</th>
            <th align="right">Betrag</th>
            <th>&nbsp;</th>
          </tr>
        {#FOREACH $kosten index=$i}
          <tr class="{#IF ($i % 2==1)}row2{#ELSE}row1{#END}">
            <td width="60" height="19" align="left">{$date}</td>
            <td width="492" height="19" align="left">{#IF $edit=="true"}<a href="konten.php?frame=edit&amp;id={$id}">{%text}</a>{#ELSE}{%text}{#END}</td>
            <td width="111" height="19" align="right">{$betrag}</td>
            <td>{#IF $edit=="true"}{#FORM del}{#INPUT id hidden $id}{#INPUT ktext hidden $text}{#SUBMIT del}{#END}{#ELSE}&nbsp;{#END}</td>
          </tr>
        {#END}
          <tr class="hdr">
            <td width="60" height="19" align="left">&nbsp;</td>
            <td width="492" height="19" align="left">Summe</td>
            <td width="111" height="19" align="right">{$sum}</td>
            <td>&nbsp;</td>
          </tr>
          <tr class="{$class}">
            <td width="60" height="19" align="left">&nbsp;</td>
            <td width="492" height="19" align="left">entspricht in &Uuml;berweisungen</td>
            <td width="111" height="19" align="right">{$users} x {%price} {%currency}</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr class="{$class}">
            <td width="60" height="19" align="left">&nbsp;</td>
            <td width="492" height="19" align="left">Saldo aller Konten:</td>
            <td width="111" height="19" align="right">{$saldo}</td>
            <td>&nbsp;</td>
          </tr>
        </table>
        <br />
        {#IF $edit=="true"}
        {#FORM remkonto}
          {#INPUT kid hidden $list_id}
          {#INPUT ktitle hidden $title}
          {#SUBMIT "Dieses Konto l&ouml;schen"}
        {#END}
        <h4>Eintrag hinzuf&uuml;gen</h4>
        {#VAR sub=edit}
        <br />
        <h4>Neues Konto anlegen</h4>
        <form method="post">
        Name: <input class="edit" name="ktitle"> <input class="button" type="submit" value="anlegen"><input type="hidden" name="action" value="add">
        </form>
        {#END}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB edit}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
        <form action="konten.php?kid={$list_id}" method="post">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>Beschreibung:</td>
            <td><input class="edit" type="text" name="text" value="{%text}" maxlength="64"></td>
          </tr>
          <tr>
            <td>Betrag:</td>
            <td><input class="edit" type="text" name="value" value="{$value}" align="right" size="6" maxlength="10"></td>
          </tr>
          <tr>
            <td>Datum:</td>
            <td><input class="edit" type="text" name="date" value="{$date}" size="10" maxlength="16"> (leer f&uuml;r jetzt)</td>
          </tr>
        </table>
          <input class="button" type="submit" value="hinzuf&uuml;gen"><input type="hidden" name="action" value="insert"><input type="hidden" name="kid" value="{$list_id}"><input type="hidden" name="id" value="{$id}">
        </form>
			</div>
		</div>
	</div>
</div>
{#END}