{#SUB header}
<div class="card-header">	
		<div class="row">
			<div class="col-12">
        <style type="text/css">
        .nltitle{font-size : 14px; font-weight:bold; text-align:center; }
        </style>
        <table>
          <tr>
          {#FOREACH $items $i}
            <td class="nltitle">
            {#FOREACH $i $t $f}
              <a {#WHEN "$f == $current" "class=\"important\""} href="netlog.php?frame={$f}">{%t}</a><br />
            {#END}
            </td>
          {#MID}
            <td class="nltitle">&middot;</td>
          {#END}
          </tr>
        </table>
  		</div>
	</div>
</div>
{#END}

{#SUB info}
<div class="card ">
{#VAR $header header}
  <div class="card-body">
		<div class="row">
			<div class="col-12">
        <div align="left" style="width:70%">
        <h2>Info/FAQ</h2>
        <h3>Sind IP- und MAC-Liste nicht inhaltlich identisch?</h3>
        <p align="justify">
          Nein, das sind sie nicht, auch wenn auf den ersten Blick dieser Eindruck entstehen mag.
          Der Unterschied wird dann deutlich, wenn z.B. ein Host seine IP-Adresse &auml;ndert. Jetzt enh&auml;lt
          die IP-Liste f&uuml;r ihn zwei Eintr&auml;ge - einen mit seiner alten und einen mit seiner neuen IP,
          die MAC-List dagegen f&uuml;hrt weiterhin nur einen Eintrag f&uuml;r ihn auf, welcher seine MAC seiner
          aktuellen IP zuordnet.
        </p>
        <p>
          Bugs:<br />
          2 IPs pro MAC wird nicht korrekt angezeigt.
        </p>
        </div>
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB default}
<div class="card ">
      {#VAR $header header}
  <div class="card-body">
		<div class="row">
      <div class="col-12">
          <p>
            Der NetLog f&uuml;hrt alle &Auml;nderungen auf, die in diesem Netzwerk vorgenommen wurden. Er wird
            generiert mittels ArpWatch und den Daten, die eroben werden k&ouml;nnen, wenn sich ein User
            im FLIP einloggt.
          </p>
      </div>
			<div class="col-12">
        {#TABLE $items maxrows=100 foundrows=$foundrows dborder=1 class=wide_table}
          {#COL Zeit $time}<nobr>{#IF $time > $header.last_visit}<span class="important">{#DATE $time}</span>{#ELSE}{#DATE $time}{#END}</nobr>
          {#COL MAC $mac}<a href="netlog.php?frame=search&amp;type=matches&amp;column=mac&amp;search={$mac}">{$mac}</a>
          {#COL IPv4 $ipv4}<a href="netlog.php?frame=search&amp;type=matches&amp;column=ipv4&amp;search={$ipv4}">{$ipv4}</a>
          {#COL Name $hostname}<a href="netlog.php?frame=search&amp;type=matches&amp;column=hostname&amp;search={$hostname}">{$hostname}</a>
          {#COL User $name}{#IF !empty($name)}<a href="netlog.php?frame=search&amp;type=matches&amp;column=user&amp;search={%name}">{%name}</a> <a href="user.php?frame=viewsubject&amp;id={$user_id}">(i)</a>{#END}
          {#COL Action $action}
          {#COL Anmerkung %description}
        {#END}
        {#FORM clearlog}
        <br />
        <table style="table-width: 100%;">
        <tr>
        <td class="tdcont">{#SUBMIT "NetLog l&ouml;schen" shortcut=c}{#INPUT log hidden clear}</td>
        </tr>
        </table>
        {#END}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB onstat}
  {#IF $action == "host_offline"}
  <span style="color:red;"><b>off</b></span>
  {#ELSE}
  <span style="color:green;"><b>on</b></span>
  {#END}
{#END}

{#SUB list}
  {#TABLE $items maxrows=100 foundrows=$foundrows dborder=1 class="wide_table"}
    {#COL LastSeen $time}<nobr>{#IF $time > $header.last_visit}<span class="important">{#DATE $time}</span>{#ELSE}{#DATE $time}{#END}</nobr>
    {#COL On align=center}{#VAR sub=onstat}
    {#COL MAC $mac}<a href="netlog.php?frame=search&amp;type=matches&amp;column=mac&amp;search={$mac}" title="{%nic_vendor}">{$mac}</a>
    {#COL IPv4 $ipv4}<a href="netlog.php?frame=search&amp;type=matches&amp;column=ipv4&amp;search={$ipv4}">{$ipv4}</a>
    {#COL DNS $dnsname}<a href="netlog.php?frame=search&amp;type=matches&amp;column=dnsname&amp;search={$dnsname}">{$dnsname}</a>
    {#COL NBName $nbname}<a href="netlog.php?frame=search&amp;type=matches&amp;column=nbname&amp;search={$nbname}">{$nbname}</a>
    {#COL NBGroup $nbgroup}<a href="netlog.php?frame=search&amp;type=matches&amp;column=nbgroup&amp;search={$nbgroup}">{$nbgroup}</a>
    {#COL User $name}{#IF !empty($name)}<a href="netlog.php?frame=search&amp;type=matches&amp;column=user&amp;search={%name}">{%name}</a> <a href="user.php?frame=viewsubject&amp;id={$user_id}">(i)</a>{#END}
    {#COL LastAction $action}
  {#END}
{#END}

{#SUB listtpl}
  {#TABLE $items maxrows=100 class="wide_table"}
    {#COL LastSeen $time}<nobr>{#IF $time > $header.last_visit}<span class="important">{#DATE $time}</span>{#ELSE}{#DATE $time}{#END}</nobr>
    {#COL On align=center}{#VAR sub=onstat}
    {#COL MAC $mac}<a href="netlog.php?frame=search&amp;type=matches&amp;column=mac&amp;search={$mac}" title="{%nic_vendor}">{$mac}</a>
    {#COL IPv4 $ipv4}<a href="netlog.php?frame=search&amp;type=matches&amp;column=ipv4&amp;search={$ipv4}">{$ipv4}</a>
    {#COL DNS $dnsname}<a href="netlog.php?frame=search&amp;type=matches&amp;column=dnsname&amp;search={$dnsname}">{$dnsname}</a>
    {#COL NBName $nbname}<a href="netlog.php?frame=search&amp;type=matches&amp;column=nbname&amp;search={$nbname}">{$nbname}</a>
    {#COL NBGroup $nbgroup}<a href="netlog.php?frame=search&amp;type=matches&amp;column=nbgroup&amp;search={$nbgroup}">{$nbgroup}</a>
    {#COL User $name}{#IF !empty($name)}<a href="netlog.php?frame=search&amp;type=matches&amp;column=user&amp;search={%name}">{%name}</a> <a href="user.php?frame=viewsubject&amp;id={$user_id}">(i)</a>{#END}
    {#COL LastAction $action}
  {#END}
{#END}

{#SUB macs}
<div class="card ">
      {#VAR $header header}
  <div class="card-body">
		<div class="row">
      <div class="col-12">
        <p>
          Diese Auflistung enth&auml;lt alle MAC-Adressen, die in diesem Netzwerk online sind oder waren.<br /><br />
        </p>
      </div>
      <div class="col-12">
      {#VAR sub=list}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB ips}
<div class="card ">
      {#VAR $header header}
  <div class="card-body">
		<div class="row">
      <div class="col-12">
        <p>
          Diese Auflistung enth&auml;lt alle IP-Adressen, die in diesem Netzwerk online sind oder waren.<br /><br />
        </p>
      </div>
			<div class="col-12">
      {#VAR sub=list}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB nbnames}
<div class="card ">
      {#VAR $header header}
  <div class="card-body">
		<div class="row">
      <div class="col-12">
        <p>
          Diese Auflistung enth&auml;lt alle NetBIOS-Namen, die in diesem Netzwerk online sind oder waren.<br /><br />
        </p>
      </div>
			<div class="col-12">
      {#VAR sub=list}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB dnsnames}
<div class="card ">
      {#VAR $header header}
  <div class="card-body">
		<div class="row">
      <div class="col-12">
        <p>
          Diese Auflistung enth&auml;lt alle Hostnamen, die in diesem Netzwerk online sind oder waren.<br /><br />
        </p>
      </div>
			<div class="col-12">
      {#VAR sub=list}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB user}
<div class="card ">
      {#VAR $header header}
  <div class="card-body">
		<div class="row">
      <div class="col-12">
        {#VAR sub=list}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB conflicts}
<div class="card ">
      {#VAR $header header}
  <div class="card-body">
		<div class="row">
      <div class="col-12">
        <p>
          Diese Auflistung enth&auml;lt alle IP-Konflikte, d.h. alle MAC-Adresse, von denen zwei die gleiche IPv4 verwenden.<br />
          Sobald eine MAC seine IPv4 welchselt (und damit den Konflikt beseitigt) wird sie in dieser Liste nicht mehr aufgef&uuml;hrt.<br /><br />
        </p>
      </div>
			<div class="col-12">
      {#VAR sub=listtpl}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB wrong}
<div class="card ">
      {#VAR $header header}
  <div class="card-body">
		<div class="row">
      <div class="col-12">
          <p>
            Sobald sich ein User im FLIP einloggt, wird &uuml;berpr&uuml;ft, ob er auch die IP verwendet, die ihm &uuml;ber das
            FLIP zugewiesen wurde. Tut er das nicht, ist er ein Kandidat f&uuml;r diese Liste. Wenn er nach dem einloggen seine IP
            ge&auml;ndert hat und der netlogd dies anhand seiner MAC nachvollziehen kann, so taucht er nicht mehr in der Liste auf.<br />
            Da es m&ouml;glich ist, dass sich der User vom Rechner eines Kollegen aus eingeloggt hat, wird auch &uuml;brpr&uuml;ft,
            ob es vielleicht noch einen anderen Rechner im Netzwerk gibt, der die zugewiesene IP dieses Users verwendet. Ist so ein Rechner
            vorhanden, wird seine MAC in der Spalte <b>mrIP</b> (<u>m</u>it <u>r</u>ichtiger <u>IP</u>) angezeigt.<br /><br />
          </p>
      </div>
			<div class="col-12">
        {#TABLE $items maxrows=100 class=wide_table}
          {#COL LastSeen $time}<nobr>{#IF $time > $header.last_visit}<span class="important">{#DATE $time}</span>{#ELSE}{#DATE $time}{#END}</nobr>
          {#COL On align=center}{#VAR sub=onstat}
          {#COL MAC $mac}
          {#COL "Verwendete IPv4" $ipv4}
          {#COL "Zugewiesene IPv4" $user_ip}
          {#COL Name $hostname}
          {#COL User $name}<a href="user.php?frame=viewsubject&amp;id={$user_id}">{%name}</a>
          {#COL Platz %user_seat}
          {#COL mrIP}
            {#IF $correct}
              <a style="color:green;" href="netlog.php?frame=search&amp;type=matches&amp;column=mac&amp;search={$correct.mac}" title="{%correct.nic_vendor}">{$correct.mac}</a>
            {#ELSE}
              <span style="color:red;"><b>Niemand</b></span>
            {#END}
        {#END}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB unknownmacs}
<div class="card ">
      {#VAR $header header}
  <div class="card-body">
		<div class="row">
      <div class="col-12">
        <p>
          Diese Auflistung enth&auml;lt alle MAC-Adresse, die <b>nicht</b> &uuml;ber das IEEE einem Hersteller zugeordnet werden k&ouml;nnen.<br />
          Solche MACs k&ouml;nnen Fakes sein, von einem unseri&ouml;sen Hersteller kommen oder zu einer WindowsXP-Netzwerkbr&uuml;cke geh&ouml;ren.<br /><br />
        </p>
      </div>
			<div class="col-12">
      {#VAR sub=listtpl}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB search}
<div class="card ">
      {#VAR $header header}
  <div class="card-body">
		<div class="row">
      <div class="col-12">
        {#FORM method=get keepparams=1}
          <table class="table" cellspacing="1">
            <tr>
            <td colspan="5" class="tdedit"><b>Suchen im NetLog</b></td></tr>
            <tr valign="middle">
            <td class="tdcont">Suche: {#INPUT search string $search}</td>
            <td class="tdcont">Art: {#INPUT type dropdown $type param=$types}</td>
            <td class="tdcont">Spalten: {#INPUT column dropdown $column param=$columns}</td>
            <td class="tdaction">{#SUBMIT Finden!}</td>
          </tr>
          </table>
        {#END}
      </div>
			<div class="col-12">
        {#TABLE $items maxrows=100 foundrows=$foundrows dborder=1 class=wide_table}
          {#COL LastSeen $time}<nobr>{#IF $time > $header.last_visit}<span class="important">{#DATE $time}</span>{#ELSE}{#DATE $time}{#END}</nobr>
          {#COL MAC $mac}<a href="netlog.php?frame=search&amp;type=matches&amp;column=mac&amp;search={$mac}">{$mac}</a>
          {#COL IPv4 $ipv4}<a href="netlog.php?frame=search&amp;type=matches&amp;column=ipv4&amp;search={$ipv4}">{$ipv4}</a>
          {#COL DNS $dnsname}<a href="netlog.php?frame=search&amp;type=matches&amp;column=dnsname&amp;search={$dnsname}">{$dnsname}</a>
          {#COL NBName $nbname}<a href="netlog.php?frame=search&amp;type=matches&amp;column=nbname&amp;search={$nbname}">{$nbname}</a>
          {#COL NBGroup $nbgroup}<a href="netlog.php?frame=search&amp;type=matches&amp;column=nbgroup&amp;search={$nbgroup}">{$nbgroup}</a>
          {#COL Action $action}
          {#COL Weiteres}
            {#IF !empty($name)}User: <a href="netlog.php?frame=search&amp;type=matches&amp;column=user&amp;search={%name}">{%name}</a> <a href="user.php?frame=viewsubject&amp;id={$user_id}">(i)</a><br />{#END}
            {#IF !empty($user_ip)}Zugewiesene IP: {$user_ip}<br />{#END}
            {#IF !empty($user_seat)}Platz: {$user_seat}<br />{#END}
            {#IF !empty($useragent)}UserAgent: {%useragent}<br />{#END}
            {#IF !empty($description)}Anmerkung: {%description}<br />{#END}
            {#IF !empty($nic_vendor)}NIC: {%nic_vendor}<br />{#END}
        {#END}
			</div>
		</div>
	</div>
</div>
{#END}