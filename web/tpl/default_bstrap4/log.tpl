{#SUB viewdblog}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
        {#IF count($types) > 1}
          <h3>
          {#FOREACH $types $t}
            <a {#WHEN "$t == $type" "class=\"important\""} href="log.php?frame=viewdblog&amp;type={$t}">{$t}</a>
          {#MID}
            &middot;
          {#END}
          </h3>
        {#END}
        {#TABLE $items maxrows=100 foundrows=$foundrows dborder=1 class="wide_table"}
          {#COL Zeit $time}<nobr>{#IF $time > $last_visit}<span class="important">{#DATE $time}</span>{#ELSE}{#DATE $time}{#END}</nobr>
          {! #COL Session $session_id}
          {#COL User %user}
          {#COL Nachricht $message}
        {#END}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB backtrace}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
        <table class="table" cellspacing="1">
          <tr><td align="right" class="tdcont">Type&nbsp;</td><td class="tdcont">{%error}</td></tr>
          <tr><td align="right" class="tdcont">Datei&nbsp;</td><td class="tdcont">{%file}[{%line}]</td></tr>
          <tr><td align="right" class="tdcont">Nachricht&nbsp;</td><td class="tdcont"><b><span class="important">{%message}</span></b></td></tr>
          <tr><td align="right" class="tdcont">Debug&nbsp;</td><td class="tdcont"><pre>{%debug}</pre></td></tr>
        <tr><td align="right" class="tdcont">Parameter&nbsp;</td><td class="tdcont"><pre>{#DEBUGPRINT $args}</pre></td></tr>
        </table>
        <br />
        {#TABLE $trace}
          {#COL function style="font-family:'Courier New'" valign="top"}
          <nobr><b style="font-family:'Courier New'">{%class}{%type}{%function}()</b></nobr>
          {#IF !empty($file)}<br /><nobr>{%file}[{$line}]</nobr>{#END}
        {#COL args}<pre>{$args}</pre>
        {#END}
			</div>
		</div>
	</div>
</div>
{#END}
