{#SUB text}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
            {$text}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB translate}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
            {#FORM translation}
            {#BACKLINK zur&uuml;ck}<br/>
            <h3>{§Original}:</h3>
            {#INPUT gtext text $gtext enabled=0}{#INPUT orig hidden $gtext}<br/>
            <h3>{§&Uuml;bersetzung}:</h3>
            {§nach}: {#INPUT locale dropdown $locale $locales}<br/>
            {#INPUT text text}<br/>
            {#INPUT hash hidden $hash}
            {#SUBMIT}
            {#END}
			</div>
		</div>
	</div>
</div>
{#END}
