{#SUB text}
  <pre>
  {%text}
  </pre>
{#END}

{#SUB msg}
  <br />
  <p>
  {$text}
  </p><br />
  {#IF !empty($backurl)}<a href="{%backurl}">Zur&uuml;ck</a><br />{#END}
  <br />
{#END}

{#SUB confirmation}
  <br />
  <p><span style="color: red">{$msg}</span></p>
  <br />
  {#FORM action=$get}
    {#FOREACH $posts $val $key}
      {#IF is_array($val)}
        {#FOREACH $val $v}{#INPUT $key[] hidden $v}{#END} 
      {#ELSE}
        {#INPUT $key hidden $val}
      {#END}
    {#END}
    <p>{#SUBMIT "&nbsp;&nbsp;Ja&nbsp;&nbsp;"}</p>
    <p>{#BACKLINK Zur&uuml;ck}</p>
  {#END}
{#END}