{#SUB blocktools}
<div class="card ">
	<div class="card-header">
		<div class="row">
			<div class="col-12">
			<a href="seats.php?frame=blockgraph"{#WHEN "$frame=='blockgraph'" " class='btn btn-md btn-primary'"} class="btn btn-md btn-secondary" role="button"><i class="far fa-file-image"></i>&nbsp;&Uuml;bersicht</a> 
			<a href="seats.php?frame=blocklist"{#WHEN "$frame=='blocklist'" " class='btn btn-md btn-primary'"} class="btn btn-md btn-secondary" role="button"><i class="fas fa-list"></i>&nbsp;Liste</a>
			<a href="seats.php?frame=block&amp;blockid={$id}"{#WHEN "$frame=='block'" " class='btn btn-md btn-primary'"} class="btn btn-md btn-secondary" role="button"><i class="fas fa-map-marker-alt"></i>&nbsp;Reservierungen</a>
			</div>
		</div>
		<div class="alert alert-info" role="alert" style="margin-bottom: 0px;">
			<div class="row">
				<div class="col-8 col-sm-8 col-md-8">
						<div><b>Admin Sitzplatzverwaltung:</b></div>
							&nbsp;<a href="seatsedit.php?frame=editblock&amp;id={$id}" {#WHEN "$frame=='editblock'" " class='btn btn-md btn-primary'"} class="btn btn-secondary btn-sm" role="button"><i class="fas fa-edit"></i>&nbsp;Block Daten editieren</a>
							&nbsp;<a href="seatsedit.php?frame=editblocks"{#WHEN "$frame=='editblocks'" " class='btn btn-md btn-primary'"} class="btn btn-sm btn-secondary" role="button"><i class="fas fa-edit"></i>&nbsp;Bl&ouml;cke Bearbeiten</a> 
							
				</div>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="row justify-content-start">
			<div class="col-2">
			Action
			</div>
			<div class="col-6">
			Beschreibung
			</div>
			<div class="col-4">&nbsp;</div>
			<div class="col-2">
			{#ACTION "neu zeichnen" redraw params="array(ids[]=$id)"}
			</div>
			<div class="col-6">
			  Der unten angezeigte Sitzplan wird neu gezeichnet und Werte in der
						  Sitzplan-Tabelle, die von anderen anderen abh&auml;ngig sind,
						  werden neu berechnet.
			</div>
			<div class="col-4">&nbsp;</div>
			<div class="col-2">
			{#ACTION "Demo erstellen" demo params="array(blockid=$id type=demo)" confirmation="Soll der Sitzplan wirklich neu gezeichnet werden? (Der alte Sitzplan wird dabei gel&ouml;scht!)"}
			</div>
			<div class="col-6">
			Erstellt einen Demo-Sitzplan.<br />
						  <span class="important">Aber Vorsicht</span>, der alte Sitzplan wird dabei gel&ouml;scht!
			</div>
			<div class="col-4">&nbsp;</div>
			
			<div class="col-2">
			<a href="seatsedit.php?frame=edit&amp;id={$id}"{#WHEN "$frame=='edit'" " class='btn btn-md btn-primary'"} class="btn btn-sm btn-secondary" role="button"><i class="fas fa-edit"></i>&nbsp; bearbeiten</a>
			</div>
			<div class="col-6">
				Es k&ouml;nnen einzelne Sitzpl&auml;tze erstellt und bearbeitet werden oder ganze Reihen hinzugef&uuml;gt werden.
			</div>
			<div class="col-4">&nbsp;</div>
			
			</div>
		</div>
		<div class="row" align="left">
			<div class="col-12">	
				{#FOREACH $seats}
						<a href="seatsedit.php?frame=editseat&amp;blockid={$block_id}&amp;id={$id}" alt="{$name}" data-toggle="tooltip"  data-html="true" title="<b>{$name}</b> - <em>{$title}</em>">
						<div style="text-align:center; position:absolute; top: {$center_y}px; left: {$center_x}px; width: 40px; height: 40px;
									-ms-transform: rotate(-{$angle}deg); /* IE 9 */
									-webkit-transform: rotate(-{$angle}deg); /* Safari */
									transform: rotate({-$angle}deg); /* Standard syntax */"
									id="{$name}" >
							<button type="button" class="btn btn-success btn-block btn-sm" style="width: 100%; height:100%;"><i class="fas fa-asterisk"></i></button><i class="far fa-user-circle"></i>
						</div>
						</a>
				{#END}
				<div class="clear"></div>
				{#DBIMAGE $background_tmp style="margin-top:10px";}
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>
{#END}

{#SUB editblocks}
<div class="card ">
	<div class="card-header">
		{#VAR sub=header file=inc.seats.tpl}
		<div class="alert alert-info" role="alert" style="margin-bottom: 0px;">
			<div class="row">
				<div class="col-8 col-sm-8 col-md-8">
						<div><b>Admin Sitzplatzverwaltung:</b></div>
							&nbsp;<a href="seatsedit.php?frame=editblock" {#WHEN "$frame=='editblock'" " class='btn btn-md btn-primary'"} class="btn btn-secondary btn-sm" role="button"><i class="fas fa-edit"></i>&nbsp;{§neu} Block erstellen</a>
				</div>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12">
			<p>{§Hier k&ouml;nnen Segmente erstellt, bearbeitet und gel&ouml;scht werden.}</p>
			{#TABLE $blocks}
			  {#COL }<a href="{%href}">{#DBIMAGE $background_tmp process="createThumbnail(100,64)" border=0}</a>
			  {#COL "Titel / Beschreibung / Link-Koordinaten" %caption}
				<b><a href="{%href}">{%caption}</a></b><br />
				{%description}<br />
				<i>{%cords}</i>{#IF !empty($link)} (<a href="{%link}">{%link}</a>){#END}
			  {#COL "Position<br />Skalierung" align="center"}{$topleft_x}/{$topleft_y}<br />{$scale}
			  {#COL bearbeiten align="center"}
				<a href="seatsedit.php?frame=editblock&amp;id={$id}" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-edit"></i>&nbsp;daten</a><br />
				<a href="seatsedit.php?frame=blocktools&amp;id={$id}" class="btn btn-secondary btn-sm" role="button"><i class="fas fa-edit"></i>&nbsp;grafisch</a>
			  {#OPTION "L&ouml;schen" deleteblock}
			  {#OPTION "Neu zeichnen" redraw}
			  {#OPTION "Link-Koordinaten neu berechnen" calccords}
			{#END}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB editblock}

<div class="card ">
	<div class="card-header">
		<div class="row">
			<div class="col-12">
			<a href="seatsedit.php?frame=editblocks"class="btn btn-secondary btn-sm" role="button"><i class="fas fa-arrow-left"></i>&nbsp;{§zur&uuml;ck}</a>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12">
			{#FORM block}
			{#INPUT id hidden $id}
			<table class="table table-bordered table-striped table-sm" width="80%">
			  <tr><td align="right">{§Titel}:</td><td>{#INPUT caption string $caption}</td></tr>
			  <tr><td align="right">{§Leserecht}:</td><td>{#INPUT view_right rights $view_right}</td></tr>
				<tr><td align="right">{§Koordinaten auf &Uuml;bersicht}<sup>1</sup>:</td>
					<td>
					<div class="row">
					<div class="col-1">X:</div><div class="col-11">{#INPUT topleft_x integer $topleft_x}</div>
					<div class="col-1">Y:</div><div class="col-11">{#INPUT topleft_y integer $topleft_y}</div>
					</td>
				</tr>
			  <tr><td align="right">{§Skalierung auf &Uuml;bersicht}<sup>2</sup>:</td><td>{#INPUT scale decimal $scale} (0 => keine Anzeige in &Uuml;bersicht)</td></tr>
			  <tr><td align="right">{§Block-Tischbilder}:</td><td>{#INPUT imagedir_block longstring $imagedir_block allowempty=0}</td></tr>
			  <tr><td align="right">{§&Uuml;bersichts-Tischbilder}:</td><td>{#INPUT imagedir_overview longstring $imagedir_overview allowempty=0}</td></tr>
			  <tr><td align="right">{§Link}<sup>3</sup>:</td><td>{#INPUT link longstring $link}</td></tr>
			  <tr><td align="right">{§Link-Koordinaten}<sup>4</sup>:</td><td>{#INPUT cords longstring $cords}</td></tr>
			  <tr><td align="right">{§&uuml;18-Block}<sup>5</sup>:</td><td>{#INPUT is_adult checkbox $is_adult}</td></tr>
			  <tr><td align="right">{§Beschreibung}:</td><td>{#INPUT description text $description}</td></tr>
			  <tr><td align="right">{§Hintergrund}:</td><td>{#INPUT background image $background}</td></tr>
			  <tr><td align="center" colspan="2">&nbsp;</td></tr>
			  <tr><td align="center" colspan="2">{#SUBMIT Speichern}</td></tr>
			  <tr><td align="center" colspan="2">&nbsp;</td></tr>
			  <tr><td align="center" colspan="2">{#BACKLINK Zur&uuml;ck}</td></tr>
			  <tr><td align="center" colspan="2">&nbsp;</td></tr>
			  <tr><td align="center" colspan="2">
				<p align="justify">
					<span class="important">Wichtig:</span> Viele &Auml;nderungen an diesen Daten zeigen sich erst, 
					nachdem der Sitzplan neu gezeichnet wurde!<br />
					<br />
					<sup>1</sup> Die Koordinaten auf der &Uuml;bersicht, geben an, an welcher Stelle die 
					Tische dieses Sitzblockes auf der &Uuml;bersichtsgrafik gezeichnet werden sollen.<br />
					<sup>2</sup> Die Skalierung auf der &Uuml;bersicht, ist ein Faktor, mit dem alle 
					Tischkoordinaten multipliziert werden, bevor sie auf der &Uuml;bersicht eingezeichnet werden. 
					Mit einem Skalierungs-Faktor von <i>0.5</i> ist Sitzblock auf der &Uuml;bersicht ale genau halb so gro&szlig;,
					wie in der Blockansicht. Achtung: Ist der Skalierungs-Faktor 0, werden die Tische nicht in 
					die &Uuml;bersicht eingezeichnet.<br />
					<sup>3</sup> Ein Sitzblock muss nicht notwendiger weise Tische enthalten. Ist dieser 
					Link angegeben, so verweist die &Uuml;bersicht nicht auf eine Blockansicht, sondern auf 
					eben diesen Link. Beispielsweise kann ein User, der auf den Stand des Caterers clickt, 
					so direkt aus die Speisekarte gef&uuml;hrt werden.<br />
					<sup>4</sup> Die Link-Koordinaten werden direkt in die Imagemap in der &Uuml;bersicht eingetragen.
					Wenn der Sitzblock Tische enth&auml;lt, k&ouml;nnen (und sollten) sie in der Blockliste automatisch 
					generiert werden.
					<br />Beispiel wenn im Template mit "area" und "map" die Koordinaten definirt werden:<b><i>shape="rect" coords="149,211,351,404"</i></b>
					<br />Beispiel wenn im Template mit "div" die Koordinaten definirt werden:<b> <i> top: 70px; left: 100px; width: 370px; height: 760px; background-color: hsla(210, 100%, 50%, 0.5);</i></b><br />
					<br />Beispiel wenn im Template mit "div" die Koordinaten definirt werden:<b> <i> top: 70px; left: 100px; width: 370px; height: 760px; background-color: rgba(255,0,0,0.5); </i></b><br />
					<sup>5</sup> Markiert diesen Block explizit f&uuml;r &uuml;ber-18-J&auml;hrige. Damit k&ouml;nnen f&uuml;r diesen Block
					im Checkin besondere abfragen get&auml;tigt oder im Turniersystem besondere Spiele 
					freigeschaltet werden.
				</p>
			  </td></tr>
			</table>
			{#END}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB edit}
<div class="card ">
	<div class="card-header">
		<div class="row">
			<div class="col-12">
			<a href="seats.php?frame=blockgraph"{#WHEN "$frame=='blockgraph'" " class='btn btn-md btn-primary'"} class="btn btn-md btn-secondary" role="button"><i class="far fa-file-image"></i>&nbsp;&Uuml;bersicht</a> 
			<a href="seats.php?frame=blocklist"{#WHEN "$frame=='blocklist'" " class='btn btn-md btn-primary'"} class="btn btn-md btn-secondary" role="button"><i class="fas fa-list"></i>&nbsp;Liste</a>
			<a href="seatsedit.php?frame=blocktools&amp;id={$block.id}"{#WHEN "$frame=='blocktools'" " class='btn btn-md btn-primary'"} class="btn btn-md btn-secondary" role="button"><i class="fas fa-map-marker-alt"></i>&nbsp;zur&uuml;ck zur Blockseite</a>
			</div>
		</div>
		<div class="alert alert-info" role="alert" style="margin-bottom: 0px;">
			<div class="row">
				<div class="col-4 col-sm-4 col-md-4">
						<div><b>Admin Sitzplatzverwaltung:</b></div>
							&nbsp;<a href="seatsedit.php?frame=editseat&amp;blockid={$block.id}&amp;id=new" {#WHEN "$frame=='editblocks'" " class='btn btn-md btn-primary'"} class="btn btn-md btn-secondary" role="button"><i class="far fa-plus-square"></i>&nbsp;Sitzplatz hinzuf&uuml;gen</a>
				</div>
				<div class="col-8 col-sm-8 col-md-8">
				<form method="post">
					<input type="hidden" name="action" value="deleteall"/>
					<input type="hidden" name="blockid" value="{$block.id}"/>
					<input type="hidden" name="confirmation" value="Soll der Sitzplan '{%block.caption}' wirklich komplett <span class=important>gel&ouml;scht</span> werden?"/>
				<input type="submit" value="Sitzplan l&ouml;schen"/>
				</form>
				</div>
			</div>
		</div>
	</div>
	<div class="card-body">
	<h3>Sitzpl&auml;tze:</h3>
		<div class="row">
			<div class="col-12">
				<table class="table table-bordered table-striped table-hover table-sm">
					<tbody> 
						{#FOREACH $rows}
						<tr>
							{#FOREACH $cols}
							<td>{#IF isset($id)}<a href="seatsedit.php?frame=editseat&amp;blockid={$block.id}&amp;id={$id}">{$name}</a>{#ELSE}&nbsp;{#END}</td>
							{#END}
						</tr>
						{#END}
					<tbody> 
				</table>
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB editseat}
<div class="card ">
	<div class="card-header">
		<div class="row">
			<div class="col-12">
			<a href="seatsedit.php?frame=edit&amp;id={$block_id}"class="btn btn-secondary btn-sm" role="button"><i class="fas fa-arrow-left"></i>&nbsp;{§zur&uuml;ck}</a>
			<a href="seatsedit.php?frame=blocktools&amp;id={$block_id}"class="btn btn-secondary btn-sm" role="button"><i class="fas fa-arrow-left"></i>&nbsp;{§zur&uuml;ck zur Blockseite}</a>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12">
				Hinweis: Eine Standard-Tischgrafik ist {$width0}x{$height0} Pixel gross.<br />
				<table style="margin:10px;border-collapse:collapse;">
				<tr><th style="padding:10px;border:1px dashed gray;">einzelner Sitzplatz:</th>
				<td style="padding:10px;border:1px dashed gray;">
				{#FORM editseat}
				{#INPUT id hidden $id}
				{#INPUT block_id hidden $block_id}
				<table>
				<tr>
				<th>Zentrum X/Y:</th>
				<td>{#INPUT center_x integer $center_x allowempty=0}/{#INPUT center_y integer $center_y allowempty=0}</td>
				</tr>
				<tr>
				<th>Winkel:</th>
				<td>{#INPUT angle integer $angle}</td>
				</tr>
				<tr>
				<th>Name:</th>
				<td>{#INPUT name string $name}</td>
				</tr>
				<tr>
				<th>IP:</th>
				<td>{#INPUT ip IP $ip}</td>
				</tr>
				</table>
				{#SUBMIT}<br />
				{#END}
				{#IF $id!="new"}
				<br />
				{#FORM delseat}{#SUBMIT l&ouml;schen shortcut=l}{#INPUT seatid hidden $id}{#END}
				{#END}
				</td></tr>
				<tr><th style="padding:10px;border:1px dashed gray;">Sitzplatzreihe:</th>
				<td style="padding:10px;border:1px dashed gray;">
				{#FORM addrow}
				{#INPUT id hidden $id}
				{#INPUT block_id hidden $block_id}
				<table>
				<tr>
				<th>Start Zentrum X/Y:</th>
				<td>{#INPUT center_x integer $center_x allowempty=0}/{#INPUT center_y integer $center_y allowempty=0}</td>
				</tr>
				<tr>
				<th>Abstand X/Y:</th>
				<td>{#INPUT spacex integer $spacex}/{#INPUT spacey integer $spacey}</td>
				</tr>
				<tr>
				<th>Anzahl:</th>
				<td>{#INPUT seatno integer $seatno} Sitzpl&auml;tze</td>
				</tr>
				<tr>
				<th>Winkel:</th>
				<td>{#INPUT angle dropdown $angle param=$angles}</td>
				</tr>
				<tr>
				<th valign="top">Namen:</th>
				<td>
				  <table><tr><td>Zeilenenpr&auml;fix</td><td>{#INPUT namerowprefix string $namerowprefix}</td></tr>
				  <tr><td>Beschriftungsanfang</td><td>{#INPUT namerowstart string $namerowstart}</td></tr>
				  <tr><td>Zeilenensuffix</td><td>{#INPUT namerowsuffix string $namerowsuffix}</td></tr>
				  <tr><td>Spaltenpr&auml;fix</td><td>{#INPUT namecolprefix string $namecolprefix}</td></tr>
				  <tr><td>Beschriftungsanfang</td><td>{#INPUT namecolstart string $namecolstart}</td></tr>
				  <tr><td>Spaltensuffix</td><td>{#INPUT namecolsuffix string $namecolsuffix}</td></tr>
				  </table>
				</td>
				</tr>
				<tr>
				 <th>Benennungsmodus:</th>
				 <td>{#INPUT namingmode dropdown $namingmode param=$namingmodes}</td>
				</tr>
				<tr>
				 <th>erste IP:</th>
				 <td>
				   {#INPUT ipa integer $ipa}.{#INPUT ipb integer $ipb}.{#INPUT ipc integer $ipc}.{#INPUT ipd integer $ipd}<br />
				 </td>
				</tr>
				<tr>
				  <th>IPschema:</th>
				  <td>{#INPUT xy dropdown $xy param=$xyarr}</td>
				</tr>
				</table>
				{#SUBMIT shortcut=r}<br />
				{#END}
				</td></tr>
				</table>
			</div>
		</div>
	</div>
</div>
{#END}
