{#SUB default}
<div class="card ">
	<div class="card-header">
		<div class="row">
			<div class="btn-group">
			{#FOREACH $types $t}
				{#IF $t == $type}<a href="#" class="btn btn-primary ml-1" role="button">{#UCFIRST $t}</a>
				{#ELSE}<a href="user.php?type={$t}" class="btn btn-secondary ml-1" role="button">{#UCFIRST $t}</a>{#END}{#MID}
			{#END}
			<a href="user.php?frame=editproperties&amp;id=create_new_subject&amp;type={$type}" class="btn btn-success ml-1" role="button"><i class="fas fa-plus"></i>&nbsp;{§add}&nbsp;{$type}</a>
			</div>
		</div>
		<div class="row">
		<div class="col-12">
		<p>{§"Rechte &uuml;ber" f&uuml;r Eigenschaften verwenden}: {$userightsover}</p>
		</div>
		</div>
	</div>
</div>
<div class="card border-info mb-3">
	<div class="card-header">
			<div class="row">
			<div class="col-12">
				{#TPL $text}
			</div>
			</div>
	</div>
</div>
<div class="card ">
	<div class="card-body">
		<div class="row">
			<div class="col-8">
				<b>Subjektsuche</b>
				<!-- 
				{#FORM subjectsearch method=get}
				 {#INPUT type hidden $type}
				 {#INPUT search string} 
				 {#SUBMIT Los}
				{#END}
				 -->
				<form action="search.php" accept-charset="utf-8">
					<div class="input-group">
						<div class="input-group-prepend"><div class="input-group-text"><i class="fas fa-search"></i></div></div>
						<input type="hidden" name="mode" value="default" class="form-control"/>
						<input type="hidden" name="usedmodules[user]" value="1" class="form-control"/>
						<input type="text" name="searchstring" value="" class="form-control"/>
						<input type="submit" class="button" value="suchen (Alt+S)" accesskey="s">
					</div>
				</form>
				</div>
			<div class="col-4"></div>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
		<div class="col table-responsive">
		{#TABLE $items maxrows=100 class="table table-sm table-hover table-striped"}
			{#IF $itemcount < 2}
				<b>{$itemcount} Eintrag</b>
			{#ELSE}
				<b>{$itemcount} Eintr&auml;ge</b>
			{#END}
			{#COL Name $name}<a href="user.php?frame=viewsubject&amp;id={$id}">{%name}</a>
			{#COL EMail %email}{#IF empty($email)}<i>Keine</i>{#ELSE}{%email}{#END}
			{! #ACTION login changeuser uri=user.php right=admin condition="$type==user"}
			{#OPTION L&ouml;schen deletesubjects condition="$type!='systemuser'" confirmation="Die gew&auml;hlten Subjekte werden aus dem System gel&ouml;scht - fortfahren?"}
		{#END}
		</div>
		</div>
	</div>
</div>
{#END}

{#SUB frames}
<div class="card-header">	
	<div class="row">
		<div class="btn-group" role="group">
			{#IF $showback}<a href="user.php?type={$type}" class="btn btn-secondary btn-md" role="button"><i class="fas fa-reply"></i>&nbsp;{§Zur&uuml;ck}</a>&nbsp;
			{#END}
			{#FOREACH $frames $v $k}<a {#WHEN "$frame == $k" "class=\"btn btn-primary btn-md\"" "role=\"button\""} href="user.php?frame={$k}&amp;id={$id}" class="btn btn-secondary btn-md" role="button">{$v}</a>{#MID}&nbsp;{#END}
			&nbsp;<a href="lanparty.php" class="btn btn-secondary btn-md" role="button"><i class="fas fa-users"></i>&nbsp;{§Teilnehmer}</a>
		</div>
	</div>
</div>

{#END}

{#SUB viewsubject}
<div class="card ">
	<div class="card-body">
		{#VAR sub=frames}
	</div>
	{#WITH $user}
		
		{#IF $type != "group"}
			{#IF $allowchange}
			<div class="card-header">
				<div class="alert alert-info" role="alert" style="margin-bottom: 0px; margin-top: 5px;">
					<div class="btn-group" role="group">
						{#IF $checkin}<a href="checkin.php?search={$id}" class="btn btn-secondary btn-md" role="button"><i class="far fa-check-square"></i>&nbsp;{§Zum Checkin}</a>{#END}
						{#IF $type != "group"}{#IF $allowchange}{#ACTION "Als %name einloggen" changeuser params="array(id=$id)" buttonclass="btn btn-warning btn-md"}{#END}{#END}
						{#IF $logged_in=="true"}<a href="webmessage.php?frame=sendmessage&amp;receiver={#URL $name}" class="btn btn-secondary btn-md" role="button"><i class="far fa-comment"></i>&nbsp;{§Nachricht senden}</a>{#END}
						{#IF $catering}<a href="shop.php?frame=adminusers&userid={$id}" class="btn btn-secondary btn-md" role="button"><i class="fas fa-shopping-cart"></i>&nbsp;{§Bestellungen einsehen}</a>{#END}
					</div>
				</div>
			</div>
			{#END}
		{#END}

		<div class="card-body">
			<div class="row">
				{#IF $type == "user"}
				<div class="col col-md-auto">
					{#IF !empty($user_img)}
					{#DBIMAGE $user_img process=createthumbnail(150,150)}
				</div>
				{#END}
				<div class="col col-md-auto col-lg-auto table-responsive">
						<table class="table table-sm table-hover table-striped">
						<thead>
							<tr>
								<th>Firstname</th>
								<th>Lastname</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{§Nickname}:</td><td>{%name}</td>
							</tr>
							<tr>
								<td>{§Homepage}:</td><td><a href="{%homepage}" target="_blank">{%homepage}</a></td>
							</tr>
							<tr>
								<td>{§Status}:</td><td>{$status_name}</td>
							</tr>
							<tr>
								<td>{§Sitzplatz}:</td><td>{#IF empty($seat)}<i>{§nicht reserviert}</i>{#ELSE}<b><a href="seats.php?frame=block&amp;blockid={$blockid}&amp;ids[]={$id}">{$seat}</a></b>{#END}</td>
							</tr>
							{#IF !empty($ip)}
							<tr>
								<td>{§IP-Adresse}:</td><td>{$ip}</td>
							</tr>
							{#END}
							<tr>
								<td>{§Beschreibung}:</td><td>{$description}</td>
							</tr>
						</table>
				</div>
			</div>
		</div>
		{#ELSEIF $type == "group"}
			<div class="card-body">
				<div class="row">
					<div class="col table table-responsive">
						<table class="table table-sm">
						  <tr>
							<td>{%description}</td>
						  </tr>
						  {#IF isset($colvals) and isset($cols)}
						  <tr>
							<td>
							  <table class="table" class="table table-sm table-hover table-striped">
								<tr>
								  <th class="tdedit">Name</th>
								  {#FOREACH $cols $c}<th class="tdedit">{$c}</th>{#END}
								</tr>
								{#FOREACH $colvals}
								<tr>
								  <td class="tdcont"><b><a href="user.php?frame=viewsubject&amp;id={$id}">{%name}</a></b></td>
								  {#FOREACH $cols $c $k}
									{#IF is_array($colvals[$name][$k])}
									  {#LOAD $colvals[$name][$k]['data'] $imgpath}
									  <td class="tdcont"><img src="{$imgpath}"></td>
									{#ELSE}
									  <td class="tdcont">{%$k}</td>
									{#END}
								  {#END}
								</tr>
								{#END}
							  </table>
							</td>
							{#END}
						  </tr>    
						</table>
					</div>
				</div>
			</div></div>
		{#END}
	{#END}
</div>
{#END}

{#SUB editproperties}
<div class="card ">
{#VAR sub=frames}
<div class="card-body">
	{#FORM properties}
	<table>
	  {#FOREACH $items}
		<tr>
		  <td align="right">{%caption}:</td>
		  {#IF $val_type=="Dropdown"}
			<td>{#INPUT $name $val_type $val allowempty=$allowempty enabled=$enabled param=$vals} {$hint}</td>
		  {#ELSE}
			<td>{#INPUT $name $val_type $val allowempty=$allowempty caption=$caption enabled=$enabled} {$hint}</td>
		  {#END}
		</tr>	
	  {#END}
	  {!der Templatecompiler kann nicht die Werte der Variablen vorraussehen und somit nicht wissen ob ein Uploadfeld vorhanden ist oder nicht ($val_type)}
	  {#IF false}{#INPUT justtomaketheformanuploadform file enabled=0}{#END}
	  <tr><td colspan="2">&nbsp;</td></tr>
	  <tr><td colspan="2">&nbsp;</td></tr>
	  <tr><td align="center" colspan="2">{#SUBMIT Speichern}</td></tr>
	  </table>
	{#INPUT subject_id hidden $id}
	{#IF $id=="create_new_subject"}{#INPUT type hidden $type}{#END}
	{#END}
	</div>
</div>
{#END}

{#SUB editrights}
<div class="card ">
{#VAR sub=frames}
<div class="card-body">
	{#TABLE $items}
	  {#COL Recht $right}<a href="user.php?frame=viewright&amp;id={$id}">{%right}</a>
	  {#COL "Geerbt von" $owner_name}{#IF $subject_id != $owner_id}<a href="user.php?frame=editrights&amp;id={$owner_id}">{%owner_name}</a>{#END}
	  {#COL "Kontrolle &uuml;ber" $controled_name}{#IF $controled_id}<a href="user.php?frame=editrights&amp;id={$controled_id}">{%controled_name}</a>{#END}
	  {#ACTION del deletesubjectright params="array(controled_id=$controled_id subject_id=$subject_id)" condition="$subject_id==$owner_id" uri="user.php?frame=editrights&amp;id=$subject_id"}
	{#END}
	{#FORM addright action="user.php?frame=editrights&amp;id=$subject_id"}
	<table class="table" cellspacing="1">
	  <tr>
		<th class="tdedit">{§Recht}:</td>
		<th class="tdedit">{§Kontrolle &uuml;ber}:</td>
		<th class="tdedit">&nbsp;</td>
	  </tr>
	  <tr>
		<td class="tdaction">{#INPUT right rights allowempty=0}</td>
		<td class="tdaction">{#INPUT subject subjects}</td>
		<td class="tdaction">{#SUBMIT Hinzuf&uuml;gen}</td>
	  </tr>
	</table>
	{#INPUT id hidden $subject_id}
	{#END}
	</div>
</div>
{#END}

{#SUB editchilds}
<div class="card ">
{#VAR sub=frames}
<div class="card-body">
	{#TABLE $childs}
	  {#COL Mitglieder $name width="150"}<a href="user.php?frame=viewsubject&amp;id={$id}">{%name}</a>
	  {! #ACTION del deletechild params="array(subject_id=$subject_id)" uri="user.php?frame=editchilds&amp;id=$subject_id"}
	  {#OPTION "Aus Gruppe entfernen" deletechilds}
	  {#OPTION "Verschieben nach: " movechilds}<br />{#INPUT group_id subjects param=group allowempty=0}
	  {#INPUT subject_id hidden $subject_id}
	{#END}
	{#FORM child action="user.php?frame=editchilds&amp;id=$subject_id"}
	<table class="table" cellspacing="1">
	  <tr>
		<th class="tdedit">{§neues Mitglied}:</td>
		<th class="tdedit">&nbsp;</td>
	  </tr>
	  <tr>
		<td class="tdaction">{#INPUT child_id subjects param=user allowempty=0}</td>
		<td class="tdaction">{#SUBMIT Hinzuf&uuml;gen}</td>
	  </tr>
	</table>
	{#INPUT subject_id hidden $subject_id}
	{#END}
	</div>
</div>
{#END}

{#SUB editparents}
<div class="card ">
{#VAR sub=frames}
<div class="card-body">
	{#TABLE $parents}
	  {#COL Gruppen $name width="150"}<a href="user.php?frame=viewsubject&amp;id={$id}">{%name}</a>
	  {#ACTION del deleteparent params="array(subject_id=$subject_id)" uri="user.php?frame=editparents&amp;id=$subject_id"}
	{#END}
	{#FORM parent action="user.php?frame=editparents&amp;id=$subject_id"}
	<table class="table" cellspacing="1">
	  <tr>
		<th class="tdedit">{§neue Gruppe}:</td>
		<th class="tdedit">&nbsp;</td>
	  </tr>
	  <tr>
		<td class="tdaction">{#INPUT parent_id subjects param=group}</td>
		<td class="tdaction">{#SUBMIT Hinzuf&uuml;gen}</td>
	  </tr>
	</table>
	{#INPUT subject_id hidden $subject_id}
	{#END}
	</div>
</div>
{#END}


{#SUB viewcolumns}
<div class="card ">
	<div class="card-header">
		<div class="row">
			<div class="col-12 my-3">
			{#TPL $text}
			</div>
			<div class="col-12">
			{#FOREACH $types $t}<a {#WHEN "$t==$type" "class=\"important btn btn-primary btn-md ml-1\""} class="btn btn-secondary ml-1" role="button" href="user.php?frame=viewcolumns&amp;type={$t}">{$t}</a> {#END}
			<a href="user.php?frame=editcolumn&amp;type={$type}" class="btn btn-success ml-1" role="button"><i class="far fa-plus-square"></i>&nbsp;{§add} COL </a>
			</div>
		</div>
	</div>
	<div class="card-body">
	{#TABLE $items class="table table-sm table-hover table-striped"}
	  <a href="user.php?frame=editcolumn&amp;type={$type}" class="btn btn-success btn-md ml-1" role="button" data-toggle="tooltip" title="{$type}"><i class="far fa-plus-square"></i>&nbsp; {§add}</a>
	  {#COL Name<br/>Bemerkung}<b>{#WHEN "$required=='Y'" "<span class=\"important\">%name</span>" %name}</b><br />{%comment}
	  {#COL Titel<br/>Hinweis}<b>{%caption}</b><br />{$hint}
	  {#COL Leserecht<br/>Schreibrecht}{#RIGHT $required_view_right view}<br />{#RIGHT $required_edit_right edit}
	  {#COL Typ}{$val_type}{#IF !empty($default_val)}<br /><i>{%default_val}</i>{#END}
	  {#FRAME edit editcolumn}
	  {#ACTION /\ switchcolumns params="array(this=$sort_index last=$last_index)" uri="user.php?frame=viewcolumns&amp;type=$type" condition=$last_index }
	  {#ACTION del delcolumn uri="user.php?frame=viewcolumns&amp;type=$type" confirmation="Soll die Spalte <span style=text-decoration:underline>$name</span> wirklich <strong>gel&ouml;scht</strong> werden?"}
	{#END}
	</div>
</div>
{#END}

{#SUB editcolumn}
<div class="card ">
	{#FORM column}
		<div class="card-header">
		{#BACKLINK Zur&uuml;ck} {#SUBMIT Speichern}
		</div>
		<div class="card-body table">
			<table class="table table-sm ">
			  <tr><td align="right">{§Name}:</td><td>{#INPUT name name $name}</td></tr>
			  <tr><td align="right">{§Bemerkung}:</td><td>{#INPUT comment longstring $comment}</td></tr>
			  <tr><td align="right">{§Titel}:</td><td>{#INPUT caption string $caption}</td></tr>
			  <tr><td align="right">{§Hinweis}:</td><td>{#INPUT hint longstring $hint}</td></tr>
			  <tr><td align="right">{§Erforderlich}:</td><td>{#INPUT required yesno $required}</td></tr>
			  <tr><td align="right">{§Leserecht}:</td><td>{#INPUT required_view_right rights $required_view_right param=infinite}</td></tr>
			  <tr><td align="right">{§Schreibrecht}:</td><td>{#INPUT required_edit_right rights $required_edit_right param=infinite}</td></tr>
			  <tr><td align="right">{§Typ}:</td><td>{#INPUT val_type inputtypes $val_type}</td></tr>
			  <tr><td align="right">{§min. L&auml;nge}:</td><td>{#INPUT min_length integer $min_length} (0 = {§unbegrenzt})</td></tr>
			  <tr><td align="right">{§max. L&auml;nge}:</td><td>{#INPUT max_length integer $max_length} (0 = {§unbegrenzt})</td></tr>
			  <tr><td align="right">{§Defaultwert}:</td><td><span class="edit" style="height:100%;">{%default_val}</span> ....
				<a href="config.php?frame=edit&amp;{#IF is_null($default_val)}key={$name}">{§Erstellen}{#ELSE}id={$name}">{§&auml;ndern}{#END}</a>
			  </td></tr>
			  <tr><td colspan="2" align="center"><br />{#SUBMIT Speichern}</td></tr>
			</table>
		{#INPUT id hidden $id}
		{#INPUT type hidden $type}
		</div>
	{#END}
</div>
{#END}

{#SUB viewrights}
<div class="card ">
	<div class="card-body">
	{#TPL $text}
	{#TABLE $items}
	  <a href="user.php?frame=editright">{§add}</a>
	  {#COL verg $count align=right}
	  {#COL Recht $right}{#IF $id != -1}<b><a href="user.php?frame=viewright&amp;id={$id}">{$right}</a></b>{#ELSE}<b>{$right}</b>{#END}
	  {#COL Beschreibung %description}
	  {#FRAME edit editright}
	  {#ACTION del deleteright condition="empty($count) and ($id != -1)" uri="user.php?frame=viewrights"}
	{#END}
	</div>
</div>
{#END}

{#SUB editright}
<div class="card ">
	{#FORM right}
		<div class="card-header">
		{#BACKLINK Zur&uuml;ck} {#SUBMIT Speichern}
		</div>
		<div class="card-body table">
			<table class="table table-sm ">
			  <tr><td align="right">{§Recht}:</td><td>{#INPUT right name $right}</td></tr>
			  <tr><td align="right">{§Beschreibung}:</td><td>{#INPUT description longstring $description}</td></tr>
			  <tr><td colspan="2" align="center"><br />{#SUBMIT Speichern}</td></tr>
			</table>
		{#INPUT id hidden $id}
		</div>
	{#END}
</div>
{#END}

{#SUB viewright}
<div class="card ">
	<div class="card-body">
	{#TPL $text}
	<p><a href="user.php?frame=viewrights">{§Zur&uuml;ck}</a></p>
	<p>{%description}</p>
	{#TABLE $subjects}
	  {#COL Besitzer $owner}<a href="user.php?frame=editrights&amp;id={$owner_id}">{%owner}</a>
	  {#COL Kontrollierter $controled}<a href="user.php?frame=editrights&amp;id={$controled_id}">{%controled}</a>
	{#END}
	</div>
</div>
{#END}

{#SUB checkin}
<div class="card ">
	<div class="card-header">
	User Checkin Frame alt! </br>
	Bitte verwende:<a href="checkin.php">das neue Checkin Modul</a>!
	</div>
</div>
{#END}

{#SUB register}
	{#FORM register action="user.php?frame=register"}
	
		<div class="card ">
		<div class="card-body">
			<div class="row">
				<div class="col-12">
				{#TPL $text}
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-12 col-xl-10">
					{#FOREACH $items}
					<div class="row form-group">
						 <label for="inputPassword" class="col-sm-4 col-md-3 col-lg-3 col-form-label">{%caption}:</label>
						<div class="col-sm-8 col-md-9 col-lg-9">
							{#INPUT $name $val_type allowempty=0 caption=$caption}{$hint}
						</div>
					</div>
					{#END} 
					{#IF $password == 'Y'} 
					<div class="row form-group">
						<label class="col-sm-4 col-md-3 col-lg-3 col-form-label">Passwort für die Anmeldung</label>
						<div class="col-sm-8 col-md-9 col-lg-9">
						{#INPUT lanparty_passwordd passwordquery}
						</div>
					</div>
					{#END}
					{#IF !empty($lanparty)}
					<div class="row my-3">
						<div class="col">
							<div class="card ">
							{#TPL $dsgvoeinwilligung}
							
							{#TPL $lanparty}
							<div class="container-fluid my-3">
							<div class="mx-auto" style="width: 300px;">
								<div class="checkbox ">
								<label style="font-size: 1.5em">
									{#INPUT lanparty_participate checkbox}
									<span class="cr"><i class="cr-icon fa fa-check"></i></span> Einverstanden
								</label>
								</div>
							</div>
						</div>
							
							
							
							</div>
						</div>
						
					
					</div>
					{#END}
					{#IF !empty($siteKey)}
						<div class="row">
						<div class="col offset-sm-2">
						<!-- VulkanLAN include recaptcha done by naaux and alra -->	
						<script src="https://www.google.com/recaptcha/api.js" async defer></script>
						<?php
						if ($resp != null && $resp->success) {
							echo "You got it!";
						}
						?>
						<form action="/" method="post">
								<fieldset>
									<legend>Recaptcha </legend>
									<div class="g-recaptcha" data-sitekey={$siteKey}></div>
									<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=de"></script>
									<br/>
									{#SUBMIT Registrieren}
								</fieldset>
							</form>
						<!-- recaptcha ende -->
						</div>
						</div>
						{#ELSE}
						{#SUBMIT Registrieren}
					{#END}
				</div>
				<div class=" col-xl-2"></div>
			</div>
			</div>
		</div>
	
	{#END}

	{#IF $lansurferimport == "Y"}
	<hr/>
	<h1>{§Account von Lansurfer &uuml;bernehmen}</h1>
	{#FORM lansurfer_import keepvals=1}
	<table>
	  <tr><td colspan="2">{§Daten von Lansurfer importieren (EMail-Adresse und Passwort von Lansurfer ben&ouml;tigt).}</td></tr>
	  <tr>
		<td align="right">Lansurfer-{§Email}:</td>
		<td>{#INPUT ls_email email allowempty=0}</td>
	  </tr>	
	  <tr>
		<td align="right">Lansurfer-{§Passwort}:</td>
		<td>{#INPUT ls_password passwordquery allowempty=0}</td>
	  </tr>
	  {#IF !empty($lanparty)}
	  <tr><td colspan="2">
		<table cellpadding="2" cellspacing="0"><tr><td>    
		  	{#INPUT lanparty_participate checkbox}
		</td><td>
		  {#TPL $lanparty}
		</td></tr></table>	
	  </td></tr>
	  {#END}
	  <tr><td colspan="2" align="center">{#SUBMIT importieren i}</td></tr>
	  {#IF count($ls_items) > 0}
	  <tr><td colspan="2"><br/>{§manuelle Eingabe, falls nicht von Lansurfer geliefert}:</td></tr>
	  {#FOREACH $ls_items}
		{#IF $name != "password"}
		<tr>
		  <td align="right">{%caption}:</td>
		  {#IF $val_type == "sex"} {! einige Inputs brauchen vorgegebene Werte (z.B. aus Dropdowns) }
		  <td>{#INPUT $name $val_type allowempty=0 caption=$caption} {$hint}</td>
		  {#ELSE}
		  <td>{#INPUT $name $val_type caption=$caption} {$hint}</td>
		  {#END}
		</tr>
		{#END}
	  {#END}  
	  <tr><td colspan="2" align="center">{#SUBMIT importieren i}</td></tr>
	  {#END}
	</table>
	{#END}
	{#END}
{#END}

{#SUB dsgvo}
	{#IF $dsgvocheck == "Y"}
	<div class="card ">
		<div class="card-body">
			<div class="row">
				{#TPL $text}
				{#TPL $dsgvoinfo} 
			</div>
			<hr>
			<div class="row justify-content-md-center">
				<div calls="col col-md-2">
				{#TPL $dsgvorevoke}
				</div>
			</div>
		</div>
	</div>
	{#ELSE}
	
	{#FORM dsgvo action="user.php?frame=dsgvo"}
		<div class="card ">
		<div class="card-body">
			<div class="row">
				<div class="col-12">
				{#TPL $text}
				</div>
			</div>
			<hr>

				<div class="row my-3">
					{#TPL $dsgvoeinwilligung}
					<div class="container-fluid my-3 justify-content-md-center">
					<p>Ich bestätige alles Vorstehende gelesen und verstanden zu haben.</p>
						<div class="mx-auto" style="width: 500px;">
							<div class="checkbox ">
							<label style="font-size: 1.2em">
								{#INPUT dsvgo1 checkbox}
								<span class="cr"><i class="cr-icon fa fa-check"></i></span> Einwilligung zur Datenverarbeitung. 
							</label>
							</div>
						</div>
					</div>
					</div>
					
				<hr>
					<div class="row ">
					<div class="col col-md-5">
					 {#SUBMIT Bestaetigung} 
					 </div>
					<div class="col col-md-1"> <p>oder</p>  </div>
					<div class="col col-md-6">
						{#TPL $useraccountrevoke}
					</div>
				</div>
				</div>
			</div>
		</div>
		{#INPUT id hidden $id}
	{#END}
	{#END}
{#END}

{#SUB resetpwd}
	{#TPL $text}
	<br />
	{#FORM reset}
	{§Ident}: {#INPUT email string}<br />
	<br />
	{#SUBMIT "Passwort zur&uuml;cksetzen"}
	{#END}
{#END}

{#SUB setpwd}
	{#FORM setpwd}
	{§Passwort}: {#INPUT password passwordedit}<br />
	<br />
	{#SUBMIT setzen}
	{#INPUT id hidden $id}
	{#INPUT code hidden $code}
	{#END}
{#END}

{#SUB resendactivation}
	{#TPL $text}
	<br />
	{#FORM resend}
	{§Ident}: {#INPUT ident string}<br />
	<br />
	{#SUBMIT "Email senden"}
	{#END}
{#END}

{#SUB changeemail}
	{#TPL $text}
	<br />
	{#FORM changeemail}
	<table>
	  <tr><td align="right">{§Ident}:</td><td>{#INPUT ident string}</td></tr>
	  <tr><td align="right">{§Password}:</td><td>{#INPUT pass passwordquery}</td></tr>
	  <tr><td align="right">{§neue Email-Adresse}:</td><td>{#INPUT email email}</td></tr>
	  <tr><td colspan="2">&nbsp;</td></tr>
	  <tr><td align="center" colspan="2">{#SUBMIT "Email &auml;ndern"}</td></tr>
	</table>
	{#END}
{#END}

{#SUB user4turnier}
	{#FORM user4turnier action="user.php?frame=user4turnier"}
	<p>{§Hier kann ein User erstellt werden, der sofort das Recht hat um an Turnieren teilzunehmen.}</p>
	<table>
	  {#FOREACH $items}
		<tr>
		  <td align="right">{%caption}:</td>
		  <td>{#INPUT $name $val_type allowempty=0 caption=$caption} {$hint}</td>
		</tr>	
	  {#END}  
	  <tr><td colspan="2">&nbsp;</td></tr>
	  <tr><td align="center" colspan="2">{#SUBMIT Registrieren}</td></tr>
	  </table>
	{#END}
{#END}

{-------------------------- login ---------------------------------------}

{#SUB login}
<div class="card ">
	<div class="card-header">	
		<div class="row">
			{#IF $isloggedin}
				<div class="col-12">
				<h3>{%user}</h3>
				</div>
			{#END}
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12 col-sm-6 col-md-6">
				{#IF $isloggedin}
				<form method="post" id="frm2">
					<input type="hidden" name="ident" value="Anonymous" />
					<input type="hidden" name="password" value="x" />
					<button type="submit" class="btn btn-primary btn-md" value="logout">Logout&nbsp; <i class="fas fa-sign-out-alt"></i>
					</button>
				</form>
				{#ELSE}
					<form method="post" id="frm2">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
							<input id="ident" type="text" class="form-control" name="ident" title="Deine UserID, dein Nickname oder deine Email-Adresse" placeholder="UserID, Nickname oder Email-Adresse" 
							onclick="this.value='';" onFocus="if(this.value=='Nickname casesensitive') this.value='';" {#WHEN "!empty($loginident) and ($loginident != \"Anonymous\")" "value=\"%loginident\" "}/>
						</div>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
							<input id="password" type="password" class="form-control" name="password" placeholder="Password" title="Passwort" onclick="this.value='';" />
						</div>
						<div class="input-group-btn" style="padding-top: 10px;">
						<button type="submit" class="btn btn-success"><i class="fas fa-sign-in-alt"></i>&nbsp; Sign In </button>
						<a href="text.php?name=user_account_trouble" class="btn btn-secondary btn-md" role="button"><i class="fas fa-magic"></i>&nbsp;<i class="fas fa-user-md"></i>&nbsp;Account Wiederherstellung</a>
						</div>
					</form>
					<script>
					var loginform = document.getElementById('frm2');
					{#IF empty($loginident) or ($loginident == "Anonymous")}
					loginform.ident.value='Nickname';	
					loginform.password.value='Password';
					{#ELSE}
					loginform.password.focus();
					{#END}
					</script>          
				{#END}
			</div>
			<div class="col-sm-6 col-md-6"></div>
		</div>
	</div>
</div>
{#END}
