{#SUB default}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
    <table cellspacing="1" class="wide_table">
        <tr>
          <th class="tdedit">{#IF $hasnew}<a href="forum.php?action=setread"><img src="{#STATICFILE images/forum_markread32.png}" width="32" height="19" alt="read" border="0" /></a>{#END}</th>
          <th class="tdedit">Forum
            &nbsp;&nbsp;&nbsp;( <a href="search.php?mode=default&amp;usedmodules%5Bforum%5D=1">Suchen</a>
            {#IF $admin} &middot; <a href="forum.php?frame=viewgroups">Admin</a>{#END} 
            {#IF !empty($configurl)} &middot; <a href="{$configurl}">Konfiguration</a>{#END} )
          </th>
        <th class="tdedit">Themen</th>
        <th class="tdedit">Beitr&auml;ge</th>
        <th class="tdedit">letzter Beitrag</th>
        </tr>
      {#FOREACH $groups}
        <tr>
          <td colspan="5" align="center" class="tdedit"><b>{%caption}</b></td>
        </tr>
        {#FOREACH $forums}
        <tr>
          <td class="tdcont" width="34" style="padding-top:2px;padding-bottom:2px;padding-left:5px;padding-right:5px;" align="center" valign="middle"><img src="{#STATICFILE $img}" width="32" height="32" alt="" /></td>
          <td class="tdcont" style="padding-left:8px;"><a href="forum.php?frame=viewforum&amp;id={$id}"><b>{%name}</b></a><br />{%description}</td>
          <td class="tdcont" align="center">{$thread_count}</td>
        <td class="tdcont" align="center">{$post_count}</td>
        <td class="tdcont" align="center">{#IF !empty($last_poster)}<a href="user.php?frame=viewsubject&amp;id={$last_post_user}">{%last_poster}</a><br />{#DATE $last_post_time}{#ELSE}-{#END}</td>
        </tr>
        {#END}
      {#END}
    </table>
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB viewforum}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
    <p>{%description}</p>
    {#IF $watches_enabled}
    {#IF $watching}
      Du beobachtest dieses Forum!
    {#ELSE}
      <a href="watches.php?action=addwatch&amp;type=forum&amp;id={$id}">Dieses Forum beobachten (alle Watches auf Threads in diesem Forum werden gel&ouml;scht!)</a>
    {#END}
    {#END}
    <table cellpadding="0" cellspacing="0">
      <tr>
        <td>
          <h3 style="margin:6px 0px 6px 0px;"><a href="forum.php">Foren</a> &rArr; <a href="forum.php?frame=viewforum&amp;id={$id}">{%name}</a></h3>
        </td>
        <td align="right">
          <h3 style="margin:6px 0px 6px 0px;">{#IF $right_post}<a href="forum.php?frame=post&amp;forum_id={$id}">neues Thema</a>{#END}</h3>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          {#TABLE $threads maxrows=50 foundrows=$foundrows dborder=1}
            {#COL $newlink style="padding-top:1px;padding-bottom:1px;padding-left:5px;padding-right:5px;" align="center" valign="middle"}<a href="forum.php?action=getnew&amp;thread_id={$id}"><img src="{#STATICFILE $img}" width="24" height="24" alt="" border="" title="neuester/letzter Beitrag" /></a>
            {#COL Thema style="padding-left:8px;" width="450"}
              <b><a href="forum.php?frame=viewthread&amp;id={$id}">{%title}</a></b>
              {#IF isset($pages) && is_array($pages)}<i>{#FOREACH $pages $link $capt} <a href="forum.php?frame=viewthread&amp;id={$id}&amp;{$link}">{$capt}</a>{#END}</i>{#END}
            {#COL Beitr&auml;ge align="center"}{$posts}
            {#COL Er&ouml;ffner}<a href="user.php?frame=viewsubject&amp;id={$poster_id}">{%poster}</a>
            {#COL "letzter Beitrag"}<nobr>{#DATE $last_change} <a href="user.php?frame=viewsubject&amp;id={$last_id}">{%last}</a></nobr>
          {#END}
        </td>
      </tr>
    </table>
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB post}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
    {#FORM post}
    <table width="80%">
      <tr><td>Name: {#INPUT user string $user enabled=0}</td></tr>
      <tr><td>Titel: {#INPUT title longstring $title enabled=$edittitle allowempty=0}</td></tr>
      <tr><td>Text:</td></tr>
      <tr><td>{#INPUT text text $text}</td></tr>
      <tr><td><table width="100%"><tr>
        <td>Optionen f&uuml;r diesen Beitrag:</td>
        <td>{#INPUT allow_nl2br checkbox $allow_nl2br} NewLine zu &lt;br&gt; Aktivieren</td>
        <td>{#INPUT allow_html checkbox $allow_html enabled=$right_html} HTML Aktivieren</td>
    <!--    <td>{#INPUT allow_fliptags checkbox $allow_fliptags enabled=$right_fliptags} <a href="text.php?name=forum_doku_fliptags">FlipTags</a> Aktivieren</td>-->
      </tr></table></td></tr>
      <tr><td>&nbsp;</td></tr>
      <tr><td align="center">{#SUBMIT Speichern}</td></tr>
      <tr><td>&nbsp;</td></tr>
      <tr><td align="center">{#BACKLINK Zur&uuml;ck}</td></tr>
        <tr><td>
      <table class="table" width="400">
        <tr><td class="tdedit" align="center" colspan="2"><b>Textgestaltung</b></td>
        </tr>
        <tr><td class="tdedit" align="right"><b>Fett</b></td>
            <td class="tdcont" align="left">[b]...[/b]</td>
        </tr>
        <tr><td class="tdedit" align="right"><i>Kursiv</i></td>
            <td class="tdcont" align="left">[i]...[/i]</td>
        </tr>
        <tr><td class="tdedit" align="right"><u>Unterstrichen</u>
            <td class="tdcont" align="left">[u]...[/u]</td>
        </tr>
        <tr><td class="tdedit" align="right">Zitat</td>
            <td class="tdcont" align="left">[quote]...[/quote]</td>
        </tr>
      </table>
    </td></tr>
    </table>
    {#INPUT id hidden $id}
    {#INPUT forum_id hidden $forum_id}
    {#INPUT thread_id hidden $thread_id}
    {#END}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB viewthread}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
    {#IF $watches_enabled}
    {#IF $watching}
      Du beobachtest diesen Thread!<br />
      Falls du keinen Watch auf diesen Thread hast, dann besitzt du m&ouml;glicherweise einen auf das ganze Forum!
    {#ELSE}
      <a href="watches.php?action=addwatch&amp;type=thread&amp;id={$id}">Diesen Thread beobachten</a>
    {#END}
    {#END}
    <div class="wide_topdiv" style="text-align:left; padding-bottom:0px;">
      <h3 style="margin:6px 0px 6px 0px;">
        <a href="forum.php">Foren</a> &rArr;
        <a href="forum.php?frame=viewforum&amp;id={$forum_id}">{%forum_name}</a> &rArr;
        <a href="forum.php?frame=viewthread&amp;id={$id}">{%title}</a>
      </h3>
    </div>
    {#TABLE $posts class="wide_table" maxrows=$posts_per_page foundrows=$foundrows dborder=1}
      {#IF $right_post}<a id="post_last" href="forum.php?frame=post&amp;thread_id={$id}" accesskey="r"><b>Antworten (Alt+R)</b></a>{#ELSE}<b><s>Antworten</s></b>{#END}
      {#COL Autor width="130" valign="top"}
        <b><a {#WHEN "$post_time > $last_visit" " class=\"important\""} id="post_{$id}" href="user.php?frame=viewsubject&amp;id={$poster_id}" >{%poster}</a></b><br />
        {#IF !empty($user_img)}
          <br />
          {#DBIMAGE $user_img process=createthumbnail($maxwidth,$maxheight)}
          <br />
        {#END}
        {#IF !empty($addposterinfo)}{$addposterinfo}<br />{#END}
        {#DATE $post_time}
        {#IF $editable or $right_mod}
          <br />
          <a href="forum.php?frame=post&amp;id={$id}">edit</a>
          {#IF $right_mod}&nbsp;&nbsp;<a href="forum.php?action=deletepost&amp;id={$id}&amp;confirmation={#URL Soll der Beitrag wirklich gel&ouml;scht werden?}">del</a>{#END}
        {#END}
      {#COL Beitrag style="padding-top:5px;padding-bottom:5px;"}
        {$text}
        {#IF $changer_id}<br /><i>Ge&auml;ndert von {%changer} am {#DATE $change_time}</i>{#END}
        {#IF !empty($signature)}<br />--<br /><div class="forum_signature">{$signature}</div>{#END}
    {#END}
    {#IF $right_mod}
    <div class="wide_div">
    <br />
    <br />
    <table class="table" cellspacing="1">
      <tr>
        <th class="tdedit" colspan="2">Moderation: Thema...</th>
      </tr>
      <tr>
        <td class="tdcont" align="center">
        {#ACTION l&ouml;schen deletethread "forum.php?frame=viewforum&amp;id=$forum_id" params="array(id=$id)" confirmation="Soll das Thema wirklich gel&ouml;scht werden?" shortcut=d}
        </td>
        <td class="tdcont" align="center">
          {#ACTION $stickycapt sticky params="array(id=$id val=$stickyval)" shortcut=s}
        </td>
      </tr>
      <tr>
      {#FORM postright keepvals=0 action="forum.php?frame=viewthread&amp;id=$id"}
        <td colspan="2" class="tdcont">
        {#INPUT $right dropdown $post_setright param="array(-1=Locked 0=Unlocked)"} {#SUBMIT setzen shortcut=l}
        {#IF $right_rights}
          {#WHEN "!($post_right==0 or $post_right==-1)" "(&auml;ndert Beitragsrecht!)"}<br />
          Beitragsrecht {#INPUT $right rights $post_setright param="array(infinite=1)"} {#SUBMIT setzen shortcut=p}
        {#END}
        </td>
      {#INPUT id hidden $id}
      {#END}
      </tr>
      <tr>
        {#FORM movethread keepvals=0 action="forum.php?frame=viewforum&amp;id=$forum_id"}
      <td colspan="2" class="tdcont">
        ins Forum {#INPUT forum dropdown $forum_id param=$foren} {#SUBMIT verschieben shortcut=v}
        {#INPUT id hidden $id}
        </td>
      {#END}
      </tr>
      <tr>
        {#FORM renamethread keepvals=0 action="forum.php?frame=viewthread&amp;id=$id"}
      <td colspan="2" class="tdcont">
        <table width="100%" cellspacing="0"><tr>
            <td width="11">in</td>
            <td>{#INPUT title longstring $title}</td>
            <td width="110" align="right">{#SUBMIT umbenennen shortcut=u}</td>
          </tr></table>
        {#INPUT id hidden $id}
        </td>
      {#END}
      </tr>
    </table>
    <br />
    </div>
    {#END}
			</div>
		</div>
	</div>
</div>
{#END}

{!********************  Admin ********************************}

{#SUB viewgroups}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
        <p>
          Dies sind Gruppen, welche Foren enthalten. 
          Sie dienen nur zur strukturierten Ordnung der Foren,
          haben dar&uuml;ber hinaus aber keinen Einfluss auf sie.
        </p>
        <h3><a href="forum.php">Forum</a></h3>
        {#TABLE $groups}
          <a href="forum.php?frame=editgroup">add</a>
          {#COL Titel}<a href="forum.php?frame=viewgroup&amp;id={$id}">{%caption}</a>
          {#FRAME edit editgroup}
          {#ACTION /\ switchgroups params="array(sort_index=$sort_index sort_index2=$sort_index2)" condition=$sort_index2 uri="forum.php?frame=viewgroups"}
          {#ACTION del deletegroup condition=!$notdeletable confirmation="Soll die Gruppe wirklich gel&ouml;scht werden?"}
        {#END}
        <br />
        {#FORM}
          {#INPUT action hidden countposts}
          {#SUBMIT "Letzten Beitrag der Foren neu berechnen"}
        {#END}
        {#END}

        {#SUB editgroup}
        {#FORM group}
        <p>Titel: {#INPUT caption longstring $caption}</p>
        <p>{#SUBMIT Speichern}</p>
        <p>{#BACKLINK Zur&uuml;ck}</p>
        {#INPUT id hidden $id}
        {#INPUT sort_index hidden $sort_index}
        {#END}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB viewgroup}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
        <h3>
          <a href="forum.php?frame=viewgroups">Gruppen&uuml;bersicht</a> &middot;
          <a href="forum.php">Forum</a>
        </h3>
        {#TABLE $forums}
          <a href="user.php?frame=editproperties&amp;id=create_new_subject&amp;type=forum&amp;group_id={$group.id}&amp;sort_index={$sortindex}">add</a>
          {#COL Name}<a href="user.php?frame=viewsubject&amp;id={$id}"><b>{%name}</b></a>
          {#COL Beschreibung}{%description}
          {#FRAME edit link="user.php?frame=editproperties&amp;id=$id"}
          {#ACTION /\ switchforums params="array(last=$last)" condition=!empty($last) uri="forum.php?frame=viewgroup&amp;id=$group.id"}
          {#ACTION empty emptyforum confirmation="Soll Das Forum wirklich geleert werden?<br />Dabei werden alle enthalten Themen und Beitr&auml;ge gel&ouml;scht."}
          {#ACTION del deleteforum confirmation="Soll das Forum wirklich gel&ouml;scht werden?"}
        {#END}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB stats}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
        {#TABLE $users}
          {#COL Nickname $name}
          {#COL Posts $summe}
          {#COL Dauer $zeit}{#LOAD "round($zeit/3600/24,0)" $days}{$days} Tage
          {#COL "in Threads" $threads}
          {#COL Posts/Thread $avg}{$avg}
        {#END}
			</div>
		</div>
	</div>
</div>
{#END}

{#SUB smalllastposts}
  letzte Posts in:<br/>
  {#FOREACH $posts}
  <a href="forum.php?action=getnew&amp;thread_id={$tid}">{%title}</a><br/>
  {#END}
{#END}
