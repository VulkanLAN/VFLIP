{#SUB default}
<div class="card ">
	<div class="card-header">	
		<div class="row">
			<div class="col-12">
        Dieser Script erm&ouml;glicht es, im handumdrehen die aktuelle Datenbank von einem Remoteserver zu importieren.<br />
        Deine Account-Daten werden ben&ouml;tigt, um dich gegen&uuml;ber dem Remoteserver zu authentifizieren.<br />
        <span class="important">Achtung:</span> Alle lokalen Daten werden mit denen vom Remoteserver &uuml;berschrieben!
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12">
      {#FORM login}
        <table>
          <tr>
            <td align="right">Script:</td>
            <td>{#INPUT script dropdownedit $script param=$scripts}</td>
          </tr>
          <tr>
            <td align="right">Ident:</td>
            <td>{#INPUT user string $user}</td>
          </tr>
          <tr>
            <td align="right">Passwort:</td>
            <td>{#INPUT password passwordquery}</td>
          </tr>
          <tr><td colspan="2">&nbsp;</td></tr>
          <tr><td colspan="2" align="center">{#SUBMIT "Tabellen ausw&auml;hlen" shortcut=a name=selecttables}</td></tr>
          <tr><td colspan="2">&nbsp;</td></tr>
          <tr><td colspan="2" align="center">{#SUBMIT "alle Ge&auml;nderten importieren" shortcut=g name=ifchanged}</td></tr>
        </table>
      {#END}
      </div>
		</div>
	</div>
</div>
{#END}

{#SUB selecttables}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
          {#IF $showall}
            Alle Tabellen Werden angezeigt.  &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="dbxfer.php?frame=selecttables">leere ausblenden</a>
          {#ELSE}
            <span class="important">Achtung:</span> Leere Tabellen sind ausgeblendet! &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="dbxfer.php?frame=selecttables&amp;showall=1">alle anzeigen</a>
          {#END}
        </div>
        <div class="col-12">
        {#TABLE $tables}
          {#COL Name $id}
          {#COL Spalten $rows align=right}
          {#COL Gr&ouml;&szlig;e $size align=right}{#BYTE $size format=kb prec=1} kB
          {#COL "letzte &Auml;nderung" $date align=center}{#DATE $date}
          {#OPTION "Importieren" importselected}
          {#OPTION "Importieren, wenn ge&auml;ndert" importifchanged}
        {#END}
        </div>
        <div class="col-12">
        <p align="center">
          <a href="dbxfer.php">Zur&uuml;ck</p>
        </p>
        </div>
       </div>
	  </div>
  </div>
</div>
{#END}

{#SUB doimport}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
        <p>
        {#IF $compare}
          Es werden nur die ge&auml;nderten Daten importiert.
        {#ELSE}
          Es werden alle Daten der aufgelisteten Tabellen importiert.
        {#END}
        </p>
        {#TABLE $tables}
          {#COL Name}{$name}
          {#COL Status width="400" style="padding:0px"}{#VAR sub=blocktable}
          {#COL Gr&ouml;&szlig;e align=right}{#BYTE $size format=kb prec=1} kB
        {#END}
        <p>
          <span class="important">Achtung:</span><br />
          Erst wenn alle Statusfelder ein farbiges Bild geladen haben wurden alle Tabellen importiert und du kannst auf "Weiter" klicken!
        </p>
        <p>
          {#FORM action="dbxfer.php?frame=report"}{#SUBMIT Weiter}{#END}
        </p>
       </div>
	  </div>
  </div>
</div>
{#END}

{#SUB blocktable}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
        <table cellpadding="0" cellspacing="1" style="width:100%; height:20px"><tr>
        {#FOREACH $blocks}
          {#IF $equal}<td style="background-color:blue;">&nbsp;</td>
          {#ELSE}<td style="background-image:url(dbxfer.php?frame=importblock&tablename={$name}&firstid={$firstid}&lastid={$lastid}&ac={$anticache}&disable-dbupdate=1);">&nbsp;</td>
          {#END}
        {#END}
        </td></table>
       </div>
	  </div>
  </div>
</div>
{#END}

{#SUB report}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
        <p>
        {#IF $error}
          Es konnten nicht alle Tabellen Fehlerfrei importiert werden. Bitte entnimm die Details den Fehlermeldungen.
        {#ELSE}
          Alle Tabellen wurden fehlerfrei importiert.
        {#END}
        </p>
        <p>
          <a href="dbxfer.php">Zur&uuml;ck</a>
        </p>
       </div>
	  </div>
  </div>
</div>
{#END}
