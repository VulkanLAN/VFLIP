{! $Id$ }

{! this subs should not be used standalone !}

{#SUB atree}
  <td valign="middle" height="100%">
    <table height="100%" width="100%" border="0" cellspacing="0" cellpadding="4">
      <tr height="20">
        <th class="treehead" style="border-bottom:1px solid black;{#WHEN $isfirstcol "border-left:1px solid black;"}{#WHEN $islastcol "border-right:1px solid black;"}"><a name="{$roundtitle}"></a>{$roundtitle}</th>
      </tr>
      <tr height="10"><TD></TD></tr>
      {#FOREACH $row index=$i}
      <tr>
        <td align="center" valign="middle" style="white-space:nowrap;padding:0px;padding-bottom:5px;margin:0px;border-spacing:0px;">
          <table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0"><tr>
          <td style="white-space:nowrap;" height="100%">
          <div class="treecell">
          <div class="{$color1}" style="margin:0px;margin-bottom:-1px;">{#IF !empty($team1) && empty($points1) && empty($points2)}{#LOAD $ready1 $ready}{#LOAD $readylink1 $readylink}{#VAR sub=_ready}{#END}{$team1html}</div>
          <div class="score text-sm-left" align="center" style="margin:0px;margin-top:-1px;margin-bottom:-1px;"><small>{$score}{#IF !empty($comment)} {$comment}{#END}</small></div>
          <div class="{$color2}" align="right" style="margin:0px;margin-top:-2px;">{$team2html}{#IF !empty($team2) && empty($points1) && empty($points2)}{#LOAD $ready2 $ready}{#LOAD $readylink2 $readylink}{#VAR sub=_ready}{#END}</div>
          </td><td valign="{#WHEN "$i % 2==0" top bottom}" style="width:12px;height:100%;vertical-align:{#WHEN "$i % 2==0" top bottom}">
          {! The smart way, but only Mozilla can draw 50% height and valign correctly :-( }
          {#IF (stristr($browser, "gecko") && !stristr($browser, "konqueror"))}
          <div style="height:50%;overflow:hidden;{#IF ($no*100) % 100/100 == 0}{#VAR sub=_atree_winnerlinealgorithm file=$treetypefile}{#ELSE}{#VAR sub=_atree_loserlinealgorithm file=$treetypefile}{#END}">
          </div>
          {#ELSE}
          {! so we have to calculate a fix height for all other browsers }
          <div style="width:90%;height:47px{!= 43height + 2*1padding + 2*1border (class=treecell+winner/loser/open/notset)};overflow:hidden;{#IF ($no*100) % 100/100 == 0}{#VAR sub=_atree_winnerlinealgorithm file=$treetypefile}{#ELSE}{#VAR sub=_atree_loserlinealgorithm file=$treetypefile}{#END}">
          </div>
          {#END}</td>
          </tr></table>
        </td>
      </tr>
      {#END}
    </table>
  </td>
{#END}

{#SUB _ready}
{#IF empty($ready)}
	{#IF $readylink}<a href="{$readylink}" title="ready"><span style="color:red;">&diams;</span></a>
	{#ELSE}<span style="color:red;">&diams;</span>
	{#END}
{#ELSE}
	<span style="color:green;">&diams;</span>
{#END}
{#END}
{#SUB _atree_winnerlinealgorithm}
{#IF $no>0}border-{#WHEN "$i % 2==1" top bottom}:{#VAR sub=_atree_lineborderstyle file=$treetypestyles};{#END}{#IF $no>1}border-right:{#VAR sub=_atree_lineborderstyle file=$treetypestyles};{#END}
{#END}

{#SUB _atree_loserlinealgorithm}
border-{#WHEN "$i % 2==1" top bottom}:{#VAR sub=_atree_lineborderstyle file=$treetypestyles};{#IF ($no*100) % 100/100==0.75 && $no>1}border-right:{#VAR sub=_atree_lineborderstyle file=$treetypestyles};{#END}
{#END}
