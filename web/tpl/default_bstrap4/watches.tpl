{#SUB default}
<div class="card ">
  <div class="card-body">
		<div class="row">
			<div class="col-12">
            {#IF $w_enabled}
            Hier lassen sich deine Watches einstellen und konfigurieren!<br />
            Watches sind wie Bewegungsmelder und benachrichtigen dich, sobald sich im Forum etwas &auml;ndert.<br />
            Soll ein neuer Watch erstellt werden, so stehen im Forum die notwendigen Schaltfl&auml;chen bereit!<br />
            {#ELSE}
            Der Administrator hat die Watches deaktiviert - deshalb kannst du sie im Moment nicht verwenden!<br />
            {#END}
            <br />
            {#TABLE $watches}
            {#COL Typ $type}
            {#COL Name $name}
            {#OPTION "L&ouml;schen" deletewatches}{$type}
            {#END}
			</div>
		</div>
	</div>
</div>
{#END}