{#SUB default}
{#END}

{#SUB pay}
  {#IF !empty($embedded)}
    <html>
    <head>
      <link rel="stylesheet" type="text/css" href="{#STATICFILE inc.page.css}" />
      <link rel="stylesheet" type="text/css" href="{#STATICFILE inc.content.css}" />
      <link rel="stylesheet" type="text/css" href="{#STATICFILE inc.form.css}" />
      <style type="text/css">
      <!--
        body { background-color: #f8f8f8; }
      //-->
      </style>
      <script type="text/javascript">
        function FormWorkaround() {
          for(f = 0; f < document.forms.length;f++) {
            document.forms[f].target = "_top";
          }
        }
      </script>
    </head>
    <body onload="javascript:FormWorkaround()">
      <div align="center">
  {#END}
  
  {#TPL $text}
  {#TABLE $buttons}
    {#IF !empty($edit_right)}<a target="_top" href="paypal.php?frame=edit{%embed_href}">add</a>{#END}
    {#COL Beschreibung %item_name}{#IF !empty($edit_right)}<a target="_top" href="paypal.php?frame=edit&id={%id}{%embed_href}">{%item_name}</a>{#ELSE}{%item_name}{#END}
    {#COL Preis "%amount_f $currency_code"}
    {#COL ""}
      {#FORM action="%ppurl"}
        {#INPUT amount hidden "%amount_f"}
        {#INPUT bn hidden "PP-BuyNowBF"}
        {#INPUT business hidden "%business"}
        {#INPUT cancel_return hidden "%cancel_return"}
        {#INPUT cmd hidden "_xclick"}
        {#INPUT currency_code hidden "%currency_code"}
        {#INPUT custom hidden "%userid"}
        {#INPUT item_name hidden "%item_name"}
        {#INPUT item_number hidden "%item_number"}
        {#INPUT no_shipping hidden 1}
        {#INPUT notify_url hidden "%notify_url"}
        {#INPUT on0 hidden FLIP-ID}
        {#INPUT on1 hidden Nick}
        {#INPUT os0 hidden "%userid"}
        {#INPUT os1 hidden "%usernick"}
        {#INPUT quantity hidden 1}
        {#INPUT return hidden "%return"}
        {#INPUT rm hidden "POST"}
        {#HTMLSUBMIT shortcut="" }
        	<img alt="Bezahlen" src="{#STATICFILE images/paypal/paypal_button.gif}" />
        {#END}
      {#END}
  {#END}
  {#IF !empty($embedded)}
      </div>
    </body>
    </html>
  {#END}
{#END}

{#SUB edit}
    <div align="center">
      Beim Erstellen eines Buttons m&uuml;ssen die URL-Felder nicht notwendigerweise abge&auml;ndert werden,
      da die Adressen bereits f&uuml;r die PayPal-Abwicklung &uuml;ber das FLIP voreingestellt sind.
      Lediglich die PayPal-URL muss angepasst werden, wenn die Transaktionsdaten z.B. zu Testzwecken
      nicht an den PayPal-Server geleitet werden sollen.
    </div>
    <br />
    {#FORM button}
      <table border="0" width="70%">
        <tr>
          <td>Buttonname:</td>
          <td>{#INPUT item_name longstring $item_name}</td>
        </tr>
        <tr>
          <td>Preis:</td>
          <td>{#INPUT amount decimal $amount}</td>
        </tr>
        <tr>
          <td>W&auml;hrung:</td>
          <td>{#INPUT currency_code dropdown $currency_code param=array("EUR=Euro GBP=\"Britische Pfund\" USD=US-Dollar CAD=\"Kanadische Dollar\" AUD=\"Australische Dollar\" JPY=\"Yen\"")}</td>
        </tr>
        <tr>
          <td>Verk&auml;ufer:</td>
          <td>{#INPUT business longstring $business}</td>
        </tr>
        <tr>
          <td>R&uuml;ckkehr-URL:</td>
          <td>{#INPUT return longstring $return}</td>
        </tr>
        <tr>
          <td>Abbruch-URL:</td>
          <td>{#INPUT cancel_return longstring $cancel_return}</td>
        </tr>
        <tr>
          <td>PayPal-URL:</td>
          <td>{#INPUT ppurl longstring $ppurl}</td>
        </tr>
        <tr>
          <td>IPN-URL:</td>
          <td>{#INPUT notify_url longstring $notify_url}</td>
        </tr>
      </table>
      <br />
      {#INPUT id hidden $id}
      {#SUBMIT Speichern}
    {#END}
  <br />
  {#ACTION L&ouml;schen deletebutton shortcut=d params="array(buttonid=%id)"}
  <br />
  {#IF $embedded}
    <a target="_top" href="javascript:history.back()">Zur&uuml;ck</a>
  {#ELSE}
    {#FORM}
      {#BACKLINK Zur&uuml;ck}
    {#END}
  {#END}
{#END}
