{! $Id$ }

{! this subs should not be used standalone !}
{! Turnierbaumaufbau !}

{#SUB atree_arrow}
<!-- muss noch angepasst werden,als Idee f�r Pfeile zwieschen den Runden im Turnierbaum -->
<script type="text/javascript">
		
		var c5=document.getElementById("c5");
		var ctx5=c5.getContext('2d');
		ctx5.strokeStyle='rgb(50,50,0)';
		ctx5.fillStyle='rgb(50,50,0)';
		ctx5.lineWidth=2;
		// draw the line for the shaft
		ctx5.moveTo(10,30);
		ctx5.lineTo(190,30);
		// draw the top of the arrow head
		ctx5.lineTo(185,28);
		// draw the curve of the back
		ctx5.arcTo(189,30, 185,32,8);
		// draw the bottom of the arrow head
		ctx5.lineTo(190,30);
		// and make it draw
		ctx5.stroke();
		ctx5.fill();
	  
	</script>
	<?php
		if($counter==1)
		{ 
		print("<div style= \"width=200px;\"> <?php echo \"$counter;\" ?>
			<canvas id=c5 width=200px height=60>
			If you see this that means that your browser doesnt support
			the cool new HTML5 canvas elements.  Boy are <em>you</em>
			missing out!  You might want to upgrade.
			</canvas>
			</div>");
		}
		else
		{
		// nothing to do
		}
		?>
{#END}

{#SUB atree}
	<?php
		$teamCount=0;
		$teamContainerWidth = 260;
		$teamContainerHeight = 120;			
	?>
  <div>
		<h3><a name="{$roundtitle}"></a>{$roundtitle}</h3>
		{#FOREACH $row index=$i}<!-- Spiele je Runde -->
			<div class="teamcontainer" style="width:<?php echo $teamContainerWidth; ?>px;height:<?php echo $teamContainerHeight; ?>px;">						
					<div class="{$color1}" align="left" style="margin:-1px;padding:0px"> <!-- Team1/Player1 -->
					{#IF !empty($team1) && empty($points1) && empty($points2)}
						{#LOAD $ready1 $ready}
						{#LOAD $readylink1 $readylink}
						{#VAR sub=_ready}
					{#END}
					<span class="d-inline-block text-truncate" style="max-width: 220px;">{$team1html}</span>
					</div>
					<div align="center" style="margin:-1px;padding:0px">{$score}</div><!-- vs -->
					<div class="{$color2}" align="right" style="margin:-1px;padding:0px"><!-- Team2/Player2 -->
					<span class="d-inline-block text-truncate" style="max-width: 220px;">{$team2html}</span>
					{#IF !empty($team2) && empty($points1) && empty($points2)}
						{#LOAD $ready2 $ready}
						{#LOAD $readylink2 $readylink}
						{#VAR sub=_ready}
					{#END}
					</div>
					{#IF !empty($comment)}
						<div>Comment:
							<small class="comment">{$comment}</small>
						</div>
					{#END}
					<div class="row" style="display:none;">
						<div class="col-2 col-sm-2 col-md-2 col-lg-2" style="vertical-align:{#WHEN "$i % 2==0" top bottom} >
						<!-- original valign="#WHEN "$i % 2==0" top bottom}" style="width:12px;height:100%;vertical-align:#WHEN "$i % 2==0" top bottom} -->
							<div style="height:50%;overflow:hidden;
								{#IF ($no*100) % 100/100 == 0}
									{#VAR sub=_atree_winnerlinealgorithm file=$treetypefile}
									{#ELSE}
									{#VAR sub=_atree_loserlinealgorithm file=$treetypefile}
								{#END}">
							</div>
						</div>
					</div>
			</div>			
		{#END}
	</div>	
{#END}

{#SUB _ready}
	{#IF empty($ready)}
		{#IF $readylink}<a href="{$readylink}" title="ready"><span style="color:red;"><i class="far fa-clock fa-1x"></i></span></a>
		{#ELSE}<span style="color:red;"><i class="far fa-clock fa-1x"></i></span>{#END}
		{#ELSE}<span style="color:green;"><i class="far fa-times-circle fa-1x"></i></span>{#END}
{#END}

{#SUB _atree_winnerlinealgorithm}
	{#IF $no>0}border-{#WHEN "$i % 2==1" top bottom}:
		{#VAR sub=_atree_lineborderstyle file=$treetypestyles};
	{#END}
	{#IF $no>1}border-right:
		{#VAR sub=_atree_lineborderstyle file=$treetypestyles};
	{#END}
{#END}

{#SUB _atree_loserlinealgorithm}
	border-{#WHEN "$i % 2==1" top bottom}:
		{#VAR sub=_atree_lineborderstyle file=$treetypestyles};
	{#IF ($no*100) % 100/100==0.75 && $no>1}
	border-right:
		{#VAR sub=_atree_lineborderstyle file=$treetypestyles};
	{#END}
{#END}
