{#SUB default}
<div class="card ">
<div class="card-header">

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
  Level {$level}: /&nbsp;
	{#FOREACH $path}
    <li class="breadcrumb-item"><a href="menu.php?id={$id}">{%caption}</a></li>{#END}
  </ol>
</nav>

{#ACTION "Menucache leeren*" flushcache formattrs="array(style=display:inline;)" buttonclass="btn btn-danger btn-sm"}  

</div>
<div class="card-body table">
	<table class="table" >
	  <tr>
		<th class="tdedit">aktiv</th>
		<th class="tdedit">Titel</th>
		<th class="tdedit">Inhalt</th>
		<th class="tdedit">Beschreibung</th>
		<th class="tdedit">Recht</th>
		<th class="tdedit">&nbsp;</th>
		<th class="tdedit">&nbsp;</th>
		<th class="tdedit">&nbsp;</th>
	  </tr>
	  {#FOREACH $blocks}
		<tr>
		  <td class="tddark" align="center"><span class="{#WHEN $enabled "stat_green" "stat_red"}">{#WHEN $enabled Ja Nein}</span></td>
		  <td class="tdedit" colspan="2">{#DBIMAGE flip_menu_blocks image_title $id $mtime} <b{#WHEN empty($parent_item_id) " class=\"important\""}>{%caption}</b></td>
		  <td class="tddark">{%description}</td>
		  <td class="tddark">{#RIGHT $view_right view}</td>
		  <td class="tdaction">{#ACTION /\ moveblock params="array(id1=$id id2=$lastid)" condition=!empty($lastid)}</td>
		  <td class="tdcont"><a href="menu.php?frame=editblock&amp;id={$id}" type="button" class="btn btn btn-outline-secondary btn-sm">edit</a></td>
		  <td class="tdaction">{#ACTION del deleteblock params="array(id=$id)" condition=empty($items) confirmation="Soll der Men&uuml;block wirklich gel&ouml;scht werden?"}</td>
		</tr>
		{#FOREACH $items}
		  <tr>
			<td class="tdcont" align="center"><span class="{#WHEN $enabled "stat_green" "stat_red"}">{#WHEN $enabled Ja Nein}</span></td>
			<td class="tdcont"><a href="menu.php?id={$id}">{#DBIMAGE flip_menu_links image $id $mtime} <b>{%caption}</b></a></td>
			<td class="tdcont">
			  {#IF !empty($frameurl)}
				frame:<a href="{%frameurl}">{%frameurl}</a>
			  {#ELSEIF !empty($text)}
				text:{%text}
			  {#ELSE}
				{#WHEN $use_new_wnd neu:}<a href="{%link}">{%link}</a>
			  {#END}
			</td>
			<td class="tdcont">{%description}</td>
			<td class="tdcont">{#RIGHT $view_right view}</td>
			<td class="tdaction">{#ACTION /\ moveitem params="array(id1=$id id2=$lastid)" condition=!empty($lastid)}</td>
			<td class="tdcont"><a href="menu.php?frame=edititem&amp;id={$id}" type="button" class="btn btn btn-outline-secondary btn-sm">edit</a></td>
			<td class="tdaction">{#ACTION del deleteitem params="array(id=$id)" condition=empty($used) confirmation="Soll der Men&uuml;eintrag wirklich gel&ouml;scht werden?"}</td>
		  </tr>
		{#END}
		<tr>
		  <td class="tdcont" colspan="7">&nbsp;</td>
		  <td class="tdcont"><a href="menu.php?frame=edititem&amp;block_id={$id}" type="button" class="btn btn-secondary btn-sm">add</a></td>
		</tr>
	  {#END}
	  <tr><td colspan="8" class="tdcont" align="right">
		Block erstellen: 
		{#IF !empty($parent)}<a href="menu.php?frame=editblock&amp;parent_item_id={$parent}" type="button" class="btn btn-secondary btn-sm">dynamisch</a> |{#END}
		<a href="menu.php?frame=editblock&amp;level_index={$level}" type="button" class="btn btn-secondary btn-sm">statisch</a>    
	  </td></tr>
	</table>
	<p>
	 * Bei jedem User wird in der Session das aktuelle Men&uuml; gecached. Dieser Cache wird nur wenn er zu voll 
	 wird sowie beim Ein- und Ausloggen geleert. Wenn die User ein bearbeitetes Men&uuml; schon vorher
	 zu Gesicht bekommen sollen, l&auml;&szlig;t sich hier der Cache in alles Sessions leeren.
	</p>
</div>
{#END}

{#SUB editblock}
<div class="card ">
	{#FORM block}
	<div class="card-header">
		{#BACKLINK Zur&uuml;ck} {#SUBMIT Speichern}
	</div>
	<div class="card-body table">
	  <table class="card-body table">
		{#IF empty($parent_item_id)}
		  <tr><td align="right">Typ:</td><td>Statisch</td></tr>
		  {#INPUT level_index hidden $level_index}
		{#ELSE}
		  <tr><td align="right">Typ:</td><td>Dynamisch {#FOREACH $path}{%caption}{#MID} / {#END}</td></tr>
		  {#INPUT parent_item_id hidden $parent_item_id}
		{#END}
		{#IF !isset($level_index)}
			<tr><td align="right">Block:</td><td>{#INPUT parent_item_id dropdown $parent_item_id $parents}</td></tr>
		{#END}
		<tr><td align="right">Level:</td><td>{$level}</td></tr>
		<tr><td align="right">Titel:</td><td>{#INPUT caption string $caption}</td></tr>
		<tr><td align="right">aktiviert:</td><td>{#INPUT enabled checkbox $enabled}</td></tr>
		<tr><td align="right">Titelbild:</td><td>{#INPUT image_title image}</td></tr>
		<tr><td align="right">Hintergrundbild:</td><td>{#INPUT image_bg image}</td></tr>
		<tr><td align="right">Leserecht:</td><td>{#INPUT view_right rights $view_right}</td></tr>
		<tr><td align="right">Callback:</td><td>{#INPUT callback_items string $callback_items}</td></tr>
		<tr><td align="right">Beschreibung:</td><td>{#INPUT description text $description}</td></tr>
		<tr><td colspan="2" align="center">{#SUBMIT Speichern}</td></tr>
	  </table>
	  {#INPUT id hidden $id}  
	  {#INPUT order hidden $order}
	</div>
	{#END}
</div>
{#END}

{#SUB edititem}
	<div class="card ">
	{#FORM item}
		<div class="card-header">
		{#BACKLINK Zur&uuml;ck} {#SUBMIT Speichern}
		</div>
		<div class="card-body table">
		  <table class="table table-sm ">
			<tr><td align="right">Titel:</td><td>{#INPUT caption string $caption allowempty=0}</td></tr>
			<tr><td align="right">in neuem Fenster &ouml;ffnen:</td><td>{#INPUT use_new_wnd checkbox $use_new_wnd}</td></tr>
			<tr><td align="right">aktiviert:</td><td>{#INPUT enabled checkbox $enabled}</td></tr>
			<tr><td align="right">Link:</td><td>{#INPUT link longstring $link}</td></tr>
			<tr><td align="right">Font_Image:</td><td>{#INPUT font_image longstring $font_image}</td></tr>
			<tr><td align="right">Frame:</td><td>{#INPUT frameurl string $frameurl}</td></tr>
			<tr><td align="right">Text:</td><td>{#INPUT text text $text}</td></tr>
			<tr><td align="right">Bild:</td><td>{#INPUT image image}</td></tr>
			<tr><td align="right">Leserecht:</td><td>{#INPUT view_right rights $view_right}</td></tr>
			<tr><td align="right">Beschreibung:</td><td>{#INPUT description text $description}</td></tr>
			<tr><td colspan="2" align="center">{#SUBMIT Speichern}</td></tr>
			</table>
			
			<p align="justify">
			  Es kann nur ein Frame, ein Text oder ein Link pro Men&uuml;eintrag angegeben werden. <br />
			  Ein Text kann beliebigen HTML-Code enthalten, der im Men&uuml; angezeigt wird.
			  Mit der einfachsten Variante "&lt;br /&gt;" oder "&lt;hr /&gt;" l&auml;&szlig;t sich beispielsweise
			  ein Spacer erstellen.<br />
			  Der Frame sieht einer URL sehr &auml;hnlich und gibt eine "Seite" (eben einen Frame) aus dem FLIP an, die 
			  dann im Men&uuml; angezeigt wird. Beispiel: user.php?frame=login<br />
			  <b>Generell gilt:</b> wie sich diese Optionen im einzelnen auswirklen und ob sie &uuml;berhaupt verwendbar sind, 
			  h&auml;ngt vom Lanpartyspezifischen Template ab.
			</p>
		  {#INPUT id hidden $id}  
		  {#INPUT block_id hidden $block_id}
		  {#INPUT order hidden $order}
		</div>
		{#END}
</div>
{#END}
