{! Template f&uuml;r Atom Feed Document (v1.0) ohne Extensions nach http://www.atompub.org/rfc4287.html - Stand: 2007-02-26 }
{! $Id$ }

{#SUB feed}
	<<?php echo"?"; ?>xml version="1.0" encoding="{#WHEN !empty($encoding) $encoding "iso-8859-1"}"<?php echo"?"; ?>>
	<feed xmlns="http://www.w3.org/2005/Atom">
	{#VAR $title sub=text tagname=title}
	<updated>{%updated}</updated>{! content MUST conform to the "date-time" production in [RFC3339]}
	<id>{%id}</id>
	{#VAR sub=persons tagname=author persons=$authors}
	{#VAR sub=categories}
	{#VAR sub=persons tagname=contributor persons=$contributors}
	<generator uri="http://www.flipdev.org/" version="{#LOAD "constant(\"FLIP_VERSION_LONG\")" $ver}{%ver}">FLIP {%ver}</generator>
	<icon>{#STATICFILE favicon.ico}</icon>
	{#IF $logo != ""}
	<logo>{%logo}</logo>
	{#END}
	<link rel="self" type="application/atom+xml" href="{#LOAD "ServerURL().GetRequestUri()" $selfURL}{%selfURL}"/>
	{#VAR sub=links}
	{#IF $rights != ""}
		{#VAR $rights sub=text tagname=rights}
	{#END}
	{#IF $subtitle != ""}
		{#VAR $subtitle sub=text tagname=subtitle}
	{#END}
	{#FOREACH $entries $entry}
		{#VAR $entry sub=entry}
	{#END}
	</feed>
{#END}

{#SUB text}
	{#IF $text != ""}
	{#IF $type != "xhtml"}
	{#LOAD "($type == \"html\") ? \"html\" : \"text\"" $type}
	<{%tagname} type="{$type}"{#VAR sub=lang}>{%text}</{%tagname}>
	{#ELSE}
		<{%tagname} type="xhtml"{#VAR sub=lang}>
			<div xmlns="http://www.w3.org/1999/xhtml">{$text}</div>
		</{%tagname}>
	{#END}
	{#END}{#END}

	{#SUB persons}{#FOREACH $persons}
		<{%tagname}>
			<name{#VAR sub=lang}>{%name}</name>
	{#IF $uri != ""}
			<uri>{%uri}</uri>{! an IRI reference [RFC3987]}
	{#END}
	{#IF $email != ""}
			<email>{%email}</email>{! conform to the "addr-spec" production in [RFC2822]}
	{#END}
		</{%tagname}>
	{#END}
	{#END}

	{#SUB categories}{#FOREACH $categories}
		<category term="{$name}"{#WHEN "$caption != \"\"" " label=\"%caption\""}{#WHEN "$scheme != \"\"" " scheme=\"%scheme\""}{#VAR sub=lang}/>
	{#END}
	{#END}

	{#SUB links}{#FOREACH $links}
		<link href="{%url}"{#WHEN "$rel != \"\"" " rel=\"%rel\""}{#WHEN "$type != \"\"" " type=\"%type\""}{#WHEN "$hreflang != \"\"" " hreflang=\"%hreflang\""}{#WHEN "$title != \"\"" " title=\"%title\""}{#WHEN "$length != \"\"" " length=\"%length\""}{#VAR sub=lang}/>{#END}
	{#END}

	{#SUB entry}
	<entry>
		{#VAR $title sub=text tagname=title}
		<updated>{%updated}</updated>{! content MUST conform to the "date-time" production in [RFC3339]}
	{#IF $published != ""}
		<published>{%published}</published>{! content MUST conform to the "date-time" production in [RFC3339]}
	{#END}
		<id>{%id}</id>
	{#VAR sub=persons tagname=author persons=$authors}
	{#VAR sub=categories}
	{#VAR sub=persons tagname=contributor persons=$contributors}
	{#IF $rights != ""}
		{#VAR $rights sub=text tagname=rights}
	{#END}
	{#IF is_array($source)}
		<source>
		{#VAR $source sub=entry}
		</source>
	{#END}
	{#VAR sub=links}
		{#VAR $summary sub=text tagname=summary}
	{#VAR $content sub=content}
	</entry>
{#END}

{#SUB content}
	{#LOAD strtolower($type) $type}
		{#IF $type == "" ||  $type == "text" || $type == "html" || $type == "xhtml"}
		{#VAR sub=text tagname=content}
		{#ELSE}
		{#IF $src == ""}
		<content type="{%type}"{#VAR sub=lang}>{$text}</content>
		{#ELSE}
		<content type="{%type}" src="{%src}"{#VAR sub=lang}/>
		{#END}
	{#END}
{#END}

{! wird &uuml;berall dort verwendet, wo Elemente "Language-Sensitive" sind }

{#SUB lang}
	{#WHEN "$lang != \"\"" " lang=\"$lang\""}
{#END}