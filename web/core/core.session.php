<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: core.session.php 1447 2007-08-10 11:09:21Z loom $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package core
 **/

/** Die Datei nur einmal includen */
if (defined("CORE.SESSION.PHP"))
	return 0;
define("CORE.SESSION.PHP", 1);

require_once ("core/core.config.php");
require_once ("core/core.mysql.php");
require_once ("core/core.log.php");

function InitSession() {
	global $Session;
	$Session = new AuthSession();
	$Session->execDoNext();
}

function SaveSession() {
	global $Session, $MysqlWriteLock;
	
	// kann keine Verbindung zur DB aufgebaut werden, gibt es keine Session
	if (is_object($Session)) {
		$l = $MysqlWriteLock;
		$MysqlWriteLock = 0;
		$Session->save();
		$MysqlWriteLock = $l;
	}
}

function SessionAddDoNext($callback, $userID = 0) {
	$callback = addslashes($callback);
	// wenn donext diesen callback noch nicht enth&auml;lt, wird er hinzugef&uuml;gt.
	$sql = "IF(LOCATE('&$callback',donext) = 0, CONCAT(donext,'&$callback'),donext)";
	$where = (empty ($userID)) ? "" : "WHERE user_id='$userID'";
	MysqlWrite("UPDATE " . TblPrefix() . "flip_session_data SET donext=$sql $where");
}

class Session {
	var $id;
	var $UserAgent;
	var $RemoteAddr;
	var $Data;
	var $TimeLastSeen;
	var $TimeCreate;
	var $_isNewSession;
	var $DoNext;

	function Session($id) {
		if (!$this->_load($id))
			trigger_error_text(text_translate("Eine Session existiert nicht") .
			"|ID:$id", E_USER_WARNING);
	}

	function _load($id) {
		//    echo "loading:$id\n";
		$id = addslashes($id);
		$dat = MysqlReadRowByID(TblPrefix() . "flip_session_data", $id, true);
		if (!$dat)
			return false;
		$this->_isNewSession = false;
		$this->id = $dat["id"];
		$this->RemoteAddr = $dat["remoteaddr"];
		$this->UserAgent = $dat["useragent"];
		$this->TimeCreate = $dat["time_create"];
		$this->TimeLastSeen = $dat["time_lastseen"];
		$this->Data = unserialize($dat["data"]);
		$this->DoNext = $dat["donext"];
		return true;
	}

	function execDoNext() {
		// $doNext ist array im Format von &path/file.php|function&path/file2.php|function2
		if (!empty ($this->DoNext)) {
			foreach (explode("&", $this->DoNext) as $callback)
				ExecCallback($callback);
			MysqlWriteByID(TblPrefix() . "flip_session_data", array (
				"donext" => ""
			), $this->id);
		}
	}

	function save() {	
		// session speichern
		$dat = array (
			"time_lastseen" => time(),
			"data" => serialize($this->Data)
		);
		
		if (!$this->_isNewSession) {
			return MysqlWriteByID(TblPrefix() . "flip_session_data", $dat, $this->id);
		}

		// oder neu erstellen
		$dat["id"] = $this->id;
		$dat["remoteaddr"] = $this->RemoteAddr;
		$dat["useragent"] = $this->UserAgent;
		$dat["time_create"] = $this->TimeCreate;
		MysqlWriteByID(TblPrefix() . "flip_session_data", $dat, 0, "", true);

		// alte sessions l&ouml;schen
		$mt = time() - (ConfigGet("session_timeout") * 60);
		MysqlExecQuery("DELETE FROM `" . TblPrefix() . "flip_session_data` WHERE (`time_lastseen` < $mt);");
	}
}

class AuthSession extends Session {
	var $Temporary;

	function AuthSession() {
		$this->Temporary = (isset ($_COOKIE["disable-flip-cookie"])) ? true : false;
		if ($this->Temporary) {
			$this->_new();
			$this->id = 0;
			LogAction(text_translate("Der Client mit der IP ?1? verzichtet auf eine Session", $this->RemoteAddr));
		} else {
			$CookieName = ConfigGet("session_cookiename");
			$msg = $this->_tryauth(isset ($_COOKIE[$CookieName]) ? $_COOKIE[$CookieName] : null);
			if (!empty ($msg)) {
				$this->_new();
				$h = nslookupbyaddr($this->RemoteAddr);
				LogAction(text_translate("Es wurde eine neue Session mit der ID {$this->id} f&uuml;r") . " $h IP:{$this->RemoteAddr} Agent:{$this->UserAgent} ID:{$this->id} " . text_translate("grund") . ":$msg");
				LogNetLog("session_created", $msg, NULL, $this, $this->RemoteAddr, $h, $this->UserAgent);
			}
			$this->_setRegistered();
			setcookie($CookieName, $this->id, time() + (ConfigGet("session_timeout") * 60));
		}
	}

	function _tryauth($id) {
		if (empty ($id))
			return "kein cookie";
		if (!$this->_load($id))
			return "ung&uuml;ltige cookie-id ($id)";
		if ($_SERVER["REMOTE_ADDR"] != $this->RemoteAddr) {
			LogAction(text_translate("Sicherheitswarnung: Der Client mit der SessionID \"?1?\" hat seine IP-Adresse von ?2? auf ?3? ge&auml;ndert.", array (
				$id,
				$this->RemoteAddr,
				$_SERVER["REMOTE_ADDR"]
			)));
			if (ConfigGet("session_checkip") == "Y")
				return text_translate("falsche IP-Adresse");
			$this->RemoteAddr = $_SERVER["REMOTE_ADDR"];
			MysqlWriteByID(TblPrefix() . "flip_session_data", array (
				"remoteaddr" => $this->RemoteAddr
			), $this->id);
		}
		if ($_SERVER["HTTP_USER_AGENT"] != $this->UserAgent) {
			LogAction(text_translate("Sicherheitswarnung: Der Client mit der SessionID \"?1?\" hat seinen UserAgent von ?2? auf ?3? ge&auml;ndert.", array (
				$id,
				$this->UserAgent,
				$_SERVER["HTTP_USER_AGENT"]
			)));
			if (ConfigGet("session_checkagent") == "Y")
				return text_translate("falscher user-agent");
			$this->UserAgent = $_SERVER["HTTP_USER_AGENT"];
			MysqlWriteByID(TblPrefix() . "flip_session_data", array (
				"useragent" => $this->UserAgent
			), $this->id);
		}
		return "";
	}

	function _new() {
		$this->_isNewSession = true;
		$this->id = sprintf("%8x", crc32(microtime() . $_SERVER["HTTP_USER_AGENT"] . $_SERVER["SCRIPT_FILENAME"]));
		$this->Data = array (
			"registerd_vars" => array ()
		);
		$this->RemoteAddr = $_SERVER["REMOTE_ADDR"];
		$this->UserAgent = $_SERVER["HTTP_USER_AGENT"];
		$this->TimeCreate = $this->TimeLastSeen = time();
	}

	function setUserID($id) {
		if (!$this->Temporary)
			MysqlWriteByID(TblPrefix() .
			"flip_session_data", array (
				"user_id" => $id
			), $this->id);
	}

	function Register($aVarname) {
		if (!isset ($this->Data["registerd_vars"][$aVarname]))
			$this->Data["registerd_vars"][$aVarname] = 1;
	}

	function Unregister($aVarname) {
		unset ($this->Data["registerd_vars"][$aVarname]);
	}

	function _getRegistered() {
		foreach ($this->Data["registerd_vars"] as $k => $v) {
			if(($k == 'User') && (is_a($v, 'User'))) {
				$v->compact();
			}
			
      global $$k;
      if($$k) $this->Data["registerd_vars"][$k] = $$k;
		}
	}

	function _setRegistered() {
		foreach ($this->Data["registerd_vars"] as $k => $v) {
      global $$k;
      $$k = $v;
		}
	}

	function save() {
		if (!$this->Temporary) {
			$this->_getRegistered();
			parent :: save();
		}
	}

}
?>