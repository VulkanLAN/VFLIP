<?php


/**
 * @author Moritz Eysholdt
 * @version $Id: core.utils.php 1702 2019-01-09 09:01:12Z loom $ edit by naaux
 * @copyright ? 2001-2007 The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package core
 **/

/** Die Datei nur einmal includen */
if (defined("CORE.UTILS.PHP"))
	return 0;
define("CORE.UTILS.PHP", 1);

/*** Konstanten ***/
/*
 *  fronk
 * Mittlerweile gibts ein paar TLDs mehr. Kein Sinn alle aufzulisten.
 * Matched Buchstaben, Zahlen, Punkt (für Subdomains (z.B. blah.priv.at) und second level domains wie co.at) und Minus (auch fuer IDN domains)
 * TLDs muessen mindestens 2 Zeichen haben.
 */
//define('TLDREGEX', '\.([a-zA-Z]{2}|com|edu|net|org|int|gov|mil|aero|biz|coop|info|museum|name|pro|arpa)');
define('TLDREGEX', '\.([a-zA-Z0-9.-]{2,})');
// /fronk

/*** some legacy-code ***********/

if (!function_exists('is_a')) {
	//PHP 4 >= 4.2.0, PHP 5
	function is_a($object, $className) {
		return ((strtolower($className) == get_class($object)) or (is_subclass_of($object, $className)));
	}
}

if (!function_exists('ctype_digit')) {
	//PHP 4 >= 4.2.0, PHP 5
	function ctype_digit($text) {
		return preg_match("/^[0-9]+$/", (string) $text);
	}
}

if (!function_exists("file_put_contents")) {
	//PHP 5
	function file_put_contents($Filename, $Data) {
		$f = fopen($Filename, "wb");
		$r = fwrite($f, $Data);
		fclose($f);
		return $r;
	}
}

//PHP 4 >= 4.3.0, PHP 5
if (!function_exists("file_get_contents")) {
	function file_get_contents($FileName) {
		$f = fopen($FileName, "rb");
		$r = fread($f, filesize($FileName));
		fclose($f);
		return $r;
	}
}

if (!function_exists("var_export")) {
	function var_export_rec($Data, $Indent) {
		if (is_array($Data)) {
			$l = array ();
			foreach ($Data as $k => $v)
				$l[] = "$Indent  " . ((is_numeric($k) ? $k : "'$k'")) . " => " . var_export_rec($v, "$Indent  ");
			$r = "array(\n" . implode(",\n", $l) . "\n$Indent)";
		}
		elseif (is_int($Data)) $r = $Data;
		elseif (is_float($Data)) $r = $Data;
		elseif (is_null($Data)) $r = "NULL";
		else
			$r = "'" . addcslashes($Data, "'\r\n\t") . "'";
		return $r;
	}

	//PHP 4 >= 4.2.0, PHP 5
	function var_export($Data, $Return = true) {
		$r = var_export_rec($Data, "") . ";\n";
		if ($Return)
			return $r;
		else
			echo $r;
	}
}

if (!function_exists("simplexml_load_string")) {
	// searches recursivly for tags
	// output is almost similar to simplexml_load_* but the root-tag is also shown an here are no objects used
	function parseXML($XMLString) {
		//split to outer tags
		$items = array ();
		preg_match_all("/<(\w+).*?>(.*?)<\/\\1>/is", $XMLString, $items, PREG_SET_ORDER); //not perfect attributehandling: for tags with numbers these are dropped. It should be checked if a space is after the tagname

		//no tag => content
		if (empty ($items))
			return $XMLString;

		//split content of tag into subtags
		foreach ($items AS $i) {
			$i[2] = parseXML($i[2]);
			if (!is_object($i[2]))
				$r-> {
				$i[1] }
			= $i[2];
else
	$r-> {
	$i[1] }
[] = $i[2];
}
//if tag is not multiple times in this level add attributes directly to array (one-dimension)
foreach ($r AS $tagname => $tagdata) {
	if (is_array($tagdata) && count($tagdata) === 1)
		$r-> $tagname = $tagdata[0];
}

return $r;
}

//remove root-tag so that the output is like simplexml_load_* output
//PHP 5
function simplexml_load_string($string) {
	$p = (array) parseXML($string);
	return array_shift($p);
}
}

/**
 * $number besteht nur aus Zahlen
 * 
 * @param String $number Zahl
 * @return boolean
 */
function is_posDigit($number) {
	if ($number === '' || is_object($number) || is_resource($number) || is_array($number))
		return false;

	/*
	 * ctype_digit('') == true
	 * 
	 * Confirming Crimson's bug in 4.3.10, Windows and Linux.  It affects all the ctype_ functions.
	 * It is caused by this bug handling large numbers,
	 * http://bugs.php.net/bug.php?id=34645
	 */
	if (version_compare(phpversion(), '5.0', '<=')) {
		if ($number === 0) {
			return true;
		}
		elseif ($number == '') {
			return false;
		}
	}

	return ctype_digit((string) $number); //"ctype_ treats small numbers as ASCII codes, not strings" (Comment by kitchin, 14-Oct-2006 04:36, http://de3.php.net/ctype_digit)
}

/**
 * SocketPutContents() Baut eine Socketverbindung auf, schreibt Daten hinein und beendet sie wieder.
 * Treten dabei Fehler auf, werden diese direkt ausgegeben. Ein Lesen aus der
 * Verbindung ist nicht m?glich.
 * 
 * @param string $Address Eine IP oder ein Hostname, optional mit Port (Host:Port)
 * @param $Data Die Daten, welche in die Socketverbindung geschrieben werden sollen.
 * @param integer $DefaultPort Ein Defaultport, welcher verwendet wird, wenn in $Adress keiner angegeben wurde.
 * @param integer $Timeout Der Timeout von fsockopen()
 * @return boolean True bei erfolg, ansonsten false.
 **/
function SocketPutContents($Address, $Data, $DefaultPort = 80, $Timeout = 30) {
	list ($host, $port) = array_pad(explode(":", $Address, 2), 2, $DefaultPort);
	$errno = 0;
	$errstr = "";
	$f = @ fsockopen($host, $port, $errno, $errstr, $Timeout);
	if (!$f) {
		trigger_error_text("Eine Socketverbindung konnte nicht hergestellt werden.|Host:$host:$port Err: $errno:$errstr", E_USER_WARNING);
		return false;
	}
	if (!fwrite($f, $Data)) {
		trigger_error_text("In eine Socketverbindung konnte nicht geschrieben werden.|Host:$host:$port Err: $errno:$errstr", E_USER_WARNING);
		fclose($f);
		return false;
	}
	fclose($f);
	return true;
}

/**
 * RelativePath()
 *   Berechnet den relativen Dateinamen einer Datei zu einem Verzeichnis.
 *   $dir und $file m?ssen den gleichen Verzeichnisursprung haben!
 * 
 * @param string $dir  Das Verzeichis zu dem der Dateinamen Relativ sein soll
 * @param string $file Der Dateiname
 * @return string Der relative Dateiname  
 **/

function RelativePath($dir, $file) {
	if (empty ($file))
		return "";
	$dirar = (strpos($dir, "/") === false) ? explode("\\", $dir) : explode("/", $dir);
	$filear = (strpos($file, "/") === false) ? explode("\\", $file) : explode("/", $file);
	if ($dirar[count($dirar) - 1] == "")
		array_pop($dirar);
	$i = 0;
	while (($i < count($dirar)) && ($i < (count($filear) - 1)) && ($dirar[$i] == $filear[$i]))
		$i++;
	$r = "";
	for ($j = $i; $j < count($dirar); $j++)
		$r .= "../";
	for ($j = $i; $j < (count($filear) - 1); $j++)
		$r .= "$filear[$j]/";
	return $r . $filear[count($filear) - 1];
}

function IsValidEmail($EMail) {
	return preg_match("/^[\d\w._+-]+@[\d\w\.\-]+" . TLDREGEX . "$/i", $EMail);
}

function Condense($data) {
	global $CoreConfig;
	return base64_encode(Encrypt(gzcompress(serialize($data)), $CoreConfig["root_password"]));
}

function Uncondense($string) {
	global $CoreConfig;
	
	$dec = Decrypt(base64_decode($string), $CoreConfig["root_password"]);
	$unc = gzuncompress($dec);
	
	return unserialize($unc);
}

/**
 * Ver&auml;ndert Parameter einer URL
 *
 * @param Array $Params Werte die als Parameter in der URL zus&auml;tzlich vorkommen sollen (leere Werte werden entfernt)
 * @param String $URL eine URL, wenn nicht angegeben wird die RequestURI verwendet (@see GetRequestURI())
 * @param boolean $htmlencode soll die neue URL HTML-escaped werden
 * @return String neue URL
 */
function EditURL($Params, $URL = '', $htmlencode = true) {
	if (empty ($URL))
		$URL = GetRequestURI();
	list ($file, $p) = array_pad(explode('?', $URL, 2), 2, '');
	$param = array ();
	parse_str($p, $param);
	$p = array ();
	foreach (array_merge($param, $Params) as $k => $v)
		if (!empty ($v)) {
			if (is_array($v))
				foreach ($v as $k2 => $a)
					$p[] = "{$k}[]=" . urlencode($a);
			else
				$p[] = "$k=" . urlencode($v);
		}
	if ($htmlencode)
		return $file . '?' . implode('&amp;', $p);
	else
		return $file . '?' . implode('&', $p);
}

function Redirect($URL) {
	LogSaveAsCookie();
	header('Location: '. $URL);
	exit();
}

function GetRequestURI() {
	// es gibt doch tats&auml;chlich Server auf denen $_SERVER["REQUEST_URI"] nicht gesetzt ist :/
	if (!empty ($_SERVER["REQUEST_URI"]))
		return $_SERVER["REQUEST_URI"];
	elseif (!empty ($_SERVER["SCRIPT_NAME"])) return $_SERVER["SCRIPT_NAME"] . ((empty ($_SERVER["QUERY_STRING"])) ? "" : "?$_SERVER[QUERY_STRING]");
	else
		trigger_error_text('Konnte die REQUEST_URI nicht ermitteln. Mit dem Webserver ist was faul...');
}

/**
 * Gibt den Host-Teil der URL zur&uuml;ck
 * z.B. http://www.example.com:81
 * 
 * @since 1356 - 06.02.2007
 * @return String Host-Teil der URL
 */
function ServerURL() {
	ArrayWithKeys($_SERVER, array (
		"SERVER_NAME",
		"SERVER_ADDR",
		"SERVER_PORT",
		"HTTP_HOST",
		"HTTPS"
	));
	$possible = array (
		$_SERVER["SERVER_NAME"],
		$_SERVER["SERVER_ADDR"],
		$_SERVER["HTTP_HOST"]
	);
	$host = "";
	foreach ($possible AS $host) {
		if (!empty ($host))
			break;
	}
	if (!empty ($host))
		return ((!empty ($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") ? "https://" : "http://") . $host . ((is_posDigit($_SERVER["SERVER_PORT"]) && $_SERVER["SERVER_PORT"] != "80") ? ":" . $_SERVER["SERVER_PORT"] : "");
	else
		return "";
}

// nameformat:
// dir/filename|function

function ExecCallback($CallbackName, $Args = array ()) {
	if (empty ($CallbackName))
		return;
	$a = explode("|", $CallbackName, 2);
	if (count($a) == 2) {
		$func = $a[1];
		include_once ($a[0]);
	} else
		$func = $a[0];
	return call_user_func_array($func, $Args);
}

function ForceDir($Dir, $ErrType = E_USER_WARNING) {
	return true;
	trigger_error_text($Dir);
	if (is_dir($Dir))
		return true;
		
	$d = "";
	foreach (explode("/", $Dir) as $f) {
		if (!empty ($f)) {
			$d .= $f;
			if (!is_dir($d))
				if (! mkdir($d)) {
					if (!empty ($ErrType)) {
						$err = "Ein Verzeichnis konnte nicht erstellt werden.|dir:$d";

						// evtl. wurde core.log.php noch nicht geladen.
						if (function_exists("ErrorCallback"))
							ErrorCallback($ErrType, $err, __FILE__, __LINE__);
						else
							trigger_error_text($err, E_USER_ERROR);
					}
					return false;
				}
		}
		$d .= "/";
	}
	return true;
}

/**
 * MicroSeconds()
 * 
 * @return float Die vergangene Zeit seit dem Unix-Timestamp in Mikrosekunden.
 * @author Moritz Eysholdt
 **/
function MicroSeconds() {
	$a = explode(" ", microtime());
	return ((float) $a[0]) + ((float) $a[1]);
}

/**
 * Encrypt3des()
 * Verschluesselt eine Zeichenfolge $data mithilfe von OpenSSL und TripleDES und $pwd.  
 * Siehe auch: Decrypt()
 * 
 * @param string $data Der zu verschuesselnde String
 * @param string $pwd Das Password mit dem der String verschluesselt wird.
 * @return string Der verschluesselte String
 * @author fronk
 * @see Decrypt()
 **/
function Encrypt($data, $pwd) {
  $key = $pwd;

  $message = $data;
  if ((strlen($message) % 8)) {
    $message = str_pad($message, strlen($message) + 8 - strlen($message) % 8, "\0");
  }
  
  // Shorten the key to 24 characters for 3DES, if neccessary
  if(strlen($key) > 24) {
    $key = substr($key, 0, 23);
  }

  // 3DES in ECB mode  
  $cipher = "des-ede3";
  $ivlen = openssl_cipher_iv_length($cipher);
  $iv = openssl_random_pseudo_bytes($ivlen);
  
  $encrypted_data = openssl_encrypt($message, $cipher, $key, OPENSSL_RAW_DATA|OPENSSL_NO_PADDING, $iv);
  
  return $encrypted_data;
}


/**
 * Encrypt()
 * Verschluesselt eine Zeichenfolge $data mithilfe von OpenSSL und TripleDES und $pwd.  
 * Siehe auch: Decrypt()
 * 
 * @param string $data Der zu verschuesselnde String
 * @param string $pwd Das Password mit dem der String verschluesselt wird.
 * @return string Der verschluesselte String
 * @author fronk
 * @see Decrypt()
 **/
function EncryptAes($data, $pwd) {
  $key = $pwd;
  $message = $data;
  
  // Shorten the key to 32 characters for AES GCM, if neccessary
  if(strlen($key) > 32) {
    $key = substr($key, 0, 32);
  }
  
  $cipher = "aes-256-gcm";
  $ivlen = openssl_cipher_iv_length($cipher);
  $iv = openssl_random_pseudo_bytes($ivlen);
  $ciphertext = openssl_encrypt($message, $cipher, $key, OPENSSL_RAW_DATA, $iv, $tag);
  
  $encrypted_data = '$$'.strlen($iv).":".$iv.'$$'.strlen($tag).":".$tag.'$$'.$ciphertext; // encode iv and tag into ciphertext
  
  return $encrypted_data;
}



/**
 * Decrypt3des()
 * Kann den von Encrypt() verschuesselten String wieder entschluesseln. Dabei findet keinerlei
 * Kontrolle statt, ob dieser Vorgang erfolgreich war.
 * 
 * @param string $data Die zu entschluesselnden Daten
 * @param string $pwd Das zu verwendene Password
 * @return string Die Entschluesselten Daten
 * @author fronk
 * @see Encrypt()
 **/
function Decrypt($encrypted_data, $password) {
  $key = $password;
    
	// Shorten the key to 24 characters for 3DES, if neccessary
  if(strlen($key) > 24) {
    $key = substr($key, 0, 23);
  }
  
  // 3DES in ECB mode  
  $cipher = "des-ede3";
  $decrypted_data = openssl_decrypt($encrypted_data, $cipher, $key, OPENSSL_RAW_DATA|OPENSSL_NO_PADDING);
  
  // Remove trailing null-bytes (\0) appended because of blockcipher padding
  $decrypted_data = rtrim($decrypted_data, "\0");
  
  return $decrypted_data;
  
}

/**
 * Decrypt()
 * Kann den von Encrypt() verschuesselten String wieder entschluesseln. Dabei findet keinerlei
 * Kontrolle statt, ob dieser Vorgang erfolgreich war.
 * 
 * @param string $data Die zu entschluesselnden Daten
 * @param string $pwd Das zu verwendene Password
 * @return string Die Entschluesselten Daten
 * @author fronk
 * @see Encrypt()
 **/
function DecryptAes($encrypted_data, $password) {
  $key = $password;
    
	// Shorten the key to 32 characters for AES GCM, if neccessary
  if(strlen($key) > 32) {
    $key = substr($key, 0, 32);
  }

  // get length of IV and hmac
  $m = [];
  if(preg_match('/^\$\$(\d+):.+\$\$(\d+):.+\$\$/s', $encrypted_data, $m)) {
    $ivlen = $m[1];
    $taglen = $m[2];
  } else {
    return Decrypt3des($encrypted_data, $password);
  }
  
  // get offsets
  $ivOffset = 3 + strlen($ivlen);
  $tagOffset = $ivOffset + $ivlen + 3 + strlen($taglen);
  $ciphertextOffset = $tagOffset + $taglen + 2;
  
  $iv = substr($encrypted_data, $ivOffset, $ivlen);
  $tag = substr($encrypted_data, $tagOffset, $taglen);
  $ciphertext = substr($encrypted_data, $ciphertextOffset);
  
  //var_dump($key, $encrypted_data, $ivlen, $iv, $taglen, $tag, $ciphertext);
  
  $cipher = "aes-256-gcm";
  $decrypted_data = openssl_decrypt($ciphertext, $cipher, $key, OPENSSL_RAW_DATA, $iv, $tag);
  //var_dump($decrypted_data);exit;
  
  // Remove potential trailing null-bytes (\0)
  $decrypted_data = rtrim($decrypted_data, "\0");
  
  return $decrypted_data;
  
}

function GetRawPostData() {
	if (version_compare(PHP_VERSION, "4.3.0", ">="))
		return file_get_contents("php://input");
	else
		return $GLOBALS['HTTP_RAW_POST_DATA'];
}

/**
 * Löscht rekursiv alle Eintr?ge in einem Array, die empty() sind.
 * 
 * @param array $Array
 * @return array Das aufger?umte Array
 * @author Moritz Eysholdt
 **/
function ArrayCleanup($Array) {
	foreach ($Array as $k => $v) {
		if (empty ($v))
			unset ($Array[$k]);
		elseif (is_array($v)) $Array[$k] = ArrayCleanup($v);
	}
	return $Array;
}

function nslookupbyaddr($ip) {
	if (ConfigGet("network_nslookup") == "Y")
		return gethostbyaddr($ip);
	return false;
}

function nslookupbyname($name) {
	if (ConfigGet("network_nslookup") == "Y")
		return gethostbyname($name);
	return false;
}

/**
 * f&uuml;hrt ausser htmlentites() eine Ersetzung von | (pipe) durch
 *
 * @param string $string Text welcher htmlescaped werden soll
 * @return string htmlescapeder String
 **/
function ErrorEntities($string) {
	$string = escapeHtml($string);
	return str_replace("|", "&brvbar;", $string);
}

function GetSimpleStacktrace($html = false) {
	$trace = debug_backtrace();
	array_shift($trace);
	$r = array ();
	$dir = dirname(dirname(__FILE__));
	foreach ($trace as $t) {
		$file = isset($t['file']) ? RelativePath($dir, $t['file']) : '';
		$class = (empty ($t['class'])) ? '' : "$t[class]->";
		$func = (empty ($t['function'])) ? '' : "|$class$t[function]()";
		$r[] = "$file$func:". (isset($t['line']) ? $t['line'] : '');
	}
	if ($html)
		return implode("<br/>\n", $r);
	return implode("\n", $r);
}

/**
 * Setzt das ausf&uuml;hrungslimit nur wenn Safemode nicht an ist, um die Fehlermeldung zu unterdr&uuml;cken
 */
function settimelimit($n = 0) {
	if (ini_get("safe_mode") != "On" && ini_get("safe_mode") != "1")
		set_time_limit($n);
}

/**
 * Speichert ein Array als CSV-Datei (im Browser "speichern unter..")
 * 
 * Jedes Arrayelement enth&auml;lt eine Zeile. Diese ist wiederum ein Array mit den Werten.
 * z.B. array("Zeile1" => array("Spalte1", "Spalte2") )
 * ergibt "Spalte1;Spalte2"
 * 
 * optional kann ein zweites Array mit &uuml;berschriften angegeben werden.
 * Die Anzahl der Spalten wird nicht gepr&uuml;ft!
 * 
 * @param String $filename Dateiname der zu speichernden Datei
 * @param Array $data Datenarray (s.o.)
 * @param Array $head optionale &uuml;berschriften
 * @return bool true
 */
function saveCSV($filename, $data, $head = false) {
	//file-extension
	if (strtolower(substr($filename, -3)) != "csv")
		$filename .= ".csv";

	//check data
	if (!is_array($data)) {
		trigger_error_text("Die CSV-Daten haben das falsche Format!|\$data ist vom Typ " . gettype($data), E_USER_ERROR, __LINE__, __FILE__);
		return false;
	}

	//head
	if (empty ($head))
		$head = false;
	if (is_array($head))
		$csvdata = formatCSVdata(array (
			$head
		));
	else
		$csvdata = "";

	//body
	$csvdata .= formatCSVdata($data);

	//Output
	header("Content-Type: text/csv; header=" . (($head) ? "present" : "absent"));
	header("Content-Disposition: attachment; filename=$filename");
	echo $csvdata;
	flush();

	return true;
}

/**
 * Wandelt ein Array in einen CSV-String um
 * 
 * @link http://www.rfc-editor.org/rfc/rfc4180.txt Common Format and MIME Type for Comma-Separated Values (CSV) Files
 * @param Array $data zweidimensionales Array; array("Zeile" => array("Spalte1", "Spalte2") )
 * @param String $seperator Ein optionales, benutzerdefiniertes Trennzeichen - default ','
 * @return String CSV-Zeichenkette
 */
function formatCSVdata($data, $seperator = ',') {
	if (!is_array($data) || empty($seperator))
		return "";
	$csvdata = "";
	foreach ($data As $row) {
		//escape double-quotes
		$row = str_replace("\"", "\"\"", $row);
		$csvdata .= "\"" . implode('"'.$seperator.'"', $row) . "\"\r\n";
	}
	return $csvdata;
}

/**
 * Setzt den String escaped in Hochkommata
 * 
 * @since 1345 - 24.01.2007
 * @param String $sqlvalue Dieser Text wird (sql-)gesichert
 * @return String
 */
function escape_sqlData_without_quotes($sqlvalue) {
	global $MysqlConnection;
	if (is_null($sqlvalue))
		return 'NULL';
	else
		return mysqli_real_escape_string($MysqlConnection, $sqlvalue);
}  

function escape_sqlData($sqlvalue) {
	if (is_null($sqlvalue))
		return 'NULL';
	else
		return '\'' .escape_sqlData_without_quotes($sqlvalue) . '\''; 
}

/**
 * Escaped jeden Eintrag eines Arrays und verkn&uuml;pft ihn
 * 
 * @since 1345 - 24.01.2007
 * @param String $glue Zwischentext
 * @param Array $array Daten
 * @return String
 */
function implode_sql($glue, $array) {
	foreach ($array AS $k => $v)
		$array[$k] = escape_sqlData($v);
	return implode($glue, $array);
}

/**
 * Alle Werte des Array werden in Hochkommata durch Komma getrennt aufgelistet
 * z.B. array("Das war was", "Da hab' ich es geschafft")
 * => 'Das war was','Da hab\' ich es geschafft'
 * 
 * @since 1345 - 24.01.2007
 * @param Array Array mit Werten
 * @return String
 */
function implode_sqlIn($array) {
	if (empty ($array))
		return "''";
	else
		return implode_sql(",", $array);
}

/**
 * Meldungen werden HTML-escaped
 * trigger_error (htmlspecialchars($msg),$type)
 * 
 * @since 1345 - 24.01.2007
 * @param String $msg Text
 * @param int $type Fehlertyp (E_USER_X)
 */
function trigger_error_text($msg, $type = E_USER_NOTICE, $file = __FILE__, $line = __LINE__) {
	return trigger_error(escapeHtml($msg) . "|Aufruf: " . basename($file) . "[line $line]", $type);
}

/**
 * Es wird die Variable $_SERVER['HTTP_REFERER'] zur&uuml;ckgegeben
 * und ggf. eine Warnung ausgegeben, wenn sie leer sein sollte
 * (z.B. wegen einer Firewall)  
 *  
 * GetHttpReferer ()
 * 
 * @since 1355 - 31.01.2007
 * @return String
 */
function GetHttpReferer($showMessage = true) {
	if (!empty ($_SERVER['HTTP_REFERER']))
		return $_SERVER['HTTP_REFERER'];
	elseif (stristr($_SERVER['QUERY_STRING'], 'menudir') && $showMessage) trigger_error_text("Der Browser hat keine HTTP-Referer-Angaben gesendet!
			    Bitte &uuml;berpr&uuml;fe deine Browser- oder deine Firewall-Einstellungen da die
			    Seite an einigen Stellen sonst nicht richtig funktionieren wird!", E_USER_WARNING);
	return '';
}

/**
 * Pr&uuml;ft ob die angegeben Schl&uuml;ssel im Array vorhanden sind, ansonsten werden
 * sie hinzugef&uuml;gt (mit null initialisiert)
 * @since 1409 - 04.06.2007
 * @author loom
 * @param Array $array zu pr&uuml;fendes Array
 * @param Array $keys Array mit Schl&uuml;sseln
 */
function ArrayWithKeys(& $array, $keys) {
	if (is_array($array) && is_array($keys)) {
		foreach ($keys AS $key) {
			if (!isset ($array[$key]))
				$array[$key] = null;
		}
	} else {
		trigger_error_text("Falscher Parameter f&uuml;r ArrayWithKeys()!|\$array: " . gettype($array) . ", \$keys: " . gettype($keys), E_USER_ERROR, __LINE__, __FILE__);
	}
}

/**
 * Funktioniert wie die Funktion empty(),
 * wertet 0 (Zahl) allerdings nicht als empty
 * @since 1471 - 07.10.2007
 * @author scope
 * @param mixed $item Das zu pr&uuml;fende Objekt
 */
function is_empty($item) {
	return (empty($item) && ($item != 0));
}

/**
 * Konvertiert die Zeichen &auml;&ouml;&uuml;&szlig; in ihre
 * HTML-&Auml;quivalenten.
 * @since 16.07.2010
 * @author scope
 * @param mixed $text Der zu konvertierende Text
 */
function convertSpecialChars($text) {
	$converted = str_replace('ä', '&auml;', $text);
	$converted = str_replace('Ä', '&Auml;', $converted);
	$converted = str_replace('ü', '&uuml;', $converted);
	$converted = str_replace('Ü', '&Uuml;', $converted);
	$converted = str_replace('ö', '&ouml;', $converted);
	$converted = str_replace('Ö', '&Ouml;', $converted);
	$converted = str_replace('ß', '&szlig;', $converted);
	$converted = str_replace('§', '&sect;', $converted);
	
	return $converted;
}

/**
 * Sorgt daf&uuml;r, dass bereits codierte HTML-Codes nicht
 * unwirksam werden, wenn das Apersand "&" nochmals kodiert wird.
 * &auml; -> &amp;auml;
 * 
 * Zwar besitzt die Funktion einen Parameter, der das erledigt - jedoch
 * erst ab PHP >= 5.2.3.
 * @param mixed $string Der zu kodierende text
 * @return Der kodierte Text ohne doppelte Kodierungen
 */
function htmlentities_single($string, $mode = ENT_COMPAT, $charset = 'UTF-8') {
	// Maskiere alle bereits codierten Entitaeten
	$coded = str_replace('&auml;', '$auml;', $string);
	$coded = str_replace('&Auml;', '$Auml;', $coded);
	$coded = str_replace('&uuml;', '$uuml;', $coded);
	$coded = str_replace('&Uuml;', '$Uuml;', $coded);
	$coded = str_replace('&ouml;', '$ouml;', $coded);
	$coded = str_replace('&Ouml;', '$Ouml;', $coded);
	$coded = str_replace('&slig;', '$szlig;', $coded);
	
	// Schicke den string durch htmlentities_single()
	$coded = htmlentities($coded, $mode, $charset);
	
	// Demaskiere die Entitaeten wieder
	$coded = str_replace('$auml;', '&auml;', $coded);
	$coded = str_replace('$Auml;', '&Auml;', $coded);
	$coded = str_replace('$uuml;', '&uuml;', $coded);
	$coded = str_replace('$Uuml;', '&Uuml;', $coded);
	$coded = str_replace('$ouml;', '&ouml;', $coded);
	$coded = str_replace('$Ouml;', '&Ouml;', $coded);
	$coded = str_replace('$slig;', '&szlig;', $coded);	 
	
	return $coded;
}

/**
 * Gibt den Text HTML-escaped zurück
 *
 * @see escapeHtml()
 * @param String $string
 * @param boolean $escapeAll gibt an ob alle Zeichen mit HTML-entsprechung escaped werden oder nur & > < "
 * @return String
 */
function escapeHtml($string, $escapeAll = false) {
	if($escapeAll)
		return htmlspecialchars($string, ENT_COMPAT, 'UTF-8');
	else
		return htmlentities_single($string, ENT_QUOTES, 'UTF-8');
}

/**
 * Returns a decimal number that has been formatted to be recognized by MySQL.
 * 
 * @param mixed $number The number to format
 */
function formatMySQLDecimal($number) {
	return str_replace(',', '.', $number);
}

/*
 * Returns bcrypt hash with 2^$cost iterations including salt, algo and cost
 */
function getPasswordHash($password) {
  $cost = 12;
  $cfgCost = ConfigGet("user_bcrypt_cost");

  // make sure config value is sane
  if(is_numeric($cfgCost) && $cfgCost > 2) {
    $cost = $cfgCost;
  }

  return password_hash($password, PASSWORD_BCRYPT,['cost' => $cost]);
}

function verifyPasswordHash($password, $hash) {
  return password_verify($password, $hash);
}
