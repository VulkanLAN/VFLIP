<?php

/**
 * Die Datei core.mysql.php baut eine Verbindung zu einer in der CoreConfig festgelegten 
 * MySQL-Datebank eine Verbindung auf und stellt dann Funktionen und Klassen bereit um in 
 * diese Datenbank zu Schreiben, bzw. zu Lesen.
 * 
 * Einige Beispiele in der Dokumentation der Funktionen und Klassen in diesem Script 
 * beziehen sich auf folgende Tabelle:
 * 
 * Tabelle `comupter`:
 * +---+------+-----+--------+
 * |id |tower |cpu  |monitor |
 * +---+------+-----+--------+
 * |1  |mini  |500  |15      |
 * +---+------+-----+--------+
 * |2  |mini  |700  |15      |
 * +---+------+-----+--------+
 * |3  |midi  |900  |17      |
 * +---+------+-----+--------+
 * |4  |midi  |1100 |17      |
 * +---+------+-----+--------+
 * |5  |big   |1300 |19      |
 * +---+------+-----+--------+
 *
 * @author Moritz Eysholdt
 * @version $Id: core.mysql.php 1499 2018-05-08 12:18:40Z scope $ edit by naaux
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package core
 **/

/** Die Datei nur einmal includen */
if (defined("CORE.MYSQL.PHP"))
	return 0;
define("CORE.MYSQL.PHP", 1);

require_once ("core/core.config.php");

/**
 * Die Funktion MysqlConnect() baut eine Verbindung zu dem MySQL-Server auf, der in der 
 * Config-Section "mysql" angegeben wurde. Ausserdem wird die dort angegebene Datenbank ausgew&auml;hlt.
 * Die Datenbankverbindung wird in der globalen Variable $MysqlConnection abgelegt.
 * 
 * @access private
 **/
function MysqlConnect() {
	global $MysqlConnection, 
		   $MysqlWriteLock, 
		   $CoreConfig, 
		   $MysqlPerformanceTime, 
		   $MysqlPerformanceQueryCount, 
		   $MySQLTransactionRunning, 
		   $MySQLTransactionDepth;
		   
	$MysqlWriteLock = $CoreConfig["db_writelock"];
	$MySQLTransactionRunning = false;
	$MySQLTransactionDepth = 0;
	
	if (!empty ($CoreConfig["db_performancelogdir"])) {
		$MysqlPerformanceLog = "";
		$MysqlPerformanceTime = MicroSeconds();
		$CoreConfig["db_performancelogdir"] = realpath($CoreConfig["db_performancelogdir"]) . "/";
	}
	
	// Always init the query counter
	$MysqlPerformanceQueryCount = 0;
	
	$flags = ($CoreConfig["db_compression"]) ? MYSQL_CLIENT_COMPRESS : 0;
	$host = "$CoreConfig[db_host]:$CoreConfig[db_port]";
	$user = $CoreConfig["db_user"];
	$pass = $CoreConfig["db_pass"];
	$pers = $CoreConfig["db_persistent"];
	$dbname = $CoreConfig["db_name"];

	if(version_compare(PHP_VERSION, "5.6.0", ">=")) {
		$MysqlConnection = ($pers) ? mysqli_connect($host, $user, $pass, $dbname) : mysqli_connect($host, $user, $pass, $dbname, false);
	} 
	else {
		MysqlError("Es konnte keine Verbindung zur Datenbank hergestellt werden. Bitte ein Upgrade auf PHP 5.6 oder hoeher durchfuehren.");
		return false;
	}

	if (!$MysqlConnection) {
		MysqlError("Es konnte keine Verbindung zur Datenbank hergestellt werden.|Verbindung: mysql://$CoreConfig[db_user]@$CoreConfig[db_host]:$CoreConfig[db_port]", E_USER_ERROR);
		return false;
	}

	if (!mysqli_select_db($MysqlConnection, $CoreConfig["db_name"])) {
		MysqlError("Auf die ben&ouml;tigte Datenbank kann nicht zugegriffen werden.|Datenbankname: $CoreConfig[db_name]", E_USER_ERROR);
		return false;
	}
	
	if (!mysqli_set_charset($MysqlConnection, "utf8")){
		MysqlError('Konnte die Datenbank nicht mit UTF-8 lesen und schreiben!', E_USER_ERROR);
		return false;
	} 
	
	else {
	mysqli_query ($MysqlConnection, 
		'SET character_set_results = \'utf8\', ' 
			. 'character_set_client = \'utf8\', ' 
			. 'character_set_connection = \'utf8\', ' 
			. 'character_set_database = \'utf8\', ' 
			. 'character_set_server = \'utf8\'');
	return true;
	} 
}

function MysqlShutdown() {
	global $CoreConfig, $MysqlPerformanceQueryCount, $MysqlPerformanceLog, $MysqlPerformanceTime;
	if (empty ($CoreConfig["db_performancelogdir"]))
		return;
	if ($CoreConfig["db_performancelog_mincount"] > $MysqlPerformanceQueryCount)
		return;
	$t = MicroSeconds() - $MysqlPerformanceTime;
	$f = $CoreConfig["db_performancelogdir"] . sprintf("page-%03d-%04.6f", $MysqlPerformanceQueryCount, $t * 1000) . "-" .
	preg_replace("/[^a-z0-9\-_=\&\.]/i", "_", basename(GetRequestURI())) . ".php";
	file_put_contents($f, $MysqlPerformanceLog);
}

/**
 * MysqlError() gibt einen Fehler mittels trigger_error() aus und reichert diesen vorher mit Datenbankspezifischen Informationen an.
 * 
 * @param $Msg Die Fehlernachricht
 * @param integer $Type (E_USER_NOTICE,E_USER_WARNING,E_USER_ERROR) Wenn nicht angegeben, wird der Type automatisch in 
 *   Abh&auml;ngigkeit vom Errorcode der DB gesetzt.
 **/
function MysqlError($Msg, $Type = 0) {
	global $MysqlConnection;
	list ($msg, $debug) = explode("|", $Msg, 2);
	/*alte abfrage $err = (is_resource($MysqlConnection)) ? mysql_errno($MysqlConnection) : mysql_errno(); */
	$err = (is_resource($MysqlConnection)) ? mysqli_errno($MysqlConnection) : ""; 
	if (!empty ($err))
		$debug .= " Fehler $err: " . ((is_resource($MysqlConnection)) ? mysqli_errno($MysqlConnection) : "");
	if (empty ($Type))
		switch ($err) {
			// messages from http://dev.mysql.com/doc/mysql/en/error-handling.html
			case (1044) : // Access denied for user '%s'@'%s' to database '%s'
				$msg .= " Der Zugriff auf die Datenbank wurde verweigert.";
				$Type = E_FLIP_SYSERROR;
				break;
			case (1062) : // Duplicate entry '%s' for key %d
				$msg .= " Es gibt bereits einen Datensatz der diese Werte verwendet. " . mysqli_error();
				$Type = E_USER_WARNING;
				break;
			case (1146) : // Table '%s.%s' doesn't exist
				// eine Tabelle existiert nicht.
				$msg .= " " . mysqli_error();
				$Type = E_FLIP_SYSERROR;
				break;
			case (2006) : // MySQL server has gone away
				$msg .= " Die Datenbank hat den Query abgebrochen. Eventuell wurde das Limit f&uuml;r die maximale Datenfeldgr&ouml;&szlig;e &uuml;berschritten.";
				$Type = E_USER_ERROR;
				break;
			case (2013) : // Lost connection to MySQL server during query
				$msg .= " Die Verbindung zur Datenbank ist w&auml;hrend eines Queries unterbrochen worden.";
				$Type = E_FLIP_SYSERROR;
				break;
			default :
				$Type = E_USER_WARNING;
		}
	ErrorCallback($Type, "$msg|$debug", __FILE__, __LINE__);
}

/**
 * Die Funktion MysqlExecQuery() f&uuml;hrt eine MySQL-Datenbankabfrage durch und gibt den Ergebnis-Zeiger zur&uuml;ck.
 * 
 * @param string $Query Der MySQL-Befehl. (z.B. SHOW TABLES;)
 * @param string $Msg Eine Fehlermeldung die ausgegeben wird, wenn der MySQL-Befehl 
 * nicht fehlerfrei ausgef&uuml;hrt werden kann.
 * @return resource Die Resourcekennung des Abfrageergebnisses, oder im falle eines Fehlers: FALSE
 * @access private
 **/
function MysqlExecQuery($Query, $Msg = "") {
	global $MysqlConnection, $MysqlWriteLock, $CoreConfig, $MysqlPerformanceLog, $MysqlPerformanceQueryCount;
	
	if (empty ($Msg)) {
		$Msg = "Bei einem Datenbankzugriff ist ein Fehler aufgetreten.";
	}
	
	if ($MysqlWriteLock) {
		if (!preg_match("/^(SELECT|SHOW|CREATE|EXPLAIN|DESCRIBE)/i", trim($Query))) {
			trigger_error_text("Die Datenbank wurde f&uuml;r Schreibzugriffe gesperrt. Die Daten wurden nicht gespeichert.");
			// echo "<pre>".trim($Query)."</pre>";
			return false;
		}
	}
	
	$Query = chop($Query);
	$MysqlPerformanceQueryCount++;
	
	if (empty ($CoreConfig["db_performancelogdir"])) {
		$r = @ mysqli_query($MysqlConnection, $Query);
	} else {
		$t = MicroSeconds();
		$r = @ mysqli_query($MysqlConnection, $Query);
		$t = (MicroSeconds() - $t) * 1000;
		$bt = GetSimpleStacktrace(false);
		$MysqlPerformanceLog .= "Query: $Query\n" .
		"Time: {$t}ms\n$bt\n\n";
		if ($CoreConfig["db_performancelog_mintime"] < $t)
			file_put_contents($CoreConfig["db_performancelogdir"] .
			sprintf("query-%04.6f.php", $t), "$Query\n\n$bt");
	}

	if ($r) {
		return $r;
	}
	
	MysqlError("$Msg|Query: $Query");
	
	return false;
}

/**
 * Die Funktion MysqlReadArea() f&uuml;hrt eine Lese-Abfrage &uuml;ber beliebig viele Spalten 
 * und Zeilen in der Datenbank aus. Das Ergebnis wird als zweidimensionales Array zur&uuml;ckgegeben.
 * Wird der Parameter $KeyName angegeben, ist die erste Dimension ein assotiatives Array, welches 
 * die Werte der duch $KeyName angegebenen Tabellen-Spalte als Keys verwendet. Ist $KeyName nicht angegeben, 
 * ist die erste Dimension nicht Assotiativ (d.H. beginnend bei Null nummerisch durchindiziert)
 * In der zweiten Dimension dienen die Spaltennamen als Keys.
 * 
 * - Beispiel -
 * <code>
 * print_r(MysqlReadArea("SELECT * FROM `computer`;","cpu"));
 * 
 * Ausgabe:
 * Array(
 *  500  => Array("id" => 1, "tower" => "mini", "cpu" => 500 , "monitor" =>15),
 *  700  => Array("id" => 2, "tower" => "mini", "cpu" => 700 , "monitor" =>15),
 *  900  => Array("id" => 3, "tower" => "midi", "cpu" => 900 , "monitor" =>17),
 *  1100 => Array("id" => 4, "tower" => "midi", "cpu" => 1100, "monitor" =>17),
 *  1300 => Array("id" => 5, "tower" => "big" , "cpu" => 1300, "monitor" =>19)
 * );
 * </code>
 * 
 * @param string $Query Der MySQL-Abfragebefehl.
 * @param string $KeyName Der Name der Spalte dessen Werte f&uuml;r die erste Dimension 
 * des Abfrageergebnisses als Keys verwendet werden. Achtung: Wenn in dieser Spalte Werte
 * doppelt auftreten, werden sich die einzelnen Zeilen "&uuml;berschreiben" und somit bei der Abfrage 
 * Zeilen verloren gehen. 
 * @return array Ein zweidimensionales Array welches das Abfrageergebnis enth&auml;lt oder FALSE.
 **/
function MysqlReadArea($Query, $KeyName = "") {
	$q = MysqlExecQuery($Query, "Beim lesen aus der Datenbank ist ein Fehler aufgetreten.");
	if (!$q)
		return false;
		
	$r = array ();
	if (empty ($KeyName))
		while ($i = mysqli_fetch_assoc($q))
			$r[] = $i;
	else
		while ($i = mysqli_fetch_assoc($q))
			$r[$i[$KeyName]] = $i;
	mysqli_free_result($q);
	
	return $r;
}

/**
 * Die Funktion MysqlReadCol() liest eine Spalte einer Tabelle aus der Datenbank 
 * aus und gibt as Ergebnis als eindimensionales Array zur&uuml;ck. Wird mit $KeyName
 * eine zweite Spalte angegeben, werden ihre Werte als Keys im Ergebnisarray verwendet.
 * 
 * - Beispiel -
 * <code>
 * print_r(MysqlReadCol("SELECT `cpu`,`tower` FROM `computer`;","tower","cpu"));
 * 
 * Ausgabe:
 * Array(
 *  500  => "mini",
 *  700  => "mini",
 *  900  => "midi",
 *  1100 => "midi",
 *  1300 => "big" 
 * );
 * </code>
 * 
 * @param string $Query Der MySQL-Abfragebefehl.
 * @param string $ColName
 * @param string $KeyName Der Name der Spalte dessen Werte f&uuml;r das Abfrageergebnis
 * als Keys verwendet werden. Achtung: Wenn in dieser Spalte Werte doppelt auftreten,
 * werden sich die einzelnen Zeilen "&uuml;berschreiben" und somit bei der Abfrage 
 * Zeilen verloren gehen. 
 * @return array Ein zweidimensionales Array welches das Abfrageergebnis enth&auml;lt.
 **/
function MysqlReadCol($Query, $ColName = "", $KeyName = "") {
	$q = MysqlExecQuery($Query, "Beim lesen einer Spalte aus der Datenbank ist ein Fehler aufgetreten.");
	if (!$q)
		return false;

	$r = array ();
	if (empty ($ColName))
		while ($i = mysqli_fetch_row($q))
			$r[] = $i[0];
	elseif (empty ($KeyName)) while ($i = mysqli_fetch_assoc($q))
		$r[] = $i[$ColName];
else
	while ($i = mysqli_fetch_assoc($q))
		$r[isset ($i[$KeyName]) ? $i[$KeyName] : null] = $i[$ColName];
mysqli_free_result($q);
return $r;
}

/**
 * Die Funktion MysqlReadRow() liest eine Zeile aus einer Tabelle der Datenbank aus und gibt
 * diese als eindimensionales Array zur&uuml;ck, wobei die Spaltennamen als Keys verwendet werden.
 * 
 * - Beispiel -
 * <code>
 * print_r(MysqlReadRow("SELECT * FROM `computer` WHERE (`cpu`= 900);"));
 * 
 * Ausgabe:
 * Array(
 *   "id" => 3,
 *   "tower" => "midi",
 *   "cpu" => 900 ,
 *   "monitor" =>17
 * );
 * </code>
 * 
 * @param string $Query Der MySQL-Abfragebefehl.
 * @return array Die erste Zeile des Abfrageergebnisses oder FALSE bei einem Fehler
 **/
function MysqlReadRow($Query, $TryOnly = false, $ErrorMsg = "") {
	$q = MysqlExecQuery($Query, (empty ($ErrorMsg)) ? "Beim Lesen eines Datensatzes aus der Datenbank ist ein Fehler aufgetreten." : $ErrorMsg);
	
	if (!$q) {
		return false; 
	}
	
	$r = mysqli_fetch_assoc($q);
	
	if (!$TryOnly)
		if (!is_array($r))
			trigger_error_text((empty ($ErrorMsg)) ? "Ein angeforderter Datensatz wurde nicht gefunden. | Query: $Query" : $ErrorMsg, E_USER_ERROR, __FILE__, __LINE__);
	mysqli_free_result($q);
	return $r;
}

/**
 * Die Funktion MysqlReadField() liest ein Feld aus einer Tabelle der Datenbank aus und gibt
 * dieses als string zur&uuml;ck.
 * 
 * - Beispiel -
 * <code>
 * print_r(MysqlReadField("SELECT `id` FROM `computer` WHERE (`cpu`= 900);"));
 * 
 * Ausgabe:
 * 3
 * </code>
 * 
 * @param string $Query Der MySQL-Abfragebefehl.
 * @param string $ColName Der Name oder der Index Spalte in der sich das abzufragende Feld befindet.
 * @return string der Wert des abgefragten Feldes.
 **/
function MysqlReadField($Query, $ColName = 0, $TryOnly = false) {
	$q = MysqlExecQuery($Query, "Beim lesen eines Feldes aus der Datenbank ist ein Fehler aufgetreten.");
	if (!$q)
		return false;
	$r = mysqli_fetch_array($q);
	mysqli_free_result($q);
	if (is_array($r))
		return $r[$ColName];
	if (!$TryOnly)
		trigger_error_text("Der Datensatz aus welchem ein Feld angefordert wurde existiert nicht.|Query: $Query", E_USER_WARNING, __FILE__, __LINE__);
	return false;
}

/**
 * Die Funktion MysqlWrite() f&uuml;hrt einen MySQL-Schreibbefehl aus. Zu den Schreibbefehlen z&auml;hlen 
 * alle MySQL-Operationen welche den Datenbestand ver&auml;ndern. Z.B. INSERT, UPDTATE, DELETE, usw.
 * 
 * @param string $Query Der MySQL-Schreibbefehl
 * @param string $ErrOnFail Eine Fehlermeldung welche ausgegeben wird wenn der Schreibbefehl 
 * nicht fehlerfrei ausgef&uuml;hrt werden konnte
 * @return bool TRUE bei erfolg, ansonsten FALSE
 **/
function MysqlWrite($Query, $ErrOnFail = "") {
	if (empty ($ErrOnFail))
		$ErrOnFail = "Beim Schreiben in die Datenbank ist ein Fehler aufgetreten.";
	return (MysqlExecQuery($Query, $ErrOnFail)) ? true : false;
}

function MysqlDeleteRow($Query) {
	global $MysqlConnection;
	if (!MysqlExecQuery($Query, "Beim L&ouml;schen eines Datensatzes ist ein Fehler aufgetreten."))
		return false;
	$r = mysqli_affected_rows($MysqlConnection);
	if ($r == 1)
		return true;
	if ($r < 1)
		trigger_error_text("Der zu l&ouml;schende Datensatz wurde nicht gefunden.|Query: $Query", E_USER_WARNING, __FILE__, __LINE__);
	if ($r > 1)
		trigger_error_text("Es wurden $r statt einem Datansatz gel&ouml;scht. Das sollte nicht vorkommen :/|Query: $Query", E_USER_WARNING, __FILE__, __LINE__);
	return false;
}

/**
 * Die Funktion MysqlReadRowByID() liest die durch $ID angegebene Zeile der Tabelle $TableName 
 * aus der Datenbank aus. Die R&uuml;ckgabe des Ergebnisses ist identisch mit MysqlReadRow().
 * Achtung: Diese Funktion kann nur auf Tabellen angewand werden, welche eine Spalte namens `id` besitzen!
 * 
 * @param string $TableName der Name der Tabelle aus der die Zeile ausgelesen werden soll.
 * @param integer $ID Die ID welche die Zeile idetifiziert. 
 * @param bool $TryOnly wenn true, wird keine Fehlermeldung ausgegeben sollte der angeforderte Datensatz nicht existieren.
 * @return array Die angeforderte Zeile.
 * @see MysqlReadRow()
 **/
function MysqlReadRowByID($TableName, $ID, $TryOnly = false) {
	$ID = addslashes($ID);
	return MysqlReadRow("SELECT * FROM `$TableName` WHERE (`id` = '$ID');", $TryOnly);
}

function MysqlReadFieldByID($TableName, $FieldName, $ID, $TryOnly = false) {
	$i = addslashes($ID);
	$f = addslashes($FieldName);
	return MysqlReadField("SELECT `$f` FROM $TableName WHERE (`id` = '$i');", $FieldName, $TryOnly);
}

/**
 * Die Funktion MysqlWriteByID() speichert einen Datensatz in einer mit $TableName angegebenen Tabelle. 
 * Die zu speichernen Daten enth&auml;lt das assotiative Array $Values. Der zu &auml;nderne Datensatz wird mit $ID angegeben.
 * Wird $ID nicht angegeben wird ein neuer Datensatz erstellt.
 * Achtung: Diese Funktion kann nur auf Tabellen angewand werden, welche eine Spalte namens `id` besitzen!
 * 
 * @param string $TableName Der Name der Tabelle in welcher die Daten gepeichert werden soll.
 * @param array $Values Die zu speichernden Werte im Format: <code>Array("column1name" => "value1", "column2name" => "value2", ...) </code>
 * @param integer $ID Die ID des zu &auml;ndernen Datensatzes oder Null wenn ein neuer Datensatz angelegt werden soll.
 * @param string $ErrOnFail Eine Fehlermeldung welche ausgegeben wird wenn der Schreibbefehl 
 * @param bool $WriteID Gibt an, ob $Values[id] auch in die Datenbank geschrieben werden soll.
 * nicht fehlerfrei ausgef&uuml;hrt werden konnte
 * @return integer Die ID des ge&auml;nderten/eingef&uuml;gten Datensatzes bei Erfolg, ansonsten FALSE
 **/
function MysqlWriteByID($TableName, $Values, $ID = 0, $ErrOnFail = "", $WriteID = false) {
	global $MysqlConnection;
	if (!is_array($Values) or (count($Values) < 1)) {
		trigger_error_text("Konnte nicht in die Datenbank schreiben, da keine Werte &uuml;bergeben wurden.|Table:$TableName Der Parameter \$Values der Funktion MysqlWriteByID darf nicht leer sein.", E_USER_WARNING, __FILE__, __LINE__);
		return false;
	}
	
	if (($WriteID == false) && isset($Values["id"])) {
		unset ($Values["id"]);
	}
	
	foreach ($Values as $col => $val) {
		$Values[$col] = "`$col` = " .escape_sqlData($val);
	}
	
	$Values = "`$TableName` SET " . implode(", ", $Values);
	if (empty ($ID)) {
		if (empty ($ErrOnFail))
			$ErrOnFail = "Beim erstellen eines neuen Datensatzes ist ein Fehler aufgetreten.";
		if (MysqlExecQuery("INSERT into $Values;", $ErrOnFail))
			return mysqli_insert_id($MysqlConnection);
	} else {
		if (empty ($ErrOnFail))
			$ErrOnFail = "Beim speichern eines Datensatzes ist ein Fehler aufgetreten.";
		if (!MysqlExecQuery("UPDATE $Values WHERE (`id` = '" . addslashes($ID) . "');", $ErrOnFail))
			return false;
		return $ID;
		//    else trigger_error_text("Der zu &auml;nderne Datensatz existiert nicht oder mehrfach. | Table: $TableName ID: $ID",E_USER_WARNING, __FILE__, __LINE__);
	}
	return false;
}

/**
 * Die Funktion MysqlDeleteByID() l&ouml;scht einen oder mehrere Datens&auml;tze anhand ihrer ID
 * @param string $TableName Der Name der Tabelle in der sich der zu l&ouml;schende Datensatz befindet.
 * @param int $ID Die ID des zu l&ouml;schenden Datensatzes
 * @return bool Wird kein Datensatz gel&ouml;scht, wird false zur&uuml;ckgegeben.
 **/
function MysqlDeleteByID($TableName, $ID) {
	return MysqlDeleteRow("DELETE FROM `$TableName` WHERE (`id` = '" . addslashes($ID) . "');");
}

function _MysqlGetRightWhere($ID, $RightCols) {
	global $User;
	if (!is_array($RightCols))
		$RightCols = array (
			$RightCols
		);
	$where = array (
		"(`id` = '" . addslashes($ID
	) . "')");
	foreach ($RightCols as $k => $r)
		$where[] = $User->sqlHasRight($r);
	return implode(" AND ", $where);
}

function MysqlReadRowByRight($TableName, $ID, $RightCols) {
	return MysqlReadRow("SELECT * FROM `$TableName` WHERE (" . _MysqlGetRightWhere($ID, $RightCols) . ");");
}

function MysqlDeleteByRight($TableName, $ID, $RightCols) {
	return MysqlDeleteRow("DELETE FROM `$TableName` WHERE (" . _MysqlGetRightWhere($ID, $RightCols) . ");");
}

function MysqlWriteByRight($TableName, $Values, $RightCols, $ID = 0, $ErrOnFail = "", $WriteID = false) {
	global $User;
	$id = addslashes($ID);
	if (!is_array($RightCols))
		$RightCols = array (
			$RightCols
		);

	// rechte auslesen
	if (!empty ($ID)) {
		$r = array ();
		//foreach ($RightCols as $v)
		//	$r[] = "`" . escape_sqlData_without_quotes($MysqlConnection, $v) . "`";
		
		//print_r($RightCols);
		foreach ($RightCols as $v) {
			//echo "'$v'-".escape_sqlData_without_quotes($v).";";
			$r[] = "`" . escape_sqlData_without_quotes($v) . "`"; 
		}
		//print_r($r); 
		
		$r = MysqlReadRow("SELECT " . implode(",", $r) . " FROM `$TableName` WHERE (`id` = '$id');");
		if (!is_array($r)) {
			trigger_error_text("Beim &uuml;berpr&uuml;fen der Rechte f&uuml;r das Schreiben eines Datensatzes ist ein Fehler aufgetreten.|Table:$TableName ID:$ID RightCols:" . implode(",", $RightCols), E_USER_WARNING, __FILE__, __LINE__);
			return false;
		}
		foreach ($RightCols as $k)
			$User->requireRight($r[$k]);
	}

	// zu schreibende rechte pr&uuml;fen
	foreach ($RightCols as $k) {
		if (isset ($Values[$k]))
			$User->requireRight($Values[$k]);
	}

	// daten schreiben
	return MysqlWriteByID($TableName, $Values, $ID, $ErrOnFail, $WriteID);
}

function MysqlGetMaxAllowedPacket() {
	return MysqlReadField("SHOW VARIABLES LIKE 'max_allowed_packet';", 1);
}

function toTimestamp($date) {
	$date = (string) $date;

	if (strlen($date) < 12)
		return $date;

	if (strtotime($date) != -1) {
		$r = strtotime($date);
	} else {
		$r = mktime(substr($date, 8, 2), substr($date, 10, 2), substr($date, 12, 2), substr($date, 4, 2), substr($date, 6, 2), substr($date, 0, 4));
	}

	return $r; //mktime($hour,$min,$sec,$month,$day,$year);
}

function TblPrefix() {
	global $CoreConfig;
	return $CoreConfig["table_prefix"];
}

/**
 * Gibt ein Array mit den m&ouml;glichen Werten f&uuml;r eine Spalte (Enum oder Set)
 * 
 * @since 1348 - 26.01.2007
 * @param String $table Tabellenname
 * @param String $col Spalte
 * @return Array
 */
function MysqlColEnum($table, $column) {
	static $tables = array ();
	if (!isset ($tables[$table])) {
		$tables[$table] = array ();
		foreach (MysqlReadArea("DESCRIBE `$table`") AS $col) {
			$t = strtolower(substr($col["Type"], 0, 4));
			if ($t == "enum" || $t == "set(") {
				preg_match("/[^(]\((.*)\)$/", $col["Type"], $data);
				$data = explode(",", $data[1]);
				foreach ($data AS $key => $val) {
					// umgebende Hochkommata entfernen
					if ($val[0] == "'" && $val[strlen($val) - 1] == "'") {
						$val = substr($val, 1);
						$val = substr($val, 0, strlen($val) - 1);
						$data[$key] = $val;
					}
				}
				$tables[$table][$col["Field"]] = $data;
			}
		}
	}
	return isset ($tables[$table][$column]) ? $tables[$table][$column] : null;
}

/**
 * Takes an array of single queries and tries to execute it as a transaction.
 * If the query does not succeed, a ROLLBACK is issued.
 * 
 * Please note that (of course) this works ONLY FOR InnoDB Tables!!
 * 
 * @param mixed $queryRows The array of queries
 * @return TRUE on success, FALSE on failure
 */
function MysqlExecTransaction($queryRows = array()) {
	if(!is_array($queryRows)) {
		trigger_error_text('Parameterfehler!|MysqlExecTransaction(): Parameter ist kein Array!', E_USER_ERROR);
		return false;
	}
	
	// Prepare the environment - disable autocommitting
	$disable_autocommit = MysqlExecQuery('SET autocommit=0');
	
	if($disable_autocommit == false) {
		trigger_error_text('Konnte AutoCommiting nicht abschalten!', E_USER_ERROR);
		return false;
	}
	
	// Start the transaction and exec the commands one by one
	$start_trans = MysqlExecQuery('START TRANSACTION');
	
	if($start_trans == false) {
		trigger_error_text('Konnte die MySQL Transaktion nicht starten!', E_USER_ERROR);
		return false;
	}
	
	foreach($queryRows as $query) {
		$safeQuery = escape_sqlData($query);
		
		$result = MysqlExecQuery($query);
		
		if($result == false) {
			trigger_error_text('Eine Transaktion schlug fehl!', E_USER_ERROR);
			MysqlExecQuery('ROLLBACK');
			
			return false;
		}
	}
	
	$commit_trans = MysqlExecQuery('COMMIT');
	
	if($commit_trans == false) {
		trigger_error_text('Konnte die MySQL Transaktion nicht beenden!', E_USER_ERROR);
		MysqlExecQuery('ROLLBACK');
		
		return false;
	}
	
	return true;
}

/**
 * Starts a transaction in the database.
 * This command also disables autocommitting.
 * 
 * @return TRUE on success, FALSE if the transaction could not be started.
 */
function MysqlTransactionStart() {
	global $MySQLTransactionDepth;
	
	if($MySQLTransactionDepth > 0) {
		$MySQLTransactionDepth++;
		return true;
	}
	
	$MySQLTransactionDepth = 1;
	$noautocommit = MysqlExecQuery('SET autocommit = 0', 'Konnte das Autocommitting nicht abschalten!');
	
	if($noautocommit == false) {
		return false;
	}
	
	return MysqlExecQuery('START TRANSACTION', 'Konnte die Transaktion nicht starten!');
}

/**
 * Commits the transaction
 */
function MysqlTransactionCommit() {
	global $MySQLTransactionDepth;

	$MySQLTransactionDepth--;
	
	if($MySQLTransactionDepth > 0) {
		return true;
	}	
	
	$commitOK = MysqlExecQuery('COMMIT', 'Konnte die Transaktion nicht festschreiben!');
	
	if($commitOK === false) {
		trigger_error_text('Konnte die Transaktion nicht abschlie&szlig;en!', E_USER_ERROR);
		return false;
	}
	
	$autocommit = MysqlExecQuery('SET autocommit = 1', 'Konnte das Autocommitting nicht wieder einschalten!');
	
	return $autocommit;
}

/**
 * Reverts the current changes to the database
 */
function MysqlTransactionRollback() {
	global $MySQLTransactionDepth;
	$MySQLTransactionDepth = 0;
	
	return MysqlExecQuery('ROLLBACK', 'Konnte die Transaktion nicht r&uuml;ckg&auml;ngig machen!');
}

/*

function getMySQLVersion()
{
  global $MysqlConnection;
  
  $ver = mysqli_get_server_info($MysqlConnection);
  $exp = explode('.',$ver);
  return array("version" => $exp[0], "revision" => $exp[1]);
}

*/

/**
 * Die Klasse MysqlQuery kapselt eine - wie der name schon sagt - MySQL-Anfrage.
 * Sie sollte verwendet werden, wenn das Ergebnis einer Abfrage schrittweise bearbeitet 
 * werden soll.
 * 
 **/
class MysqlQuery {
	var $Result; // Die Resourcenkennung der Datenbanabfrage

	/**
	 * Der Konstruktor MysqlQuery::MysqlQuery() macht eine Datenbankabfrage 
	 * und instanziiert das Objekt.
	 * 
	 * @param string $Query Der auszuf&uuml;hrende MySQL-Befehl
	 * @param string $ErrOnFail Eine Fehlermeldung die ausgegeben wird, 
	 * wenn der Befehl nciht fehlerfrei ausgef&uuml;hrt werden kann.
	 * @return object Na, eine Instanz der Klasse MysqlQuery eben ;)
	 **/
	function MysqlQuery($Query, $ErrOnFail = "") {
		$this->Result = MysqlExecQuery($Query, $ErrOnFail);
	}

	/**
	 * zu MysqlQuery::fetchAssoc() siehe die PHP-Dokumentation von mysqli_fetch_assoc()
	 **/
	function fetchAssoc() {
		if (!$this->Result)
			return false;
		return mysqli_fetch_assoc($this->Result);
	}

	/**
	 * zu MysqlQuery::dataSeek() siehe die PHP-Dokumentation von mysqli_data_seek()
	 **/
	function dataSeek($ID) {
		if (!$this->Result)
			return false;
		return mysqli_data_seek($this->Result, $ID);
	}

	/**
	 * zu MysqlQuery::numFields() siehe die PHP-Dokumentation von mysqli_num_fields()
	 **/
	function numFields() {
		if (!$this->Result)
			return false;
		return mysqli_num_fields($this->Result);
	}

	/**
	 * zu MysqlQuery::numRows() siehe die PHP-Dokumentation von mysqli_num_rows()
	 **/
	function numRows() {
		if (!$this->Result)
			return false;
		return mysqli_num_rows($this->Result);
	}

}
?>
