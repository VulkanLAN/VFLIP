<?php


/**
 * Die Datei core.translate.php dient zur &uuml;bersetzung von Textteilen
 * innerhalb des Codes. Damit ist Flip mehrsprachenf&auml;hig.
 *
 * @author Moritz Eysholdt
 * @version $Id: core.mysql.php 659 2004-12-14 22:32:01Z docfx $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package core
 **/

/** Die Datei nur einmal includen */
if (defined("CORE.TRANSLATE.PHP"))
	return 0;
define("CORE.TRANSLATE.PHP", 1);

require_once ("core/core.mysql.php");

/**
 * Die Funktion text_translate() gibt einenText auf der Relation TblPrefix().flip_textdata
 * zur&uuml;ck. &Uuml;bergeben wird der deutsche Text. Dieser wird auch bei deutsch wieder
 * zur&uuml;ckgegeben. Bei anderen Sprechen wird versucht, diesen zu &uuml;bersetzen. Beim
 * erfolgreichen &uuml;bersetzen, wird dieser dann &uuml;bersetzt.
 * 
 * Variablen k&ouml;nnen im Text mit ?1? (n = Zahl) angegeben werden. Diese werden
 * mit den Werten in $vars ersetzt
 * 
 * //todo Cachingmechanismus auf Programmebene zum verhindern der
 * Datenbankqueries
 * @param String $gtext deutscher Text
 * @param Array $vars Werte zum ersetzen von Variablen
 * @return String &uuml;bersetzter Text (ggf. mit ersetzten Variablen)
 * 
 **/
function text_translate($gtext, $vars = array ()) {
	static $TranslateNesting = 0;
	
	if($TranslateNesting > 100) {
		trigger_error('Nested translate detected - please inform a developer! GText was: "' . $gtext. '"');
		$TranslateNesting--;
		return text_replaceVars($gtext, $vars);
	}
	
	$TranslateNesting++;
	
	if ($gtext == "") {
		return $gtext;
	}
	
	if (!is_array($vars)) {
		$vars = array($vars);
	}
	
//	return text_replaceVars($gtext, $vars);
	
	$lang = ConfigGet("translate_language");
	$text = "";
	$hash = md5($gtext);
	
	static $translations      = array();
	static $translations_hash = array();
	
	if ($lang == "de_DE") {
		$text = $gtext;
	}
	
	elseif (!isset ($translations[$lang])) {
		$translations[$lang] = MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_textdata` WHERE `locale` = " . escape_sqlData($lang), 'name');
		$translations_hash[$lang] = MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_textdata` WHERE `locale` = " . escape_sqlData($lang), 'hash');
	}
	
	if ($lang != "de_DE") {
		
		// Try reading by hash
		if(isset($translations_hash[$lang][$hash])) {
			$text = $translations_hash[$lang][$hash]['text'];
		} 
		
		// Try reading the old way, by name
		elseif(isset($translations[$lang][$gtext])) {
			$text = $translations[$lang][$gtext]['text'];
		}
		
		// If that fails, too, the text has not been translated at all!
		else {
			global $User;
			
			if (is_object($User) && $User->hasRight("translate")) {
				trigger_error('Translate: ' . "<a target=\"_blank\" href=\"text.php?frame=translate&amp;hash=$hash&amp;text=" . urlencode($gtext) . "\">" . $lang . " {" . escapeHtml($gtext) . "}</a> (" . $hash . ")");
//				$text = "<a href=\"text.php?frame=translate&amp;hash=$hash&amp;text=" . urlencode($gtext) . "\">" . $lang . " {" . $gtext . "}</a>";
			} else {
//				trigger_error('Translate: ' . $lang . "{" . $gtext . "}");
//				$text = $lang . "{" . $gtext . "}";
			}
			
			$text = $gtext;
		}
	}
	
	$TranslateNesting--;
	
	return text_replaceVars($text, $vars);
}

/**
 * @see text_translate()
 */
function text_replaceVars($text, $vars) {
	$newText = $text;
	
	if (is_array($vars)) {
		foreach (array_values($vars) as $index => $value) {
			$charidx = $index + 1; 
			$pattern = "/\\?$charidx\\?/";
			$newText = preg_replace($pattern, $value, $newText, 1);
		}
	}
	
	return $newText;
}
?>
