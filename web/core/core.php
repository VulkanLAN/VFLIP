<?php

/**
 * core.php
 * In der Datei core.php werden/wird...
 * - alle core-script included 
 * - festgelegt wo sich welche Verzeichnisse befinden
 * - einige globale Variablen initialisiert
 * 
 * @author Moritz Eysholdt
 * @version $Id: core.php 1702 2019-01-09 09:01:12Z VulkanLAN $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package core
 **/

/** Die Datei nur einmal includen */
if (defined("CORE.PHP"))
	return 0;
define("CORE.PHP", 1);

/********************************** basic config ***************************************/

/*** constants ***/
// die benötigte Datenbank-Revisionsnummer. An ihr erkennt DBUpdate seinen Einsatz.
define('REQUIRED_DB_VERSION', 64);

// Base version
define('FLIP_VERSION', '1.7');

// Full version of this system
define('FLIP_VERSION_LONG', FLIP_VERSION . "." . REQUIRED_DB_VERSION);

// The user id of the every anonymous user, e.g. those who are not logged in
define('SYSTEM_ANONYMOUS_USER_ID', 1);

// The user id of the deleted user account
// It is used when a user is deleted from the system to avoid errors when modules try to load that user 
define('SYSTEM_DELETED_USER_ID', 3);

// Die UserID des SystemUser. Er wird vom Messaging verwendet.
define('SYSTEM_USER_ID', 4);

//Zeitzone f&uuml;r PHP5
define('TIMEZONE', 'Europe/Berlin');

/*** variables (config-file) ***/
function getConfigName() {
	$prefix = "core/flipconfig";
	$dirname = strtolower(str_replace("/", "_", trim(dirname($_SERVER["SCRIPT_NAME"]), "/")));
	$srvname = $_SERVER["SERVER_NAME"];
	if (is_file("$prefix.$dirname.php"))
		return "$prefix.$dirname.php";
	if (is_file("$prefix.$srvname.php"))
		return "$prefix.$srvname.php";
	if (is_file("$prefix.$srvname-$dirname.php"))
		return "$prefix.$srvname-$dirname.php";
	return "$prefix.php";
}

$ConfigName = getConfigName();
if (!is_file($ConfigName)) {
	if (is_file('install/install.php')) {
		// ohne Config zum Installer umleiten
		header('Location: install/install.php');
		exit ();
	} else {
		// ohne Installer die default-Config benutzen
		copy("core/core.config.default.php", $ConfigName);
	}
}
if (!is_file($ConfigName))
	die("Die Datei '$ConfigName' wude nicht gefunden und konnte nicht erstellt werden.");
if (is_file('tmp/flipconfig.txt'))
	if (!unlink('tmp/flipconfig.txt'))
		die('Die Datei tmp/flipconfig.txt konnte nicht geloescht werden! Diese vom FLIP-Installer erstellte Datei enthaelt Passwoerter im Klartext! Bitte entfernen.');

/************************************ includes *****************************************/
require_once ("core/core.utils.php");
include_once ($ConfigName);
require_once ("core/core.mysql.php");
require_once ("core/core.config.php");
require_once ("core/core.translate.php");
require_once ("core/core.cache.php");
require_once ("core/core.log.php");
require_once ("core/core.subject.php");
require_once ("core/core.session.php");

/************************** initialization of global vars ******************************/

register_shutdown_function("ShutdownHook");

//timezone
if (version_compare(phpversion(), "5.1.0", ">=")) {
	ini_set("date.timezone", TIMEZONE);
}

//turn magic_quotes_gpc indirectly off
//from the note of pestilence @ http://php.net/get_magic_quotes_gpc or http://www.php.net/manual/en/function.get-magic-quotes-gpc.php
//The php-manual says:
//This method is inefficient so it's preferred to instead set the appropriate directives elsewhere.
//from: http://www.php.net/manual/en/security.magicquotes.disabling.php
function unfck($v) {
	return is_array($v) ? array_map('unfck', $v) : stripslashes($v);
}
function unfck_gpc() {
	foreach (array (
			'POST',
			'GET',
			'REQUEST',
			'COOKIE'
		) as $gpc)
		$GLOBALS["_$gpc"] = array_map('unfck', $GLOBALS["_$gpc"]);
}
if (get_magic_quotes_gpc())
	unfck_gpc();

// Register the error handler
LogRegisterErrorHook();

// Init and connect the database
$mysqlOK = MysqlConnect();

if($mysqlOK == false) {
	trigger_error('Failed to init the FLIP database connection - shutting down!<br />');
	exit(1);
}

CacheInit();

// get version
$rev_fields = explode(' ', ConfigGet('dbupdate_revision'));
define('FLIP_CURRENT_VERSION', $rev_fields[2]);

//TryDBUpdate();
InitSession();
SetGlobalUser();

function ShutdownHook() {
	SaveSession();
	MysqlShutdown();
}

function ChangeUser($u) {
	global $User, $Session;
	
	if (!is_object($User)) {
		$User = $u;
		$Session->setUserID($u->id);
    $Session->save();
	} elseif ($User->id != $u->id) {
		$User = $u;
		$Session->setUserID($u->id);
    //$Session->save(); // XXX: is save() appropriate here?
	}
}

function SetGlobalUser() {
	global $User, $Session;
	
	$u = UserTryLogin();
	if (is_object($u)) {
		ChangeUser($u);
	}
	if (!is_object($User)) {
		ChangeUser(new User("Anonymous"));
	}
	
	$Session->register("User");
}

/**
 * Triggers an update of the database by evaluating
 * the version number in dbupdate_revision.
 */
function TryDBUpdate() {
	if (isset ($_GET['disable-dbupdate'])) {
		return;
	}
	
	$rev = ConfigGet('dbupdate_revision');
	
	if (!empty ($rev)) {
		// example: $Id: core.php 1486 2007-10-17 17:27:02Z scope $
		list (,, $rev, $date) = explode(' ', $rev);
		
		// Everything older that 24.07.2010 is from v. 1.2
		// Trigger the old update system first.
		list($year, $month, $day) = explode('-', $date);
		$ts_database = mktime(0, 0, 0, $month, $day, $year);
		$ts_update   = mktime(0, 0, 0, 7, 24, 2010);
		
		// Run the old updater first
		if($ts_database < $ts_update) {
			include_once ('mod/mod.dbupdate.old.php');
			DBUpdateToRev1506();
			// The database has rev. 1506 now.
			// Set $rev to 3 to trigger updating with the new system
			$rev = 3;
			
			// Store the changes in the database
			$date = date('Y-m-d H:i:s\Z');
			ConfigSet('dbupdate_revision', '$Id: flip.sql $rev $date dbupdate $');
		}
		
		// Now run the new updater
		if ($rev < REQUIRED_DB_VERSION) {
			include_once ('mod/mod.dbupdate.php');
			DBUpdateDoUpdate();
		}
		
	} else {
		trigger_error(
			text_translate(
				"Die Datenbank ist zu alt um automatisch auf den aktuellsten Stand gebracht zu werden. " .
				"Sie muss manuell bis auf Version <b>573</b> gebracht werden, danach kann das DBUpdate greifen. " .
				"Einen Dump von Version 573 findest du unter http://www.flipdev.org/download/flip-573.sql."
			), 
			E_USER_WARNING
		);
	}
}

//install script
$devMode = false;

if (is_file('install/install.php') && !$devMode) {
	trigger_error('Das Installationsskript "install/install.php" existiert noch. Es w&auml;re m&ouml;glich die Konfiguration zu &auml;ndern!', E_USER_WARNING);
}
?>
