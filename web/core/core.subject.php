<?php


/**
 * @author Moritz Eysholdt
 * @version $Id: core.subject.php 1702 2019-01-09 09:01:12Z scope $ edit by VulkanLAN
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package core
 **/

/** Die Datei nur einmal includen */
if (defined("CORE.SUBJECT.PHP"))
	return 0;
define("CORE.SUBJECT.PHP", 1);

require_once ("core/core.mysql.php");

/**
 * Gibt Informationen &uuml;ber einen oder alle Subjekt-Typen zur&uuml;ck.
 * Informationen &uuml;ber einen Subject-Typ sind wiederum als Eigenschaften eines Subjectes gespeichert.
 * (Das bedeutet, der Subject-Typ "type" definiert sich selbst). GetTypeInfo() cached das R&uuml;ckgabeergebnis
 * und kann daher ohne Performanceeinbussen beliebig oft aufgerufen werden.
 * @param string $Type (optional) Der Name des Types zu dem Informationen abgefragt werden sollen.
 * @return array wird $Type angegeben, ein assoziatives Array im Sinne von:
 * <code>
 * array (
 *  'id' => '1294',
 *  'name' => 'forum',
 *  'rights_over_for_properties' => 'Y',
 *  'can_be_child' => 'Y',
 *  'can_be_parent' => 'N',
 * );
 * </code>
 * ansonsten, wenn $Type nicht angegeben wird:
 * <code>
 * array (
 *   'user' => 
 *   array (
 *     'id' => '1294',
 *     'name' => 'forum',
 *     'rights_over_for_properties' => 'Y',
 *     'can_be_child' => 'Y',
 *     'can_be_parent' => 'N',
 *   ),
 *   'group' => ...,
 *   ...
 * );
 *</code>
 **/
function GetTypeInfo($Type = "") {
	global $DisableSubjectCaching;
	static $cache;
	if (!is_array($cache) or $DisableSubjectCaching) {
		$cache = CacheGet("subject_typeinfo");
		if (!is_array($cache)) {
			$cache = array ();
				foreach (GetSubjects("type", array (), "", "name") as $v)
				$cache[$v["name"]] = $v;
			CacheSet("subject_typeinfo", $cache, array (
				"flip_user_data",
				"flip_user_column"
			));
		}
	}
	return (empty ($Type)) ? $cache : $cache[$Type];
}

/**
 * Gibt ein assoziatives Array zur&uuml;ck, welches den Rechte-IDs die Rechte-Namen zuordnet.
 * Dabei wird das R&uuml;ckgabeergebnis gecached, so dass ein h&auml;ufiger Aufruf von GetRights() kein 
 * Performanceproblem darstellt.
 * @return array Ein assoziatives Array, welches die ID als Key und den Namen der Rechte als Value enth&auml;lt.
 **/
function GetRights($ForceReload = false) {
	global $DisableSubjectCaching;
	static $rights;
	if (!$ForceReload or $DisableSubjectCaching) {
		if (is_array($rights))
			return $rights;
		if ($rights = CacheGet("subject_rights"))
			return $rights;
	}
	$rights = MysqlReadCol("SELECT `id`,`right` FROM `" . TblPrefix() . "flip_user_right`;", "right", "id");
	return CacheSet("subject_rights", $rights, array (
		"flip_user_right"
	));
}

/**
 * Liefert den Namen eines Rechtes zur&uuml;ck.
 * Wenn das angefragte Recht nicht existiert, wird eine Warnmeldung ausgegeben.
 * @param string $Ident entweder die ID oder der Name des Rechtes.
 * @return string Der Name des Rechtes.
 **/
function GetRightName($Ident) {
	$r = GetRights();
	if (isset ($r[$Ident]))
		return $r[$Ident];
	if (in_array($Ident, $r))
		return $Ident;
	trigger_error_text(text_translate("Das Recht \"?1?\" existiert nicht.", $Ident), E_USER_WARNING);
	return false;
}

/**
 * Liefert die ID eines Rechtes zur&uuml;ck.
 * Wenn das angefragte Recht nicht existiert, wird eine Warnmeldung ausgegeben.
 * @param string $Ident entweder die ID oder der Name des Rechtes.
 * @return int die ID des Rechtes.
 **/
function GetRightID($Ident) {
	$n = array_flip($r = GetRights());
	if (isset ($n[$Ident]))
		return $n[$Ident];
	if (isset ($r[$Ident]))
		return $Ident;
	trigger_error_text(text_translate("Das Recht \"?1?\" existiert nicht.", $Ident), E_USER_WARNING);
	return false;
}

function GetRightOwners($aRightIdent, $controlled = 0) {
	$right = GetRightID($aRightIdent);
	$controlled = (int) $controlled;
	$r = MysqlReadArea("
		    SELECT s.* FROM (" . TblPrefix() . "flip_user_rights r)
		      LEFT JOIN " . TblPrefix() . "flip_user_subject s ON (r.owner_id = s.id)
		      WHERE ((r.right_id = '$right') AND (r.controled_id = $controlled));
		  ", "id");
	return _AddChildIDS($r);
}

/**
 * Gibt die ID von einem Subject zur&uuml;ck.
 * Zus&auml;tzlich wird das R&uuml;ckgabeergebnis w&auml;hren der Programmausf&uuml;hrung gecached.
 * Es wird nicht &uuml;berpr&uuml;ft, ob die zur&uuml;ckgegebene Subject-ID g&uuml;ltig ist.
 * @param mixed $Subject Irgend etwas, dass sich als Subject interpretieren l&auml;sst:
 * - Die Instanz einer Klasse vom Typ Subject oder deren Nachfahren.
 * - Ein Array, welches den Key "id" enth&auml;lt. (z.B. ein Datensatz aus der Tabelle TblPrefix().flip_user_subject)
 * - Eine Zahl.
 * - Ein String welcher ein Subject-Name oder die Email-Adrese eines Subjectes ist.
 * @param boolean $DoLookupIfNecc Um die ID zu einem Namen oder einer EMail-Adresse ausfindig zu machen,
 * ist eine Datenbankabfrage notwendig. $DoLookupIfNecc gibt an, ob diese gestattet ist.
 * @return int Wenn die Funktion erfolgreich war, wird die ID des Subjectes zur&uuml;ckgegeben, ansonsten der Wert 0.
 **/
function GetSubjectID($Subject, $DoLookupIfNecc = true) {
	global $DisableSubjectCaching;
	static $cache;
	if (is_object($Subject))
		return $Subject->id;
	if (is_array($Subject))
		return $Subject["id"];
	if (preg_match("/^\d+$/", $Subject))
		return $Subject;
	if (empty ($Subject))
		return 0;

	if (!$DisableSubjectCaching and isset ($cache[$Subject]))
		return $cache[$Subject];
	if ($DoLookupIfNecc) {
		$q = addslashes($Subject);
		$type = (IsValidEmail($q)) ? "email" : "name";
		$id = MysqlReadField("SELECT `id` FROM `" . TblPrefix() . "flip_user_subject` WHERE (`$type` = '$q');", 0, true); //FIXME es kann mehrere Typen mit dem selben namen geben (z.B. user und gruppe)!
		if (!empty ($id))
			return $cache[$Subject] = $id;
	}
	return 0;
}

function GetSubjectName($Subject, $DoLookupIfNecc = true) {
	if (empty ($Subject))
		return "";
	if (is_object($Subject))
		return $Subject->name;
	if (is_array($Subject))
		return $Subject["name"];
	if ($DoLookupIfNecc) {
		if (is_posDigit($Subject)) {
			return MysqlReadField("SELECT `name` FROM `" . TblPrefix() . "flip_user_subject` WHERE (`id` = '$Subject');", "name", true);
		}
		$q = addslashes($Subject);
		return MysqlReadField("SELECT `name` FROM `" . TblPrefix() . "flip_user_subject` WHERE ((`name` = '$q') OR (`email` = '$q'))", "name", true);
	}
	return 0;
}

/**
 * Verkn&uuml;pft ein Child mit einem Parent.
 * @param mixed $aChild
 * @param mixed $aParent
 * @return int bei erfolg, Die ID der Verkn&uuml;pfung, ansonsten false.
 * @access private
 **/
function _AddToGroup($aChild, $aParent) {
	$r = false;
	$c = GetSubjectID($aChild);
	$p = GetSubjectID($aParent);
	if ($c and $p)
		$r = MysqlWriteByID(TblPrefix() . "flip_user_groups", array (
			"child_id" => $c,
			"parent_id" => $p
		));
	if (!$r)
		trigger_error_text(text_translate("Ein Parent konnte nicht zu einem Child hinzugef&uuml;gt werden.") .
		"|Child: [$c]$aChild Parent: [$p]$aParent", E_USER_WARNING);
	else
		SessionAddDoNext("mod/mod.user.php|UserRefreshRights", $c);
	return $r;
}

/**
 * L&ouml;scht die Verkn&uuml;pfung zwischen einem Child und einem Parent
 * @param mixed $aChild
 * @param mixed $aParent
 * @return bool bei erfolg true ansonsten false.
 * @access private
 **/
function _RemFromGroup($aChild, $aParent) {
	$r = false;
	$c = GetSubjectID($aChild);
	$p = GetSubjectID($aParent);
	if (is_posDigit($c) && is_posDigit($p))
		$r = MysqlWrite("DELETE FROM `" . TblPrefix() . "flip_user_groups` WHERE ((`child_id` = $c) AND (`parent_id` = $p));");
	if (!$r)
		trigger_error_text(text_translate("Ein Parent konnte nicht von einem Child entfernt werden.") .
		"|Child: [$c]$aChild Parent: [$p]$aParent", E_USER_WARNING);
	else
		SessionAddDoNext("mod/mod.user.php|UserRefreshRights", $c);
	return $r;
}

/**
 * F&uuml;gt ein Recht zu einem Subject hinzu.
 * @param mixed $aOwner Das Subject, welches das Recht erhalten soll.
 * @param mixed $aRight Die ID oder der Name des Rechtes.
 * @param mixed $aControled (optional) Ein Subject, &uuml;ber welches das Recht ausge&uuml;bt werden soll.
 * @return int bei erfolg, Die ID der Verkn&uuml;pfung, ansonsten false.
 * @access private
 **/
function _AddRight($aOwner, $aRight, $aControled = 0) {
	$s = false;
	$o = GetSubjectID($aOwner);
	$r = GetRightID($aRight);
	$c = (empty ($aControled)) ? 0 : GetSubjectID($aControled);
	if ($o and $r)
		$s = MysqlWriteByID(TblPrefix() . "flip_user_rights", array (
			"owner_id" => $o,
			"controled_id" => $c,
			"right_id" => $r
		));
	if (!$s)
		trigger_error_text(text_translate("Ein Recht konnte nicht hinzugef&uuml;gt werden.") .
		"|Owner: [$o]$aOwner Controled: [$c]$aControled Right: [$r]$aRight", E_USER_WARNING);
	else
		SessionAddDoNext("mod/mod.user.php|UserRefreshRights", $o);
	return $s;
}

/**
 * Entfernt ein Recht von einem Subject.
 * @param mixed $aOwner der Besitzer des Subjectes.
 * @param mixed $aRight die ID oder der Name des zu entfernenden Rechtes.
 * @param mixed $aControled Ein Subject, &uuml;ber welches das Recht ausge&uuml;bt wurde.
 * @return boolean bei erfolg true, ansonsten false
 * @access private
 **/
function _RemRight($aOwner, $aRight, $aControled = 0) {
	$s = false;
	$o = GetSubjectID($aOwner);
	$r = GetRightID($aRight);
	$c = (empty ($aControled)) ? 0 : GetSubjectID($aControled);
	if ($o and $r)
		$s = MysqlWrite("DELETE FROM `" . TblPrefix() . "flip_user_rights` WHERE ((`owner_id` = $o) AND (`controled_id` = $c) AND (`right_id` = $r));");
	if (!$s)
		trigger_error_text(text_translate("Ein Recht konnte nicht entfernt werden.") .
		"|Owner: [$o]$aOwner Controled: [$c]$aControled Right: [$r]$aRight", E_USER_WARNING);
	else
		SessionAddDoNext("mod/mod.user.php|UserRefreshRights", $o);
	return false;
}

/**
 * Gibt ein Array mit den IDs aller Parents zur&uuml;ck.
 * @param mixed $Child
 * @return array Ein Array im Format <code> array($ParentID => $ParentName) </code>
 * @access private
 **/
function _GetInheritedIDs($Child) {
	global $DisableSubjectCaching;
	static $cache;
	$ChildID = GetSubjectID($Child);
	if (!$DisableSubjectCaching and isset ($cache[$ChildID]))
		return $cache[$ChildID];
	$r = array ();
	$left = array (
		$ChildID
	);
	do {
		$q = MysqlReadArea("
				      SELECT g1.parent_id AS p, s.name AS n, count(g2.parent_id) AS c
				        FROM (`" . TblPrefix() . "flip_user_groups` g1, `" . TblPrefix() . "flip_user_subject` s)
				        LEFT JOIN `" . TblPrefix() . "flip_user_groups` g2 ON (g1.parent_id = g2.child_id)
				        WHERE ((g1.child_id IN (" . implode_sqlIn($left) . ")) AND (g1.parent_id = s.id))
				        GROUP BY g1.parent_id;
				    ", "p");
		$left = array ();
		foreach ($q as $gid => $a) {
			if (!isset ($r[$gid]) and ($a["c"] != 0))
				$left[] = $gid;
			$r[$gid] = $a["n"];
		}
	} while (count($left) > 0);
	return $cache[$ChildID] = $r;
}

function _AddChildIDS($Parents) {
	$result = $Parents;
	$query = array_keys($Parents);
	do {
		$r = MysqlReadArea("
				      SELECT s.*, COUNT(g2.id) AS `child_count` 
				        FROM (`" . TblPrefix() . "flip_user_groups` g1)
				        LEFT JOIN `" . TblPrefix() . "flip_user_subject` s ON (g1.child_id = s.id)
				        LEFT JOIN `" . TblPrefix() . "flip_user_groups` g2 ON (g2.parent_id = g1.child_id)
				        WHERE(g1.parent_id IN (" . implode_sqlIn($query) . "))
				        GROUP BY g1.child_id;
				    ", "id");
		$query = array ();
		foreach ($r as $id => $v) {
			if (!isset ($result[$id]) and ($v["child_count"] != 0))
				$query[] = $id;
			$result[$id] = $v;
		}
	} while (!empty ($query));
	return $result;
}

function _GetColumnInfo($Type, $FlushCache = false) {
	global $DisableSubjectCaching;
	static $cache;
	if ($FlushCache or $DisableSubjectCaching)
		$cache = false;
	if (isset ($cache[$Type]) && is_array($cache[$Type]))
		return $cache[$Type];
	$t = addslashes($Type);
	if (is_array($info = CacheGet("subject_columninfo_$Type")))
		return $cache[$Type] = $info;
	$newcols = (defined('FLIP_CURRENT_VERSION') && FLIP_CURRENT_VERSION >= 1373) ? ' u.min_length, u.max_length,' : '';
	$info = MysqlReadArea("
		    SELECT u.name, u.caption, u.callback_read, u.callback_write, u.access, u.id,$newcols c.value AS `default_val`, u.val_type
		      FROM (`" . TblPrefix() . "flip_user_column` u)
		      LEFT JOIN `" . TblPrefix() . "flip_config` c ON (c.key = u.name)
		      WHERE (u.type = '$t') ORDER BY u.sort_index;", "name");
	CacheSet("subject_columninfo_$Type", $info, array (
		"flip_user_column",
		"flip_config"
	));
	return $cache[$Type] = $info;
}

function GetColumns($Type) {
	$a = array ();
	foreach (_GetColumnInfo($Type) as $name => $dat)
		$a[$name] = $dat["caption"];
	return $a;
}

function GetColumnDeteils($Type) {
	return MysqlReadArea("
		    SELECT u.*, c.value AS `default_val` 
		      FROM (`" . TblPrefix() . "flip_user_column` u)
		      LEFT JOIN `" . TblPrefix() . "flip_config` c ON (c.key = u.name)
		      WHERE (u.type = '" . addslashes($Type) . "') ORDER BY u.sort_index;
		  ", "name");
}

function CheckSubjectName($aName, $raiseError = true) {
	$l = ConfigGet("subject_min_namelen");

	if (is_numeric($aName))
		$err = "Er darf nicht nur aus Ziffern bestehen.";
	elseif (strlen($aName) < $l) $err = text_translate("Er muss mindestens ?1? Zeichen lang sein.", $l);
	elseif (IsValidEmail($aName)) $err = text_translate("Er darf keine g&uuml;ltige Email-Adresse sein.");

	if (!isset ($err))
		return true;
	if ($raiseError)
		trigger_error_text(text_translate("Der Subjekt-Name \"?1?\" ist ung&uuml;ltig.", $aName) . " $err", E_USER_WARNING);
	return false;
}

function CreateSubject($Type, $Name, $Email = NULL) {
	if (!CheckSubjectName($Name))
		return false;
	if (!is_null($Email) and !IsValidEmail($Email)) {
		trigger_error_text(text_translate("Die Email-Adresse \"?1?\" ist ung&uuml;ltig.", $Email), E_USER_WARNING);
		return false;
	}
	$a = array (
		"type" => $Type,
		"name" => $Name,
		"email" => $Email
	);
	if (!($a["id"] = MysqlWriteByID(TblPrefix() . "flip_user_subject", $a, 0, text_translate("Ein Subject konnte nicht erstellt werden.") . "|Type:$Type Name:$Name")))
		return false;
	return CreateSubjectInstance($a);
}

/**
 * F&uuml;gt Subject-Properties zu einem Array, das bereits Subject-IDs enth&auml;lt, hinzu.
 */
function AddSubjectProperties($Type, $List, $Col, $Columns) {
	if (empty ($List))
		return array ();
	$ids = array ();
	foreach ($List as $l)
		$ids[] = $l[$Col];
	array_unshift($Columns, "id");
	$dat = GetSubjects($Type, $Columns, "s.id IN (" . implode_sqlIn($ids) . ")");
	foreach ($List as $k => $subj)
		foreach ($Columns as $n => $c) {
			$key = (is_numeric($n)) ? $c : $n;
			$List[$k][$key] = $dat[$subj[$Col]][$c];
		}
	return $List;
}

function GetSubjects($Type, $Columns = array (), $Where = "", $OrderBy = "") {
	$vals = $joins = array ();
	$col = _GetColumnInfo($Type);
	if (empty ($Columns)) {
		$Columns = array_keys($col);
		$key = "id";
	} else
		$key = $Columns[0];
	$callbacks = array ();
	$Columns = array_unique(array_merge($Columns, array (
		"id",
		"email",
		"name",
		"type"
	))); // wird gebraucht, um Subject-Object ohne weiteren Datenbankaufruf erstellen zu k&ouml;nnen.
	$required_fields = array (
		"name",
		"type",
		"email",
		"id"
	);
	foreach ($required_fields AS $f) {
		if (!isset ($col[$f]))
			$col[$f] = array (
				"access" => "subject"
			);
	}

	foreach ($Columns as $c) {
		if (isset ($col[$c])) {
			$cid = isset ($col[$c]["id"]) ? $col[$c]["id"] : null;
			$def = (empty ($col[$c]["default_val"])) ? NULL : addslashes($col[$c]["default_val"]);
			switch ($col[$c]["access"]) {
				case ("subject") :
					if (is_null($def))
						$vals[] = "s.$c";
					else
						$vals[] = "IFNULL(s.$c,'$def') AS `$c`";
					break;
				case ("data") :
					if (is_null($def))
						$vals[] = "t$cid.val AS `$c`";
					else
						$vals[] = "IFNULL(t$cid.val,'$def') AS `$c`";
					$joins[] = "LEFT JOIN `" . TblPrefix() . "flip_user_data` t$cid ON ((t$cid.column_id = '$cid') AND (t$cid.subject_id = s.id))";
					break;
				case ("binary") :
					// as text
					$valdata = "CONCAT('user.php?frame=getdata&sid=', b$cid.subject_id, '&cid=', b$cid.column_id )";
					// as binary data
					// $valdata = "b$cid.data";
					if (is_null($def))
						$vals[] = "$valdata AS `$c`";
					else
						$vals[] = "IFNULL($valdata,'$def') AS `$c`";
					$joins[] = "LEFT JOIN `" . TblPrefix() . "flip_user_binary` b$cid ON ((b$cid.column_id = '$cid') AND (b$cid.subject_id = s.id))";
					break;
				case ("callback") :
					$callbacks[$c] = $col[$c]["callback_read"];
					break;
				default :
					trigger_error_text(text_translate("Die Werte der Spalte mit dem Namen \"?1?\" k&ouml;nnen nicht zur&uuml;ckgegeben werden, da der access-typ nicht unterst&uuml;tzt wird.", $c) . "|" . $col[$c]["access"], E_USER_WARNING);
			}
		} else
			trigger_error_text(text_translate("Die Spalte mit dem Namen \"?1?\" existiert nicht.", $c) . "|type:$Type", E_USER_WARNING);
	}

	if (!empty ($Where))
		$Where = " AND ($Where)";

	$ob = "";
	if (isset ($col[$OrderBy]["access"])) {
		switch ($col[$OrderBy]["access"]) {
			case "subject" :
				$ob = "ORDER BY s.$OrderBy";
				break;
			case "data" :
				$ob = "ORDER BY t" . $col[$OrderBy]['id'] . ".val";
				break;
			case "binary" :
				$ob = "ORDER BY b" . $col[$OrderBy]['id'] . ".data";
				break;
		}
	}

	$r = MysqlReadArea("
		    SELECT " . implode(", ", $vals) . "
		      FROM (`" . TblPrefix() . "flip_user_subject` s)
		      " . implode("\n", $joins) . "
		      WHERE (s.type = '" . addslashes($Type) . "')$Where
		      $ob;
		  ", $key);

	foreach ($callbacks as $col => $func)
		foreach ($r as $k => $v)
			$r[$k][$col] = ExecCallback($func, array (
				CreateSubjectInstance($v
			)));

	return $r;
}

/**
 * Deletes a subject from the system, deleting or anonymizing all data
 * that was created by the subject.
 * 
 * @param mixed $aSubject The subject to delete
 */
function DeleteSubject(& $aSubject) {
	$s = CreateSubjectInstance($aSubject);
	
	// System accounts cannot be deleted
	if($s->system == 1) {
		trigger_error_text('Das System-Subjekt "' . $s->name . '" kann nicht gel&ouml;scht werden!', E_USER_ERROR);
		return false;	
	}
	
	$s->delete();
	
	// Calendar - Change UID & delete signups
	MysqlWrite(
		'UPDATE `' . TblPrefix() . 'flip_calender_events` ' . 
		'SET `last_change_user_id` = '. SYSTEM_DELETED_USER_ID . 
		' WHERE `last_change_user_id` = ' . $s->id
	);
	
	MysqlWrite(
		'DELETE FROM `' . TblPrefix() . 'flip_calender_signups` ' . 
		'WHERE `user_id` = ' . $s->id
	);
	
	// Catering - Delete orders
	MysqlWrite(
		'DELETE FROM `' . TblPrefix() . 'flip_catering_bestellungen` ' . 
		'WHERE `subject_id` = ' . $s->id
	);
	
	// Clans - Delete join requests
	MysqlWrite(
		'DELETE FROM `' . TblPrefix() . 'flip_clan_join` ' . 
		'WHERE `user_id` = ' . $s->id
	);
	
	// Content - Change user id
	MysqlWrite(
		'UPDATE `' . TblPrefix() . 'flip_content_image` ' .
		'SET `edit_user_id` = ' . SYSTEM_DELETED_USER_ID .
		' WHERE `edit_user_id` = ' . $s->id
	);
	
	MysqlWrite(
		'UPDATE `' . TblPrefix() . 'flip_content_text` ' .
		'SET `edit_user_id` = ' . SYSTEM_DELETED_USER_ID .
		' WHERE `edit_user_id` = ' . $s->id
	);
	
	// Featuresys - Delete accounts
	MysqlWrite(
		'DELETE FROM `' . TblPrefix() . 'flip_featuresys_accounts` ' . 
		'WHERE `user_id` = ' . $s->id
	);
	
	// Forum - replace the owner of posts by the deleted user id
	MysqlWrite(
		'UPDATE `'. TblPrefix() .'flip_forum_threads` ' . 
		'SET poster_id = ' . SYSTEM_DELETED_USER_ID . 
		' WHERE poster_id = ' . $s->id
	);
	
	MysqlWrite(
		'UPDATE `'. TblPrefix() .'flip_forum_posts` ' . 
		'SET poster_id = ' . SYSTEM_DELETED_USER_ID . 
		' WHERE poster_id = ' . $s->id
	);
	
	MysqlWrite(
		'UPDATE `'. TblPrefix() .'flip_forum_posts` ' .
		'SET changer_id = '. SYSTEM_DELETED_USER_ID . 
		' WHERE changer_id = ' . $s->id
	);
	
	MysqlWrite(
		'DELETE FROM `'. TblPrefix() .'flip_forum_watches` ' . 
		'WHERE owner_id = ' . $s->id
	);
	
	// Gallery - Change user ids?
	// TODO: Implement, pls :)
	
	// Internet request system - Delete requests
	MysqlWrite(
		'DELETE FROM `'. TblPrefix() .'flip_inet_requests` ' . 
		'WHERE `user_id` = ' . $s->id
	);
	
	// Last visits - Delete records
	MysqlWrite(
		'DELETE FROM `'. TblPrefix() .'flip_lastvisit` ' . 
		'WHERE `user_id` = ' . $s->id
	);
	
	// Log - Delete messages which reference the deleted users
	MysqlWrite(
		'DELETE FROM `'. TblPrefix() .'flip_log_log` ' . 
		'WHERE `user_id` = ' . $s->id
	);
	
	// Netlog - Same as Log
	MysqlWrite(
		'DELETE FROM `'. TblPrefix() .'flip_netlog_log` ' . 
		'WHERE `user_id` = ' . $s->id
	);

	// News - Change ids
	MysqlWrite(
		'UPDATE `'. TblPrefix() .'flip_news_comments` ' . 
		'SET `user_id` = ' . SYSTEM_DELETED_USER_ID . 
		' WHERE `user_id` = ' . $s->id
	);
	
	// Poll - Change uID
	MysqlWrite(
		'UPDATE `'. TblPrefix() .'flip_poll_users` ' . 
		'SET `user_id` = ' . SYSTEM_DELETED_USER_ID . 
		' WHERE `user_id` = ' . $s->id
	);	
	
	// Seats - Free all seats and update the plan(s)
	MysqlWrite(
		'UPDATE `'. TblPrefix() .'flip_seats_seats` ' . 
		'SET `user_id` = NULL, `user_status` = \'\', `reserved` = \'N\' ' . 
		'WHERE `user_id` = ' . $s->id
	);
	
	include_once 'mod/mod.seats.php';
	RedrawSeats();
	
	// Sessions - Delete session data
	MysqlWrite(
		'DELETE FROM `'. TblPrefix() .'flip_session_data` ' . 
		'WHERE `user_id` = ' . $s->id
	);
	
	// Setpaid-History - Replace user ids
	MysqlWrite(
		'UPDATE `'. TblPrefix() .'flip_setpaid_history` ' . 
		'SET `orga_id` = ' . SYSTEM_DELETED_USER_ID . 
		' WHERE `orga_id` = ' . $s->id
	);
		
	MysqlWrite(
		'UPDATE `'. TblPrefix() .'flip_setpaid_history` ' . 
		'SET `user_id` = ' . SYSTEM_DELETED_USER_ID . 
		' WHERE `user_id` = ' . $s->id
	);
	
	// Sponsors - Update the uID
	MysqlWrite(
		'UPDATE `'. TblPrefix() .'flip_sponsor_sponsors` ' . 
		'SET `last_edit_user` = ' . SYSTEM_DELETED_USER_ID . 
		' WHERE `last_edit_user` = ' . $s->id
	);
	
	// Tickets - Replace the user
	MysqlWrite(
		'UPDATE `'. TblPrefix() .'flip_ticket_event` ' . 
		'SET `create_user` = ' . SYSTEM_DELETED_USER_ID . 
		' WHERE `create_user` = ' . $s->id
	);

	MysqlWrite(
		'UPDATE `'. TblPrefix() .'flip_ticket_tickets` ' . 
		'SET `create_user` = ' . SYSTEM_DELETED_USER_ID . 
		' WHERE `create_user` = ' . $s->id
	);
	
	MysqlWrite(
		'UPDATE `'. TblPrefix() .'flip_ticket_tickets` ' . 
		'SET `opening_user` = ' . SYSTEM_DELETED_USER_ID . 
		' WHERE `opening_user` = ' . $s->id
	);
	
	// Tournaments
	MysqlWrite(
		'DELETE FROM `'. TblPrefix() .'flip_tournament_invite` ' . 
		'WHERE `user_id` = ' . $s->id
	);
	
	MysqlWrite(
		'UPDATE `'. TblPrefix() .'flip_tournament_ranking` ' . 
		'SET `combatant_id` = ' . SYSTEM_DELETED_USER_ID . 
		' WHERE `combatant_id` = ' . $s->id
	);
	
	// Delete common data
	MysqlWrite("DELETE FROM `" . TblPrefix() . "flip_user_binary` WHERE (`subject_id` = '{$s->id}');");
	MysqlWrite("DELETE FROM `" . TblPrefix() . "flip_user_subject` WHERE (`id` = '{$s->id}');");
	MysqlWrite("DELETE FROM `" . TblPrefix() . "flip_user_data` WHERE (`subject_id` = '{$s->id}');");
	MysqlWrite("DELETE FROM `" . TblPrefix() . "flip_user_groups` WHERE ((`child_id` = '{$s->id}') OR (`parent_id` = '{$s->id}'));");
	MysqlWrite("DELETE FROM `" . TblPrefix() . "flip_user_rights` WHERE ((`owner_id` = '{$s->id}') OR (`controled_id` = '{$s->id}'));");
	
	// Webmessages - delete messages and folders
	MysqlWrite(
		'DELETE FROM `'. TblPrefix() .'flip_webmessage_message` ' . 
		'WHERE `owner_id` = ' . $s->id
	);
	
	MysqlWrite(
		'UPDATE `'. TblPrefix() .'flip_webmessage_message` ' . 
		'SET `sender_id` = ' . SYSTEM_DELETED_USER_ID . 
		' WHERE `sender_id` = ' . $s->id
	);
	
	MysqlWrite(
		'DELETE FROM `'. TblPrefix() .'flip_webmessage_folder` ' . 
		'WHERE `user_id` = ' . $s->id
	);
	
	$aSubject = NULL;
	
	return true;
}

function SubjectExists($aIdent, $aType = "") {
	$id = GetSubjectID($aIdent);
	if (!is_posDigit($id) or empty ($id))
		return false;
	$type = addslashes($aType);
	if (empty ($aType))
		return (MysqlReadFieldByID(TblPrefix() . "flip_user_subject", "id", $id, true) == $id) ? true : false;
	else
		return (MysqlReadFieldByID(TblPrefix() . "flip_user_subject", "type", $id, true) == $type) ? true : false;
}

function _readSubject($aIdent, $tryOnly) {
	// es ist schneller den typ des Idents fest zu stellen, als mysql in allen drei Spalten suchen zu lassen.
	$id = addslashes($aIdent);
	if (is_posDigit($aIdent))
		$type = "id";
	elseif (IsValidEmail($aIdent)) $type = "email";
	else
		$type = "name";
	return MysqlReadRow("SELECT * FROM `" . TblPrefix() . "flip_user_subject` WHERE (`$type` = '$id');", $tryOnly, "Eine SubjektID ist ung&uuml;ltig.|ID:$id");
}

function & CreateSubjectInstance(& $aIdent, $aForceType = "subject") {
	if (empty ($aIdent))
		trigger_error_text(text_translate("Ein Subject konnte nicht erstellt werden.") .
		"|Ident:$aIdent ForceType:$aForceType", E_USER_ERROR, __FILE__, __LINE__);

	if (is_a($aIdent, $aForceType))
		return $aIdent;
	elseif (is_object($aIdent)) trigger_error_text(text_translate("Ein &uuml;bergebenes Objekt ist nicht vom Typ \"?1?\"!", $aForceType) . "|ObjectClass:" . get_class($aIdent) . " Name:" . $aIdent->name, E_USER_ERROR, __FILE__, __LINE__);

	if (is_array($aIdent))
		$info = $aIdent;
	else
		$info = _readSubject($aIdent, false);

	$type = $info["type"];
	if (!class_exists($type)) {
		include_once ("mod/mod.subject.$type.php");
		if (!class_exists($type))
			trigger_error_text(text_translate("Ein Subject konnte nicht erstellt werden.") .
			"|Ident:$aIdent ForceType:$aForceType RealType:{$r->type}", E_USER_ERROR, __FILE__, __LINE__);
	}
	$r = new $type ($info);
	if (!is_a($r, $aForceType))
		trigger_error_text(text_translate("Ein Subject konnte nicht erstellt werden.") .
		"|Ident:$aIdent ForceType:$aForceType RealType:{$r->type}", E_USER_ERROR, __FILE__, __LINE__);
	return $r;
}

function RequireAllowCreateSubject($Type) {
	$a = array (
		"type" => $Type
	);
	$subj = CreateSubjectInstance($a);
	return $subj->requireAllowCreate();
}

class Subject {
	var $id;
	var $type;
	var $name;
	var $email;
	var $system;

	function Subject($Ident) {
		if (!is_array($Ident)) {
			$id = $Ident;
			$Ident = _readSubject($Ident, true);
			if (!$Ident)
				trigger_error_text(text_translate("Ein Subject konnte nicht gefunden werden.") .
				"|User:\"$id\"", E_USER_WARNING);
		}
		$this->id = (isset ($Ident["id"]) && is_posDigit($Ident["id"])) ? $Ident["id"] : null;
		$this->type = (isset ($Ident["type"])) ? $Ident["type"] : null;
		$this->name = (isset ($Ident["name"])) ? $Ident["name"] : null;
		$this->email = (isset ($Ident["email"])) ? $Ident["email"] : null;
		$this->system = ($this->type == 'system') ? 1 : 0;
	}

	function setProperties($Prop) {
		$err = false;
		$subj = array ();
		$cols = _GetColumnInfo($this->type);
		foreach ($Prop as $key => $val) {
			$key = strtolower($key);
			//vorhanden?
			if (!isset ($cols[$key])) {
				trigger_error_text(text_translate("Das Subject \"?1?\" hat keine Eigenschaft namens \"?2?\". Daher konnte sie nicht gespeichert werden.", array (
					$this->type,
					$key
				)), E_USER_WARNING);
				$err = true;
				continue;
			}
			if(!isset($cols[$key]['min_length'])) {
				// defaults for older DBs (during dbupdate)
				$cols[$key]['min_length'] = 0;
				$cols[$key]['max_length'] = 999999999999;
			}
			//Länge
			if ($val != '' && $cols[$key]['min_length'] > 0 && strlen($val) < $cols[$key]['min_length']) {
				trigger_error_text(text_translate('Die minimale Länge (?1?) von \'?2?\' wurde unterschritten.', array (
					$cols[$key]['min_length'],
					$cols[$key]['caption']
				)), E_USER_WARNING);
				$err = true;
				continue;
			}

			// Anpassung - Bilder Upload auf bestimmte größe einschränken
			if ($cols[$key]["val_type"] != "Image") {
        if ($cols[$key]["max_length"] > 0 && strlen($val) > $cols[$key]["max_length"]) {
          trigger_error_text(text_translate("Die maximale L&auml;nge (?1?) von \"?2?\" wurde &uuml;berschritten.", array (
            $cols[$key]["max_length"],
            $cols[$key]["caption"]
          )), E_USER_WARNING);
          $err = true;
          continue;
        }
			}
			else {
				$tempfilename = tempnam(".", "");
				file_put_contents($tempfilename, $val);

				// Edit VulkanLAN: max_length ist die maximale Bildgroesse, min_length die maximale Aufloesung (Hoehe und Breite)
				$max_size = $cols[$key]["max_length"];
				$max_res = $cols[$key]["min_length"];
				$imagesize = getimagesize ($tempfilename);
				unlink($tempfilename);

				if (($max_size > 0) and (strlen($val) > $max_size))
				  trigger_error_text("Das hochgeladene Bild darf nicht mehr als $max_size Bytes haben!", E_USER_ERROR);
				if (($max_res > 0) and (($imagesize["0"] > $max_res) or ($imagesize["1"] > $max_res)))
				  trigger_error_text("Die Aufl&ouml;sung des Bildes darf je Seite nicht mehr als $max_res Pixel haben.", E_USER_ERROR);
			}
      
			//je nach access-typ setzen
			switch ($cols[$key]["access"]) {
				case ("subject") :
					if (($key == "name") and !CheckSubjectName($val))
						$err = true;
					elseif (($key == "email") and !IsValidEmail($val)) {
						trigger_error_text(text_translate("Die Email-Adresse \"?1?\" ist ung&uuml;ltig.", $val), E_USER_WARNING);
						$err = true;
					}
					elseif ($this-> $key != $val) $this-> $key = $subj[$key] = $val;
					break;
				case ("data") :
					$v = addslashes($val);
					$colid = $cols[$key]["id"];
					if (($cols[$key]["default_val"] == $v) or is_empty($v)) {
						MysqlWrite("DELETE FROM `" . TblPrefix() . "flip_user_data` WHERE (`subject_id` = '{$this->id}') AND (`column_id` = '$colid')");
					} else {
						if (!MysqlWrite("REPLACE " . TblPrefix() . "flip_user_data (subject_id,column_id,val) VALUES('{$this->id}','$colid','$v');", "Die Eigenschaft \"$key\" von \"{$this->name}\" konnte nicht gespeichert werden."))
							$err = true;
					}
					break;
				case ("binary") :
					//if Uploadinfo, get data from file
					if (is_array($val) && isset ($val["tmp_name"])) {
						$mimetype = $val["type"];
						$val = file_get_contents($val["tmp_name"]);
					} else {
						//get MIME-Type
						$uploaddir = ini_get("upload_tmp_dir");
						if (empty ($uploaddir))
							$uploaddir = ConfigGet("template_temp"); //the template-dir should be writeable ;)
						if (function_exists("mime_content_type")) {
							//save temporary as file to get MIME-Type
							$tempfilename = tempnam(".", "");
							file_put_contents($tempfilename, $val);
							$mimetype = mime_content_type($tempfilename);
							unlink($tempfilename);
						}
						elseif (strtolower($cols[$key]["val_type"]) == "image") {
							//image: save temporary as file to get MIME-Type
							$tempfilename = tempnam(".", "");
							file_put_contents($tempfilename, $val);
							include_once ("mod/mod.image.php");
							$aImageLoader = new ImageLoaderFile($tempfilename);
							$mimetype = $aImageLoader->loadInfo();
							$mimetype = $mimetype["mime"];
							unset ($aImageLoader);
							unlink($tempfilename);
						} else {
							//default
							$mimetype = "application/octet-stream";
						}
					}

					$v = addslashes($val);
					$colid = $cols[$key]["id"];
					if (($cols[$key]["default_val"] == $v) or empty ($v)) {
						MysqlWrite("DELETE FROM `" . TblPrefix() . "flip_user_binary` WHERE (`subject_id` = '{$this->id}') AND (`column_id` = '$colid')");
					} else {
						//VulkanLAN edit Image Bildaufloesung
						if (!MysqlWrite("REPLACE INTO `" . TblPrefix() . "flip_user_binary` (subject_id,column_id,mimetype,data) VALUES('{$this->id}','$colid','$mimetype','$v');", "Die Eigenschaft \"$key\" von \"{$this->name}\" konnte nicht gespeichert werden.")) {
							$err = true;
						}
					}
					break;
				case ("callback") :
					trigger_error_text(text_translate("Die Eigenschaft \"?1?\" von \"?2?\" besitzt keine Funktion um sie zu schreiben.", array (
						$key,
						$this->name
					)), E_USER_WARNING);
					break;
				case ("method") :
					// kommt ncoh
					break;
				default :
					trigger_error_text(text_translate("Unbekannter Accesstyp") . '|access:"'.$cols[$key]['access'].'", FieldName: '.$key, E_USER_WARNING);
					$err = true;
			}
		}

		if (count($subj) > 0)
			if (!MysqlWriteById(TblPrefix() . "flip_user_subject", $subj, $this->id))
				$err = true;
		return ($err) ? false : true;
	}

	function getProperties($aNames = array ()) {
		$binary = $data = $r = array ();
		$cols = _GetColumnInfo($this->type);
		if (count($aNames) == 0)
			$aNames = array_keys($cols);
		foreach ($aNames as $name) {
			if (!isset ($cols[$name])) {
				trigger_error_text(
					text_translate("Das Subjekt '" . $this->name .  "' besitzt keine Eigenschaft namens '".$name."'!") 
						. "|PropertyName: $name"
					, E_USER_WARNING
				);
				continue;
			}
			
			switch ($cols[$name]["access"]) {
				case ("subject") :
          $r[$name] = $this->$name;
					break;
				case ("data") :
					$data[] = $name;
					break;
				case ("binary") :
					if (strtolower($cols[$name]["val_type"]) == "image") {
						$tmpColData = MysqlReadRow("SELECT b.id, b.mtime FROM `" . TblPrefix() . "flip_user_column` c LEFT JOIN " . TblPrefix() . "flip_user_binary b ON b.column_id = c.id AND b.subject_id = {$this->id} WHERE c.name = '$name' AND c.type = '{$this->type}'");
						
						// FIXME: Determine correct value for these fields
						$dt1 = $dt2 = '';
						
						$r[$name] = (!empty ($tmpColData["id"])) ? TblPrefix() . "flip_user_binary:data:" . $tmpColData["id"] . ":" . $tmpColData["mtime"] . ":$dt1:$dt2" : "";
					} else {
						$binary[] = $name;
					}
					break;
				case ("callback") :
					$r[$name] = ExecCallback($cols[$name]["callback_read"], array (
						$this
					));
					break;
				case ("method") :
					// kommt ncoh
					break;
				default :
					trigger_error_text(text_translate("Unbekannter Accesstyp") . "|access:\"{$cols[$name][access]}\", PropertyName: $name", E_USER_WARNING);
			}
		}
		//user_data
		if (count($data) > 0) {
			$n = implode_sqlIn($data);
			$q = MysqlReadCol("
						        SELECT c.name,d.val FROM (" . TblPrefix() . "flip_user_column c)
						          LEFT JOIN " . TblPrefix() . "flip_user_data d ON ((d.column_id = c.id) AND (d.subject_id = {$this->id}))
						          WHERE (c.name IN ($n) AND (c.type = '{$this->type}'));
						      ", "val", "name");
			if (is_array($q))
				foreach ($q as $k => $v)
					$r[$k] = (is_null($v)) ? $cols[$k]["default_val"] : $v;
		}
		//user_binary
		if (count($binary) > 0) {
			$n = implode_sqlIn($binary);
			$q = MysqlReadCol("
						        SELECT c.name,b.data FROM (" . TblPrefix() . "flip_user_column c)
						          LEFT JOIN " . TblPrefix() . "flip_user_binary b ON ((b.column_id = c.id) AND (b.subject_id = {$this->id}))
						          WHERE (c.name IN ($n) AND (c.type = '{$this->type}'));
						      ", "data", "name");
			if (is_array($q))
				foreach ($q as $k => $v)
					$r[$k] = (is_null($v)) ? $cols[$k]["default_val"] : $v;
		}
		return $r;
	}

	function setProperty($Name, $Value) {
		return $this->setProperties(array (
			$Name => $Value
		));
	}

	function getProperty($aName) {
		$r = $this->getProperties(array (
			$aName
		));
		return $r[$aName];
	}

	// Methoden zum &uuml;berschreiben in den konkreten Implementierungen (User, Group,...)
	function requireAllowCreate() {
		trigger_error_text(text_translate("Subjekte vom Typ ?1? k&ouml;nnen nicht erstellt werden.", get_class($this)), E_USER_ERROR);
	}

	function afterCreation() {}

	function delete() {}
}

class RightSubject extends Subject {
	var $_Rights; // array(RightID => array(ControledSubjID1,ControledSubjID2,...),...)
	var $_InheritedIDs;

	function RightSubject($Ident) {
		parent :: Subject($Ident);
	}

	function _getInheritedIDs() {
		if (!(isset ($this->_InheritedIDs) && is_array($this->_InheritedIDs))) {
			$i = _GetInheritedIDs($this->id);
			$i[$this->id] = $this->name;
			$this->_InheritedIDs = $i;
		}
		return $this->_InheritedIDs;
	}

	function _getAllRights($aRight = 0) {
		if (!(isset ($this->_Rights) && is_array($this->_Rights))) {
			$a = array ();
			$sids = implode_sqlIn(array_keys($this->_getInheritedIDs()));
			$rights = MysqlReadArea("SELECT * FROM `" . TblPrefix() . "flip_user_rights` WHERE (`owner_id` IN ($sids));");
			foreach ($rights as $r) {
				if (!isset ($a[$r["right_id"]]))
					$a[$r["right_id"]] = array (
						$r["controled_id"]
					);
				else
					$a[$r["right_id"]][] = $r["controled_id"];
			}
			$this->_Rights = $a;
		}
		if (empty ($aRight)) {
			return $this->_Rights;
		} else {
			return isset ($this->_Rights[GetRightID($aRight)]) ? $this->_Rights[GetRightID($aRight)] : null;
		}
	}

	function getRights() {
		$sids = array_keys($this->_getInheritedIDs());
		$sids[] = 0;
		$sids = implode_sqlIn($sids);
		return MysqlReadArea("
				      SELECT r.* FROM `" . TblPrefix() . "flip_user_rights` s, `" . TblPrefix() . "flip_user_right` r 
				        WHERE (
				          (s.owner_id IN ($sids)) AND 
				          (s.controled_id IN ($sids)) AND 
				          (s.right_id = r.id)
				        )
						ORDER BY r.right;
				    ", "id");
	}

	function getRightsOver($aSubject) {
		$r = array ();
		$id = GetSubjectID($aSubject);
		$ids = array_keys(_GetInheritedIDs($id));
		$ids[] = $id;
		$rights = GetRights();
		foreach ($this->_getAllRights() as $rid => $subj)
			if (count(array_intersect($subj, $ids)) > 0)
				$r[$rid] = $rights[$rid];
		return $r;
	}

	/**
	 * Alle Subjekte, welche Rechte &uuml;ber dieses Subjekt haben.
	 * 
	 * @author loom
	 * @since 1398 - 19.05.2007
	 * @param mixed $right (optional) Recht (ID oder Name)
	 * @return Array alle SubjektIDs welche Recht &uuml;ber dieses Subjekt haben
	 */
	function getEntitled($right = false) {
		if ($right) {

			$right = " AND `right_id` = " . escape_sqlData(GetRightID($right));
		}
		return MysqlReadCol("SELECT owner_id FROM `" . TblPrefix() . "flip_user_rights` WHERE (`controled_id` = " . escape_sqlData($this->id) . ")$right;", "owner_id", "owner_id");
	}

	function getControlableIDs($aRight) {
		$rights = $this->_getAllRights();
		return $rights[GetRightID($aRight)];
	}

	function getControlableSubjects($aRight) {
		$ids = implode_sqlIn($this->getControlableIDs());
		if (empty ($ids))
			return array ();
		$users = MysqlReadArea("SELECT * FROM " . TblPrefix() . "flip_user_subjects WHERE (`id` IN ($ids))", "id");
		return _AddChildIDS($users);
	}

	function getRightsInfo() {
		$sids = implode_sqlIn(array_keys($this->_getInheritedIDs()));
		return MysqlReadArea("
				      SELECT r.*, s.id AS rights_id,  s.owner_id, o.name AS owner_name, o.type AS owner_type, 
				             s.controled_id, c.name AS controled_name, c.type AS controled_type 
				        FROM (`" . TblPrefix() . "flip_user_rights` s, `" . TblPrefix() . "flip_user_right` r)
				        LEFT JOIN `" . TblPrefix() . "flip_user_subject` o ON (s.owner_id = o.id)
				        LEFT JOIN `" . TblPrefix() . "flip_user_subject` c ON (s.controled_id = c.id)
				        WHERE ((`owner_id` IN ($sids)) AND (r.id = s.right_id))
				        ORDER BY r.right;
				    ");
	}

	function hasRight($aRight) {
		if (empty ($aRight))
			return true;
		if (($aRight == -1) or ($aRight == "infinite"))
			return false;
		$subj = $this->_getAllRights($aRight);
		if (!is_array($subj))
			return false;
		if (in_array(0, $subj))
			return true;
		if (count(array_intersect($subj, array_keys($this->_getInheritedIDs()))) > 0)
			return true;
		return false;
	}

	function hasRightOver($aRight, $aSubject) {
		if (empty ($aRight))
			return true;
		if (($aRight == -1) or ($aRight == "infinite"))
			return false;
		$subj = $this->_getAllRights($aRight);
		if (!is_array($subj))
			return false;
		$s = GetSubjectID($aSubject);
		if ($s == $this->id)
			if (in_array(0, $subj))
				return true;
		if (in_array($s, $subj))
			return true;
		if (count(array_intersect($subj, array_keys(_GetInheritedIDs($s)))) > 0)
			return true;
		return false;
	}

	function requireRight($aRight, $aErrorMsg = "") {
		if ($this->hasRight($aRight))
			return true;
		$aErrorMsg = htmlentities_single($aErrorMsg);
		if (empty ($aErrorMsg))
			if (method_exists($this, "isLoggedIn"))
				if (!$this->isLoggedIn()) {
					$aErrorMsg = text_translate("Bitte logge dich mit deinem Account ein, damit diese Seite angezeigt werden kann!.<br/>" . "Wenn du noch keinen Account hast, kannst du einen <a href=\"user.php?frame=register\">neuen Account erstellen</a>.") . "|Right:" . htmlentities_single($aRight);
				}
		if (empty ($aErrorMsg))
			$aErrorMsg = text_translate("Der Benutzer ist nicht berechtigt diese Seite anzuzeigen.") . "|" . htmlentities_single("User:{$this->name} Right:$aRight");
		trigger_error($aErrorMsg, E_USER_ERROR);
	}

	function requireRightOver($aRight, $aSubject, $aErrorMsg = "") {
		if ($this->hasRightOver($aRight, $aSubject))
			return true;
		if (empty ($aErrorMsg))
			if (method_exists($this, "isLoggedIn"))
				if (!$this->isLoggedIn())
					$aErrorMsg = text_translate("Bitte logge dich mit deinem Account ein, damit diese Seite angezeigt werden kann!") . "|Right:$aRight Subject:" . GetSubjectID($aSubject);
		if (empty ($aErrorMsg))
			$aErrorMsg = text_translate("Der Benutzer hat nicht gen&uuml;gend Rechte.") . "|User:{$this->name} Right:$aRight Subject:" . GetSubjectID($aSubject);
		trigger_error_text($aErrorMsg, E_USER_ERROR);
	}

	function sqlHasRight($ColName) {
		if (!strstr($ColName, "."))
			$ColName = "`$ColName`";
		$iids = array_keys($this->_getInheritedIDs());
		$iids[] = 0;
		$r = array (
			0
		);
		foreach ($this->_getAllRights() as $rid => $sids) {
			if (count(array_intersect($iids, $sids)) > 0)
				$r[] = $rid;
		}
		return "($ColName IN (" . implode_sqlIn($r) . "))";
	}

	function isChildOf($aParent) {
		$ids = $this->_getInheritedIDs();
		$subj = GetSubjectID($aParent);
		return ((isset ($ids[$subj])) or in_array($subj, $ids)) ? true : false;
	}

	function addRight($aRight, $aControledSubj = 0) {
		if ($r = _AddRight($this->id, $aRight, $aControledSubj))
			unset ($this->_Rights);
		return $r;
	}

	function remRight($aRight, $aControledSubj = 0) {
		if ($r = _RemRight($this->id, $aRight, $aControledSubj))
			unset ($this->_Rights);
		return $r;
	}
}

class ChildSubject extends RightSubject {
	function ChildSubject($Ident) {
		parent :: RightSubject($Ident);
	}

	function getParents($type = false) {
		if ($type)
			$type = " AND (s.type = '$type')";
		return MysqlReadCol("
				      SELECT s.id,s.name
				        FROM `" . TblPrefix() . "flip_user_groups` g, `" . TblPrefix() . "flip_user_subject` s
				        WHERE ((g.child_id = {$this->id}) AND (g.parent_id = s.id)$type)
				        ORDER BY s.name
				    ", "name", "id");
	}

	function addParent($aSubject) {
		global $Session;
		if ($r = _AddToGroup($this->id, $aSubject)) {
			unset ($this->_InheritedIDs);
			unset ($this->_Rights);
		}
		return $r;
	}

	function remParent($aSubject) {
		global $Session;
		if ($r = _RemFromGroup($this->id, $aSubject)) {
			unset ($this->_InheritedIDs);
			unset ($this->_Rights);
		}
		return $r;
	}
}

class ParentSubject extends RightSubject {
	/**
	 * array(id => name)
	 * @access private
	 */
	var $childs = array ();

	function ParentSubject($Ident) {
		parent :: RightSubject($Ident);
	}

	function getChilds($type = false) {
		$key = "all";
		if (!empty ($type)) {
			$key = $type;
			$type = " AND (s.type = '$type')";
		}
		if (!isset ($this->childs[$key])) {
			$this->childs[$key] = MysqlReadCol("
					      SELECT g.child_id, s.name
					        FROM `" . TblPrefix() . "flip_user_groups` g, `" . TblPrefix() . "flip_user_subject` s
					        WHERE ((g.parent_id = '{$this->id}') AND (g.child_id = s.id)$type)
					        ORDER BY s.name
					    ", "name", "child_id");
		}
		return $this->childs[$key];
	}

	function getChildsCount() {
		return MysqlReadField("SELECT COUNT(`child_id`) FROM `" . TblPrefix() . "flip_user_groups` WHERE (`parent_id` = {$this->id});");
	}

	function getChildsCountIgnoreDisabled() {
		$cols = _GetColumnInfo("user");
		$colid = $cols["enabled"]["id"];
		return MysqlReadField("
				      SELECT COUNT(g.child_id) 
				        FROM `" . TblPrefix() . "flip_user_groups` g, `" . TblPrefix() . "flip_user_data` d
				        WHERE (g.parent_id = {$this->id}) AND (d.subject_id = g.child_id) AND (d.val = 'Y') AND (d.column_id = $colid);
				    ");
	}

	function addChild($aSubject) {
		global $Session;
		if ($r = _AddToGroup($aSubject, $this->id)) {
			$this->childs[GetSubjectID($aSubject)] = GetSubjectName($aSubject);
			unset ($this->_InheritedIDs);
			unset ($this->_Rights);
		}
		return $r;
	}

	function remChild($aSubject) {
		global $Session;
		if ($r = _RemFromGroup($aSubject, $this->id)) {
			unset ($this->childs[GetSubjectID($aSubject)]);
			unset ($this->_InheritedIDs);
			unset ($this->_Rights);
		}
		return $r;
	}
}

function UserTryLogin() {
	if (isset ($_POST["ident"]) and $_POST["password"] != "") {
		return UserLogin($_POST["ident"], $_POST["password"]);
	}
	
	if (isset ($_GET["ident"]) and $_GET["password"] != "") {
		return UserLogin($_GET["ident"], $_GET["password"]);
	}
	
	if (isset ($_COOKIE["ident"]) and $_COOKIE["password"] != "") {
		return UserLogin($_COOKIE["ident"], $_COOKIE["password"]);
	}
	
	return false;
}

function UserLogin($Ident, $Password, $ForceEnabled = true) {
	global $User;
  
	$loginerr = text_translate("Login fehlgeschlagen: Der Account existiert nicht oder das Password ist falsch. Bitte verwende deine UserID, deine Email-Adresse oder deinen Nicknamen zusammen mit deinem Password zum Einloggen. Solltest du dein Password vergessen haben, kannst du es <a href=\"text.php?name=user_account_trouble\">&uuml;berschreiben</a>.") . "|Ident:" . htmlentities_single($Ident);
	old_Try();
	$u = new User($Ident);
	
	// Logout? If so, redirect to the index page
	if(is_a($User, 'User')) {
		if(($User->name != 'Anonymous') && ($Ident == 'Anonymous')) {
			header('Location: index.php');
			return $u;
		}
	}
	
	if (old_Except(false))
		return trigger_error($loginerr, E_USER_WARNING);
	if ($u->isLoggedIn()) {
		if (!$u->checkPassword($Password))
			return trigger_error_text($loginerr, E_USER_WARNING);
		if ($ForceEnabled) {
			if ($u->getProperty("enabled") != "Y")
				return trigger_error(text_translate("Dein Account ist noch deaktiviert (oder wurde deaktiviert, da deine Emailadresse ung&uuml;ltig geworden ist). Einmalig kannst du ihn mittels der Aktivierungs-URL, die du per EMail von uns erhalten hast, aktivieren. Diese Mail kannst du auch <a href=\"text.php?name=user_account_trouble\">erneut anfordern</a>."), E_USER_WARNING);
			$u->setProperty("last_login_time", time());
			LogAction(text_translate("Erfolgreich eingeloggt als ?1? [?2?]", array (
				$u->name,
				$u->id
			)));
			LogNetLog("user_login", "", $u);

			// ip-adresse &uuml;berpr&uuml;fen
			/*      $ip = $_SERVER["REMOTE_ADDR"];
			      if(preg_match("/^10\.10\..*$/",$ip))
			      {
			        $uip = $u->getProperty("ip");
			        if($uip != $ip) trigger_error_text("Deine IP-Addresse ($ip) ist nicht die dir zugewiesene ($uip). Wenn du dich von deinem Computer aus eingeloggt hast, dann ist es wichtig, dass du deine IP-Addresse korrigierst.");
			      }*/
			$DoWrong = ConfigGet("user_ip_wrong_msg_opt");
			$DoBogon = ConfigGet("user_ip_bogon_msg_opt");
			$DoEmpty = ConfigGet("user_ip_empty_msg_opt");

			if (!empty ($DoWrong) or !empty ($DoBogon) or !empty ($DoEmpty)) {
				$rip = $_SERVER["REMOTE_ADDR"];
				$uip = $u->getProperty("ip");
				$msg = " " . text_translate("Deine IP ist: \"?1?\". Deine dir zugewiesene IP ist: \"?2?\"", array (
					$rip,
					$uip
				));

				// wenn der user keine ip hat
				if (empty ($uip) and !empty ($DoEmpty))
					trigger_error_text(ConfigGet("user_ip_empty_msg") . $msg, $DoEmpty);

				// wenn der user die falsche ip hat.
				if ($rip != $uip) {
					include_once ("mod/mod.seats.php");
					$user_id = SeatGetUserIDByIP($uip);
					if (empty ($user_id) and !empty ($DoBogon))
						trigger_error_text(ConfigGet("user_ip_bogon_msg") . $msg, $DoBogon);
					elseif (!empty ($DoWrong)) trigger_error_text(ConfigGet("user_ip_wrong_msg") . $msg, $DoWrong);
				}
			}
		}
	}
	elseif ($ForceEnabled) {
		LogAction(text_translate("Erfolgreich ausgeloggt."));
		LogNetLog("user_logout", "");
	}
	return $u;
}

class User extends ChildSubject {
	var $_Config = array ();

	function User($Ident) {
		parent :: ChildSubject($Ident);
		if (($this->type != "user") && $this->type != "systemuser") {
			trigger_error_text(
				text_translate("Ein User kann nur von Subjecten des Typs \"user\" erstellt werden!") . 
					"|Ident : $Ident", 
				E_USER_WARNING
			);
		}
	}

	function checkPassword($Password) {
    $correct = false;

    $p = $this->getProperty("password");
    /*
     * new password hashing using bcrypt
     */
    if(verifyPasswordHash($Password, $p)) {
      return true;
    }
    
    /*
     * Old password hashing methods for legacy support.
     * If authentication is successful, password hash will be upgraded to current
     * hashing method.
     */
		if ($p == md5($Password)) {
			$correct = true;
		}
		if (function_exists("sha1") && $p == sha1($Password)) { //since PHP 4.3.0
			$correct = true;
		}
		if ($p == sprintf("%u", crc32($Password))) {
			$correct = true;
		}
		if ($p == crypt($Password)) {
			$correct = true;
		}
		if ($p == MysqlReadField("SELECT PASSWORD('" . addslashes(addcslashes($Password, "*")) . "')")) {
			$correct = true;
		}
		//if ($p == MysqlReadField("SELECT OLD_PASSWORD('" . addslashes($Password) . "')")) {
		//	$correct = true;
		//}
    
    if($correct === true) {
      // upgrade password
      $newHash = getPasswordHash($Password);
      $this->setProperty('password', $newHash);
      
      return true;
    }
 
		return false;
	}

	function isLoggedIn() {
		return (($this->name != "Anonymous") and ($this->name != "")) ? true : false;
	}

	function setProperties($Prop) {
		$r = parent :: setProperties($Prop);
		if ($r)
			foreach ($Prop as $k => $v)
				if (isset ($this->_Config[$k]))
					$this->_Config[$k] = $v;
		return $r;
	}

	function getConfigs($Keys) {
		$query = $result = array ();

		foreach ($Keys as $k)
			if (array_key_exists($k, $this->_Config))
				$result[$k] = $this->_Config[$k];
			else
				$query[] = $k;

		if (count($query) > 0)
			foreach ($this->getProperties($query) as $k => $v)
				$result[$k] = $this->_Config[$k] = $v;

		return $result;
	}

	function getConfig($Key) {
		$r = $this->getConfigs(array (
			$Key
		));
		return $r[$Key];
	}

	function setRandomID() {
		$rnd = $this->getProperty("random_id");
		if (empty ($rnd)) {
			srand((float) microtime() * 1000000);
			$rnd = md5(rand());
			$this->setProperty("random_id", $rnd);
		}
		return $rnd;
	}

	function requireAllowCreate() {
		global $User;
		if (!$User->hasRight("user_admin_subjects") and !$User->hasRight("checkin"))
			trigger_error_text(text_translate("Um Subjekte erstellen zu d&uuml;rfen ben&ouml;tigst du das Recht sie zu Administrieren oder Einzuchecken."), E_USER_ERROR);
	}

	/**
	 * Removes unneccessary data from the User object.
	 * Use this method to strip data from the object before serializing it.
	 */
	function compact() {
		unset($this->_Rights);
		unset($this->_InheritedIDs);
	}
}

class Group extends ParentSubject {
	function requireAllowCreate() {
		global $User;
		$User->requireRight("user_admin_subjects");
	}
}
?>
