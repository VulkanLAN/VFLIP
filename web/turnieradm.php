<?php
/**
 * Turnierverwaltungsseite
 * @author Daniel Raap
 * @version $Id: turnieradm.php 1702 2019-01-09 09:01:12Z loom $ edit by VulkanLAN
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ('core/core.php');
require_once ('inc/inc.page.php');
require_once ('mod/mod.tournament.admin.php');
//require_once ('mod/mod.tournament.liga.php');

class turnieradmPage extends Page {
	//Config
	var $partydateconfig = 'lanparty_date';
	var $partynameconfig = 'lanparty_fulltitle';
	//Templates
	var $texttplfile = 'text.tpl';
	var $texttplsub = 'text';
	var $texttplvar = 'text';
	//Module
	var $xmlmod = 'mod/mod.xmlgen.php';
	//Dateien
	var $wwclgameini = 'gameini.xml';
	//(Paket)intern
	var $adm; //object of class "TurnierAdmin" (mod.tournament.admin.php)
	var $tpl; //all the template data which is collected in the methods

	//php 7 public function __construct()
	//php 5 original function turnieradmPage()
	function __construct() {
		//php 5:
		//$this->Page();
		//php7 neu:
		parent::__construct();
		$id = (isset($this->FrameVars["id"])) ? $this->FrameVars["id"] : null;
		$this->adm = new TurnierAdmin($id);
		$this->Caption = "Turnieradmin";
		global $User;
		if (!$User->hasRight(TournamentAdminright()))
			$User->requireRightOver("tournament_orga", 'Turniersubjekt'.$_GET["id"]);
		$this->tpl = array ();
		$this->tpl["id"] = $this->adm->id;
	}

	function framedefault($get, $post) {
		return $this->TextOnlyOutput("Bitte benutze einen g&uuml;ltigen Link zum administrieren!");
	}

	function TextOnlyOutput($text = "") {
		$this->TplFile = $this->texttplfile;
		$this->TplSub = $this->texttplsub;
		return array ($this->texttplvar => $text);
	}

	function NoOutput() {
		$this->Caption = "";
		$this->ShowContentCell = false;
		return $this->TextOnlyOutput();
	}

	function framesettostart() {
		if (!$this->adm->Orga)
			return TournamentError("Keine Berechtigung!", E_USER_ERROR, __FILE__, __LINE__);
		$this->adm->changeStatus("start");
		$this->gotoTree();
	}

	function framecreate_matches($get) {
		ArrayWithKeys($get, array("id"));
		if ($this->adm->status == "start") {
			$t = MysqlReadRowByID(TblPrefix()."flip_tournament_tournaments", $this->adm->id);
			if ($t["type"] == "group")
				$t["status"] = $status = "grpgames";
			else
				$t["status"] = $status = "games";
			$this->submitedit($t);
			$statuses = TournamentStatuses();
			$text = "Status wurde auf ".$statuses[$status]." gesetzt.<br>";
			TournamentError($text."<div align=\"center\"><a href=\"tournament.php?frame=tree&amp;id=".$get["id"]."\">zur&uuml;ck</a></div>");
			//Leere Seite (nur Nachricht^)
			return $this->NoOutput();
		}
		if ($this->adm->GenerateMatches()) {
			$this->gotoTree();
			return array ();
		} else {
			$text = "Spiele wurden NICHT erstellt.<br>";
			TournamentError($text."<div align=\"center\"><a href=\"tournament.php?frame=tree&amp;id=".$get["id"]."\">zur&uuml;ck</a></div>");
			//Leere Seite (nur Nachricht^)
			return $this->NoOutput();
		}
	}

	function frameliga() {
		global $User;
		$User->requireRight(TournamentAdminright());
		$this->Caption = 'Liga ausw&auml;hlen';
		$this->tpl['ligalist'] = TournamentLigen();
		$this->tpl['ligen'] = array(); 
		foreach($this->tpl['ligalist'] AS $dbName => $caption) {
			$liga = TournamentGetLiga($dbName);
			$this->tpl['ligen'][] = array('liga'=>$liga->dbAndClassName, 'xml'=>isset($liga->xmlfile));
		}
		return $this->tpl;
	}
	
	function submitliga($post) {
		global $User;
		$User->requireRight(TournamentAdminright());
		ArrayWithKeys($post, array("liga"));
		Redirect("turnieradm.php?frame=add&liga=".urlencode($post["liga"]));
	}
	
	function actionupdategames($post) {
		global $User;
		$User->requireRight(TournamentAdminright());
		ArrayWithKeys($post, array("liga"));
		if(!empty($post["liga"])) {
			$liga = TournamentGetLiga($post['liga']);
			return $liga->updateGames();
		}
	}

	function frameadd($get) {
		global $User;
		ArrayWithKeys($get, array('liga'));
		$User->requireRight(TournamentAdminright());
		$this->Caption = 'Turnier hinzuf&uuml;gen';
		/* Tournament- and Forminfo */
		$this->tpl['formaction'] = 'turnieradm.php?frame=change&amp;id=0';
		$this->tpl['formname'] = 'add';
		$this->tpl['frame'] = 'default';
		$this->_TEditValues($get['liga']);
		$this->tpl['submit'] = 'hinzuf&uuml;gen';
		/* Defaults */
		$this->tpl['teamsize'] = '1';
		$this->tpl['maximum'] = '32';
		$this->tpl['roundtime'] = '30';
		$this->tpl['coins'] = '0';
		$this->tpl['description'] = '';
		$this->tpl['start'] = time() + 2 * 60 * 60;
		$this->tpl['type'] = 'ko';
		$this->tpl['treetype'] = 'default';
		$this->tpl['looser'] = 'N';
		$this->tpl['u18'] = 'Y';
		$this->tpl['groups'] = '0';
		$this->tpl['screenshots'] = 'N';
		$this->tpl['status'] = 'open';

		return $this->tpl;
	}

	function _TEditValues($liga) {
		if(!isset($liga)) {
			Redirect('turnieradm.php?frame=liga');
		} else {
			if(!TournamentValidLiga($liga)) {
				return trigger_error_text(text_translate('Es wurde keine g&uuml;ltige Liga angegeben!'),E_USER_ERROR, __FILE__,__LINE__);
			}
			$this->tpl['liga'] = $liga;
		}
		$ligaObj = TournamentGetLiga($liga);
		$this->tpl['games'] = $ligaObj->getGameStringArray();
		$this->tpl['notAskTeamsize'] = TournamentGamesIncludeTeamsize($ligaObj);
		$this->tpl['types'] = $this->adm->types;
		$this->tpl['treetypes'] = $this->adm->treetypes;
		$this->tpl['ligen'] = TournamentLigen();
		$this->tpl['grouplist'] = MysqlReadCol('SELECT `id`, `name` FROM `'.TblPrefix().'flip_tournament_groups` ORDER BY `order`', 'name', 'id');
		$this->tpl['coinslist'] = array();
		foreach (TournamentGetCoins() AS $aCoin)
			$this->tpl['coinlist'][$aCoin['id']] = $aCoin['currency'].' ('.$aCoin['maxcoins'].')';
		if(empty($this->tpl['grouplist']) || empty($this->tpl['coinlist'])) {
			trigger_error_text('Es muss eine Turniergruppe und eine Kostengruppe vorhanden sein! Bitte fehlendes erstellen.', E_USER_WARNING, __FILE__,__LINE__);
			Redirect('turnieradm.php?frame=editgroups');
		}
		$this->tpl['currency'] = $this->adm->currency;
		$this->tpl['html'] = '1';
		$this->tpl['statuses'] = TournamentStatuses();
		$this->tpl['u18configname'] = $this->adm->u18CheckConfig;
		$this->tpl['u18config'] = ConfigGet($this->adm->u18CheckConfig);
	}

	function submitadd($a) {
		global $User;
		/* Checks */
		$User->requireRight(TournamentAdminright());
		if ($a["html"] != "1")
			$a["description"] = "<pre>".$a["description"]."</pre>\n";
		/* Add new tournament and go to edit page/frame */
		if (($neuesTurnier = $this->adm->createTournament($a)) && $this->FrameVars["frame"] = "change") {
			$this->FrameVars["id"] = $neuesTurnier->id;
			$neuesTurnier->addOrga($User);
			//HACK to Page::execSubmit() not to go "backlink" but forward ;-) [case-sensitive!]
			$this->NextPage = str_replace("&id=0", "&id=".$this->FrameVars["id"], $this->NextPage);
			$this->tpl["done"] = TournamentGetTournamentString($this->FrameVars["id"])."-Turnier wurde angelegt.<br>\n";
		} else {
			$liga = TournamentGetLiga($a['liga']);
			return TournamentError($liga->getGameString($a['game']).'-Turnier wurde NICHT angelegt.', E_USER_NOTICE, __FILE__, __LINE__);
		}
		return true;
	}

	function framechange($get) {
		$tournament = MysqlReadRowByID(TblPrefix()."flip_tournament_tournaments", $get["id"]);
		$this->Caption = "Turnier bearbeiten";

		$this->_TEditValues($tournament["liga"]);
		$this->tpl["frame"] = "overview&amp;id=".$get["id"];
		$this->tpl["formname"] = "edit";
		foreach ($tournament AS $col => $val)
			$this->tpl[$col] = $val;
		$this->tpl["submit"] = "&auml;ndern";
		$this->TplSub = "add";
		return $this->tpl;
	}

	function submitedit($a) {
		if ($a["html"] != "1")
			$a["description"] = "<pre>".$a["description"]."</pre>\n";
		if (!isset ($a["icon"]))
			unset ($a["icon"]);
		if ($this->adm->Edit($a["id"], $a)) {
			$this->tpl["done"] = "Turnierdaten wurde ge&auml;ndert.<br>\n";
		} else {
			return TournamentError(TournamentGetTournamentString($a["id"])."-Turnier wurde NICHT ge&auml;ndert.<br>\n", E_USER_WARNING, __FILE__, __LINE__);
		}
		return $this->adm->_Log("Turnier wurde bearbeitet.");
	}

	function frameeditgroups() {
		global $User;
		$User->requireRight(TournamentAdminright());

		$this->Caption = text_translate('Gruppenverwaltung');
		$this->tpl['groups'] = TournamentGetGroups();
		$first = true;
		foreach ($this->tpl['groups'] AS $key => $group)
			if (!$first) {
				$this->tpl['groups'][$key]['move'] = true;
			} else {
				$first = false;
				$this->tpl['groups'][$key]['move'] = false;
			}

		$this->tpl['coins'] = TournamentGetCoins();

		return $this->tpl;
	}

	/**
	 * L&ouml;scht eine Zeile nur, wenn mehrere vorhanden sind
	 * 
	 * @access private
	 * @param String $table Tabellenname
	 * @param integer $id ID des zu l&ouml;schenden Datensatzes
	 * @param String $columnstring Bezeichnung des Datensatzes. Wird in den Fehlermeldungen verwendet. 
	 **/
	function _deleteRowremainOne($table, $id, $columnstring = 'Zeile') {
		global $User;
		$User->requireRight(TournamentAdminright());

		if (1 < MysqlReadField("SELECT COUNT(*) FROM `$table`", "COUNT(*)")) {
			if (MysqlDeleteByID($table, $id))
				trigger_error_text("Die $columnstring wurde gel&ouml;scht.");
			else
				trigger_error_text("Fehler beim L&ouml;schen der $columnstring|ID=".$id, E_USER_WARNING);
		} else {
			trigger_error_text("Es muss mindestens eine $columnstring vorhanden sein!", E_USER_WARNING);
		}
	}

	function actiondeltournamentgroup($post) {
		$this->_deleteRowremainOne(TblPrefix().'flip_tournament_groups', $post['id'], 'Turniergruppe');
	}

	function actiondelcoingroup($post) {
		$this->_deleteRowremainOne(TblPrefix().'flip_tournament_coins', $post['id'], 'Kostengruppe');
	}

	function frameeditgroup($get) {
		global $User;
		$User->requireRight(TournamentAdminright());
		ArrayWithKeys($get, array('gid'));

		if (empty ($get['gid'])) {
			$this->Caption = text_translate('Turniergruppe erstellen');
			$this->tpl['group'] = array ();
		} else {
			$this->Caption = text_translate('Turniergruppe bearbeiten');
			$this->tpl['group'] = TournamentGetGroup($get['gid']);
		}

		return $this->tpl;
	}

	function submitgroup($post) {
		global $User;
		$User->requireRight(TournamentAdminright());
		ArrayWithKeys($post, array('gid'));

		$id = $post['gid'];
		unset ($post['gid']);
		if (empty ($id)) {
			$post['order'] = $this->_getnextgrouporder();
		}
		elseif (MysqlReadRow("SELECT id FROM ".TblPrefix()."flip_tournament_groups WHERE `order`='".escape_sqlData_without_quotes($post["order"])."' AND `id`<>'".escape_sqlData_without_quotes($id)."'", true)) {
			$post['order'] = $this->_getnextgrouporder();
		}
		if (!$post['order'])
			return $this->turnier->Error('Fehler bei der Reihenfolge der Gruppe.', E_USER_WARNING);
		if (MysqlWriteByID(TblPrefix().'flip_tournament_groups', $post, $id))
			return true;
		else
			return false;
	}

	function frameeditcoin($get) {
		global $User;
		$User->requireRight(TournamentAdminright());
		ArrayWithKeys($get, array('xid'));

		$this->Caption = text_translate('Kostengruppe bearbeiten');

		$this->tpl['coin'] = array ();
		if (!empty ($get['cid']))
			$this->tpl['coin'] = MysqlReadRowByID(TblPrefix().'flip_tournament_coins', $get['cid']);

		return $this->tpl;
	}

	function submitcoin($post) {
		global $User;
		$User->requireRight(TournamentAdminright());
		ArrayWithKeys($post, array('id'));

		if (MysqlWriteByID(TblPrefix().'flip_tournament_coins', $post, $post['id']))
			return true;
		else
			return false;
	}

	function _getnextgrouporder() {
		$r = MysqlReadField("SELECT MAX(`order`)+1 AS `next` FROM ".TblPrefix()."flip_tournament_groups", "next");
		if (is_null($r))
			return 1;
		elseif ($r) return $r;
		else
			return false;
	}

	function actiongup($get) {
		global $User;
		$User->requireRight(TournamentAdminright());
		ArrayWithKeys($get, array("id"));

		$id = escape_sqlData($get["id"]);
		$currentorder = MysqlReadFieldByID(TblPrefix()."flip_tournament_groups", "order", $id);
		if (!($lowerid = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_tournament_groups WHERE `order`='$currentorder'-1", "id"))) {
			return MysqlExecQuery("UPDATE ".TblPrefix()."flip_tournament_groups SET `order`='". ($currentorder -1)."' WHERE id='$id'");
		} else {
			MysqlExecQuery("UPDATE ".TblPrefix()."flip_tournament_groups SET `order`='65000' WHERE id='$id'");
			MysqlExecQuery("UPDATE ".TblPrefix()."flip_tournament_groups SET `order`='$currentorder' WHERE id='$lowerid'");
			MysqlExecQuery("UPDATE ".TblPrefix()."flip_tournament_groups SET `order`='". ($currentorder -1)."' WHERE id='$id'");
		}
	}

	function frameeditorgas($get) {
		global $User;
		$User->requireRight(TournamentAdminright());
		$this->Caption = "Orgaverwaltung";
		$this->tpl["orgas"] = MysqlReadArea("SELECT s.id, s.name FROM `".TblPrefix()."flip_user_subject` s, `".TblPrefix()."flip_user_right` r, `".TblPrefix()."flip_user_rights` t WHERE t.controled_id='".GetSubjectID("Turniersubjekt".$this->adm->id)."' AND r.right='tournament_orga' AND s.id=t.owner_id AND r.id=t.right_id");
		return $this->tpl;
	}

	function submitaddorga($post) {
		ArrayWithKeys($post, array("type"));
		if ($post["type"] == "user")
			$u = $post["userident"];
		else
			$u = $post["subject"];

		return $this->adm->addOrga($u);
	}

	function actiondelorga($post) {
		global $User;
		$User->requireRight(TournamentAdminright());
		ArrayWithKeys($post, array("ids"));
		if(is_array($post["ids"])) {
			foreach ($post["ids"] AS $subject_id) {
				_RemRight($subject_id, "tournament_orga", "Turniersubjekt".$this->adm->id);
				$this->adm->_Log("Der Orga $subject_id wurde aus dem Turnier entfernt.");
			}
		}
	}

	function framerem() {
		global $User;
		$User->requireRight(TournamentAdminright());
		$this->Caption = "Turnier ausw&auml;hlen";
		foreach (TournamentGetTournaments(true) AS $tournament) {
			$this->tpl["tournament"][] = array ("tournamentid" => $tournament["id"], "TournamentString" => TournamentGetTournamentString($tournament["id"]));
		}
		// Ausgabe
		return $this->tpl;
	}

	function actiondel($a) {
		global $User;
		$User->requireRight(TournamentAdminright());
		ArrayWithKeys($a, array("id"));
		return $this->adm->Delete($a["id"]);
	}

	function framescreenshots($get) {
		global $User;

		$this->tpl["GroupID"] = $this->adm->ContentGroup;
		$this->tpl["turnierid"] = $this->adm->id;
		// bilder auslesen
		$this->tpl["images"] = MysqlReadArea("
									      SELECT i.name,i.id,i.caption,i.edit_time,i.tn_width,i.tn_height,i.description,
									        i.view_right, i.edit_right, a.name AS author, LENGTH(i.data) AS `size` 
									        FROM `".TblPrefix()."flip_content_image` i
														LEFT JOIN `".TblPrefix()."flip_content_groups` g ON (g.id = i.group_id)
									          LEFT JOIN `".TblPrefix()."flip_user_subject` a ON (i.edit_user_id = a.id)
									        WHERE ((g.name = '".$this->adm->ContentGroup."' AND i.name LIKE '%ID{$this->adm->id}%') AND ".$User->sqlHasRight("i.view_right").")
									        ORDER BY i.name;
														    ");

		$this->tpl["imagecount"] = count($this->tpl["images"]);
		$this->tpl["imagesize"] = 0;
		foreach ($this->tpl["images"] as $k => $v)
			$this->tpl["imagesize"] += $v["size"];

		return $this->tpl;
	}

	function actionDeleteImage($post) {
		if (is_array($post["ids"]))
			foreach ($post["ids"] as $id) {
				$name = escapeHtml(MysqlReadFieldByID(TblPrefix()."flip_content_image", "name", $id));
				if (MysqlDeleteByRight(TblPrefix()."flip_content_image", $id, array ("view_right", "edit_right")))
					LogChange("Der <strong>Screenshot</strong> $name wurde gel&ouml;scht.");
			}
	}


	function frameserver($get) {
		$this->Caption = "Turnierserververwaltung";
		ArrayWithKeys($get, array('game','liga'));

		$this->tpl['liga'] = $get['liga'];
		
		if (!empty ($get['liga'])) $ligaName = $get['liga'];
		                      else $ligaName = "Keine";

		if (!empty ($get['game'])) {

			//original
			//$w = 'game=\''.mysql_escape_string($get['game']).'\'';
			//mod
			$liga = $this->_getLiga($ligaName);
			//$w = 'game=\''.escape_sqlData_without_quotes($get['game']).'\'';
			$w = 'game='.escape_sqlData_without_quotes($get['game']).'';
			//$this->tpl['game'] = $this->adm->liga->getGameString($get['game']);

			// Mod VulkanLAN - Gibt vom kurzen Namen den langen Namen des Turniers und umgekehrt wieder
			$GemeString = $liga->getGameString($get['game']);
			$GemeStringKey = $liga->getGameStringKey($get['game']);

			$w = "game='$GemeStringKey'";
			$this->tpl['game'] = $w;

			$this->tpl['short'] = $get['game'];
		} else {
			$w = '1';
			$this->tpl['game'] = 'alle';
		}

		$servercount = MysqlReadCol("SELECT server_id, COUNT(id) FROM ".TblPrefix()."flip_tournament_serverusage GROUP BY server_id", "COUNT(id)", "server_id");

		$games = array();
		foreach (MysqlReadArea('SELECT * FROM `'.TblPrefix().'flip_tournament_servers` WHERE '.$w.' ORDER BY game, name') AS $aserver) {
			$aserver['count'] = isset($servercount[$aserver['id']]) ? (integer) $servercount[$aserver['id']] : 0;
			$liga = $this->_getLiga($aserver['game']);
			$aserver['game'] = $liga->getGameString($aserver['game']);
			$games[$aserver['game']] = $aserver['game'];
			$this->tpl['server'][] = $aserver;
		}
		
		$this->tpl['games'] = array();
		foreach ($games AS $game=>$gamename)
		{
			//print "Short: $game - Long: $gamename";
			$this->tpl['games'][] = array ('short' => $game, 'long' => $gamename);
		}
		return $this->tpl;
	}

	
	/**
	 * Liefert zu einem Spielk&uuml;rzel die Liga
	 *
	 * @param String $game
	 * @return Object instance of BasicLiga
	 */
	function _getLiga($game) {
		$ligaIdent = preg_split('/_/', $game);
		$ligaIdent = $ligaIdent[0];
		if(!TournamentValidLiga($ligaIdent)) {
			$ligaIdent = 'Keine';
		}
		return TournamentGetLiga($ligaIdent);
	}

	function frameviewserver($get) {
		$this->Caption = "Turnierserver ".MysqlReadFieldByID(TblPrefix()."flip_tournament_servers", "name", $get["serverid"]);
		ArrayWithKeys($get, array("serverid"));

		if (empty ($get["serverid"]))
			return TournamentError("Es wurde kein Turnierserver angegeben!", E_USER_ERROR, __FILE__, __LINE__);
		else
			$serverid = escape_sqlData_without_quotes($get["serverid"]);

		$this->tpl["tournaments"] = array();
		foreach (MysqlReadCol("SELECT t.id FROM ".TblPrefix()."flip_tournament_servers s, ".TblPrefix()."flip_tournament_tournaments t WHERE s.id='$serverid' AND t.game=s.game", "id") AS $aturnierid)
			$this->tpl["tournaments"][$aturnierid] = TournamentGetTournamentString($aturnierid);
		$this->tpl["id"] = $this->adm->id; //tournament_id

		$this->tpl["matches"] = array();
		foreach (MysqlReadArea("SELECT m.* FROM ".TblPrefix()."flip_tournament_matches m, ".TblPrefix()."flip_tournament_serverusage u WHERE u.server_id='$serverid' AND m.id=u.match_id ORDER BY m.endtime") AS $servermatch) {
			$this->tpl["matches"][] = array ("endtime" => $servermatch["endtime"], "tournamentid" => $servermatch["tournament_id"], "tournament" => TournamentGetTournamentString($servermatch["tournament_id"]), "round" => TournamentLvl2txt($servermatch["level"], $servermatch["tournament_id"]), "team1" => TournamentCombatantID2Name($servermatch["team1"]), "vs" => ($servermatch["points1"] != "") ? "<span style=\"color:green;\">".$servermatch["points1"].":".$servermatch["points2"]."</span>" : "<span style=\"color:red;\">offen</span>", "team2" => TournamentCombatantID2Name($servermatch["team2"]),);
		}
		return $this->tpl;
	}
	
	function frameserverliga($get) {
		unset($get['frame']);
		$this->tpl['hiddens'] = $get;
		$this->tpl['ligen'] = TournamentLigen();
		return $this->tpl;
	}

	function frameeditserver($get) {
		ArrayWithKeys($get, array('liga','serverid'));
		$serverdata = null;
		if(is_posDigit($get['serverid'])) {
			$serverdata = MysqlReadRowByID(TblPrefix().'flip_tournament_servers', $get['serverid']);
		}
		if(is_array($serverdata)) {
			$liga = $this->_getLiga($serverdata['game']);
		} elseif(empty($get['liga'])) {
			Redirect(EditURL(array('frame'=>'serverliga'),false,false));
		} else {
			$liga = TournamentGetLiga($get['liga']);
		}
		if(!is_object($liga)) {
			trigger_error_text('Ung&uuml;ltige Liga!|liga='.$get['liga'], E_USER_ERROR);
			return false;
		}
		$this->tpl['liga'] = $liga->name();
		$this->tpl['games'] = $liga->getGameStringArray();
		if (empty ($get['serverid'])) {
			$this->Caption = 'Server anlegen';
		} else {
			$this->Caption = 'Server bearbeiten';
			$this->tpl = array_merge($this->tpl, $serverdata);
			$this->tpl['serverid'] = $this->tpl['id'];
		}
		return $this->tpl;
	}

	function submittosetserver($post) {
		$this->NextPage = "turnieradm.php?frame=setserver&serverid=".$post["serverid"]."&id=".$post["id"];
		$this->SubmitMessage = "";
		return true;
	}

	function submitserver($post) {
		ArrayWithKeys($post, array("serverid","name"));
		$serverid = $post["serverid"];
		unset ($post["serverid"]);
		$savedid = MysqlWriteByID(TblPrefix()."flip_tournament_servers", $post, $serverid);
		if ($serverid != $savedid)
			trigger_error_text("neuer Server \"".$post["name"]."\" wurde gespeichert.");
		else
			return true;
	}

	function actiondelserver($post) {
		ArrayWithKeys($post, array("ids"));
		if(is_array($post["ids"]))
			if (MysqlWrite("DELETE FROM ".TblPrefix()."flip_tournament_servers WHERE id IN (".implode_sqlIn($post["ids"]).")"))
				if (MysqlWrite("DELETE FROM ".TblPrefix()."flip_tournament_serverusage WHERE server_id IN (".implode_sqlIn($post["ids"]).")"))
					trigger_error_text("Daten wurden gel&ouml;scht.");
	}

	function frameserverplan() {
		$this->Caption = "Turnierservereinsatzplan";

		$serverdata = MysqlReadArea("SELECT m.*, s.id AS serverid, s.name AS server, s.ip
														                                 FROM ".TblPrefix()."flip_tournament_matches m, ".TblPrefix()."flip_tournament_serverusage u, ".TblPrefix()."flip_tournament_servers s
														                                 WHERE m.id=u.match_id
														                                   AND s.id=u.server_id
														                                 ORDER BY m.endtime");
		foreach ($serverdata AS $servermatch) {
			$this->tpl["matches"][] = array ("endtime" => $servermatch["endtime"], "serverid" => $servermatch["serverid"], "server" => $servermatch["server"], "ip" => $servermatch["ip"], "tournamentid" => $servermatch["tournament_id"], "tournament" => TournamentGetTournamentString($servermatch["tournament_id"]), "round" => TournamentLvl2txt($servermatch["level"], $servermatch["tournament_id"]), "team1" => TournamentCombatantID2Name($servermatch["team1"]), "vs" => ($servermatch["points1"] != "") ? "<span style=\"color:green;\">".$servermatch["points1"].":".$servermatch["points2"]."</span>" : "<span style=\"color:red;\">offen</span>", "team2" => TournamentCombatantID2Name($servermatch["team2"]),);
		}
		foreach ($this->adm->GetFreeServers() AS $server) {
			$liga = $this->_getLiga($server['game']);
			$server['game'] = $liga->getGameString($server['game']);
			$this->tpl['servers'][] = $server;
		}

		return $this->tpl;
	}

	function framesetserver($get) {
		$getservers = $getmatches = false;
		$this->tpl["serverinputtype"] = "dropdown";
		$this->tpl["matchinputtype"] = "dropdown";
		if (isset ($get["matchid"])) {
			$this->tpl["matchid"] = $matchid = escape_sqlData($get["matchid"]);
			$this->tpl["matchinputtype"] = "hidden";
			$teams = MysqlReadRow("SELECT team1, team2 FROM ".TblPrefix()."flip_tournament_matches WHERE id='$matchid'");
			$this->Caption = "Server f&uuml;r ".TournamentCombatantID2Name($teams["team1"])." vs. ".TournamentCombatantID2Name($teams["team2"]);
			$game = MysqlReadField("SELECT t.game FROM ".TblPrefix()."flip_tournament_matches m, ".TblPrefix()."flip_tournament_tournaments t WHERE m.tournament_id=t.id AND m.id='".$matchid."'", "game");
			$this->tpl["serverid"] = MysqlReadField("SELECT server_id FROM ".TblPrefix()."flip_tournament_serverusage WHERE match_id='$matchid'", "server_id", true);
			$getservers = true;
		}
		elseif (isset ($get["serverid"])) {
			$this->tpl["serverid"] = $serverid = escape_sqlData($get["serverid"]);
			$this->tpl["serverinputtype"] = "hidden";
			$this->Caption = "Match f&uuml;r Server \"".escapeHtml(MysqlReadFieldByID(TblPrefix()."flip_tournament_servers", "name", $serverid))."\" ausw&auml;hlen";
			$this->tpl["matchid"] = MysqlReadField("SELECT match_id FROM ".TblPrefix()."flip_tournament_serverusage WHERE server_id='$serverid'", "match_id", true);
			$getmatches = true;
		} else {
			if (empty ($this->adm->game))
				return TournamentError("So nicht!<br />\nWenn ein Link hierher gef&uuml;hrt hat ist etwas falsch, ansonsten: Lass es!|Turnier->Game fehlt.", E_USER_ERROR, __FILE__, __LINE__);
			$game = $this->adm->game;
			$getmatches = true;
			$getservers = true;
		}

		$this->tpl["matches"] = array ();
		if ($getmatches) {
			if (!empty ($this->adm->id)) {
				foreach ($this->adm->GetMatches() AS $amatch)
					if ($amatch["points1"] == "" && $amatch["points2"] == "" && !MysqlReadField("SELECT id FROM ".TblPrefix()."flip_tournament_serverusage  WHERE match_id='".$amatch["id"]."'", "id", true))
						$this->tpl["matches"][$amatch["id"]] = TournamentLvl2txt($amatch["level"], $amatch["tournament_id"]).": ".TournamentCombatantID2Name($amatch["team1"])." vs. ".TournamentCombatantID2Name($amatch["team2"]);
			} else {
				return TournamentError("So nicht!<br />\nWenn ein Link hierher gef&uuml;hrt hat ist etwas falsch, ansonsten: Lass es!|TurnierID fehlt.", E_USER_ERROR, __FILE__, __LINE__);
			}
		}

		if ($getservers) {
			$this->tpl["servers"] = array_merge(array ("0" => "<keiner>"), MysqlReadCol("SELECT id, name
																					                                                        FROM ".TblPrefix()."flip_tournament_servers
																					                                                        WHERE game='$game'
																					                                                       ", "name", "id"));
			$freeservers = $this->adm->GetFreeServers();
			foreach ($this->tpl["servers"] AS $id => $name)
				if (isset ($freeservers[$id]))
					$this->tpl["servers"][$id] .= " (free)";
		}

		return $this->tpl;
	}

	function submitsetserver($post) {
		$id = MysqlReadField("SELECT id FROM ".TblPrefix()."flip_tournament_serverusage WHERE match_id='".$post["matchid"]."'", "id", true);
		return MysqlWriteByID(TblPrefix()."flip_tournament_serverusage", array ("server_id" => $post["serverid"], "match_id" => $post["matchid"]), $id);
	}

	function frameexport($get) {
		global $User;
		$User->requireRight(TournamentAdminright());
		ArrayWithKeys($get, array("type"));

		//type of export
		if (empty ($get["type"])) {
			$mostliga = MysqlReadRow("SELECT liga, COUNT(liga) AS ligacount FROM ".TblPrefix()."flip_tournament_tournaments GROUP BY liga ORDER BY ligacount DESC");
			if (!empty ($mostliga["liga"]))
				$exporttype = $mostliga["liga"];
			else
				$exporttype = "HTML";
		} else {
			$exporttype = $get["type"];
		}
		$this->tpl["exporttype"] = $exporttype;

		//menu
		$this->tpl["exporttypes"] = array ("HTML", "NGL", "WWCL", "CSV");
		//data for exportform
		switch ($exporttype) {
			case "CSV":
				break;
				
			case "HTML" :
				$this->tpl["firstn"] = array (1 => "1", 3 => "3", 5 => "5", 10 => "10", 10000 => "alle");
				$this->tpl["outputs"] = array ("html" => "HTML (body)", "htmlcode" => "HTML Code", "htmlfile" => "HTML Datei");
				break;

			case "NGL" :
				$this->tpl["countries"] = array ("de" => "Deutschland", "at" => "&Ouml;sterreich", "ch" => "Schweiz");
				$this->tpl["party_datum"] = ConfigGet($this->partydateconfig);
			case "WWCL" :
			default :
				$i = 0;
				foreach (TournamentGetTournaments() AS $tournament) {
					if ($tournament["status"] != "end" || $tournament["liga"] != $exporttype)
						continue;

					$this->tpl["turniere"][$i]["tournamentid"] = $tournament["id"];
					$this->tpl["turniere"][$i]["tournamenttitle"] = TournamentGetTournamentString($tournament["id"]);
					$i ++;
				}
				if (empty ($this->tpl["turniere"]))
					TournamentError("Es sind keine beendeten Turniere vorhanden von denen ein Export erstellt werden kann.", E_USER_NOTICE, __FILE__, __LINE__);
		}

		return $this->tpl;
	}

	function frameresultexport($get, $post) {
		$this->ShowMenu = false;
		$this->tpl["title"] = ConfigGet("page_title");
		$this->tpl["caption"] = "Platzierungen der ".ConfigGet($this->partynameconfig);

		$this->tpl["partydate"] = date("d.m.Y", ConfigGet($this->partydateconfig));

		foreach (TournamentGetTournaments() AS $t) {
			$results = $this->adm->GetResults($t["id"]);
			$ranking = array ();
			for ($i = 1; $i <= $post["firstn"]; $i ++) {
				$current = array_shift($results);
				if (empty ($current))
					break;
				$ranking[] = array ("rank" => $current["rank"], "teamname" => TournamentCombatantID2Name($current["combatant_id"]));
			}
			
			$this->tpl["tournaments"][] = array ("gamename" => TableGetDisplay($gamesTable, $t["game"], $Vars), 
												 "title" => TournamentGetTournamentString($t["id"]), 
												 "ranking" => $ranking);
		}
		
		uasort($this->tpl["tournaments"], "sortbygamename");
		$this->tpl["flipversion"] = FLIP_VERSION_LONG;

		/* Ausgabe */
		switch ($post["outputtype"]) {
			case "htmlcode" :
			case "htmlfile" :
				//hack for PHP5 and compression (inc.page.php,Page::_printTemplate->DoCompress)
				if (version_compare(PHP_VERSION, "5.0") >= 0) {
					global $User;
					$originalconfig = $User->GetConfig("page_compress_output");
					$User->SetProperty("page_compress_output", "N");
				}

				$tpl = new Template("inc.tournament.tpl", $this->tpl, $this->TplSub);
				$page = new Page();
				ob_start();
				$this->_printTemplate($tpl);
				$output = ob_get_contents();
				ob_end_clean();

				if (version_compare(PHP_VERSION, "5.0") >= 0)
					$User->SetProperty("page_compress_output", $originalconfig);

				$this->TplFile = "text.tpl";
				$this->TplSub = "text";
				if ($post["outputtype"] == "htmlcode") {
					return array ("text" => "<pre>".escapeHtml($output)."</pre>");
				} else {
					$this->_ShowFrame = false;
					$this->NextPage = false;
					//Output
					header("Content-Type: text/html");
					header("Content-Disposition: attachment; filename=ranking.html");
					echo "<html>\n";
					echo "<head>\n";
					echo "  <title>".$this->tpl["caption"]."</title>\n";
					echo "</head>\n";
					echo "<body>\n";
					echo $output;
					echo "</body>\n";
					echo "</html>\n";
					flush();
				}
				break;
			case "html" :
			default :
				$this->TplFile = "inc.tournament.tpl";
				return $this->tpl;
		}
	}

	//Stand von: wwcl_ergebnis_submit_v5.pdf (v7.0/06-09-2004)
	function submitWWCLexport($a) {
		$flip_wwcl_export_version = "2.3"; // !!! (1 = TDF-Format, 2.0 = XML, 2.1 = Save File, 2.2 = mod.xmlgen, 2.3 = Liga-Objekt)

		global $User;
		$User->requireRight(TournamentAdminright());

		// Liga-Objekt
		require_once('mod/mod.tournament.liga.php');
		$wwcl = new TournamentLigaWWCL();
		
		//Daten prüfen
		if (!is_a($wwcl, 'BasicLiga')) {
			trigger_error('Kein Liga-Objekt für WWCL!', E_USER_ERROR, __FILE__,__LINE__);
			return false;
		}
		if (empty ($a["party_id"]) OR empty ($a["party_vdid"]))
			return TournamentError("Es wurden nicht alle Daten angegeben.", E_USER_WARNING, __FILE__, __LINE__);
		if (empty ($a["tournaments"]))
			return TournamentError("Es wurden keine Turniere ausgewählt. Wenn keine Turniere aufgelistet werden kann es sein, dass die Turniere noch nicht beendet wurden oder es keine WWCL-Turniere gibt.", E_USER_WARNING, __FILE__, __LINE__);
		
		//XML-Datei einlesen um Spiele zu aktualisieren
		$wwcl->updateGames();
		
		require_once ($this->xmlmod);
		$xmldoc = new XMLDocument("wwcl");

		//<submit>
		$submitdata = array ("tool" => "FLIP-WWCL-Export $flip_wwcl_export_version (www.flipdev.org)", "timestamp" => time(), "party_name" => ConfigGet($this->partynameconfig), "pid" => $a["party_id"], "pvdid" => $a["party_vdid"], "stadt" => ((empty ($a["party_ort"])) ? "-" : $a["party_ort"]));
		$xmldoc->addTwoDimArrayContent("submit", array ($submitdata));

		//<tmpplayer>
		//read data
		$tmpplayer_ids = MysqlReadCol("SELECT d.val, d.subject_id FROM ".TblPrefix()."flip_user_column c, ".TblPrefix()."flip_user_data d, ".TblPrefix()."flip_tournament_combatant t
														                                 WHERE t.tournament_id IN (".implode_sqlIn($a["tournaments"]).")
														                                   AND d.subject_id = t.team_id
														                                   AND d.column_id = c.id
														                                   AND c.type = 'turnierteam'
														                                   AND c.name = 'tournament_wwcl_id'
														                                   AND (d.val LIKE 'PT%' OR d.val LIKE 'CT%')
														                                ", "val", "subject_id");
		$tmpplayer_emails = MysqlReadCol("SELECT d.val, d.subject_id FROM ".TblPrefix()."flip_user_column c, ".TblPrefix()."flip_user_data d
														                                 WHERE c.name = 'wwcl_email'
														                                   AND c.type = 'turnierteam'
														                                   AND d.column_id = c.id
														                                   AND d.subject_id IN (".implode_sqlIn(array_keys($tmpplayer_ids)).")
														                                ", "val", "subject_id");
		$tmpplayer_names = MysqlReadCol("SELECT d.val, d.subject_id FROM ".TblPrefix()."flip_user_column c, ".TblPrefix()."flip_user_data d
														                                 WHERE c.name = 'teamname'
														                                   AND c.type = 'turnierteam'
														                                   AND d.column_id = c.id
														                                   AND d.subject_id IN (".implode_sqlIn(array_keys($tmpplayer_ids)).")
														                                ", "val", "subject_id");
		//format data
		$ct = $pt = 1;
		$tmpplayerdata = array();
		foreach ($tmpplayer_ids AS $subject_id => $wwclid) {
			if (strpos($wwclid, "PT") !== false)
				$wwclid = "PT".$pt ++;
			elseif (strpos($wwclid, "CT") !== false)
				$wwclid = "CT".$ct ++;
			$tmpplayerdata[$subject_id] = array ("tmpid" => $wwclid, "name" => $tmpplayer_names[$subject_id], "email" => $tmpplayer_emails[$subject_id]);
		}
		$tmpplayertag = new XMLTag("tmpplayer");
		$tmpplayertag->addTwoDimArrayContent("data", $tmpplayerdata);
		$xmldoc->addTag($tmpplayertag);

		//<tourney>
		//selected tournaments
		foreach ($a["tournaments"] AS $tournament_id) {
			//get tournamentdata
			$t = new Turniersystem($tournament_id);
			if ($t->liga->dbAndClassName != $wwcl->dbAndClassName) {
				TournamentError("Das ".TournamentGetTournamentString($tournament_id)."-Turnier wurde nicht als WWCL-Turnier angelegt!<br />\nDeshalb sind keine WWCL-Daten vorhanden und es kann kein Export erstellt werden. Es sollte nicht einfach der Typ des Turnieres ge&auml;ndert werden, da keine Spielerdaten (WWCL-IDs) vorhanden sind!", E_USER_WARNING, __FILE__, __LINE__);
				continue;
			}

			$teamids = array ();
			$teams = $t->GetCombatants();
			foreach ($teams AS $key => $team)
				$teamids[] = $team["team_id"];
			$wwclids = MysqlReadCol("SELECT d.val, d.subject_id FROM ".TblPrefix()."flip_user_column c, ".TblPrefix()."flip_user_data d
		                               WHERE c.name='tournament_wwcl_id'
		                                 AND c.type='turnierteam'
		                                 AND d.column_id=c.id
		                                 AND d.subject_id IN (".implode_sqlIn($teamids).")
		                              ", "val", "subject_id");
			
			$name = $t->GetGameString();
			if(!preg_match("/^wwcl_([\d]+)$/", $t->game, $gid))
				return TournamentError("Schwerer Fehler: Der Name des '".escapeHtml($name)."' Turnieres wurde nicht in der vorliegenden \"gamesini.xml\" der WWCL gefunden! Ohne g&uuml;ltige GID kann kein Export erzeugt werden.|game=".$t->game, E_USER_ERROR, __FILE__, __LINE__);
			$gid = $gid[1];
			$max = pow(2, ceil(log(count($teams)) / log(2)));
			$mode = ($t->looser == "Y") ? "D" : "S";

			//matches
			$wlevel = 0;
			$llevel = 0;
			$first = true;
			$oldlevel = -1;
			$looser = false;
			$roundtags = array ();
			$roundtag = null; //damit Eclipse gl&uuml;cklich ist ;)
			$matchdata = array ();
			foreach ($t->GetMatches() AS $match) {
				if ($match["level"] >= TournamentGroupmatchestartlevel())
					return TournamentError("Das ".TournamentGetTournamentString($tournament_id)."-Turnier wurde im Gruppenmodus gespielt. Hierf&uuml;r kann kein Export erzeugt werden!", E_USER_ERROR, __FILE__, __LINE__);

				if ($oldlevel != $match["level"]) {
					//Check what bracket and increment that bracket (winner/loser)
					if (!$this->adm->isLoserBracket($match["level"]) && !$first) {
						$looser = false;
						$wlevel ++;
					}
					elseif (!$first) {
						$looser = true;
						$llevel ++;
					}
					$oldlevel = $match["level"];

					if ($first) {
						$first = false;
					} else {
						$roundtag->addTwoDimArrayContent("match", $matchdata);
						$roundtags[] = $roundtag;
						$matchdata = array ();
					}

					//<winner/looserX>
					if ($looser)
						$round = "looser$llevel";
					else
						$round = "winner$wlevel";
					$roundtag = new XMLTag($round);
				}

				//Ergebnis
				if ($match["points1"] > $match["points2"]) {
					$winnerno = 1;
					$looserno = 2;
				} else {
					$winnerno = 2;
					$looserno = 1;
				}
				if (isset ($tmpplayerdata[$match["team$winnerno"]]))
					$winnerid = $tmpplayerdata[$match["team$winnerno"]]["tmpid"];
				else
					$winnerid = $wwclids[$match["team$winnerno"]];
				if (empty ($winnerid))
					$winnerid = "F";
				if (isset ($tmpplayerdata[$match["team$looserno"]]))
					$looserid = $tmpplayerdata[$match["team$looserno"]]["tmpid"];
				else
					$looserid = $wwclids[$match["team$looserno"]];
				if (empty ($looserid))
					$looserid = "F";
				$matchdata[] = array ("win" => $winnerid, "loose" => $looserid);
			}
			$roundtag->addTwoDimArrayContent("match", $matchdata);
			$roundtags[] = $roundtag;

			//data for <tourney>
			$tourneydata[] = array_merge(array ("name" => $name, "gid" => $gid, "maxplayer" => $max, "mode" => $mode), $roundtags);
		}
		$xmldoc->addTwoDimArrayContent("tourney", $tourneydata);

		$this->_ShowFrame = false;
		$this->NextPage = false;
		$xmldoc->save("wwclexport.xml");

		return true;
	}


	function submitNGLexport($a) {
		global $User;
		$User->requireRight(TournamentAdminright());
		//Daten pr&uuml;fen
		if (empty ($a["party_id"]) OR empty ($a["party_datum"]) OR empty ($a["party_mail"]))
			return TournamentError("Es wurden nicht alle Daten angegeben.", E_USER_WARNING, __FILE__, __LINE__);
		if (empty ($a["tournaments"]))
			return TournamentError("Es wurden keine Turniere ausgew&auml;hlt. Wenn keine Turniere aufgelistet werden kann es sein, dass die Turniere noch nicht beendet wurden oder es keine NGL-Turniere gibt.", E_USER_WARNING, __FILE__, __LINE__);

		//Zeitlimit hochsetzen
		settimelimit(count($a["tournaments"]) * 30); //30 Sekunden je Turnier
		
		//XML-Datei einlesen um Spiele zu aktualisieren
		$this->adm->readNGLFile();
		
		require_once ($this->xmlmod);
		$xmldoc = new XMLDocument(array ("export", array ("version" => "1.4")));
		$xmldoc->encoding = "ISO-8859-15";

		//<laninfo>
		$laninfo = array ("eventid" => $a["party_id"], "name" => ConfigGet($this->partynameconfig), "country" => $a["party_country"], "date" => date("Y-m-d", $a["party_datum"]), "contact" => $a["party_mail"]);
		$xmldoc->addTwoDimArrayContent("laninfo", array ($laninfo));

		//<game>
		foreach ($a["tournaments"] AS $tournamentid) {
			$tournamentdata = TournamentGetTournament($tournamentid);
			if ($tournamentdata["liga"] != "NGL")
				return TournamentError("Das ".TournamentGetTournamentString($tournamentid)."-Turnier wurde nicht als NGL-Turnier angelegt. Deshalb sind keine NGL-Daten vorhanden und es kann kein Export erstellt werden!", E_USER_ERROR, __FILE__, __LINE__);

			//<gameinfo>
			$gametype = $this->_nglGameCheck($tournamentdata);
			$gamedata = array (array ("type" => $gametype, "mode" => ($tournamentdata["looser"] == "Y") ? "DE" : "SE"));
			$gametag = new XMLTag("game");
			$gametag->addTwoDimArrayContent("gameinfo", $gamedata);

			$ranks = MysqlReadCol("SELECT rank, combatant_id FROM ".TblPrefix()."flip_tournament_ranking WHERE tournament_id='".$tournamentdata["id"]."'", "rank", "combatant_id");
			$teamstag = new XMLTag("teams");
			foreach (TournamentGetCombatants($tournamentdata["id"]) AS $teamid => $ateam) {
				$team = CreateSubjectInstance($teamid, "turnierteam");

				//<members>
				$players = array ();
				foreach ($team->getChilds() AS $uid => $uname) {
					$u = CreateSubjectInstance($uid, "user");
					$players[] = array ("nglid" => ($this->adm->nglcheck($u->getProperty("tournament_ngl_player_id"), false)) ? $u->getProperty("tournament_ngl_player_id") : "0", "nickname" => $uname, "email" => $u->email, "firstname" => $u->getProperty("givenname"), "lastname" => $u->getProperty("familyname"), "leader" => ($team->ngl_email == $u->email) ? "yes" : "");
				}
				$memberstag = new XMLTag("members");
				$memberstag->addTwoDimArrayContent("player", $players);

				//<teams>
				$teamdata = array (array ("place" => $ranks[$team->id], "nglid" => ($this->adm->nglcheck($team->getProperty("tournament_ngl_id"), false)) ? $team->getProperty("tournament_ngl_id") : "0", "name" => $team->name, "tmpid" => $team->id, $memberstag));
				$teamstag->addTwoDimArrayContent("team", $teamdata);
			}
			$gametag->addTag($teamstag);
			//<matches>
			$matchestag = new XMLTag("matches");
			$wbcount = 0;
			$lbcount = 0;
			$oldlevel = -1;
			$matchdata = array ();
			foreach ($this->adm->getMatches(false, $tournamentid) AS $match) {
				if ($match["level"] >= $this->adm->groupmatchestartlevel)
					return TournamentError("Das ".TournamentGetTournamentString($tournamentid)."-Turnier wurde im Gruppenmodus gespielt. Hierf&uuml;r kann kein Export erzeugt werden!", E_USER_ERROR, __FILE__, __LINE__);

				//<round>
				if ($match["level"] != $oldlevel) {
					$oldlevel = $match["level"];
					if ($this->adm->isLoserBracket($match["level"])) {
						$bracket = "LB";
						$lbcount ++;
						$bracketid = $lbcount;
					} else {
						$bracket = "WB";
						$wbcount ++;
						$bracketid = $wbcount;
					}
					if (isset ($roundtag)) {
						$roundtag->addTwoDimArrayContent("match", $matchdata);
						$matchestag->addTag($roundtag);
					}
					$roundtag = new XMLTag("round", array ($bracket => $bracketid));
					$matchdata = array ();
				}

				//<match>
				$matchdata[] = array ("tmpid1" => $match["team1"], "tmpid2" => $match["team2"], "score1" => $match["points1"], "score2" => $match["points2"], "winner" => ($match["points1"] > $match["points2"]) ? $match["team1"] : $match["team2"]);
			}
			$roundtag->addTwoDimArrayContent("match", $matchdata);
			$matchestag->addTag($roundtag);
			unset ($roundtag); //empty for next loop
			$gametag->addTag($matchestag);
			$xmldoc->addTag($gametag);
		}

		$this->_ShowFrame = false;
		$this->NextPage = false;
		$xmldoc->save("nglexport.xml");

		return true;
	}
	
	/**
	 * Pr&uuml;ft ob ein Spielk&uuml;rzel ein NGL-k&uuml;rzel ist
	 * @access private
	 */
	function _nglGameCheck($tournamentdata) {
			if (!preg_match("/^ngl_(.+)$/", $tournamentdata["game"], $gametype))
				return TournamentError("Das \"".TournamentGetGameString($tournamentdata["game"])."\"-Turnier wird laut der NGLGames.xml nicht von der NGL unterst&uuml;tzt!|".$tournamentdata["game"]." ".$tournamentdata["teamsize"]."on".$tournamentdata["teamsize"], E_USER_ERROR, __FILE__, __LINE__);
			$gametype = $gametype[1];
			$nglTeamsize = explode("_", $gametype);
			$nglTeamsize = array_pop($nglTeamsize);
			if($nglTeamsize != $tournamentdata["teamsize"])
				return TournamentError("Das \"".TournamentGetGameString($tournamentdata["game"])."\"-Turnier wird laut der NGLGames.xml als {$nglTeamsize}on{$nglTeamsize} und nicht als ".$tournamentdata["teamsize"]."on".$tournamentdata["teamsize"]." durchgef&uuml;hrt!", E_USER_ERROR, __FILE__, __LINE__);
			return $gametype;
	}
	
	/**
	 * alle Turnierdaten als CSV exportieren
	 */
	function submitCSVexport($post) {
		foreach(TournamentGetTournamentDetails() AS $groupkey=>$tournamentgroup)
			foreach($tournamentgroup AS $key=>$aTournament) {
				//keine Bilder in der CSV
				unset($aTournament["icon"]);
				unset($aTournament["icon_small"]);
				$tournaments[$groupkey][$key] = $aTournament;
			}
		return $this->_saveCSVdata($tournaments, "tournaments.csv");
	}
	
	/**
	 * Rankings formatiert als CSV exportieren
	 */
	function submitCSVranking($post) {
		$rankings = $head = array();
		$i = 0;
		foreach(TournamentGetTournaments() As $aTournament) {
			foreach($this->adm->GetResults($aTournament["id"]) As $aTournamentRanking) {
				$team = CreateSubjectInstance($aTournamentRanking["combatant_id"]);
				$rankings[$i][] = array("id"		=> $aTournamentRanking["id"],
									"tournament_id"	=> $aTournamentRanking["tournament_id"],
									"tournament_name" => TournamentGetTournamentString($aTournamentRanking["tournament_id"]),
									"team_id"		=> $team->id,
									"team_name"		=> $team->name,
									"rank"			=> $aTournamentRanking["rank"],
									"team_members"	=> implode(",", $team->getChilds("user")),
									"member_emails" => implode(",", MysqlReadCol("SELECT email FROM `".TblPrefix()."flip_user_subject` WHERE id IN (".implode_sqlIn(array_keys($team->getChilds())).")"))
									);
			}
			$i++;
		}
		
		return $this->_saveCSVdata($rankings, "rankings.csv");
	}
	
	function _saveCSVdata($data, $filename = "data.csv") {
		global $User;
		$User->requireRight(TournamentAdminright());
		
		$items = $head = array();
		foreach($data AS $aItemlist)
			$items = array_merge($aItemlist, $items);
		
		//die Schl&uuml;ssel des ersten Elements der letzten Liste als Spaltenbeschriftung nutzen 
		foreach($aItemlist[0] AS $key=>$value)
			$head[] = $key;
		
		$this->_ShowFrame = false;
		$this->NextPage = false;
		return saveCSV($filename, $items, $head);
	}

	function framedoku() {
		$this->Caption = ""; //ist im Template
		return array ("currency" => $this->adm->currency);
	}

	function gotoTree() {
		Redirect("tournament.php?frame=tree&id=".$this->adm->id);
	}

	function frameoverlap($get) {
		global $User;
		$User->requireRight(TournamentAdminright());
		$tournamentid = $get['id'];
		$active_only = (($get['inactive']) ? 0:1);
		$filter = $get['filter'];
		$unfilter = $get['unfilter'];
		
		// Filter Liste ermitteln und erstellen
		$exclude_tournaments = array();
		
		if(is_array($get["ids"])) {
			foreach($get["ids"] AS $key=>$val) 
			$get[$key] = escape_sqlData($val);
			$exclude_tournaments = $get["ids"];
		}
		
		if (($tournamentid != 0) and ($tournamentid != NULL)) {

			$this->tpl['id'] = $tournamentid;
			// get all players registered for selected tournament
			$sql = "SELECT t.id as tid, t.game as game, t.status as status, g.child_id as uid, t.start as start
							FROM flip_tournament_tournaments t
							LEFT JOIN flip_tournament_combatant c ON (c.tournament_id = t.id)
							LEFT JOIN flip_user_groups g ON (g.parent_id = c.team_id)
							WHERE t.status NOT IN ('closed', 'end') AND t.id = $tournamentid
							ORDER BY g.child_id, t.id";
			$RowsTournament = MysqlReadArea($sql);
		}
		else {			
			trigger_error_text("Turnier nicht angegeben!", E_USER_WARNING);
		}

		$userseats = MysqlReadArea("
			SELECT s.*,u.name AS nickname FROM (".TblPrefix()."flip_seats_seats s)
			LEFT JOIN ".TblPrefix()."flip_user_subject u ON (s.user_id = u.id)			
			ORDER BY s.id;
		  ");
		
		// Teamnamen ermitteln
		$selected_tournament = new Turniersystem($tournamentid);
		$users_team = array();
		$Combatants = $selected_tournament->GetCombatants();
		$teamidarr = array ();
		foreach ($Combatants AS $ateam)
			$teamidarr[] = $ateam["team_id"];
		$teamids = implode_sqlIn($teamidarr);
		if (!empty ($teamidarr)) {

			$users_team = MysqlReadArea("SELECT s.id, s.name, g.parent_id, d.val AS teamname
			FROM ".TblPrefix()."flip_user_groups g, ".TblPrefix()."flip_user_subject s, ".TblPrefix()."flip_user_data d
			WHERE g.child_id = s.id AND g.parent_id IN ($teamids)
			AND g.parent_id = d.subject_id
			ORDER BY g.parent_id, s.name");
		} 
		else {
			$users_team = array ();
		}

		$turniere = [];
		$turnierDetails = TournamentGetTournamentDetails();
		foreach($turnierDetails as $turnierg) {
			foreach($turnierg as $t) {
				$turniere[$t['id']] = $t;				
			}
		}

		$bgcolors = ['ffffff', 'e0e0e0', '53aab8'];
		$users = [];
		$overlaps = [];
		$userssolo = [];
		$usercount = 0;
		$lastUsername = ""; 

		foreach($RowsTournament as $rowid => $r) {
			if(!$r['uid'] || !$r['tid']) {
				continue;
			}			
			$Player = CreateSubjectInstance($r['uid'], 'user');
			$tcount = 0;
			$rankedcount = 0;
			$u = array();

			// Doppelte User überspringen, Mehrfachteilnahme wird in der innersten Schleife berücksichtigt
			if ($lastUsername == $Player->name) { continue; }

			// Seat und Clan ermitteln
			for($l=0; $l < count($userseats); $l++) {
				if ($userseats[$l]["user_id"] == $r['uid']) {
					//$u['seat_id'] = $userseats[$l]["id"];
					$u['block_id'] = $userseats[$l]["block_id"];
					$u['seat_name'] = $userseats[$l]["name"];

					if ($userseats[$l]["user_id_clan"] != NULL) {
						$u['clan_id'] = $userseats[$l]["user_id_clan"];					
						$clan = CreateSubjectInstance($u['clan_id'], "clan");
						$u['clan_name'] = $clan->name; 
					}
					break;
				}
			}

			// Team ermitteln
			for($l=0; $l < count($users_team); $l++) {
				if ($users_team[$l]["id"] == $r['uid']) {
					// Workaround fuer schwaches SQL Statement weil mehrere Eintraege pro User existieren - Tominator
					if (strlen($users_team[$l]["teamname"]) <= 1) continue;

					$u['teamname'] = $users_team[$l]["teamname"];
					$u['teamid'] = $users_team[$l]["parent_id"];
					break;
				}
			}

			if (($tournamentid != 0) and ($tournamentid != NULL)) {
				foreach($turnierDetails as $turnierg) {
					foreach($turnierg as $t) {
						if(!TournamentIsValidID($tournamentid)) {
							TournamentError('Das Turnier existiert nicht!', E_USER_ERROR, __FILE__, __LINE__);
						}

						if ($t['id'] == $tournamentid) {
							continue;
						}

						$turnier = new Turniersystem($t['id']);
						$ranking = $turnier->GetResults();	

						$UserIsPlayer = $turnier->isPlayer($Player->id);

						if ($UserIsPlayer) {
							if ($lastUsername != $Player->name) {								
								$u['username'] = $Player->name; 
								$u['uid'] = $r['uid'];
							}
							else {
								$u['username'] = ""; 
								$u['clan_id'] = "";
								$u['clan_name'] = "";
								$u['block_id'] = "";
								$u['seat_name'] = "";
								$u['teamname'] = "";
								$u['teamid'] = "";
							}
														
							$tcount++;													
							$u['tid'] = $turnier->id;
							$u['game'] = $turniere[$turnier->id]['game'];							
							$u['start'] = date('d.m.Y H:i', $turnier->start);

							$overlaps[$turnier->id]['game'] = $u['game'];
							$overlaps[$turnier->id]['tid'] = $turnier->id;
								

							$IsRanked = false;
							foreach ($ranking AS $rankdata) {
								if ($Player->isChildOf($rankdata["combatant_id"])) {
									$IsRanked = true;									
									break;
								}
							}							
							
							if ($IsRanked) {
								$rankedcount++;
								$overlaps[$turnier->id]['out']++;
								$u['game'] .= " (ausgeschieden)";
							}
							else {
								$overlaps[$turnier->id]['in']++;
							}
							
							if (($IsRanked) and ($active_only)) { ; }
							else {
								// Match Filter
								if (!(in_array($t['id'], $exclude_tournaments))) {
									$usercount++;
									$u['bgcolor'] = $bgcolors[$usercount % 2];	
									$u['bgcolor_tournament'] = ($IsRanked ? $bgcolors[2]:$u['bgcolor']);
									$users[] = $u;
									$lastUsername = $Player->name;
								}
							}

							// Filter fuer Turniere setzen
							$exclude_list = $exclude_tournaments;
							if (!in_array($turnier->id, $exclude_list)) $exclude_list[] = $turnier->id;
							else
								if (($key = array_search($turnier->id, $exclude_list)) !== false) {
									unset($exclude_list[$key]);
							}
							
							$exclude_list_url = "";
							$first = true;
							foreach ($exclude_list AS $id) {
								if (!$first) {
									$exclude_list_url .= "&amp;ids[]=$id";
								} else {
									$exclude_list_url = $id;
									$first = false;
								}
							}
							$overlaps[$turnier->id]["filter_list"] = $exclude_list_url;

							if ($overlaps[$turnier->id]["filter_action"] == "")							
								if (in_array($turnier->id, $exclude_tournaments)) $overlaps[$turnier->id]["filter_action"] = 1;
								else $overlaps[$turnier->id]["filter_action"] = 0;
						}
					}
				}
			}

			// Spieler die nur bei einem Turnier dabei sind
			if (($tcount == 0) && ($rankedcount == 0)) {
				$u['username'] = $Player->name; 
				$u['uid'] = $Player->id;				
				$u['bgcolor'] = $bgcolors[0];
				$userssolo[] = $u;
			}
		}

		$this->tpl['tournamentname'] = $turniere[$tournamentid]['game'];
		$this->tpl['users'] = $users;
		$this->tpl['userssolo'] = $userssolo;
		$this->tpl['overlaps'] = $overlaps;
		$this->tpl['sub'] = 'overlap';
		$this->tpl['inactive'] = (($active_only) ? 0:1);
		if($get['singletournament']) $this->tpl['singletournament'] = 1;
		return $this->tpl;

/*		Code von fronk

		foreach($rows as $rowid => $r) {
			if(!$r['uid'] || !$r['tid']) {
				continue;
			}

			if(!TournamentIsValidID($r['tid'])) {
				continue;
			}

			if($get['multitournament']) {
				// ignore users with only one tournament
				if($lastuser != $r['uid']) {
					$userTcount = 0;
				}

				$next_uid = $rows[$rowid+1]['uid'];
				$u['next_uid'] = $next_uid;
				$u['tcount'] = $userTcount;

				if($next_uid != $r['uid'] && $lastuser != $r['uid']) {
					// if next row is another user
					if($userTcount == 0) {
						continue;
					}
				}
			}

			$user = CreateSubjectInstance($r['uid'], 'user');

			//var_dump($turnier);


			if($lastuser == $r['uid']) {
				$u['username'] = "";
				$userTcount++;
			} else {
				$u['username'] = $user->name;
				$usercount++;
				$u['bgcolor'] = $bgcolors[$usercount % 2];
				//$userTcount = 0;
			}

			$u['uid'] = $r['uid'];
			$u['tid'] = $r['tid'];
			$u['game'] = $turniere[$r['tid']]['game'];
			$u['start'] = date('d.m.Y H:i', $turniere[$r['tid']]['start']);
			$users[] = $u;

			$lastuser = $r['uid'];
			//var_dump ($u);

		}


		foreach($rows as $r) {
			if(!$user[$r['uid']]) {
				$user[$r['uid']]['user'] = CreateSubjectInstance($r['uid'], 'user');
			}
			$user[$r['uid']]['tournaments'][] = [
					'id' => $r['tid'],
					'game' => $r['game'],
					'status' => $r['status'],
					'start' => $r['start']
				];
		}

		//var_dump($get);exit;
		

		$this->tpl['users'] = $users;
		$this->tpl['sub'] = 'overlap';
		return $this->tpl; */
	}

	function framedev($get) {
		$this->tpl['sub'] = 'overlap';
		return $this->tpl;
	}

}
runPage("turnieradmPage");

function sortbygamename($array1, $array2) {
	if ($array1["gamename"] == $array2["gamename"])
		return ($array1["title"] > $array2["title"]) ? 1 : -1;
	return ($array1["gamename"] > $array2["gamename"]) ? 1 : -1;
}
?>
