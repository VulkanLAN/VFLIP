<?php

/**
 * @author Moritz Eysholdt
 * @version $Id: text.php 1389 2007-04-01 18:00:18Z loom $
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once ("core/core.php");
require_once ("inc/inc.page.php");
require_once ("inc/inc.text.php");

class TextPage extends Page {
	var $right_translate = "translate";
	
	function frameDefault($get, $post) {
		return LoadText($get["name"], $this->Caption);
	}
	
	function frametranslate($get) {
		global $User;
		$User->requireRight($this->right_translate);
		
		$this->Caption = 'Translator';
		
		return array(
			'hash' => $get['hash'],
			"gtext" => $get["text"],
			"locale"=> 'en_GB',
			"locales"=> MysqlReadCol("SELECT DISTINCT `locale` FROM `".TblPrefix()."flip_textdata`", "locale", 'locale')
		);
	}
	
	function submittranslation($post) {
		global $User;
		$User->requireRight($this->right_translate);
		if(isset($post["orig"]) && isset($post["text"]) && isset($post["locale"]) && isset($post['hash'])) {
			if($post["locale"] != "de_DE") {
				$rowid = MysqlReadField(
					"SELECT id FROM `".TblPrefix()."flip_textdata` " 
						. "WHERE (`hash` = " . escape_sqlData($post['hash']) . ") " 
						. "OR (`name` = ".escape_sqlData($post["orig"]).")", "id", 
					true
				);
				
				if($rowid) {
					MysqlWriteByID(
						TblPrefix()."flip_textdata", 
						array(
							"text"   => $post["text"], 
							"locale" => $post["locale"], 
							'hash'   => $post['hash']
						), 
						$rowid
					);
				} else {
					MysqlWriteByID(
						TblPrefix()."flip_textdata", 
						array(
							"name"   => $post["orig"], 
							"text"   => $post["text"], 
							"locale" => $post["locale"], 
							'hash'   => $post['hash']
						)
					);
				}
				
				return true;
			}
		}
		return false;
	}
}

RunPage("TextPage");
?>