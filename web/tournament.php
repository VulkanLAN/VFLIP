<?php
/**
 * @author Daniel Raap
 * @version $Id: tournament.php 1702 2019-01-09 09:01:12Z loom $ edit by alra u. naaux
 * @copyright (c) The FLIP Project Team
 * @license COPYING Licensed under the GNU GPL. For full terms see the file COPYING.
 * @package pages
 **/

/** FLIP-Kern */
require_once('core/core.php');
require_once ('inc/inc.page.php');
require_once ('mod/mod.tournament.php');

class TournamentPage extends Page {
	//Module
	var $imagelibary = 'mod/mod.image.php';
	var $messagelibary = 'mod/mod.sendmessage.php';
	//Config
	var $commentconfig = 'tournament_comment_orgaonly';
	//Templates
	var $texttplfile = 'text.tpl';
	var $texttplsub = 'text';
	var $texttplvar = 'text';
	//Rechte
	var $myinfoRight = 'logged_in';
	//(Paket)intern
	var $turnier; //object of class "Turnier" (mod.tournament.php)
	var $tpl; //all the template data which is collected in the methods
	var $maxScreenshotSize = 2097152; // 2MB (in Doku festgehalten!)
	//diverses:
	var $DevelMode = 0;
	/*-viel Subjektkram
	 */

	 //php 7 public function __construct()
	//php 5 original function TournamentPage()
	function __construct() {
		global $User;

		//php 5:
		//$this->Page();
		//php7 neu:
		parent::__construct();
		ArrayWithKeys($this->FrameVars, array('id','frame')); 
		$id = $this->FrameVars['id']; 
		$frame = ($this->FrameVars['frame'] != '') ? strtolower($this->FrameVars['frame']) : 'default'; 
		$this->turnier = TournamentIsValidID($id) ? new Turniersystem($id) : null;
		/** anpassung naaux - check evt. wieder aktivieren || error handling
		* if($this->turnier == null) {
		*	trigger_error_text('Huch, ein Turnier-Objekt ist NULL?!', E_USER_ERROR);
		* } **/
		
		/* &uuml;berschrift des Turnieres */
		if (isset($this->turnier->id)) {
			$this->Caption = $this->turnier->GetTournamentString();
		} else {
			$this->Caption = text_translate('Turniersystem');
		}

		/* Variablen &uuml;berpr&uuml;fen */
		$noidframes = array ('default', 'myinfo', 'coinoverview');
		if ($id == '' && !in_array($frame, $noidframes)) {
			return TournamentError('Es wurde kein Turnier ausgew&auml;hlt.<br/>'."\n", E_USER_WARNING, __FILE__, __LINE__);
		}
		elseif ($this->turnier === null && !in_array($frame, $noidframes)) {
			return TournamentError('Die angegebene TurnierID ist nicht g&uuml;ltig.|ID='.$id, E_USER_ERROR, __FILE__, __LINE__);
		}
		$this->tpl['file'] = basename(__FILE__);
		$this->tpl['curframe'] = $this->Frame;
		$this->tpl['tournamentid'] = $id;
		if ($User->hasRight($this->myinfoRight))
			$this->tpl['logged_in'] = 'true';
	}

	function framedefault($get, $post) {
		global $User;
		$this->Caption = text_translate('Turnierliste');
		
		$this->tpl['turniertypen'] = $this->_sortierteGruppen();

		/* Adminlink */
		if (TournamentIsAdmin()) {
			$this->tpl['adminlinks'][] = array ('frame' => 'add', 'tournamentid' => '0', 'linktext' => 'Turnier hinzuf&uuml;gen');
			$this->tpl['adminlinks'][] = array ('frame' => 'editgroups', 'tournamentid' => '0', 'linktext' => 'Gruppen-/Coinsverwaltung');
			if (!empty ($this->tpl['turniertypen'])) {
				$this->tpl['adminlinks'][] = array ('frame' => 'rem', 'tournamentid' => '0', 'linktext' => 'Turnier entfernen');
				$this->tpl['adminlinks'][] = array ('frame' => 'export', 'tournamentid' => '0', 'linktext' => 'Export');
			}
			$this->tpl['adminlinks'][] = array ('frame' => 'server', 'tournamentid' => '0', 'linktext' => 'Turnierserver verwalten');
			$this->tpl['adminlinks'][] = array ('frame' => 'doku', 'tournamentid' => '0', 'linktext' => 'Dokumentation');
			//$this->tpl['adminlinks'][] = array ('frame' => 'overlap','tournamentid' => '0', 'linktext' => 'Spieler-Turnierzeiten');
			if ($this->DevelMode == 1) $this->tpl['adminlinks'][] = array ('frame' => 'dev','tournamentid' => '0', 'linktext' => 'Dev-Page');
			$tablenameid = MysqlReadField('SELECT `id` FROM `'.TblPrefix().'flip_table_tables` WHERE `name`=\''.escape_sqlData_without_quotes(TournamentTablename_Badnames()).'\'', 'id');
			$this->tpl['tournamentfooter'][] = array ('url' => 'table.php?frame=viewtable&amp;id='.$tablenameid, 'linktext' => 'Verbotene Teamnamen');
		}
		include_once ('mod/mod.config.php');
		if (ConfigCanEdit()) {
			$this->tpl['tournamentfooter'][] = array ('url' => 'config.php?category=tournament', 'linktext' => 'Turniersystem-Config');
		}

		//Coins
		$this->tpl['coins'] = $this->_maxcoins();

		/* Ausgabe */
		return $this->tpl;
	}

	function framedev($get) {
		print "DEV";
		return 1;
	}
	
	/**
	 * Liefert alle Turniere in sortierten Gruppen
	 * @return Array array(order => array(caption, image, turniere))
	 */
	function _sortierteGruppen() {
		/* Gruppieren: */
		$o = 0;
		$turniertypen = array ();
		foreach (TournamentGetTournamentDetails() AS $groupid => $turniere) {
			//sortieren nach Name und Teamgr&ouml;&szlig;e
			$sortedts = array ();
			foreach ($turniere AS $aTurnier)
				$sortedts[$aTurnier['game'].$aTurnier['teamsize'].$aTurnier['id']] = $aTurnier;
			ksort($sortedts);

			$groupdata = $this->_getGroupData($groupid, $o);
			$o ++;
			$turniertypen[$groupdata['order']] = array ('caption' => $groupdata['name'], 'image' => $groupdata['image'], 'turniere' => $sortedts);
		}
		ksort($turniertypen);
		return $turniertypen; 
	}
	
	/**
	 * Liefert zu einer GruppenID die Gruppendaten
	 * @param int $groupid ID einer Turniergruppe
	 * @param int $order SortierIndex falls keiner vorhanden ist
	 * @return Array array(name, order)
	 */
	function _getGroupData($groupid, $order) {
		static $groups = null;
		if($groups === null) $groups = TournamentGetGroups();
		
		if (strpos($groupid, 'liga_') !== false) {
			$groupdata = array ('name' => str_replace('liga_', '', $groupid), 'order' => $order);
		} else {
			$groupdata = $groups[$groupid];
		}
		return $groupdata;
	}
	
	function _maxcoins() {
		return MysqlReadField('SELECT MAX(maxcoins) AS maxcoin FROM `'.TblPrefix().'flip_tournament_coins`', 'maxcoin');
	}

	function framemyinfo() {
		global $User;
		$User->requireRight($this->myinfoRight);
		$this->Caption = text_translate('Meine pers&ouml;nliche Seite');

		if ($this->_maxcoins() > 0) {
			$this->tpl['coins'] = TournamentGetUserCoins($User);
		}

		//Meine Teams
		#only SubjectInfo (without ID):
		#$this->tpl["teams"] = GetSubjects("turnierteam", array(), "s.name IN (".implode_sqlIn($User->getParents("turnierteam")).")");
		#full tournamentinfo (teamid, teamname, count(players), game, tcount(tournaments), tournament(XvsX), tournament_id):
		$this->tpl['teams'] = MysqlReadArea('SELECT g.parent_id AS `id`, d.val AS `teamname`, COUNT(DISTINCT(h.child_id)) AS `count`, COUNT(DISTINCT(t.`id`)) AS `tcount`, t.`id` AS tournament_id, t.`status` AS tstatus
	                                         FROM `'.TblPrefix().'flip_user_data` d, `'.TblPrefix().'flip_user_column` s, `'.TblPrefix().'flip_user_groups` g, `'.TblPrefix().'flip_user_groups` h, `'.TblPrefix().'flip_tournament_tournaments` t, `'.TblPrefix().'flip_tournament_combatant` c
	                                         WHERE s.`name`=\'teamname\' AND s.`type`=\'turnierteam\' AND d.column_id=s.id AND g.`child_id`=\''.$User->id.'\' AND d.subject_id=g.parent_id AND h.parent_id=g.parent_id AND c.team_id=g.parent_id AND t.id=c.tournament_id
	                                         GROUP BY g.parent_id
	                                         ');
		foreach ($this->tpl['teams'] AS $key => $team) {
			//Wenn das Team an nur einem Turnier teilnimmt...
			if ($team['tcount'] == 1) {
				//Link um Team zu editieren (nur wenn Turnier offen ist/Anmeldung)
				if ($team['tstatus'] == 'open') {
					$this->tpl['teams'][$key]['teamname'] = '<a href="'.basename(__FILE__).'?frame=editTeam&amp;id='.$team['tournament_id'].'&amp;newteamid='.$team['id'].'">'.$team['teamname'].'</a>';
				} else {
					$this->tpl['teams'][$key]['teamname'] = escapeHtml($team['teamname']);
				}
				//Titel des Turnieres
				$this->tpl['teams'][$key]['tournament'] = TournamentGetTournamentString($team['tournament_id']);
			}
		}

		//Meine Matches
		$this->tpl['matches'] = MysqlReadArea('SELECT m.* FROM `'.TblPrefix().'flip_tournament_matches` m, `'.TblPrefix().'flip_user_groups` g WHERE g.child_id=\''.$User->id.'\' AND (g.parent_id=m.team1 OR g.parent_id=m.team2) ORDER BY endtime DESC');
		$screenlist = MysqlReadCol('SELECT `name` FROM `'.TblPrefix().'flip_content_image` WHERE `name` LIKE \'%_turnier_ID%\'', 'name', 'name');
		foreach ($this->tpl['matches'] AS $key => $match) {
			if (!empty ($match['team1']) && $User->isChildOf($match['team1']))
				$this->tpl['matches'][$key]['vs'] = TournamentCombatantID2Name($match['team2']);
			else
				$this->tpl['matches'][$key]['vs'] = TournamentCombatantID2Name($match['team1']);
			$t = new Turniersystem($match['tournament_id']);
			if ($t->screenshots == 'Y' && (!empty ($match['points1']) || !empty ($match['points2']))) {
				$screenshotname = $t->game.'_turnier_ID'.$match['tournament_id'].'_Match'.$match['id'].'_1';
				if (!empty ($screenlist[$screenshotname])) // oder in_array($screenshotname, $screenlist)
					$this->tpl['matches'][$key]['status'] = '<a href="'.basename(__FILE__).'?frame=screenshots&amp;id='.$match['tournament_id'].'&amp;matchid='.$match['id'].'">'.$match['points1'].':'.$match['points2'].'</a>';
				else
					$this->tpl['matches'][$key]['status'] = $match['points1'].':'.$match['points2'];
			} else
				$this->tpl['matches'][$key]['status'] = (empty ($match['points1']) && empty ($match['points2'])) ? 'offen' : $match['points1'].':'.$match['points2'];
			$this->tpl['matches'][$key]['level'] = TournamentLvl2txt($match['level'], $t->id);
			$this->tpl['matches'][$key]['tournament'] = TournamentGetTournamentString($match['tournament_id']);
			//Server
			$serverdata = MysqlReadRow('SELECT s.name, s.ip
				FROM `'.TblPrefix().'flip_tournament_servers` s, `'.TblPrefix().'flip_tournament_serverusage` u
				WHERE u.match_id=\''.$match['id'].'\' AND s.id=u.server_id', true);
			$this->tpl['matches'][$key]['server'] = $serverdata['name'].' ('.$serverdata['ip'].')';
		}

		//Meine Turniere
		$alleturniere = TournamentGetTournamentDetails(MysqlReadArea('SELECT t.* FROM `'.TblPrefix().'flip_tournament_tournaments` t, `'.TblPrefix().'flip_tournament_combatant` c, `'.TblPrefix().'flip_user_groups` s WHERE s.child_id=\''.$User->id.'\' AND c.team_id=s.parent_id AND c.tournament_id=t.id'));
		$myranking = MysqlReadCol('SELECT r.tournament_id, r.rank FROM `'.TblPrefix().'flip_tournament_ranking` r, `'.TblPrefix().'flip_user_groups` g WHERE r.combatant_id=g.parent_id AND g.child_id=\''.$User->id.'\'', 'rank', 'tournament_id');
		foreach ($alleturniere AS $groupid => $turniere) {
			foreach ($turniere AS $key => $turnier) {
				$turnier['rank'] = isset($myranking[$turnier['id']]) ? $myranking[$turnier['id']] : null;
				$this->tpl['turniere'][TournamentGetGroupName($groupid)][$key] = $turnier;
			}
		}

		return $this->tpl;
	}

	function framecoinoverview() {
		$this->Caption = 'Kosten&uuml;bersicht der Turniere';
		$this->tpl['coins'] = TournamentGetUserCoins();
		$coindata = TournamentGetCoins();
		foreach (TournamentGetTournamentDetails() AS $groupid => $turniere)
			foreach ($turniere AS $tournament) {
				$this->tpl['groups'][$coindata[$tournament['coin_id']]['currency']]['turniere'][] = array ('id' => $tournament['id'], 'name' => TournamentGetTournamentString($tournament['id']), 'coins' => $tournament['coins'], 'currency' => $coindata[$tournament['coin_id']]['currency']);
			}
		if (empty ($this->tpl['groups']))
			$this->tpl['groups'][]['turniere'] = array ();
		ksort($this->tpl['groups']);
		
		foreach ($this->tpl['groups'] AS $key => $dataarray)
			ksort($this->tpl['groups'][$key]['turniere']);
		$this->tpl['adminright'] = TournamentAdminright();
		return $this->tpl;
	}

	function actionaddcoins($post) {
		global $User;
		$User->requireRight(TournamentAdminright());
		return MysqlWrite('UPDATE '.TblPrefix().'flip_tournament_tournaments SET coins = coins + 1 WHERE id=\''.escape_sqlData_without_quotes($post['id']).'\'');
	}

	function actionremcoins($post) {
		global $User;
		$User->requireRight(TournamentAdminright());
		if (MysqlReadFieldByID(TblPrefix().'flip_tournament_tournaments', 'coins', $post['id']) > 0)
			return MysqlWrite('UPDATE '.TblPrefix().'flip_tournament_tournaments SET coins = coins - 1 WHERE id=\''.escape_sqlData_without_quotes($post['id']).'\'');
		else
			return TournamentError('Weniger als 0 ist nicht m&ouml;glich.',E_USER_ERROR,__FILE__,__LINE__);
	}

	function framescreenshots($get) {
		if (empty ($get['matchid']))
			return TournamentError('Keine Paarung angegeben.|MatchID ist leer.', E_USER_WARNING, __FILE__, __LINE__);
		$match = $this->turnier->GetMatches($get['matchid']);
		$this->tpl['screens'] = MysqlReadCol('SELECT `name` FROM `'.TblPrefix().'flip_content_image` WHERE `name` LIKE \'%_turnier_ID'.$match['tournament_id'].'_Match'.$match['id'].'%\'', 'name');
		return $this->tpl;
	}

	function _getTurnierdaten() {
		$r = array();
		/* Status */
		$r['id'] = $this->turnier->id;
		$r['mtime'] = $this->turnier->mtime;
		$r['orgas'] = $this->turnier->getOrgas();
		$r['coins'] = $this->turnier->coins;
		$r['currency'] = $this->turnier->currency;
		$r['begin'] = $this->turnier->GetStartString();
		$r['status'] = $this->turnier->GetStatusString();
		$r['round'] = ($this->turnier->roundtime > 0) ? $this->turnier->roundtime.' Min.' : text_translate('unbegrenzt');
		$r['mode'] = $this->turnier->GetTypeString();
		$r['teamsize'] = $this->turnier->teamsize;
		/* Beschreibung */
		$r['TDescription'] = $this->turnier->description;
		/* hinzufuegen runden zu spielen f�r Punktesystem */
		return $r;
	}
	
	function frameoverview($get) {
		global $User;
		$this->Caption .= ': &uuml;bersicht';

		$turnierdaten = $this->_getTurnierdaten();
		$this->tpl = array_merge($this->tpl, $turnierdaten);
		
		/* Anmeldelink */
		/* VulkanLAN Mod - Right */
		if ($User->hasRight('status_paid') || ('status_paid_clan') ){
			$this->tpl['right_status_paid_true'] = 'true';
		}
		$this->tpl['anmeldeid'] = $this->turnier->UserNotJoinText();
		if(!$this->tpl['anmeldeid'])
			$this->tpl['anmeldeid'] = $get['id'];

		/* Teilnehmerliste */
		if (($teamdata = $this->turnier->GetCombatants()) === false)
			TournamentError('Konnte Teams nicht lesen: '.mysqli_error()."<br/>\n", E_USER_WARNING, __FILE__, __LINE__);
		$this->tpl['numteams'] = count($teamdata).'/'.$this->turnier->maximum;
		$teamidarr = array ();
		foreach ($teamdata AS $ateam)
			$teamidarr[] = $ateam["team_id"];
		$teamids = implode_sqlIn($teamidarr);
		if (!empty ($teamidarr)) {
			$users = MysqlReadArea("SELECT s.id, s.name, g.parent_id
						FROM ".TblPrefix()."flip_user_groups g, ".TblPrefix()."flip_user_subject s
						WHERE g.child_id = s.id AND g.parent_id IN ($teamids)
						ORDER BY g.parent_id, s.name");
			$teamnames = MysqlReadCol("SELECT d.val AS teamname, d.subject_id
						FROM ".TblPrefix()."flip_user_column c, ".TblPrefix()."flip_user_data d
						WHERE d.subject_id IN ($teamids)
						AND d.column_id=c.id
						AND c.name='".TournamentTeamNameColumn()."'
						AND c.type='turnierteam'", "teamname", "subject_id");
		} else {
			$users = array ();
			$teamnames = array ();
		}
		foreach ($users AS $auser)
			$groupedusers[$auser["parent_id"]][$auser["id"]] = $auser["name"];
		unset ($users);
		//Anfragen
		$askdata = array();
		$asking = MysqlReadArea("SELECT * FROM ".TblPrefix()."flip_tournament_invite WHERE `from`='user' AND tournaments_id='".$this->turnier->id."'", "user_id");
		foreach ($asking AS $ask)
			$askdata[$ask["team_id"]][$ask["user_id"]] = $ask["id"];
		$asked = MysqlReadArea("SELECT * FROM ".TblPrefix()."flip_tournament_invite WHERE `from`='team' AND tournaments_id='".$this->turnier->id."'", "user_id");
		foreach ($asked AS $ask)
			$askdata[$ask["user_id"]][$ask["team_id"]] = $ask["id"];

		$usercoins = $this->turnier->GetUserCoins();
		$UserIsPlayer = $this->turnier->isPlayer($User->id);
		
		$i = (integer) 0;
		$k = (integer) 0;
		$userids = array ();
		if (is_array($teamdata)) {
			foreach ($teamdata AS $getteams) {
				/* Teamgr&ouml;&szlig;e ermitteln */
				$userids = $groupedusers[$getteams["team_id"]];
				$countteam = count($userids);
				$this->tpl["teams"][$i]["id"] = $getteams["team_id"];
				$this->tpl["teams"][$i]["team"] = htmlentities_single($teamnames[$getteams["team_id"]]);
				$this->tpl["teams"][$i]["action"] = "&nbsp;";
				if (isset ($userids[$User->id]) || $this->turnier->Orga) {
					$myteam = true;
					/* Eigenes Team editierbar wenn Turnier noch nicht l&auml;uft */
					if ($this->turnier->status == "open") {
						$this->tpl["anmeldestatus"] = true;
						$this->tpl["andemeldet"] = true;
						$this->tpl["teams"][$i]["team"] = "<a href=\"".basename(__FILE__)."?frame=editTeam&amp;id=".$get["id"]."&amp;newteamid=".$getteams["team_id"]."\"><b>".htmlentities_single($teamnames[$getteams["team_id"]])."</b></a>";
						//Anzeige von Anfragen
						if (isset($askdata[$getteams['team_id']]) && count($askdata[$getteams['team_id']]) > 0) {
							$this->tpl['teams'][$i]['action'] .= '<a href="'.basename(__FILE__).'?frame=saveTeam&amp;id='.$get["id"].'&amp;newteamid='.$getteams["team_id"].'">Anfragen vorhanden</a> ';
						}
						// Abmeldebutton
						if(isset ($userids[$User->id])) { // f�r Turnierorga ausblenden, wenn kein member
							$this->tpl['teams'][$i]['action'] .= '<a href="'.basename(__FILE__).'?frame=overview&amp;id='.$get["id"].'&amp;action=abmelden&amp;teamid='.$getteams["team_id"].'" class="text-danger">von Team abmelden</a> ';
						}
					} else {
						$this->tpl['teams'][$i]['team'] = '<strong>'.htmlentities_single($teamnames[$getteams['team_id']]).'</strong>';
					}
				}
				if (!isset ($userids[$User->id]) || $this->turnier->Orga) //Orga bekommt edit und anfragen
					{
					$myteam = false;
					$joinlink = false;
					if ($this->turnier->status == "open" && $User->hasRight($this->turnier->playright) && !($this->turnier->u18 == "N" && $User->getProperty("is_adult") != "Y") && $usercoins >= $this->turnier->coins) {
						$this->tpl['anmeldestatus'] = true;
						/* nicht anfragende Spieler k&ouml;nnen teilnehmen */
						if (isset ($askdata[$getteams['team_id']][$User->id]))
							$hasasked = true;
						else
							$hasasked = false;
						if (!$hasasked && $countteam < $this->turnier->teamsize && !$UserIsPlayer)
							$joinlink = "<a href=\"".basename(__FILE__)."?frame=overview&amp;id=".$get["id"]."&amp;action=askTeam&amp;teamid=".$getteams["team_id"]."\">anfragen</a>";
						/* wenn schon angefragt, dann kein Link sondern Text */
						elseif ($hasasked) $joinlink = '<span class="footer text-success">Anfrage gesendet</span>';
						/* Eingeladene Spieler k&ouml;nnen dem Team beitreten */
						if (isset ($askdata[$User->id][$getteams['team_id']])) {
							$joinlink = "<a href=\"".basename(__FILE__)."?frame=overview&amp;id=".$get["id"]."&amp;action=addPlayer&amp;ids[]=".
							$askdata[$User->id][$getteams["team_id"]]."\">Einladung annehmen</a>";
						}
						if ($joinlink)
							$this->tpl['teams'][$i]['action'] .= $joinlink;
					}
				}
				$j = (integer) 0;
				if(!is_array($userids)) $userids = array();
				foreach ($userids AS $id => $name) {
					/* Spielernamen */
					$this->tpl['teams'][$i]['players'][$j]['nick'] = htmlentities_single($name);
					$this->tpl['teams'][$i]['players'][$j]['userid'] = $id;
					/* alra u. naaux Anpassung - ingame nickname Anzeige Turnier Teilnehmerliste */
					$ingameNickNameUser = new User($id);
					$this->tpl['teams'][$i]['players'][$j]['ingame_nick_name'] = $ingameNickNameUser->getConfig("ingame_nick_name");
					/*anpassung alra u. naaux ende */
					if ($myteam && $countteam < $this->turnier->teamsize) {
						$combatant = CreateSubjectInstance($getteams['team_id']);
						if ($combatant->getProperty('fixed_size') < 1 || $combatant->getProperty('fixed_size') > $countteam) {
							// Bei nicht vollz&auml;hligem eigenen Team Namen rot
							$this->tpl['teams'][$i]['players'][$j]['nick'] = '<span style="color:red">'.htmlspecialchars($name).'</span>';
						}
					}
					$j ++;
					$k ++;
				}
				$i ++;
			}
		}

		$this->_TAdminLinks();

		$this->tpl['numplayers'] = $k.'/'.count($teamdata) * $this->turnier->teamsize;

		/* Ausgabe */
		$this->TplSub = 'Overview';
		return $this->tpl;
	}
	
	function frameShowDescription($g) {
		if(!empty($g["id"]))
			$desc = MysqlReadField("SELECT `description` FROM `".TblPrefix()."flip_tournament_tournaments` WHERE `id` = ".escape_sqlData($g["id"]).";");
		else
			trigger_error_text("Es wurde keine Turnier-ID f&uuml;r die Beschreibung &uuml;bergeben!",E_USER_ERROR);
		return array("description" => $desc);
	}

	function _TAdminLinks() {
		/* Orga-/Adminlink */
		$this->tpl["adminlinks"] = array ();
		if ($this->turnier->Orga) {
			//Turnier bearbeiten
			$this->tpl["adminlinks"][] = array ("frame" => "change", "tournamentid" => $this->turnier->id, "linktext" => "Bearbeiten");
			//Status abh&auml;ngige
			if ($this->turnier->status == "open")
				$this->tpl["adminlinks"][] = array ("frame" => "settostart", "tournamentid" => $this->turnier->id, "linktext" => "Aufw&auml;rmphase");
			if ($this->turnier->status == "open" || $this->turnier->status == "start")
				$this->tpl["adminlinks"][] = array ("frame" => "create_matches", "tournamentid" => $this->turnier->id, "linktext" => "Turnier starten");
			if ($this->turnier->status == "games" || $this->turnier->status == "grpgames")
				$this->tpl["adminlinks"][] = array ("frame" => "server&amp;game=".$this->turnier->game, "tournamentid" => $this->turnier->id, "linktext" => "Server&uuml;bersicht");
			if ($this->turnier->screenshots == "Y")
				$this->tpl["adminlinks"][] = array ("frame" => "screenshots", "tournamentid" => $this->turnier->id, "linktext" => "Screenshots");
		}
		/* Adminlink */
		if (TournamentIsAdmin()) {
			//Orgaverwaltung
			$this->tpl["adminlinks"][] = array ("frame" => "editorgas", "tournamentid" => $this->turnier->id, "linktext" => "Orgaverwaltung");
			//Overlap-Ansicht
			$this->tpl["adminlinks"][] = array ("frame" => "overlap", "tournamentid" => $this->turnier->id, "linktext" => "Turnierkollisionen");
		}
	}

	function frametree($get) {
		global $User;
		ArrayWithKeys($get, array("nomenu")); 
		$this->tpl["nomenu"] = $get["nomenu"];
		if ($get["nomenu"] == 1) {
			$this->ShowMenu = false;
			$this->tpl["title"] = ConfigGet("page_title");
			$this->tpl["caption"] = $this->Caption;
		}
		$this->Caption .= ": Turnierbaum";

		if (empty ($this->turnier->treetype)) {
			$this->tpl["treetypefile"] = "inc.tree.default.tpl";
			$this->tpl["treetypestyles"] = "inc.tree.default.styles.tpl";
		} else {
			$this->tpl["treetypefile"] = "inc.tree.".$this->turnier->treetype.".tpl";
			$this->tpl["treetypestyles"] = "inc.tree.".$this->turnier->treetype.".styles.tpl";
		}

		/* Wenn Zeit abgelaufen ist und automatisch ein zuf&auml;lliger Gewinner eingetragen werden soll */
		if (ConfigGet("tournament_defaultwinbytimeout") > 0) {
			$latematches = MysqlReadCol("SELECT id FROM ".TblPrefix()."flip_tournament_matches WHERE endtime < '".time()."' AND endtime > 0 AND points1 IS NULL AND team1 != ''", "id");
			srand((double) microtime() * 1000000);
			foreach ($latematches AS $amatchid) {
				if (rand() % 2 == 0) {
					$points1 = 0;
					$points2 = 1;
				} else {
					$points1 = 1;
					$points2 = 0;
				}
				$this->turnier->InsertScore(new TournamentMatch($amatchid), "$points1", "$points2", "Zeit abgelaufen", true);
			}
		}

		/* Alle Spiele auslesen und nach Level/Runde und Spiel sortieren */
		$matches = array();
		foreach ($this->turnier->GetMatches() AS $match) {
			if ($match['level'] > TournamentGroupmatchestartlevel())
				continue; //Gruppenspiele auslassen
			$matches[$match['level']][$match['levelid']] = $match;
		}
		$this->tpl['showgames'] = false;
		if (count($matches) > 0 && ($this->turnier->status != 'start' || ($this->turnier->status == 'start' && $this->turnier->Orga))) {
			$this->tpl['showgames'] = true;
			//f&uuml;r korrekteste Darstellung
			$this->tpl['browser'] = $_SERVER['HTTP_USER_AGENT'];

			//f&uuml;r Teamsuche
			$teamslastround = MysqlReadCol("SELECT g.parent_id, MIN(m.level) AS level FROM ".TblPrefix()."flip_user_groups g, ".TblPrefix()."flip_tournament_matches m
			                                    WHERE (g.parent_id = m.team1 OR g.parent_id = m.team2)
			                                      AND m.tournament_id = '".$match["tournament_id"]."'
			                                    GROUP BY g.parent_id
			                                   ", "level", "parent_id");

			$toplevel = $this->turnier->FirstRound;
			/* Tabellendaten */
			//Spaltenanzahl festlegen
			if ($this->turnier->looser == 'Y' && $toplevel > 0) {
				$this->tpl['coldef']['span'] = (integer) $toplevel +1;
				$this->tpl['lcoldef']['span'] = (integer) 2 * ($toplevel -1);
				$this->tpl['lcoldef']['width'] = 100 / $this->tpl['lcoldef']['span'].'%';
			}
			elseif ($toplevel > 0) {
				$this->tpl['coldef']['span'] = (integer) $toplevel;
			} else {
				$this->tpl['coldef']['span'] = 1;
			}
			$this->tpl['coldef']['width'] = 100 / $this->tpl['coldef']['span'].'%';
			$k = 0;
			//Spalten des Loserbracket
			if ($this->turnier->looser == "Y") {
				for ($spalte = $toplevel -0.75; $spalte >= 0.75; $spalte -= 0.5) {
					//Zeilen F&uuml;llen
					$rows = pow(2, round($spalte, 0)) / 2;
					if (!isset ($maxrows))
						$maxrows = $rows;
					$this->tpl['lcol'][$k] = $this->_TreeGetColData($matches, $spalte, $rows, $maxrows, $teamslastround);
					$this->tpl['lcol'][$k]['no'] = $spalte;
					$this->tpl['lcol'][$k]['roundtitle'] = TournamentLvl2txt($spalte, $this->turnier->id);
					$k ++;
				}
			}
			//Spalten des Winnerbracket
			$maxrows = (pow(2, $toplevel) / 2);
			for ($spalte = $toplevel; $spalte >= 0; $spalte --) {
				if ($this->turnier->looser == "N" && $spalte == 1)
					$spalte --;
				//Zeilen F&uuml;llen
				if ($spalte != 0)
					$rows = pow(2, $spalte) / 2;
				else
					$rows = 1;
				$this->tpl['col'][$k] = $this->_TreeGetColData($matches, $spalte, $rows, $maxrows, $teamslastround);
				$this->tpl['col'][$k]['no'] = $spalte;
				$this->tpl['col'][$k]['roundtitle'] = TournamentLvl2txt($spalte, $this->turnier->id);
				$k ++;
			}
		}
		elseif ($this->turnier->type == 'group') {
			$this->tpl['coldef']['span'] = 1;
			$this->tpl['coldef']['width'] = 100 / $this->tpl['coldef']['span'].'%';
		}

		/*****************
		 * Gruppenspiele *
		 *****************/
		$matches = $this->turnier->GetMatches();
		$matches = array_reverse($matches, true); //groups ascending not descending (level)
		if (count($matches) > 0 && $this->turnier->type == "group" && ($this->turnier->status != "start" || ($this->turnier->status == "start" && $this->turnier->Orga))) {
			$this->tpl["showgames"] = true;
			foreach ($matches AS $match) {
				if ($match["level"] < TournamentGroupmatchestartlevel())
					continue; //only groupmatches, no KO-matches

				$this->tpl["groups"][$match["level"]]["title"] = TournamentLvl2txt($match["level"], $match['tournament_id']);

				/* Tabelle */
				#Freilos mitrechnen if(!($match["team1"]=="0" || $match["team2"]=="0"))
				#Freilos mitrechnen {
				//Beide Teams initialisieren
				for($i = 1; $i <= 2; $i++) {
					if (!isset ($this->tpl["groups"][$match["level"]]["ergebnis"][$match["team$i"]]["name"])) {
						//Eigenes Team hervorheben
						if ($User->isChildOf($match["team1"]))
							$this->tpl["groups"][$match["level"]]["ergebnis"][$match["team$i"]]["name"] = "<strong>".TournamentCombatantID2Name($match["team$i"])."</strong>";
						else
							$this->tpl["groups"][$match["level"]]["ergebnis"][$match["team$i"]]["name"] = TournamentCombatantID2Name($match["team$i"]);
						ArrayWithKeys($this->tpl["groups"][$match["level"]]["ergebnis"][$match["team$i"]], array("matchcount","pluspunkte","minuspunkte")); 
					}
				}

				// kein Sieg, Niederlage, Unentschieden -> "0" eintragen
				for($i = 1; $i <= 2; $i++) {
					//Team i
					if (empty ($this->tpl["groups"][$match["level"]]["ergebnis"][$match["team$i"]]["matchcount"]))
						$this->tpl["groups"][$match["level"]]["ergebnis"][$match["team$i"]]["matchcount"] = 0;
					if (empty ($this->tpl["groups"][$match["level"]]["ergebnis"][$match["team$i"]]["points"]))
						$this->tpl["groups"][$match["level"]]["ergebnis"][$match["team$i"]]["points"] = 0;
					if (empty ($this->tpl["groups"][$match["level"]]["ergebnis"][$match["team$i"]]["won"]))
						$this->tpl["groups"][$match["level"]]["ergebnis"][$match["team$i"]]["won"] = 0;
					if (empty ($this->tpl["groups"][$match["level"]]["ergebnis"][$match["team$i"]]["equal"]))
						$this->tpl["groups"][$match["level"]]["ergebnis"][$match["team$i"]]["equal"] = 0;
					if (empty ($this->tpl["groups"][$match["level"]]["ergebnis"][$match["team$i"]]["lost"]))
						$this->tpl["groups"][$match["level"]]["ergebnis"][$match["team$i"]]["lost"] = 0;
				}

				if ($match["points1"] != "" && $match["points2"] != "") {
					//Anzahl Spiele
					$this->tpl["groups"][$match["level"]]["ergebnis"][$match["team1"]]["matchcount"]++;
					$this->tpl["groups"][$match["level"]]["ergebnis"][$match["team2"]]["matchcount"]++;

					//Gesamtscore
					$this->tpl["groups"][$match["level"]]["ergebnis"][$match["team1"]]["pluspunkte"] += (int) $match["points1"];
					$this->tpl["groups"][$match["level"]]["ergebnis"][$match["team1"]]["minuspunkte"] += (int) $match["points2"];
					$this->tpl["groups"][$match["level"]]["ergebnis"][$match["team2"]]["pluspunkte"] += (int) $match["points2"];
					$this->tpl["groups"][$match["level"]]["ergebnis"][$match["team2"]]["minuspunkte"] += (int) $match["points1"];

					//Punkte
					if ($match["points1"] > $match["points2"]) { //Team1 hat gewonnen
						$this->tpl["groups"][$match["level"]]["ergebnis"][$match["team1"]]["points"] += 3;
						$this->tpl["groups"][$match["level"]]["ergebnis"][$match["team1"]]["won"]++;
						$this->tpl["groups"][$match["level"]]["ergebnis"][$match["team2"]]["lost"]++;
					}
					elseif ($match["points1"] < $match["points2"]) { //Team2 hat gewonnen
						$this->tpl["groups"][$match["level"]]["ergebnis"][$match["team2"]]["points"] += 3;
						$this->tpl["groups"][$match["level"]]["ergebnis"][$match["team2"]]["won"]++;
						$this->tpl["groups"][$match["level"]]["ergebnis"][$match["team1"]]["lost"]++;
					}
					elseif ($match["points1"] == $match["points1"]) { //Unentschieden
						$this->tpl["groups"][$match["level"]]["ergebnis"][$match["team1"]]["points"]++;
						$this->tpl["groups"][$match["level"]]["ergebnis"][$match["team1"]]["equal"]++;
						$this->tpl["groups"][$match["level"]]["ergebnis"][$match["team2"]]["points"]++;
						$this->tpl["groups"][$match["level"]]["ergebnis"][$match["team2"]]["equal"]++;
					}
				}
				#Freilos mitrechnen }

				//Freilos aus Tabelle ausblenden
				unset ($this->tpl["groups"][$match["level"]]["ergebnis"][0]);

				/* Paarungen */
				$team1 = TournamentCombatantID2Name($match["team1"]);
				$team2 = TournamentCombatantID2Name($match["team2"]);
				$me = false;
				// Team1
				if ($User->isChildOf($match["team1"]) || ($match["points1"] == "" AND $this->turnier->Orga)) {
					$me = 1;
					$team1 = "<strong>".$team1."</strong>";
					/* Wenn noch nicht gespielt -> Gegner als Sitzplan-Link */
					if ($match["points1"] == "" AND $match["points2"] == "" && !empty ($match["team1"]) && !empty ($match["team2"])) 
					{
						$search = "";
						$first = true;
						$g = CreateSubjectInstance($match["team2"]);
						foreach ($g->getChilds() AS $id => $nick) {
							if (!$first) {
								$search .= "&amp;ids[]=$id";
							} else {
								$search = $id;
								$first = false;
							}
						}
						if ($team2 != "-" && $team2 != "Freilos")
							$team2 = "<a href=\"seats.php?ids[]='.$search.'\">".$team2."</a>";
					}
				}
				// Team2
				if ($User->isChildOf($match["team2"]) || ($match["points1"] == "" AND $this->turnier->Orga)) {
					$me = 2;
					$team2 = "<strong>".$team2."</strong>";
					/* Wenn noch nicht gespielt -> Gegner als Sitzplan-Link */
					if ($match["points1"] == "" AND $match["points2"] == "" && !empty ($match["team1"]) && !empty ($match["team2"])) {
						$g = CreateSubjectInstance($match["team1"]);
						$search = "";
						$first = true;
						foreach ($g->getChilds() AS $id => $nick) {
							if (!$first) {
								$search .= "&amp;ids[]=$id";
							} else {
								$search = $id;
								$first = false;
							}
						}
						if ($team1 != "-" && $team1 != "Freilos")
							$team1 = "<a href=\"seats.php?ids[]='.$search.'\">".$team1."</a>";
					}
				}
				$match["team1html"] = $team1;
				$match["team2html"] = $team2;
				$this->tpl["groups"][$match["level"]]["rounds"][$match["levelid"]]["matches"][] = array_merge($match, $this->_TreeGetScoreData($match, $me, "grpgames"));
			}
			//Runden sortieren
			ksort($this->tpl["groups"][$match["level"]]["rounds"]);
			//Tabelle nach Punkten sortieren
			foreach ($this->tpl["groups"] AS $key => $team) {
				$team["ergebnis"] = $this->turnier->sortGroups($team["ergebnis"]);
				$this->tpl["groups"][$key] = $team;
			}
		}

		if ($this->turnier->Orga) {
			$this->tpl["orga"] = "true";
			$this->_TAdminLinks();
			if ($this->turnier->status == "games" || $this->turnier->status == "grpgames") {
				//Suchergebnis bei mehreren Teamnamen
				$teamids = array();
				if (isset ($this->FrameVars["like"])) {
					$teamids = MysqlReadCol("SELECT d.val, d.subject_id FROM ".TblPrefix()."flip_user_column c, ".TblPrefix()."flip_user_data d, ".TblPrefix()."flip_tournament_combatant t
			                                    WHERE c.type = 'turnierteam'
			                                      AND c.name = 'teamname'
			                                      AND d.column_id = c.id
			                                      AND t.tournament_id = '".$this->turnier->id."'
			                                      AND d.subject_id = t.team_id
			                                      AND d.val LIKE '%".escape_sqlData_without_quotes($this->FrameVars["like"])."%'
			                                   ", "subject_id", "val");
				}
				$this->tpl["teamids"] = $teamids;
			}
		}
		/* Ausgabe */
		return $this->tpl;
	}

	function _TreeGetColData($matches, $spalte, $rows, $maxrows, $teamslastround) {
		global $User;
		$j = 0;
		for ($zeile = 1; $zeile <= $rows; $zeile ++) {
			$currentmatch = array();
			if(isset($matches["$spalte"]["$zeile"])) {
				$currentmatch = $matches["$spalte"]["$zeile"];
			}
			ArrayWithKeys($currentmatch, array('team1','team2','points1','points2','level','endtime','tournament_id'));
			
			// Template F&uuml;llen
			$coldata['row'][$j] = $currentmatch;
			$coldata['row'][$j]['team1html'] = TournamentCombatantID2Name($currentmatch['team1']);
			$coldata['row'][$j]['team2html'] = TournamentCombatantID2Name($currentmatch['team2']);
			/* Eigenes Team fett */
			//TODO nicht in der Schleife jeweils eine Instanz erzeugen
			$turnier = new Turniersystem($currentmatch['tournament_id']);
			
			if(isset($turnier->type) && ($turnier->type == 'dm')) {
				$me = true;
			} else {
				$me = false;
			}
			//je Team
			for($i = 1; $i <= 2; $i++) { //FIXME: 2 ist in Schleife als (2+1) verwendet
				$other = 2+1 - $i;
				if ((!empty ($currentmatch["team$i"]) && $User->isChildOf($currentmatch["team$i"])) || ($currentmatch["points$i"] == "" AND $this->turnier->Orga)) {
					$me = $i; //wird bei Orgas mit 2 &uuml;berschrieben
					$coldata['row'][$j]['team'.$i.'html'] = '<strong>'.$coldata['row'][$j]['team'.$i.'html'].'</strong>';
					/* Wenn noch nicht gespielt -> Gegner als Sitzplan-Link */
					if($currentmatch["points1"]=="" AND $currentmatch["points2"]=="" && !empty($currentmatch["team1"]) && !empty($currentmatch["team2"]))
					{
			            $g = CreateSubjectInstance($currentmatch["team$other"]);
			            $search='';
			            $first = true;
			            foreach($g->getChilds() AS $id=>$nick)
			            {
			              if(!$first)
			              {
			                $search .= '&amp;ids[]='.$id;
			              }
			              else
			              {
			                $search = $id;
			                $first = false;
			              }
			            }
			            if(!empty($currentmatch["team$other"]))
							//anpassung tominator u. naaux '.$search.'
			            	$coldata['row'][$j]["team{$other}html"] = '<a href="seats.php?frame=search&amp;ids[]='.$search.'">'.$coldata['row'][$j]["team{$other}html"].'</a>';
			          
			          }
				}
			}
			//je Team
			for($i = 1; $i <= 2; $i++) {
				if (isset($teamslastround[$currentmatch["team$i"]]) && $teamslastround[$currentmatch["team$i"]] == $currentmatch['level']) {
					$coldata['row'][$j]["team{$i}html"] = '<a name="'.$currentmatch["team$i"].'"></a>'.$coldata['row'][$j]["team{$i}html"];
				}
			}
			$coldata['row'][$j] = array_merge($coldata['row'][$j], $this->_TreeGetScoreData($currentmatch, $me, 'games'));
			$j ++;
		}
		return $coldata;
	}

	function _TreeGetScoreData($match, $me, $editstatus) {
		$nextround = false;
		if (!empty ($match)) {
			//n&auml;chste Runde ermitteln
			$newlevel = $this->turnier->getnextLevel($match["level"]);
			//n&auml;chste Runde schon gespielt?
			if ((isset ($newlevel["winner"]) && $this->turnier->isRoundplayed($newlevel["winner"], $match["team1"], $match["team2"])) || (isset ($newlevel["looser"]) && $this->turnier->isRoundplayed($newlevel["looser"], $match["team1"], $match["team2"])))
				$nextround = true;
			else
				$nextround = false;
		}
		/* Gewinner/Verlierer hervorheben */
		$color1 = $color2 = 'notset';
		if ($match['points1'] > $match['points2']) {
			$color1 = 'winner';
			$color2 = 'loser';
		}
		elseif ($match['points1'] < $match['points2']) {
			$color2 = 'winner';
			$color1 = 'loser';
		}
		elseif ($match['team1'] != '' && $match['team2'] != '') {
			$color1 = $color2 = 'open';
		}
		/* Ergebnis oder Eintragenlink */
		$showtime = false;
		$readylink = '';
		if ($match['points1'] == '' && $match['points2'] == '') {
			//Eintragelink
			if (time() > $match['endtime'] && !empty ($match['endtime'])) {
				if ($this->turnier->Orga && ($match['team1'] != '' && $match['team2'] != ''))
					$score = "<a class=\"btn btn-danger btn-sm\" role=\"button\" style=\"padding-bottom: 0px;padding-top: 0px;\" href=\"".basename(__FILE__)."?frame=editPoints&amp;id=".$match["tournament_id"]."&amp;matchid=".$match["id"]."\">zu sp&auml;t*</a>";
				else
					$score = "<span style=\"color:red;\">zu sp&auml;t*</span>";
				$this->tpl['toolate'] = 'true';
			}
			elseif (($me || $this->turnier->Orga) && ($match["team1"] != "" && $match["team2"] != "") && $this->turnier->status != "end") {
				$score = "<a class=\"btn btn-outline-dark btn-sm\" role=\"button\"  style=\"padding-bottom: 0px;padding-top: 0px;\" href=\"".basename(__FILE__)."?frame=editPoints&amp;id=".$match["tournament_id"]."&amp;matchid=".$match["id"]."\">vs.</a>";
				$readylink = "tournament.php?frame=tree&amp;id=".$match["tournament_id"]."&amp;action=ready&amp;matchid=".$match["id"];
			} else {
				$score = 'vs.';
			}
			$showtime = true;
		}
		elseif ($this->turnier->Orga && ($match["points1"] != "" || $this->turnier->FirstRound == $match["level"]) && ($match["points2"] != "" || $this->turnier->FirstRound == $match["level"]) && (($match["team1"] != "0" && $match["team2"] != "0") || TournamentIsAdmin()) && !$nextround && $this->turnier->status == $editstatus) {
			//Editierlink (mit Ergebnis)
			$score = "<a class=\"btn btn-outline-dark btn-sm\" role=\"button\" href=\"".basename(__FILE__)."?frame=editPoints&amp;id=".$match["tournament_id"]."&amp;matchid=".$match["id"]."\">".$match["points1"].":".$match["points2"]."</a>";
		} else {
			//nur Ergebnis
			$score = $match["points1"].":".$match["points2"];
			//Screenshotlink?
			if ($this->turnier->screenshots == "Y") {
				static $screenlist;
				if (empty ($screenlist))
					$screenlist = MysqlReadCol("SELECT `name` FROM `".TblPrefix()."flip_content_image` WHERE `name` LIKE '%_turnier_ID".$this->turnier->id."%'", "name", "name");
				$screenshotname = $this->turnier->game."_turnier_ID".$this->turnier->id."_Match".$match["id"]."_1";
				if (!empty ($screenlist[$screenshotname])) // oder in_array($screenshotname, $screenlist)
					$score = "<a href=\"".basename(__FILE__)."?frame=screenshots&amp;id=".$this->turnier->id."&amp;matchid=".$match["id"]."\">".$match["points1"].":".$match["points2"]."</a>";
			}
		}

		/* Kommentar */
		$comment = "";
		if (!empty ($match["comment"]))
			$comment = htmlspecialchars($match["comment"]);

		//Rundenendzeit
		if ($showtime) {
			if (!empty ($match["endtime"])) {
				$timestring = "bis&nbsp;".date("H:i", $match["endtime"])."\n";

				if ($this->turnier->Orga)
					$timestring = "<a class=\"btn btn-secondary btn-sm\" style=\"padding-bottom: 0px;padding-top: 0px;\" role=\"button\" href=\"".basename(__FILE__)."?frame=edittime&amp;id=".$this->turnier->id."&amp;matchid=".$match["id"]."\">$timestring</a>";

				if (empty ($comment))
					$comment = $timestring;
				else
					$comment .= "<br>\n$timestring";
			}
			if ($this->turnier->Orga && ($match["team1"] != "" || $match["team2"] != ""))
				$comment .= " - <a class=\"btn btn-secondary btn-sm\" role=\"button\" style=\"padding-bottom: 0px;padding-top: 0px;\" href=\"turnieradm.php?frame=setserver&amp;matchid=".$match["id"]."\">Server</a>";
		}
		return array ("score" => $score, "comment" => $comment, "color1" => $color1, "color2" => $color2, "readylink$me"=>$readylink);
	}

	function submitjumpto($post) {
		$teamids = MysqlReadCol("SELECT d.val, d.subject_id FROM ".TblPrefix()."flip_user_column c, ".TblPrefix()."flip_user_data d, ".TblPrefix()."flip_tournament_combatant t
		                              WHERE c.type = 'turnierteam'
		                                AND c.name = 'teamname'
		                                AND d.column_id = c.id
		                                AND t.tournament_id = '".$this->turnier->id."'
		                                AND d.subject_id = t.team_id
		                                AND d.val LIKE '%".escape_sqlData_without_quotes($post["teamname"])."%'
		                             ", "subject_id", "val");
		if (count($teamids) > 1)
			$this->NextPage .= "&like=".urlencode($post["teamname"]);
		else
			$this->NextPage = $_SERVER["SCRIPT_URI"]."#".array_shift($teamids);
		return true;
	}

	function actionready($post) {
		global $User;
		if(!$this->turnier->isPlayer())
			return false;
		$match = $this->turnier->GetMatches($post["matchid"]);
		if (empty ($match))
			return false;
		
		if($User->isChildOf($match["team1"])) {
			$n = 1;
		} elseif($User->isChildOf($match["team2"])) {
			$n = 2;
		} else {
			return false;
		}
		
		if(MysqlWriteByID(TblPrefix()."flip_tournament_matches", array("ready$n"=>time()), $match["id"])) {
			LogAction("Turniersystem: ".$User->name." hat das Team ".TournamentCombatantID2Name($match["team$n"])." als bereit eingetragen (".$match["id"].")");
			$this->turnier->_webmessage("tournament_teamready", $match["team".($n%2+1)], array("opponent"=>TournamentCombatantID2Name($match["team$n"])));
			trigger_error_text(text_translate("Dein Team wurde als 'bereit' eingetragen."));
			Redirect("tournament.php?frame=tree&id=".$match["tournament_id"]);
			return true;
		}
		return false;
	}

	function submitaddpoints($post) {
		global $User;
		$returnfalse = false;
		if (empty ($post["matchid"]))
			return TournamentError("Interner Fehler: Ergebnis konnte nicht eingetragen werden. Es wurde keine MatchID angegeben.", E_USER_WARNING, __FILE__, __LINE__);

		if ($this->turnier->screenshots == "Y") {
			require_once ($this->imagelibary);
			$returntrue = array ();
			$maxfilesize = $this->maxScreenshotSize;

			//Vordefinierte Bildeigenschaften
			$contentgroupid = MysqlReadField("SELECT `id` FROM `".TblPrefix()."flip_content_groups` WHERE `name`='".$this->turnier->ContentGroup."';", "id");
			if (empty ($contentgroupid))
				return TournamentError("Die Gruppe f&uuml;r die Bilder existiert nicht!|Contentgruppe '{$this->turnier->ContentGroup}'", E_USER_WARNING, __FILE__, __LINE__);
			$props4all = array ("name" => $this->turnier->game."_turnier_ID".$this->turnier->id."_Match".$post["matchid"], "caption" => $this->turnier->game."_turnier_ID".$this->turnier->id."_Match".$post["matchid"], "view_right" => GetRightID($this->turnier->playright), "edit_right" => GetRightID(TournamentAdminright()), "group_id" => $contentgroupid, "edit_user_id" => $User->id, "edit_time" => time());
			//Alle Bilder eintragen
			for ($i = 1; $i <= 2; $i ++) {
				$formimage = $post["screen$i"];
				if (is_array($formimage) && !empty ($formimage)) {
					if (empty ($formimage["tmp_name"]))
						continue;
					if ($formimage["size"] > $maxfilesize) {
						trigger_error_text("Die hochgeladene Datei ({$formimage[name]}) ist zu gross (>".round($maxfilesize / 1024, 1)."kB).", E_USER_WARNING);
						$returnfalse = true;
						continue;
					}
					$file = $formimage["tmp_name"];
					$info = GetImageInfo($file);
					if (!is_array($info)) {
						trigger_error_text("Die hochgeladene Datei ({$formimage[name]}) ist besch&auml;digt oder hat ein unbekanntes Bild-Format und wurde deshalb nicht gespeichert.", E_USER_WARNING);
						$returnfalse = true;
						continue;
					}

					$r = $props4all;
					$r["name"] .= "_$i";
					$r["caption"] .= "_$i";
					$r["width"] = $info["width"];
					$r["height"] = $info["height"];
					$r["mime"] = $info["mime"];

					$f = fopen($file, "rb");
					$r["data"] = fread($f, $formimage["size"]);
					fclose($f);

					if ($tn = CreateThumbnail($file, 100, 200)) {
						$r["tn_width"] = $tn->Width;
						$r["tn_height"] = $tn->Height;
						$r["tn_data"] = $tn->getPngData();
					} else {
						$r["tn_width"] = 0;
						$r["tn_height"] = 0;
						$r["tn_data"] = "";
						trigger_error_text("Das Thumbnail konnte nicht erstellt werden.", E_USER_WARNING);
					}					

					if ($id = MysqlWriteByID(TblPrefix()."flip_content_image", $r)) {						
						$returntrue[] = "Der <strong>Screenshot</strong> <a href=\"content.php?frame=editimage&amp;id=$id\">".$r["name"]."</a> wurde hinzugef&uuml;gt.";
					} else {
						$returnfalse = true;
					}
				}
			}

			foreach ($returntrue AS $screenshottext) {
				$this->turnier->_Log($screenshottext, true);
			}
		}

		//Ergebnis speichern
		$match = $this->turnier->GetMatches($post['matchid']);
		if (!empty ($match) && ($User->isChildOf($match['team1']) || $User->isChildOf($match['team2']) || $this->turnier->Orga || $this->turnier->type == 'dm')) {
			//Kommantar nur f&uuml;r Orgas
			if (ConfigGet($this->commentconfig) && !$this->turnier->Orga)
				$post['comment'] = '';
			//Seeding f&uuml;r Orgas
			if ($this->turnier->Orga && $match["level"] == $this->turnier->FirstRound && isset ($post["team1"]) && ($match["team1"] !== $post["team1"] || $match["team2"] !== $post["team2"])) {
				if ($post["team1"] == $post["team2"] && $post["team1"] != 0)
					return TournamentError("Team1 und Team2 d&uuml;rfen nicht dasselbe Team sein!", E_USER_ERROR, __FILE__, __LINE__);
				if (MysqlWriteByID(TblPrefix()."flip_tournament_matches", array ("team1" => $post["team1"], "team2" => $post["team2"]), $post["matchid"])) {
					//Cache l&ouml;schen wegen neuer Paarung
					$this->turnier->GetMatches($post["matchid"], false, true);
					$this->turnier->_Log("In ".TournamentLvl2txt($match["level"], $match['tournament_id'])." wurde die Paarung '".TournamentCombatantID2Name($match["team1"])."' vs. '".TournamentCombatantID2Name($match["team2"])."' ge&auml;ndert in '".TournamentCombatantID2Name($post["team1"])."' vs. '".TournamentCombatantID2Name($post["team2"])."'");
					TournamentError("Teams wurden gesetzt.<br /><a href=\"tournament.php?frame=tree&amp;id=".$this->turnier->id."\">zum Turnierbaum</a>");
				}
			}
			//Ergebnis eintragen (und n&auml;chste Paarung ermitteln)
			if (!$this->turnier->InsertScore(new TournamentMatch($post["matchid"]), $post["points1"], $post["points2"], $post["comment"]))
				$returnfalse = true;
		}
		elseif (!empty ($match)) {
			return TournamentError("Du kannst nur f&uuml;r dein eigenes Team die Punkte eintragen!");
		} else {
			return TournamentError("Fehler bei der Ergebnis&uuml;bermittlung.|Matchid {$post["matchid"]} nicht g&uuml;ltig.", E_USER_WARNING, __FILE__, __LINE__);
		}

		//Falls bei Screenshotupload Fehler aufgetreten sind false zur&uuml;ckgeben (nach false trotzdem Ergebnis eintragen)
		if ($returnfalse)
			return false;
		return true;
	}

	function frameeditPoints($get) {
		global $User;
		$this->Caption = "Ergebnis &auml;ndern";

		if (empty ($get["matchid"]))
			return TournamentError("Es wurde kein Match angegeben.", E_USER_ERROR, __FILE__, __LINE__);

		$this->tpl['type'] = $this->turnier->type;
		
		/* Server */
		$server = MysqlReadRow("SELECT s.name AS servername, s.ip
	                     FROM ".TblPrefix()."flip_tournament_servers s, ".TblPrefix()."flip_tournament_serverusage u
	                     WHERE u.match_id='".$get["matchid"]."' AND s.id=u.server_id", true);
		if (!$server)
			$server = array (); //"name"=>"<keiner>", "ip"=>"0.0.0.0");

		$match = array_merge($server, $this->turnier->GetMatches($get["matchid"]));
		$match["editor"]  = ($match["editor"] == 0) ? text_translate("System") : GetSubjectName($match["editor"]);
		
		if($this->turnier->type == 'dm') {
			foreach ($this->turnier->getCombatants() AS $teamID => $teamData) {
				$this->tpl['teams'][$teamID] = TournamentCombatantID2Name($teamID);
			}
		} else {
			/* Eigenes Team ermitteln */
			if ($User->isChildOf($match["team1"]))
				$me1 = true;
			else
				$me1 = false;
			if ($User->isChildOf($match["team2"]))
				$me2 = true;
			else
				$me2 = false;
			if (!$me1 && !$me2 && !$this->turnier->Orga)
				$User->requireRight(TournamentAdminRight());
	
			/* Namen f&uuml;r die Gruppen-ID  */
			$match["team1id"] = $match["team1"];
			$match["team2id"] = $match["team2"];
			$teamname = TournamentCombatantID2Name($match["team1"]);
			if ($me1)
				$match["team1"] = "<strong>".$teamname."</strong>";
			else
				$match["team1"] = $teamname;
			$teamname = TournamentCombatantID2Name($match["team2"]);
			if ($me2)
				$match["team2"] = "<strong>".$teamname."</strong>";
			else
				$match["team2"] = $teamname;

			/* Orga: Seeding */
			$this->tpl['pointinput'] = array();
			if ($this->turnier->Orga && $match["level"] == $this->turnier->FirstRound && $match["points1"] == "") {
				$this->tpl["pointinput"]["isadmin"] = "true";
				$notseedableteams = array_flip(array_merge(MysqlReadCol("SELECT team1 FROM ".TblPrefix()."flip_tournament_matches m, ".TblPrefix()."flip_tournament_ranking r
			                   WHERE (points1 IS NULL
			                          OR (team1=combatant_id AND rank IS NOT NULL)
			                          OR level<='".$match["level"]."'
			                         )
			                     AND m.tournament_id='".$match["tournament_id"]."'
			                     AND r.tournament_id='".$match["tournament_id"]."'
			                     AND team1!='0'
			                     AND team1!='".$match["team1id"]."'
			                     AND team1!='".$match["team2id"]."'
			                  ", "team1", "team1"), MysqlReadCol("SELECT team2 FROM 
						".TblPrefix()."flip_tournament_matches m, 
						".TblPrefix()."flip_tournament_ranking r
			                   WHERE (points1 IS NULL
			                          OR (team2=combatant_id AND rank IS NOT NULL)
			                          OR level<='".$match["level"]."'
			                         )
			                     AND m.tournament_id='".$match["tournament_id"]."'
			                     AND r.tournament_id='".$match["tournament_id"]."'
			                     AND team2!='0'
			                     AND team2!='".$match["team1id"]."'
			                     AND team2!='".$match["team2id"]."'
			                  ", "team2", "team2")));
				foreach ($this->turnier->GetCombatants(true) AS $id => $combatant)
					if (!isset ($notseedableteams[$id]))
						$this->tpl["pointinput"]["combatants"][$id] = TournamentCombatantID2Name($combatant["team_id"]);
			}
		}

		/* Screenshots */
		if ($this->turnier->screenshots == "Y") {
			require_once ($this->imagelibary);
			$match["types"] = GetSupportedImageTypes();
			for ($i = 1; $i <= 2; $i ++) {
				$match["enabled$i"] = "1";
				$name = $this->turnier->game."_turnier_ID".$this->turnier->id."_Match".$match["id"]."_$i";
				if (MysqlReadField("SELECT id FROM ".TblPrefix()."flip_content_image WHERE `name`='$name'", "id", true)) {
					if ($this->turnier->Orga)
						$match["screen$i"] = "<a href=\"turnieradm.php?frame=screenshots&amp;id=".$this->turnier->id."\">vorhanden</a>";
					else
						$match["screen$i"] = "<a href=\"image.php?name=$name\" target=\"_blank\">vorhanden</a>";
					$match["enabled$i"] = "0";
				}
			}
		}
		
		$this->tpl["pointinput"] 
			= isset($this->tpl["pointinput"]) 
				? array_merge($this->tpl["pointinput"], $match) 
				: $match;

		//commentfield
		if ((ConfigGet($this->commentconfig) && $this->turnier->Orga) || !ConfigGet($this->commentconfig))
			$this->tpl["pointinput"]["cancomment"] = "true";

		/* Orga */
		if ($this->turnier->Orga) {
			//Punkte zur&uuml;cksetzen
			if ($match["points1"] != "") {
				$this->tpl["pointinput"]["admin"][] = array ("action" => "delPoints", "text" => "Punkte zur&uuml;cksetzen", "confirm" => "Soll das Ergebnis und damit die n&auml;chsten Spiele zur&uuml;ckgesezt werden?", "confirm_uri" => basename(__FILE__)."?frame=tree&id=".$match["tournament_id"]);
			}
		}

		/* Ausgabe */
		$this->TplSub = "PointForm";
		return $this->tpl;
	}

	function actiondelPoints($post) {
		global $User;
		if (!$this->turnier->Orga)
			$User->requireRight(TournamentAdminright());
		if (empty ($post["matchid"]))
			return false;
		return $this->turnier->deleteScore($post["matchid"]);
	}

	function frameedittime($get) {
		$this->Caption = "Rundenzeit bearbeiten";
		$match = $this->turnier->GetMatches($get["matchid"]);
		$this->tpl["matchid"] = $match["id"];
		$this->tpl["endtime"] = date("Y-m-d H:i", $match["endtime"]);
		$this->tpl["nextround"] = "1";
		/* Ausgabe */
		return $this->tpl;
	}

	function actionsettime($post) {
		global $User;
		if (!$this->turnier->Orga)
			$User->requireRight(TournamentAdminright());
		if (empty ($post["matchid"]))
			return TournamentError("Die erforderliche MatchID fehlt.", E_USER_WARNING, __FILE__, __LINE__);
		$match = $this->turnier->GetMatches($post["matchid"]);
		if (empty ($match))
			return TournamentError("Fehler: Das Match konnte nicht gelesen werden.|ID:".$post["matchid"], E_USER_WARNING, __FILE__, __LINE__);
		$timestamp = strtotime($post["endtime"]);
		if ($timestamp === -1) {
			return TournamentError("Rundenzeit hat kein g&uuml;ltiges Format.", E_USER_WARNING, __FILE__, __LINE__);
		}
		elseif (empty ($timestamp)) {
			return TournamentError("Fehler beim setzen der Rundenzeit.", E_USER_ERROR, __FILE__, __LINE__);
		}
		elseif ($post["nextround"] == "1") {
			$diff = $timestamp - $match["endtime"];
			if ($this->turnier->status != "grpgames")
				$where = " AND level<='{$match["level"]}'"; //alle weiteren Runden
			else
				$where = " AND endtime>='{$match["endtime"]}'"; //alle sp&auml;teren Spiele
			$query = "UPDATE `".TblPrefix()."flip_tournament_matches` SET endtime=endtime+'$diff' WHERE tournament_id='{$match["tournament_id"]}'$where";
		} else {
			if ($this->turnier->status != "grpgames")
				$where = " AND level='{$match["level"]}'";
			else
				$where = " AND levelid='{$match["levelid"]}'";
			$query = "UPDATE ".TblPrefix()."flip_tournament_matches SET endtime='$timestamp' WHERE tournament_id='{$match["tournament_id"]}'$where";
		}

		if (MysqlWrite($query, "SQL-Fehler in ".basename(__FILE__)." line ".__LINE__."<br>\n")) {
			$this->turnier->GetMatches(0, false, true); //update cache
			TournamentError("Rundenzeit ".date("d.m.Y H:i", $timestamp)." wurde gesetzt.");
			return true;
		}
		TournamentError("Rundenzeit wurde NICHT gesetzt.", E_USER_WARNING, __FILE__, __LINE__);
		return false;
	}

	function frameresults() {
		$this->Caption .= ": Platzierung";

		$ranking = $this->turnier->GetResults();
		if (count($ranking) < 1) {
			$this->tpl["rankings"][0]["rank"] = "";
			$this->tpl["rankings"][0]["name"] = "Noch keine Platzierung vorhanden.";
		} else {
			$i = (integer) 0;
			global $User;
			foreach ($ranking AS $rankdata) {
				$me = $User->isChildOf($rankdata["combatant_id"]);
				switch ($rankdata["rank"]) {
					case "1" :
						if ($me)
							$this->tpl["isFirst"] = "true";
						$this->tpl["first"] = TournamentCombatantID2Name($rankdata["combatant_id"]);
						break;
					case "2" :
						if ($me)
							$this->tpl["isSecond"] = "true";
						$this->tpl["second"] = TournamentCombatantID2Name($rankdata["combatant_id"]);
						break;
					case "3" :
						if ($me)
							$this->tpl["isThird"] = "true";
						if (empty ($this->tpl["third"]))
							$this->tpl["third"] = TournamentCombatantID2Name($rankdata["combatant_id"]);
						else
							$this->tpl["third"] = $this->tpl["third"].", ".TournamentCombatantID2Name($rankdata["combatant_id"]);
						break;
					default :
						if ($me)
							$this->tpl["rankings"][$i]["isInTeam"] = "true";
						$this->tpl["rankings"][$i]["rank"] = $rankdata["rank"];
						$this->tpl["rankings"][$i]["name"] = TournamentCombatantID2Name($rankdata["combatant_id"]);
						$i ++;
				}
			}
		}
		/* Ausgabe */
		$this->TplSub = "Results";
		return $this->tpl;
	}

	function framegetTeam() {
		global $User;
		$this->Caption = "Team "; //wird weiter unten erweitert
		$Captionextended = false;
		if ($this->turnier->isPlayer($User->id))
			return TournamentError("Du nimmst bereits an diesem Turnier teil.", E_USER_ERROR, __FILE__, __LINE__);

		if ($this->turnier->maxcoins > 0) {
			$this->tpl['coins'] = $this->turnier->GetUserCoins();
			$this->tpl['tcoins'] = (integer) $this->turnier->coins;
			$this->tpl['currency'] = $this->turnier->currency;
		}

		if ($this->turnier->teamsize == '1') {
			// 1on1 (Username = Teamname)
			$this->tpl['action'] = basename(__FILE__).'?frame=overview&amp;id='.$this->turnier->id;
			$this->tpl['nickname'] = $User->name;
			$columntype = 'player';
			$existingid = MysqlReadCol("SELECT `subject_id` FROM ".TblPrefix()."flip_user_column c LEFT JOIN ".TblPrefix()."flip_user_data d ON c.id=d.column_id
					WHERE c.type='turnierteam' AND c.name='teamname' AND d.val='".$User->name."'", "subject_id", true);
			$new = true;
			if (!empty ($existingid)) {
				foreach ($existingid AS $cid) {
					$combatant = CreateSubjectInstance($cid, "turnierteam");
					if ($this->turnier->liga->dbAndClassName == 'Keine' || ($this->turnier->liga->dbAndClassName != 'Keine' && $combatant->getProperty("tournament_".strtolower($this->turnier->liga->dbAndClassName)."_id") != '')) {
						$new = false;
						$this->Caption .= 'ausw&auml;hlen';
						$this->tpl['oldsingleplayer'] = 'true';
						$this->tpl['oldteam'][0]['teams'][$cid] = htmlentities_single($User->name);
						break;
					}
				}
			}
			if ($new) {
				$this->Caption .= 'anlegen';
				$this->tpl['newsingleplayer'] = 'true';
			}
			$this->tpl['nametype'] = $this->tpl['idtype'] = 'hidden';
		} else {
			$this->Caption .= 'anlegen';
			$this->tpl['action'] = basename(__FILE__).'?frame=saveTeam&amp;id='.$this->turnier->id;
			$columntype = 'clan';
			$this->tpl['nametype'] = 'string';
			$this->tpl['idtype'] = 'dropdown';
			// Vorhandene Teams anzeigen
			foreach ($User->getParents('turnierteam') AS $id => $team) {
				$combatant = CreateSubjectInstance($id);
				if ($combatant->getProperty('fixed_size') == $this->turnier->teamsize) {
					if ($this->turnier->liga->dbAndClassName != 'Keine' && $combatant->getProperty("tournament_".strtolower($this->turnier->liga->dbAndClassName)."_id") == '')
						$this->tpl['newligaid'] = true;
					if (!$Captionextended) {
						$this->Caption .= '/ausw&auml;hlen';
						$Captionextended = true;
					}
					$this->tpl['oldteam'][0]['teams'][$id] = htmlentities_single($combatant->name);
				}
			}
			unset ($combatant);
		}

		if (!empty ($this->oldteam)) {
			$this->Caption .= 'ausw&auml;hlen';
		}

		$this->tpl['liga'] = $this->turnier->liga->dbAndClassName;
		if ($this->turnier->liga->dbAndClassName != 'Keine') {
			$default = strtoupper($User->getProperty('tournament_'.strtolower($this->turnier->liga->dbAndClassName).'_'.$columntype.'_id'));
			if (empty ($default))
				$default = 'none';
			$this->tpl['ligaid'] = $default;
		}

		if (!empty ($this->tpl['oldteam']))
			$this->tpl['oldteam'][0]['frame'] = 'overview';

		/* Ausgabe */
		return $this->tpl;
	}

	function submitfixsize($post) {
		global $User;
		//User darf am Turnier teilnehmen
		if(false !== ($text = $this->turnier->UserNotJoinText()))
			return TournamentError("Du darfst an dem Turnier nicht teilnehmen: $text", E_USER_ERROR, __FILE__,__LINE__);
		if (!empty ($post["teamid"])) {
			$g = CreateSubjectInstance($post["teamid"]);
			if ($g->CountTeam() > $this->turnier->teamsize)
				return TournamentError("Das Team ist gr&ouml;&szlig;er als die Teamgr&ouml;&szlig;e des Turnieres!", E_USER_WARNING, __FILE__, __LINE__);
			$minteamsize = MysqlReadField("SELECT MIN(teamsize) AS minteamsize FROM (".TblPrefix()."flip_tournament_combatant c) LEFT JOIN ".TblPrefix()."flip_tournament_tournaments t ON t.id=c.tournament_id WHERE c.team_id='".$g->id."'", "minteamsize");
			if ($this->turnier->teamsize < $minteamsize || is_null($minteamsize)) //null = not in a tournament
				$minteamsize = $this->turnier->teamsize;
			if (!$g->setProperty("fixed_size", $minteamsize))
				return TournamentError("Die Gr&ouml;&szlig;e des Teams konnte nicht fixiert werden.", E_USER_WARNING, __FILE__, __LINE__);
			//ggf. LigaID setzen
			if ($this->turnier->liga->dbAndClassName != 'Keine' && $g->getProperty("tournament_".strtolower($this->turnier->liga->dbAndClassName)."_id") == "") {
				if(!$this->turnier->ValidLigaID($post["ligaid"]))
					return TournamentError("Es muss eine g&uuml;ltige ".$this->turnier->liga->dbAndClassName."-ID angegeben werden.|".$post["ligaid"],E_USER_WARNING,__FILE__,__LINE__);
				$g->setProperty("tournament_".strtolower($this->turnier->liga->dbAndClassName)."_id", $post["ligaid"]);
			}
			$this->turnier->AddCombatant($g->id);
			MysqlExecQuery("DELETE FROM ".TblPrefix()."flip_tournament_invite WHERE user_id='{$User->id}' AND tournaments_id='".$this->turnier->id."'");
		}
	}

	function submitaddTeam($post) {
		global $User;
		//User darf am Turnier teilnehmen
		if($text = $this->turnier->UserNotJoinText())
			return TournamentError("Du darfst an dem Turnier nicht teilnehmen: $text", E_USER_ERROR, __FILE__,__LINE__);
		//Teamname OK
		require_once('mod/mod.subject.turnierteam.php');
		if(!TurnierteamValidTeamname($post["teamname"]))
			return TournamentError("Team kann nicht erstellt werden.|ung&uuml;ltier Teamname",E_USER_ERROR,__FILE__,__LINE__);
		//liga
		if(!$this->turnier->ValidLigaID($post["ligaid"]))
			return TournamentError("Die ".$this->turnier->liga->dbAndClassName."-ID ist ung&uuml;ltig!|".$post["ligaid"],E_USER_ERROR,__FILE__,__LINE__);

		/* Gruppe erstellen */
		//tempor&auml;rer Name
		if (!($newteam = CreateSubject("turnierteam", "t".time(), "t".time()."@invalid.com")))
			return TournamentError("Fehler beim erstellen der Gruppe.", E_USER_ERROR, __FILE__, __LINE__);
		if (!is_object($newteam))
			return TournamentError("Fehler w&auml;hrend des Erstellens des Teams.|Subject wurde evtl erstellt (".print_r($newteam).")", E_USER_ERROR, __FILE__, __LINE__);
		//Properties setzen
		$newteam->setProperties(array ("teamname" => $post["teamname"], //Teamname festlegen
		"name" => $post["teamname"]."_".$newteam->id, //eindeutiger SubjektName durch name+ID
		#"email" => "Combatant".$newteam->id."@flip.test", //Email eindeutig durch ID
		"fixed_size" => $this->turnier->teamsize //Team kann nicht gr&ouml;&szlig;er werden als die teamgr&ouml;&szlig;e dieses turnieres
		));
		/* zum Turnier hinzuf&uuml;gen */
		$this->turnier->AddCombatant($newteam->id);
		/* Ersteller in Gruppe aufnehmen und Einladungen l&ouml;schen */
		$User->addParent($newteam->id);
		MysqlExecQuery("DELETE FROM ".TblPrefix()."flip_tournament_invite WHERE user_id='{$User->id}' AND tournaments_id='".$this->turnier->id."'");
		/* WWCL? */
		if ($this->turnier->liga->dbAndClassName == 'WWCL') {
			//Noch keine ID? Dann tempor&auml;re ID
			if ($this->turnier->teamsize > 1)
				$pt = "C";
			else
				$pt = "P";
			if ($post["ligaid"] == "none") {
				$lasttmp = MysqlReadField("SELECT d.val FROM ".TblPrefix()."flip_user_data d, ".TblPrefix()."flip_user_column c WHERE c.type='turnierteam' AND c.name='tournament_wwcl_id' AND c.id=d.column_id AND val LIKE '{$pt}T%' ORDER BY d.val DESC", "val", true);
				if (!stristr($lasttmp, $pt."T")) //ungenau da nur Gross-/Kleinschreibung aber nicht Position gepr&uuml;ft wird
					{
					$post["ligaid"] = $pt."T1";
				} else {
					$lasttmp = explode($pt."T", $lasttmp);
					$i = (integer) $lasttmp[1];
					$i ++;
					$post["ligaid"] = $pt."T$i";
				}
			}
		}
		if ($this->turnier->liga->dbAndClassName != 'Keine') {
			$newteam->setProperty("tournament_".strtolower($this->turnier->liga->dbAndClassName)."_id", $post["ligaid"]);
			$newteam->setProperty(strtolower($this->turnier->liga->dbAndClassName)."_email", $User->email);
		}

		$this->NextPage .= '&newteamid='.$newteam->id;
		return true;
	}

	function actionaskTeam() {
		global $User;
		$User->requireRight($this->turnier->playright);
		$team = CreateSubjectInstance($this->FrameVars['teamid']);
		if ($team->type != 'turnierteam')
			return TournamentError('TeamID ung&uuml;ltig!|'.$this->FrameVars['teamid'].' ist nicht vom Typ \'turnierteam\'', E_USER_WARNING, __FILE__, __LINE__);
		if ($this->turnier->u18 == 'N' && $User->getProperty('is_adult') != 'Y')
			return TournamentError('Dieses Turnier ist erst ab 18. Laut Datenbank bist du nicht vollj&auml;hrig.', E_USER_WARNING, __FILE__, __LINE__);
		if ($this->turnier->GetUserCoins() < $this->turnier->coins)
			return TournamentError('Dein Guthaben reicht leider nicht f&uuml;r eine teilnahme.', E_USER_WARNING, __FILE__, __LINE__);
		if (!$this->turnier->isPlayer($User->id))
			$iID = MysqlWriteByID(TblPrefix().'flip_tournament_invite', array ('from' => 'user', 'tournaments_id' => $this->turnier->id, 'user_id' => $User->id, 'team_id' => $this->FrameVars['teamid']));
		else
			return TournamentError('Du bist bereits Turnierteilnehmer.', E_USER_WARNING, __FILE__, __LINE__);
		foreach ($team->getChilds() AS $memberid => $member)
			$this->_sendWebMessage(array ('receiver' => $memberid, 'subject' => 'Anfrage von "'.$User->name.'" f&uuml;r das Team "'.$team->name.'"', 'message' => '"'.$User->name.'" m&ouml;chte in deinem Team "'.$team->name.'" im "'.$this->turnier->GetTournamentString().'"-Turnier mitmachen.
									      Um die Anfrage anzunehmen braucht lediglich ein Teammitglied diese zu best&auml;tigen:
									      
									      Turniersystem->'.$this->turnier->GetTournamentString().'->'.$team->name.'->Spieler hinzuf&uuml;gen
									      
									      [ '. ServerURL().$_SERVER['SCRIPT_NAME'].'?frame=overview&id='.$this->turnier->id.'&action=addplayer&ids[]='.$iID.' ].'));
		TournamentError('Deine Anfrage wurde an das Team weitergeleitet.<br/>Eines der Teammitglieder muss deiner Anfrage noch zustimmen.<br/>');
		Redirect(EditUrl(array('action'=>'','teamid'=>''), '', false));
	}

	function framesaveTeam($get) {
		global $User;
		ArrayWithKeys($get, array('teamid','newteamid','id'));
		if (isset ($this->ActionVars['newteamid']))
			$newteamid = $this->ActionVars['newteamid'];
		else
			$newteamid = $get['newteamid'];
		if (empty ($newteamid))
			return TournamentError(text_translate('Es wurde kein Team angegeben.'), E_USER_ERROR, __FILE__, __LINE__);
		if (!$User->isChildOf($newteamid) && !$this->turnier->Orga)
			return TournamentError(text_translate('Du bist nicht berechtigt dieses Team zu bearbeiten!'), E_USER_WARNING, __FILE__, __LINE__);

		/* Anzahl der gesamten Spieler &uuml;berpr&uuml;fen (Maximum) */
		if ($this->turnier->isFull()) {
			return TournamentError(text_translate('Die Anzahl der Teams hat bereits das Maximum erreicht.'), E_USER_ERROR, __FILE__, __LINE__);
		}
		/* Anzahl der Spieler im Team &uuml;berpr&uuml;fen (teamsize) */
		$max_teamsize = (integer) $this->turnier->teamsize;
		$combatant = CreateSubjectInstance($newteamid);
		$members = $combatant->GetChilds();
		$free = $max_teamsize -count($members);

		if ($free > 0 && ($combatant->getProperty('fixed_size') < 1 || $combatant->getProperty('fixed_size') > count($members))) {
			$this->Caption = text_translate('Spieler hinzuf&uuml;gen');

			$this->tpl['free'] = $free;
			$this->tpl['newteamid'] = $newteamid;
			/* eingeladene Spieler anzeigen */
			$usrinvited = array ();
			foreach ($this->turnier->GetAsked($newteamid) AS $invited) {
				$u = CreateSubjectInstance($invited['user_id']);
				$this->tpl['invited'][]['nickname'] = htmlentities_single($u->name);
				$usrinvited[] = $invited['user_id'];
			}
			/* anfragende Spieler anzeigen */
			foreach ($this->turnier->GetAsking($newteamid) AS $join) {
				$u = CreateSubjectInstance($join['user_id']);
				$this->tpl['join'][] = array ('id' => $join['id'], 'nickname' => htmlentities_single($u->name));
				$usrinvited[] = $join['user_id'];
			}
			unset ($u);
			/* Link zum Wechsel der "aussuch"-Gruppe */
			$this->tpl['teamlinks'][] = array ('newteamid' => $newteamid, 'teamname' => 'Alle anzeigen');
			if ($User->isChildOf($newteamid) || $this->turnier->Orga) {
				foreach ($User->getParents('turnierteam') as $team_id => $teamname) {
					$combatant = CreateSubjectInstance($team_id);
					if ($combatant->id != $newteamid && $combatant->CountTeam() > 1)
						$this->tpl['teamlinks'][] = array ('newteamid' => $newteamid, 'teamid' => $team_id, 'teamname' => htmlentities_single($combatant->name));
				}
			}
			/* Liste der einladbaren Spieler */
			$players = MysqlReadCol("SELECT g.child_id FROM ".TblPrefix()."flip_user_groups g, ".TblPrefix()."flip_tournament_combatant c WHERE c.tournament_id='".$this->turnier->id."' AND c.team_id=g.parent_id", "child_id");
			$this->tpl['candidates'] = array();
			if (!empty ($get["teamid"])) {
				/* Spieler aus der Gruppe anzeigen */
				$members = MysqlReadArea("SELECT u.id, u.name FROM ".TblPrefix()."flip_user_subject u, ".TblPrefix()."flip_user_groups g WHERE (g.parent_id = '".$get["teamid"]."') AND (u.id = g.child_id)$searchnick ORDER BY name;", "id");
				foreach ($members AS $id => $user) {
					if (in_array($id, $players) || in_array($id, $usrinvited))
						continue;
					$this->tpl['candidates'][] = array ('id' => $id, 'nick' => $user['name']);
				}
			} else {
				//default (alle die das Recht haben an Turnieren teilzunehmen)
				foreach (GetRightOwners($this->turnier->playright) as $id => $v)
					//Spielersuche
					if ($v['type'] == 'user' && !(in_array($id, $players) || in_array($id, $usrinvited)))
					 if(!isset ($this->ActionVars['search']) || stristr($v['name'], $this->ActionVars['search']))
						$this->tpl['candidates'][] = array ('id' => $id, 'nick' => $v['name']);
			}
			/* Suchdialog ab 12 Leuten in der "Aussuchgruppe" */
			$teamsize = count($this->tpl['candidates']);
			if ($teamsize > 11) {
				ArrayWithKeys($this->ActionVars, array('search'));
				$this->tpl['searchform'][0] = array ('newteamid' => $newteamid, 'teamid' => $get['teamid'], 'search' => $this->ActionVars['search']);
			}
			if ($teamsize > 5)
				$selectsize = 5;
			else
				$selectsize = $teamsize;
			$this->tpl['action'] = 'invitePlayer';
			$this->tpl['submitval'] = 'einladen';
			/* Ausgabe */
			$this->TplSub = 'AddPlayer';
			return $this->tpl;
		}
		elseif (!$free > 0) {
			return TournamentError('Das Team ist voll!<br><a href="'.basename(__FILE__).'?frame=overview&amp;id='.$get['id'].'">zur&uuml;ck zum Turnier</a><br/>', E_USER_ERROR, __FILE__, __LINE__);
		} else {
			return TournamentError('Da dieses Team mehrfach angemeldet wurde kann die Gr&ouml;&szlig;e nicht ge&auml;ndert werden:<br/><br/>ben&ouml;tigte Teamgr&ouml;&szlig;e: '.$this->turnier->teamsize.'<br/>feste Teamgr&ouml;&szlig;e: '.$combatant->getProperty('fixed_size').'</div><br/><a href="'.basename(__FILE__).'?frame=overview&amp;id='.$get['id'].'">zur&uuml;ck</a><br/>', E_USER_ERROR, __FILE__, __LINE__);
		}
	}

	function actioninvitePlayer($post) {
		global $User;

		if ($User->isChildOf($this->FrameVars['newteamid']) || $this->turnier->Orga) {
			/* Anzahl der gesamten Spieler &uuml;berpr&uuml;fen (Maximum) */
			if ($this->turnier->isFull($this->FrameVars['id'])) {
				return TournamentError('Die Anzahl der Spieler hat bereits das Maximum erreicht.', E_USER_WARNING, __FILE__, __LINE__);
			}
			/* Anzahl der Spieler im Team &uuml;berpr&uuml;fen (teamsize) */
			$newteam = CreateSubjectInstance($this->FrameVars['newteamid']);
			if ($newteam->type == 'turnierteam') {
				$max_teamsize = (integer) $this->turnier->teamsize;
				$free = $max_teamsize - $newteam->CountTeam();
				/* Zu viele Leute ausgew&auml;hlt? */
				if ($free > 0) {
					/* Spieler einladen */
					if (is_array($post['ids'])) {
						foreach ($post['ids'] AS $userid) {
							/* Spieler schon im Turnier? */
							if ($this->turnier->isPlayer($userid)) {
								TournamentError('Der Spieler "'.htmlspecialchars(GetSubjectName($userid)).'" nimmt bereits an diesem Turnier teil.', E_USER_WARNING, __FILE__, __LINE__);
								continue;
							}

							$iID = MysqlWriteByID(TblPrefix().'flip_tournament_invite', array ('from' => 'team', 'tournaments_id' => $this->turnier->id, 'user_id' => $userid, 'team_id' => $newteam->id));
							//Benachrichtigung schicken
							$teamname = $newteam->name;
							$this->_sendWebMessage(array ('receiver' => $userid, 'subject' => 'Einladung vom Team "'.$teamname.'"', 'message' => 'Du wurdest vom Team "'.$teamname.'" zum "'.$this->turnier->GetTournamentString().'"-Turnier eingeladen.
												        Um im Team mitzuspielen brauchst du lediglich die Einladung annehmen:
												        
												        Turnierliste->'.$this->turnier->GetTournamentString().'->Einladung annehmen
												        
										                [ '. ServerURL().$_SERVER['SCRIPT_NAME'].'?frame=overview&id='.$this->turnier->id.'&action=addplayer&ids[]='.$iID.' ].'));
						}
						TournamentError('Spieler wurde(n) eingeladen.');
						return true;
					} else {
						return TournamentError('Fehler: Du musst einen Spieler ausw&auml;hlen, den du einladen m&ouml;chtest!', E_USER_WARNING, __FILE__, __LINE__);
					}
				} else {
					return TournamentError("Es sind noch $free Pl&auml;tze frei aber ".count($post["ids"])." Spieler ausgew&auml;hlt.", E_USER_WARNING, __FILE__, __LINE__);
				}
			} else {
				return TournamentError("Kein Team angegeben.|invitePlayer() $newteam= vom typ ".gettype($newteam), E_USER_WARNING, __FILE__, __LINE__);
			}
		} else {
			return TournamentError('Du bist nicht berechtigt diese Aktion durchzuf&uuml;hren.', E_USER_ERROR, __FILE__, __LINE__);
		}
	}

	function _sendWebMessage($data) {
		include_once ($this->messagelibary);
		global $User;
		return SendMessageSendMessage($data['receiver'], $User, $data['subject'], $data['message']);
	}

	function actionaddPlayer($post) {
		$count = 0;
		foreach($post["ids"] AS $joinid) {
			if ($this->turnier->ConfirmCombatant($joinid)) {
				$count++;
			} else {
				return TournamentError("addPlayer: Fehler bei der Anfrage.", E_USER_WARNING);
			}
		}
		TournamentError("$count Spieler wurde(n) dem Team hinzugef&uuml;gt.");
		if(isset($_GET["action"])) { //reset GET-Parameters
			Redirect(EditURL(array("action"=>"", "ids[]"=>""), '', false));
		}
		return true;
	}

	function actionreminvite($post) {
		$count = 0;
		foreach($post["ids"] AS $joinid) {
			if (!MysqlDeleteByID(TblPrefix()."flip_tournament_invite", $joinid))
				return TournamentError("Fehler beim l&ouml;schen von ID '".htmlentities_single($joinid)."'!",E_USER_ERROR);
			else
				$count++;
		}
		return TournamentError("$count Einladungen wurden gel&ouml;scht.");
	}

	function frameeditTeam($get) {
		ArrayWithKeys($get, array('id','newteamid','teamid'));
		$this->Caption = "Team bearbeiten";
		if ($this->turnier->status == "open" || $this->turnier->Orga) {
			$newteam = CreateSubjectInstance($get["newteamid"]);
			$teamcount = $newteam->CountTeam();
			/* Link um Spieler hinzuzuf&uuml;gen */
			if ($teamcount < $this->turnier->teamsize && ($newteam->getProperty("fixed_size") < 1 || $newteam->getProperty("fixed_size") > $teamcount)) {
				$this->tpl["links"][] = array ("frame" => "saveTeam", "id" => $get["id"], "newteamid" => $get["newteamid"], "teamid" => $get["teamid"], "text" => "Spieler hinzuf&uuml;gen");
			}
			/* Link um Spieler zu entfernen */
			if ($teamcount > 1) {
				$this->tpl["links"][] = array ("frame" => "delfromTeam", "id" => $get["id"], "newteamid" => $get["newteamid"], "text" => "Spieler entfernen");
			}
			/* Teamname umbenennen */
			if ($this->turnier->teamsize == "1")
				$this->tpl["singleplayer"] = "true";
			else
				$this->tpl["teamname"] = $newteam->name;

			/* Team l&ouml;schbar? */
			if (MysqlReadRow("SELECT t.id FROM ".TblPrefix()."flip_tournament_combatant c, ".TblPrefix()."flip_tournament_tournaments t WHERE c.team_id='".$newteam->id."' AND (t.status='end' OR t.status='games' OR t.status='grpgames') AND t.id=c.tournament_id", true))
				$this->tpl["deleteable"] = "false";
			else
				$this->tpl["deleteable"] = "true";

			/* LigaID &auml;ndern */
			if ($this->tpl["liga"] = $this->turnier->liga->dbPrefix) {
				$this->tpl["ligaid"] = strtoupper($newteam->getProperty("tournament_".strtolower($this->turnier->liga->dbAndClassName)."_id"));
			}

		}
		$this->tpl["id"] = $get["id"];
		// Ausgabe
		return $this->tpl;
	}

	function actionchangeliga($post) {
		if (isset ($this->FrameVars["newteamid"]) && $this->turnier->liga->dbAndClassName != 'Keine' && isset ($post["ligaid"])) {
			global $User;
			if ($User->isChildOf($this->FrameVars["newteamid"]) || $this->turnier->Orga) {
				if (!$this->turnier->ValidLigaID($post["ligaid"]))
					return false;
				if ($team = CreateSubjectInstance($this->FrameVars["newteamid"], "turnierteam")) {
					//check if team is not in an other tournament where this ID is used by another team
					$user_property_name = "tournament_".strtolower($this->turnier->liga->dbAndClassName)."_id";
					$team_tournaments = MysqlReadCol("SELECT tournament_id FROM ".TblPrefix()."flip_tournament_combatant WHERE team_id='".$team->id."'","tournament_id");
					if (!MysqlReadRow("SELECT t.id FROM ".TblPrefix()."flip_tournament_combatant t
							           LEFT JOIN ".TblPrefix()."flip_user_data d ON t.team_id=d.subject_id
							           LEFT JOIN ".TblPrefix()."flip_user_column c ON c.id=d.column_id  
				                     WHERE c.name='$user_property_name'
				                       AND c.type='turnierteam'
				                       AND d.val='".escape_sqlData_without_quotes($post["ligaid"])."'
				                       AND t.tournament_id IN (".implode_sqlIn($team_tournaments).")
				                    ", true)) {
						$team->setProperty($user_property_name, $post["ligaid"]);
					} else {
						return TournamentError("Die ID wird bereits von einem anderen Team verwendet!",E_USER_WARNING,__FILE__,__LINE__);
					}
				}
			} else {
				return TournamentError("Du bist nicht berechtigt die LigaID zu &auml;ndern.|".$User->name, E_USER_WARNING, __FILE__, __LINE__);
			}
		}
	}

	function actionrenTeam($post) {
		if (isset ($this->FrameVars["newteamid"]) && $this->turnier->teamsize > 1 && isset ($post["teamname"])) {
			//edit VulkanLAN by naaux, bug behoben
			//if(!$this->turnier->IsBadName($post["teamname"])) {
			require_once('mod/mod.subject.turnierteam.php');
			if(!TurnierteamIsBadName($post["teamname"])){
				global $User;
				if ($User->isChildOf($this->FrameVars["newteamid"]) || $this->turnier->Orga) {
					$team = CreateSubjectInstance($this->FrameVars["newteamid"]);
					if ($team->setProperty("teamname", $post["teamname"])) {
						//update the cache which is used in TournamentCombatantID2Name()
						CacheSet("Combatants", MysqlReadArea("SELECT d.subject_id, d.val FROM (".TblPrefix()."flip_user_column c) LEFT JOIN ".TblPrefix()."flip_user_data d ON (d.column_id = c.id) WHERE (c.name IN ('teamname') AND (c.type = 'turnierteam'));", "subject_id"), array ("flip_tournament_combatant")); //not perfect, but the flip_user_x tables may change too often, so that caching isn't effectiv
						return TournamentError("Name wurde aktualisiert.");
					} else {
						return TournamentError("Fehler beim aktualisieren des Teamnamens.", E_USER_ERROR, __FILE__, __LINE__);
					}
				} else {
					return TournamentError("Du bist nicht berechtigt das Team umzubenennen.", E_USER_WARNING, __FILE__, __LINE__);
				}
			} else {
				return TournamentError("Der Teamname ist nicht erlaubt.", E_USER_WARNING, __FILE__, __LINE__);
			}
		} else {
			return TournamentError("Die Daten zum umbenennen sind fehlerhaft.", E_USER_NOTICE, __FILE__, __LINE__);
		}
	}

	function actiondelteam($post) {
		return $this->turnier->DeleteCombatant($post["newteamid"]);
	}

	function actionremteam($post) {
		return $this->turnier->RemoveCombatant($post["newteamid"], $post["id"]);
	}

	function framedelfromTeam($get) {
		global $User;
		$this->Caption = "Spieler ausw&auml;hlen";

		/* Spieler aus der Gruppe anzeigen */
		$combatant = CreateSubjectInstance($get["newteamid"]);
		$members = $combatant->GetChilds();
		$count = count($members);
		unset ($combatant);
		if (!$count > 0)
			return TournamentError("Keine Spieler vorhanden.<br>\n<a href=\"".basename(__FILE__)."?frame=overview&amp;id=".$get["id"]."\">zur Turnier&uuml;bersicht</a>", E_USER_WARNING);
		if (!$count > 1)
			return TournamentError("Der letzte Spieler kann nicht gel&ouml;scht werden. Bitte gesamtes Team l&ouml;schen<br>\n<a href=\"".basename(__FILE__)."?frame=overview&amp;id=".$get["id"]."\">zur Turnier&uuml;bersicht</a>", E_USER_WARNING);
		foreach ($members AS $id => $name) {
			$this->tpl["candidates"][] = array ("id" => $id, "nick" => $name);
		}
		$this->tpl["action"] = "deletePlayer";
		$this->tpl["submitval"] = "Entfernen";
		$this->tpl["file"] = basename(__FILE__);
		/* Ausgabe */
		$this->TplSub = "RemPlayer";
		return $this->tpl;
	}

	function actionabmelden($get) {
		global $User;
		$uid = $User->id;
		$turnier_id = $get['id'];
		$team_id = false;

		if ($this->turnier->status != "open") {
			return TournamentError("Das Turnier l&auml;uft bereits! Du kannst dich nicht mehr abmelden", E_USER_ERROR, __FILE__, __LINE__);
		}

		if(isset($get['teamid'])) {
			$team_id = $get['teamid'];
		}

		if(!$team_id) {
			// get $team_id from user id
			$combatants = TournamentGetCombatants($turnier_id);
			foreach($combatants as $tid => $c) {
				if($User->isChildOf($tid)) {
					$team_id = $tid;
					break;
				}
			}
		}

		if (!$User->isChildOf($team_id)) {
			return TournamentError("Du bist kein Teammitglied!", E_USER_ERROR, __FILE__, __LINE__);
		}

		$combatant = CreateSubjectInstance($team_id, "turnierteam");

		// Wenn Teamturnier, dann check ob noch Mitglieder vorhanden sind
		if($this->turnier->teamsize > 1) {
			if($combatant->CountTeam() <= 1) {
				return TournamentError("Du bist das einzige Teammitglied. Bitte l&ouml;sche stattdessen das Team!", E_USER_ERROR, __FILE__, __LINE__);
			} else {
				$combatant->remChild($uid);
				return TournamentError("Du wurdest vom Team abgemeldet.");
			}
		}

		// Wenn 1on1 Turnier, dann kann das "Team" gel�scht werden
		$this->turnier->DeleteCombatant($team_id);
		return TournamentError("Du wurdest vom Turnier abgemeldet.");

	}

	function actiondeletePlayer($post) {
		global $User;

		if ($this->turnier->status != "open") {
			return TournamentError("Das Turnier l&auml;uft bereits! Du kannst dein Team nicht mehr bearbeiten.", E_USER_ERROR, __FILE__, __LINE__);
		}

		if ($User->isChildOf($this->FrameVars["newteamid"]) || $this->turnier->Orga) {
			/* ausgew&auml;hlte Spieler aus der Gruppe entfernen */
			if (!is_array($post["ids"])) {
				return TournamentError("Keine Spielerdaten angegeben!", E_USER_ERROR, __FILE__, __LINE__);
			}

			$combatant = CreateSubjectInstance($this->FrameVars["newteamid"], "turnierteam");

			//letzter Spieler nicht entfernbar
			if ($combatant->CountTeam() == count($post["ids"])) {
				$_SERVER['HTTP_REFERER'] = basename(__FILE__)."?frame=editTeam&id=".$this->FrameVars["id"]."&newteamid=".$this->FrameVars["newteamid"];
				return TournamentError("Du kannst nicht alle Spieler aus dem Team nehmen. Um das Team zu l&ouml;schen bitte die entsprechende Option verwenden!", E_USER_ERROR, __FILE__, __LINE__);
			}

			//Spieler aus dem Team entfernen
			$i = 0;
			foreach ($post["ids"] AS $uID) {
				$combatant->remChild($uID);
				$i ++;
			}

			if (in_array($User->id, $post["ids"])) {
				//not back to editTeam if user has removed himself
				$_SERVER['HTTP_REFERER'] = basename(__FILE__)."?frame=overview&id=".$this->FrameVars["id"];
				return TournamentError("Du hast das Team verlassen.<br>\n<a href=\"".basename(__FILE__)."?frame=overview&amp;id=".$this->FrameVars["id"]."\">zur&uuml;ck zur Turnierseite</a><br>\n", E_USER_ERROR, __FILE__, __LINE__);
			}

			$n = ($i > 1) ? "n" : "";
			trigger_error_text("$i Spieler wurde$n entfernt.");
			#return true;
		} else {
			return TournamentError("Du bist nicht berechtigt diese Aktion durchzuf&uuml;hren.", E_USER_ERROR, __FILE__, __LINE__);
		}
	}
}

RunPage("TournamentPage");
?>
