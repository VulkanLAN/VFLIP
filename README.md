# V-FLIP

# VulkanLAN - [Free LAN In(tra|ter)net Portal]

**README** v1704
________________________________________________________________________________

**Inhat**

 1. VFLIP?
 2. Status
 3. Resources
 4. Copyright
 5. Features
 6. Changelog
 7. Install
 8. Credits

## 1. VFLIP?

**Was ist das VFLIP?** 
Diese Frage beantwortet sich am besten, wenn man sich den Namen
auf der Zunge zergehen lässt.
Zunächst ist das VFLIP ein Portal in Form einer Webseite, welche auf PHP und einer
Datenbank (vorzugsweise MySQL) aufsetzt. Es bietet den Entwicklern ein
umfangreiches Framework, welches viele Standard-Aufgaben (Überprüfung von
Formulareingaben, Templates, etc.) übernimmt. Somit ist das VFLIP einfach
erweiterbar für verschiedenste Zwecke einsetzbar.
Darüber hinaus spezialisiert sich das VFLIP auf den Einsatzbereich von E-Sport Events, LAN-Parties, abwicklung von Turnieren und der Vereinsverwaltung.
Solch ein E-Sport Event oder eine LAN-Party wird für gewöhnlich schon Monate im Voraus 
über das Internet geplant. Hier dient Das VFLIP nicht nur den Organisatoren durch 
ToDoTickets, Forum, Messaging, Kontenführung, Sponsorenliste usw. als
Kommunikationsplattform. Gleichzeitig bildet es durch sein Content Management System
(CMS) für die Teilnehmer die Präsentation des geplanten Events. Des weiteren können
sich die Teilnehmer im VFLIP einen Account erstellen, mit dem sie sich für
das Event anmelden, einen Sitzplatz reservieren und später auf dem Event einchecken
können. Auf dem Event stellt das VFLIP z.B. ein Turniersystem, ein Catering System zur Verfügung.

## 2. Status

Das FLIP wurde auf folgenden LAN-Parties eingeführt:
- unitedLANs presents "The Refrigeration" (6. - 8. Dez. 2002)
- unitedLANs presents "What are you waiting for? ...Christmas" (14. - 16. Nov. 2003)

anschliessend wurde es erfolgreich auf vielen anderen LAN-Parties und E-Sport Events eingesetzt. u.a.:
- nFrag 4 (700 Teilnehmer, Mai 2004)
- Slaughterhouse #12 (1200 Teilnehmer, Oktober 2005)
- OrangeLAN v6 (250 Teilnehmer, Oktober	 2004)
- Slaughterhouse #13 (1400 Teilnehmer, Oktober 2005)
- GSH-X (400 Teilnehmer, Oktober 2005)
- GSH-XI (400 Teilnehmer, April 2006)
- GSH-XII (400 Teilnehmer, Oktober 2006)
- Slaughterhouse #14 (1300 Teilnehmer, November 2006)
- OrangeLAN COLD Comeback! (250 Teilnehmer, Dezember 2006)
- 44.VulkanLAN (330 Teilnehmer, Oktober 2017)
- 46.VulkanLAN (500 Teilnehmer, Oktober 2018)
- 48.VulkanLAN (600 Teilnehmer, Oktober 2019)
- 50.VulkanLAN (500 Teilnehmer, Oktober 2021)

Zur Zeit ist das FLIP sehr leistungsfähig und recht stabil. Es geht jetzt
hauptsächlich darum Bugs zu beseitigen und Performanceprobleme aufzudecken und das Turniersystem zu aktualisieren.
Wenn ein neues Feature gewünscht wird, wird dies natürlich trotzdem implementiert ;)
(Stand: Januar 2018)


## 3. Resources

**Homepage:**
http://www.vulkanlan.at

**Fork:**
https://gitlab.com/VulkanLAN/VFLIP/

**Original**
Sourceforge:
http://sourceforge.net/projects/flip/


## 4. Copyright

VFLIP VulkanLAN - [Free LAN In(tra|ter)net Portal]
Copyright © 2001-2019 The VFLIP Project Team

Dieses Programm ist freie Software. Sie können es unter den Bedingungen der GNU
General Public License, wie von der Free Software Foundation veröffentlicht,
weitergeben und/oder modifizieren, entweder gemäß Version 3 der Lizenz oder
(nach Ihrer Option) jeder späteren Version. 

Die Veröffentlichung dieses Programms erfolgt in der Hoffnung, dass es Ihnen von
Nutzen sein wird, aber OHNE IRGENDEINE GARANTIE, sogar ohne die implizite
Garantie der MARKTREIFE oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
Details finden Sie in der GNU General Public License. 

Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
Programm erhalten haben. Falls nicht, schreiben Sie an die Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 

Eine Kopie der GNU General Public License liegt diesem Programm in der Datei
COPYING bei.


## 5. Features

Dies ist der Anfang einer Featureliste für das FLIP. Sie ist weder vollständig
noch geordnet:

- Datenbank-abstraktions-layer 
- einfache Anpassung durch eigene Templateengine 
- Verwaltung meherer Designs, für jeden Account individuell einstelllbar
- leicht bedienbares News-script 
- Benutzerdatenbank mit einfacher Erweiterbarkeit und leichter Administierbarkeit
- Onlinebetrachtung der Eventlogs 
- Anmelde-, Bezahl- und Checkin-/Checkoutsystem 
- Sitzplan (zeigt auch an, wenn Teilnehmer ihren PC aufgebaut haben) 
- CLAN Sitzplätze und CLAN Verwaltung
- Messaging über Web, Mail(?), Jabber (letztere wahlweise komplett oder nur
  Benachrichtigung) 
- Rechtesystem inkl. diverser Rechte sowie Gruppen und Vererbung der
  Gruppenrechte (!) 
- CMS, textuelle Inhalte liegen ausschließlich in der Datenbank, mit lokalem caching
- Schnittstelle fürs Hochladen von Bildern fürs CMS
- dynamisch zusammenstellbares Hauptmenü und Untermenüs 
- Menüpunkte individuell mit Lese- und texte mit Lese-/Schreibrechten versehbar 
- Newsletterfunktion mit großer Statusübersicht, Mails werden parallel versendet 
- Newsletter werden gespeichert und es können Variablen verwendet (nick, login/pw, etc.)
  werden und sind "konditionierbar"[?] (if/else/... - z.b. ist angemeldet? hat bezahlt?
  hat sitzplatz?) 
- Generation von DNS-Zonefiles für bind (?) zur Zuordnung von DNS-Namen (reiheX.platzY....)
  zu den Benutzer-IPs 
- Handout zum Ausdrucken mit den Benutzerdaten + Barcode 
- Hardwarebarcode wird unterstützt (muss noch verbessert werden) 
- Forum mit sehr differienzierbaren Rechten für verschiedene Benutzer
- eine Sponsorenliste (um die Sponsorendaten intern zu verwalten und ihre Werbebanner
  bei Bedarf öffentlich zu schalten) 
- Umfangreiches Ticketsystem für die Plannung
- eine sehr komfortable Möglichkeit für Entwickler, sich die aktuellste Datenbank aus dem 
  Internet in ihre lokale Datenbank einzuspielen. 
- Umfangreiches Tuniersystem zur individuellen Turnierplanung, -verwaltung und -durchführung
- etc, wird zeit, mal ne offizielle featureliste zusammenzustellen 


## 6. Changelog

siehe https://gitlab.com/VulkanLAN/VFLIP


## 7. Install

siehe die Datei INSTALL in diesem Verzeichnis

## 8. Credits
FLIP:

*  DocFX (Moritz Eysholdt)
*  Loom  (Daniel Raap)
*  Ryd   (Jens Mücke)
*  Scope (Matthias Groß)

VFLIP:
*  naaux (Kurt Mauerhofer)
*  tominator (Thomas Rossmann)
*  alra (Albert Raus)
*  BornTo5hoot
